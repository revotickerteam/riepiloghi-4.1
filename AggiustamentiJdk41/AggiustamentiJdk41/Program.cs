﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggiustamentiJdk41
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo key;
            Exception error = null;
            string command = "";
            string file = "";
            string mode = "";
            bool testScripts = false;
            bool doctorFalken = clsUtils.Login(args, out mode, out testScripts);
            bool lContinue = true;
            Console.WriteLine("INIZIO");
            Wintic.Data.IConnection conn = null;

            clsUtils.testScripts = false;
            if (testScripts)
                clsUtils.testScripts = testScripts;

            if (doctorFalken)
            {
                lContinue = false;
                Console.WriteLine("Buongiorno Dottor Falken:");
                Console.WriteLine(string.Format(" mode {0} ? (y/n): ", mode));
                key = Console.ReadKey(true);
                lContinue = (key.Key == ConsoleKey.Y);
            }

            if (lContinue)
            {

                try
                {
                    System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                    System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                    string path = oFileInfo.Directory.FullName;
                    System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(path + @"\scripts");
                    int countObjects = 0;
                    if (!oDirInfo.Exists)
                        throw new Exception(string.Format("Impossibile trovare il percorso {0}", path + @"\scripts"));
                    if (oDirInfo.Exists)
                    {
                        string indexFile = "";
                        foreach (System.IO.FileInfo oFileScriptInfo in oDirInfo.GetFiles())
                        {
                            if (oFileScriptInfo.Name == "index.txt")
                            {
                                indexFile = oFileScriptInfo.FullName;
                                break;
                            }
                        }
                        if (string.IsNullOrEmpty(indexFile))
                            indexFile = path + @"\" + "index.txt";

                        if (!System.IO.File.Exists(indexFile))
                            throw new Exception(string.Format("Impossibile trovare il file indice dei comandi {0}", indexFile));

                        if (!string.IsNullOrEmpty(indexFile))
                        {
                            if (System.IO.File.ReadAllLines(indexFile).Length == 0)
                                throw new Exception(string.Format("Indice dei comandi {0} VUOTO", indexFile));


                            string[] lines = (new List<string>()).ToArray();
                            if (doctorFalken)
                                clsUtils.DrF(indexFile, mode, out lines);
                            else
                                clsUtils.DrF(indexFile, "use", out lines);

                            for (int nInterazione = 1; nInterazione <= (!doctorFalken ? 2 : 1); nInterazione++)
                            {
                                bool lCheckValidFiles = (nInterazione == 1);

                                foreach (string line in lines)
                                {
                                    file = "";
                                    command = "";
                                    if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#"))
                                    {
                                        if (line.StartsWith("CONNECTION=") && !doctorFalken && !lCheckValidFiles)
                                        {
                                            Console.WriteLine("Nuova Connessione:");
                                            if (conn != null && conn.State == System.Data.ConnectionState.Open)
                                            {
                                                Console.WriteLine(" chiusura della precedente connessione...");
                                                conn.Close();
                                                conn.Dispose();
                                                conn = null;
                                            }
                                            string user = "";
                                            string password = "";
                                            string dataSource = "";


                                            user = line.Split('=')[1].Split(';')[0];
                                            dataSource = line.Split('=')[1].Split(';')[1];

                                            Console.WriteLine(string.Format(" {0} {1}", user, dataSource));

                                            password = "";
                                            if (user.Trim().ToUpper() == "WTIC")
                                                password = "OBELIX";
                                            else if (user.Trim().ToUpper() == "CINEMA" || user.Trim().ToUpper() == "TEX")
                                                password = user;
                                            else if (user.Trim().ToUpper() == "SYSTEM")
                                            {
                                                password = "";
                                                Console.Write(string.Format("Inserire la password per l'utente {0} e premere Invio:", user));
                                                while (true)
                                                {
                                                    key = Console.ReadKey(true);
                                                    if (key.Key != ConsoleKey.Backspace)
                                                    {
                                                        if (key.Key != ConsoleKey.Enter)
                                                        {
                                                            password += key.KeyChar;
                                                            Console.Write("*");
                                                        }
                                                        else
                                                        {
                                                            string f = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.Write("\b");
                                                    }
                                                    if (key.Key == ConsoleKey.Enter)
                                                        break;
                                                }
                                            }
                                            Console.WriteLine(" apertura connessione...");
                                            conn = new Wintic.Data.oracle.CConnectionOracle();
                                            conn.ConnectionString = string.Format("USER={0};PASSWORD={1};DATA SOURCE={2}", user, password, dataSource);
                                            conn.Open();
                                            Console.WriteLine(" connesso.");
                                        }
                                        else if (line.StartsWith("OBJECT="))
                                        {
                                            //OBJECT=IDX_Z_KEEP_A_02;INDEX;FALSE;TRUE;018_IDX_Z_KEEP_A_02.txt
                                            countObjects += 1;
                                            if (doctorFalken)
                                                Console.WriteLine(string.Format("{0} oggetto", mode));
                                            else
                                                Console.WriteLine("Creazione oggetto:");

                                            if (!doctorFalken && !lCheckValidFiles)
                                            {
                                                if (conn == null)
                                                    throw new Exception("Impossibile eseguire un comando senza connessione.");
                                            }


                                            string objectName = line.Split('=')[1].Split(';')[0];
                                            string objectType = line.Split('=')[1].Split(';')[1];
                                            bool forceCreate = line.Split('=')[1].Split(';')[2].Trim().ToUpper() == "TRUE";
                                            bool raiseError = line.Split('=')[1].Split(';')[3].Trim().ToUpper() == "TRUE";
                                            string objectFile = line.Split('=')[1].Split(';')[4];
                                            file = objectFile;

                                            if (doctorFalken)
                                            {
                                                clsUtils.DrF(path + @"\scripts\" + objectFile, mode, out lines);
                                                if (mode == "encrypt")
                                                {
                                                    string[] checkLines = null;
                                                    clsUtils.DrF(path + @"\scripts\" + objectFile, "decrypt", out checkLines, true);
                                                }
                                            }
                                            else if (lCheckValidFiles)
                                            {
                                                string checkCommand = "";
                                                if (clsUtils.testScripts)
                                                    checkCommand = System.IO.File.ReadAllText(path + @"\scripts\" + objectFile);
                                                else
                                                    checkCommand = clsUtils.Decrypt(System.IO.File.ReadAllText(path + @"\scripts\" + objectFile));
                                            }
                                            else
                                            {
                                                Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();
                                                String queryVerifica = "SELECT OBJECT_NAME FROM ALL_OBJECTS WHERE OBJECT_NAME = :pOBJECT_NAME AND OBJECT_TYPE = :pOBJECT_TYPE";
                                                string ownerObject = "";
                                                if (objectName.Contains("."))
                                                {
                                                    ownerObject = objectName.Split('.')[0];
                                                    objectName = objectName.Split('.')[1];
                                                    oPars.Add(":pOBJECT_NAME", objectName);
                                                    oPars.Add(":pOBJECT_TYPE", objectType);
                                                    oPars.Add(":pOWNER", ownerObject);
                                                    queryVerifica = "SELECT OBJECT_NAME FROM ALL_OBJECTS WHERE OBJECT_NAME = :pOBJECT_NAME AND OBJECT_TYPE = :pOBJECT_TYPE AND OWNER = :pOWNER";
                                                }
                                                else
                                                {
                                                    oPars.Add(":pOBJECT_NAME", objectName);
                                                    oPars.Add(":pOBJECT_TYPE", objectType);
                                                }
                                                System.Data.DataTable tabCheck = conn.ExecuteQuery(queryVerifica, oPars);
                                                bool lExists = (tabCheck != null && tabCheck.Rows != null && tabCheck.Rows.Count > 0);
                                                Console.WriteLine(string.Format(" tipo {0} oggetto {1} esiste {2} creazione forzata:{3}\r\n{4}", objectType, objectName, lExists.ToString(), forceCreate.ToString(), objectFile));
                                                if (!lExists || forceCreate)
                                                {
                                                    Console.WriteLine(" creazione...");
                                                    if (!System.IO.File.Exists(path + @"\scripts\" + objectFile))
                                                    {
                                                        throw new Exception(string.Format("File script non trovato\r\n{0}", path + @"\scripts\" + objectFile));
                                                    }
                                                    if (clsUtils.testScripts)
                                                        command = System.IO.File.ReadAllText(path + @"\scripts\" + objectFile);
                                                    else
                                                        command = clsUtils.Decrypt(System.IO.File.ReadAllText(path + @"\scripts\" + objectFile));
                                                    if (command != "")
                                                    {
                                                        //Console.WriteLine(command);
                                                        try
                                                        {
                                                            conn.ExecuteNonQuery(command);
                                                        }
                                                        catch (Exception exCommand)
                                                        {
                                                            if (raiseError)
                                                                throw;
                                                            else
                                                                Console.WriteLine("Errore non importante.");
                                                        }
                                                        //Console.WriteLine("Premere un tasto per continuare.");
                                                        //Console.ReadKey(true);
                                                    }
                                                    else
                                                        throw new Exception("comando non valido.");
                                                }
                                                else
                                                {
                                                    Console.WriteLine(" esiste e non deve essere forzato.");
                                                }
                                            }
                                        }
                                        else if (line.StartsWith("SCRIPT="))
                                        {
                                            countObjects += 1;
                                            if (doctorFalken)
                                                Console.WriteLine(string.Format("{0} oggetto", mode));
                                            else
                                                Console.WriteLine("esecuzione script:");

                                            if (!doctorFalken && !lCheckValidFiles)
                                            {
                                                if (conn == null)
                                                    throw new Exception("Impossibile eseguire un comando senza connessione.");
                                            }


                                            string scriptFile = line.Split('=')[1];
                                            file = scriptFile;

                                            if (doctorFalken)
                                            {
                                                clsUtils.DrF(path + @"\scripts\" + scriptFile, mode, out lines);
                                                if (mode == "encrypt")
                                                {
                                                    string[] checkLines = null;
                                                    clsUtils.DrF(path + @"\scripts\" + scriptFile, "decrypt", out checkLines, true);
                                                }
                                            }
                                            else if (lCheckValidFiles)
                                            {
                                                string checkCommand = "";
                                                if (clsUtils.testScripts)
                                                    checkCommand = System.IO.File.ReadAllText(path + @"\scripts\" + scriptFile);
                                                else
                                                    checkCommand = clsUtils.Decrypt(System.IO.File.ReadAllText(path + @"\scripts\" + scriptFile));
                                            }
                                            else
                                            {
                                                Console.WriteLine(" esecuzione...");
                                                if (!System.IO.File.Exists(path + @"\scripts\" + scriptFile))
                                                {
                                                    throw new Exception(string.Format("File script non trovato\r\n{0}", path + @"\scripts\" + scriptFile));
                                                }
                                                if (clsUtils.testScripts)
                                                    command = System.IO.File.ReadAllText(path + @"\scripts\" + scriptFile);
                                                else
                                                    command = clsUtils.Decrypt(System.IO.File.ReadAllText(path + @"\scripts\" + scriptFile));
                                                if (command != "")
                                                {
                                                    //Console.WriteLine(command);
                                                    conn.BeginTransaction();
                                                    try
                                                    {
                                                        conn.ExecuteNonQuery(command);
                                                        conn.EndTransaction();
                                                    }
                                                    catch (Exception exCommand)
                                                    {
                                                        conn.RollBack();
                                                        throw;
                                                    }
                                                }
                                                else
                                                    throw new Exception("comando non valido.");
                                            }
                                        }
                                    }
                                }

                            }



                        }

                        if (countObjects == 0)
                            throw new Exception("Nessun oggetto creato");
                    }
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(command))
                        Console.WriteLine("Ultimo comando\r\n" + command);
                    if (!string.IsNullOrEmpty(file))
                        Console.WriteLine("Ultimo file\r\n" + file);
                    error = ex;
                    Console.WriteLine("ERRORE");
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        if (conn.State == System.Data.ConnectionState.Open)
                        {
                            Console.WriteLine("Chiusura connessione...");
                            conn.Close();
                        }
                        conn.Dispose();
                    }
                }

                if (error == null)
                    Console.WriteLine("terminato OK.");
                else
                    Console.WriteLine("terminato con errori.");
            }
            Console.WriteLine("FINE.");
            Console.WriteLine("Premere un tasto per terminare.");
            Console.ReadKey(true);
        }
    }
}
