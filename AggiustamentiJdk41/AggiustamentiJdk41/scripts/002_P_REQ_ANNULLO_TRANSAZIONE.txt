﻿CREATE OR REPLACE PROCEDURE WTIC."P_REQ_ANNULLO_TRANSAZIONE" (cCodiceCarta VARCHAR2 := null, nProgressivo IN NUMBER := null, cSigillo IN VARCHAR2 := null, nFlagLta IN NUMBER, logCUR OUT SYS_REFCURSOR, errCUR OUT SYS_REFCURSOR, wrnCUR OUT SYS_REFCURSOR, cauCUR OUT SYS_REFCURSOR, logANN OUT SYS_REFCURSOR)
IS
    nErrorCode NUMBER;
    nWarningCode NUMBER;
    oRow WTIC.OBJ_ROW_LOG_BASE;
    v_tab WTIC.OBJ_TABLE_LOG_BASE;
    v_tab_annullo WTIC.OBJ_TABLE_LOG_BASE;
    cProprietaValore VARCHAR2(100);
    nIdTex NUMBER;
    nContaRateiAttivi NUMBER;
    v_tab_ratei WTIC.OBJ_TABLE_LOG_BASE;
    nRiga NUMBER;
    dDataEvento DATE;
    v_tab_annullamento_empty WTIC.OBJ_TABLE_LOG_BASE;

    nSTATO_CALENDARIO NUMBER;
    nSTATO_DATA_CALENDARIO NUMBER;
    cSTATO_TITOLO_LTA VARCHAR2(2);
    cLISTA_CAUSALI_VALIDE VARCHAR2(100);
    oRowCombAnnulli OBJ_ROW_COMBINAZIONI_ANNULLO;

    oRow_PrimoRateoTurnoFisso WTIC.OBJ_ROW_LOG_BASE;
    nCountEventiTurnoFissoAnn NUMBER;
    cTIPO_TURNO VARCHAR2(2);
BEGIN
    nErrorCode := 0;
    nWarningCode := 0;
    nSTATO_CALENDARIO := NULL;
    nSTATO_DATA_CALENDARIO := NULL;
    cSTATO_TITOLO_LTA := NULL;
    oRowCombAnnulli := NULL;
    oRow_PrimoRateoTurnoFisso := NULL;
    nCountEventiTurnoFissoAnn := NULL;

    /* VALORIZZO SEMPRE IL CURSORE DI ERRORE CON NESSUNA RIGA */
    OPEN errCUR FOR SELECT * FROM (SELECT 0 AS CODICE, '' AS LIVELLO, '' AS DESCRIZIONE FROM DUAL) WHERE CODICE = -1;
    OPEN wrnCUR FOR SELECT * FROM (SELECT 0 AS CODICE, '' AS LIVELLO, '' AS DESCRIZIONE FROM DUAL) WHERE CODICE = -1;

    /* VALORIZZO SEMPRE IL CURSORE DELLE CAUSALI CON NESSUNA RIGA */
    OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE CODICE IS NULL;

    /* LA FUNZIONE F_TABLE_LOG_CARTA_PROG_SIGILLO ritorna sempre v_tab ANCHE SE CON NESSUNA RIGA */
    v_tab := WTIC.F_TABLE_LOG_CARTA_PROG_SIGILLO(cCodiceCarta, nProgressivo, cSigillo);

    IF (nFlagLta = 1) THEN
        OPEN logCUR FOR SELECT
        LOG.*,
        LTA.CF_TITOLARE_CA,
        LTA.CODICE_SISTEMA_CA,
        LTA.DATA_APERTURA_ACCESSI,
        LTA.ORA_APERTURA_ACCESSI,
        LTA.DATA_ORA_APERTURA_ACCESSI,
        LTA.CORRISPETTIVO_LORDO,
        LTA.DATA_ANNULLAMENTO,
        LTA.ORA_ANNULLAMENTO,
        LTA.CODICE_CARTA_ANNULLAMENTO,
        LTA.PROGRESSIVO_TITOLO_ANN,
        LTA.SIGILLO_ANNULLAMENTO,
        LTA.CODICE_SUPP_IDENTIFICATIVO,
        LTA.DESCR_SUPP_IDENTIFICATIVO,
        LTA.IDENTIFICATIVO_SUPP,
        LTA.IDENTIFICATIVO_SUPP_ALT,
        LTA.DATA_NASCITA_PARTECIPANTE,
        LTA.LUOGO_NASCITA_PARTECIPANTE,
        LTA.DATA_INSERIMENTO_LTA,
        LTA.ORA_INSERIMENTO_LTA,
        LTA.DATA_ORA_INSERIMENTO_LTA,
        LTA.STATO_TITOLO,
        LTA.DATA_INGRESSO_CA,
        LTA.ORA_INGRESSO_CA,
        LTA.DATA_ORA_INGRESSO_CA
        FROM TABLE(v_tab) LOG, VR_LTA LTA
    WHERE LOG.IDVEND = LTA.IDVEND(+);
    ELSE
        OPEN logCUR FOR SELECT * FROM TABLE(v_tab);
    END IF;

    IF (v_tab IS NULL OR v_tab.LAST IS NULL OR v_tab.LAST = 0) THEN
        nErrorCode := 1;    /* Transazione non trovata. */
    END IF;

    IF (nErrorCode = 0 AND v_tab IS NOT NULL AND v_tab.LAST IS NOT NULL AND v_tab.LAST > 0) THEN

        oRow := v_tab(v_tab.LAST);

        /* VERIFICO SUBITO SE SI TRATTA DI UNA TRANSAZIONE DI ANNULLO */
        IF (nErrorCode = 0 AND oRow.ANNULLATO = 'A') THEN
            nErrorCode := 2; /* ERRORE BLOCCANTE : Transazione di annullo. */
        END IF;

        /* VERIFICO SE LA TRANSAZIONE E' GIA' STATA ANNULLATA */
        IF (nErrorCode = 0) THEN

            v_tab_annullo := F_TABLE_LOG_CARTA_PROG_ANNULLO(oRow.CODICE_CARTA, oRow.PROGRESSIVO_TITOLO);

            IF (v_tab_annullo IS NOT NULL AND v_tab_annullo.LAST IS NOT NULL AND v_tab_annullo.LAST > 0) THEN

                nErrorCode := 3;    /* ERRORE BLOCCANTE : Transazione annullata precedentemente. */

                IF (nFlagLta = 1) THEN
                    OPEN logANN FOR SELECT
                    LOG.*,
                    LTA.CF_TITOLARE_CA,
                    LTA.CODICE_SISTEMA_CA,
                    LTA.DATA_APERTURA_ACCESSI,
                    LTA.ORA_APERTURA_ACCESSI,
                    LTA.DATA_ORA_APERTURA_ACCESSI,
                    LTA.CORRISPETTIVO_LORDO,
                    LTA.DATA_ANNULLAMENTO,
                    LTA.ORA_ANNULLAMENTO,
                    LTA.CODICE_CARTA_ANNULLAMENTO,
                    LTA.PROGRESSIVO_TITOLO_ANN,
                    LTA.SIGILLO_ANNULLAMENTO,
                    LTA.CODICE_SUPP_IDENTIFICATIVO,
                    LTA.DESCR_SUPP_IDENTIFICATIVO,
                    LTA.IDENTIFICATIVO_SUPP,
                    LTA.IDENTIFICATIVO_SUPP_ALT,
                    LTA.DATA_NASCITA_PARTECIPANTE,
                    LTA.LUOGO_NASCITA_PARTECIPANTE,
                    LTA.DATA_INSERIMENTO_LTA,
                    LTA.ORA_INSERIMENTO_LTA,
                    LTA.DATA_ORA_INSERIMENTO_LTA,
                    LTA.STATO_TITOLO,
                    LTA.DATA_INGRESSO_CA,
                    LTA.ORA_INGRESSO_CA,
                    LTA.DATA_ORA_INGRESSO_CA
                    FROM TABLE(v_tab_annullo) LOG, VR_LTA LTA
                WHERE LOG.IDVEND = LTA.IDVEND(+);
                ELSE
                    OPEN logANN FOR SELECT * FROM TABLE(v_tab_annullo);
                END IF;
            END IF;
        END IF;

        /* SE SI TRATTA DI UN BIGLIETTO DI QUALSIASI NATURA PRENDO LE INFORMAZIONI DELL'EVENTO e DELL'INGRESSO */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'T') THEN
            P_GETINFO_BIGL_PER_ANNULLO(oRow, nSTATO_CALENDARIO, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode);
        END IF;

        /* SE SI TRATTA DI UN BIGLIETTO RATEO CONTROLLI SPECIFICI */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'T' AND oRow.TITOLO_IVA_PREASSOLTA = 'B') THEN

            IF (nErrorCode = 0 AND (oRow.CODICE_BIGLIETTO_ABBONAMENTO IS NULL OR LENGTH(TRIM(oRow.CODICE_BIGLIETTO_ABBONAMENTO)) <> 8)) THEN
                nErrorCode := 201; /* ERRORE BLOCCANTE : Codice biglietto abbonamento non valido. */
            END IF;

            IF (nErrorCode = 0 AND (oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO IS NULL OR oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO <= 0)) THEN
                nErrorCode := 202; /* ERRORE BLOCCANTE : Num Prog biglietto abbonamento non valido. */
            END IF;

            /* RICERCO IL TIPO DI TURNO SULL'ABBONAMENTO PRADRE */
            IF (nErrorCode = 0) THEN
                cTIPO_TURNO := NULL;
                SELECT MAX(TIPO_TURNO) INTO cTIPO_TURNO FROM VR_LOGTRANS WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABBONAMENTO = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;
                IF (cTIPO_TURNO IS NULL) THEN
                    SELECT MAX(TIPO_TURNO) INTO cTIPO_TURNO FROM VR_LOGTRANS_ARCHIVE WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABBONAMENTO = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;
                END IF;
                IF (cTIPO_TURNO IS NULL) THEN
                    SELECT MAX(TIPO_TURNO) INTO cTIPO_TURNO FROM VR_SMART_TESSERE WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABB = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;
                END IF;
                IF (cTIPO_TURNO IS NULL) THEN
                    SELECT MAX(TIPO_TURNO) INTO cTIPO_TURNO FROM VR_STORICO_SMART_TESSERE WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABB = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;
                END IF;
                IF (cTIPO_TURNO IS NULL) THEN
                    nErrorCode := 203; /* ERRORE BLOCCANTE : impossibile trovare abbonamento padre. */
                END IF;
                IF (nErrorCode = 0) THEN
                    IF (cTIPO_TURNO = 'F') THEN
                        nErrorCode := 204; /* ERRORE BLOCCANTE : abbonamento padre turno fisso, annullare abbonamento. */
                    END IF;
                END IF;

                IF (nErrorCode = 0) THEN
                    SELECT MAX(IDTEX) INTO nIdTex FROM VR_TESSERE WHERE DOTAZIONEF = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND IDTEX = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;
                    IF (nIdTex IS NULL) THEN
                        nErrorCode := 205; /* ERRORE BLOCCANTE : impossibile trovare la tessera padre. */
                    END IF;
                END IF;

                IF (nErrorCode = 0) THEN
                    SELECT VALORE INTO cProprietaValore FROM WTIC.PROPRIETA_RIEPILOGHI WHERE PROPRIETA = 'ANNULLO_RATEI';
                    IF (cProprietaValore IS NULL OR cProprietaValore <> 'ABILITATO') THEN
                        nErrorCode := 206;    /* ERRORE BLOCCANTE : Annullo rateo non consentito come da configurazione. */
                    END IF;
                END IF;
            END IF;
        END IF;

        /* SE SI TRATTA DI UN BIGLIETTO DI QUALSIASI NATURA CONTROLLO FINALE */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'T') THEN
            P_CHECK_BIGL_ANNULLO(oRow.DIGITALE_TRADIZIONALE, 'N', nSTATO_CALENDARIO, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode, nWarningCode, cLISTA_CAUSALI_VALIDE);
            IF (nErrorCode = 0) THEN
                OPEN cauCUR FOR SELECT CAUSALI_DI_ANNULLO.CODICE, CAUSALI_DI_ANNULLO.DESCRIZIONE FROM CAUSALI_DI_ANNULLO,
                                (SELECT REGEXP_SUBSTR(cLISTA_CAUSALI_VALIDE,'[^,]+', 1, LEVEL) AS CODICE FROM DUAL CONNECT BY REGEXP_SUBSTR(cLISTA_CAUSALI_VALIDE, '[^,]+', 1, LEVEL) IS NOT NULL) CAUSALI_APPLICABILI
                                WHERE CAUSALI_DI_ANNULLO.CODICE = CAUSALI_APPLICABILI.CODICE;
            END IF;
        END IF;

        /* SE SI TRATTA DI UN ABBONAMENTO CONTROLLI SPECIFICI*/
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'A') THEN

            /* VERIFICO SE SI TRATTA DI UN ABBONAMENTO CIVETTA */
            IF (nErrorCode = 0 AND oRow.CODICE_ABBONAMENTO LIKE '#%') THEN
                nErrorCode := 301;    /* ERRORE BLOCCANTE : Annullo abbonamento civetta non consentito. */
            END IF;

            /* VERIFICO CODICE_ABBONAMENTO */
            IF (nErrorCode = 0 AND (oRow.CODICE_ABBONAMENTO IS NULL OR LENGTH(TRIM(oRow.CODICE_ABBONAMENTO)) <> 8)) THEN
                nErrorCode := 302;    /* ERRORE BLOCCANTE : Codice abbonamento non valido. */
            END IF;

            /* VERIFICO NUM_PROG_ABBONAMENTO */
            IF (nErrorCode = 0 AND (oRow.NUM_PROG_ABBONAMENTO IS NULL OR oRow.NUM_PROG_ABBONAMENTO <= 0)) THEN
                nErrorCode := 303;    /* ERRORE BLOCCANTE : Codice abbonamento non valido. */
            END IF;

            /* RICERCO LA TESSERA */
            IF (nErrorCode = 0) THEN
                SELECT NVL(MAX(IDTEX), 0) INTO nIdTex FROM VR_TESSERE WHERE DOTAZIONEF = oRow.CODICE_ABBONAMENTO AND IDTEX = oRow.NUM_PROG_ABBONAMENTO;
                IF (nIdTex = 0) THEN
                    nErrorCode := 304; /* ERRORE BLOCCANTE : impossibile trovare la tessera. */
                END IF;
            END IF;

            /* VERIFICO SUL TURNO LIBERO SE TUTTI I RATEI SONO ANNULLATI */
            IF (nErrorCode = 0 AND oRow.TIPO_TURNO = 'L') THEN
                nContaRateiAttivi := WTIC.F_NUM_RATEI_ABBONAMENTO_ATTIVI(oRow.CODICE_ABBONAMENTO, oRow.NUM_PROG_ABBONAMENTO);
                IF (nContaRateiAttivi > 0) THEN
                    nErrorCode := 305;  /* ERRORE BLOCCANTE: Annullo abbonamento turno libero, non tutti i rati sono stati annullati. */
                END IF;
            END IF;

            /* PER IL TURNO FISSO OCCORRE VERIFICARE IL PRIMO RATEO */
            IF (nErrorCode = 0 AND oRow.TIPO_TURNO = 'L') THEN
                OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE CODICE IN ('001','002','004','005');
            END IF;

            /* PER IL TURNO FISSO OCCORRE VERIFICARE IL PRIMO RATEO */
            IF (nErrorCode = 0 AND oRow.TIPO_TURNO = 'F') THEN
                dDataEvento := NULL;
                v_tab_ratei := WTIC.F_TABLE_LOG_RATEI_ABB_FISSO(oRow.CODICE_ABBONAMENTO, oRow.NUM_PROG_ABBONAMENTO, oRow.SIGILLO);
                IF (v_tab_ratei IS NOT NULL AND v_tab_ratei.LAST IS NOT NULL AND v_tab_ratei.LAST > 0) THEN
                    nCountEventiTurnoFissoAnn := 0;
                    FOR nRiga IN v_tab_ratei.FIRST .. v_tab_ratei.LAST
                    LOOP
                        IF (v_tab_ratei(nRiga).ANNULLATO = ' ') THEN
                            IF (dDataEvento IS NULL) THEN
                                dDataEvento := v_tab_ratei(nRiga).DATA_ORA_EVENTO;
                                oRow_PrimoRateoTurnoFisso := v_tab_ratei(nRiga);
                                P_GETINFO_BIGL_PER_ANNULLO(oRow_PrimoRateoTurnoFisso, nSTATO_CALENDARIO, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode);
                                IF (nSTATO_CALENDARIO = 2) THEN
                                    nCountEventiTurnoFissoAnn := nCountEventiTurnoFissoAnn + 1;
                                END IF;
                            ELSE
                                IF (v_tab_ratei(nRiga).DATA_ORA_EVENTO < dDataEvento) THEN
                                    dDataEvento := v_tab_ratei(nRiga).DATA_ORA_EVENTO;
                                    oRow_PrimoRateoTurnoFisso := v_tab_ratei(nRiga);
                                    P_GETINFO_BIGL_PER_ANNULLO(oRow_PrimoRateoTurnoFisso, nSTATO_CALENDARIO, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode);
                                    IF (nSTATO_CALENDARIO = 2) THEN
                                        nCountEventiTurnoFissoAnn := nCountEventiTurnoFissoAnn + 1;
                                    END IF;
                                END IF;
                            END IF;
                        END IF;
                        IF (nErrorCode > 0) THEN
                            EXIT;
                        END IF;
                    END LOOP;

                    IF (nErrorCode = 0) THEN
                        IF (dDataEvento IS NOT NULL AND oRow_PrimoRateoTurnoFisso IS NOT NULL) THEN
                            P_GETINFO_BIGL_PER_ANNULLO(oRow_PrimoRateoTurnoFisso, nSTATO_CALENDARIO, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode);
                            IF (nErrorCode = 0) THEN
                                IF (nCountEventiTurnoFissoAnn = oRow.NUMERO_EVENTI_ABILITATI) THEN
                                    /* TUTTI GLI EVENTI SONO STATI ANNULLATI quindi passo nSTATO_CALENDARIO = 2 */
                                    P_CHECK_BIGL_ANNULLO(oRow_PrimoRateoTurnoFisso.DIGITALE_TRADIZIONALE, 'S', 2, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode, nWarningCode, cLISTA_CAUSALI_VALIDE);
                                ELSE
                                    /* NON TUTTI GLI EVENTI SONO STATI ANNULLATI quindi passo nSTATO_CALENDARIO = 0 */
                                    P_CHECK_BIGL_ANNULLO(oRow_PrimoRateoTurnoFisso.DIGITALE_TRADIZIONALE, 'S', 0, nSTATO_DATA_CALENDARIO, cSTATO_TITOLO_LTA, nErrorCode, nWarningCode, cLISTA_CAUSALI_VALIDE);
                                END IF;
                                IF (nErrorCode = 0) THEN
                                    OPEN cauCUR FOR SELECT CAUSALI_DI_ANNULLO.CODICE, CAUSALI_DI_ANNULLO.DESCRIZIONE FROM CAUSALI_DI_ANNULLO,
                                                    (SELECT REGEXP_SUBSTR(cLISTA_CAUSALI_VALIDE,'[^,]+', 1, LEVEL) AS CODICE FROM DUAL CONNECT BY REGEXP_SUBSTR(cLISTA_CAUSALI_VALIDE, '[^,]+', 1, LEVEL) IS NOT NULL) CAUSALI_APPLICABILI
                                                    WHERE CAUSALI_DI_ANNULLO.CODICE = CAUSALI_APPLICABILI.CODICE;
                                END IF;
                            END IF;
                        ELSE
                            nErrorCode := 306;  /* ERRORE BLOCCANTE: Annullo abbonamento turno fisso, impossibile determinare la data del primo evento usufruibile. */
                        END IF;
                    END IF;
                ELSE
                    nErrorCode := 306;  /* ERRORE BLOCCANTE: Annullo abbonamento turno fisso, impossibile determinare la data del primo evento usufruibile. */
                END IF;
            END IF;
        END IF;
    END IF;

    IF (nErrorCode > 0) THEN
        OPEN errCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nErrorCode;
    END IF;

    IF (nWarningCode > 0) THEN
        OPEN wrnCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nWarningCode;
    END IF;

    IF logANN IS NULL THEN
        v_tab_annullamento_empty := WTIC.OBJ_TABLE_LOG_BASE();
        OPEN logANN FOR SELECT * FROM TABLE(v_tab_annullamento_empty);
    END IF;
END;

