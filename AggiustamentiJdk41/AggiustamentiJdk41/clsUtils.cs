﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AggiustamentiJdk41
{

    
    public class clsUtils
    {
        public static bool testScripts = false;

        private class clsItemEncDec
        {
            public string Hash { get; set; }
            public string Value { get; set; }

            public clsItemEncDec()
            { }

            public clsItemEncDec(string value)
            {
                this.Value = value;
                this.Hash = CalcHash(this.Value);
            }

            public static string CalcHash(string theString)
            {
                string hash;
                using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
                {
                    hash = BitConverter.ToString(
                      md5.ComputeHash(Encoding.UTF8.GetBytes(theString))
                    ).Replace("-", String.Empty);
                }
                return hash;
            }
        }

        private const string kToken = "F5AA450FE2119D0B7EDF48AF";
        private static string Encrypt(string toEncrypt, string parCryptKey)
        {
            return Encrypt(toEncrypt, parCryptKey, true);
        }

        private static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            clsItemEncDec item = new clsItemEncDec(toEncrypt);
            string value = Newtonsoft.Json.JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.Indented);

            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(value);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        

        private static string Decrypt(string toDecrypt, string parCryptKey)
        {
            return Decrypt(toDecrypt, parCryptKey, true);
        }

        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            string objectString = UTF8Encoding.UTF8.GetString(resultArray);
            string result = "";
            try
            {
                clsItemEncDec item = (clsItemEncDec)Newtonsoft.Json.JsonConvert.DeserializeObject<clsItemEncDec>(objectString);
                if (item.Hash == clsItemEncDec.CalcHash(item.Value))
                    result = item.Value;
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                result = "";
                throw new Exception("comando non valido.");
            }
            


            return result;
        }


        public static string Decrypt(string value)
        {
            return Decrypt(value, kToken); ;
        }

        public static string Encrypt(string value)
        {
            return Encrypt(value, kToken);
        }

        public static bool Login(string[] args, out string mode, out bool testScripts)
        {
            mode = "";
            testScripts = false;
            bool result = false;
            //args != null && (args.Length == 2)
            if (args != null && args.Length == 3)
            {
                result = args[0] == "andrea" && args[1] == "actarus" && (args[2] == "encrypt" || args[2] == "decrypt");
                if (result)
                    mode = args[2];
            }
            else if (args != null && args.Length == 1 && args[0] == "test")
            {
                testScripts = true;
            }
            return result;
        } 

        public static void DrF(string file, string mode, out string[] lines, bool decryptReadOnly = false)
        {
            lines = (new List<string>()).ToArray();
            if (mode == "encrypt")
            {
                lines = System.IO.File.ReadAllLines(file);
                System.IO.File.WriteAllText(file, clsUtils.Encrypt(System.IO.File.ReadAllText(file)));
            }
            else if (mode == "decrypt")
            {
                string content = Decrypt(System.IO.File.ReadAllText(file), kToken);
                if (decryptReadOnly)
                {
                    if (content == "")
                        throw new Exception(string.Format("file {0} non valido", file));
                }
                else if (content != "")
                {
                    System.IO.File.WriteAllText(file, content);
                    lines = System.IO.File.ReadAllLines(file);
                }
            }
            else if (mode == "use")
            {
                string content = "";
                if (testScripts)
                    content = System.IO.File.ReadAllText(file);
                else
                    content = Decrypt(System.IO.File.ReadAllText(file), kToken);
                lines = content.Replace("\r\n", "\r").Split('\r');
            }
        }
    }
}
