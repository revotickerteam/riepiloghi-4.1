﻿CREATE OR REPLACE FORCE VIEW WTIC.LOGTRANS
(
   FILE_DATE,
   LOG_DATA,
   IDVEND
)
AS
   SELECT log_date AS file_date,
             RPAD (codice_fiscale_organizzatore, 16, ' ')
          || RPAD (codice_fiscale_titolare, 16, ' ')
          || titolo_abbonamento
          || titolo_iva_preassolta
          || spettacolo_intrattenimento
          || valuta
          || LPAD (TO_CHAR (imponibile_intrattenimenti), 9, '0')
          || RPAD (NVL (ordine_di_posto, ' '), 2, ' ')
          || RPAD (NVL (posto, ' '), 6, ' ')
          || tipo_titolo
          || RPAD (NVL (annullato, ' '), 1, ' ')
          || LPAD (TO_CHAR (progressivo_annullati), 8, '0')
          || data_emissione_annullamento
          || ora_emissione_annullamento
          || LPAD (TO_CHAR (progressivo_titolo), 8, '0')
          || RPAD (NVL (codice_punto_vendita, ' '), 8, ' ')
          || sigillo
          || codice_sistema
          || codice_carta
          || RPAD (' ', 40, ' ')
          || DECODE (
                titolo_abbonamento,
                'T',    RPAD (codice_locale, 13, ' ')
                     || RPAD (data_evento, 8, ' ')
                     || RPAD (tipo_evento, 2, ' ')
                     || RPAD (titolo_evento, 40, ' ')
                     || RPAD (ora_evento, 4, ' ')
                     || RPAD (NVL (causale_omaggio_riduzione, ' '), 30, ' '),
                   RPAD (tipo_turno, 1, ' ')
                || LPAD (TO_CHAR (numero_eventi_abilitati), 4, '0')
                || LPAD (data_limite_validita, 8, ' ')
                || RPAD (codice_abbonamento, 8, ' ')
                || LPAD (TO_CHAR (num_prog_abbonamento), 8, '0')
                || LPAD (TO_CHAR (rateo_evento), 9, '0')
                || LPAD (TO_CHAR (iva_rateo), 9, '0')
                || LPAD (TO_CHAR (rateo_imponibile_intra), 9, '0')
                || RPAD (NVL (causale_omaggio_riduzione_open, ' '), 30, ' ')
                || '           ')
          || DECODE (
                titolo_iva_preassolta,
                'N',    LPAD (TO_CHAR (corrispettivo_titolo), 9, '0')
                     || LPAD (TO_CHAR (corrispettivo_prevendita), 8, '0')
                     || LPAD (TO_CHAR (iva_titolo), 9, '0')
                     || LPAD (TO_CHAR (iva_prevendita), 8, '0')
                     || '                ',
                   LPAD (TO_CHAR (corrispettivo_figurativo), 9, '0')
                || LPAD (TO_CHAR (iva_figurativa), 9, '0')
                || DECODE (
                      titolo_abbonamento,
                      'T',    RPAD (codice_fiscale_abbonamento, 16, ' ')
                           || RPAD (codice_biglietto_abbonamento, 8, ' ')
                           || LPAD (TO_CHAR (num_prog_biglietto_abbonamento),
                                    8,
                                    '0'),
                         RPAD (codice_fiscale_organizzatore, 16, ' ')
                      || RPAD (codice_abbonamento, 8, ' ')
                      || RPAD (TO_CHAR (num_prog_abbonamento), 8, '0')))
          || RPAD (NVL (codice_prestazione1, ' '), 3, ' ')
          || LPAD (TO_CHAR (importo_prestazione1), 9, '0')
          || RPAD (NVL (codice_prestazione2, ' '), 3, ' ')
          || LPAD (TO_CHAR (importo_prestazione2), 9, '0')
          || RPAD (NVL (codice_prestazione3, ' '), 3, ' ')
          || LPAD (TO_CHAR (importo_prestazione3), 9, '0')
          || RPAD (NVL (carta_originale_annullato, ' '), 8, ' ')
          || RPAD (NVL (causale_annullamento, ' '), 3, ' ')
             AS log_data,
          idvend AS idvend
     FROM wtic.log_transazioni
     UNION ALL
     SELECT FILE_DATE, LOG_DATA, IDVEND FROM LOGTRANS_KEEP
     UNION ALL
     SELECT FILE_DATE, LOG_DATA, IDVEND FROM LOGTRANS_ARCHIVE_KEEP
