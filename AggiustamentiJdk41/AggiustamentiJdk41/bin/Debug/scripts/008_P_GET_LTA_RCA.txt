﻿CREATE OR REPLACE PROCEDURE WTIC."P_GET_LTA_RCA" (dGiornoMese IN DATE, nProgressivo IN NUMBER, ref_curLTA OUT SYS_REFCURSOR, ref_curRCA OUT SYS_REFCURSOR, nMAX_LOG_ID_OUT OUT NUMBER)
IS
    dDataRif DATE;
    v_tab_Lta WTIC.OBJ_TABLE_LTA;
BEGIN
    dDataRif := TRUNC(dGiornoMese);

    nMAX_LOG_ID_OUT := 0;

    v_tab_Lta := WTIC.F_GET_LTA(dDataRif);

    OPEN ref_curLTA FOR SELECT * FROM TABLE(v_tab_Lta) ORDER BY LTA_ID;

    SELECT NVL(MAX(ID_INSUPD_LTA), 0) INTO nMAX_LOG_ID_OUT FROM TABLE(v_tab_Lta);

    OPEN ref_curRCA FOR SELECT RCA.DATA_RIEPILOGO,
                               TRIM(TO_CHAR(SYSDATE, 'YYYYMMDD')) AS DATA_GENERAZIONE_RIEPILOGO,
                               TRIM(TO_CHAR(SYSDATE, 'HH24MISS')) AS ORA_GENERAZIONE_RIEPILOGO,
                               nProgressivo AS PROGRESSIVO_RIEPILOGO,
                               RCA.TITOLARE, 
                               RCA.CF_TITOLARE_CA, 
                               RCA.CODICE_SISTEMA_CA,
                               RCA.CODICE_FISCALE_ORGANIZZATORE, 
                               RCA.DENOMINAZIONE_ORGANIZZATORE, 
                               RCA.TIPOLOGIA_ORGANIZZATORE, 
                               RCA.SPETTACOLO_INTRATTENIMENTO, 
                               RCA.INCIDENZA_INTRATTENIMENTO, 
                               RCA.DENOMINAZIONE_LOCALE, 
                               RCA.CODICE_LOCALE, 
                               RCA.DATA_EVENTO, 
                               RCA.ORA_EVENTO, 
                               RCA.DATA_ORA_EVENTO, 
                               RCA.TIPO_EVENTO, 
                               RCA.TITOLO_EVENTO,
                               NVL(TITOLI.AUTORE, '') AS AUTORE, 
                               NVL(TITOLI.ESECUTORE, '') AS ESECUTORE, 
                               NVL(TITOLI.NAZIONALITA, '') AS NAZIONALITA, 
                               NVL(TITOLI.NUMERO_OPERE_RAPPRESENTATE, 1) AS NUMERO_OPERE_RAPPRESENTATE,
                               RCA.CODICE_SISTEMA_EMISSIONE, 
                               RCA.TF, 
                               RCA.TIPO_TITOLO, 
                               RCA.ORDINE_DI_POSTO,
                               RCA.CAPIENZA_SETTORE,
                               RCA.TOT,
                               RCA.TOT_VT,
                               RCA.TOT_VD,
                               RCA.TOT_ZT,
                               RCA.TOT_ZD,
                               RCA.TOT_MT,
                               RCA.TOT_MD,
                               RCA.TOT_DT,
                               RCA.TOT_DD,
                               RCA.TOT_FT,
                               RCA.TOT_FD,
                               RCA.TOT_AT,
                               RCA.TOT_AD,
                               RCA.TOT_BT,
                               RCA.TOT_BD
                               FROM
                               (
                               SELECT 
                               LISTA_RCA.DATA_RIEPILOGO,
                               LISTA_RCA.TITOLARE, 
                               LISTA_RCA.CF_TITOLARE_CA, 
                               LISTA_RCA.CODICE_SISTEMA_CA,
                               LISTA_RCA.CODICE_FISCALE_ORGANIZZATORE, 
                               LISTA_RCA.DENOMINAZIONE_ORGANIZZATORE, 
                               LISTA_RCA.TIPOLOGIA_ORGANIZZATORE, 
                               LISTA_RCA.SPETTACOLO_INTRATTENIMENTO, 
                               LISTA_RCA.INCIDENZA_INTRATTENIMENTO, 
                               LISTA_RCA.DENOMINAZIONE_LOCALE, 
                               LISTA_RCA.CODICE_LOCALE, 
                               LISTA_RCA.DATA_EVENTO, 
                               LISTA_RCA.ORA_EVENTO, 
                               LISTA_RCA.DATA_ORA_EVENTO, 
                               LISTA_RCA.TIPO_EVENTO, 
                               LISTA_RCA.TITOLO_EVENTO,
                               LISTA_RCA.CODICE_SISTEMA_EMISSIONE, 
                               LISTA_RCA.TF, 
                               LISTA_RCA.TIPO_TITOLO, 
                               LISTA_RCA.ORDINE_DI_POSTO,
                               LISTA_RCA.CAPIENZA_SETTORE,
                               SUM(1) AS TOT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'VT', 1, 0)) AS TOT_VT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'VD', 1, 0)) AS TOT_VD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'ZT', 1, 0)) AS TOT_ZT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'ZD', 1, 0)) AS TOT_ZD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'MT', 1, 0)) AS TOT_MT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'MD', 1, 0)) AS TOT_MD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'DT', 1, 0)) AS TOT_DT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'DD', 1, 0)) AS TOT_DD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'FT', 1, 0)) AS TOT_FT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'FD', 1, 0)) AS TOT_FD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'AT', 1, 0)) AS TOT_AT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'AD', 1, 0)) AS TOT_AD,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'BT', 1, 0)) AS TOT_BT,
                               SUM(DECODE(LISTA_RCA.STATO_TITOLO, 'BD', 1, 0)) AS TOT_BD
                               FROM
                               (
                               SELECT
                               LTA_RCA.DATA_RIEPILOGO,
                               LTA_RCA.TITOLARE, 
                               LTA_RCA.CF_TITOLARE_CA, 
                               LTA_RCA.CODICE_SISTEMA_CA,
                               LTA_RCA.CODICE_FISCALE_ORGANIZZATORE, 
                               LTA_RCA.DENOMINAZIONE_ORGANIZZATORE, 
                               LTA_RCA.TIPOLOGIA_ORGANIZZATORE,
                               LTA_RCA.SPETTACOLO_INTRATTENIMENTO, 
                               DECODE(LTA_RCA.SPETTACOLO_INTRATTENIMENTO, 'S', 0, NVL(VR_EVENTI_RCA.INCIDENZA_INTRATTENIMENTO, 100)) AS INCIDENZA_INTRATTENIMENTO,
                               LTA_RCA.DATA_EVENTO, 
                               LTA_RCA.ORA_EVENTO,
                               LTA_RCA.DATA_ORA_EVENTO, 
                               LTA_RCA.TIPO_EVENTO,
                               LTA_RCA.CODICE_LOCALE,
                               (SELECT MIN(VR_SALE.DESCR || ' ' || VR_SALE.ALIAS) FROM VR_SALE WHERE VR_SALE.CODICE_LOCALE = LTA_RCA.CODICE_LOCALE) AS DENOMINAZIONE_LOCALE, 
                               LTA_RCA.TITOLO_EVENTO,
                               LTA_RCA.CODICE_SISTEMA_EMISSIONE,
                               LTA_RCA.TF, 
                               LTA_RCA.TIPO_TITOLO, 
                               LTA_RCA.ORDINE_DI_POSTO, 
                               LTA_RCA.STATO_TITOLO,
                               DECODE(NVL(LTA_RCA.CAPIENZA_SETTORE, 0), 0, F_GET_CAPIENZA_SETTORE(LTA_RCA.CODICE_LOCALE, LTA_RCA.ORDINE_DI_POSTO, LTA_RCA.IDCALEND, LTA_RCA.DATA_ORA_EVENTO, LTA_RCA.TITOLO_EVENTO, LTA_RCA.IDSALA), LTA_RCA.CAPIENZA_SETTORE) AS CAPIENZA_SETTORE
                               FROM
                               (
                               SELECT 
                               VR_LTA.DATA_EVENTO AS DATA_RIEPILOGO,
                               VR_CODICE_SISTEMA.TITOLARE, 
                               VR_LTA.CF_TITOLARE_CA, 
                               VR_LTA.CODICE_SISTEMA_CA,
                               VR_LTA.CODICE_FISCALE_ORGANIZZATORE, 
                               NVL(VR_ORGANIZZATORI.RAG_SOC, '') AS DENOMINAZIONE_ORGANIZZATORE, 
                               NVL(VR_ORGANIZZATORI.TIPO_ORGANIZZATORE, 'G') AS TIPOLOGIA_ORGANIZZATORE,
                               LISTA_TIPOEVENTO.SPETTACOLO_INTRATTENIMENTO, 
                               VR_LTA.DATA_EVENTO, 
                               VR_LTA.ORA_EVENTO, 
                               VR_LTA.DATA_ORA_EVENTO,
                               VR_LTA.TIPO_EVENTO,
                               VR_LTA.CODICE_LOCALE, 
                               VR_LTA.TITOLO_EVENTO,
                               VR_LTA.CODICE_SISTEMA_EMISSIONE,
                               VR_LTA.TIPO_TITOLO, 
                               VR_LTA.ORDINE_DI_POSTO, 
                               VR_LTA.STATO_TITOLO,
                               VR_LTA.CAPIENZA_SETTORE,
                               VR_LTA.TF,
                               VR_LTA.IDCALEND,
                               VR_LTA.IDSALA,
                               VR_LTA.IDBIGLIETTO
                               FROM 
                               (SELECT * FROM TABLE(v_tab_Lta)) VR_LTA, VR_CODICE_SISTEMA, VR_ORGANIZZATORI,
                               (SELECT VR_TIPOEVENTO.CODICE, MIN(NVL(TE_SPET_INTRA.SPETTACOLO_INTRATTENIMENTO, 'S')) AS SPETTACOLO_INTRATTENIMENTO 
                                FROM VR_TIPOEVENTO, (SELECT TIPO_EVENTO, MIN(SPETTACOLO_INTRATTENIMENTO) AS SPETTACOLO_INTRATTENIMENTO FROM IMPOSTE GROUP BY TIPO_EVENTO) TE_SPET_INTRA
                                WHERE VR_TIPOEVENTO.CODICE = TE_SPET_INTRA.TIPO_EVENTO(+) GROUP BY VR_TIPOEVENTO.CODICE) LISTA_TIPOEVENTO
                               WHERE
                               VR_LTA.CODICE_SISTEMA_CA = VR_CODICE_SISTEMA.CODICE_SISTEMA
                               AND
                               VR_LTA.DATA_ORA_EVENTO >= TRUNC(dDataRif) AND VR_LTA.DATA_ORA_EVENTO < (TRUNC(dDataRif) + 1)
                               AND
                               VR_LTA.CODICE_FISCALE_ORGANIZZATORE = VR_ORGANIZZATORI.ORGANIZZATORE(+)
                               AND
                               VR_LTA.TIPO_EVENTO = LISTA_TIPOEVENTO.CODICE
                               ) LTA_RCA, VR_EVENTI_RCA
                               WHERE LTA_RCA.CODICE_FISCALE_ORGANIZZATORE = VR_EVENTI_RCA.CF_ORGANIZZATORE(+)
                               AND LTA_RCA.CODICE_LOCALE = VR_EVENTI_RCA.CODICE_LOCALE(+)
                               AND LTA_RCA.DATA_ORA_EVENTO = VR_EVENTI_RCA.DATA_ORA_EVENTO(+) 
                               AND LTA_RCA.TIPO_EVENTO = VR_EVENTI_RCA.TIPO_EVENTO(+)
                               AND LTA_RCA.SPETTACOLO_INTRATTENIMENTO = VR_EVENTI_RCA.SPETTACOLO_INTRATTENIMENTO(+)
                               AND LTA_RCA.TITOLO_EVENTO = VR_EVENTI_RCA.TITOLO_EVENTO(+)
                               ) LISTA_RCA
                               GROUP BY
                               LISTA_RCA.TITOLARE,
                               LISTA_RCA.CF_TITOLARE_CA,
                               LISTA_RCA.CODICE_SISTEMA_CA,
                               LISTA_RCA.CODICE_FISCALE_ORGANIZZATORE,
                               LISTA_RCA.DENOMINAZIONE_ORGANIZZATORE,
                               LISTA_RCA.TIPOLOGIA_ORGANIZZATORE,
                               LISTA_RCA.SPETTACOLO_INTRATTENIMENTO,
                               LISTA_RCA.INCIDENZA_INTRATTENIMENTO,
                               LISTA_RCA.DENOMINAZIONE_LOCALE,
                               LISTA_RCA.CODICE_LOCALE,
                               LISTA_RCA.DATA_EVENTO,
                               LISTA_RCA.ORA_EVENTO,
                               LISTA_RCA.DATA_ORA_EVENTO, 
                               LISTA_RCA.TIPO_EVENTO,
                               LISTA_RCA.TITOLO_EVENTO,
                               LISTA_RCA.CODICE_SISTEMA_EMISSIONE,
                               LISTA_RCA.TF,
                               LISTA_RCA.ORDINE_DI_POSTO,
                               LISTA_RCA.TIPO_TITOLO,
                               LISTA_RCA.CAPIENZA_SETTORE) RCA,
                               (SELECT 
                                TITOLO_EVENTO, 
                                PRODUTTORE_CINEMA, 
                                AUTORE, 
                                ESECUTORE, 
                                NAZIONALITA, 
                                DISTRIBUTORE, 
                                NUMERO_OPERE_RAPPRESENTATE
                                FROM 
                                (SELECT
                                 VR_PACCHETTO.DESCRIZIONE AS TITOLO_EVENTO, 
                                 VR_FILM.TITOLO, 
                                 VR_PRODUTTORI.RAG_SOC AS PRODUTTORE_CINEMA,
                                 VR_FILM.REGISTA AS AUTORE, 
                                 '' AS ESECUTORE, 
                                 VR_FILM.IDNAZIONE AS NAZIONALITA, 
                                 VR_NOLEGGIO.RAG_SOC AS DISTRIBUTORE,
                                 COUNT(DISTINCT VR_PACCHETTO.IDFILM) OVER (PARTITION BY VR_PACCHETTO.IDPACCHETTO) AS NUMERO_OPERE_RAPPRESENTATE,
                                 VR_PACCHETTO.PROGR
                                 FROM WTIC.VR_PACCHETTO,WTIC.VR_FILM,WTIC.VR_PRODUTTORI,WTIC.VR_NOLEGGIO 
                                 WHERE VR_PACCHETTO.IDFILM = VR_FILM.IDFILM AND VR_FILM.IDNOLO = VR_NOLEGGIO.IDNOLO AND VR_FILM.IDPROD = VR_PRODUTTORI.IDPROD) WHERE PROGR = 1 ORDER BY TITOLO_EVENTO) TITOLI
                               WHERE
                               RCA.TITOLO_EVENTO = TITOLI.TITOLO_EVENTO(+)
                               ORDER BY
                               RCA.TITOLARE,
                               RCA.CF_TITOLARE_CA,
                               RCA.CODICE_SISTEMA_CA,
                               RCA.CODICE_FISCALE_ORGANIZZATORE,
                               RCA.DENOMINAZIONE_ORGANIZZATORE,
                               RCA.TIPOLOGIA_ORGANIZZATORE,
                               RCA.DATA_EVENTO,
                               RCA.ORA_EVENTO,
                               RCA.SPETTACOLO_INTRATTENIMENTO,
                               RCA.INCIDENZA_INTRATTENIMENTO,
                               RCA.DENOMINAZIONE_LOCALE,
                               RCA.CODICE_LOCALE,
                               RCA.DATA_EVENTO,
                               RCA.ORA_EVENTO,
                               RCA.TIPO_EVENTO,
                               RCA.TITOLO_EVENTO,
                               RCA.CODICE_SISTEMA_EMISSIONE,
                               RCA.ORDINE_DI_POSTO,
                               RCA.TF,
                               RCA.TIPO_TITOLO;

END;
