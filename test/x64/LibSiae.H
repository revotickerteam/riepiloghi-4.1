typedef unsigned char	BYTE;
typedef unsigned long	DWORD;
typedef unsigned short	WORD;

BYTE	GetKeyIDML (int nSlot);
int	ComputeSigillo (BYTE * Data_Ora, DWORD Prezzo, BYTE * SN, BYTE * mac, DWORD * cnt);
int	ComputeSigilloEx (BYTE * Data_Ora, DWORD Prezzo, BYTE * mac, DWORD * cnt);
int	ComputeSigilloExML (BYTE * Data_Ora, DWORD Prezzo, BYTE * mac, DWORD * cnt, int nSlot);
int	ComputeSigilloFast (BYTE * Data_Ora, DWORD Prezzo, BYTE * SN, BYTE * mac, DWORD * cnt);
int	ComputeSigilloFastML (BYTE * Data_Ora, DWORD Prezzo, BYTE * SN, BYTE * mac, DWORD * cnt, int nSlot);
int	Finalize (void);
int	FinalizeML (int nSlot);
int	GetCACertificateML (BYTE * cert, int * dim, int nSlot);
int	GetCertificateML (BYTE * cert, int * dim, int nSlot);
int	GetCardsNumber (void);
int	GetReadersNumber (void);
int	GetSIAECertificateML (BYTE * cert, int * dim, int nSlot);
int	GetSN (BYTE serial [8]);
int	GetSNML (BYTE serial [8], int nSlot);
int	Hash (int mec, BYTE * toHash, int Len, BYTE * Hashed);
int	ImpostaTimeout (int ValoreTimeout);
int	Initialize (int nSlot);
int	isCardIn (int n);
int	MessageRequest (HANDLE hWnd, UINT Msg);
int	Padding (BYTE * toPad, int Len, BYTE * Padded);
int	ReadBalance (DWORD * value);
int	ReadBalanceML (DWORD * value, int nSlot);
int	ReadBinary (WORD Offset, BYTE * Buffer, int * Len);
int	ReadBinaryML (WORD Offset, BYTE * Buffer, int * Len, int nSlot);
int	ReadCounter (DWORD * value);
int	ReadCounterML (DWORD * value, int nSlot);
int	ReadRecord (int nRec, BYTE * Buffer, int * Len);
int	ReadRecordML (int nRec, BYTE * Buffer, int * Len, int nSlot);
int	Select (WORD fid);
int	SelectML (WORD fid, int nSlot);
int	SignML (int kx, BYTE * toSign, BYTE * Signed, int nSlot);
int	StatoTimer (void);
int	TimerStart (int nSlot);
int	TimerStop (int nSlot);
int	VerifyPIN (int nPIN, char * pin);
int	VerifyPINML (int nPIN, char * pin, int nSlot);
int	LibSiaeVersion (BYTE nVersione [4]);

/* Definizione dei messaggi di errore */
#define C_OK                          0x0000
#define C_CONTEXT_ERROR               0x0001
#define C_NOT_INITIALIZED             0x0002
#define C_ALREADY_INITIALIZED         0x0003
#define C_NO_CARD                     0x0004
#define C_UNKNOWN_CARD                0x0005
#define C_WRONG_LENGTH                0x6282
#define C_WRONG_TYPE                  0x6981
#define C_NOT_AUTHORIZED              0x6982
#define C_PIN_BLOCKED                 0x6983
#define C_WRONG_DATA                  0x6A80
#define C_FILE_NOT_FOUND              0x6A82
#define C_RECORD_NOT_FOUND            0x6A83
#define C_WRONG_LEN                   0x6A85
#define C_UNKNOWN_OBJECT              0x6A88
#define C_ALREADY_EXISTS              0x6A89
#define C_GENERIC_ERROR               0xFFFF

#define HASH_SHA1                     0x01
#define HASH_MD5                      0x02