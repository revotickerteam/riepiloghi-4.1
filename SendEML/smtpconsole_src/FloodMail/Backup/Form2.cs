using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using vmlinux.Net;
using System.IO;

namespace FloodMail
{
	/// <summary>
	/// Summary description for Form2.
	/// </summary>
	public class Form2 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbConsole;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		SocketHelper sh=null;

		private SocketHelper Helper
		{
			get
			{
				if(sh==null)
					sh=new SocketHelper("smtp.163.com",25);

				return sh;
			}
		}
		public Form2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			tbConsole.Text=this.Helper.GetFullResponse();
			tbConsole.SelectionStart=tbConsole.TextLength;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			this.Helper.SendCommand("QUIT");
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbConsole = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// tbConsole
			// 
			this.tbConsole.BackColor = System.Drawing.SystemColors.WindowText;
			this.tbConsole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbConsole.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbConsole.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.tbConsole.Location = new System.Drawing.Point(0, 0);
			this.tbConsole.Multiline = true;
			this.tbConsole.Name = "tbConsole";
			this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbConsole.Size = new System.Drawing.Size(520, 353);
			this.tbConsole.TabIndex = 0;
			this.tbConsole.Text = "";
			this.tbConsole.WordWrap = false;
			this.tbConsole.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbConsole_KeyDown);
			// 
			// Form2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 353);
			this.Controls.Add(this.tbConsole);
			this.Name = "Form2";
			this.Text = "Socket Console";
			this.ResumeLayout(false);

		}
		#endregion

		private void tbConsole_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(tbConsole.SelectionStart<tbConsole.TextLength-tbConsole.Lines[tbConsole.Lines.Length-1].Length)
				tbConsole.SelectionStart=tbConsole.TextLength;

			if(e.KeyValue==13)	//enter
			{

				int pos=tbConsole.Text.LastIndexOf("\n");
				string cmd="";
				string res="\r\n";
				bool getresp=true;
				bool datamode=false;
				byte[] bts=null;
				if(pos>-1)
					cmd=tbConsole.Text.Substring(pos+1,tbConsole.TextLength-pos-1);
				else
					cmd=tbConsole.Text;

				if(cmd==string.Empty)
					return;

				if(cmd[0]=='$')
				{
					cmd=Convert.ToBase64String(
						System.Text.Encoding.ASCII.GetBytes(cmd.Substring(1)));
					tbConsole.Text+="["+cmd+"]";
				}
				else if(cmd[0]=='.' && cmd.Length>1)
				{
					cmd=cmd.Substring(1);
					getresp=false;
				}
				else if(cmd[0]=='@')
				{
					cmd=cmd.Substring(1);
					if(System.IO.File.Exists(cmd))
					{
						FileStream file=System.IO.File.OpenRead(cmd);
						bts=new byte[file.Length];
						file.Read(bts,0,bts.Length);
						file.Close();
						datamode=true;
						getresp=false;
					}
				}
				if(datamode)
					this.Helper.SendData(bts);
				else
					this.Helper.SendCommand(cmd);
				if(getresp)
					res+=this.Helper.GetFullResponse();

				tbConsole.Text+=res;
				tbConsole.SelectionStart=tbConsole.TextLength;
				e.Handled=true;
			}
			else if(e.KeyValue==38 || e.KeyValue==40)	//move up or down
				e.Handled=true;
			else if(e.KeyValue==39)	//move right
			{
				if(tbConsole.SelectionStart<tbConsole.TextLength)
				{
					char c=tbConsole.Text[tbConsole.SelectionStart];
					if(c=='\r' || c=='\n')
						e.Handled=true;
				}
			}
			else if(e.KeyValue==37 || e.KeyValue==8)	//move left
			{
				if(tbConsole.SelectionStart>0)
				{
					char c=tbConsole.Text[tbConsole.SelectionStart-1];
					if(c=='\r' || c=='\n')
						e.Handled=true;
				}
			}
		}
	}
}
