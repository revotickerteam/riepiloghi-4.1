using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Web.Mail;

namespace FloodMail
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbFrom;
		private System.Windows.Forms.TextBox tbTo;
		private System.Windows.Forms.TextBox tbServer;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbUser;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbPass;
		private System.Windows.Forms.TextBox tbSubject;
		private System.Windows.Forms.TextBox tbBody;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbFrom = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbTo = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbServer = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbUser = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbPass = new System.Windows.Forms.TextBox();
			this.tbSubject = new System.Windows.Forms.TextBox();
			this.tbBody = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbFrom
			// 
			this.tbFrom.Location = new System.Drawing.Point(176, 32);
			this.tbFrom.Name = "tbFrom";
			this.tbFrom.TabIndex = 0;
			this.tbFrom.Text = "xxx";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(48, 32);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "From";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(48, 80);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "To";
			// 
			// tbTo
			// 
			this.tbTo.Location = new System.Drawing.Point(176, 80);
			this.tbTo.Name = "tbTo";
			this.tbTo.TabIndex = 3;
			this.tbTo.Text = "vmlinuxx@hotmail.com";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(48, 128);
			this.label3.Name = "label3";
			this.label3.TabIndex = 4;
			this.label3.Text = "Server";
			// 
			// tbServer
			// 
			this.tbServer.Location = new System.Drawing.Point(176, 128);
			this.tbServer.Name = "tbServer";
			this.tbServer.TabIndex = 5;
			this.tbServer.Text = "smtp.163.net";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(48, 176);
			this.label4.Name = "label4";
			this.label4.TabIndex = 6;
			this.label4.Text = "User";
			// 
			// tbUser
			// 
			this.tbUser.Location = new System.Drawing.Point(176, 176);
			this.tbUser.Name = "tbUser";
			this.tbUser.TabIndex = 7;
			this.tbUser.Text = "nbhero";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(48, 224);
			this.label5.Name = "label5";
			this.label5.TabIndex = 8;
			this.label5.Text = "Pass";
			// 
			// tbPass
			// 
			this.tbPass.Location = new System.Drawing.Point(176, 224);
			this.tbPass.Name = "tbPass";
			this.tbPass.TabIndex = 9;
			this.tbPass.Text = "useheart";
			// 
			// tbSubject
			// 
			this.tbSubject.Location = new System.Drawing.Point(128, 264);
			this.tbSubject.Name = "tbSubject";
			this.tbSubject.Size = new System.Drawing.Size(168, 21);
			this.tbSubject.TabIndex = 10;
			this.tbSubject.Text = "test";
			// 
			// tbBody
			// 
			this.tbBody.Location = new System.Drawing.Point(80, 312);
			this.tbBody.Multiline = true;
			this.tbBody.Name = "tbBody";
			this.tbBody.Size = new System.Drawing.Size(216, 80);
			this.tbBody.TabIndex = 11;
			this.tbBody.Text = "testtesttest";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(224, 400);
			this.button1.Name = "button1";
			this.button1.TabIndex = 12;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(360, 429);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.tbBody);
			this.Controls.Add(this.tbSubject);
			this.Controls.Add(this.tbPass);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbUser);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.tbServer);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.tbTo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbFrom);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form2());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			MailMessage mail = new MailMessage();
			mail.To = tbTo.Text;
			mail.From = tbFrom.Text;
			mail.Subject = tbSubject.Text;
			mail.Body = tbBody.Text;
			mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
			mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", tbUser.Text); //set your username here
			mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", tbPass.Text);	//set your password here

			SmtpMail.SmtpServer = tbServer.Text;  //your real server goes here
			SmtpMail.Send( mail );

		}

	}
}
