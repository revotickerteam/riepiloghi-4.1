using System;
using System.Net.Sockets;
using System.IO;

namespace vmlinux.Net
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class SocksHelper
	{
		private TcpClient client=null;
		private StreamReader reader=null;
		private StreamWriter writer=null;
		private string resp="";

		public SocksHelper(string name,int port)
		{
			//
			// TODO: Add constructor logic here
			//
			client=new TcpClient(name,port);
			reader=new StreamReader(client.GetStream());
			writer=new StreamWriter(client.GetStream());
		}

		public SocksHelper(TcpClient tc)
		{
			client=tc;
			reader=new StreamReader(client.GetStream());
			writer=new StreamWriter(client.GetStream());
		}

		public void SendCommand(string cmd)
		{
			if(writer.BaseStream.CanWrite)
			{
				writer.WriteLine(cmd);
				writer.Flush();
			}
		}

		public string RecvResponse()
		{
			if(reader.BaseStream.CanRead)
			{
				resp=reader.ReadLine();
				return resp;
			}
			else
				return "";
		}

		public int GetResponseState()
		{
			if(resp.Length>=3)
				return Convert.ToInt32(resp.Substring(0,3));
			else
				return -1;
		}

		public bool HaveNextResponse()
		{
			if(resp!=string.Empty)
			{
				if(resp.Length>=4 && resp[3]=='-')
					return true;
				else
					return false;
			}
			else
				return false;
		}
	}
}
