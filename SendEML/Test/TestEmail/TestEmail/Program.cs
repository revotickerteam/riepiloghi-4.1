﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TestEmail
{
    class Program
    {

        private static string sEmlPath = @"C:\Users\Administrator\Documents\Visual Studio 2017\Projects\Riepiloghi\SendEML\Test\RPG_2014_06_27_001.xsi.eml";
        static void Main(string[] args)
        {
            //AsynchronousClient.MainMyClient(null);
            //SendFileEml("smtp.creaweb.it", 2500, "smtp.creaweb.it", "aogrozucae", "andrea@creaweb.it", "andrea.alziati@gmail.com", false, sEmlPath);
            SendFileEml("mobility.creaweb.it", 2500, "importante@creaweb.it", "nonsidaingiro", "andrea@creaweb.it", "andrea.alziati@gmail.com", false, sEmlPath);
            //Console.ReadKey();
        }

        static bool SendFileEml(string smtpServer, int smtpPort, string user, string password, string from, string to, bool useSsl, string file)
        {
            bool result = false;
            SmtpScoektClient socket = new SmtpScoektClient(smtpServer, smtpPort, useSsl);
            socket.eventHandlerResponse = new EventHandler<string>(Capture);
            result = socket.SendFileEml(user, password, from, to, file);
            return result;
        }

        static void Capture(object sender, string e)
        {
            Console.WriteLine(e);
        }
    }
}
