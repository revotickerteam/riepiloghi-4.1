﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Net;

namespace TestEmail
{

    public class clsSockectSmtpClient : Socket
    {
        public clsSockectSmtpClient(AddressFamily f, SocketType t, ProtocolType p)
            :base(f, t, p)
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP  socket.  
            Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            sender.Connect("mobility.creaweb.it", 2500);
            if (sender.Connected)
            {

                sender.Disconnect(false);
            }
            sender.Close();
            sender.Dispose();
        }


        public void trest()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);
        }


    }

    

    public class SmtpScoektClient
    {
        private TcpClient client = null;
        private NetworkStream stream = null;
        private SslStream sslStream = null;
        private StreamReader reader = null;
        private StreamWriter writer = null;
        private string resp = "";
        private int state = -1;

        private string server = "";
        private int port = 0;
        private bool useSsl = false;

        public Exception error = null;

        public EventHandler<string> eventHandlerResponse;

        public SmtpScoektClient(string smtpServer, int smtpPort, bool useSSL)
        {
            this.server = smtpServer;
            this.port = smtpPort;
            client = new TcpClient();
            this.useSsl = useSSL;
        }

        public bool Connect()
        {
            bool Result = false;
            try
            {
                client.Connect(server, port);
                if (client.Connected)
                {
                    stream = client.GetStream();
                    if (useSsl)
                    {
                        SslStream sslStream = new SslStream(stream);
                        sslStream.AuthenticateAsClient(server);
                        reader = new StreamReader(sslStream);
                        writer = new StreamWriter(sslStream);
                        //writer.AutoFlush = true;
                    }
                    else
                    {
                        reader = new StreamReader(stream);
                        writer = new StreamWriter(stream);
                        //writer.AutoFlush = true;
                    }
                    Result = true;
                }
            }
            catch (Exception ex)
            {
                this.error = ex;
                Result = false;
            }

            return Result;
        }


        public void SendData(byte[] bts)
        {
            if (GetResponseState() != 221)
            {
                stream.Write(bts, 0, bts.Length);
                stream.Flush();
            }
        }

        public void SendCommand(string cmd)
        {
            SendCommand(cmd, false);
        }

        public void SendCommand(string cmd, bool base64)
        {
            if (GetResponseState() != 221)
            {
                if (!base64)
                    writer.WriteLine(cmd);
                else
                    writer.WriteLine(Convert.ToBase64String(Encoding.UTF8.GetBytes(cmd)));
                writer.Flush();
            }
        }

        public string RecvResponse()
        {
            if (GetResponseState() != 221)
                resp = reader.ReadLine();
            else
                resp = "221 closed!";

            return resp;
        }

        public int GetResponseState()
        {
            if (resp.Length >= 3 && IsNumber(resp[0]) && IsNumber(resp[1]) && IsNumber(resp[2]))
                state = Convert.ToInt32(resp.Substring(0, 3));

            return state;
        }

        private bool IsNumber(char c)
        {
            return c >= '0' && c <= '9';
        }

        public string GetFullResponse()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(RecvResponse());
            sb.Append("\r\n");
            while (HaveNextResponse())
            {
                sb.Append(RecvResponse());
                sb.Append("\r\n");
            }
            return sb.ToString();
        }

        public bool HaveNextResponse()
        {
            if (GetResponseState() > -1)
            {
                if (resp.Length >= 4 && resp[3] != ' ')
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void RaiseResponse(string value)
        {
            if (this.eventHandlerResponse != null)
            {
                this.eventHandlerResponse(this, value);
            }
        }

        private void Wait()
        {
            System.Threading.Thread.Sleep(1);
        }

        public bool SendFileEml(string user, string password, string from, string to, string file)
        {
            bool result = false;
            bool lAuth = !string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password);
            string response = "";
            if (this.Connect())
            {
                if (lAuth)
                {

                    if (useSsl)
                    {
                        this.SendCommand(string.Format("EHLO {0}", server));
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand("auth login");
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand(user, true);
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand(password, true);
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();
                    }
                    else
                    {
                        this.SendCommand(string.Format("HELO {0}", server));
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand("auth login");
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand(user, true);
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();

                        this.SendCommand(password, true);
                        response = this.GetFullResponse();
                        this.RaiseResponse(response);
                        this.Wait();
                    }
                }
                else
                {
                    this.SendCommand(string.Format("HELO {0}", server));
                    response = this.GetFullResponse();
                    this.RaiseResponse(response);
                    this.Wait();


                }

                this.SendCommand(string.Format("MAIL FROM: <{0}>", from));
                response = this.GetFullResponse();
                this.RaiseResponse(response);
                this.Wait();



                this.SendCommand(string.Format("RCPT TO: <{0}>", to));
                response = this.GetFullResponse();
                this.RaiseResponse(response);
                this.Wait();



                this.SendCommand("DATA");
                response = this.GetFullResponse();
                this.RaiseResponse(response);
                this.Wait();



                string sCharSet = "utf-8";
                byte[] abMailOringal = System.Text.Encoding.GetEncoding(sCharSet).GetBytes(File.ReadAllText(file, Encoding.GetEncoding(sCharSet)));
                byte[] abMailUTF8 = System.Text.Encoding.Convert(System.Text.Encoding.GetEncoding(sCharSet), Encoding.UTF8, abMailOringal);
                string value = System.Text.Encoding.UTF8.GetString(abMailUTF8).Replace("?" + sCharSet + "?", "?UTF-8?").Replace("=" + sCharSet, "=UTF-8") + ".";
                this.SendCommand(value);
                response = this.GetFullResponse();
                this.RaiseResponse(response);
                this.Wait();

                //this.SendCommand("");
                this.SendCommand("\n");
                this.SendCommand("\n");
                this.SendCommand("\n");
                this.SendCommand("\n");
                this.SendCommand(".");
                this.SendCommand("QUIT");
                result = true;
            }
            return result;
        }
    }
}
