﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintic.Data;
using Wintic.Data.oracle;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace libBordero
{
    public class clsPdfDocument
    {
        public iTextSharp.text.Document DocPdf = null;
        public iTextSharp.text.pdf.PdfWriter Writer = null;
        private Dictionary<FontStyle, iTextSharp.text.Font> oFonts = new Dictionary<FontStyle, iTextSharp.text.Font>();
        private string FontPath = "";
        private System.IO.MemoryStream oStream;

        public clsPdfDocument()
        {
            this.DocPdf = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
            this.oStream = new MemoryStream();
            //this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, new FileStream(FilePdf, FileMode.Create));
            this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, this.oStream);
            this.InitLocalFonts();
        }

        public clsPdfDocument(string FontPath)
        {
            this.DocPdf = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
            this.oStream = new MemoryStream();
            //this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, new FileStream(FilePdf, FileMode.Create));
            this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, this.oStream);
            this.FontPath = FontPath;
            this.InitLocalFonts();
        }

        private void InitLocalFonts()
        {
            this.oFonts = new Dictionary<FontStyle, iTextSharp.text.Font>();
            try
            {
                string cApplicationPath = (this.FontPath.Trim() != "" ? this.FontPath : (new System.IO.FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location)).DirectoryName);

                iTextSharp.text.FontFactory.Register(cApplicationPath + "\\SWZ721C.TTF", "SWISS");
                iTextSharp.text.FontFactory.Register(cApplicationPath + "\\SWZ721BC.TTF", "SWISS_B");
                iTextSharp.text.FontFactory.Register(cApplicationPath + "\\SWZ721CI.TTF", "SWISS_I");
                iTextSharp.text.FontFactory.Register(cApplicationPath + "\\SW721BCI.TTF", "SWISS_BI");
                this.oFonts.Add(FontStyle.Regular, iTextSharp.text.FontFactory.GetFont("SWISS"));
                this.oFonts.Add(FontStyle.Bold, iTextSharp.text.FontFactory.GetFont("SWISS_B"));
                this.oFonts.Add(FontStyle.Italic, iTextSharp.text.FontFactory.GetFont("SWISS_I"));
                this.oFonts.Add(FontStyle.Bold | FontStyle.Italic, iTextSharp.text.FontFactory.GetFont("SWISS_BI"));
            }
            catch
            {
            }
        }

        public iTextSharp.text.Font GetFont(FontStyle Style, int Size)
        {
            iTextSharp.text.Font oRet = null;
            try
            {
                if (this.oFonts.ContainsKey(Style))
                {
                    iTextSharp.text.Font oFontStyled = (iTextSharp.text.Font)this.oFonts[Style];
                    oRet = iTextSharp.text.FontFactory.GetFont(oFontStyled.Familyname, Size, (int)Style);
                }
            }
            catch
            {
                oRet = iTextSharp.text.FontFactory.GetFont("Helvetica", Size, (int)Style);
            }

            return oRet;
        }

        public static iTextSharp.text.Image GetBarcode(iTextSharp.text.pdf.PdfWriter parWriter, string parText, string parType)
        {
            iTextSharp.text.Image result = null;
            bool parShowText = true;

            parWriter.DirectContent.SetColorFill(iTextSharp.text.BaseColor.WHITE);
            try
            {
                switch (parType)
                {
                    case "EAN13":
                        {
                            System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex("^[0-9]*$");
                            if (oRegex.IsMatch(parText) && parText.Length == 13)
                            {
                                iTextSharp.text.pdf.BarcodeEAN code = new iTextSharp.text.pdf.BarcodeEAN();
                                code.CodeType = iTextSharp.text.pdf.BarcodeEAN.EAN13;
                                code.ChecksumText = true;
                                code.GenerateChecksum = true;
                                code.StartStopText = true;
                                code.Code = parText;
                                code.AltText = parShowText ? parText : String.Empty;
                                result = code.CreateImageWithBarcode(parWriter.DirectContent, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                            }
                            break;
                        }
                    case "EAN8":
                        {
                            System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex("^[0-9]*$");
                            if (oRegex.IsMatch(parText) && parText.Length == 8)
                            {
                                iTextSharp.text.pdf.BarcodeEAN code = new iTextSharp.text.pdf.BarcodeEAN();
                                code.CodeType = iTextSharp.text.pdf.BarcodeEAN.EAN8;
                                code.ChecksumText = true;
                                code.GenerateChecksum = true;
                                code.StartStopText = true;
                                code.Code = parText;
                                code.AltText = parShowText ? parText : String.Empty;
                                result = code.CreateImageWithBarcode(parWriter.DirectContent, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                            }
                            break;
                        }
                    case "CODABAR":
                        {
                            iTextSharp.text.pdf.BarcodeCodabar code = new iTextSharp.text.pdf.BarcodeCodabar();
                            code.CodeType = iTextSharp.text.pdf.BarcodeCodabar.CODABAR;
                            code.ChecksumText = true;
                            code.GenerateChecksum = true;
                            code.StartStopText = true;
                            code.Code = parText;
                            code.AltText = parShowText ? parText : String.Empty;
                            result = code.CreateImageWithBarcode(parWriter.DirectContent, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                            break;
                        }
                    case "CODE128":
                        {
                            iTextSharp.text.pdf.Barcode128 code = new iTextSharp.text.pdf.Barcode128();
                            code.CodeType = iTextSharp.text.pdf.Barcode128.CODE128;
                            code.ChecksumText = true;
                            code.GenerateChecksum = true;
                            code.StartStopText = true;
                            code.Code = parText;
                            code.AltText = parShowText ? parText : String.Empty;
                            result = code.CreateImageWithBarcode(parWriter.DirectContent, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                            break;
                        }
                    case "CODE39":
                        {
                            iTextSharp.text.pdf.Barcode39 code = new iTextSharp.text.pdf.Barcode39();
                            code.CodeType = iTextSharp.text.pdf.Barcode39.CODE128;
                            code.ChecksumText = true;
                            code.GenerateChecksum = true;
                            code.StartStopText = true;
                            code.Code = parText;
                            code.AltText = parShowText ? parText : String.Empty;
                            result = code.CreateImageWithBarcode(parWriter.DirectContent, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                            break;
                        }
                }
            }
            catch
            {
                result = null;
            }

            parWriter.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);

            //public const int CODABAR = 12;
            //public const int CODE128 = 9;
            //public const int CODE128_RAW = 11;
            //public const int CODE128_UCC = 10;
            //public const int EAN13 = 1;
            //public const int EAN8 = 2;
            //public const int PLANET = 8;
            //public const int POSTNET = 7;
            //public const int SUPP2 = 5;
            //public const int SUPP5 = 6;
            //public const int UPCA = 3;
            //public const int UPCE = 4;

            return result;
        }

        public static string GetTemptPdfFileName(string Prefix, string cTempPath)
        {
            string cFile = "";
            try
            {
                string cPath = (cTempPath.Trim() != "" ? cTempPath : (new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)).DirectoryName);
                Int64 nCounter = 0;
                while (true)
                {
                    cFile = cPath + "\\" + Prefix + nCounter.ToString().Trim() + ".pdf";
                    if (!System.IO.File.Exists(cFile))
                    {
                        break;
                    }
                    else
                    {
                        nCounter += 1;
                    }
                }

            }
            catch
            {
                cFile = "";
            }
            return cFile;
        }


        public bool SaveToFile(string FileName)
        {
            FileStream oFileStream = System.IO.File.Create(FileName, this.oStream.GetBuffer().Length);
            oFileStream.Write(this.oStream.GetBuffer(), 0, this.oStream.GetBuffer().Length);
            oFileStream.Flush();
            oFileStream.Close();
            oFileStream.Dispose();
            return true;
        }

        public System.IO.MemoryStream GetStream()
        {
            System.IO.MemoryStream result = new MemoryStream(this.oStream.GetBuffer());
            return result;
        }

        public void Dispose()
        {
            if (this.DocPdf != null)
            {
                this.DocPdf.Dispose();
            }
            if (this.Writer != null)
            {
                this.Writer.Dispose();
            }
            if (this.oStream != null)
            {
                try
                {
                    this.oStream.Close();
                    this.oStream.Dispose();
                }
                catch (Exception)
                {
                }
            }
        }
    }

    // Classe per singola colonna relativa sia al Body che ai totali del documento
    public class clsItemConfigCell
    {
        public string CodiceCampo = "";
        public string Descrizione = "";
        public int Percentuale = 0;
        public ContentAlignment Align = ContentAlignment.MiddleCenter;

        public clsItemConfigCell()
        {
        }

        public clsItemConfigCell(string cCodiceCampo, string cDescrizione, int nPercentuale, ContentAlignment nAlign)
        {
            this.CodiceCampo = cCodiceCampo;
            this.Descrizione = cDescrizione;
            this.Percentuale = nPercentuale;
            this.Align = nAlign;
        }

        public int PdfAling
        {
            get
            {
                return clsItemConfigCell.GetPdfAlign(this.Align);
            }
        }

        public static int GetPdfAlign(ContentAlignment nAlign)
        {
            int nRet = iTextSharp.text.Element.ALIGN_CENTER;
            switch (nAlign)
            {
                case ContentAlignment.TopLeft:
                case ContentAlignment.MiddleLeft:
                case ContentAlignment.BottomLeft:
                    {
                        nRet = iTextSharp.text.Element.ALIGN_LEFT;
                        break;
                    }
                case ContentAlignment.TopCenter:
                case ContentAlignment.MiddleCenter:
                case ContentAlignment.BottomCenter:
                    {
                        nRet = iTextSharp.text.Element.ALIGN_CENTER;
                        break;
                    }
                case ContentAlignment.TopRight:
                case ContentAlignment.MiddleRight:
                case ContentAlignment.BottomRight:
                    {
                        nRet = iTextSharp.text.Element.ALIGN_RIGHT;
                        break;
                    }
            }
            return nRet;
        }
    }
}
