﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintic.Data;
using Wintic.Data.oracle;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data;
using System.Security.Cryptography;
using System.Data.SqlTypes;
using System.Reflection;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.ComponentModel;
using System.Data.Common;
using System.Diagnostics.Eventing.Reader;

namespace libBordero
{
    public class Bordero
    {
        private static string EmptyChar = ((char)0).ToString();
        private static string FormatCurrencty = "#,##0.00";
        private static int BorderoFontSize = 10;

        private class clsItemBordero
        {
            public DataTable Dati = null;
            public DataTable Quote = null;
            public DataTable Abbonamenti = null;
            public DataTable AltriProventi = null;
            public DataTable TitoliMultipli = null;
            public decimal TotaleQuadroD = 0;
            public string Chiave = "";
            public int NumeroPagine = 1;
            public string CODICE_FISCALE_ORGANIZZATORE = "";
            public string DENOMINAZIONE_ORGANIZZATORE = "";
            public string TIPO_CF_PI = "";
            public DateTime GIORNO = DateTime.MinValue;

            public clsItemBordero()
            {

            }

            public clsItemBordero(DataTable Dati, DataTable Quote, DataTable TitoliMultipli, string Chiave, string codice_fiscale_organizzatore, string denominazione_organizzatore, string tipo_cf_pi, DateTime giorno)
            {
                this.Dati = Dati;
                this.Quote = Quote;
                this.TitoliMultipli = TitoliMultipli;
                this.Chiave = Chiave;
                this.CODICE_FISCALE_ORGANIZZATORE = codice_fiscale_organizzatore;
                this.DENOMINAZIONE_ORGANIZZATORE = denominazione_organizzatore;
                this.TIPO_CF_PI = tipo_cf_pi;
                this.GIORNO = giorno;
            }

            public clsItemBordero(DataTable Abbonamenti, string Chiave, string codice_fiscale_organizzatore, string denominazione_organizzatore, string tipo_cf_pi, DateTime giorno)
            {
                this.Abbonamenti = Abbonamenti;
                this.Chiave = Chiave;
                this.CODICE_FISCALE_ORGANIZZATORE = codice_fiscale_organizzatore;
                this.DENOMINAZIONE_ORGANIZZATORE = denominazione_organizzatore;
                this.TIPO_CF_PI = tipo_cf_pi;
                this.GIORNO = giorno;
            }


            public Decimal TotIVADaAssolvere
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        nRet = (decimal)oRow["B_TOT_CORRISPETTIVO_TITOLO"];
                        break;
                    }
                    return nRet;
                }
            }

            public Decimal TotIVAPreAssolta
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        nRet = (decimal)oRow["B_TOT_CORRISPETTIVO_FIGURATIVO"];
                        break;
                    }
                    return nRet;
                }
            }

            public string DEMDescrizione { get; set; }

            
            public Decimal TotAltrePrestazioni
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        try
                        {
                            nRet = (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE1"] + 
                                   (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE2"] + 
                                   (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE3"];
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    }
                    return nRet;
                }
            }

            public Decimal AltrePrestazioniLORDO_SENZA_PRESTAZIONI
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        try
                        {
                            nRet = (decimal)oRow["B_TOT_CORRISPETTIVO_TITOLO"] + 
                                   (decimal)oRow["B_TOT_CORRISPETTIVO_FIGURATIVO"] -
                                   (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE1"] -
                                   (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE2"] -
                                   (decimal)oRow["B_TOT_IMPORTO_PRESTAZIONE3"];
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    }
                    return nRet;
                }
            }

            public Decimal AltrePrestazioniNETTO_SENZA_PRESTAZIONI
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        try
                        {
                            nRet = (decimal)oRow["B_BASE_DEM"];
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    }
                    return nRet;
                }
            }

            public Decimal AltrePrestazioniNETTISSIMO_SENZA_PRESTAZIONI
            {
                get
                {
                    decimal nRet = 0;
                    foreach (DataRow oRow in this.Dati.Rows)
                    {
                        try
                        {
                            nRet = (decimal)oRow["B_BASE_DEM"] - (decimal)(oRow["DEM_BORDERO"]);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    }
                    return nRet;
                }
            }
        }

        public class clsFilterBordero
        {
            public string TipoFiltro = "";
            public List<object[]> RigheFiltro = new List<object[]>();

            public clsFilterBordero()
            {
            }

            public clsFilterBordero(string cTipoFiltro)
                : this()
            {
                this.TipoFiltro = cTipoFiltro;
            }

            public clsFilterBordero(string cTipoFiltro, object[] values)
                : this(cTipoFiltro)
            {
                this.TipoFiltro = cTipoFiltro;
                this.RigheFiltro.Add(values);
            }

        }

        public class clsInfoDataSet
        {
            public DataSet Dataset { get; set; }
            public DateTime GiornoCompetenza { get; set; }
            public DateTime? DataOraEvento { get; set; }
            public Dictionary<string, string> InfoFiltri { get; set; }
            public System.IO.MemoryStream StreamPdf { get; set; }

            public string StringId { get; set; }

            public clsInfoDataSet()
            {
                this.Dataset = new DataSet();
                this.GiornoCompetenza = DateTime.MinValue;
                this.InfoFiltri = new Dictionary<string, string>();
                this.StreamPdf = null;
                this.StringId = "";
            }
            public clsInfoDataSet(DataSet dataSet)
                : this()
            {
                this.Dataset = dataSet;
            }

            public clsInfoDataSet(DataSet dataSet, DateTime giornoCompetenza)
                : this(dataSet)
            {
                this.GiornoCompetenza = giornoCompetenza;
            }

            public clsInfoDataSet(DataSet dataSet, DateTime giornoCompetenza, string filtro, string descrizioneFiltro)
                : this(dataSet, giornoCompetenza)
            {
                this.InfoFiltri.Add(filtro, descrizioneFiltro);
            }

            public clsInfoDataSet(DataSet dataSet, DateTime giornoCompetenza, Dictionary<string, string> infoFiltri)
                : this(dataSet, giornoCompetenza)
            {
                this.InfoFiltri = new Dictionary<string, string>(infoFiltri);
            }

            public static string GetDescrizioneFiltro(string tipoFiltro, DataRow row)
            {
                string result = "";

                switch (tipoFiltro)
                {
                    case "BORDERO_FILT_GIORNI":
                        {
                            result = ((DateTime)row["GIORNO_COMPETENZA"]).ToLongDateString();
                            break;
                        }
                    case "BORDERO_FILT_ORGANIZZATORI":
                        {
                            result = String.Format("Organizzatore {0}", row["DENOMINAZIONE_ORGANIZZATORE"].ToString());
                            break;
                        }
                    case "BORDERO_FILT_TIPI_EVENTO":
                        {
                            result = String.Format("Tipologie Evento {0} {1}", row["DESC_SPETT_INTRATT"].ToString(), row["DESCRIZIONE_TIPO_EVENTO"].ToString());
                            break;
                        }
                    case "BORDERO_FILT_SALE":
                        {
                            result = String.Format("Luogo evento/Sala {0}", row["DENOMINAZIONE_LOCALE"].ToString());
                            break;
                        }
                    case "BORDERO_FILT_TITOLI":
                        {
                            result = String.Format("Titolo {0}", row["TITOLO_EVENTO"].ToString());
                            break;
                        }
                    case "BORDERO_FILT_PRODUTTORI":
                        {
                            result = String.Format("Produttore {0}", row["PRODUTTORE"].ToString());
                            break;
                        }
                    case "BORDERO_FILT_NOLEGGIATORI":
                        {
                            result = String.Format("Distibutore {0}", row["NOLEGGIO"].ToString());
                            break;
                        }
                }

                return result;
            }

            public static List<clsInfoDataSet> SplitSpettacoli(clsInfoDataSet infoDataSet)
            {
                List<clsInfoDataSet> result = new List<clsInfoDataSet>();

                if (infoDataSet.Dataset == null || !infoDataSet.Dataset.Tables.Contains("BORDERO_DATI") || infoDataSet.Dataset.Tables["BORDERO_DATI"].Rows.Count == 0)
                    result.Add(infoDataSet);
                else
                {
                    List<string> eventi = new List<string>();
                    string fieldCHIAVE_BORDERO = "CHIAVE_BORDERO";
                    foreach (DataRow row in infoDataSet.Dataset.Tables["BORDERO_DATI"].Rows)
                    {
                        string cKeyBordero = row[fieldCHIAVE_BORDERO].ToString();
                        if (!eventi.Contains(cKeyBordero))
                        {
                            eventi.Add(cKeyBordero);
                        }
                    }
                    eventi.ForEach(chiave =>
                    {
                        clsInfoDataSet infoDataSetEvento = new clsInfoDataSet()
                        {
                            GiornoCompetenza = infoDataSet.GiornoCompetenza,
                            StreamPdf = infoDataSet.StreamPdf,
                            StringId = infoDataSet.StringId,
                            Dataset = new DataSet()
                        };
                        if (infoDataSet.InfoFiltri != null)
                        {
                            infoDataSetEvento.InfoFiltri = new Dictionary<string, string>();
                            infoDataSet.InfoFiltri.ToList().ForEach(x =>
                            {
                                infoDataSetEvento.InfoFiltri.Add(x.Key, x.Value);
                            });
                        }
                        result.Add(infoDataSetEvento);
                        foreach (DataTable table in infoDataSet.Dataset.Tables)
                        {
                            DataTable tableEvento = new DataTable() { TableName = table.TableName };
                            bool existsColChiaveBordero = false;
                            foreach (DataColumn col  in table.Columns)
                            {
                                tableEvento.Columns.Add(new DataColumn(col.ColumnName, col.DataType, col.Expression, col.ColumnMapping));
                                if (col.ColumnName == fieldCHIAVE_BORDERO)
                                    existsColChiaveBordero = true;
                            }
                            infoDataSetEvento.Dataset.Tables.Add(tableEvento);
                            foreach (DataRow rowTable in table.Rows)
                            {
                                if (!existsColChiaveBordero 
                                    || 
                                    (rowTable[fieldCHIAVE_BORDERO] != null 
                                     && rowTable[fieldCHIAVE_BORDERO] != DBNull.Value 
                                     && rowTable[fieldCHIAVE_BORDERO].ToString() == chiave))
                                {
                                    DataRow newRow = tableEvento.NewRow();
                                    newRow.ItemArray = rowTable.ItemArray;
                                    tableEvento.Rows.Add(newRow);



                                    if (infoDataSetEvento.DataOraEvento == null && table.TableName == "BORDERO_DATI")
                                        infoDataSetEvento.DataOraEvento = (DateTime)rowTable["INIZIO_PRIMO_SPETTACOLO"];
                                }
                            }
                        }
                    });
                }
                return result;
            }
        }

        public class clsSearchEvento
        {
            public string TipoRicerca { get; set; }
            public bool FlagDataEvento { get; set; }
            public DateTime InizioDataEvento { get; set; }
            public DateTime FineDataEvento { get; set; }
            public bool FlagTitolo { get; set; }
            public string Titolo { get; set; }
            public bool FlagCodiceLocale { get; set; }
            public string CodiceLocale { get; set; }

            public List<clsSearchSingoloEvento> Eventi { get; set; }
        }

        public class clsSearchSingoloEvento
        {
            public string TitoloEvento { get; set; }
            public string CodiceLocale { get; set; }
            public DateTime DataOraEvento { get; set; }
        }

        public class clsAltroProventoBordero
        {
            //public DateTime? DATA { get; set; }
            //public long IDCALEND { get; set; }
            public string TIPO_PROVENTO { get; set; }
            public string DESCRIZIONE { get; set; }
            public decimal CORRISPETTIVO_LORDO { get; set; }
            public decimal IVA_CORRISPETTIVO { get; set; }
            public string GENERE_PREVALENTE { get; set; }
            public DateTime DATA_ORA { get; set; }
            public string TITOLO { get; set; }
            public string CODICE_LOCALE { get; set; }
            public string ORGANIZZATORE { get; set; }
            public decimal IMPONIBILE_IVA { get; set; }
            public decimal PERC_IVA { get; set; }
            public decimal INTRA_CORRISPETTIVO { get; set; }
            public decimal IMPONIBILE_INTRA { get; set; }
            public decimal PERC_INTRA { get; set; }

            public override string ToString()
            {
                return string.Format("{0} {1} {2} €   IVA {3}% {4} € ", TIPO_PROVENTO, DESCRIZIONE, CORRISPETTIVO_LORDO.ToString("#,##0.00"), PERC_IVA.ToString(), IVA_CORRISPETTIVO.ToString("#,##0.00"));
            }

            public clsAltroProventoBordero()
            { }

            public clsAltroProventoBordero(DataRow row)
            {
                List<PropertyInfo> listInfo = new List<PropertyInfo>(this.GetType().GetProperties());
                foreach (DataColumn col in row.Table.Columns)
                {

                    if (listInfo.Exists(i => i.Name == col.ColumnName))
                    {
                        PropertyInfo info = listInfo.FirstOrDefault(i => i.Name == col.ColumnName);
                        if (row[col.ColumnName] != null)
                        {
                            info.SetValue(this, row[col.ColumnName]);
                        }
                    }
                }
            }
        }

        public class clsBorderoCedas
        {
            public long CEDAS_ID { get; set; }
            public string CODICE_LOCALE { get; set; }
            public string DATA { get; set; }
            public long PROGRESSIVO { get; set; }
            public string ORA_INIZIO { get; set; }
            public string TITOLO { get; set; }
            public string XML { get; set; }
            public string SENT_XML_ERROR { get; set; }
            public string SENT_PDF_ERROR { get; set; }
            public string TIPO { get; set; }
        }

        public static IConnection GetConnection(string DataSource, out Exception error)
        {
            error = null;
            IConnection Connection = new Wintic.Data.oracle.CConnectionOracle();
            Connection.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", DataSource);
            Connection.Open();
            if (!libBordero.clsCheckVersioneWintic.CheckVersioneWintic(Connection))
            {
                error = new Exception("Versione non compatibile");
                Connection.Close();
                Connection.Dispose();
            }
            return Connection;
        }

        public static Int64 GetNewIdRichiestaBordero(IConnection Connection)
        {
            Int64 nIdRichiesta = 0;
            try
            {
                IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery("SELECT WTIC.SEQ_IDRICHIESTA_BORDERO.NEXTVAL AS IDRICHIESTA_BORDERO FROM DUAL");
                if (!oRS.EOF && !oRS.Fields("IDRICHIESTA_BORDERO").IsNull)
                {
                    Int64.TryParse(oRS.Fields("IDRICHIESTA_BORDERO").Value.ToString(), out nIdRichiesta);
                }
                oRS.Close();
            }
            catch (Exception)
            {
                nIdRichiesta = 0;
            }
            return nIdRichiesta;
        }

        public static void DeleteDatiRichiestaBordero(IConnection Connection, Int64 IdRichiestaBordero, bool TransactionInProgress)
        {
            try
            {
                if (!TransactionInProgress)
                {
                    Connection.BeginTransaction();
                }
                Connection.ExecuteNonQuery("DELETE BORDERO_QUOTE WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_GIORNI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_ORGANIZZATORI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_NOLEGGIATORI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_PRODUTTORI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_SALE WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_TIPI_EVENTO WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_FILT_TITOLI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_DATI_PDF WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                Connection.ExecuteNonQuery("DELETE BORDERO_DATI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero), false);
                if (!TransactionInProgress)
                {
                    Connection.EndTransaction();
                }
            }
            catch (Exception)
            {
                if (!TransactionInProgress)
                {
                    Connection.RollBack();
                }
            }

        }

        public static void DeleteDatiRichiestaBorderoFull(IConnection Connection, bool TransactionInProgress, long nIdUxc)
        {
            try
            {
                if (!TransactionInProgress)
                {
                    Connection.BeginTransaction();
                }
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_QUOTE X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_ORGANIZZATORI X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_GIORNI X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_NOLEGGIATORI X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_PRODUTTORI X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_SALE X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_TIPI_EVENTO X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_FILT_TITOLI X WHERE EXISTS (SELECT NULL FROM WTIC.BORDERO_DATI WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = X.IDRICHIESTA_BORDERO)", new clsParameters(":pIDUXC", nIdUxc), false);
                //Connection.ExecuteNonQuery("DELETE BORDERO_DATI_PDF", false);
                Connection.ExecuteNonQuery("DELETE FROM WTIC.BORDERO_DATI WHERE IDUXC = :pIDUXC", new clsParameters(":pIDUXC", nIdUxc), false);
                if (!TransactionInProgress)
                {
                    Connection.EndTransaction();
                }
            }
            catch (Exception ex)
            {
                if (!TransactionInProgress)
                {
                    Connection.RollBack();
                }
            }

        }

        public static List<clsInfoDataSet> SaveTokenPdfGenerated(IConnection Connection, long IdUxc, DateTime dDataOraInizio, DateTime dDataOraFine, long IdRichiesta, List<clsInfoDataSet> ListData, out Exception oError)
        {
            oError = null;
            List<clsInfoDataSet> result = new List<clsInfoDataSet>();
            try
            {
                string cKey = IdUxc.ToString() + "_" + IdRichiesta.ToString() + "_" +
                              dDataOraInizio.Year.ToString() + "-" + dDataOraInizio.Month.ToString() + "-" + dDataOraInizio.Day.ToString() + "_" +
                              dDataOraInizio.Year.ToString() + "-" + dDataOraInizio.Month.ToString() + "-" + dDataOraInizio.Day.ToString();
                long nRiga = 0;
                foreach (clsInfoDataSet infoDataSet in ListData)
                {
                    nRiga += 1;
                    StringBuilder oSB = new StringBuilder();
                    oSB.Append("INSERT INTO BORDERO_DATI_PDF");
                    oSB.Append(" (IDRICHIESTA, RIGA, GIORNO_COMPETENZA, JSON_DESCRIPTION, DATA_PDF, DATA_INS)");
                    oSB.Append(" VALUES");
                    oSB.Append(" (:pIDRICHIESTA, :pRIGA, :pGIORNO_COMPETENZA, :pJSON_DESCRIPTION, :pDATA_PDF, SYSDATE)");

                    clsParameters oPars = new clsParameters();
                    oPars.Add(":pIDRICHIESTA", IdRichiesta);
                    oPars.Add(":pRIGA", nRiga);
                    oPars.Add(":pGIORNO_COMPETENZA", infoDataSet.GiornoCompetenza);
                    oPars.Add(":pJSON_DESCRIPTION", Newtonsoft.Json.JsonConvert.SerializeObject(infoDataSet.InfoFiltri));
                    oPars.Add(":pDATA_PDF", Convert.ToBase64String(infoDataSet.StreamPdf.ToArray()));
                    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                    clsInfoDataSet oItem = new clsInfoDataSet(null, infoDataSet.GiornoCompetenza, infoDataSet.InfoFiltri);
                    oItem.StringId = Encrypt(cKey + "_" + nRiga.ToString(), kToken, false);
                    result.Add(oItem);
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return result;
        }

        public static string GetTokenPdfGenerated(IConnection Connection, string referenceToken, out Exception oError)
        {
            oError = null;
            string result = null;
            try
            {
                long nIdRichiesta = 0;
                long nRiga = 0;
                string decryptedRef = Decrypt(referenceToken, kToken, false);


                if (decryptedRef.Contains("_") && long.TryParse(decryptedRef.Split('_')[1], out nIdRichiesta) && long.TryParse(decryptedRef.Split('_')[4], out nRiga))
                {
                    StringBuilder oSB = new StringBuilder();
                    oSB.Append("SELECT DATA_PDF FROM BORDERO_DATI_PDF WHERE IDRICHIESTA = :pIDRICHIESTA AND RIGA = :pRIGA");
                    clsParameters oPars = new clsParameters();
                    oPars.Add(":pIDRICHIESTA", nIdRichiesta);
                    oPars.Add(":pRIGA", nRiga);

                    IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                    if (!oRS.EOF && !oRS.Fields("DATA_PDF").IsNull)
                    {
                        result = oRS.Fields("DATA_PDF").Value.ToString();
                    }

                    oRS.Close();

                    oSB = new StringBuilder();
                    oSB.Append("delete FROM BORDERO_DATI_PDF WHERE IDRICHIESTA = :pIDRICHIESTA AND RIGA = :pRIGA");
                    oPars = new clsParameters();
                    oPars.Add(":pIDRICHIESTA", nIdRichiesta);
                    oPars.Add(":pRIGA", nRiga);

                    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return result;
        }

        private const string kToken = "F5EE450FE2119D0B7EDF48FA";

        private static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string EncryptGen(string value)
        {
            return Encrypt(value, kToken, false);
        }
        public static string DecryptGen(string value)
        {
            return Decrypt(value, kToken, false);
        }

        public static long GetIdUxcFromProfileId(IConnection Connection, long ProfileId, out Exception oError)
        {
            oError = null;
            long result = 0;
            try
            {
                IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery("SELECT IDUXC FROM BORD_UXC WHERE PROFILEID = :pPROFILEID", new clsParameters(":pPROFILEID", ProfileId));
                result = (!oRS.EOF && !oRS.Fields("IDUXC").IsNull ? long.Parse(oRS.Fields("IDUXC").Value.ToString()) : 0);
                oRS.Close();
                if (result == 0)
                {
                    oError = new Exception("Profilo non valido");
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return result;
        }

        public static bool CheckIdUxcFromProfileId(IConnection Connection, long nIdUdxc, bool allCodiciLocali, List<string> codiciLocali, bool allCfOrganizzatori, List<string> cfOrganizzatori, out Exception oError)
        {
            bool result = false;
            oError = null;
            try
            {
                StringBuilder oSB;
                clsParameters oPars;
                IRecordSet oRS;

                oSB = new StringBuilder();
                oSB.Append("SELECT IDUXC, NVL(ALL_CODICI_LOCALI, 0) AS ALL_CODICI_LOCALI, NVL(ALL_CF_ORGANIZZATORI, 0) AS ALL_CF_ORGANIZZATORI FROM WTIC.BORD_UXC WHERE IDUXC = :pIDUXC");
                oPars = new clsParameters();
                oPars.Add(":pIDUXC", nIdUdxc);
                oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                if (oRS.EOF)
                {
                    oError = new Exception("IdUxc non valido");
                }
                else
                {
                    Connection.BeginTransaction();

                    try
                    {
                        long nAllCdLoc = long.Parse(oRS.Fields("ALL_CODICI_LOCALI").Value.ToString());
                        long nAllCfOrg = long.Parse(oRS.Fields("ALL_CF_ORGANIZZATORI").Value.ToString());

                        if (nAllCdLoc == 1 && allCodiciLocali)
                        {
                            // nessuna modifica, PER SICUREZZA elimino eventuali impostazioni errate
                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM  WTIC.BORD_CL WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                        else if (nAllCdLoc == 0 && allCodiciLocali)
                        {

                            oSB = new StringBuilder();
                            oSB.Append("UPDATE WTIC.BORD_UXC SET ALL_CODICI_LOCALI = 1 WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                            // elimino associazione IDUXC e sale tanto sono sempre tutte
                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM  WTIC.BORD_CL WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                        else if (!allCodiciLocali)
                        {
                            if (nAllCdLoc == 1)
                            {
                                oSB = new StringBuilder();
                                oSB.Append("UPDATE WTIC.BORD_UXC SET ALL_CODICI_LOCALI = 0 WHERE IDUXC = :pIDUXC");
                                oPars = new clsParameters();
                                oPars.Add(":pIDUXC", nIdUdxc);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }

                            oSB = new StringBuilder();
                            oSB.Append("SELECT * FROM WTIC.BORD_CL WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            IRecordSet oRS_SUB = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                            List<string> qCodiciLocali = new List<string>();
                            foreach (string cCodiceLocale in codiciLocali)
                            {
                                if (!qCodiciLocali.Contains(cCodiceLocale))
                                    qCodiciLocali.Add(cCodiceLocale);
                            }
                            while (!oRS_SUB.EOF)
                            {
                                string cCodiceLocale = oRS_SUB.Fields("CODICE_LOCALE").Value.ToString();
                                if (!qCodiciLocali.Contains(cCodiceLocale))
                                {
                                    oSB = new StringBuilder();
                                    oSB.Append("DELETE FROM  WTIC.BORD_CL WHERE IDUXC = :pIDUXC AND CODICE_LOCALE = :pCODICE_LOCALE");
                                    oPars = new clsParameters();
                                    oPars.Add(":pIDUXC", nIdUdxc);
                                    oPars.Add(":pCODICE_LOCALE", cCodiceLocale);
                                    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                                }
                                else
                                {
                                    qCodiciLocali.Remove(cCodiceLocale);
                                }
                                oRS_SUB.MoveNext();
                            }
                            oRS_SUB.Close();

                            foreach (string cCodiceLocale in qCodiciLocali)
                            {
                                oSB = new StringBuilder();
                                oSB.Append("INSERT INTO WTIC.BORD_CL (IDUXC, CODICE_LOCALE) VALUES (:pIDUXC, :pCODICE_LOCALE)");
                                oPars = new clsParameters();
                                oPars.Add(":pIDUXC", nIdUdxc);
                                oPars.Add(":pCODICE_LOCALE", cCodiceLocale);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }
                        }

                        if (nAllCfOrg == 1 && allCfOrganizzatori)
                        {
                            // nessuna modifica, PER SICUREZZA elimino eventuali impostazioni errate
                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM  WTIC.BORD_CF WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                        else if (nAllCfOrg == 0 && allCfOrganizzatori)
                        {

                            oSB = new StringBuilder();
                            oSB.Append("UPDATE WTIC.BORD_UXC SET ALL_CF_ORGANIZZATORI = 1 WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                            // elimino associazione IDUXC e Organizzatori tanto sono sempre tutti
                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM  WTIC.BORD_CF WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                        else if (!allCfOrganizzatori)
                        {
                            if (nAllCfOrg == 1)
                            {
                                oSB = new StringBuilder();
                                oSB.Append("UPDATE WTIC.BORD_UXC SET ALL_CF_ORGANIZZATORI = 0 WHERE IDUXC = :pIDUXC");
                                oPars = new clsParameters();
                                oPars.Add(":pIDUXC", nIdUdxc);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }

                            oSB = new StringBuilder();
                            oSB.Append("SELECT * FROM WTIC.BORD_CF WHERE IDUXC = :pIDUXC");
                            oPars = new clsParameters();
                            oPars.Add(":pIDUXC", nIdUdxc);
                            IRecordSet oRS_SUB = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                            List<string> qCfOrganizzatori = new List<string>();
                            foreach (string cCfOrganizzatore in cfOrganizzatori)
                            {
                                if (!qCfOrganizzatori.Contains(cCfOrganizzatore))
                                    qCfOrganizzatori.Add(cCfOrganizzatore);
                            }
                            while (!oRS_SUB.EOF)
                            {
                                string cCfOrganizzatore = oRS_SUB.Fields("CF_ORGANIZZATORE").Value.ToString();
                                if (!qCfOrganizzatori.Contains(cCfOrganizzatore))
                                {
                                    oSB = new StringBuilder();
                                    oSB.Append("DELETE FROM  WTIC.BORD_CF WHERE IDUXC = :pIDUXC AND CF_ORGANIZZATORE = :pCF_ORGANIZZATORE");
                                    oPars = new clsParameters();
                                    oPars.Add(":pIDUXC", nIdUdxc);
                                    oPars.Add(":pCF_ORGANIZZATORE", cCfOrganizzatore);
                                    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                                }
                                else
                                {
                                    qCfOrganizzatori.Remove(cCfOrganizzatore);
                                }
                                oRS_SUB.MoveNext();
                            }
                            oRS_SUB.Close();

                            foreach (string cCfOrganizzatore in qCfOrganizzatori)
                            {
                                oSB = new StringBuilder();
                                oSB.Append("INSERT INTO WTIC.BORD_CF (IDUXC, CF_ORGANIZZATORE) VALUES (:pIDUXC, :pCF_ORGANIZZATORE)");
                                oPars = new clsParameters();
                                oPars.Add(":pIDUXC", nIdUdxc);
                                oPars.Add(":pCF_ORGANIZZATORE", cCfOrganizzatore);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        oError = ex;
                    }

                    if (oError != null)
                    {
                        Connection.RollBack();
                    }
                    else
                    {
                        Connection.EndTransaction();
                        result = true;
                    }
                }
                oRS.Close();

            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return result;
        }

        public static System.Data.DataSet GetBordero(IConnection Connection, long nIdUxc, DateTime Inizio, DateTime Termine, ref Int64 IdRichiestaBordero, out Exception oError)
        {
            oError = null;
            System.Data.DataSet oDataSet = new DataSet();
            System.Data.DataTable oTable = null;
            try
            {
                if (IdRichiestaBordero == 0)
                {
                    IdRichiestaBordero = GetNewIdRichiestaBordero(Connection);
                    clsParameters oPars = new clsParameters();
                    clsParameter oPar;
                    oPar = new clsParameter("nIdUxc", nIdUxc);
                    oPars.Add(oPar);
                    oPar = new clsParameter("cINIZIO", Inizio.Year.ToString("0000") + Inizio.Month.ToString("00") + Inizio.Day.ToString("00"));
                    oPars.Add(oPar);
                    oPar = new clsParameter("cTERMIN", Termine.Year.ToString("0000") + Termine.Month.ToString("00") + Termine.Day.ToString("00"));
                    oPars.Add(oPar);
                    oPar = new clsParameter("nIdRichiesta", IdRichiestaBordero);
                    oPars.Add(oPar);
                    Connection.ExecuteNonQuery("P_CALC_BORDERO", oPars, IConnectionCommand.StoredProcedure, false);
                }
                StringBuilder oSB = new StringBuilder();

                oSB.Append("SELECT * FROM BORDERO_DATI WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO");
                oSB.Append(" ORDER BY ");
                oSB.Append(" CODICE_SISTEMA,");
                oSB.Append(" CODICE_FISCALE_TITOLARE,");
                oSB.Append(" DENOMINAZIONE_TITOLARE,");
                oSB.Append(" GIORNO_COMPETENZA,");
                oSB.Append(" CODICE_FISCALE_ORGANIZZATORE,");
                oSB.Append(" DENOMINAZIONE_ORGANIZZATORE,");
                oSB.Append(" TIPO_CF_PI,");
                oSB.Append(" CODICE_LOCALE,");
                oSB.Append(" DENOMINAZIONE_LOCALE,");
                oSB.Append(" LUOGO_LOCALE,");
                oSB.Append(" INIZIO_PRIMO_SPETTACOLO,");
                oSB.Append(" DECODE(TITOLO_IVA_PREASSOLTA,'N',1,'F',2,'B',3),");
                oSB.Append(" DECODE(FLAG_OMAGGIO, 0, 1, 2),");
                oSB.Append(" ORDINE_DI_POSTO,");
                oSB.Append(" TIPO_BIGLIETTO,");
                oSB.Append(" CORRISPETTIVO_TITOLO,");
                oSB.Append(" CORRISPETTIVO_PREVENDITA,");
                oSB.Append(" CORRISPETTIVO_FIGURATIVO");


                oTable = (System.Data.DataTable)Connection.ExecuteQuery(oSB.ToString(), new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero));
                oTable.TableName = "BORDERO_DATI";
                oDataSet.Tables.Add(oTable);

                oTable = (System.Data.DataTable)Connection.ExecuteQuery("SELECT * FROM BORDERO_QUOTE WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero));
                oTable.TableName = "BORDERO_QUOTE";
                oDataSet.Tables.Add(oTable);

                oTable = GetTableAbbonamenti(Connection, nIdUxc, Inizio, Termine, IdRichiestaBordero, out oError);
                oTable.TableName = "BORDERO_ABBONAMENTI";
                oDataSet.Tables.Add(oTable);

                oTable = GetTableAltriProventi(Connection, nIdUxc, Inizio, Termine, IdRichiestaBordero, out oError);
                oTable.TableName = "BORDERO_ALTRI_PROVENTI";
                oDataSet.Tables.Add(oTable);

                bool multicinemaExists = false;
                try
                {
                    IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery("SELECT MULTICINEMA FROM CINEMA.CINEMA");
                    multicinemaExists = (!oRS.EOF && !oRS.Fields("MULTICINEMA").IsNull && int.Parse(oRS.Fields("MULTICINEMA").Value.ToString()) > 0);
                    oRS.Close();
                }
                catch (Exception)
                {
                }

                if (!multicinemaExists || nIdUxc <= 0)
                {
                    oTable = (System.Data.DataTable)Connection.ExecuteQuery("SELECT DESCR FROM CINEMA.CINEMA");
                    oTable.TableName = "BORDERO_CINEMA";
                    oDataSet.Tables.Add(oTable);
                }
                else
                {
                    try
                    {
                        oTable = (System.Data.DataTable)Connection.ExecuteQuery("SELECT CINEMA_DESCRIZIONE AS DESCR FROM CINEMA.V_MC_MULTICINEMA WHERE IDCINEMA = :pIDCINEMA", new clsParameters(":pIDCINEMA", nIdUxc));
                        oTable.TableName = "BORDERO_CINEMA";
                        oDataSet.Tables.Add(oTable);
                    }
                    catch (Exception)
                    {
                        oTable = (System.Data.DataTable)Connection.ExecuteQuery("SELECT DESCR FROM CINEMA.CINEMA");
                        oTable.TableName = "BORDERO_CINEMA";
                        oDataSet.Tables.Add(oTable);
                    }
                    
                }

                string[] aListaTabelleFiltro = new string[] { "BORDERO_FILT_GIORNI", "BORDERO_FILT_ORGANIZZATORI", "BORDERO_FILT_TIPI_EVENTO", "BORDERO_FILT_SALE", "BORDERO_FILT_TITOLI", "BORDERO_FILT_PRODUTTORI", "BORDERO_FILT_NOLEGGIATORI" };
                foreach (string cTabellaFiltro in aListaTabelleFiltro)
                {
                    oTable = (System.Data.DataTable)Connection.ExecuteQuery(string.Format("SELECT * FROM {0} WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO", cTabellaFiltro), new clsParameters(":pIDRICHIESTA_BORDERO", IdRichiestaBordero));
                    oTable.TableName = cTabellaFiltro;
                    oDataSet.Tables.Add(oTable);
                }
                

                oSB = new StringBuilder();
                oSB.Append("SELECT TITOLO_EVENTO");
                oSB.Append("      , TITOLO");
                oSB.Append("      , NAZIONE");
                oSB.Append("      , NOLEGGIO");
                oSB.Append("      , PRODUTTORE");
                oSB.Append("      , PROGR");
                oSB.Append(" FROM (SELECT TITOLO_EVENTO");
                oSB.Append("            , TITOLO");
                oSB.Append("            , NAZIONE");
                oSB.Append("            , NOLEGGIO");
                oSB.Append("            , PRODUTTORE");
                oSB.Append("            , PROGR");
                oSB.Append("            , COUNT(DISTINCT IDFILM) OVER (PARTITION BY TITOLO_EVENTO) AS QTA_TITOLI");
                oSB.Append("       FROM (");
                oSB.Append("             SELECT  ");
                oSB.Append("                    BORDERO_DATI.TITOLO_EVENTO");
                oSB.Append("                  , VR_PACCHETTO.PROGR");
                oSB.Append("                  , VR_FILM.IDFILM");
                oSB.Append("                  , VR_FILM.TITOLO");
                oSB.Append("                  , VR_FILM.IDNAZIONE AS NAZIONE");
                oSB.Append("                  , VR_NOLEGGIO.RAG_SOC AS NOLEGGIO");
                oSB.Append("                  , VR_PRODUTTORI.RAG_SOC AS PRODUTTORE");
                oSB.Append("             FROM WTIC.BORDERO_DATI");
                oSB.Append("             INNER JOIN WTIC.VR_PACCHETTO ON VR_PACCHETTO.DESCRIZIONE = BORDERO_DATI.TITOLO_EVENTO");
                oSB.Append("             INNER JOIN WTIC.VR_FILM ON VR_FILM.IDFILM = VR_PACCHETTO.IDFILM");
                oSB.Append("             INNER JOIN VR_NOLEGGIO ON VR_NOLEGGIO.IDNOLO = VR_FILM.IDNOLO");
                oSB.Append("             INNER JOIN WTIC.VR_PRODUTTORI ON VR_PRODUTTORI.IDPROD = VR_FILM.IDPROD");

                if (multicinemaExists && nIdUxc > 1)
                {
                    oSB.Append(" INNER JOIN CINEMA.FILM ON FILM.IDFILM = VR_FILM.IDFILM AND FILM.STOREID = :pSTOREID");
                    oSB.Append(" INNER JOIN CINEMA.PACCHETTO ON PACCHETTO.IDPACCHETTO = VR_PACCHETTO.IDPACCHETTO AND PACCHETTO.STOREID = :pSTOREID");
                }

                oSB.Append("             WHERE IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO");
                oSB.Append("             GROUP BY BORDERO_DATI.TITOLO_EVENTO");
                oSB.Append("                    , VR_PACCHETTO.PROGR");
                oSB.Append("                    , VR_FILM.IDFILM");
                oSB.Append("                    , VR_FILM.TITOLO");
                oSB.Append("                    , VR_FILM.IDNAZIONE");
                oSB.Append("                    , VR_NOLEGGIO.RAG_SOC");
                oSB.Append("                    , VR_PRODUTTORI.RAG_SOC");
                oSB.Append("            )) WHERE QTA_TITOLI > 1 ORDER BY TITOLO_EVENTO, PROGR");


                clsParameters parsTitoli = new clsParameters();
                parsTitoli.Add(":pIDRICHIESTA_BORDERO", IdRichiestaBordero);
                if (multicinemaExists && nIdUxc > 1)
                    parsTitoli.Add(":pSTOREID", nIdUxc);

                oTable = (System.Data.DataTable)Connection.ExecuteQuery(oSB.ToString(), parsTitoli);
                oTable.TableName = "BORDERO_TITOLI_MULTIPLI";
                oDataSet.Tables.Add(oTable);

                if (oDataSet.Tables["BORDERO_ABBONAMENTI"] != null && oDataSet.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0)
                {
                    List<DateTime> giorniAbbonamenti = new List<DateTime>();
                    //BORDERO_FILT_GIORNI: IDRICHIESTA_BORDERO, GIORNO_COMPETENZA
                    foreach (DataRow row in oDataSet.Tables["BORDERO_ABBONAMENTI"].Rows)
                    {
                        DateTime giorno = (DateTime)row["GIORNO_MESE"];
                        bool lAppend = true;
                        foreach (DataRow rowFind in oDataSet.Tables["BORDERO_FILT_GIORNI"].Rows)
                        {
                            if ((DateTime)rowFind["GIORNO_COMPETENZA"] == giorno)
                            {
                                lAppend = false;
                                break;
                            }
                        }
                        if (lAppend)
                        {
                            DataRow newRow = oDataSet.Tables["BORDERO_FILT_GIORNI"].NewRow();
                            newRow["IDRICHIESTA_BORDERO"] = IdRichiestaBordero;
                            newRow["GIORNO_COMPETENZA"] = giorno;
                            oDataSet.Tables["BORDERO_FILT_GIORNI"].Rows.Add(newRow);
                        }
                    }

                    //BORDERO_FILT_ORGANIZZATORI: IDRICHIESTA_BORDERO, CODICE_FISCALE_ORGANIZZATORE, DENOMINAZIONE_ORGANIZZATORE, TIPO_CF_PI
                    foreach (DataRow row in oDataSet.Tables["BORDERO_ABBONAMENTI"].Rows)
                    {
                        string codiceFiscaleOrganizzatore = row["CODICE_FISCALE_ORGANIZZATORE"].ToString();
                        string denominazioneFiscaleOrganizzatore = row["DENOMINAZIONE_ORGANIZZATORE"].ToString();
                        string cTipoCfPi = row["TIPO_CF_PI"].ToString();
                        bool lAppend = true;
                        foreach (DataRow rowFind in oDataSet.Tables["BORDERO_FILT_ORGANIZZATORI"].Rows)
                        {
                            if (rowFind["CODICE_FISCALE_ORGANIZZATORE"].ToString() == codiceFiscaleOrganizzatore &&
                                rowFind["DENOMINAZIONE_ORGANIZZATORE"].ToString() == denominazioneFiscaleOrganizzatore &&
                                rowFind["TIPO_CF_PI"].ToString() == cTipoCfPi)
                            {
                                lAppend = false;
                                break;
                            }
                        }
                        if (lAppend)
                        {
                            DataRow newRow = oDataSet.Tables["BORDERO_FILT_ORGANIZZATORI"].NewRow();
                            newRow["IDRICHIESTA_BORDERO"] = IdRichiestaBordero;
                            newRow["CODICE_FISCALE_ORGANIZZATORE"] = codiceFiscaleOrganizzatore;
                            newRow["DENOMINAZIONE_ORGANIZZATORE"] = denominazioneFiscaleOrganizzatore;
                            newRow["TIPO_CF_PI"] = cTipoCfPi;
                            oDataSet.Tables["BORDERO_FILT_ORGANIZZATORI"].Rows.Add(newRow);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return oDataSet;
        }

        public static DataTable GetTableAltriProventi(IConnection Connection, long nIdUxc, DateTime Inizio, DateTime Termine, Int64 IdRichiestaBordero, out Exception oError)
        {
            oError = null;
            DataTable oTable = null;

            try
            {
                clsParameters oPars = new clsParameters();
                clsParameter oPar;
                oPar = new clsParameter(":pIDUXC", nIdUxc);
                oPars.Add(oPar);
                //oPar = new clsParameter(":pINIZIO", Inizio.Year.ToString("0000") + Inizio.Month.ToString("00") + Inizio.Day.ToString("00"));
                //oPars.Add(oPar);
                //oPar = new clsParameter(":pTERMIN", Termine.Year.ToString("0000") + Termine.Month.ToString("00") + Termine.Day.ToString("00"));
                //oPars.Add(oPar);
                oPar = new clsParameter(":pIDRICHIESTA_BORDERO", IdRichiestaBordero);
                oPars.Add(oPar);
                oTable = (System.Data.DataTable)Connection.ExecuteQuery(GetQueryAltriProventi(), oPars);
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return oTable;

        }

        public static string GetQueryAltriProventi()
        {
            StringBuilder oSB = new StringBuilder();

            oSB.Append("SELECT");
            oSB.Append(" :pIDUXC AS IDUXC,");
            oSB.Append(" :pIDRICHIESTA_BORDERO AS IDRICHIESTA_BORDERO,");
            oSB.Append(" VR_ALTRIPROVENTI.DATA,");
            oSB.Append(" VR_ALTRIPROVENTI.IDCALEND,");
            oSB.Append(" VR_ALTRIPROVENTI.TIPO_PROVENTO,");
            oSB.Append(" VR_TIPOPROVENTO.DESCRIZIONE,");
            oSB.Append(" VR_ALTRIPROVENTI.CORRISPETTIVO_LORDO,");
            oSB.Append(" VR_ALTRIPROVENTI.IVA_CORRISPETTIVO,");
            oSB.Append(" VR_ALTRIPROVENTI.GENERE_PREVALENTE,");
            oSB.Append(" VR_ALTRIPROVENTI.DATA_ORA,");
            oSB.Append(" VR_ALTRIPROVENTI.TITOLO,");
            oSB.Append(" VR_ALTRIPROVENTI.CODICE_LOCALE,");
            oSB.Append(" VR_ALTRIPROVENTI.ORGANIZZATORE,");
            oSB.Append(" VR_ALTRIPROVENTI.IMPONIBILE_IVA,");
            oSB.Append(" VR_ALTRIPROVENTI.PERC_IVA,");
            oSB.Append(" VR_ALTRIPROVENTI.INTRA_CORRISPETTIVO,");
            oSB.Append(" VR_ALTRIPROVENTI.IMPONIBILE_INTRA,");
            oSB.Append(" VR_ALTRIPROVENTI.PERC_INTRA ");
            oSB.Append(" FROM ");
            oSB.Append(" WTIC.VR_ALTRIPROVENTI");
            oSB.Append(" INNER JOIN WTIC.VR_TIPOPROVENTO ON VR_TIPOPROVENTO.CODICE = VR_ALTRIPROVENTI.TIPO_PROVENTO  ");
            oSB.Append(" WHERE");
            oSB.Append(" EXISTS (");
            oSB.Append(" SELECT NULL FROM BORDERO_DATI ");
            oSB.Append(" WHERE BORDERO_DATI.IDUXC = :pIDUXC AND BORDERO_DATI.IDRICHIESTA_BORDERO = :pIDRICHIESTA_BORDERO");
            oSB.Append(" AND VR_ALTRIPROVENTI.DATA_ORA BETWEEN BORDERO_DATI.INIZIO_PRIMO_SPETTACOLO AND BORDERO_DATI.INIZIO_ULTIMO_SPETTACOLO");
            oSB.Append(" AND TRIM(BORDERO_DATI.TITOLO_EVENTO) = TRIM(VR_ALTRIPROVENTI.TITOLO)");
            oSB.Append(" AND TRIM(BORDERO_DATI.CODICE_LOCALE) = TRIM(VR_ALTRIPROVENTI.CODICE_LOCALE)");
            oSB.Append(" AND TRIM(BORDERO_DATI.CODICE_FISCALE_ORGANIZZATORE) = TRIM(VR_ALTRIPROVENTI.ORGANIZZATORE)");
            oSB.Append(" AND BORDERO_DATI.TIPO_EVENTO = VR_ALTRIPROVENTI.GENERE_PREVALENTE");
            oSB.Append(" )");


            return oSB.ToString();
        }


        public static DataTable GetTableAbbonamenti(IConnection Connection, long nIdUxc, DateTime Inizio, DateTime Termine, Int64 IdRichiestaBordero, out Exception oError)
        {
            oError = null;
            DataTable oTable = null;

            try
            {
                clsParameters oPars = new clsParameters();
                clsParameter oPar;
                oPar = new clsParameter(":pIDUXC", nIdUxc);
                oPars.Add(oPar);
                oPar = new clsParameter(":pINIZIO", Inizio.Year.ToString("0000") + Inizio.Month.ToString("00") + Inizio.Day.ToString("00"));
                oPars.Add(oPar);
                oPar = new clsParameter(":pTERMIN", Termine.Year.ToString("0000") + Termine.Month.ToString("00") + Termine.Day.ToString("00"));
                oPars.Add(oPar);
                oPar = new clsParameter(":pIDRICHIESTA_BORDERO", IdRichiestaBordero);
                oPars.Add(oPar);

                bool multicinemaExists = false;
                long nSEQ_IDFILTER_RIEPILOGHI = 0;
                clsParameters prSMART_TESSERE = null;
                StringBuilder sbSMART_TESSERE = null;
                long insertedFilterIdVend = 0;
                try
                {
                    IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery("SELECT MULTICINEMA FROM CINEMA.CINEMA");
                    multicinemaExists = (!oRS.EOF && !oRS.Fields("MULTICINEMA").IsNull && int.Parse(oRS.Fields("MULTICINEMA").Value.ToString()) > 0);
                    oRS.Close();
                    if (multicinemaExists)
                    {
                        oRS = (IRecordSet)Connection.ExecuteQuery("SELECT WTIC.SEQ_IDFILTER_RIEPILOGHI.NEXTVAL AS IDFILTER FROM DUAL");
                        nSEQ_IDFILTER_RIEPILOGHI = !oRS.EOF && !oRS.Fields("IDFILTER").IsNull ? long.Parse(oRS.Fields("IDFILTER").Value.ToString()) : 0;
                        oRS.Close();

                        if (nSEQ_IDFILTER_RIEPILOGHI > 0)
                        {
                            prSMART_TESSERE = new clsParameters();
                            prSMART_TESSERE.Add(":pINIZIO", Inizio.Date);
                            prSMART_TESSERE.Add(":pTERMIN", Termine.Date);
                            prSMART_TESSERE.Add(":pIDFILTER", nSEQ_IDFILTER_RIEPILOGHI);
                            prSMART_TESSERE.Add(":pIDUXC", nIdUxc);

                            sbSMART_TESSERE = new StringBuilder();
                            sbSMART_TESSERE.Append("INSERT INTO WTIC.RIEPILOGHI_FILTER (IDFILTER, IDVEND)");
                            sbSMART_TESSERE.Append(" SELECT :pIDFILTER AS IDFILTER, ST.IDVEND ");
                            sbSMART_TESSERE.Append(" FROM ");
                            sbSMART_TESSERE.Append(" WTIC.BORD_CF ");
                            sbSMART_TESSERE.Append(" INNER JOIN CINEMA.SMART_TESSERE ST ON ST.CF_ORGANIZZATORE   = BORD_CF.CF_ORGANIZZATORE");
                            sbSMART_TESSERE.Append(" WHERE BORD_CF.IDUXC = :pIDUXC ");
                            sbSMART_TESSERE.Append("   AND ST.STOREID = :pIDUXC");
                            sbSMART_TESSERE.Append("   AND ST.DATA_EMI BETWEEN TRUNC(:pINIZIO) AND (TRUNC(:pTERMIN) + 1 - (1 / 86400))");

                            insertedFilterIdVend = long.Parse(Connection.ExecuteNonQuery(sbSMART_TESSERE.ToString(), prSMART_TESSERE).ToString());
                        }
                    }
                }
                catch (Exception)
                {
                }

                oPars.Add(":pIDFILTER", nSEQ_IDFILTER_RIEPILOGHI);

                oTable = (System.Data.DataTable)Connection.ExecuteQuery(GetQueryAbbonamenti(Connection, nIdUxc, multicinemaExists), oPars);


                if (nSEQ_IDFILTER_RIEPILOGHI > 0)
                {
                    prSMART_TESSERE = new clsParameters();
                    prSMART_TESSERE.Add(":pIDFILTER", nSEQ_IDFILTER_RIEPILOGHI);

                    sbSMART_TESSERE = new StringBuilder();
                    sbSMART_TESSERE.Append("DELETE FROM WTIC.RIEPILOGHI_FILTER WHERE IDFILTER = :pIDFILTER");
                    long deletedFilterIdVend = long.Parse(Connection.ExecuteNonQuery(sbSMART_TESSERE.ToString(), prSMART_TESSERE).ToString());
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return oTable;
        }

        public static string GetQueryAbbonamenti(IConnection Connection, long nIdUxc, bool multicinemaExists)
        {
            StringBuilder oSB = new StringBuilder();

            oSB.Append("SELECT Z.*,");
            oSB.Append(" (SELECT DESCR FROM VR_TIPO_BIGLIETTO WHERE IDTIPOBIGL = Z.MAX_IDTIPOBIGL) AS DESCRIZIONE_TIPO_TITOLO");
            oSB.Append(" FROM (");
            oSB.Append(" SELECT");
            oSB.Append(" :pIDUXC AS IDUXC,");
            oSB.Append(" :pIDRICHIESTA_BORDERO AS IDRICHIESTA_BORDERO,");
            oSB.Append(" CODICE_SISTEMA ||");
            oSB.Append(" CODICE_FISCALE_TITOLARE ||");
            oSB.Append(" DENOMINAZIONE_TITOLARE ||");
            oSB.Append(" CODICE_FISCALE_ORGANIZZATORE ||");
            oSB.Append(" DENOMINAZIONE_ORGANIZZATORE ||");
            oSB.Append(" TRIM(TO_CHAR(GIORNO_MESE,'YYYYMMDD')) AS CHIAVE_ORGANIZZATORE,");
            oSB.Append(" CODICE_SISTEMA ||");
            oSB.Append(" CODICE_FISCALE_TITOLARE ||");
            oSB.Append(" DENOMINAZIONE_TITOLARE ||");
            oSB.Append(" CODICE_FISCALE_ORGANIZZATORE ||");
            oSB.Append(" DENOMINAZIONE_ORGANIZZATORE ||");
            oSB.Append(" TRIM(TO_CHAR(GIORNO_MESE,'YYYYMMDD')) ||");
            oSB.Append(" CODICE_ABBONAMENTO AS CHIAVE_ABBONAMENTO,");
            oSB.Append(" CODICE_SISTEMA,");
            oSB.Append(" CODICE_FISCALE_TITOLARE,");
            oSB.Append(" DENOMINAZIONE_TITOLARE,");
            oSB.Append(" GIORNO_MESE,");
            oSB.Append(" CODICE_FISCALE_ORGANIZZATORE,");
            oSB.Append(" DENOMINAZIONE_ORGANIZZATORE,");
            oSB.Append(" TIPO_CF_PI,");
            oSB.Append(" TITOLO_ABBONAMENTO,");
            oSB.Append(" SPETTACOLO_INTRATTENIMENTO,");
            oSB.Append(" TITOLO_IVA_PREASSOLTA,");
            oSB.Append(" TIPO_TITOLO,");
            if (multicinemaExists)
                oSB.Append($" (SELECT MAX(A.IDTIPOBIGL) FROM CINEMA.TIPO_BIGLIETTO A WHERE A.TIPO_TITOLO = X.TIPO_TITOLO AND A.TASTO LIKE 'T%' AND STOREID = {nIdUxc.ToString()}) AS MAX_IDTIPOBIGL,");
            else
                oSB.Append(" (SELECT MAX(A.IDTIPOBIGL) FROM VR_TIPO_BIGLIETTO A WHERE A.TIPO_TITOLO = X.TIPO_TITOLO AND A.TASTO LIKE 'T%') AS MAX_IDTIPOBIGL,");
            oSB.Append(" CORRISPETTIVO_TITOLO,");
            oSB.Append(" CORRISPETTIVO_PREVENDITA,");
            oSB.Append(" CORRISPETTIVO_FIGURATIVO,");
            oSB.Append(" NETTO_TITOLO,");
            oSB.Append(" NETTO_PREVENDITA,");
            oSB.Append(" NETTO_INTRATTENIMENTO,");
            oSB.Append(" NETTO_FIGURATIVO,");
            oSB.Append(" NETTO_INTRA_FIGURATIVO,");
            oSB.Append(" IVA_TITOLO,");
            oSB.Append(" IVA_PREVENDITA,");
            oSB.Append(" IMPOSTA_INTRATTENIMENTO,");
            oSB.Append(" IVA_FIGURATIVA,");
            oSB.Append(" IMPOSTA_INTRA_FIGURATIVA,");
            oSB.Append(" TIPO_TURNO,");
            oSB.Append(" NUMERO_EVENTI_ABILITATI,");
            oSB.Append(" DATA_LIMITE_VALIDITA,");
            oSB.Append(" DATA_SCADENZA_ABBONAMENTO,");
            oSB.Append(" CODICE_ABBONAMENTO,");
            oSB.Append(" RATEO_EVENTO,");
            oSB.Append(" IVA_RATEO,");
            oSB.Append(" RATEO_IMPONIBILE_INTRA,");
            oSB.Append(" QTA_EMESSI,");
            oSB.Append(" QTA_ANNULLATI,");
            oSB.Append(" TOT_QTA,");
            oSB.Append(" TOT_CORRISPETTIVO_TITOLO,");
            oSB.Append(" TOT_CORRISPETTIVO_PREVENDITA,");
            oSB.Append(" TOT_CORRISPETTIVO_FIGURATIVO,");
            oSB.Append(" TOT_NETTO_TITOLO,");
            oSB.Append(" TOT_NETTO_PREVENDITA,");
            oSB.Append(" TOT_NETTO_INTRATTENIMENTO,");
            oSB.Append(" TOT_NETTO_FIGURATIVO,");
            oSB.Append(" TOT_NETTO_INTRA_FIGURATIVO,");
            oSB.Append(" TOT_IVA_TITOLO,");
            oSB.Append(" TOT_IVA_PREVENDITA,");
            oSB.Append(" TOT_IMPOSTA_INTRATTENIMENTO,");
            oSB.Append(" TOT_IVA_FIGURATIVA,");
            oSB.Append(" TOT_IMPOSTA_INTRA_FIGURATIVA,");
            oSB.Append(" ANN_CORRISPETTIVO_TITOLO,");
            oSB.Append(" ANN_CORRISPETTIVO_PREVENDITA,");
            oSB.Append(" ANN_CORRISPETTIVO_FIGURATIVO,");
            oSB.Append(" ANN_NETTO_TITOLO,");
            oSB.Append(" ANN_NETTO_PREVENDITA,");
            oSB.Append(" ANN_NETTO_INTRATTENIMENTO,");
            oSB.Append(" ANN_NETTO_FIGURATIVO,");
            oSB.Append(" ANN_NETTO_INTRA_FIGURATIVO,");
            oSB.Append(" ANN_IVA_TITOLO,");
            oSB.Append(" ANN_IVA_PREVENDITA,");
            oSB.Append(" ANN_IMPOSTA_INTRATTENIMENTO,");
            oSB.Append(" ANN_IVA_FIGURATIVA,");
            oSB.Append(" ANN_IMPOSTA_INTRA_FIGURATIVA,");

            // TOTALE LORDO RIGA TOT_LORDO
            oSB.Append(" GREATEST(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_CORRISPETTIVO_TITOLO + TOT_CORRISPETTIVO_PREVENDITA - ANN_CORRISPETTIVO_TITOLO + ANN_CORRISPETTIVO_PREVENDITA, TOT_CORRISPETTIVO_FIGURATIVO - ANN_CORRISPETTIVO_FIGURATIVO), 0) AS TOT_LORDO,");
            // TOTALE NETTO RIGA TOT_NETTO
            oSB.Append(" GREATEST(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_NETTO_TITOLO + TOT_NETTO_PREVENDITA - ANN_NETTO_TITOLO - ANN_NETTO_PREVENDITA, TOT_NETTO_FIGURATIVO - ANN_NETTO_FIGURATIVO), 0) AS TOT_NETTO,");
            // TOTALE LORDO RIGA TOT_LORDO
            oSB.Append(" GREATEST(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_IVA_TITOLO + TOT_IVA_PREVENDITA - ANN_IVA_TITOLO - ANN_IVA_PREVENDITA, TOT_IVA_FIGURATIVA - ANN_IVA_FIGURATIVA), 0) AS TOT_IVA,");

            // TOTALI PER CHIAVE
            oSB.Append(" SUM(TOT_QTA) OVER (PARTITION BY CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,TIPO_CF_PI) B_TOT_QTA,");
            oSB.Append(" SUM(QTA_ANNULLATI) OVER (PARTITION BY CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,TIPO_CF_PI) B_QTA_ANNULLATI,");
            oSB.Append(" GREATEST(SUM(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_CORRISPETTIVO_TITOLO + TOT_CORRISPETTIVO_PREVENDITA - ANN_CORRISPETTIVO_TITOLO - ANN_CORRISPETTIVO_PREVENDITA, TOT_CORRISPETTIVO_FIGURATIVO - ANN_CORRISPETTIVO_FIGURATIVO)) OVER (PARTITION BY CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,TIPO_CF_PI), 0) B_TOT_LORDO,");
            oSB.Append(" GREATEST(SUM(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_NETTO_TITOLO + TOT_NETTO_PREVENDITA - ANN_NETTO_TITOLO - ANN_NETTO_PREVENDITA, TOT_NETTO_FIGURATIVO - ANN_NETTO_FIGURATIVO)) OVER (PARTITION BY CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,TIPO_CF_PI), 0) B_TOT_NETTO,");
            oSB.Append(" GREATEST(SUM(DECODE(TITOLO_IVA_PREASSOLTA, 'N', TOT_IVA_TITOLO + TOT_IVA_PREVENDITA - ANN_IVA_TITOLO - ANN_IVA_PREVENDITA, TOT_IVA_FIGURATIVA - ANN_IVA_FIGURATIVA)) OVER (PARTITION BY CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,TIPO_CF_PI), 0) B_TOT_IVA");

            oSB.Append(" FROM");
            oSB.Append(" TABLE(WTIC.F_CALC_RIEPILOGHI_DETTAGLIO(TO_DATE(:pINIZIO,'YYYYMMDD'),TO_DATE(:pTERMIN,'YYYYMMDD'),'G', 'TRUE', 0, :pIDFILTER, 'FALSE')) X");
            oSB.Append(" WHERE");
            oSB.Append(" TITOLO_ABBONAMENTO = 'A'");
            oSB.Append(" ) Z");

            //if (Connection != null)
            //{
            //    //INNER JOIN WTIC.BORD_CF ON BORD_CF.CF_ORGANIZZATORE = Z.CODICE_FISCALE_ORGANIZZATORE
            //    bool multicinemaExists = false;
            //    try
            //    {
            //        IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery("SELECT MULTICINEMA FROM CINEMA.CINEMA");
            //        multicinemaExists = (!oRS.EOF && !oRS.Fields("MULTICINEMA").IsNull && int.Parse(oRS.Fields("MULTICINEMA").Value.ToString()) > 0);
            //        oRS.Close();

            //        if (multicinemaExists)
            //        {
            //            try
            //            {
            //                oRS = (IRecordSet)Connection.ExecuteQuery("SELECT IDCINEMA, CINEMA_DESCRIZIONE FROM CINEMA.V_MC_MULTICINEMA WHERE IDCINEMA  = :pIDCINEMA", new clsParameters(":pIDCINEMA", nIdUxc));
            //                if (!oRS.EOF)
            //                {
            //                    oSB.Append(" INNER JOIN WTIC.BORD_CF ON BORD_CF.CF_ORGANIZZATORE = Z.CODICE_FISCALE_ORGANIZZATORE AND BORD_CF.IDUXC = :pIDUXC");
            //                    oSB.Append(" INNER JOIN CINEMA.SMART_TESSERE ST ON ST.CF_ORGANIZZATORE   = Z.CODICE_FISCALE_ORGANIZZATORE");
            //                    oSB.Append("                                   AND ST.CODICE_ABBONAMENTO = Z.CODICE_ABBONAMENTO");
            //                    oSB.Append("                                   AND ST.IDTEX              = Z.NUM_PROG_ABBONAMENTO");
            //                    oSB.Append("                                   AND ST.STOREID            = :pIDUXC");
            //                }
            //                oRS.Close();
            //            }
            //            catch (Exception)
            //            {
            //            }
            //        }
            //    }
            //    catch (Exception)
            //    {
            //    }

                
            //}



            oSB.Append(" ORDER BY");
            oSB.Append(" CODICE_SISTEMA,");
            oSB.Append(" CODICE_FISCALE_TITOLARE,");
            oSB.Append(" DENOMINAZIONE_TITOLARE,");
            oSB.Append(" CODICE_FISCALE_ORGANIZZATORE,");
            oSB.Append(" DENOMINAZIONE_ORGANIZZATORE,");
            oSB.Append(" GIORNO_MESE,");
            oSB.Append(" CODICE_ABBONAMENTO");

            return oSB.ToString();
        }

        public static bool FilterDatiBordero(ref System.Data.DataSet oDataSetBordero, List<clsFilterBordero> Filtri)
        {
            bool lRet = true;
            if (Filtri != null && Filtri.Count > 0)
            {
                foreach (clsFilterBordero oItemFiltro in Filtri)
                {
                    if (oItemFiltro.RigheFiltro.Count == 0) continue;
                    List<DataRow> oRowsToDelete = new List<DataRow>();
                    foreach (DataRow oRow in oDataSetBordero.Tables["BORDERO_DATI"].Rows)
                    {
                        bool lRowValid = false;
                        foreach (object[] oItemRiga in oItemFiltro.RigheFiltro)
                        {
                            object[] oRowValues = null;
                            switch (oItemFiltro.TipoFiltro)
                            {
                                case "BORDERO_FILT_GIORNI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["GIORNO_COMPETENZA"] };
                                        break;
                                    }
                                case "BORDERO_FILT_ORGANIZZATORI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["CODICE_FISCALE_ORGANIZZATORE"], oRow["DENOMINAZIONE_ORGANIZZATORE"], oRow["TIPO_CF_PI"] };
                                        break;
                                    }
                                case "BORDERO_FILT_NOLEGGIATORI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["NOLEGGIO"] };
                                        break;
                                    }
                                case "BORDERO_FILT_PRODUTTORI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["PRODUTTORE"] };
                                        break;
                                    }
                                case "BORDERO_FILT_SALE":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["CODICE_LOCALE"], oRow["DENOMINAZIONE_LOCALE"], oRow["LUOGO_LOCALE"] };
                                        break;
                                    }
                                case "BORDERO_FILT_TIPI_EVENTO":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["SPETTACOLO_INTRATTENIMENTO"], oRow["DESC_SPETT_INTRATT"], oRow["TIPO_EVENTO"], oRow["DESCRIZIONE_TIPO_EVENTO"] };
                                        break;
                                    }
                                case "BORDERO_FILT_TITOLI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["TITOLO_EVENTO"] };
                                        break;
                                    }
                            }
                            if (oRowValues == null)
                            {
                                break;
                            }
                            else
                            {
                                try
                                {
                                    int index = 0;
                                    int count = 0;
                                    foreach (object oValCompare in oRowValues)
                                    {
                                        if (oValCompare == null && oItemRiga[index] == null)
                                        {
                                            count += 1;
                                        }
                                        else if (oValCompare == null && oItemRiga[index] != null)
                                        {
                                            break;
                                        }
                                        else if (oValCompare != null && oItemRiga[index] == null)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            if (oValCompare.ToString().Equals(oItemRiga[index].ToString()))
                                            {
                                                count += 1;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        index += 1;
                                    }
                                    lRowValid = count == oRowValues.Length;
                                }
                                catch (Exception)
                                {
                                }
                            }
                            if (lRowValid) { break; }
                        }
                        if (!lRowValid) { oRowsToDelete.Add(oRow); }
                    }

                    foreach (DataRow oRow in oRowsToDelete)
                    {
                        oDataSetBordero.Tables["BORDERO_DATI"].Rows.Remove(oRow);
                    }


                    oRowsToDelete = new List<DataRow>();
                    foreach (DataRow oRow in oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows)
                    {
                        bool lRowValid = false;
                        foreach (object[] oItemRiga in oItemFiltro.RigheFiltro)
                        {
                            object[] oRowValues = null;
                            switch (oItemFiltro.TipoFiltro)
                            {
                                case "BORDERO_FILT_GIORNI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["GIORNO_MESE"] };
                                        break;
                                    }
                                case "BORDERO_FILT_ORGANIZZATORI":
                                    {
                                        oRowValues = new object[] { oRow["IDRICHIESTA_BORDERO"], oRow["CODICE_FISCALE_ORGANIZZATORE"], oRow["DENOMINAZIONE_ORGANIZZATORE"], oRow["TIPO_CF_PI"] };
                                        break;
                                    }
                            }
                            if (oRowValues == null)
                            {
                                break;
                            }
                            else
                            {
                                try
                                {
                                    int index = 0;
                                    int count = 0;
                                    foreach (object oValCompare in oRowValues)
                                    {
                                        if (oValCompare == null && oItemRiga[index] == null)
                                        {
                                            count += 1;
                                        }
                                        else if (oValCompare == null && oItemRiga[index] != null)
                                        {
                                            break;
                                        }
                                        else if (oValCompare != null && oItemRiga[index] == null)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            if (oValCompare.ToString().Equals(oItemRiga[index].ToString()))
                                            {
                                                count += 1;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        index += 1;
                                    }
                                    lRowValid = count == oRowValues.Length;
                                }
                                catch (Exception)
                                {
                                }
                            }
                            if (lRowValid) { break; }
                        }
                        if (!lRowValid) { oRowsToDelete.Add(oRow); }
                    }

                    foreach (DataRow oRow in oRowsToDelete)
                    {
                        oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Remove(oRow);
                    }
                }
            }
            return lRet;
        }

        public static List<clsInfoDataSet> MultiBorderoDataSet(string FilterTable, DataSet DataSetBorderoOriginal, clsBordQuoteConfig config)
        {

            List<clsInfoDataSet> oListaDataSet = new List<clsInfoDataSet>();
            clsFilterBordero oFiltro = null;
            string filterTableGiornoCompetenza = "BORDERO_FILT_GIORNI";
            List<clsInfoDataSet> oListaDataSetFiltered = new List<clsInfoDataSet>();

            //if (FilterTable != null && !string.IsNullOrEmpty(FilterTable) && FilterTable.Trim() != "" && DataSetBorderoOriginal.Tables.Contains(FilterTable))
            if (FilterTable != null && !string.IsNullOrEmpty(FilterTable) && FilterTable.Trim() != "")
            {
                if (!FilterTable.Contains(","))
                {
                    FilterTable += ",";
                }

                bool lFirstFilter = true;
                foreach (string currentFilterTable in FilterTable.Split(','))
                {
                    if (currentFilterTable.Trim() != "" && DataSetBorderoOriginal.Tables.Contains(currentFilterTable) && currentFilterTable != filterTableGiornoCompetenza)
                    {
                        if (lFirstFilter)
                        {
                            lFirstFilter = false;
                            foreach (DataRow oRow in DataSetBorderoOriginal.Tables[currentFilterTable].Rows)
                            {
                                object[] values = oRow.ItemArray;
                                oFiltro = new clsFilterBordero(currentFilterTable, oRow.ItemArray);
                                DataSet oItemDataSet = DataSetBorderoOriginal.Copy();
                                libBordero.Bordero.FilterDatiBordero(ref oItemDataSet, new List<clsFilterBordero>() { oFiltro });
                                clsInfoDataSet oInfo = new clsInfoDataSet(oItemDataSet);
                                oInfo.InfoFiltri.Add(oFiltro.TipoFiltro, clsInfoDataSet.GetDescrizioneFiltro(oFiltro.TipoFiltro, oRow));
                                oListaDataSetFiltered.Add(oInfo);
                            }
                        }
                        else
                        {
                            List<clsInfoDataSet> oListaBuffer = new List<clsInfoDataSet>();
                            foreach (clsInfoDataSet oInfoDataSetFiltered in oListaDataSetFiltered)
                            {
                                foreach (DataRow oRow in oInfoDataSetFiltered.Dataset.Tables[currentFilterTable].Rows)
                                {
                                    object[] values = oRow.ItemArray;
                                    oFiltro = new clsFilterBordero(currentFilterTable, oRow.ItemArray);
                                    DataSet oItemDataSet = oInfoDataSetFiltered.Dataset.Copy();
                                    libBordero.Bordero.FilterDatiBordero(ref oItemDataSet, new List<clsFilterBordero>() { oFiltro });
                                    clsInfoDataSet oInfo = new clsInfoDataSet(oItemDataSet);
                                    foreach (KeyValuePair<string, string> itemPreFiltro in oInfoDataSetFiltered.InfoFiltri)
                                    {
                                        oInfo.InfoFiltri.Add(itemPreFiltro.Key, itemPreFiltro.Value);
                                    }
                                    oInfo.InfoFiltri.Add(oFiltro.TipoFiltro, clsInfoDataSet.GetDescrizioneFiltro(oFiltro.TipoFiltro, oRow));
                                    oListaBuffer.Add(oInfo);
                                }
                            }
                            oListaDataSetFiltered = new List<clsInfoDataSet>(oListaBuffer);

                        }
                    }
                }

                if (oListaDataSetFiltered.Count > 0)
                {
                    if (FilterTable.Contains("BORDERO_FILT_GIORNI"))
                    {
                        foreach (clsInfoDataSet oInfoDataSetFiltered in oListaDataSetFiltered)
                        {
                            foreach (DataRow oRow in oInfoDataSetFiltered.Dataset.Tables[filterTableGiornoCompetenza].Rows)
                            {
                                object[] values = oRow.ItemArray;
                                oFiltro = new clsFilterBordero(filterTableGiornoCompetenza, oRow.ItemArray);
                                DataSet oItemDataSet = oInfoDataSetFiltered.Dataset.Copy();
                                libBordero.Bordero.FilterDatiBordero(ref oItemDataSet, new List<clsFilterBordero>() { oFiltro });
                                clsInfoDataSet oInfo = new clsInfoDataSet(oItemDataSet, (DateTime)oRow["GIORNO_COMPETENZA"]);
                                foreach (KeyValuePair<string, string> itemPreFiltro in oInfoDataSetFiltered.InfoFiltri)
                                {
                                    oInfo.InfoFiltri.Add(itemPreFiltro.Key, itemPreFiltro.Value);
                                }
                                oInfo.InfoFiltri.Add(oFiltro.TipoFiltro, clsInfoDataSet.GetDescrizioneFiltro(oFiltro.TipoFiltro, oRow));
                                if (!config.BorderoPerSpettacolo)
                                    oListaDataSet.Add(oInfo);
                                else
                                {
                                    foreach (clsInfoDataSet infoDataSetEvento in clsInfoDataSet.SplitSpettacoli(oInfo))
                                        oListaDataSet.Add(infoDataSetEvento);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (clsInfoDataSet oInfoDataSetFiltered in oListaDataSetFiltered)
                        {
                            if (!config.BorderoPerSpettacolo)
                                oListaDataSet.Add(oInfoDataSetFiltered);
                            else
                            {
                                foreach (clsInfoDataSet infoDataSetEvento in clsInfoDataSet.SplitSpettacoli(oInfoDataSetFiltered))
                                    oListaDataSet.Add(infoDataSetEvento);
                            }
                            
                        }
                    }
                }
            }

            if (oListaDataSetFiltered.Count == 0)
            {
                foreach (DataRow oRow in DataSetBorderoOriginal.Tables[filterTableGiornoCompetenza].Rows)
                {
                    object[] values = oRow.ItemArray;
                    oFiltro = new clsFilterBordero(filterTableGiornoCompetenza, oRow.ItemArray);
                    DataSet oItemDataSet = DataSetBorderoOriginal.Copy();
                    libBordero.Bordero.FilterDatiBordero(ref oItemDataSet, new List<clsFilterBordero>() { oFiltro });
                    clsInfoDataSet oInfo = new clsInfoDataSet(oItemDataSet, (DateTime)oRow["GIORNO_COMPETENZA"]);
                    oInfo.InfoFiltri.Add(oFiltro.TipoFiltro, clsInfoDataSet.GetDescrizioneFiltro(oFiltro.TipoFiltro, oRow));

                    if (!config.BorderoPerSpettacolo)
                        oListaDataSet.Add(oInfo);
                    else
                    {
                        foreach (clsInfoDataSet infoDataSetEvento in clsInfoDataSet.SplitSpettacoli(oInfo))
                            oListaDataSet.Add(infoDataSetEvento);
                    }
                }
            }

            return oListaDataSet;
        }


        public static List<clsInfoDataSet> MultiBorderoStream(string FilterTable, DateTime dDataOraInizio, DateTime dDataOraFine, DataSet DataSetBorderoOriginal, string FontPath, clsBordQuoteConfig config)
        {
            List<clsInfoDataSet> result = new List<clsInfoDataSet>();
            System.IO.MemoryStream stream = null;

            if (config.BorderoPerSpettacolo)
            {
                if (DataSetBorderoOriginal.Tables.Contains("BORDERO_DATI") && DataSetBorderoOriginal.Tables["BORDERO_DATI"].Rows.Count > 0 && DataSetBorderoOriginal.Tables["BORDERO_DATI"].Columns.Contains("QTA_EVENTI"))
                {
                    //QTA_EVENTI
                    foreach (DataRow row in DataSetBorderoOriginal.Tables["BORDERO_DATI"].Rows)
                    {
                        decimal nQta = 0;
                        if (row["QTA_EVENTI"] != null && row["QTA_EVENTI"] != DBNull.Value && decimal.TryParse(row["QTA_EVENTI"].ToString(), out nQta) && nQta > 1)
                        {
                            row["QTA_EVENTI"] = 1;
                        }
                    }
                }
            }

            if (FilterTable.Trim() == "")
            {
                clsInfoDataSet oInfoDataSet = new clsInfoDataSet(DataSetBorderoOriginal);
                oInfoDataSet.InfoFiltri = new Dictionary<string, string>();
                oInfoDataSet.InfoFiltri.Add("PERIODO", string.Format("Dal {0} - Al {1}", dDataOraInizio.ToLongDateString(), dDataOraFine.ToLongDateString()));
                oInfoDataSet.StreamPdf = libBordero.Bordero.GetBorderoPdfStream(oInfoDataSet.Dataset, FontPath, config);
                result.Add(oInfoDataSet);
            }
            else
            {
                foreach (clsInfoDataSet oInfoDataSet in libBordero.Bordero.MultiBorderoDataSet(FilterTable, DataSetBorderoOriginal, config))
                {
                    if ((oInfoDataSet.Dataset.Tables.Contains("BORDERO_DATI") &&
                         oInfoDataSet.Dataset.Tables["BORDERO_DATI"].Rows != null &&
                         oInfoDataSet.Dataset.Tables["BORDERO_DATI"].Rows.Count > 0) ||
                        (oInfoDataSet.Dataset.Tables.Contains("BORDERO_ABBONAMENTI") &&
                         oInfoDataSet.Dataset.Tables["BORDERO_ABBONAMENTI"].Rows != null &&
                         oInfoDataSet.Dataset.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0))
                    {
                        oInfoDataSet.StreamPdf = libBordero.Bordero.GetBorderoPdfStream(oInfoDataSet.Dataset, FontPath, config);
                        result.Add(oInfoDataSet);
                    }
                }
            }

            

            return result;
        }

        public static byte[] GetBorderoPdfBytes(System.Data.DataSet oDataSet, string cFontPath, clsBordQuoteConfig config)
        {
            System.IO.MemoryStream oStream = GetBorderoPdfStream(oDataSet, cFontPath, config);
            return oStream.ToArray();
        }

        public static string GetBorderoPdfStringBase64(System.Data.DataSet oDataSet, string cFontPath, clsBordQuoteConfig config)
        {
            byte[] buffer = GetBorderoPdfBytes(oDataSet, cFontPath, config);
            return System.Convert.ToBase64String(buffer);
        }

        public static System.IO.FileInfo GetBorderoPdfFileInfo(System.Data.DataSet oDataSet, string cFontPath, string FileName, clsBordQuoteConfig config)
        {
            byte[] buffer = GetBorderoPdfBytes(oDataSet, cFontPath, config);
            System.IO.File.WriteAllBytes(FileName, buffer);
            return new System.IO.FileInfo(FileName);
        }

        private class clsItemOrganizzatoreOrderPages
        {
            public string CODICE_FISCALE_ORGANIZZATORE = "";
            public string DENOMINAZIONE_ORGANIZZATORE = "";
            public string TIPO_CF_PI = "";
            public DateTime GIORNO = DateTime.MinValue;

            public clsItemOrganizzatoreOrderPages(string codice_fiscale_organizzatore, string denominazione_organizzatore, string tipo_cf_pi, DateTime giorno)
            {
                this.CODICE_FISCALE_ORGANIZZATORE = codice_fiscale_organizzatore;
                this.DENOMINAZIONE_ORGANIZZATORE = denominazione_organizzatore;
                this.TIPO_CF_PI = tipo_cf_pi;
                this.GIORNO = giorno;
            }
        }

        public static System.IO.MemoryStream GetBorderoPdfStream(System.Data.DataSet oDataSet, string cFontPath, clsBordQuoteConfig config)
        {
            //System.IO.FileInfo oFileInfoPDF = null;
            System.IO.MemoryStream resultStream = null;
            System.Data.DataTable oTableData = null;
            System.Data.DataTable oTableQuote = null;
            System.Data.DataTable oTableTitoliMultipli = null;
            System.Data.DataTable oTableAbbonamenti = null;
            System.Data.DataTable oTableAltriProventi = null;
            System.Data.DataTable oTableCinema = null;
            bool asteriscoSupplementi = (config != null ? config.AsteriscoSupplementi : true);
            bool visualizzaTitoliFilms = (config != null ? !config.TitoloUnico : false);
            string NomeDelCinema = "";
            try
            {
                oTableData = oDataSet.Tables["BORDERO_DATI"];
                oTableQuote = oDataSet.Tables["BORDERO_QUOTE"];
                oTableAbbonamenti = oDataSet.Tables["BORDERO_ABBONAMENTI"];
                oTableAltriProventi = oDataSet.Tables["BORDERO_ALTRI_PROVENTI"];
                oTableCinema = oDataSet.Tables["BORDERO_CINEMA"];
                oTableTitoliMultipli = oDataSet.Tables["BORDERO_TITOLI_MULTIPLI"];

                if (oTableCinema != null && oTableCinema.Rows != null && oTableCinema.Rows.Count > 0)
                {
                    try
                    {
                        NomeDelCinema = oTableCinema.Rows[0]["DESCR"].ToString();
                    }
                    catch (Exception)
                    {
                        NomeDelCinema = "";
                    }
                }

                string[] aKeysFieldsBordero = new string[] { "CHIAVE_BORDERO" };
                string cKey = "";
                List<clsItemBordero> oListTablesDati = new List<clsItemBordero>();
                List<clsItemBordero> oListTablesAbbonamenti = new List<clsItemBordero>();
                DataTable oTableBordero = null;
                DataTable oTableBorderoAbbonamenti = null;
                DataTable oTableQuoteBordero = null;
                DataTable oTableAltriProventiBordero = null;
                DataTable oTableTitoliMultipliBordero = null;

                foreach (DataRow oRow in oTableData.Rows)
                {
                    string cKeyBordero = "";
                    foreach (string cKeyField in aKeysFieldsBordero)
                    {
                        cKeyBordero += oRow[cKeyField].ToString();
                    }
                    string codice_fiscale_organizzatore = oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString();
                    string denominazione_organizzatore = oRow["DENOMINAZIONE_ORGANIZZATORE"].ToString();
                    string tipo_cf_pi = oRow["TIPO_CF_PI"].ToString();
                    DateTime giorno = (DateTime)oRow["GIORNO_COMPETENZA"];



                    clsItemBordero oItemBordero = null;
                    foreach (clsItemBordero oItem in oListTablesDati)
                    {
                        if (oItem.Chiave == cKeyBordero)
                        {
                            oItemBordero = oItem;
                            break;
                        }
                    }

                    if (oItemBordero == null)
                    {
                        cKey = cKeyBordero;
                        oTableBordero = new DataTable();

                        foreach (DataColumn oCol in oTableData.Columns)
                        {
                            DataColumn oColBordero = new DataColumn(oCol.ColumnName, oCol.DataType);
                            oTableBordero.Columns.Add(oColBordero);
                        }
                        oTableQuoteBordero = new DataTable();
                        foreach (DataColumn oCol in oTableQuote.Columns)
                        {
                            DataColumn oColBordero = new DataColumn(oCol.ColumnName, oCol.DataType);
                            oTableQuoteBordero.Columns.Add(oColBordero);
                        }
                        oTableTitoliMultipliBordero = new DataTable();
                        foreach (DataColumn oCol in oTableTitoliMultipli.Columns)
                        {
                            DataColumn oColBordero = new DataColumn(oCol.ColumnName, oCol.DataType);
                            oTableTitoliMultipliBordero.Columns.Add(oColBordero);
                        }
                        if (visualizzaTitoliFilms)
                        {
                            foreach (DataRow oRowTitolo in oTableTitoliMultipli.Rows)
                            {
                                if (oRowTitolo["TITOLO_EVENTO"].ToString() == oRow["TITOLO_EVENTO"].ToString())
                                {
                                    DataRow newRow = oTableTitoliMultipliBordero.NewRow();
                                    foreach (DataColumn oCol in oTableTitoliMultipliBordero.Columns)
                                    {
                                        newRow[oCol.ColumnName] = oRowTitolo[oCol.ColumnName];
                                    }
                                    oTableTitoliMultipliBordero.Rows.Add(newRow);
                                }
                            }
                        }
                        oListTablesDati.Add(new clsItemBordero(oTableBordero, oTableQuoteBordero, oTableTitoliMultipliBordero, cKeyBordero, codice_fiscale_organizzatore, denominazione_organizzatore, tipo_cf_pi, giorno));
                        oTableBordero.Rows.Add(oRow.ItemArray);
                    }
                    else
                    {
                        oItemBordero.Dati.Rows.Add(oRow.ItemArray);
                    }
                }

                foreach (clsItemBordero oItemBordero in oListTablesDati)
                {
                    foreach (DataRow oRow in oTableQuote.Rows)
                    {
                        string cKeyBordero = "";
                        foreach (string cKeyField in aKeysFieldsBordero)
                        {
                            cKeyBordero += oRow[cKeyField].ToString();
                        }
                        if (cKeyBordero == oItemBordero.Chiave)
                        {
                            oItemBordero.Quote.Rows.Add(oRow.ItemArray);
                            try
                            {
                                string descrizioneQuota = oRow["DESCRIZIONE"].ToString();
                                if (config != null && config.QuoteDEM != null)
                                {
                                    clsBordQuota quotaDescDem = config.QuoteDEM.FirstOrDefault(q => descrizioneQuota.StartsWith(q.Descr));
                                    if (quotaDescDem != null)
                                        oItemBordero.DEMDescrizione += (oItemBordero.DEMDescrizione == null || string.IsNullOrEmpty(oItemBordero.DEMDescrizione) ? "" : "\r\n") + quotaDescDem.Descr;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }

                aKeysFieldsBordero = new string[] { "CHIAVE_ORGANIZZATORE" };
                foreach (DataRow oRow in oTableAbbonamenti.Rows)
                {
                    string cKeyBordero = "";
                    foreach (string cKeyField in aKeysFieldsBordero)
                    {
                        cKeyBordero += oRow[cKeyField].ToString();
                    }
                    string codice_fiscale_organizzatore = oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString();
                    string denominazione_organizzatore = oRow["DENOMINAZIONE_ORGANIZZATORE"].ToString();
                    string tipo_cf_pi = oRow["TIPO_CF_PI"].ToString();
                    DateTime giorno = (DateTime)oRow["GIORNO_MESE"];
                    clsItemBordero oItemBordero = null;
                    foreach (clsItemBordero oItem in oListTablesAbbonamenti)
                    {
                        if (oItem.Chiave == cKeyBordero)
                        {
                            oItemBordero = oItem;
                            break;
                        }
                    }

                    if (oItemBordero == null)
                    {
                        cKey = cKeyBordero;
                        oTableBorderoAbbonamenti = new DataTable();

                        foreach (DataColumn oCol in oTableAbbonamenti.Columns)
                        {
                            DataColumn oColBordero = new DataColumn(oCol.ColumnName, oCol.DataType);
                            oTableBorderoAbbonamenti.Columns.Add(oColBordero);
                        }
                        oListTablesAbbonamenti.Add(new clsItemBordero(oTableBorderoAbbonamenti, cKeyBordero, codice_fiscale_organizzatore, denominazione_organizzatore, tipo_cf_pi, giorno));
                        oTableBorderoAbbonamenti.Rows.Add(oRow.ItemArray);
                    }
                    else
                    {
                        oItemBordero.Abbonamenti.Rows.Add(oRow.ItemArray);
                    }
                }

                if (oTableAltriProventi != null && oTableAltriProventi.Rows.Count > 0)
                {
                    foreach (DataRow oRow in oTableAltriProventi.Rows)
                    {
                        string codice_fiscale_organizzatore = oRow["ORGANIZZATORE"].ToString();
                        DateTime dataOra = (DateTime)oRow["DATA_ORA"];
                        //clsItemBordero oItemBordero = null;
                        foreach (clsItemBordero oItem in oListTablesDati)
                        {
                            if (oItem.CODICE_FISCALE_ORGANIZZATORE == codice_fiscale_organizzatore)
                            {
                                //oItemBordero = oItem;
                                if (oItem.AltriProventi == null)
                                {
                                    oItem.AltriProventi = new DataTable();
                                    foreach (DataColumn col in oTableAltriProventi.Columns)
                                        oItem.AltriProventi.Columns.Add(new DataColumn(col.ColumnName, col.DataType));
                                }
                                oItem.AltriProventi.Rows.Add(oRow.ItemArray);
                            }
                        }

                        //if (oItemBordero != null)
                        //{
                        //    if (oItemBordero.AltriProventi == null)
                        //    {
                        //        oItemBordero.AltriProventi = new DataTable();
                        //        foreach (DataColumn col in oTableAltriProventi.Columns)
                        //            oItemBordero.AltriProventi.Columns.Add(new DataColumn(col.ColumnName, col.DataType));
                        //    }
                        //    oItemBordero.AltriProventi.Rows.Add(oRow.ItemArray);
                        //}
                    }

                }
                float nLeftMargin = 10;
                float nTopMargin = 5;
                float nRightMargin = 10;
                float nBottomMargin = 10;

                clsPdfDocument oPdfDocument = new clsPdfDocument(cFontPath);

                oPdfDocument.DocPdf.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                oPdfDocument.DocPdf.SetMargins(nLeftMargin, nRightMargin, nTopMargin, nBottomMargin);
                oPdfDocument.DocPdf.Open();

                int nCurrentPage = 0;
                int nMaxPage = 0;
                int nCurrentPageBordero = 0;

                #region "Calcolo pagine"

                #region "Biglietti"


                foreach (clsItemBordero oItemBordero in oListTablesDati)
                {
                    nMaxPage += 1;
                    DataTable oDati = oItemBordero.Dati;
                    DataTable oQuote = oItemBordero.Quote;
                    DataTable oTitoliMultipli = oItemBordero.TitoliMultipli;
                    float nPageHeight = GetHeightPrintArea(oPdfDocument);
                    nPageHeight = oPdfDocument.DocPdf.PageSize.Height;

                    iTextSharp.text.pdf.PdfPTable oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                    iTextSharp.text.pdf.PdfPTable oHeaderTitolareOrganizzatore = GetHeaderTitolareOrganizzatore(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderSistemaSalaTipoEvento = GetHeaderSistemaSalaTipoEvento(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderComuneProvinciaFilm = GetHeaderComuneProvinciaFilm(oDati, oTitoliMultipli, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderTitoloOrariEventi = GetHeaderTitoloOrariEventi(oDati, oTitoliMultipli, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderBody = GetHeaderBody(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oQuadroC = GetQuadroC(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oQuadroD = GetQuadroD(oQuote, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oFooterBody = GetFooterBody(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oFooterBaseDEM = GetFooterBaseDEM(oDati, oPdfDocument, oItemBordero);
                    iTextSharp.text.pdf.PdfPTable oFooterDEMNettissimo = GetFooterDEMNettissimo(oDati, oPdfDocument, oItemBordero, asteriscoSupplementi);
                    iTextSharp.text.pdf.PdfPTable oFooterPageBordero = GetFooterPageBordero(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);

                    float nTotalHeightHeaders = oHeaderPage.TotalHeight + oHeaderTitolareOrganizzatore.TotalHeight + oHeaderSistemaSalaTipoEvento.TotalHeight + oHeaderComuneProvinciaFilm.TotalHeight + oHeaderTitoloOrariEventi.TotalHeight;
                    float nTotalHeightFooters = oFooterBody.TotalHeight + System.Math.Max(oFooterBaseDEM.TotalHeight, oFooterDEMNettissimo.TotalHeight) + oFooterPageBordero.TotalHeight;
                    float nBodyHeight = nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight - nTotalHeightFooters - nTopMargin - nBottomMargin;
                    float nBodyHeightRows = 0;
                    int nRowIndex = 0;
                    bool lIvaDaAssolvere = oDati.Rows.Count > 0 && oDati.Rows[0]["TITOLO_IVA_PREASSOLTA"].ToString() == "N";

                    iTextSharp.text.pdf.PdfPTable oTabBody = GetTabBody(oDati, oPdfDocument, nBodyHeight);

                    iTextSharp.text.pdf.PdfPTable oTabBodyTEMP_count = GetTabBody(oDati, oPdfDocument, nBodyHeight);
                    foreach (DataRow oRow in oDati.Rows)
                    {
                        lIvaDaAssolvere = oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N";
                        float nRowHeight = 0;

                        //iTextSharp.text.pdf.PdfPTable oTabBodyTEMP = GetTabBody(oDati, oPdfDocument, nBodyHeight);

                        if (oTabBodyTEMP_count == null)
                            oTabBodyTEMP_count = GetTabBody(oDati, oPdfDocument, nBodyHeight);

                        AddBody(oRow, oPdfDocument, oTabBodyTEMP_count, asteriscoSupplementi);

                        if (oItemBordero.TotIVADaAssolvere > 0 && oItemBordero.TotIVAPreAssolta > 0)
                        {
                            if (lIvaDaAssolvere && nRowIndex < oDati.Rows.Count - 1)
                            {
                                if (oDati.Rows[nRowIndex + 1]["TITOLO_IVA_PREASSOLTA"].ToString() != "N")
                                {
                                    AddBodyIvaDaAssolvere(oRow, oPdfDocument, oTabBodyTEMP_count);
                                }
                            }
                            else if (!lIvaDaAssolvere && nRowIndex == oDati.Rows.Count - 1)
                            {
                                //lTotIvaPreassoltaAppend = true;
                                AddBodyIvaPreAssolta(oRow, oPdfDocument, oTabBodyTEMP_count);
                            }
                        }

                        // calcolo lo spazio verticale utile per questa riga
                        nRowHeight = 0;
                        for (int nIndexRows = 0; nIndexRows <= oTabBodyTEMP_count.Rows.Count - 1; nIndexRows++)
                        {
                            nRowHeight += oTabBodyTEMP_count.GetRowHeight(nIndexRows);
                        }

                        if (nBodyHeightRows + nRowHeight > nBodyHeight)
                        {
                            // Aumento il numero di pagine massimo e quelle del singolo borderò
                            oItemBordero.NumeroPagine += 1;
                            nMaxPage += 1;
                            oTabBody = GetTabBody(oDati, oPdfDocument, nBodyHeight);
                            nBodyHeightRows = 0;
                            AddBody(oRow, oPdfDocument, oTabBody, asteriscoSupplementi);
                            oTabBodyTEMP_count = null;
                        }
                        else
                        {
                            AddBody(oRow, oPdfDocument, oTabBody, asteriscoSupplementi);
                            oTabBodyTEMP_count = null;
                        }

                        nBodyHeightRows += nRowHeight;
                        nRowIndex += 1;
                    }
                }

                #endregion

                #region "Abbonamenti"

                foreach (clsItemBordero oItemBordero in oListTablesAbbonamenti)
                {
                    nMaxPage += 1;
                    DataTable oDati = oItemBordero.Abbonamenti;
                    float nPageHeight = GetHeightPrintArea(oPdfDocument);
                    nPageHeight = oPdfDocument.DocPdf.PageSize.Height;

                    iTextSharp.text.pdf.PdfPTable oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                    iTextSharp.text.pdf.PdfPTable oHeaderTitolareOrganizzatore = GetHeaderTitolareOrganizzatoreAbbonamenti(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderSistema = GetHeaderSistemaAbbonamenti(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oHeaderBody = GetHeaderBodyAbbonamenti(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oFooterBody = GetFooterBodyAbbonamenti(oDati, oPdfDocument);
                    iTextSharp.text.pdf.PdfPTable oFooterPageBordero = GetFooterPageBorderoAbbonamenti(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);

                    float nTotalHeightHeaders = oHeaderPage.TotalHeight + oHeaderTitolareOrganizzatore.TotalHeight + oHeaderSistema.TotalHeight + 5;
                    float nTotalHeightFooters = oFooterBody.TotalHeight + oFooterPageBordero.TotalHeight;
                    float nBodyHeight = nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight - nTotalHeightFooters - nTopMargin - nBottomMargin;
                    float nBodyHeightRows = 0;
                    int nRowIndex = 0;

                    iTextSharp.text.pdf.PdfPTable oTabBody = GetTabBodyAbbonamenti(oDati, oPdfDocument, nBodyHeight);

                    foreach (DataRow oRow in oDati.Rows)
                    {
                        float nRowHeight = 0;
                        AddBodyAbbonamenti(oRow, oPdfDocument, oTabBody);
                        nRowHeight = oTabBody.GetRowHeight(oTabBody.Rows.Count - 1);
                        if (nBodyHeightRows + nRowHeight > nBodyHeight)
                        {
                            oItemBordero.NumeroPagine += 1;
                            nMaxPage += 1;
                            oTabBody = GetTabBodyAbbonamenti(oDati, oPdfDocument, nBodyHeight);
                            nBodyHeightRows = 0;
                        }

                        nBodyHeightRows += nRowHeight;
                        nRowIndex += 1;
                    }
                }

                #endregion

                Queue<clsItemOrganizzatoreOrderPages> OrganizzatoriOrderPages = new Queue<clsItemOrganizzatoreOrderPages>();
                List<clsItemOrganizzatoreOrderPages> listaOrganizzatoriOrderPages = new List<clsItemOrganizzatoreOrderPages>();
                foreach (clsItemBordero oItemBordero in oListTablesDati)
                {
                    if (!listaOrganizzatoriOrderPages.Exists(o => o.CODICE_FISCALE_ORGANIZZATORE == oItemBordero.CODICE_FISCALE_ORGANIZZATORE &&
                                                                  o.DENOMINAZIONE_ORGANIZZATORE == oItemBordero.DENOMINAZIONE_ORGANIZZATORE &&
                                                                  o.TIPO_CF_PI == oItemBordero.TIPO_CF_PI &&
                                                                  o.GIORNO == oItemBordero.GIORNO))
                    {
                        clsItemOrganizzatoreOrderPages organizzatoreOrder = new clsItemOrganizzatoreOrderPages(oItemBordero.CODICE_FISCALE_ORGANIZZATORE, oItemBordero.DENOMINAZIONE_ORGANIZZATORE, oItemBordero.TIPO_CF_PI, oItemBordero.GIORNO);
                        listaOrganizzatoriOrderPages.Add(organizzatoreOrder);
                        OrganizzatoriOrderPages.Enqueue(organizzatoreOrder);
                    }
                }

                foreach (clsItemBordero oItemBordero in oListTablesAbbonamenti)
                {
                    if (!listaOrganizzatoriOrderPages.Exists(o => o.CODICE_FISCALE_ORGANIZZATORE == oItemBordero.CODICE_FISCALE_ORGANIZZATORE &&
                                              o.DENOMINAZIONE_ORGANIZZATORE == oItemBordero.DENOMINAZIONE_ORGANIZZATORE &&
                                              o.TIPO_CF_PI == oItemBordero.TIPO_CF_PI &&
                                              o.GIORNO == oItemBordero.GIORNO))
                    {
                        clsItemOrganizzatoreOrderPages organizzatoreOrder = new clsItemOrganizzatoreOrderPages(oItemBordero.CODICE_FISCALE_ORGANIZZATORE, oItemBordero.DENOMINAZIONE_ORGANIZZATORE, oItemBordero.TIPO_CF_PI, oItemBordero.GIORNO);
                        listaOrganizzatoriOrderPages.Add(organizzatoreOrder);
                        OrganizzatoriOrderPages.Enqueue(organizzatoreOrder);
                    }

                }

                #endregion

                bool lFirstPage = true;
                nCurrentPage = 1;

                while (OrganizzatoriOrderPages.Count > 0)
                {
                    clsItemOrganizzatoreOrderPages organizzatoreOrderPages = OrganizzatoriOrderPages.Dequeue();

                    foreach (clsItemBordero oItemBordero in oListTablesDati.Where(i => i.CODICE_FISCALE_ORGANIZZATORE == organizzatoreOrderPages.CODICE_FISCALE_ORGANIZZATORE &&
                                                                                       i.DENOMINAZIONE_ORGANIZZATORE == organizzatoreOrderPages.DENOMINAZIONE_ORGANIZZATORE &&
                                                                                       i.TIPO_CF_PI == organizzatoreOrderPages.TIPO_CF_PI &&
                                                                                       i.GIORNO == organizzatoreOrderPages.GIORNO))

                    {
                        DataTable oDati = oItemBordero.Dati;
                        DataTable oQuote = oItemBordero.Quote;
                        DataTable oTitoliMultipli = oItemBordero.TitoliMultipli;
                        nCurrentPageBordero = 1;
                        if (!lFirstPage)
                        {
                            nCurrentPage += 1;
                            oPdfDocument.DocPdf.NewPage();
                        }
                        lFirstPage = false;


                        float nPageHeight = GetHeightPrintArea(oPdfDocument) + nTopMargin + nBottomMargin;
                        nPageHeight = oPdfDocument.DocPdf.PageSize.Height;

                        iTextSharp.text.pdf.PdfPTable oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                        iTextSharp.text.pdf.PdfPTable oHeaderTitolareOrganizzatore = GetHeaderTitolareOrganizzatore(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderSistemaSalaTipoEvento = GetHeaderSistemaSalaTipoEvento(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderComuneProvinciaFilm = GetHeaderComuneProvinciaFilm(oDati, oTitoliMultipli, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderTitoloOrariEventi = GetHeaderTitoloOrariEventi(oDati, oTitoliMultipli, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderBody = GetHeaderBody(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oQuadroC = GetQuadroC(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oQuadroD = GetQuadroD(oQuote, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oFooterQuadroD = GetFooterQuadroD(oQuote, oPdfDocument, out oItemBordero.TotaleQuadroD);
                        iTextSharp.text.pdf.PdfPTable oFooterBody = GetFooterBody(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oFooterBaseDEM = GetFooterBaseDEM(oDati, oPdfDocument, oItemBordero);
                        iTextSharp.text.pdf.PdfPTable oFooterDEMNettissimo = GetFooterDEMNettissimo(oDati, oPdfDocument, oItemBordero, asteriscoSupplementi);
                        iTextSharp.text.pdf.PdfPTable oFooterPageBordero = GetFooterPageBordero(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);

                        float nTotalHeightHeaders = oHeaderPage.TotalHeight + oHeaderTitolareOrganizzatore.TotalHeight + oHeaderSistemaSalaTipoEvento.TotalHeight + oHeaderComuneProvinciaFilm.TotalHeight + oHeaderTitoloOrariEventi.TotalHeight;
                        float nTotalHeightFooters = oFooterBody.TotalHeight + System.Math.Max(oFooterBaseDEM.TotalHeight, oFooterDEMNettissimo.TotalHeight) + oFooterPageBordero.TotalHeight;
                        float nBodyHeight = nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight - nTotalHeightFooters - nTopMargin - nBottomMargin;
                        float nProgHeightHeaders = nTopMargin;

                        oHeaderPage.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderPage.TotalHeight;
                        oHeaderTitolareOrganizzatore.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderTitolareOrganizzatore.TotalHeight;
                        oHeaderSistemaSalaTipoEvento.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderSistemaSalaTipoEvento.TotalHeight;
                        oHeaderComuneProvinciaFilm.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderComuneProvinciaFilm.TotalHeight;
                        oHeaderTitoloOrariEventi.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderTitoloOrariEventi.TotalHeight;
                        if (oItemBordero.NumeroPagine > nCurrentPageBordero)
                        {
                            GetQuadroC(null, oPdfDocument).WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        }
                        else
                        {
                            oQuadroC.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        }

                        oHeaderBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oQuadroC.TotalHeight;
                        if (oItemBordero.NumeroPagine > nCurrentPageBordero)
                        {
                            iTextSharp.text.pdf.PdfPTable oQuadroD_TEMP = GetQuadroD(null, oPdfDocument);
                            FillTableHeight(oPdfDocument, oQuadroD_TEMP, nBodyHeight - oQuadroC.TotalHeight + 3, true);
                            oQuadroD_TEMP.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3, oPdfDocument.Writer.DirectContent);
                            GetFooterBody(null, oPdfDocument).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                            GetFooterBaseDEM(null, oPdfDocument, null).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                            GetFooterDEMNettissimo(null, oPdfDocument, null, asteriscoSupplementi).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                        }
                        else
                        {
                            FillTableHeight(oPdfDocument, oQuadroD, nBodyHeight - oQuadroC.TotalHeight + 3, true);
                            oQuadroD.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3, oPdfDocument.Writer.DirectContent);
                            oFooterQuadroD.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3 - oQuadroD.TotalHeight, oPdfDocument.Writer.DirectContent);
                            oFooterBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                            oFooterBaseDEM.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                            oFooterDEMNettissimo.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                        }
                        oFooterPageBordero.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight - System.Math.Max(oFooterDEMNettissimo.TotalHeight, oFooterBaseDEM.TotalHeight), oPdfDocument.Writer.DirectContent);

                        float nBodyHeightRows = 0;
                        int nRowIndex = 0;
                        bool lIvaDaAssolvere = oDati.Rows.Count > 0 && oDati.Rows[0]["TITOLO_IVA_PREASSOLTA"].ToString() == "N";
                        bool lTotIvaPreassoltaAppend = false;

                        iTextSharp.text.pdf.PdfPTable oTabBody = GetTabBody(oDati, oPdfDocument, nBodyHeight);

                        DataRow oLastRow = null;
                        iTextSharp.text.pdf.PdfPTable oTabBodyTEMP = GetTabBody(oDati, oPdfDocument, nBodyHeight);
                        foreach (DataRow oRow in oDati.Rows)
                        {

                            int nB_QTA_ANNULLATI = 0;
                            int nB_QTA_EMESSI = 0;
                            int nB_TOT_QTA = 0;

                            bool lBorderoEmpty = (int.TryParse(oRow["B_QTA_ANNULLATI"].ToString(), out nB_QTA_ANNULLATI) &&
                                                  int.TryParse(oRow["B_QTA_EMESSI"].ToString(), out nB_QTA_EMESSI) &&
                                                  int.TryParse(oRow["B_TOT_QTA"].ToString(), out nB_TOT_QTA) &&
                                                  nB_QTA_ANNULLATI + nB_QTA_EMESSI + nB_TOT_QTA == 0);

                            if (lBorderoEmpty)
                            {
                                PrintOutEmpty(oPdfDocument);
                            }



                            lIvaDaAssolvere = oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N";
                            float nRowHeight = 0;

                            if (oTabBodyTEMP == null)
                                oTabBodyTEMP = GetTabBody(oDati, oPdfDocument, nBodyHeight);

                            AddBody(oRow, oPdfDocument, oTabBodyTEMP, asteriscoSupplementi);
                            if (oItemBordero.TotIVADaAssolvere > 0 && oItemBordero.TotIVAPreAssolta > 0)
                            {
                                if (lIvaDaAssolvere && nRowIndex < oDati.Rows.Count - 1)
                                {
                                    if (oDati.Rows[nRowIndex + 1]["TITOLO_IVA_PREASSOLTA"].ToString() != "N")
                                    {
                                        AddBodyIvaDaAssolvere(oRow, oPdfDocument, oTabBodyTEMP);
                                    }
                                }
                                else if (!lIvaDaAssolvere && nRowIndex == oDati.Rows.Count - 1)
                                {
                                    //lTotIvaPreassoltaAppend = true;
                                    AddBodyIvaPreAssolta(oRow, oPdfDocument, oTabBodyTEMP);
                                }
                            }

                            nRowHeight = 0;
                            for (int nIndexRows = 0; nIndexRows <= oTabBodyTEMP.Rows.Count - 1; nIndexRows++)
                            {
                                nRowHeight += oTabBodyTEMP.GetRowHeight(nIndexRows);
                            }

                            if (nBodyHeightRows + nRowHeight > nBodyHeight)
                            {
                                FillTableHeight(oPdfDocument, oTabBody, nBodyHeight, false);
                                oTabBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight, oPdfDocument.Writer.DirectContent);

                                oTabBody = GetTabBody(oDati, oPdfDocument, nBodyHeight);
                                nBodyHeightRows = 0;

                                oPdfDocument.DocPdf.NewPage();
                                nProgHeightHeaders = nTopMargin;
                                nCurrentPage += 1;
                                nCurrentPageBordero += 1;

                                oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                                oHeaderPage.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderPage.TotalHeight;
                                oHeaderTitolareOrganizzatore.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderTitolareOrganizzatore.TotalHeight;
                                oHeaderSistemaSalaTipoEvento.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderSistemaSalaTipoEvento.TotalHeight;
                                oHeaderComuneProvinciaFilm.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderComuneProvinciaFilm.TotalHeight;
                                oHeaderTitoloOrariEventi.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderTitoloOrariEventi.TotalHeight;
                                if (oItemBordero.NumeroPagine > nCurrentPageBordero)
                                {
                                    GetQuadroC(null, oPdfDocument).WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                }
                                else
                                {
                                    oQuadroC.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                }

                                oHeaderBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oQuadroC.TotalHeight;
                                if (oItemBordero.NumeroPagine > nCurrentPageBordero)
                                {
                                    iTextSharp.text.pdf.PdfPTable oQuadroD_TEMP = GetQuadroD(null, oPdfDocument);
                                    FillTableHeight(oPdfDocument, oQuadroD_TEMP, nBodyHeight - oQuadroC.TotalHeight + 3, true);
                                    oQuadroD_TEMP.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3, oPdfDocument.Writer.DirectContent);
                                    GetFooterBody(null, oPdfDocument).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                                    GetFooterBaseDEM(null, oPdfDocument, null).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                                    GetFooterDEMNettissimo(null, oPdfDocument, null, asteriscoSupplementi).WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                                }
                                else
                                {
                                    FillTableHeight(oPdfDocument, oQuadroD, nBodyHeight - oQuadroC.TotalHeight + 3, true);
                                    oQuadroD.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3, oPdfDocument.Writer.DirectContent);
                                    oFooterQuadroD.WriteSelectedRows(0, -1, nLeftMargin + oHeaderBody.TotalWidth + 3, nPageHeight - nProgHeightHeaders - 3 - oQuadroD.TotalHeight, oPdfDocument.Writer.DirectContent);
                                    oFooterBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                                    oFooterBaseDEM.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                                    oFooterDEMNettissimo.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                                }
                                oFooterPageBordero = GetFooterPageBordero(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);
                                oFooterPageBordero.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistemaSalaTipoEvento.TotalHeight - oHeaderComuneProvinciaFilm.TotalHeight - oHeaderTitoloOrariEventi.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight - System.Math.Max(oFooterDEMNettissimo.TotalHeight, oFooterBaseDEM.TotalHeight), oPdfDocument.Writer.DirectContent);
                            }

                            AddBody(oRow, oPdfDocument, oTabBody, asteriscoSupplementi);
                            oTabBodyTEMP = null;
                            //nRowHeight = oTabBody.GetRowHeight(oTabBody.Rows.Count - 1);
                            if (oItemBordero.TotIVADaAssolvere > 0 && oItemBordero.TotIVAPreAssolta > 0)
                            {
                                if (lIvaDaAssolvere && nRowIndex < oDati.Rows.Count - 1)
                                {
                                    if (oDati.Rows[nRowIndex + 1]["TITOLO_IVA_PREASSOLTA"].ToString() != "N")
                                    {
                                        AddBodyIvaDaAssolvere(oRow, oPdfDocument, oTabBody);
                                        //nRowHeight += oTabBody.GetRowHeight(oTabBody.Rows.Count - 1);
                                    }
                                }
                                else if (!lIvaDaAssolvere && nRowIndex == oDati.Rows.Count - 1)
                                {
                                    lTotIvaPreassoltaAppend = true;
                                    AddBodyIvaPreAssolta(oRow, oPdfDocument, oTabBody);
                                    //nRowHeight += oTabBody.GetRowHeight(oTabBody.Rows.Count - 1);
                                }
                            }

                            nBodyHeightRows += nRowHeight;
                            nRowIndex += 1;
                            oLastRow = oRow;
                        }

                        if (oTabBody.Rows.Count > 0)
                        {
                            if (oItemBordero.TotIVADaAssolvere > 0 && oItemBordero.TotIVAPreAssolta > 0 && !lIvaDaAssolvere && oLastRow != null && !lTotIvaPreassoltaAppend)
                            {
                                AddBodyIvaPreAssolta(oLastRow, oPdfDocument, oTabBody);
                            }
                            else if (oItemBordero.TotIVADaAssolvere == 0 && oItemBordero.TotIVAPreAssolta > 0 && !lIvaDaAssolvere && oLastRow != null && !lTotIvaPreassoltaAppend)
                            {
                                AddBodyIvaPreAssolta(oLastRow, oPdfDocument, oTabBody);
                            }

                            FillTableHeight(oPdfDocument, oTabBody, nBodyHeight);
                            oTabBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                        }
                    }

                    foreach (clsItemBordero oItemBordero in oListTablesAbbonamenti.Where(i => i.CODICE_FISCALE_ORGANIZZATORE == organizzatoreOrderPages.CODICE_FISCALE_ORGANIZZATORE &&
                                                                                         i.DENOMINAZIONE_ORGANIZZATORE == organizzatoreOrderPages.DENOMINAZIONE_ORGANIZZATORE &&
                                                                                         i.TIPO_CF_PI == organizzatoreOrderPages.TIPO_CF_PI &&
                                                                                         i.GIORNO == organizzatoreOrderPages.GIORNO))
                    {
                        DataTable oDati = oItemBordero.Abbonamenti;
                        nCurrentPageBordero = 1;
                        if (!lFirstPage)
                        {
                            nCurrentPage += 1;
                            oPdfDocument.DocPdf.NewPage();
                        }
                        lFirstPage = false;

                        float nPageHeight = GetHeightPrintArea(oPdfDocument);
                        nPageHeight = oPdfDocument.DocPdf.PageSize.Height;

                        iTextSharp.text.pdf.PdfPTable oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                        iTextSharp.text.pdf.PdfPTable oHeaderTitolareOrganizzatore = GetHeaderTitolareOrganizzatoreAbbonamenti(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderSistema = GetHeaderSistemaAbbonamenti(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oHeaderBody = GetHeaderBodyAbbonamenti(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oFooterBody = GetFooterBodyAbbonamenti(oDati, oPdfDocument);
                        iTextSharp.text.pdf.PdfPTable oFooterPageBordero = GetFooterPageBorderoAbbonamenti(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);

                        float nTotalHeightHeaders = oHeaderPage.TotalHeight + oHeaderTitolareOrganizzatore.TotalHeight + oHeaderSistema.TotalHeight + 5;
                        float nTotalHeightFooters = oFooterBody.TotalHeight + oFooterPageBordero.TotalHeight;
                        float nBodyHeight = nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight - nTotalHeightFooters - nTopMargin - nBottomMargin;
                        float nBodyHeightRows = 0;
                        int nRowIndex = 0;

                        float nProgHeightHeaders = nTopMargin;

                        oHeaderPage.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderPage.TotalHeight;
                        oHeaderTitolareOrganizzatore.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderTitolareOrganizzatore.TotalHeight;
                        oHeaderSistema.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderSistema.TotalHeight;
                        oHeaderBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                        nProgHeightHeaders += oHeaderBody.TotalHeight;

                        oFooterBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistema.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                        oFooterPageBordero.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistema.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);

                        iTextSharp.text.pdf.PdfPTable oTabBody = GetTabBodyAbbonamenti(oDati, oPdfDocument, nBodyHeight);

                        foreach (DataRow oRow in oDati.Rows)
                        {
                            float nRowHeight = 0;
                            iTextSharp.text.pdf.PdfPTable oTabBodyTEMP = GetTabBodyAbbonamenti(oDati, oPdfDocument, nBodyHeight);

                            AddBodyAbbonamenti(oRow, oPdfDocument, oTabBodyTEMP);

                            nRowHeight = oTabBodyTEMP.GetRowHeight(oTabBodyTEMP.Rows.Count - 1);

                            if (nBodyHeightRows + nRowHeight > nBodyHeight)
                            {
                                FillTableHeight(oPdfDocument, oTabBody, nBodyHeight - 5, false);
                                oTabBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight, oPdfDocument.Writer.DirectContent);

                                nTotalHeightHeaders = oHeaderPage.TotalHeight + oHeaderTitolareOrganizzatore.TotalHeight + oHeaderSistema.TotalHeight + 5;
                                nTotalHeightFooters = oFooterBody.TotalHeight + oFooterPageBordero.TotalHeight;
                                nBodyHeight = nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight - nTotalHeightFooters - nTopMargin - nBottomMargin;


                                oTabBody = GetTabBodyAbbonamenti(oDati, oPdfDocument, nBodyHeight);
                                nBodyHeightRows = 0;

                                oPdfDocument.DocPdf.NewPage();
                                nProgHeightHeaders = nTopMargin;
                                nCurrentPage += 1;
                                nCurrentPageBordero += 1;

                                nProgHeightHeaders = nTopMargin;

                                oHeaderPage = GetHeaderPage(oPdfDocument, nCurrentPage, nMaxPage, NomeDelCinema);
                                oHeaderTitolareOrganizzatore = GetHeaderTitolareOrganizzatoreAbbonamenti(oDati, oPdfDocument);
                                oHeaderSistema = GetHeaderSistemaAbbonamenti(oDati, oPdfDocument);
                                oHeaderBody = GetHeaderBodyAbbonamenti(oDati, oPdfDocument);
                                oFooterBody = GetFooterBodyAbbonamenti(oDati, oPdfDocument);
                                oFooterPageBordero = GetFooterPageBorderoAbbonamenti(oDati, oPdfDocument, nCurrentPageBordero, oItemBordero.NumeroPagine);

                                oHeaderPage.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderPage.TotalHeight;
                                oHeaderTitolareOrganizzatore.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderTitolareOrganizzatore.TotalHeight;
                                oHeaderSistema.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderSistema.TotalHeight;
                                oHeaderBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nProgHeightHeaders, oPdfDocument.Writer.DirectContent);
                                nProgHeightHeaders += oHeaderBody.TotalHeight;

                                oFooterBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistema.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight, oPdfDocument.Writer.DirectContent);
                                oFooterPageBordero.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - oHeaderPage.TotalHeight - oHeaderTitolareOrganizzatore.TotalHeight - oHeaderSistema.TotalHeight - oHeaderBody.TotalHeight - nBodyHeight - oFooterBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                            }

                            AddBodyAbbonamenti(oRow, oPdfDocument, oTabBody);

                            nBodyHeightRows += nRowHeight;
                            nRowIndex += 1;
                        }

                        if (oTabBody != null && oTabBody.Rows.Count > 0)
                        {
                            FillTableHeight(oPdfDocument, oTabBody, nBodyHeight - 5, false);
                            oTabBody.WriteSelectedRows(0, -1, nLeftMargin, nPageHeight - nTotalHeightHeaders - oHeaderBody.TotalHeight, oPdfDocument.Writer.DirectContent);
                        }
                    }
                }

                oPdfDocument.DocPdf.Close();

                resultStream = oPdfDocument.GetStream();

                oPdfDocument.DocPdf.Dispose();

                //oFileInfoPDF = new System.IO.FileInfo(cFilePdf);
            }
            catch (Exception ex)
            {
                string cError = ex.ToString();
            }
            finally
            {
            }
            return resultStream;
        }

        private static void PrintOutEmpty(clsPdfDocument oPdfDocument)
        {

            try
            {
                oPdfDocument.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.GRAY);
                oPdfDocument.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.GRAY);
                PointF oPoint = new PointF(15, 45);
                string cText = "NESSUN BIGLIETTO";
                iTextSharp.text.pdf.BaseFont oFont = oPdfDocument.GetFont(FontStyle.Regular, 10).BaseFont;
                oPdfDocument.Writer.DirectContent.BeginText();
                oPdfDocument.Writer.DirectContent.SetFontAndSize(oFont, 60);
                oPdfDocument.Writer.DirectContent.MoveText((oPoint.X * 6) + oPdfDocument.DocPdf.LeftMargin, oPdfDocument.DocPdf.PageSize.Height - (oPoint.Y * 7) - oPdfDocument.DocPdf.TopMargin);
                oPdfDocument.Writer.DirectContent.ShowTextKerned(cText);
                oPdfDocument.Writer.DirectContent.EndText();
            }
            catch (Exception)
            {
            }
            finally
            {
                oPdfDocument.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);
                oPdfDocument.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
            }
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderPage(clsPdfDocument oPdfDocument, int nCurrentPage, int nMaxPage, string nomeDelCinema)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(2);
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);

            //nomeDelCinema

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("DOCUMENTO NON FISCALE" + (String.IsNullOrEmpty(nomeDelCinema) ? "" : " - ") + nomeDelCinema, oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            //oP.Trim();
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph(string.Format("Pagina {0} di {1}", nCurrentPage, nMaxPage), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            //oP.Trim();
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderTitolareOrganizzatore(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 40, 15, 30 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Titolare Sistema di Emissione:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_TITOLARE"].ToString(), oFontBold));
            // + (nomeDelCinema.Trim() == "" ? "" : "\r\n" + nomeDelCinema)
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("CF:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oP.Trim();
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_FISCALE_TITOLARE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Organizzatore dell'evento:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            //oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString() + ":", oFontRegular);
            oP = new iTextSharp.text.Paragraph("CF:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderTitolareOrganizzatoreAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 40, 15, 30 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Titolare Sistema di Emissione:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_TITOLARE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("CF:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oP.Trim();
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_FISCALE_TITOLARE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Organizzatore abbonamenti:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            //oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString() + ":", oFontRegular);
            oP = new iTextSharp.text.Paragraph("CF:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderSistemaSalaTipoEvento(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 15, 10, 20, 10, 30 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;

            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Codice Sistema di Emissione:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_SISTEMA"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Codice Locale:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_LOCALE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Tipo Evento:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["TIPO_EVENTO"].ToString(), oFontBold));
            oTab.AddCell(oCell);




            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            //oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DESCR_SALA"].ToString() + " " + oTable.Rows[0]["ALIAS_SALA"].ToString(), oFontBold));
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DENOMINAZIONE_LOCALE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["DESCRIZIONE_TIPO_EVENTO"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderSistemaAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 40, 15, 30 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;

            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Codice Sistema di Emissione:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["CODICE_SISTEMA"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            //oCell = GetDeafultCell(false);
            //oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontRegular));
            //oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Data Emissione:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Colspan = 2;
            oCell.AddElement(new iTextSharp.text.Paragraph(((DateTime)oTable.Rows[0]["GIORNO_MESE"]).ToLongDateString(), oFontRegular));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderComuneProvinciaFilm(DataTable oTable, DataTable oTableTitoli, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 15, 6, 5, 7, 52 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;

            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Comune:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["COMUNE"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Provincia:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["PROVINCIA"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Nazione:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            string nazione = oTable.Rows[0]["NAZIONE"].ToString();
            if (oTableTitoli != null && oTableTitoli.Rows != null && oTableTitoli.Rows.Count > 1)
            {
                nazione = "";
                foreach (DataRow rowTitolo in oTableTitoli.Rows)
                {
                    nazione += (string.IsNullOrEmpty(nazione) ? "" : ", ") + rowTitolo["NAZIONE"].ToString();
                }
            }
            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(nazione, oFontBold));
            oTab.AddCell(oCell);


            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Produttore:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            string produttore = oTable.Rows[0]["PRODUTTORE"].ToString();
            if (oTableTitoli != null && oTableTitoli.Rows != null && oTableTitoli.Rows.Count > 1)
            {
                produttore = "";
                foreach (DataRow rowTitolo in oTableTitoli.Rows)
                {
                    if (!string.IsNullOrEmpty(rowTitolo["PRODUTTORE"].ToString()) &&
                        rowTitolo["PRODUTTORE"].ToString() != "." &&
                        !produttore.Contains(rowTitolo["PRODUTTORE"].ToString()))
                        produttore += (string.IsNullOrEmpty(produttore) ? "" : "\r\n") + rowTitolo["PRODUTTORE"].ToString();
                }
            }

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(produttore, oFontBold));
            oTab.AddCell(oCell);


            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Data Evento:", oFontRegular));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(((DateTime)oTable.Rows[0]["GIORNO_COMPETENZA"]).ToLongDateString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Noleggio:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            string noleggio = oTable.Rows[0]["NOLEGGIO"].ToString();
            if (oTableTitoli != null && oTableTitoli.Rows != null && oTableTitoli.Rows.Count > 1)
            {
                noleggio = "";
                foreach (DataRow rowTitolo in oTableTitoli.Rows)
                {
                    if (!string.IsNullOrEmpty(rowTitolo["NOLEGGIO"].ToString()) &&
                        rowTitolo["NOLEGGIO"].ToString() != "." &&
                        !produttore.Contains(rowTitolo["NOLEGGIO"].ToString()))
                        noleggio += (string.IsNullOrEmpty(noleggio) ? "" : "\r\n") + rowTitolo["NOLEGGIO"].ToString();
                }
            }

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(noleggio, oFontBold));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderTitoloOrariEventi(DataTable oTable, DataTable oTableTitoli, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTab = new iTextSharp.text.pdf.PdfPTable(new float[] { 15, 26 + 5, 7, 10 + 5, 7, 10 + 5, 5, 5 });
            oTab.WidthPercentage = 100;
            oTab.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTab.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTab.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph("Titolo:", oFontRegular));
            oTab.AddCell(oCell);

            string titolo = oTable.Rows[0]["TITOLO_EVENTO"].ToString();
            if (oTableTitoli != null && oTableTitoli.Rows != null && oTableTitoli.Rows.Count > 1)
            {
                titolo = oTable.Rows[0]["TITOLO_EVENTO"].ToString();
                foreach (DataRow rowTitolo in oTableTitoli.Rows)
                {
                    string titoloFilm = @"""" + rowTitolo["TITOLO"].ToString() + @"""";
                    if (!string.IsNullOrEmpty(titoloFilm) &&
                        titoloFilm != "." &&
                        !titolo.Contains(titoloFilm))
                        titolo += (string.IsNullOrEmpty(titolo) ? "" : "\r\n") + titoloFilm;
                }
            }

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(titolo, oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Inizio eventi:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["INIZIO_PRIMO_SPETTACOLO"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("Fine Eventi:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["FINE_SPETTACOLI"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("N° Eventi:", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            // TODO
            oCell.AddElement(new iTextSharp.text.Paragraph(oTable.Rows[0]["QTA_EVENTI"].ToString(), oFontBold));
            oTab.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.AddElement(new iTextSharp.text.Paragraph(EmptyChar, oFontBold));
            oTab.AddCell(oCell);

            return oTab;
        }

        private static iTextSharp.text.pdf.PdfPTable GetQuadroC(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabQuadroC = new iTextSharp.text.pdf.PdfPTable(new float[] { 8, 32, 30, 30 });

            oTabQuadroC.TotalWidth = GetWidthPrintArea(oPdfDocument) / 5;
            oTabQuadroC.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabQuadroC.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);

            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("C", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            oCell.AddElement(oP);
            oTabQuadroC.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph("Altri Proventi\r\nEvento", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            oCell.AddElement(oP);
            oCell.Colspan = 3;
            oTabQuadroC.AddCell(oCell);

            string[] aHeaders = new string[] { "Provento Lordo", "IVA", "Provento Netto" };
            foreach (string cHeader in aHeaders)
            {
                oCell = GetDeafultCell(true);
                if (cHeader == aHeaders[0])
                {
                    oCell.Colspan = 2;
                }
                oP = new iTextSharp.text.Paragraph(cHeader, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);
            }

            if (oTable == null)
            {
                oCell = GetDeafultCell(true);
                oCell.Colspan = 2;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);
            }
            else
            {
                oCell = GetDeafultCell(true);
                oCell.Colspan = 2;
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_CORRISPETTIVO_PREVENDITA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_IVA_PREVENDITA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_NETTO_PREVENDITA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabQuadroC.AddCell(oCell);
            }

            return oTabQuadroC;
        }

        private static iTextSharp.text.pdf.PdfPTable GetQuadroD(DataTable oQuote, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabQuadroD = new iTextSharp.text.pdf.PdfPTable(new float[] { 8, 32, 30, 30 });

            oTabQuadroD.TotalWidth = GetWidthPrintArea(oPdfDocument) / 5;
            oTabQuadroD.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabQuadroD.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            oCell = GetDeafultCell(false);
            oP = new iTextSharp.text.Paragraph("D", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            oCell.AddElement(oP);
            oTabQuadroD.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph("RIEPILOGHI SOMME DA VERSARE", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            oCell.AddElement(oP);
            oCell.Colspan = 3;
            oTabQuadroD.AddCell(oCell);

            if (oQuote == null)
            {
                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oCell.Colspan = 4;
                oP = new iTextSharp.text.Paragraph("vedi ultima pagina", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oTabQuadroD.AddCell(oCell);
            }
            else
            {
                if (oQuote.Rows.Count == 0)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                    oCell.Colspan = 4;
                    oP = new iTextSharp.text.Paragraph("", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                    oCell.AddElement(oP);
                    oTabQuadroD.AddCell(oCell);
                }
                else
                {
                    bool lFirstRow = true;
                    foreach (DataRow oRow in oQuote.Rows)
                    {
                        oCell = GetDeafultCell(false);
                        if (lFirstRow)
                        {
                            oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                        }
                        else
                        {
                            oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
                        }
                        oCell.Colspan = 3;
                        oP = new iTextSharp.text.Paragraph(oRow["DESCRIZIONE"].ToString(), oFontRegular);
                        oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                        oCell.AddElement(oP);
                        oTabQuadroD.AddCell(oCell);

                        oCell = GetDeafultCell(false);
                        oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                        decimal nValore = (decimal)(oRow["VALORE"]);
                        oP = new iTextSharp.text.Paragraph((nValore.ToString(FormatCurrencty)), oFontRegular);
                        oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                        oCell.AddElement(oP);
                        oTabQuadroD.AddCell(oCell);

                        lFirstRow = false;
                    }
                }
            }


            return oTabQuadroD;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterQuadroD(DataTable oQuote, clsPdfDocument oPdfDocument, out decimal nTotaleQuadroD)
        {
            nTotaleQuadroD = 0;
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oFooterTabQuadroD = new iTextSharp.text.pdf.PdfPTable(new float[] { 8, 32, 30, 30 });

            oFooterTabQuadroD.TotalWidth = GetWidthPrintArea(oPdfDocument) / 5;
            oFooterTabQuadroD.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oFooterTabQuadroD.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;

            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            if (oQuote == null)
            {
                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oCell.Colspan = 4;
                oP = new iTextSharp.text.Paragraph("vedi ultima pagina", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oFooterTabQuadroD.AddCell(oCell);
            }
            else
            {
                if (oQuote.Rows.Count == 0)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                    oCell.Colspan = 4;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                    oCell.AddElement(oP);
                    oFooterTabQuadroD.AddCell(oCell);
                }
                else
                {
                    foreach (DataRow oRow in oQuote.Rows)
                    {
                        decimal nValore = (decimal)(oRow["VALORE"]);
                        nTotaleQuadroD += nValore;
                    }

                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                    oCell.Colspan = 3;
                    oP = new iTextSharp.text.Paragraph("Totale", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oFooterTabQuadroD.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                    oP = new iTextSharp.text.Paragraph((nTotaleQuadroD.ToString(FormatCurrencty)), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oFooterTabQuadroD.AddCell(oCell);
                }
            }
            return oFooterTabQuadroD;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderBody(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabHeaderQuadroB = new iTextSharp.text.pdf.PdfPTable(new float[] { 32, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8 });
            oTabHeaderQuadroB.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
            oTabHeaderQuadroB.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabHeaderQuadroB.DefaultCell.BorderWidth = 1F;



            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            string[] aHeaders = new string[] { "Tipo Titolo", "Bigl. Ann.", "Bigl. Vend.", "Prezzo Netto", "Imposta Intratt.", "IVA", "Prezzo Lordo", "Incasso Netto", "Imposta Intratt.", "IVA", "Totale Lordo" };
            foreach (string cHeader in aHeaders)
            {
                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(cHeader, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oTabHeaderQuadroB.AddCell(oCell);
            }

            //aHeaders = new string[] { "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*", "9999", "99999", "999,99", "999,99", "999,99", "999,99", "999.999,99", "999.999,99", "999.999,99", "999.999,99" };
            //foreach (string cHeader in aHeaders)
            //{
            //    oCell = GetDeafultCell(true);
            //    oP = new iTextSharp.text.Paragraph(cHeader, oFontRegular);
            //    oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
            //    oCell.AddElement(oP);
            //    oTabHeaderQuadroB.AddCell(oCell);
            //}
            return oTabHeaderQuadroB;
        }

        private static iTextSharp.text.pdf.PdfPTable GetHeaderBodyAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabHeaderAbbonamenti = new iTextSharp.text.pdf.PdfPTable(new float[] { 24, 7, 11, 6, 6, 8, 6, 4, 14, 14 });
            oTabHeaderAbbonamenti.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTabHeaderAbbonamenti.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabHeaderAbbonamenti.DefaultCell.BorderWidth = 1F;



            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            string[] aHeaders = new string[] { "Tipo Titolo", "Codice Abbonamento", "Intrattenimento Spettacolo", "Fisso Libero", "Numero Venduti", "Importo Lordo Incassato", "Abbonamenti Annullati", "Numero Eventi", "IVA", "Netto" };

            oCell = GetDeafultCell(false);
            oCell.Colspan = aHeaders.Length;
            oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabHeaderAbbonamenti.AddCell(oCell);


            foreach (string cHeader in aHeaders)
            {
                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(cHeader, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oTabHeaderAbbonamenti.AddCell(oCell);
            }
            return oTabHeaderAbbonamenti;
        }

        private static iTextSharp.text.pdf.PdfPTable GetTabBodyAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument, float nBodyHeight)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabBodyAbbonamenti = new iTextSharp.text.pdf.PdfPTable(new float[] { 24, 7, 11, 6, 6, 8, 6, 4, 14, 14 });
            oTabBodyAbbonamenti.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTabBodyAbbonamenti.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabBodyAbbonamenti.DefaultCell.BorderWidth = 1F;
            return oTabBodyAbbonamenti;
        }

        private static void AddBodyAbbonamenti(DataRow oRow, clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oTabBody)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            string[] aHeaders = new string[] { "Tipo Titolo", "Codice Abbonamento", "Intrattenimento Spettacolo", "Fisso Libero", "Numero Venduti", "Importo Lordo Incassato", "Abbonamenti Annullati", "Numero Eventi", "IVA", "Netto" };

            oCell = GetDeafultCell(false);
            oCell.Border = (iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);

            string cTipoTitolo = oRow["TIPO_TITOLO"].ToString();

            try
            {
                if (oRow["DESCRIZIONE_TIPO_TITOLO"] != null && oRow["DESCRIZIONE_TIPO_TITOLO"] != System.DBNull.Value && oRow["DESCRIZIONE_TIPO_TITOLO"].ToString().Trim() != "")
                    cTipoTitolo += " " + oRow["DESCRIZIONE_TIPO_TITOLO"].ToString();
            }
            catch (Exception)
            {
                cTipoTitolo = oRow["TIPO_TITOLO"].ToString();
            }

            try
            {
                if (oRow["TITOLO_IVA_PREASSOLTA"] != null && oRow["TITOLO_IVA_PREASSOLTA"] != System.DBNull.Value && oRow["TITOLO_IVA_PREASSOLTA"].ToString().Trim() == "F")
                    cTipoTitolo += " (certif.Fatt.)";
            }
            catch (Exception)
            {
            }

            //oP = new iTextSharp.text.Paragraph(oRow["TIPO_TITOLO"].ToString(), oFontRegular);
            oP = new iTextSharp.text.Paragraph(cTipoTitolo, oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["CODICE_ABBONAMENTO"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["SPETTACOLO_INTRATTENIMENTO"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["TIPO_TURNO"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["TOT_QTA"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(((decimal)oRow["TOT_LORDO"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["QTA_ANNULLATI"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(oRow["NUMERO_EVENTI_ABILITATI"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(((decimal)oRow["TOT_IVA"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);

            oCell = GetDeafultCell(false);
            oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
            oP = new iTextSharp.text.Paragraph(((decimal)oRow["TOT_NETTO"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabBody.AddCell(oCell);
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterBodyAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterAbbonamenti = new iTextSharp.text.pdf.PdfPTable(new float[] { 24, 7, 11, 6, 6, 8, 6, 4, 14, 14 });
            oTabFooterAbbonamenti.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTabFooterAbbonamenti.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterAbbonamenti.DefaultCell.BorderWidth = 1F;



            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            string[] aHeaders = new string[] { "Tipo Titolo", "Codice Abbonamento", "Intrattenimento Spettacolo", "Fisso Libero", "Numero Venduti", "Importo Lordo Incassato", "Abbonamenti Annullati", "Numero Eventi", "IVA", "Netto" };

            try
            {
                Dictionary<string, Dictionary<string, decimal>> totali = new Dictionary<string, Dictionary<string, decimal>>();
                foreach (DataRow rowAbb in oTable.Rows)
                {
                    string keyRow = "Tot. Turno " +
                                    (rowAbb["TIPO_TURNO"] != null && rowAbb["TIPO_TURNO"] != System.DBNull.Value ? (rowAbb["TIPO_TURNO"].ToString() == "F" ? "Fisso" : "Libero") : "") +
                                    " IVA " +
                                    (rowAbb["TITOLO_IVA_PREASSOLTA"] != null && rowAbb["TITOLO_IVA_PREASSOLTA"] != System.DBNull.Value ? (rowAbb["TITOLO_IVA_PREASSOLTA"].ToString() == "F" ? "Certif.Fattura" : "da Assolvere") : "");
                    if (!totali.ContainsKey(keyRow))
                        totali.Add(keyRow, new Dictionary<string, decimal>()
                    {
                        { "TOT_QTA", 0 }, { "TOT_LORDO", 0 },{ "QTA_ANNULLATI", 0 },{ "TOT_IVA", 0 },{ "TOT_NETTO", 0 }
                    });
                    totali[keyRow]["TOT_QTA"] += (rowAbb["TOT_QTA"] != null && rowAbb["TOT_QTA"] != System.DBNull.Value ? decimal.Parse(rowAbb["TOT_QTA"].ToString()) : 0);
                    totali[keyRow]["TOT_LORDO"] += (rowAbb["TOT_LORDO"] != null && rowAbb["TOT_LORDO"] != System.DBNull.Value ? decimal.Parse(rowAbb["TOT_LORDO"].ToString()) : 0);
                    totali[keyRow]["QTA_ANNULLATI"] += (rowAbb["QTA_ANNULLATI"] != null && rowAbb["QTA_ANNULLATI"] != System.DBNull.Value ? decimal.Parse(rowAbb["QTA_ANNULLATI"].ToString()) : 0);
                    totali[keyRow]["TOT_IVA"] += (rowAbb["TOT_IVA"] != null && rowAbb["TOT_IVA"] != System.DBNull.Value ? decimal.Parse(rowAbb["TOT_IVA"].ToString()) : 0);
                    totali[keyRow]["TOT_NETTO"] += (rowAbb["TOT_NETTO"] != null && rowAbb["TOT_NETTO"] != System.DBNull.Value ? decimal.Parse(rowAbb["TOT_NETTO"].ToString()) : 0);
                }

                totali.ToList().ForEach(totale =>
                {
                    oCell = GetDeafultCell(true);
                    oCell.Colspan = 4;
                    oP = new iTextSharp.text.Paragraph(totale.Key, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(totale.Value["TOT_QTA"].ToString(), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(((decimal)totale.Value["TOT_LORDO"]).ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(totale.Value["QTA_ANNULLATI"].ToString(), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(string.Empty, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(((decimal)totale.Value["TOT_IVA"]).ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);

                    oCell = GetDeafultCell(true);
                    oP = new iTextSharp.text.Paragraph(((decimal)totale.Value["TOT_NETTO"]).ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterAbbonamenti.AddCell(oCell);
                });
            }
            catch (Exception)
            {
            }

            oCell = GetDeafultCell(true);
            oCell.Colspan = 4;
            oP = new iTextSharp.text.Paragraph("Totali", oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["B_TOT_QTA"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(((decimal)oTable.Rows[0]["B_TOT_LORDO"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["B_QTA_ANNULLATI"].ToString(), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(string.Empty, oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(((decimal)oTable.Rows[0]["B_TOT_IVA"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            oCell = GetDeafultCell(true);
            oP = new iTextSharp.text.Paragraph(((decimal)oTable.Rows[0]["B_TOT_NETTO"]).ToString(FormatCurrencty), oFontRegular);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterAbbonamenti.AddCell(oCell);

            return oTabFooterAbbonamenti;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterBody(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterBody = new iTextSharp.text.pdf.PdfPTable(new float[] { 32, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8 });
            oTabFooterBody.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
            oTabFooterBody.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterBody.DefaultCell.BorderWidth = 1F;



            iTextSharp.text.pdf.PdfPCell oCell;

            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            if (oTable == null)
            {
                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph("Totali", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);
            }
            else
            {
                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph("Totali", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["B_QTA_ANNULLATI"].ToString(), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(oTable.Rows[0]["B_TOT_QTA"].ToString(), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_IMPOSTA_INTRATTENIMENTO"]) + (decimal)(oTable.Rows[0]["B_TOT_IMPOSTA_INTRA_FIGURATIVA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_IVA_TITOLO"]) + (decimal)(oTable.Rows[0]["B_TOT_IVA_FIGURATIVA"]) + (decimal)(oTable.Rows[0]["B_IVA_OMAGGI_ECCEDENTI"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_CORRISPETTIVO_TITOLO"]) + (decimal)(oTable.Rows[0]["B_TOT_CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBody.AddCell(oCell);
            }



            return oTabFooterBody;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterBaseDEM(DataTable oTable, clsPdfDocument oPdfDocument, clsItemBordero oItemBordero)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterBaseDEM = new iTextSharp.text.pdf.PdfPTable(new float[] { 60, 40 });
            oTabFooterBaseDEM.TotalWidth = GetWidthPrintArea(oPdfDocument) / 5;
            oTabFooterBaseDEM.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterBaseDEM.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, 1);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


            if (oTable == null)
            {
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph("vedi ultima pagina", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterBaseDEM.AddCell(oCell);

            }
            else
            {
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph("Totale Netto", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                if ((decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"]) > (decimal)(oTable.Rows[0]["B_BASE_DEM"]))
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                    oP = new iTextSharp.text.Paragraph("Sconto P10", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterBaseDEM.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                    oP = new iTextSharp.text.Paragraph("-" + ((decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"]) - (decimal)(oTable.Rows[0]["B_BASE_DEM"])).ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterBaseDEM.AddCell(oCell);

                }
                else
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterBaseDEM.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterBaseDEM.AddCell(oCell);
                }

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph("Netto Omaggi Eccedenti", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_OMAGGI_DEM"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph("Netto Altri Proventi", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["B_SUPPL_DEM"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph("Base x Calcolo " + (oItemBordero != null && oItemBordero.DEMDescrizione != null && !string.IsNullOrEmpty(oItemBordero.DEMDescrizione) && !string.IsNullOrWhiteSpace(oItemBordero.DEMDescrizione) ? oItemBordero.DEMDescrizione.Trim() : "DEM"), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["BASE_DEM_BORDERO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterBaseDEM.AddCell(oCell);
            }

            return oTabFooterBaseDEM;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterPageBordero(DataTable oTable, clsPdfDocument oPdfDocument, int nCurrentPageBordero, int nMaxPagesBordero)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterPageBordero = new iTextSharp.text.pdf.PdfPTable(1);
            oTabFooterPageBordero.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTabFooterPageBordero.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterPageBordero.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);

            oCell = GetDeafultCell(false);
            string cPaginaBordero = "Bordero Codice Sistema:{0} - Organizzatore:{1} {2} {3} - Codice Locale:{4} {5} - Tipo Evento:{6} {7} - Titolo:{8}      Pag. {9} di {10}";
            cPaginaBordero = string.Format(cPaginaBordero, oTable.Rows[0]["CODICE_SISTEMA"].ToString(),
                                                           oTable.Rows[0]["TIPO_CF_PI"].ToString(),
                                                           oTable.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"].ToString(),
                                                           oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString(),
                                                           oTable.Rows[0]["CODICE_LOCALE"].ToString(),
                                                           oTable.Rows[0]["DENOMINAZIONE_LOCALE"].ToString(),
                                                           oTable.Rows[0]["TIPO_EVENTO"].ToString(),
                                                           oTable.Rows[0]["DESCRIZIONE_TIPO_EVENTO"].ToString(),
                                                           oTable.Rows[0]["TITOLO_EVENTO"].ToString(),
                                                           nCurrentPageBordero.ToString(), nMaxPagesBordero.ToString());
            oP = new iTextSharp.text.Paragraph(cPaginaBordero, oFontSmall);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterPageBordero.AddCell(oCell);

            return oTabFooterPageBordero;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterPageBorderoAbbonamenti(DataTable oTable, clsPdfDocument oPdfDocument, int nCurrentPageBordero, int nMaxPagesBordero)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterPageBordero = new iTextSharp.text.pdf.PdfPTable(1);
            oTabFooterPageBordero.TotalWidth = GetWidthPrintArea(oPdfDocument);
            oTabFooterPageBordero.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterPageBordero.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);

            oCell = GetDeafultCell(false);
            string cPaginaBordero = "Bordero Abbonamenti Codice Sistema:{0} - Organizzatore:{1} {2} {3}                                                                 Pag. {4} di {5}";
            cPaginaBordero = string.Format(cPaginaBordero, oTable.Rows[0]["CODICE_SISTEMA"].ToString(),
                                                           oTable.Rows[0]["TIPO_CF_PI"].ToString(),
                                                           oTable.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"].ToString(),
                                                           oTable.Rows[0]["DENOMINAZIONE_ORGANIZZATORE"].ToString(),
                                                           nCurrentPageBordero.ToString(), nMaxPagesBordero.ToString());
            oP = new iTextSharp.text.Paragraph(cPaginaBordero, oFontSmall);
            oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
            oCell.AddElement(oP);
            oTabFooterPageBordero.AddCell(oCell);

            return oTabFooterPageBordero;
        }

        private static iTextSharp.text.pdf.PdfPTable GetFooterDEMNettissimo(DataTable oTable, clsPdfDocument oPdfDocument, clsItemBordero oItemBordero, bool asteriscoSupplementi)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabFooterDEMNettissimo = new iTextSharp.text.pdf.PdfPTable(new float[] { 18.6f, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7.8f, 11, 9.2f });
            //oTabFooterDEMNettissimo.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
            oTabFooterDEMNettissimo.TotalWidth = GetWidthPrintArea(oPdfDocument) - 3;
            oTabFooterDEMNettissimo.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabFooterDEMNettissimo.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontSmall = oPdfDocument.GetFont(FontStyle.Regular, 1);
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);
            iTextSharp.text.Font oFontAltriProventi = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 4);


            if (oTable == null)
            {
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = oTabFooterDEMNettissimo.AbsoluteWidths.Length;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph((asteriscoSupplementi ? "* biglietti con supplemento" : EmptyChar), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 4; // oTabFooterDEMNettissimo.AbsoluteWidths.Length;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col
                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);
            }
            else
            {
                List<string> descrizioniAltriProventi = new List<string>();
                if (oItemBordero.AltriProventi != null && oItemBordero.AltriProventi.Rows.Count > 0 && oItemBordero.Dati != null && oItemBordero.Dati.Rows.Count > 0
                    && oItemBordero.Dati.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"] != null
                    && oItemBordero.Dati.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"] != System.DBNull.Value
                    && oItemBordero.Dati.Rows[0]["CODICE_LOCALE"] != null
                    && oItemBordero.Dati.Rows[0]["CODICE_LOCALE"] != System.DBNull.Value
                    && oItemBordero.Dati.Rows[0]["TITOLO_EVENTO"] != null
                    && oItemBordero.Dati.Rows[0]["TITOLO_EVENTO"] != System.DBNull.Value
                    && oItemBordero.Dati.Rows[0]["TIPO_EVENTO"] != null
                    && oItemBordero.Dati.Rows[0]["TIPO_EVENTO"] != System.DBNull.Value
                    && oItemBordero.Dati.Rows[0]["INIZIO_PRIMO_SPETTACOLO"] != null
                    && oItemBordero.Dati.Rows[0]["INIZIO_ULTIMO_SPETTACOLO"] != System.DBNull.Value
                    )
                {
                    string organizzatore = oItemBordero.Dati.Rows[0]["CODICE_FISCALE_ORGANIZZATORE"].ToString();
                    string codice_locale = oItemBordero.Dati.Rows[0]["CODICE_LOCALE"].ToString();
                    string titolo_evento = oItemBordero.Dati.Rows[0]["TITOLO_EVENTO"].ToString();
                    string tipo_evento = oItemBordero.Dati.Rows[0]["TIPO_EVENTO"].ToString();
                    DateTime inizio_primo_evento = (DateTime)oItemBordero.Dati.Rows[0]["INIZIO_PRIMO_SPETTACOLO"];
                    DateTime inizio_ultimo_evento = (DateTime)oItemBordero.Dati.Rows[0]["INIZIO_ULTIMO_SPETTACOLO"];
                    foreach (DataRow row in oItemBordero.AltriProventi.Rows)
                    {
                        if (row["ORGANIZZATORE"] != null
                            && row["ORGANIZZATORE"] != System.DBNull.Value
                            && row["CODICE_LOCALE"] != null
                            && row["CODICE_LOCALE"] != System.DBNull.Value
                            && row["TITOLO"] != null
                            && row["TITOLO"] != System.DBNull.Value
                            && row["GENERE_PREVALENTE"] != null
                            && row["GENERE_PREVALENTE"] != System.DBNull.Value
                            && row["DATA_ORA"] != null
                            && row["DATA_ORA"] != System.DBNull.Value)
                        {
                            clsAltroProventoBordero altroProvento = new clsAltroProventoBordero(row);
                            if (altroProvento.ORGANIZZATORE == organizzatore
                                && altroProvento.CODICE_LOCALE == codice_locale
                                && altroProvento.TITOLO == titolo_evento
                                && altroProvento.GENERE_PREVALENTE == tipo_evento
                                && altroProvento.DATA_ORA >= inizio_primo_evento
                                && altroProvento.DATA_ORA <= inizio_ultimo_evento)
                            {
                                descrizioniAltriProventi.Add(altroProvento.ToString());
                            }
                        }
                    }
                }


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oCell.Colspan = oTabFooterDEMNettissimo.AbsoluteWidths.Length;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph((asteriscoSupplementi ? "* biglietti con supplemento" : EmptyChar), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                if (descrizioniAltriProventi.Count > 0)
                    oP = new iTextSharp.text.Paragraph("Altri proventi evento:", oFontAltriProventi);
                else
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 4; // oTabFooterDEMNettissimo.AbsoluteWidths.Length;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col (1)
                oCell = GetDeafultCell(false);
                
                oP = new iTextSharp.text.Paragraph(oItemBordero.TotAltrePrestazioni > 0 ? "Tot.senza altre prestazioni" : EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                oCell.AddElement(oP);
                oCell.Colspan = 2;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(oItemBordero != null && oItemBordero.DEMDescrizione != null && !string.IsNullOrEmpty(oItemBordero.DEMDescrizione) && !string.IsNullOrWhiteSpace(oItemBordero.DEMDescrizione) ? oItemBordero.DEMDescrizione.Trim() : "DEM", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(((decimal)(oTable.Rows[0]["DEM_BORDERO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                if (descrizioniAltriProventi.Count >= 1)
                    oP = new iTextSharp.text.Paragraph(descrizioniAltriProventi[0], oFontAltriProventi);
                else
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col (2)
                if (oItemBordero.TotAltrePrestazioni == 0)
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oCell.Colspan = 2;
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }
                else
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph("Lordo", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(oItemBordero.AltrePrestazioniLORDO_SENZA_PRESTAZIONI.ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph("Nettissimo", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                decimal nNettissimo = (decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"]) - (decimal)(oTable.Rows[0]["DEM_BORDERO"]);
                decimal nNettoBordero = (decimal)(oTable.Rows[0]["B_TOT_NETTO_BORD"]);
                oP = new iTextSharp.text.Paragraph((nNettissimo).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                if (descrizioniAltriProventi.Count >= 2)
                    oP = new iTextSharp.text.Paragraph(descrizioniAltriProventi[1], oFontAltriProventi);
                else
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col (3)
                if (oItemBordero.TotAltrePrestazioni == 0)
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oCell.Colspan = 2;
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }
                else
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph("Prest.C.", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(oItemBordero.TotAltrePrestazioni.ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph("Tot.Q (D)", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph(oItemBordero.TotaleQuadroD.ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                if (descrizioniAltriProventi.Count >= 3)
                    oP = new iTextSharp.text.Paragraph(descrizioniAltriProventi[2], oFontAltriProventi);
                else
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col (5)
                if (oItemBordero.TotAltrePrestazioni == 0)
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oCell.Colspan = 2;
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }
                else
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph("Netto", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(oItemBordero.AltrePrestazioniNETTO_SENZA_PRESTAZIONI.ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }


                oCell = GetDeafultCell(false);
                oP = new iTextSharp.text.Paragraph("Netto Tot.", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 7;
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                //oP = new iTextSharp.text.Paragraph((nNettissimo - oItemBordero.TotaleQuadroD).ToString(FormatCurrencty), oFontRegular);
                oP = new iTextSharp.text.Paragraph((System.Math.Max(0, nNettoBordero - oItemBordero.TotaleQuadroD)).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabFooterDEMNettissimo.AddCell(oCell);

                oCell = GetDeafultCell(false);
                if (descrizioniAltriProventi.Count >= 4)
                    oP = new iTextSharp.text.Paragraph(descrizioniAltriProventi[3], oFontAltriProventi);
                else
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontSmall);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oCell.Colspan = 3;
                oTabFooterDEMNettissimo.AddCell(oCell);

                // qui altre 2 col (5)
                if (oItemBordero.TotAltrePrestazioni == 0)
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oCell.Colspan = 2;
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }
                else
                {
                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph("Nettissimo", oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oP = new iTextSharp.text.Paragraph(oItemBordero.AltrePrestazioniNETTISSIMO_SENZA_PRESTAZIONI.ToString(FormatCurrencty), oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabFooterDEMNettissimo.AddCell(oCell);
                }

            }
            return oTabFooterDEMNettissimo;
        }

        private static iTextSharp.text.pdf.PdfPTable GetTabBody(DataTable oTable, clsPdfDocument oPdfDocument, float nBodyHeight)
        {
            iTextSharp.text.Paragraph oP;
            iTextSharp.text.pdf.PdfPTable oTabQuadroB = new iTextSharp.text.pdf.PdfPTable(new float[] { 32, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8 });
            oTabQuadroB.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
            oTabQuadroB.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
            oTabQuadroB.DefaultCell.BorderWidth = 1F;

            iTextSharp.text.pdf.PdfPCell oCell;
            return oTabQuadroB;
        }

        private static iTextSharp.text.pdf.PdfPTable GetTabTotaliIvaDaAssolvere(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.pdf.PdfPTable oTabTotIvaDaAssolvere = null;
            if (decimal.Parse(oTable.Rows[0]["B_TOT_CORRISPETTIVO_TITOLO"].ToString()) > 0)
            {
                DataRow oRow = oTable.Rows[0];
                iTextSharp.text.Paragraph oP;
                oTabTotIvaDaAssolvere = new iTextSharp.text.pdf.PdfPTable(new float[] { 32, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8 });
                oTabTotIvaDaAssolvere.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
                oTabTotIvaDaAssolvere.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
                oTabTotIvaDaAssolvere.DefaultCell.BorderWidth = 1F;

                iTextSharp.text.pdf.PdfPCell oCell;

                iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
                iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);


                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph("Totali IVA da Assolvere", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_NETTO_TITOLO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_CORRISPETTIVO_TITOLO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

            }

            return oTabTotIvaDaAssolvere;
        }

        private static iTextSharp.text.pdf.PdfPTable GetTabTotaliIvaPreassolta(DataTable oTable, clsPdfDocument oPdfDocument)
        {
            iTextSharp.text.pdf.PdfPTable oTabTotIvaDaAssolvere = null;
            if (decimal.Parse(oTable.Rows[0]["B_TOT_CORRISPETTIVO_FIGURATIVO"].ToString()) > 0)
            {
                DataRow oRow = oTable.Rows[0];
                iTextSharp.text.Paragraph oP;
                oTabTotIvaDaAssolvere = new iTextSharp.text.pdf.PdfPTable(new float[] { 32, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8 });
                oTabTotIvaDaAssolvere.TotalWidth = GetWidthPrintArea(oPdfDocument) - GetQuadroC(oTable, oPdfDocument).TotalWidth - 3;
                oTabTotIvaDaAssolvere.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.Black);
                oTabTotIvaDaAssolvere.DefaultCell.BorderWidth = 1F;

                iTextSharp.text.pdf.PdfPCell oCell;
                iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
                iTextSharp.text.Font oFontBold = oPdfDocument.GetFont(FontStyle.Bold, BorderoFontSize);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph("Totali IVA Preassolta", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_NETTO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabTotIvaDaAssolvere.AddCell(oCell);

            }

            return oTabTotIvaDaAssolvere;
        }

        private static void AddBody(DataRow oRow, clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oTabBody, bool asteriscoSupplementi)
        {
            AddBody(oRow, oPdfDocument, oTabBody, false, asteriscoSupplementi);
        }

        private static void FillTableHeight(clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oPdfTable, float nHeight)
        {
            FillTableHeight(oPdfDocument, oPdfTable, nHeight, iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
        }

        private static void FillTableHeight(clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oPdfTable, float nHeight, bool lOnlyBorderAround)
        {
            if (lOnlyBorderAround)
            {
                iTextSharp.text.Paragraph oP;

                iTextSharp.text.pdf.PdfPCell oCell;
                iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);

                if (oPdfTable.TotalHeight < nHeight)
                {
                    int nIndexCol = 0;
                    foreach (float nW in oPdfTable.AbsoluteWidths)
                    {
                        oCell = GetDeafultCell(false);
                        if (oPdfTable.AbsoluteWidths.Length == 1)
                        {

                        }
                        else
                        {
                            if (nIndexCol == 0)
                            {
                                oCell.Border = iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                            }
                            else if (nIndexCol == oPdfTable.AbsoluteWidths.Length - 1)
                            {
                                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
                            }
                            else
                            {
                                oCell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                            }
                        }

                        oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                        oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                        oCell.AddElement(oP);
                        oCell.FixedHeight = nHeight - oPdfTable.TotalHeight;
                        oPdfTable.AddCell(oCell);
                        nIndexCol += 1;
                    }
                }
            }
            else
            {
                FillTableHeight(oPdfDocument, oPdfTable, nHeight, iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
            }
        }

        private static void FillTableHeight(clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oPdfTable, float nHeight, int nBorder)
        {
            iTextSharp.text.Paragraph oP;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);

            if (oPdfTable.TotalHeight < nHeight)
            {
                foreach (float nW in oPdfTable.AbsoluteWidths)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = nBorder;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oCell.FixedHeight = nHeight - oPdfTable.TotalHeight;
                    oPdfTable.AddCell(oCell);
                }
            }

        }

        private static void AddBody(DataRow oRow, clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oTabBody, bool lFooterBorder, bool asteriscoSupplementi)
        {
            iTextSharp.text.Paragraph oP;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);
            iTextSharp.text.Font oFontSupplemento = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);
            iTextSharp.text.Font oFontEccedenza = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize - 2);

            if (oRow == null)
            {

                foreach (float nWidt in oTabBody.AbsoluteWidths)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);
                }
            }
            else
            {
                int nB_QTA_ANNULLATI = 0;
                int nB_QTA_EMESSI = 0;
                int nB_TOT_QTA = 0;

                bool lBorderoEmpty = (int.TryParse(oRow["B_QTA_ANNULLATI"].ToString(), out nB_QTA_ANNULLATI) &&
                                      int.TryParse(oRow["B_QTA_EMESSI"].ToString(), out nB_QTA_EMESSI) &&
                                      int.TryParse(oRow["B_TOT_QTA"].ToString(), out nB_TOT_QTA) &&
                                      nB_QTA_ANNULLATI + nB_QTA_EMESSI + nB_TOT_QTA == 0);


                bool lFlagOmaggio = (oRow["FLAG_OMAGGIO"].ToString() == "1");
                decimal nQtaOmaggiEccedenti = (decimal)oRow["QTA_OMAGGI_ECCEDENTI_IVA"];
                decimal nSupplemento = (decimal)oRow["CORRISPETTIVO_PREVENDITA"];

                string cDescPrestazioni = "";

                try
                {
                    string codicePrestazione1 = (oRow["CODICE_PRESTAZIONE1"] != null && oRow["CODICE_PRESTAZIONE1"] != System.DBNull.Value ? oRow["CODICE_PRESTAZIONE1"].ToString() : "");
                    string codicePrestazione2 = (oRow["CODICE_PRESTAZIONE2"] != null && oRow["CODICE_PRESTAZIONE2"] != System.DBNull.Value ? oRow["CODICE_PRESTAZIONE2"].ToString() : "");
                    string codicePrestazione3 = (oRow["CODICE_PRESTAZIONE3"] != null && oRow["CODICE_PRESTAZIONE3"] != System.DBNull.Value ? oRow["CODICE_PRESTAZIONE3"].ToString() : "");
                    decimal importoPrestazione1 = 0;
                    decimal importoPrestazione2 = 0;
                    decimal importoPrestazione3 = 0;

                    if (!string.IsNullOrEmpty(codicePrestazione1) && oRow["IMPORTO_PRESTAZIONE1"] != null && oRow["IMPORTO_PRESTAZIONE1"] != System.DBNull.Value && decimal.TryParse(oRow["IMPORTO_PRESTAZIONE1"].ToString(), out importoPrestazione1))
                    { }
                    else importoPrestazione1 = 0;

                    if (!string.IsNullOrEmpty(codicePrestazione2) && oRow["IMPORTO_PRESTAZIONE2"] != null && oRow["IMPORTO_PRESTAZIONE2"] != System.DBNull.Value && decimal.TryParse(oRow["IMPORTO_PRESTAZIONE2"].ToString(), out importoPrestazione2))
                    { }
                    else importoPrestazione2 = 0;

                    if (!string.IsNullOrEmpty(codicePrestazione3) && oRow["IMPORTO_PRESTAZIONE3"] != null && oRow["IMPORTO_PRESTAZIONE3"] != System.DBNull.Value && decimal.TryParse(oRow["IMPORTO_PRESTAZIONE3"].ToString(), out importoPrestazione3))
                    { }
                    else importoPrestazione3 = 0;


                    if (importoPrestazione1 + importoPrestazione2 + importoPrestazione3 > 0)
                    {
                        string cDescPrezzo = ((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["CORRISPETTIVO_TITOLO"] : oRow["CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty);
                        if (!string.IsNullOrEmpty(codicePrestazione1) && importoPrestazione1 > 0)
                            cDescPrestazioni += (string.IsNullOrEmpty(cDescPrestazioni) ? string.Format("{0} di cui ", cDescPrezzo) : " ") + string.Format("{0} {1}", codicePrestazione1, importoPrestazione1.ToString(FormatCurrencty));
                        if (!string.IsNullOrEmpty(codicePrestazione2) && importoPrestazione2 > 0)
                            cDescPrestazioni += (string.IsNullOrEmpty(cDescPrestazioni) ? string.Format("{0} di cui ", cDescPrezzo) : " ") + string.Format("{0} {1}", codicePrestazione2, importoPrestazione2.ToString(FormatCurrencty));
                        if (!string.IsNullOrEmpty(codicePrestazione3) && importoPrestazione3 > 0)
                            cDescPrestazioni += (string.IsNullOrEmpty(cDescPrestazioni) ? string.Format("{0} di cui ", cDescPrezzo) : " ") + string.Format("{0} {1}", codicePrestazione3, importoPrestazione3.ToString(FormatCurrencty));
                    }
                }
                catch (Exception)
                {
                    cDescPrestazioni = "";
                }

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(oRow["TIPO_BIGLIETTO"].ToString() + (asteriscoSupplementi && nSupplemento > 0 ? "*" : ""), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph((lBorderoEmpty ? EmptyChar : oRow["QTA_ANNULLATI"].ToString()), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                if (lFlagOmaggio && nQtaOmaggiEccedenti > 0)
                {
                    oP = new iTextSharp.text.Paragraph(((decimal)oRow["TOT_QTA"] - nQtaOmaggiEccedenti).ToString(), oFontRegular);
                }
                else
                {
                    oP = new iTextSharp.text.Paragraph((lBorderoEmpty ? EmptyChar : oRow["TOT_QTA"].ToString()), oFontRegular);
                }

                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph((lBorderoEmpty ? EmptyChar : ((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["NETTO_TITOLO"] : oRow["NETTO_FIGURATIVO"])).ToString(FormatCurrencty)), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph((lBorderoEmpty ? EmptyChar : ((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["IVA_TITOLO"] : oRow["IVA_FIGURATIVA"])).ToString(FormatCurrencty)), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph((lBorderoEmpty ? EmptyChar : ((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["CORRISPETTIVO_TITOLO"] : oRow["CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty)), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["TOT_NETTO_TITOLO"] : oRow["TOT_NETTO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["TOT_IMPOSTA_INTRATTENIMENTO"] : oRow["TOT_IMPOSTA_INTRA_FIGURATIVA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["TOT_IVA_TITOLO"] : oRow["TOT_IVA_FIGURATIVA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(false);
                oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["TOT_CORRISPETTIVO_TITOLO"] : oRow["TOT_CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                if (lFlagOmaggio && nQtaOmaggiEccedenti > 0)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(" ecc." +
                                                       " " + oRow["OMAGGIO_ORA_EVENTO_RIF"].ToString() +
                                                       " " + oRow["OMAGGIO_ORDINE_DI_POSTO_RIF"].ToString() +
                                                       " " + oRow["SETTORE_OMAGGIO_RIF"].ToString() +
                                                       " " + oRow["CAPIENZA"].ToString() +
                                                       "", oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(nQtaOmaggiEccedenti.ToString(), oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(((decimal)oRow["IVA_OMAGGI_ECCEDENTI"]).ToString(FormatCurrencty), oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(((decimal)(oRow["TITOLO_IVA_PREASSOLTA"].ToString() == "N" ? oRow["TOT_CORRISPETTIVO_TITOLO"] : oRow["TOT_CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty), oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                }
                else if (!string.IsNullOrEmpty(cDescPrestazioni))
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(cDescPrestazioni, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                    oCell = GetDeafultCell(false);
                    oCell.Border = (lFooterBorder ? iTextSharp.text.Rectangle.BOTTOM_BORDER | iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER : iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER);
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontEccedenza);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);

                }
            }
        }

        private static void AddBodyIvaDaAssolvere(DataRow oRow, clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oTabBody)
        {
            iTextSharp.text.Paragraph oP;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);

            if (oRow == null)
            {

                foreach (float nWidt in oTabBody.AbsoluteWidths)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);
                }
            }
            else
            {
                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph("Totale IVA da Assolvere", oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_NETTO_TITOLO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_IMPOSTA_INTRATTENIMENTO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_IVA_TITOLO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_CORRISPETTIVO_TITOLO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);
            }
        }

        private static void AddBodyIvaPreAssolta(DataRow oRow, clsPdfDocument oPdfDocument, iTextSharp.text.pdf.PdfPTable oTabBody)
        {
            iTextSharp.text.Paragraph oP;

            iTextSharp.text.pdf.PdfPCell oCell;
            iTextSharp.text.Font oFontRegular = oPdfDocument.GetFont(FontStyle.Regular, BorderoFontSize);

            if (oRow == null)
            {

                foreach (float nWidt in oTabBody.AbsoluteWidths)
                {
                    oCell = GetDeafultCell(false);
                    oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER;
                    oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                    oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    oCell.AddElement(oP);
                    oTabBody.AddCell(oCell);
                }
            }
            else
            {
                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph("Totale IVA Preassolta".PadLeft(15, '_').PadRight(15, '_'), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(EmptyChar, oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_NETTO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_IMPOSTA_INTRA_FIGURATIVA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_IVA_FIGURATIVA"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);

                oCell = GetDeafultCell(true);
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER;
                oP = new iTextSharp.text.Paragraph(((decimal)(oRow["B_TOT_CORRISPETTIVO_FIGURATIVO"])).ToString(FormatCurrencty), oFontRegular);
                oP.Alignment = iTextSharp.text.Element.ALIGN_RIGHT;
                oCell.AddElement(oP);
                oTabBody.AddCell(oCell);
            }
        }

        private static float GetWidthPrintArea(clsPdfDocument oPdfDocument)
        {
            return oPdfDocument.DocPdf.PageSize.Width - oPdfDocument.DocPdf.LeftMargin - oPdfDocument.DocPdf.RightMargin;
        }

        // Dimensione verticale della pagina
        private static float GetHeightPrintArea(clsPdfDocument oPdfDocument)
        {
            return oPdfDocument.DocPdf.PageSize.Height - oPdfDocument.DocPdf.TopMargin - oPdfDocument.DocPdf.BottomMargin - 20;
        }

        private static iTextSharp.text.pdf.PdfPCell GetDeafultCell(bool Borders)
        {
            //Borders = true;
            return GetDeafultCell(Borders, "", null, iTextSharp.text.Element.ALIGN_LEFT);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDefaultCell(bool Borders, string cContent, iTextSharp.text.Font oFont)
        {
            return GetDeafultCell(Borders, cContent, oFont, iTextSharp.text.Element.ALIGN_LEFT);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDefaultCell(bool Borders, string cContent, iTextSharp.text.Font oFont, int nAlign)
        {
            return GetDeafultCell(Borders, cContent, oFont, nAlign);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDeafultCell(bool Borders, string cContent, iTextSharp.text.Font oFont, int nAlign)
        {
            iTextSharp.text.pdf.PdfPCell oCell = new iTextSharp.text.pdf.PdfPCell();
            if (Borders)
            {
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            }
            else
            {
                oCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            }
            //oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            oCell.HorizontalAlignment = iTextSharp.text.pdf.PdfPCell.ALIGN_LEFT;
            oCell.VerticalAlignment = iTextSharp.text.pdf.PdfPCell.ALIGN_MIDDLE;
            oCell.PaddingTop = 0;
            oCell.PaddingBottom = 4;
            if (cContent.Trim() != "")
            {
                iTextSharp.text.Paragraph oParagraph = new iTextSharp.text.Paragraph(cContent, oFont);
                oParagraph.Alignment = nAlign;
                oParagraph.Trim();
                oCell.AddElement(oParagraph);
                oCell.Normalize();
            }
            return oCell;
        }


        #region Gestione configurazione

        public class clsBordQuotaTE
        {
            public string TE { get; set; }
            public string Descr { get; set; }
        }

        public class clsBordQuotaRiga
        {
            public long Riga { get; set; }
            public string CodiceLocale { get; set; }
            public string GiorniSettimana { get; set; }
            public long NumeroEventoPerSala { get; set; }
            public string CampoConfronto { get; set; }
            public decimal CampoConfrontoMIN { get; set; }
            public decimal CampoConfrontoMAX { get; set; }
            public string CampoCalcolo { get; set; }
            public string TipoCalcolo { get; set; }
            public decimal ValoreFisso { get; set; }
            public decimal Percentuale { get; set; }

            public clsBordQuotaRiga Clone()
            {
                clsBordQuotaRiga result = new clsBordQuotaRiga();
                result.Riga = this.Riga;
                result.CodiceLocale = this.CodiceLocale;
                result.GiorniSettimana = this.GiorniSettimana;
                result.NumeroEventoPerSala = this.NumeroEventoPerSala;
                result.CampoConfronto = this.CampoConfronto;
                result.CampoConfrontoMIN = this.CampoConfrontoMIN;
                result.CampoConfrontoMAX = this.CampoConfrontoMAX;
                result.CampoCalcolo = this.CampoCalcolo;
                result.TipoCalcolo = this.TipoCalcolo;
                result.ValoreFisso = this.ValoreFisso;
                result.Percentuale = this.Percentuale;
                return result;
            }

            public void SetFromOther(clsBordQuotaRiga riga)
            {
                this.Riga = riga.Riga;
                this.CodiceLocale = riga.CodiceLocale;
                this.GiorniSettimana = riga.GiorniSettimana;
                this.NumeroEventoPerSala = riga.NumeroEventoPerSala;
                this.CampoConfronto = riga.CampoConfronto;
                this.CampoConfrontoMIN = riga.CampoConfrontoMIN;
                this.CampoConfrontoMAX = riga.CampoConfrontoMAX;
                this.CampoCalcolo = riga.CampoCalcolo;
                this.TipoCalcolo = riga.TipoCalcolo;
                this.ValoreFisso = riga.ValoreFisso;
                this.Percentuale = riga.Percentuale;
            }
        }

        public class clsBordQuotaRigaDEM
        {
            public long Riga { get; set; }
            public string FlagLordoNetto { get; set; }
            public decimal PercentualeIncasso { get; set; }
            public decimal PercentualeDEM { get; set; }
            public decimal PercentualeSupplemento { get; set; }
            public long FlgSupplDemEcc { get; set; } = 1;
            public decimal PercentualeOmaggi { get; set; }
            public decimal ValoreMinimo { get; set; }
            public bool FlagScontoP10 { get; set; }
            //public string FlagsUlterioriFiltri { get; set; }
            public DateTime DataEvento { get; set; }
            //public bool FlagDataEvento { get; set; }
            public string TitoloEvento { get; set; }
            //public bool FlagTitoloEvento { get; set; }
            public string CodiceLocale { get; set; }
            public bool FlagCodiceLocale { get; set; }
            public long IdSala { get; set; }
            public string DescrCodiceLocale { get; set; }
            public DateTime DataOraEvento { get; set; }
            public bool FlagEventoSpecifico { get; set; }


        }

        public class clsBordQuota
        {
            public long IdQuota { get; set; }
            public bool IsQuotaDEM { get; set; }
            public string Descr { get; set; }
            public string SpettIntra { get; set; }
            public List<clsBordQuotaTE> TipiEvento { get; set; }
            public List<clsBordQuotaRiga> Righe { get; set; }
            public List<clsBordQuotaRigaDEM> RigheDEM { get; set; }

            public bool Opzionale { get; set; }

            public clsBordQuota Clone()
            {
                clsBordQuota result = new clsBordQuota();
                result.IdQuota = this.IdQuota;
                result.IsQuotaDEM = this.IsQuotaDEM;
                result.Descr = this.Descr;
                result.SpettIntra = this.SpettIntra;
                if (this.TipiEvento == null)
                    result.TipiEvento = null;
                else
                    result.TipiEvento = new List<clsBordQuotaTE>(this.TipiEvento);
                if (this.Righe == null)
                    result.Righe = null;
                else
                    result.Righe = new List<clsBordQuotaRiga>(this.Righe);
                if (this.RigheDEM == null)
                    result.RigheDEM = null;
                else
                    result.RigheDEM = new List<clsBordQuotaRigaDEM>(this.RigheDEM);
                result.Opzionale = this.Opzionale;
                return result;
            }
        }

        public class clsDescriptorField
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }

        public class clsBordQuoteConfig
        {
            public bool TitoloUnico { get; set; }
            public bool BorderoPerSpettacolo { get; set; }
            public long OraInizioEventi { get; set; }
            public List<clsBordQuota> QuoteDEM { get; set; }
            public List<clsBordQuota> AltreQuote { get; set; }

            public List<clsDescriptorField> DescrCodiciLocali { get; set; }
            public List<clsDescriptorField> DescrCampiBordero { get; set; }
            public List<clsDescriptorField> DescrTipiEvento { get; set; }
            public bool AsteriscoSupplementi { get; set; }

            public clsBordQuoteConfig()
            {
                this.AsteriscoSupplementi = true;
            }

            public clsBordQuoteConfig Clone()
            {
                clsBordQuoteConfig result = new clsBordQuoteConfig();
                result.TitoloUnico = this.TitoloUnico;
                result.BorderoPerSpettacolo = this.BorderoPerSpettacolo;
                result.OraInizioEventi = this.OraInizioEventi;
                result.AsteriscoSupplementi = this.AsteriscoSupplementi;
                if (this.QuoteDEM == null)
                    result.QuoteDEM = null;
                else
                {
                    result.QuoteDEM = new List<clsBordQuota>();
                    foreach (clsBordQuota quota in this.QuoteDEM)
                        result.QuoteDEM.Add(quota.Clone());
                }
                if (this.AltreQuote == null)
                    result.AltreQuote = null;
                else
                {
                    result.AltreQuote = new List<clsBordQuota>();
                    foreach (clsBordQuota quota in this.AltreQuote)
                        result.AltreQuote.Add(quota.Clone());
                }
                result.DescrCodiciLocali = new List<clsDescriptorField>(this.DescrCodiciLocali);
                result.DescrCampiBordero = new List<clsDescriptorField>(this.DescrCampiBordero);
                result.DescrTipiEvento = new List<clsDescriptorField>(this.DescrTipiEvento);
                return result;
            }
        }

        public static clsBordQuoteConfig GetConfigurazione(IConnection Connection, long IdUxc)
        {
            clsBordQuoteConfig Configurazione = null;

            try
            {
                StringBuilder oSB;
                clsParameters oPars;
                IRecordSet oRS_Proprieta;
                IRecordSet oRS_Quote;
                IRecordSet oRS_TE;
                IRecordSet oRS_DEM;
                IRecordSet oRS_Altro;



                oSB = new StringBuilder("SELECT * FROM BORD_PROPRIETA WHERE IDUXC = :pIDUXC AND PROPRIETA IS NOT NULL AND VALORE IS NOT NULL");
                oPars = new clsParameters(":pIDUXC", IdUxc);
                oRS_Proprieta = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                if (!oRS_Proprieta.EOF)
                {
                    Configurazione = new clsBordQuoteConfig();
                    Configurazione.TitoloUnico = false;
                    Configurazione.BorderoPerSpettacolo = false;
                    Configurazione.OraInizioEventi = 6;
                    Configurazione.QuoteDEM = null;
                    Configurazione.AltreQuote = null;
                    while (!oRS_Proprieta.EOF)
                    {
                        string proprieta = oRS_Proprieta.Fields("PROPRIETA").Value.ToString();
                        string valore = oRS_Proprieta.Fields("VALORE").Value.ToString();
                        switch (proprieta)
                        {
                            case "TITOLO_UNICO":
                                {
                                    Configurazione.TitoloUnico = (valore.Trim().ToUpper() == "ABILITATO");
                                    break;
                                }
                            case "BORDERO_PER_SPETTACOLO":
                                {
                                    Configurazione.BorderoPerSpettacolo = (valore.Trim().ToUpper() == "ABILITATO");
                                    break;
                                }
                            case "ORA_INIZIO_SPETTACOLI":
                                {
                                    long nOre = 0;
                                    if (long.TryParse(valore, out nOre))
                                    {
                                        Configurazione.OraInizioEventi = nOre;
                                    }
                                    break;
                                }
                            case "ASTERISCO_SUPPLEMENTI":
                                {
                                    if (valore.Trim().ToUpper() != "ABILITATO")
                                        Configurazione.AsteriscoSupplementi = false;
                                    else
                                        Configurazione.AsteriscoSupplementi = true;
                                    break;
                                }
                        }
                        oRS_Proprieta.MoveNext();
                    }
                }

                oRS_Proprieta.Close();

                if (Configurazione != null)
                {

                    oSB = new StringBuilder();
                    oSB.Append("SELECT");
                    oSB.Append(" VR_BORD_QUOTE.IDQUOTA,");
                    oSB.Append(" VR_BORD_QUOTE.DESCRIZIONE,");
                    oSB.Append(" NVL(VR_BORD_QUOTE_SPETINTRA.SPETTACOLO_INTRATTENIMENTO, 'X') AS SPETTACOLO_INTRATTENIMENTO ");
                    oSB.Append(" FROM");
                    oSB.Append(" VR_BORD_QUOTE, VR_BORD_QUOTE_SPETINTRA");
                    oSB.Append(" WHERE");
                    oSB.Append(" VR_BORD_QUOTE.IDUXC = :pIDUXC");
                    oSB.Append(" AND");
                    oSB.Append(" VR_BORD_QUOTE.IDQUOTA = VR_BORD_QUOTE_SPETINTRA.IDQUOTA(+)");
                    oSB.Append(" ORDER BY ");
                    oSB.Append(" VR_BORD_QUOTE.IDQUOTA");
                    oPars = new clsParameters(":pIDUXC", IdUxc);
                    oRS_Quote = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                    oSB = new StringBuilder();
                    oSB.Append("SELECT * FROM BORD_QUOTE_DEM WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_DEM.IDQUOTA) ORDER BY BORD_QUOTE_DEM.IDQUOTA");
                    oPars = new clsParameters(":pIDUXC", IdUxc);
                    oRS_DEM = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                    oSB = new StringBuilder();
                    oSB.Append("SELECT * FROM BORD_QUOTE_ALTRO WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_ALTRO.IDQUOTA) ORDER BY BORD_QUOTE_ALTRO.IDQUOTA, BORD_QUOTE_ALTRO.RIGA_QUOTA");
                    oPars = new clsParameters(":pIDUXC", IdUxc);
                    oRS_Altro = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                    oSB = new StringBuilder();
                    oSB.Append("SELECT BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.TIPO_EVENTO, VR_TIPOEVENTO.DESCRIZIONE FROM BORD_QUOTE_TE, VR_TIPOEVENTO WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_TE.IDQUOTA) AND BORD_QUOTE_TE.TIPO_EVENTO = VR_TIPOEVENTO.CODICE ORDER BY BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.TIPO_EVENTO");
                    oPars = new clsParameters(":pIDUXC", IdUxc);
                    oRS_TE = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                    while (!oRS_Quote.EOF)
                    {
                        clsBordQuota oQuota = new clsBordQuota();

                        oQuota.IdQuota = long.Parse(oRS_Quote.Fields("IDQUOTA").Value.ToString());
                        oQuota.Descr = oRS_Quote.Fields("DESCRIZIONE").Value.ToString();
                        oQuota.SpettIntra = oRS_Quote.Fields("SPETTACOLO_INTRATTENIMENTO").Value.ToString();
                        oQuota.Righe = new List<clsBordQuotaRiga>();
                        oQuota.RigheDEM = new List<clsBordQuotaRigaDEM>();
                        oQuota.TipiEvento = new List<clsBordQuotaTE>();

                        oRS_TE.MoveFirst();
                        while (!oRS_TE.EOF)
                        {
                            if (long.Parse(oRS_TE.Fields("IDQUOTA").Value.ToString()) == oQuota.IdQuota)
                            {
                                clsBordQuotaTE oBordQuotaTe = new clsBordQuotaTE();
                                oBordQuotaTe.TE = oRS_TE.Fields("TIPO_EVENTO").Value.ToString();
                                oBordQuotaTe.Descr = oRS_TE.Fields("DESCRIZIONE").Value.ToString();
                                oQuota.TipiEvento.Add(oBordQuotaTe);
                            }
                            oRS_TE.MoveNext();
                        }

                        oRS_DEM.MoveFirst();
                        while (!oRS_DEM.EOF)
                        {
                            if (long.Parse(oRS_DEM.Fields("IDQUOTA").Value.ToString()) == oQuota.IdQuota)
                            {
                                oQuota.IsQuotaDEM = true;
                                if (oQuota.RigheDEM == null) oQuota.RigheDEM = new List<clsBordQuotaRigaDEM>();
                                clsBordQuotaRigaDEM oRigaDem = new clsBordQuotaRigaDEM();
                                oRigaDem.Riga = oQuota.RigheDEM.Count + 1;
                                oRigaDem.FlagLordoNetto = (!oRS_DEM.Fields("FLG_LORDO_NETTO").IsNull ? oRS_DEM.Fields("FLG_LORDO_NETTO").Value.ToString() : "N");
                                oRigaDem.PercentualeIncasso = (!oRS_DEM.Fields("PERC_INCASSO").IsNull ? decimal.Parse(oRS_DEM.Fields("PERC_INCASSO").Value.ToString()) : 100);
                                oRigaDem.PercentualeDEM = (!oRS_DEM.Fields("PERC_DEM").IsNull ? decimal.Parse(oRS_DEM.Fields("PERC_DEM").Value.ToString()) : 2);
                                oRigaDem.PercentualeSupplemento = (!oRS_DEM.Fields("PERC_SUPPL_DEM").IsNull ? decimal.Parse(oRS_DEM.Fields("PERC_SUPPL_DEM").Value.ToString()) : 15);
                                oRigaDem.FlgSupplDemEcc = (!oRS_DEM.Fields("FLG_SUPPL_DEM_ECC").IsNull ? long.Parse(oRS_DEM.Fields("FLG_SUPPL_DEM_ECC").Value.ToString()) : 1);
                                oRigaDem.PercentualeOmaggi = (!oRS_DEM.Fields("PERC_OMAGGI_DEM").IsNull ? decimal.Parse(oRS_DEM.Fields("PERC_OMAGGI_DEM").Value.ToString()) : 20);
                                oRigaDem.ValoreMinimo = (!oRS_DEM.Fields("VAL_MINIMO").IsNull ? decimal.Parse(oRS_DEM.Fields("VAL_MINIMO").Value.ToString()) : 0);
                                oRigaDem.FlagScontoP10 = (!oRS_DEM.Fields("FLG_P10SCONTO").IsNull ? long.Parse(oRS_DEM.Fields("FLG_P10SCONTO").Value.ToString()) == 1 : false);
                                oRigaDem.DataEvento = (!oRS_DEM.Fields("DATA_EVENTO").IsNull ? ((DateTime)oRS_DEM.Fields("DATA_EVENTO").Value).Date : DateTime.MinValue);
                                oRigaDem.DataOraEvento = (!oRS_DEM.Fields("DATA_ORA_EVENTO").IsNull ? (DateTime)oRS_DEM.Fields("DATA_ORA_EVENTO").Value : DateTime.MinValue);
                                oRigaDem.TitoloEvento = (!oRS_DEM.Fields("TITOLO_EVENTO").IsNull ? oRS_DEM.Fields("TITOLO_EVENTO").Value.ToString() : "");
                                oRigaDem.CodiceLocale = (!oRS_DEM.Fields("CODICE_LOCALE").IsNull ? oRS_DEM.Fields("CODICE_LOCALE").Value.ToString() : "");
                                oRigaDem.IdSala = (!oRS_DEM.Fields("IDSALA").IsNull ? long.Parse(oRS_DEM.Fields("IDSALA").Value.ToString()) : 0);

                                oRigaDem.FlagCodiceLocale = oRigaDem.CodiceLocale != "";
                                //oRigaDem.FlagTitoloEvento = oRigaDem.TitoloEvento != "";
                                //oRigaDem.FlagDataEvento = oRigaDem.DataEvento != DateTime.MinValue;
                                oRigaDem.FlagEventoSpecifico = oRigaDem.DataOraEvento != DateTime.MinValue && oRigaDem.TitoloEvento != "" && oRigaDem.CodiceLocale != "";

                                if (oRigaDem.FlagEventoSpecifico)
                                {
                                    oRigaDem.FlagCodiceLocale = false;
                                    //oRigaDem.FlagTitoloEvento = false;
                                    //oRigaDem.FlagDataEvento = false;
                                }

                                oQuota.RigheDEM.Add(oRigaDem);
                            }
                            oRS_DEM.MoveNext();
                        }

                        if (oQuota.IsQuotaDEM)
                        {
                            if (Configurazione.QuoteDEM == null) Configurazione.QuoteDEM = new List<clsBordQuota>();
                            Configurazione.QuoteDEM.Add(oQuota);
                        }
                        else
                        {
                            oRS_Altro.MoveFirst();
                            while (!oRS_Altro.EOF)
                            {
                                if (long.Parse(oRS_Altro.Fields("IDQUOTA").Value.ToString()) == oQuota.IdQuota)
                                {
                                    clsBordQuotaRiga oRiga = new clsBordQuotaRiga();
                                    oRiga.Riga = long.Parse(oRS_Altro.Fields("RIGA_QUOTA").Value.ToString());
                                    oRiga.CodiceLocale = (!oRS_Altro.Fields("CODICE_LOCALE").IsNull ? oRS_Altro.Fields("CODICE_LOCALE").Value.ToString() : "");
                                    oRiga.GiorniSettimana = (!oRS_Altro.Fields("GIORNI_SETTIMANA").IsNull ? oRS_Altro.Fields("GIORNI_SETTIMANA").Value.ToString() : "11111110");
                                    oRiga.NumeroEventoPerSala = (!oRS_Altro.Fields("NUMERO_EVENTO_PER_SALA").IsNull ? long.Parse(oRS_Altro.Fields("NUMERO_EVENTO_PER_SALA").Value.ToString()) : 0);
                                    oRiga.CampoConfronto = (!oRS_Altro.Fields("CAMPO_CONFRONTO").IsNull ? oRS_Altro.Fields("CAMPO_CONFRONTO").Value.ToString() : "");
                                    oRiga.CampoConfrontoMIN = (!oRS_Altro.Fields("CAMPO_CONFRONTO_MIN").IsNull ? decimal.Parse(oRS_Altro.Fields("CAMPO_CONFRONTO_MIN").Value.ToString()) : 0);
                                    oRiga.CampoConfrontoMAX = (!oRS_Altro.Fields("CAMPO_CONFRONTO_MAX").IsNull ? decimal.Parse(oRS_Altro.Fields("CAMPO_CONFRONTO_MAX").Value.ToString()) : 0);
                                    oRiga.CampoCalcolo = (!oRS_Altro.Fields("CAMPO_CALCOLO").IsNull ? oRS_Altro.Fields("CAMPO_CALCOLO").Value.ToString() : "");
                                    oRiga.TipoCalcolo = (!oRS_Altro.Fields("TIPO_CALCOLO").IsNull ? oRS_Altro.Fields("TIPO_CALCOLO").Value.ToString() : ""); ;
                                    oRiga.Percentuale = 0;
                                    oRiga.ValoreFisso = 0;
                                    if (oRiga.TipoCalcolo == "P")
                                    {
                                        if (oQuota.Righe == null) oQuota.Righe = new List<clsBordQuotaRiga>();
                                        oRiga.Percentuale = (!oRS_Altro.Fields("PERC_CAMPO_CALCOLO").IsNull ? decimal.Parse(oRS_Altro.Fields("PERC_CAMPO_CALCOLO").Value.ToString()) : 0);
                                        oQuota.Righe.Add(oRiga);
                                    }
                                    else if (oRiga.TipoCalcolo == "F")
                                    {
                                        if (oQuota.Righe == null) oQuota.Righe = new List<clsBordQuotaRiga>();
                                        oRiga.ValoreFisso = (!oRS_Altro.Fields("VALORE_FISSO").IsNull ? decimal.Parse(oRS_Altro.Fields("VALORE_FISSO").Value.ToString()) : 0);
                                        oQuota.Righe.Add(oRiga);
                                    }
                                }
                                oRS_Altro.MoveNext();
                            }
                            if (Configurazione.AltreQuote == null) Configurazione.AltreQuote = new List<clsBordQuota>();
                            //try
                            //{
                            //    IRecordSet oRSOpzionale = (IRecordSet)Connection.ExecuteQuery("SELECT IDQUOTA FROM BORD_QUOTE_OPZIONALI WHERE IDQUOTA = :pIDQUOTA", new clsParameters(":pIDQUOTA", oQuota.IdQuota));
                            //    oQuota.Opzionale = (!oRSOpzionale.EOF);
                            //    oRSOpzionale.Close();
                            //}
                            //catch (Exception)
                            //{


                            //}
                            Configurazione.AltreQuote.Add(oQuota);
                        }

                        oRS_Quote.MoveNext();
                    }

                    oRS_DEM.Close();
                    oRS_Altro.Close();
                    oRS_Quote.Close();


                    IRecordSet oRSDescriptions = null;

                    Configurazione.DescrCodiciLocali = new List<clsDescriptorField>();
                    Configurazione.DescrCampiBordero = new List<clsDescriptorField>();
                    Configurazione.DescrTipiEvento = new List<clsDescriptorField>();

                    oSB = new StringBuilder();
                    oSB.Append("SELECT CODICE_LOCALE AS CODICE, TRIM(NVL(DESCR_SALA,'')|| ' ' || NVL(ALIAS,'')) AS DESCRIZIONE FROM VR_BORD_CL WHERE IDUXC = :pIDUXC");
                    oPars = new clsParameters(":pIDUXC", IdUxc);
                    oRSDescriptions = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                    while (!oRSDescriptions.EOF)
                    {
                        clsDescriptorField item = new clsDescriptorField();
                        item.Key = oRSDescriptions.Fields("CODICE").Value.ToString();
                        item.Value = oRSDescriptions.Fields("DESCRIZIONE").Value.ToString();
                        Configurazione.DescrCodiciLocali.Add(item);
                        oRSDescriptions.MoveNext();
                    }
                    oRSDescriptions.Close();

                    oSB = new StringBuilder();
                    oSB.Append("SELECT NOME_CAMPO AS CODICE, DESCRIZIONE FROM VR_TIPI_TOTALI_BORDERO");
                    oRSDescriptions = (IRecordSet)Connection.ExecuteQuery(oSB.ToString());
                    while (!oRSDescriptions.EOF)
                    {
                        clsDescriptorField item = new clsDescriptorField();
                        item.Key = oRSDescriptions.Fields("CODICE").Value.ToString();
                        item.Value = oRSDescriptions.Fields("DESCRIZIONE").Value.ToString();
                        Configurazione.DescrCampiBordero.Add(item);
                        oRSDescriptions.MoveNext();
                    }
                    oRSDescriptions.Close();

                    oSB = new StringBuilder();
                    oSB.Append("SELECT CODICE, DESCRIZIONE FROM VR_TIPOEVENTO");
                    oRSDescriptions = (IRecordSet)Connection.ExecuteQuery(oSB.ToString());
                    while (!oRSDescriptions.EOF)
                    {
                        clsDescriptorField item = new clsDescriptorField();
                        item.Key = oRSDescriptions.Fields("CODICE").Value.ToString();
                        item.Value = oRSDescriptions.Fields("DESCRIZIONE").Value.ToString();
                        Configurazione.DescrTipiEvento.Add(item);
                        oRSDescriptions.MoveNext();
                    }
                    oRSDescriptions.Close();
                }
            }
            catch (Exception)
            {
            }

            return Configurazione;
        }

        public static clsBordQuoteConfig SetConfigurazione(IConnection Connection, long IdUxc, clsBordQuoteConfig dataConfigToSave, out Exception oError)
        {
            clsBordQuoteConfig Configurazione = null;
            bool lRet = false;

            clsBordQuoteConfig dataConfig = dataConfigToSave.Clone();

            oError = null;
            Connection.BeginTransaction();
            StringBuilder oSB;
            clsParameters oPars;

            try
            {
                IRecordSet oRS_Proprieta;
                IRecordSet oRS_Quote;
                IRecordSet oRS_TE;
                IRecordSet oRS_DEM;
                IRecordSet oRS_Altro;

                Dictionary<string, string> listaProprieta = new Dictionary<string, string>();
                listaProprieta.Add("TITOLO_UNICO", (dataConfig.TitoloUnico ? "ABILITATO" : "DISABILITATO"));
                listaProprieta.Add("BORDERO_PER_SPETTACOLO", (dataConfig.BorderoPerSpettacolo ? "ABILITATO" : "DISABILITATO"));
                listaProprieta.Add("ORA_INIZIO_SPETTACOLI", dataConfig.OraInizioEventi.ToString());
                listaProprieta.Add("ASTERISCO_SUPPLEMENTI", (dataConfig.AsteriscoSupplementi ? "ABILITATO" : "DISABILITATO"));

                foreach (KeyValuePair<string, string> itemProprieta in listaProprieta)
                {
                    string cProprieta = itemProprieta.Key;
                    string cValue = "";

                    switch (cProprieta)
                    {
                        case "TITOLO_UNICO":
                            {
                                cValue = (dataConfig.TitoloUnico ? "ABILITATO" : "DISABILITATO");
                                break;
                            }
                        case "BORDERO_PER_SPETTACOLO":
                            {
                                cValue = (dataConfig.BorderoPerSpettacolo ? "ABILITATO" : "DISABILITATO");
                                break;
                            }
                        case "ORA_INIZIO_SPETTACOLI":
                            {
                                cValue = dataConfig.OraInizioEventi.ToString();
                                break;
                            }
                        case "ASTERISCO_SUPPLEMENTI":
                            {
                                cValue = (dataConfig.AsteriscoSupplementi ? "ABILITATO" : "DISABILITATO");
                                break;
                            }
                    }

                    oSB = new StringBuilder("SELECT * FROM BORD_PROPRIETA WHERE IDUXC = :pIDUXC AND PROPRIETA = :pPROPRIETA");
                    oPars = new clsParameters();
                    oPars.Add(":pIDUXC", IdUxc);
                    oPars.Add(":pPROPRIETA", cProprieta);
                    oRS_Proprieta = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                    if (oRS_Proprieta.EOF)
                    {
                        oSB = new StringBuilder();
                        oSB.Append("INSERT INTO BORD_PROPRIETA (IDUXC, PROPRIETA, VALORE) VALUES (:pIDUXC, :pPROPRIETA, :pVALORE)");
                        oPars = new clsParameters(":pIDUXC", IdUxc);
                        oPars.Add(":pIDUXC", IdUxc);
                        oPars.Add(":pPROPRIETA", cProprieta);
                        oPars.Add(":pVALORE", cValue);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                    }
                    else if (oRS_Proprieta.Fields("VALORE").IsNull || oRS_Proprieta.Fields("VALORE").Value.ToString() != cValue)
                    {
                        oSB = new StringBuilder();
                        oSB.Append("UPDATE BORD_PROPRIETA SET VALORE = :pVALORE WHERE IDUXC = :pIDUXC AND PROPRIETA = :pPROPRIETA");
                        oPars.Add(":pIDUXC", IdUxc);
                        oPars.Add(":pPROPRIETA", cProprieta);
                        oPars.Add(":pVALORE", cValue);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                    }

                    oRS_Proprieta.Close();
                }


                oSB = new StringBuilder();
                oSB.Append("SELECT");
                oSB.Append(" VR_BORD_QUOTE.IDQUOTA,");
                oSB.Append(" VR_BORD_QUOTE.DESCRIZIONE,");
                oSB.Append(" NVL(VR_BORD_QUOTE_SPETINTRA.SPETTACOLO_INTRATTENIMENTO, 'X') AS SPETTACOLO_INTRATTENIMENTO ");
                oSB.Append(" FROM");
                oSB.Append(" VR_BORD_QUOTE, VR_BORD_QUOTE_SPETINTRA");
                oSB.Append(" WHERE");
                oSB.Append(" VR_BORD_QUOTE.IDUXC = :pIDUXC");
                oSB.Append(" AND");
                oSB.Append(" VR_BORD_QUOTE.IDQUOTA = VR_BORD_QUOTE_SPETINTRA.IDQUOTA(+)");
                oSB.Append(" ORDER BY ");
                oSB.Append(" VR_BORD_QUOTE.IDQUOTA");
                oPars = new clsParameters(":pIDUXC", IdUxc);
                oRS_Quote = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                oSB = new StringBuilder();
                oSB.Append("SELECT * FROM BORD_QUOTE_DEM WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_DEM.IDQUOTA) ORDER BY BORD_QUOTE_DEM.IDQUOTA");
                oPars = new clsParameters(":pIDUXC", IdUxc);
                oRS_DEM = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                oSB = new StringBuilder();
                oSB.Append("SELECT * FROM BORD_QUOTE_ALTRO WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_ALTRO.IDQUOTA) ORDER BY BORD_QUOTE_ALTRO.IDQUOTA, BORD_QUOTE_ALTRO.RIGA_QUOTA");
                oPars = new clsParameters(":pIDUXC", IdUxc);
                oRS_Altro = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                oSB = new StringBuilder();
                oSB.Append("SELECT BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.TIPO_EVENTO, VR_TIPOEVENTO.DESCRIZIONE FROM BORD_QUOTE_TE, VR_TIPOEVENTO WHERE EXISTS (SELECT NULL FROM BORD_QUOTE WHERE IDUXC = :pIDUXC AND BORD_QUOTE.IDQUOTA = BORD_QUOTE_TE.IDQUOTA) AND BORD_QUOTE_TE.TIPO_EVENTO = VR_TIPOEVENTO.CODICE ORDER BY BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.IDQUOTA, BORD_QUOTE_TE.TIPO_EVENTO");
                oPars = new clsParameters(":pIDUXC", IdUxc);
                oRS_TE = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);

                List<long> quoteDaCancellare = new List<long>();

                oRS_Quote.MoveFirst();
                while (!oRS_Quote.EOF)
                {
                    long idQuota = long.Parse(oRS_Quote.Fields("IDQUOTA").Value.ToString());
                    bool lFind = false;
                    if (dataConfig.QuoteDEM != null)
                    {
                        foreach (clsBordQuota quota in dataConfig.QuoteDEM)
                        {
                            if (quota.IdQuota == idQuota)
                            {
                                lFind = true;
                                break;
                            }
                        }
                    }
                    if (!lFind)
                    {
                        if (dataConfig.AltreQuote != null)
                        {
                            foreach (clsBordQuota quota in dataConfig.AltreQuote)
                            {
                                if (quota.IdQuota == idQuota)
                                {
                                    lFind = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!lFind)
                    {
                        quoteDaCancellare.Add(idQuota);
                    }
                    oRS_Quote.MoveNext();
                }

                // Eliminazione
                List<string> tabelleDaCancellare = new List<string>() { "BORD_QUOTE_TE", "BORD_QUOTE_SPETINTRA", "BORD_QUOTE_DEM", "BORD_QUOTE_ALTRO", "BORD_QUOTE" };
                foreach (long idQuotaDaEliminare in quoteDaCancellare)
                {
                    foreach (string nomeTabella in tabelleDaCancellare)
                    {
                        oSB = new StringBuilder();
                        oSB.Append(string.Format("DELETE FROM {0} WHERE IDQUOTA = :pIDQUOTA", nomeTabella));
                        oPars = new clsParameters();
                        oPars.Add(":pIDQUOTA", idQuotaDaEliminare);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                    }
                }

                if (dataConfig.QuoteDEM != null)
                {
                    foreach (clsBordQuota oQuota in dataConfig.QuoteDEM)
                    {
                        if (oQuota.IdQuota > 0)
                        {
                            oRS_Quote.MoveFirst();
                            while (!oRS_Quote.EOF)
                            {
                                if (long.Parse(oRS_Quote.Fields("IDQUOTA").Value.ToString()) == oQuota.IdQuota)
                                {
                                    if (oRS_Quote.Fields("DESCRIZIONE").Value.ToString() != oQuota.Descr)
                                    {
                                        oSB = new StringBuilder();
                                        oSB.Append("UPDATE BORD_QUOTE SET DESCRIZIONE = :pDESCRIZIONE WHERE IDQUOTA = :pIDQUOTA");
                                        oPars = new clsParameters();
                                        oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                                        oPars.Add(":pDESCRIZIONE", oQuota.Descr);
                                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                                    }
                                }
                                oRS_Quote.MoveNext();
                            }

                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM BORD_QUOTE_DEM WHERE IDQUOTA = :pIDQUOTA");
                            oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);


                        }
                        else
                        {
                            // insert
                            IRecordSet oRSNewIdQuota = (IRecordSet)Connection.ExecuteQuery("SELECT SEQ_QUOTE_BORD.NEXTVAL AS IDQUOTA FROM DUAL");
                            oQuota.IdQuota = long.Parse(oRSNewIdQuota.Fields("IDQUOTA").Value.ToString());
                            oRSNewIdQuota.Close();
                            oSB = new StringBuilder();
                            oPars = new clsParameters();
                            oSB.Append("INSERT INTO BORD_QUOTE");
                            oSB.Append(" (IDUXC, IDQUOTA, DESCRIZIONE)");
                            oSB.Append(" VALUES");
                            oSB.Append(" (:pIDUXC, :pIDQUOTA, :pDESCRIZIONE)");
                            oPars.Add(":pIDUXC", IdUxc);
                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pDESCRIZIONE", oQuota.Descr);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }

                        // inserimento righe
                        foreach (clsBordQuotaRigaDEM oRiga in oQuota.RigheDEM)
                        {
                            oSB = new StringBuilder();
                            oPars = new clsParameters();
                            oSB.Append("INSERT INTO BORD_QUOTE_DEM ");
                            oSB.Append(" (IDQUOTA, FLG_LORDO_NETTO, PERC_INCASSO, PERC_DEM, PERC_SUPPL_DEM, FLG_SUPPL_DEM_ECC, PERC_OMAGGI_DEM, VAL_MINIMO, FLG_P10SCONTO, DATA_ORA_EVENTO, DATA_EVENTO, TITOLO_EVENTO, CODICE_LOCALE, IDSALA)");
                            oSB.Append(" VALUES");
                            oSB.Append(" (:pIDQUOTA, :pFLG_LORDO_NETTO, :pPERC_INCASSO, :pPERC_DEM, :pPERC_SUPPL_DEM, :pFLG_SUPPL_DEM_ECC, :pPERC_OMAGGI_DEM, :pVAL_MINIMO, :pFLG_P10SCONTO,");


                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pFLG_LORDO_NETTO", oRiga.FlagLordoNetto);
                            oPars.Add(":pPERC_INCASSO", oRiga.PercentualeIncasso);
                            oPars.Add(":pPERC_DEM", oRiga.PercentualeDEM);
                            oPars.Add(":pPERC_SUPPL_DEM", oRiga.PercentualeSupplemento);
                            oPars.Add(":pFLG_SUPPL_DEM_ECC", oRiga.FlgSupplDemEcc);
                            oPars.Add(":pPERC_OMAGGI_DEM", oRiga.PercentualeOmaggi);
                            oPars.Add(":pVAL_MINIMO", oRiga.ValoreMinimo);
                            oPars.Add(":pFLG_P10SCONTO", (oRiga.FlagScontoP10 ? 1 : 0));

                            if (oRiga.FlagEventoSpecifico &&
                                oRiga.DataOraEvento != DateTime.MinValue &&
                                oRiga.TitoloEvento != null && oRiga.TitoloEvento != "" &&
                                oRiga.CodiceLocale != null && oRiga.CodiceLocale != "")
                            {
                                oSB.Append(" :pDATA_ORA_EVENTO, NULL, :pTITOLO_EVENTO, :pCODICE_LOCALE,");
                                oPars.Add(":pDATA_ORA_EVENTO", oRiga.DataOraEvento);
                                oPars.Add(":pTITOLO_EVENTO", oRiga.TitoloEvento);
                                oPars.Add(":pCODICE_LOCALE", oRiga.CodiceLocale);
                            }
                            else
                            {
                                if (oRiga.DataOraEvento != DateTime.MinValue)
                                {
                                    oPars.Add(":pDATA_ORA_EVENTO", oRiga.DataOraEvento);
                                    oSB.Append(" :pDATA_ORA_EVENTO, ");
                                }
                                else
                                    oSB.Append(" NULL,");

                                if (oRiga.DataEvento != DateTime.MinValue)
                                {
                                    oPars.Add(":pDATA_EVENTO", oRiga.DataEvento);
                                    oSB.Append(" :pDATA_EVENTO, ");
                                }
                                else
                                    oSB.Append(" NULL,");

                                if (oRiga.TitoloEvento != null && !string.IsNullOrEmpty(oRiga.TitoloEvento) && !string.IsNullOrWhiteSpace(oRiga.TitoloEvento) && oRiga.TitoloEvento != "")
                                {
                                    oPars.Add(":pTITOLO_EVENTO", oRiga.TitoloEvento);
                                    oSB.Append(" :pTITOLO_EVENTO,");
                                }
                                else
                                    oSB.Append(" NULL,");

                                if (oRiga.FlagCodiceLocale && oRiga.CodiceLocale != "")
                                {
                                    oPars.Add(":pCODICE_LOCALE", oRiga.CodiceLocale);
                                    oSB.Append(" :pCODICE_LOCALE,");
                                }
                                else
                                    oSB.Append(" NULL,");
                            }



                            if (oRiga.IdSala > 0)
                                oSB.Append(" :pIDSALA)");
                            else
                                oSB.Append(" NULL)");

                            if (oRiga.IdSala > 0)
                                oPars.Add(":pIDSALA", oRiga.IdSala);

                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }


                        oSB = new StringBuilder();
                        oSB.Append("DELETE FROM BORD_QUOTE_TE WHERE IDQUOTA = :pIDQUOTA");
                        oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                        foreach (clsBordQuotaTE oQuotaTE in oQuota.TipiEvento)
                        {
                            oSB = new StringBuilder();
                            oSB.Append("INSERT INTO BORD_QUOTE_TE (IDQUOTA, TIPO_EVENTO) VALUES (:pIDQUOTA, :pTIPO_EVENTO)");
                            oPars = new clsParameters();
                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pTIPO_EVENTO", oQuotaTE.TE);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }

                        oSB = new StringBuilder();
                        oSB.Append("DELETE FROM BORD_QUOTE_SPETINTRA WHERE IDQUOTA = :pIDQUOTA");
                        oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                        if (oQuota.SpettIntra != null && !string.IsNullOrEmpty(oQuota.SpettIntra) && !string.IsNullOrWhiteSpace(oQuota.SpettIntra))
                        {
                            oSB = new StringBuilder();
                            oSB.Append("INSERT INTO BORD_QUOTE_SPETINTRA (IDQUOTA, SPETTACOLO_INTRATTENIMENTO) VALUES (:pIDQUOTA, :pSPETTACOLO_INTRATTENIMENTO)");
                            oPars = new clsParameters();
                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pSPETTACOLO_INTRATTENIMENTO", oQuota.SpettIntra);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                    }

                }



                if (dataConfig.AltreQuote != null)
                {
                    foreach (clsBordQuota oQuota in dataConfig.AltreQuote)
                    {
                        if (oQuota.IdQuota > 0)
                        {
                            oRS_Quote.MoveFirst();
                            while (!oRS_Quote.EOF)
                            {
                                if (long.Parse(oRS_Quote.Fields("IDQUOTA").Value.ToString()) == oQuota.IdQuota)
                                {
                                    if (oRS_Quote.Fields("DESCRIZIONE").Value.ToString() != oQuota.Descr)
                                    {
                                        oSB = new StringBuilder();
                                        oSB.Append("UPDATE BORD_QUOTE SET DESCRIZIONE = :pDESCRIZIONE WHERE IDQUOTA = :pIDQUOTA");
                                        oPars = new clsParameters();
                                        oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                                        oPars.Add(":pDESCRIZIONE", oQuota.Descr);
                                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                                    }
                                }
                                oRS_Quote.MoveNext();
                            }

                            oSB = new StringBuilder();
                            oSB.Append("DELETE FROM BORD_QUOTE_ALTRO WHERE IDQUOTA = :pIDQUOTA");
                            oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }
                        else
                        {
                            // insert
                            IRecordSet oRSNewIdQuota = (IRecordSet)Connection.ExecuteQuery("SELECT SEQ_QUOTE_BORD.NEXTVAL AS IDQUOTA FROM DUAL");
                            oQuota.IdQuota = long.Parse(oRSNewIdQuota.Fields("IDQUOTA").Value.ToString());
                            oRSNewIdQuota.Close();
                            oSB = new StringBuilder();
                            oPars = new clsParameters();
                            oSB.Append("INSERT INTO BORD_QUOTE");
                            oSB.Append(" (IDUXC, IDQUOTA, DESCRIZIONE)");
                            oSB.Append(" VALUES");
                            oSB.Append(" (:pIDUXC, :pIDQUOTA, :pDESCRIZIONE)");
                            oPars.Add(":pIDUXC", IdUxc);
                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pDESCRIZIONE", oQuota.Descr);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }

                        // inserimento righe
                        foreach (clsBordQuotaRiga oRiga in oQuota.Righe)
                        {
                            oSB = new StringBuilder();
                            oPars = new clsParameters();
                            oSB.Append("INSERT INTO BORD_QUOTE_ALTRO ");
                            oSB.Append(" (  IDQUOTA,   RIGA_QUOTA,   TIPO_CALCOLO,   GIORNI_SETTIMANA,   NUMERO_EVENTO_PER_SALA, CODICE_LOCALE, CAMPO_CONFRONTO, CAMPO_CONFRONTO_MIN, CAMPO_CONFRONTO_MAX, CAMPO_CALCOLO, PERC_CAMPO_CALCOLO, VALORE_FISSO)");
                            oSB.Append(" VALUES");
                            oSB.Append(" (:pIDQUOTA, :pRIGA_QUOTA, :pTIPO_CALCOLO");

                            //:pCAMPO_CALCOLO, :pPERC_CAMPO_CALCOLO, :pVALORE_FISSO)

                            oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                            oPars.Add(":pRIGA_QUOTA", oRiga.Riga);
                            oPars.Add(":pTIPO_CALCOLO", oRiga.TipoCalcolo);

                            if (!string.IsNullOrEmpty(oRiga.GiorniSettimana) && !string.IsNullOrWhiteSpace(oRiga.GiorniSettimana) && oRiga.GiorniSettimana != "")
                            {
                                oSB.Append(", :pGIORNI_SETTIMANA");
                                oPars.Add(":pGIORNI_SETTIMANA", oRiga.GiorniSettimana);
                            }
                            else
                                oSB.Append(", NULL");

                            if (oRiga.NumeroEventoPerSala >= 0)
                            {
                                oSB.Append(", :pNUMERO_EVENTO_PER_SALA");
                                oPars.Add(":pNUMERO_EVENTO_PER_SALA", oRiga.NumeroEventoPerSala);
                            }
                            else
                                oSB.Append(", NULL");

                            if (!string.IsNullOrEmpty(oRiga.CodiceLocale) && !string.IsNullOrWhiteSpace(oRiga.CodiceLocale) && oRiga.CodiceLocale != "")
                            {
                                oSB.Append(", :pCODICE_LOCALE");
                                oPars.Add(":pCODICE_LOCALE", oRiga.CodiceLocale);
                            }
                            else
                            {
                                oSB.Append(", NULL");
                            }


                            if (!string.IsNullOrEmpty(oRiga.CampoConfronto) && !string.IsNullOrWhiteSpace(oRiga.CampoConfronto) && oRiga.CampoConfronto != "")
                            {
                                oSB.Append(", :pCAMPO_CONFRONTO, :pCAMPO_CONFRONTO_MIN, :pCAMPO_CONFRONTO_MAX");
                                oPars.Add(":pCAMPO_CONFRONTO", oRiga.CampoConfronto);
                                oPars.Add(":pCAMPO_CONFRONTO_MIN", oRiga.CampoConfrontoMIN);
                                oPars.Add(":pCAMPO_CONFRONTO_MAX", oRiga.CampoConfrontoMAX);
                            }
                            else
                            {
                                oSB.Append(", NULL, 0, 0");
                            }

                            if (!string.IsNullOrEmpty(oRiga.CampoCalcolo) && !string.IsNullOrWhiteSpace(oRiga.CampoCalcolo) && oRiga.CampoCalcolo != "")
                            {
                                oSB.Append(", :pCAMPO_CALCOLO, :pPERC_CAMPO_CALCOLO");
                                oPars.Add(":pCAMPO_CALCOLO", oRiga.CampoCalcolo);
                                oPars.Add(":pPERC_CAMPO_CALCOLO", oRiga.Percentuale);
                            }
                            else
                            {
                                oSB.Append(", NULL, 0");
                            }

                            if (oRiga.ValoreFisso != null)
                            {
                                oSB.Append(", :pVALORE_FISSO)");
                                oPars.Add(":pVALORE_FISSO", oRiga.ValoreFisso);
                            }
                            else
                                oSB.Append(", NULL)");

                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        }


                        oSB = new StringBuilder();
                        oSB.Append("DELETE FROM BORD_QUOTE_TE WHERE IDQUOTA = :pIDQUOTA");
                        oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                        if (oQuota.TipiEvento != null)
                        {
                            foreach (clsBordQuotaTE oQuotaTE in oQuota.TipiEvento)
                            {
                                oSB = new StringBuilder();
                                oSB.Append("INSERT INTO BORD_QUOTE_TE (IDQUOTA, TIPO_EVENTO) VALUES (:pIDQUOTA, :pTIPO_EVENTO)");
                                oPars = new clsParameters();
                                oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                                oPars.Add(":pTIPO_EVENTO", oQuotaTE.TE);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }
                        }

                        oSB = new StringBuilder();
                        oSB.Append("DELETE FROM BORD_QUOTE_SPETINTRA WHERE IDQUOTA = :pIDQUOTA");
                        oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                        oSB = new StringBuilder();
                        oSB.Append("INSERT INTO BORD_QUOTE_SPETINTRA (IDQUOTA, SPETTACOLO_INTRATTENIMENTO) VALUES (:pIDQUOTA, :pSPETTACOLO_INTRATTENIMENTO)");
                        oPars = new clsParameters();
                        oPars.Add(":pIDQUOTA", oQuota.IdQuota);
                        oPars.Add(":pSPETTACOLO_INTRATTENIMENTO", oQuota.SpettIntra);
                        Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                        //try
                        //{
                        //    oSB = new StringBuilder();
                        //    oSB.Append("DELETE FROM BORD_QUOTE_OPZIONALI WHERE IDQUOTA = :pIDQUOTA");
                        //    oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        //    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        //    if (oQuota.Opzionale)
                        //    {
                        //        oSB = new StringBuilder();
                        //        oSB.Append("INSERT INTO BORD_QUOTE_OPZIONALI (IDQUOTA) VALUES (:pIDQUOTA)");
                        //        oPars = new clsParameters(":pIDQUOTA", oQuota.IdQuota);
                        //        Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                        //    }
                        //}
                        //catch (Exception)
                        //{
                        //}
                    }

                }

                lRet = true;
            }
            catch (Exception ex)
            {
                lRet = false;
                oError = ex;
            }

            if (lRet)
            {
                Connection.EndTransaction();
                Configurazione = GetConfigurazione(Connection, IdUxc);
            }
            else
                Connection.RollBack();

            return Configurazione;
        }

        public static List<clsSearchSingoloEvento> RicercaEventi(IConnection Connection, long IdUxc, clsSearchEvento Search, long idCinema)
        {
            List<clsSearchSingoloEvento> Eventi = new List<clsSearchSingoloEvento>();

            try
            {
                clsParameters oPars = new clsParameters();
                oPars.Add(":pIDUXC", IdUxc);
                if (Search.FlagTitolo) oPars.Add(":pTITOLO_EVENTO", "%" + Search.Titolo.Trim().ToUpper().Replace(" ", "%") + "%");
                if (Search.FlagCodiceLocale) oPars.Add(":pCODICE_LOCALE", Search.CodiceLocale);
                if (Search.FlagDataEvento) oPars.Add(":pINIZIO_DATA_EVENTO", Search.InizioDataEvento.Date);
                if (Search.FlagDataEvento) oPars.Add(":pFINE_DATA_EVENTO", Search.FineDataEvento.Date);

                StringBuilder oSB = new StringBuilder();


                if (Search.TipoRicerca == "RicercaEventoDem")
                {
                    oSB.Append(" SELECT ");
                    oSB.Append(" VR_CALENDARIO.DATA_ORA,");
                    oSB.Append(" VR_SALE.CODICE_LOCALE,");
                    oSB.Append(" VR_PACCHETTO.DESCRIZIONE AS TITOLO_EVENTO");
                }
                else if (Search.TipoRicerca == "RicercaTitoloDem")
                {
                    oSB.Append(" SELECT ");
                    oSB.Append(" DISTINCT VR_PACCHETTO.DESCRIZIONE AS TITOLO_EVENTO");
                }

                oSB.Append(" FROM");
                oSB.Append(" VR_CALENDARIO, ");
                oSB.Append(" VR_BORD_CF,");
                oSB.Append(" VR_BORD_CL,");
                oSB.Append(" VR_SALE,");
                oSB.Append(" VR_PACCHETTO");
                oSB.Append(" WHERE");
                oSB.Append(" VR_BORD_CF.IDUXC = :pIDUXC");
                oSB.Append(" AND");
                oSB.Append(" VR_BORD_CL.IDUXC = :pIDUXC");

                if (Search.FlagTitolo) oSB.Append(" AND UPPER(TRIM(VR_PACCHETTO.DESCRIZIONE)) LIKE :pTITOLO_EVENTO");
                if (Search.FlagCodiceLocale) oSB.Append(" AND VR_SALE.CODICE_LOCALE = :pCODICE_LOCALE");
                if (Search.FlagDataEvento) oSB.Append(" AND VR_CALENDARIO.DATA_ORA >= TRUNC(:pINIZIO_DATA_EVENTO)");
                if (Search.FlagDataEvento) oSB.Append(" AND VR_CALENDARIO.DATA_ORA < TRUNC(:pFINE_DATA_EVENTO) + 1");

                oSB.Append(" AND");
                oSB.Append(" VR_CALENDARIO.ORGANIZZATORE = VR_BORD_CF.CF_ORGANIZZATORE");
                oSB.Append(" AND");
                oSB.Append(" VR_CALENDARIO.IDSALA = VR_SALE.IDSALA");
                oSB.Append(" AND");
                oSB.Append(" VR_SALE.IDSALA = VR_BORD_CL.IDSALA");
                oSB.Append(" AND");
                oSB.Append(" VR_PACCHETTO.PROGR = 1");
                oSB.Append(" AND");
                oSB.Append(" VR_CALENDARIO.IDPACCHETTO = VR_PACCHETTO.IDPACCHETTO");

                if (idCinema > 0)
                {
                    oSB.Append(" AND EXISTS (SELECT NULL FROM CINEMA.CALENDARIO WHERE CALENDARIO.IDCALEND = VR_CALENDARIO.IDCALEND AND STOREID = :pSTOREID) ");
                    oPars.Add(":pSTOREID", idCinema);
                }

                if (Search.TipoRicerca == "RicercaEventoDem")
                {
                    oSB.Append(" ORDER BY");
                    oSB.Append(" TRUNC(VR_CALENDARIO.DATA_ORA),");
                    oSB.Append(" VR_SALE.CODICE_LOCALE,");
                    oSB.Append(" VR_CALENDARIO.DATA_ORA");
                }
                else if (Search.TipoRicerca == "RicercaTitoloDem")
                {
                    oSB.Append(" ORDER BY");
                    oSB.Append(" VR_PACCHETTO.DESCRIZIONE");
                }

                IRecordSet oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                while (!oRS.EOF)
                {
                    clsSearchSingoloEvento oEvento = new clsSearchSingoloEvento();

                    if (Search.TipoRicerca == "RicercaEventoDem")
                    {
                        oEvento.CodiceLocale = oRS.Fields("CODICE_LOCALE").Value.ToString();
                        oEvento.TitoloEvento = oRS.Fields("TITOLO_EVENTO").Value.ToString();
                        oEvento.DataOraEvento = (DateTime)oRS.Fields("DATA_ORA").Value;
                    }
                    else if (Search.TipoRicerca == "RicercaTitoloDem")
                    {
                        oEvento.CodiceLocale = "";
                        oEvento.TitoloEvento = oRS.Fields("TITOLO_EVENTO").Value.ToString();
                        oEvento.DataOraEvento = DateTime.MinValue;
                    }

                    Eventi.Add(oEvento);
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch (Exception)
            {
            }

            return Eventi;
        }

        #endregion
    }

    public class clsItemPdfBordero
    {
        public string Descrizione { get; set; }

        public byte[] Buffer { get; set; }
        public DataTable Table { get; set; }
        public DataTable TableAbbonamenti { get; set; }

        public string FileName { get; set; }

        public string DescrizioneEstesa { get; set; }

        public clsItemPdfBordero Clone()
        {
            clsItemPdfBordero result = new clsItemPdfBordero();
            result.Descrizione = this.Descrizione;
            result.Buffer = this.Buffer;
            result.FileName = this.FileName;
            return result;
        }
    }

    public class clsCredenzialiCodiceSistema
    {
        public bool useEmailCodiceSistema = true;
        public string emailFrom = "";
        public string emailSmtp = "";
        public int emailPort = 0;
        public string emailUser = "";
        public string emailPassword = "";
        public bool UseDefaultCredentials = true;
        public bool EnableSsl = false;
    }

    public class clsItemFullData
    {
        public DataSet DataSetBordero { get; set; }
        public long IdRichiestaBordero { get; set; }
        public libBordero.Bordero.clsBordQuoteConfig Config { get; set; }
        public bool Result {  get; set; }
    }

    public class clsItemGruppo
    {
        
        public string Descrizione = "";
        public string NomeConnessione = "SERVERX";
        public DateTime Inizio { get; set; }
        public DateTime Fine { get; set; }
        public bool PeriodoEsterno { get; set; }

        public IConnection conn = null;

        public string modalitaGiorno = "";
        public DataSet datasetBordero { get; set; }
        public List<clsItemPdfBordero> ItemsPdfBordero { get; set; }


        public bool split_GIORNI = false;
        public bool split_ORGANIZZATORE = false;
        public bool split_TIPO_EVENTO = false;
        public bool split_SALA = false;
        public bool split_TITOLO = false;
        public bool split_PRODUTTORE = false;
        public bool split_NOLEGGIATORE = false;

        public string value_ORGANIZZATORE = "";
        public string value_TIPO_EVENTO = "";
        public string value_SALA = "";
        public string value_TITOLO = "";
        public string value_PRODUTTORE = "";
        public string value_NOLEGGIATORE = "";

        public bool isFiltered => !string.IsNullOrEmpty(value_ORGANIZZATORE.Trim() + value_TIPO_EVENTO.Trim() + value_SALA.Trim() + value_TITOLO.Trim() + value_PRODUTTORE.Trim() + value_NOLEGGIATORE.Trim());
        public bool isSplitted => this.split_GIORNI || this.split_NOLEGGIATORE || this.split_ORGANIZZATORE || this.split_PRODUTTORE || this.split_SALA || this.split_TIPO_EVENTO || this.split_TITOLO;

        public string destinationPath = "";
        public string emailDests = "";
        public bool generazionePdf = false;
        public bool generazioneCsv = false;

        public bool useEmailCodiceSistema = true;
        public string emailFrom = "";
        public string emailSmtp = "";
        public int emailPort = 0;
        public string emailUser = "";
        public string emailPassword = "";
        public bool UseDefaultCredentials = true;
        public bool EnableSsl = false;

        public string NomeDll = "";

        public string serviceUserInfo = "";

        public clsCredenzialiCodiceSistema CredenzialiCodiceSistema { get; set; }

        public clsItemGruppo Clone()
        {
            clsItemGruppo result = new clsItemGruppo();

            result.Descrizione = this.Descrizione;
            result.NomeConnessione = this.NomeConnessione;
            result.Inizio = this.Inizio;
            result.Fine = this.Fine;

            result.conn = this.conn;

            result.modalitaGiorno = this.modalitaGiorno;
            result.datasetBordero = this.datasetBordero;
            result.ItemsPdfBordero = null;
            if (this.ItemsPdfBordero != null)
            {
                result.ItemsPdfBordero = new List<clsItemPdfBordero>();
                this.ItemsPdfBordero.ForEach(i => result.ItemsPdfBordero.Add(i.Clone()));
            }

            result.split_GIORNI = this.split_GIORNI;
            result.split_ORGANIZZATORE = this.split_ORGANIZZATORE;
            result.split_TIPO_EVENTO = this.split_TIPO_EVENTO;
            result.split_SALA = this.split_SALA;
            result.split_TITOLO = this.split_TITOLO;
            result.split_PRODUTTORE = this.split_PRODUTTORE;
            result.split_NOLEGGIATORE = this.split_NOLEGGIATORE;

            result.value_ORGANIZZATORE = this.value_ORGANIZZATORE;
            result.value_TIPO_EVENTO = this.value_TIPO_EVENTO;
            result.value_SALA = this.value_SALA;
            result.value_TITOLO = this.value_TITOLO;
            result.value_PRODUTTORE = this.value_PRODUTTORE;
            result.value_NOLEGGIATORE = this.value_NOLEGGIATORE;

            result.destinationPath = this.destinationPath;
            result.emailDests = this.emailDests;
            result.generazionePdf = this.generazionePdf;
            result.generazioneCsv = this.generazioneCsv;
            result.useEmailCodiceSistema = this.useEmailCodiceSistema;
            result.emailFrom = this.emailFrom;
            result.emailSmtp = this.emailSmtp;
            result.emailPort = this.emailPort;
            result.emailUser = this.emailUser;
            result.emailPassword = this.emailPassword;
            result.UseDefaultCredentials = this.UseDefaultCredentials;
            result.EnableSsl = this.EnableSsl;

            result.serviceUserInfo = this.serviceUserInfo;

            result.NomeDll = this.NomeDll;

            return result;
        }

        public string DescrizioneFiltri
        {
            get
            {
                string result = "";
                if (this.isFiltered)
                {
                    if (this.value_ORGANIZZATORE.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Organizzatore: {0}", this.value_ORGANIZZATORE);
                    if (this.value_NOLEGGIATORE.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Distributore: {0}", this.value_NOLEGGIATORE);
                    if (this.value_PRODUTTORE.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Produttore: {0}", this.value_PRODUTTORE);
                    if (this.value_SALA.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Sala: {0}", this.value_SALA);
                    if (this.value_TIPO_EVENTO.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Tipo evento: {0}", this.value_TIPO_EVENTO);
                    if (this.value_TITOLO.Trim() != "") result += (result.Trim() == "" ? "" : "\r\n") + string.Format("Titolo evento: {0}", this.value_TITOLO);
                }
                return result;
            }
        }

        public string DescrizioneSplit
        {
            get
            {
                string result = "";
                if (this.isFiltered)
                {
                    if (this.split_GIORNI) result += (result.Trim() == "" ? "" : "\r\n") + "Giorni";
                    if (this.split_ORGANIZZATORE) result += (result.Trim() == "" ? "" : "\r\n") + "Organizzatore";
                    if (this.split_NOLEGGIATORE) result += (result.Trim() == "" ? "" : "\r\n") + "Distributore";
                    if (this.split_PRODUTTORE) result += (result.Trim() == "" ? "" : "\r\n") + "Produttore";
                    if (this.split_SALA) result += (result.Trim() == "" ? "" : "\r\n") + "Sala";
                    if (this.split_TIPO_EVENTO) result += (result.Trim() == "" ? "" : "\r\n") + "Tipo evento";
                    if (this.split_TITOLO) result += (result.Trim() == "" ? "" : "\r\n") + "Titolo evento";
                }
                return result;
            }
        }

        public string DescrizioneModalitaPeriodo
        {
            get
            {
                string result = "";
                DateTime today = DateTime.Now.Date;
                if (this.modalitaGiorno == "IERI")
                    result = "Ieri " + today.Date.AddDays(-1).ToShortDateString();
                else if (this.modalitaGiorno == "GIORNO")
                    result = this.Inizio.Date.Equals(this.Fine.Date) ? "il " + this.Inizio.ToShortDateString() : "da " + this.Inizio.ToShortDateString() + " a " + this.Fine.Date.ToShortDateString();
                else if (this.modalitaGiorno == "INIZIO")
                    result = this.Inizio.Date.Equals(this.Fine.Date) ? "il " + this.Inizio.ToShortDateString() : "da " + this.Inizio.ToShortDateString() + " a " + this.Fine.Date.ToShortDateString();
                else if (this.modalitaGiorno == "FINE")
                    result = this.Inizio.Date.Equals(this.Fine.Date) ? "il " + this.Inizio.ToShortDateString() : "da " + this.Inizio.ToShortDateString() + " a " + this.Fine.Date.ToShortDateString();
                else if (this.modalitaGiorno == "SETTIMANA_PREC")
                    result = "Settimana precedente da " + today.AddDays(-7).Date.ToShortDateString() + " a " + today.AddDays(-1).Date.ToShortDateString();
                else if (this.modalitaGiorno == "MESE_PREC")
                {
                    int year = today.Year;
                    int month = today.Month - 1;
                    if (today.Month == 1)
                    {
                        year -= 1;
                        month = 12;
                    }
                    today = new DateTime(year, month, 1);
                    result = "Mese precedente da " + today.Date.ToShortDateString() + " a " + today.AddMonths(1).AddDays(-1).Date.ToShortDateString();
                }

                return result;
            }
        }
    }

    public class clsConfigurazioneGruppi
    {
        public List<clsItemGruppo> Gruppi = new List<clsItemGruppo>();
        public static string ExternalFileName = "";
        public static string GetNomeDb()
        {
            if (ExternalFileName != "")
                return ExternalFileName;
            else
            {
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath);
                return oFileInfo.Directory.FullName + @"\BorderoAuto.db";
            }
        }

        public static bool CheckExistsFileGruppi()
        {
            return System.IO.File.Exists(GetNomeDb());
        }

        public static clsConfigurazioneGruppi GetConfigFromFile(out Exception error)
        {
            error = null;
            clsConfigurazioneGruppi result = null;
            string file = GetNomeDb();
            if (!System.IO.File.Exists(file))
                error = new Exception("File di configurazione non trovato\r\n" + file);
            else
            {
                string textDB = System.IO.File.ReadAllText(file);
                try
                {
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<clsConfigurazioneGruppi>(textDB);
                }
                catch (Exception ex)
                {
                    result = null;
                    error = new Exception("Errore nella lettura del file di configurazione\r\n" + file + "\r\n" + ex.Message);
                }
            }
            return result;
        }

        public static clsConfigurazioneGruppi SaveConfigFromFile(libBordero.clsConfigurazioneGruppi configurazione, out Exception error)
        {
            error = null;
            clsConfigurazioneGruppi result = null;
            string file = GetNomeDb();
            if (true)
            {
                string textDB = Newtonsoft.Json.JsonConvert.SerializeObject(configurazione, Newtonsoft.Json.Formatting.Indented);
                System.IO.File.WriteAllText(file, textDB);
                result = libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error);
            }
            return result;
        }
    }


    public class clsExportCsv
    {
        List<string> fields = new List<string>();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> currentRow { get { return rows[rows.Count - 1]; } }
        public string path = "";
        public byte[] buffer;
        private Dictionary<string, string> oParameters = new Dictionary<string, string>();
        private Exception oError = null;
        protected bool lExportColumnNames = true;

        private static int minIndex = 3;

        public Exception Error
        {
            get { return this.oError; }
        }

        public Dictionary<string, string> Parameters
        {
            get { return this.oParameters; }
            set { this.oParameters = value; }
        }

        public object this[string field]
        {
            set
            {
                // Keep track of the field names, because the dictionary loses the ordering
                if (!fields.Contains(field)) fields.Add(field);
                currentRow[field] = value;
            }
        }

        public void AddRow()
        {
            rows.Add(new Dictionary<string, object>());
        }

        string MakeValueCsvFriendly(object value)
        {
            if (value == null) return "";
            if (value is INullable && ((INullable)value).IsNull) return "";
            if (value is DateTime)
            {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }
            string output = value.ToString();
            if (output.Contains(";") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';
            return output;
        }

        public string Export()
        {
            StringBuilder sb = new StringBuilder();

            // The header
            if (lExportColumnNames)
            {
                foreach (string field in fields)
                {
                    sb.Append(field).Append(";");
                }
                sb.AppendLine();
            }

            // The rows
            foreach (Dictionary<string, object> row in rows)
            {
                foreach (string field in fields)
                {
                    sb.Append(MakeValueCsvFriendly(row[field])).Append(";");
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public void ExportToFile(string path)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            System.IO.File.WriteAllText(path, Export());
        }

        public byte[] ExportToBytes()
        {
            return Encoding.UTF8.GetBytes(Export());
        }

        public virtual bool WriteDataSet(System.Data.DataSet DataSet, out Exception Error)
        {
            bool lRet = false;
            Error = null;
            if (this.Parameters.ContainsKey("FILE"))
            {
                this.path = this.Parameters["FILE"];
            }
            try
            {
                foreach (System.Data.DataTable Table in DataSet.Tables)
                {
                    foreach (System.Data.DataRow Row in Table.Rows)
                    {
                        this.AddRow();
                        int index = 0;
                        foreach (System.Data.DataColumn Col in Table.Columns)
                        {
                            index += 1;
                            if (index >= minIndex)
                                this[Col.ColumnName] = Row[Col.ColumnName];
                        }
                    }
                }
                if (this.path.Trim() != "")
                {
                    ExportToFile(this.path);
                }
                else
                {
                    this.buffer = this.ExportToBytes();
                }
                lRet = true;
            }
            catch (Exception ex)
            {
                lRet = false;
                Error = ex;
            }
            return lRet;
        }

        public virtual bool WriteDataTable(System.Data.DataTable Table, out Exception Error)
        {
            bool lRet = false;
            Error = null;
            if (this.Parameters.ContainsKey("FILE"))
            {
                this.path = this.Parameters["FILE"];
            }
            try
            {
                foreach (System.Data.DataRow Row in Table.Rows)
                {
                    this.AddRow();
                    int index = 0;
                    foreach (System.Data.DataColumn Col in Table.Columns)
                    {
                        index += 1;
                        if (index >= minIndex)
                            this[Col.ColumnName] = Row[Col.ColumnName];
                    }
                }
                if (this.path.Trim() != "")
                {
                    ExportToFile(this.path);
                }
                else
                {
                    this.buffer = this.ExportToBytes();
                }
                lRet = true;
            }
            catch (Exception ex)
            {
                lRet = false;
                Error = ex;
            }
            return lRet;
        }

        public object Result
        {
            get
            {
                if (this.path.Trim() != "")
                {
                    return this.path;
                }
                else
                {
                    return this.buffer;
                }
            }
        }
    }

    public static class clsCheckVersioneWintic
    {
        public static bool CheckVersioneWintic(IConnection Connection)
        {
            bool result = false;
            try
            {
                DataTable table = Connection.ExecuteQuery("SELECT OBJECT_NAME FROM ALL_OBJECTS WHERE OWNER = 'WTIC' AND OBJECT_NAME = 'P_CALC_BORDERO' AND OBJECT_TYPE = 'PROCEDURE'");
                result = table != null && table.Rows != null && table.Rows.Count > 0;
                table.Dispose();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }

    public class clsBorderoServiceConfig
    {
        public string Url { get; set; }
        public int IdCinema { get; set; }

        public string Token { get; set; }
        
        public clsAccount Account { get; set; }
        public clsCodiceSistema CodiceSistema { get; set; }
    }

    public static class clsBorderoServiceConfigUtility
    {
        public static clsBorderoServiceConfig GetServiceConfig()
        {
            clsBorderoServiceConfig result = null;
            System.IO.FileInfo oFileInfo = new System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath);
            string fileName = oFileInfo.Directory.FullName + @"\BorderoServiceConfig.json";
            if (System.IO.File.Exists(fileName))
            {
                try
                {
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<clsBorderoServiceConfig>(System.IO.File.ReadAllText(fileName));
                    if (string.IsNullOrEmpty(result.Url))
                        result = null;
                }
                catch (Exception)
                {
                    result = null;
                }
            }
            return result;
        }





        public static ResponseRiepiloghi GetConfigurazioneFromService(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Riepiloghi/GetConfigBordero", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResponseRiepiloghi SetConfigurazioneFromService(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Riepiloghi/SetConfigBordero", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResponseRiepiloghi CalcBordero(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Riepiloghi/CalcBordero", oRequest, false));
                string errCalc = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(result, true, "DataSetBorderoJson");
                if (string.IsNullOrEmpty(errCalc))
                {
                    DataSet oNewDataSet = ResultJsonDataSet.GetDataSet(result.Data.DataSetBorderoJson);
                    result.Data.DataSetBordero = oNewDataSet;
                }
                else
                    throw new Exception(errCalc);
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        #region Comunicazione

        public static ResponseRiepiloghi Login(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Auth/Login", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResponseRiepiloghi GetAccount(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Auth/GetAccount", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResponseRiepiloghi GetTokenCodiceSistema(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Auth/GetTokenCodiceSistema", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResponseRiepiloghi BorderoSearchEvento(PayLoadRiepiloghi oRequest, string urlBase)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest(urlBase + "/Riepiloghi/BorderoSearchEvento", oRequest, false));
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        public static T JsonToObject<T>(string parJson)
        {
            return (T)JsonConvert.DeserializeObject(parJson, typeof(T));
        }

        private static string GetWebRequest(string parUrl, object parRequest, bool Crypt)
        {

            string result = string.Empty;

            HttpWebResponse resp = null;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(parUrl);
            req.CookieContainer = new CookieContainer();
            req.Credentials = CredentialCache.DefaultNetworkCredentials;
            req.UserAgent = ": Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
            req.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Anonymous;
            req.Method = "POST";
            req.ContentType = "text/json";
            //req.Timeout = 5000;

            string json = JsonConvert.SerializeObject(parRequest);

            using (StreamWriter streamWriter = new StreamWriter(req.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();

                resp = (HttpWebResponse)req.GetResponse();
                Encoding enc = Encoding.UTF8;
                using (StreamReader responseStream = new StreamReader(resp.GetResponseStream(), enc))
                {
                    result = responseStream.ReadToEnd();
                    responseStream.Close();
                }
                resp.Close();
            }
            return result;
        }

        public static string GetDescResponseError(libBordero.ResponseRiepiloghi response, bool checkData, string propertyToCheck)
        {
            string result = "";
            if (response == null)
                result = "Nessuna risposta.";
            else
            {
                if (response.Status == null)
                    result = "Risposta non valida.";
                else
                {
                    if (!response.Status.StatusOK)
                    {
                        result = response.Status.StatusMessage;
                        if (string.IsNullOrEmpty(result))
                            result = "Risposta non valida.";
                    }
                    else if (checkData)
                    {
                        if (response.Data == null)
                            result = "Nessun dato recuperato";
                        else
                        {
                            if (!string.IsNullOrEmpty(propertyToCheck))
                            {
                                PropertyInfo pInfo = typeof(libBordero.ResponseDataRiepiloghi).GetProperty(propertyToCheck);
                                if (pInfo.GetValue(response.Data) == null)
                                    result = "Dati non validi";
                                else if (string.IsNullOrEmpty(pInfo.GetValue(response.Data).ToString()))
                                    result = "Dati non validi";
                            }
                        }
                    }
                }
            }
            return result;
        }

        #endregion

        #region REQUEST GENERICHE

        public static ResponseRiepiloghi RequestToRiepiloghi(PayLoadRiepiloghi oRequest, string urlBase, string method)
        {
            ResponseRiepiloghi result = null;
            try
            {
                result = JsonConvert.DeserializeObject<ResponseRiepiloghi>(GetWebRequest($"{urlBase}/Riepiloghi/{method}", oRequest, false));
                string err = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(result, true, "");
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
            }
            catch (Exception ex)
            {
                result = new ResponseRiepiloghi();
                result.Status = new ResultBase();
                result.Status.StatusOK = false;
                result.Status.StatusMessage = ex.Message;
            }
            return result;
        }

        #endregion
    }

    public class ResultContentFile
    {
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string Buffer { get; set; }
    }

    public class ResultBase
    {
        public bool StatusOK = true;

        public string StatusMessage = "";

        public Int64 StatusCode = 0;

        public string execution_time = "";

        public ResultContentFile contentFile { get; set; }

        public DateTime Sysdate { get; set; }

        public ResultBase()
        {
        }

        public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
        {
            this.StatusOK = lStatusOK;
            this.StatusMessage = cStatusMessage;
            this.StatusCode = nStatusCode;
            this.execution_time = cExecutionTime;
        }

        public class ResultBaseToken : ResultBase
        {
            public string Token = "";
            public clsAccount Account = null;

            public ResultBaseToken()
            {
            }

            public ResultBaseToken(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cToken) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Token = cToken;
            }
        }
    }

    public class clsCodiceLocale
    {
        public string CodiceSistema = "";
        public string CodiceLocale = "";
        public string Descrizione = "";
        public bool Enabled = false;
    }

    public class clsCFOrganizzatore
    {
        public string CodiceSistema = "";
        public string CFOrganizzatore = "";
        public string Descrizione = "";
        public bool Enabled = false;
    }

    public class clsProfileFilter
    {
        public string CodiceSistema = "";
        public string Campo = "";
        public string Tipo = "";
        public string DescCampo = "";
        public string TipoRiga = "";
        public string DescrizioneTipoRiga = "";
        public string DescrizioneFiltro = "";
        public List<object> Valori = new List<object>();
    }


    public class clsMultiCinema
    {
        public long IdCinema { get; set; }
        public string Descrizione { get; set; }
    }
    public class clsCodiceSistema
    {
        // sdfsdf
        public string CodiceSistema = "";
        public string Descrizione = "";
        public bool Enabled = false;
        public string ConnectionString = "";
        public List<clsCodiceLocale> CodiciLocale = new List<clsCodiceLocale>();
        public List<clsCFOrganizzatore> CFOrganizzatori = new List<clsCFOrganizzatore>();
        public List<clsProfileFilter> Filters = new List<clsProfileFilter>();
        public bool AllCodiciLocali = false;
        public bool AllCFOrganizzatori = false;
        public clsMultiCinema MultiCinema { get; set; }
    }
    public class clsOperazioneRiepiloghi
    {
        public Int64 Ordine = 0;
        public string IdOperazione = "";
        public string Descrizione = "";
        public Int64 IdCategoria = 0;
        public Int64 OrdineCategoria = 0;
        public string DescrizioneCategoria = "";
        public bool Enabled = false;
    }

    public class clsCategoriaOperazioniRiepiloghi
    {
        public Int64 IdCategoria = 0;
        public Int64 Ordine = 0;
        public string Descrizione = "";
        public List<clsOperazioneRiepiloghi> ListaOperazioni = new List<clsOperazioneRiepiloghi>();
    }

    public class clsAccount
    {
        public string AccountId = "";
        public string Nome = "";
        public string Cognome = "";
        public string Email = "";
        public bool Enabled = false;
        public List<clsCodiceSistema> CodiciSistema = new List<clsCodiceSistema>();
        public List<clsCategoriaOperazioniRiepiloghi> Operazioni = new List<clsCategoriaOperazioniRiepiloghi>();
        public long ProfileId = 0;

        public clsAccount()
        { }
    }
    public class clsProfile
    {
        public Int64 ProfileId = 0;
        public Int64 ParentProfileId = 0;
        public string Descrizione = "";
        public bool Enabled = false;
        public bool AllCodiciSistema = false;
        public List<clsCodiceSistema> CodiciSistema = new List<clsCodiceSistema>();
        public List<clsCategoriaOperazioniRiepiloghi> OperazioniProfile = new List<clsCategoriaOperazioniRiepiloghi>();
    }

    public class ResponseDataRiepiloghi
    {
        public string Token { get; set; }
        public clsAccount Account { get; set; }
        public clsProfile Profile { get; set; }

        public List<clsProfile> Profiles { get; set; }
        public List<clsAccount> Accounts = null;

        // GetCodiceSistema
        public clsCodiceSistema CodiceSistema { get; set; }

        public clsCodiceLocale[] CodiciLocali { get; set; }

        // GetTransazioni, GetTransazioniAnnullati
        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }

        public string Proprieta { get; set; }
        public string Valore { get; set; }


        public Int64 IdCalend { get; set; }
        public DateTime DataOraEvento { get; set; }
        public string DescrizioneSala { get; set; }
        public string CodiceLocale { get; set; }
        public string OraEvento { get; set; }
        public string TitoloEvento { get; set; }

        public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }
        public DataSet DataSetBordero { get; set; }

        public ResultJsonDataSet DataSetBorderoJson { get; set; }
        public long IdRichiestaBordero { get; set; }
        public libBordero.Bordero.clsSearchEvento RichiestaEventi { get; set; }

        public libBordero.Bordero.clsBorderoCedas BorderoCedas { get; set; }
    }

    public class ResponseRiepiloghi
    {
        public ResponseDataRiepiloghi Data { get; set; }
        public ResultBase Status { get; set; }
    }
    public class PlayLoadFiltri
    {
        public string DataEmissioneAnnullamento { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string CodiceLocale { get; set; }
        public string TitoloEvento { get; set; }
    }
    public class PayLoadRiepiloghi
    {
        public string Mode { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string AccountId { get; set; }
        public string CodiceSistema { get; set; }
        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }
        public string Proprieta { get; set; }
        public PlayLoadFiltri Filtri { get; set; }
        public clsAccount Account { get; set; }
        public clsProfile Profile { get; set; }
        public List<long> QuoteManualiEscluse { get; set; }
        public List<long> QuoteManualiTe { get; set; }
        public List<long> QuoteManualiSi { get; set; }
        public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }
        public libBordero.Bordero.clsSearchEvento BorderoSearchEvento { get; set; }
        public libBordero.Bordero.clsBorderoCedas BorderoCedas { get; set; }
    }


    


    public class ResultJsonColumn
    {
        public string ColumnName { get; set; }
        public Type DataType { get; set; }
        public object Value { get; set; }

        public ResultJsonColumn()
        { }

        public ResultJsonColumn(DataColumn col)
            : this()
        {
            this.ColumnName = col.ColumnName;
            this.DataType = col.DataType;
        }

        public ResultJsonColumn(DataColumn col, object value)
            : this(col)
        {
            this.Value = value;
        }

        public static DataColumn GetDataColumn(ResultJsonColumn colJson)
        {
            return new DataColumn() { ColumnName = colJson.ColumnName, DataType = colJson.DataType };
        }
    }

    public class ResultJsonRow
    {
        public List<ResultJsonColumn> Columns { get; set; }

        public ResultJsonRow()
        { }

        public ResultJsonRow(DataRow row)
            : this()
        {
            this.Columns = new List<ResultJsonColumn>();
            foreach (DataColumn col in row.Table.Columns)
            {
                this.Columns.Add(new ResultJsonColumn(col, row[col.ColumnName]));
            }
        }

        public static DataRow GetDataRow(ResultJsonRow rowJson, DataTable table)
        {
            DataRow row = table.NewRow();
            foreach (DataColumn col in table.Columns)
            {
                ResultJsonColumn colJson = rowJson.Columns.FirstOrDefault(c => c.ColumnName == col.ColumnName);
                if (colJson != null)
                {
                    if (colJson.Value != null && colJson.Value != System.DBNull.Value)
                        row[col.ColumnName] = colJson.Value;
                    else
                        row[col.ColumnName] = System.DBNull.Value;
                }
                else
                    row[col.ColumnName] = System.DBNull.Value;
            }
            return row;
        }
    }

    public class ResultJsonTable
    {
        public string TableName { get; set; }
        public List<ResultJsonColumn> Columns { get; set; }
        public List<ResultJsonRow> Rows { get; set; }

        public ResultJsonTable()
        { }

        public ResultJsonTable(DataTable table)
            : this()
        {
            this.TableName = table.TableName;
            this.Columns = new List<ResultJsonColumn>();
            this.Rows = new List<ResultJsonRow>();
            foreach (DataColumn col in table.Columns)
            {
                this.Columns.Add(new ResultJsonColumn(col));
            }
            foreach (DataRow row in table.Rows)
            {
                this.Rows.Add(new ResultJsonRow(row));
            }
        }

        public static DataTable GetDataTable(ResultJsonTable tableJson)
        {
            DataTable table = new DataTable() { TableName = tableJson.TableName };

            foreach (ResultJsonColumn colJson in tableJson.Columns)
            {
                DataColumn col = new DataColumn() { ColumnName = colJson.ColumnName, DataType = colJson.DataType };
                table.Columns.Add(col);
            }
            foreach (ResultJsonRow rowJson in tableJson.Rows)
            {
                DataRow row = ResultJsonRow.GetDataRow(rowJson, table);
                table.Rows.Add(row);
            }
            return table;
        }
    }

    public class ResultJsonDataSet
    {
        public string DataSetName { get; set; }
        public List<ResultJsonTable> Tables { get; set; }

        public ResultJsonDataSet()
        { }

        public ResultJsonDataSet(DataSet dataset)
            : this()
        {
            this.DataSetName = dataset.DataSetName;
            this.Tables = new List<ResultJsonTable>();
            foreach (DataTable table in dataset.Tables)
            {
                this.Tables.Add(new ResultJsonTable(table));
            }
        }

        public static DataSet GetDataSet(ResultJsonDataSet dataSetJson)
        {
            DataSet result = new DataSet() { DataSetName = dataSetJson.DataSetName };
            foreach (ResultJsonTable tableJson in dataSetJson.Tables)
            {
                result.Tables.Add(ResultJsonTable.GetDataTable(tableJson));
            }
            return result;
        }
    }

    public interface ISenderData
    {
        string Description();
        string NomeDll();
        clsItemGruppo GetGruppo(IConnection conn);
        void Send(IConnection conn, libBordero.clsBorderoServiceConfig _serviceConfig, clsItemGruppo gruppo, bool userInterface, Func<string, bool> delegateMessages = null);
    }

    public static class FinderInterfaceSenderData
    {
        public static List<ISenderData> GetSendersList()
        {
            List<ISenderData> result = null;
            System.Reflection.Assembly oAssembly = null;

            try
            {

                string path = GetDirApp();
                string implementedSearchType = "libBordero.ISenderData";
                Exception errorLoadDll = null;
                foreach (string dllFileName in Directory.GetFiles(path, "*.dll"))
                {
                    errorLoadDll = null;
                    try
                    {
                        string dllFileNameFull = dllFileName;
                        if (System.IO.File.Exists(dllFileNameFull))
                        {
                            oAssembly = System.Reflection.Assembly.LoadFile(dllFileNameFull);
                            Type[] FileTypes = oAssembly.GetTypes();
                            System.Diagnostics.FileVersionInfo AssemblyVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(dllFileNameFull);
                            string AssemblyName = AssemblyVersion.OriginalFilename;
                            foreach (Type singleFileType in FileTypes)
                            {
                                Type[] interfaceTypes = singleFileType.GetInterfaces();
                                for (int indexInterface = 0; indexInterface < interfaceTypes.Length; indexInterface++)
                                {
                                    Type implementedType = interfaceTypes[indexInterface];
                                    if (implementedType.FullName == implementedSearchType)
                                    {
                                        string currentPath = System.Environment.CurrentDirectory;
                                        FileInfo oFileInfo = new FileInfo(dllFileNameFull);
                                        string dllPath = oFileInfo.Directory.FullName;
                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                System.Environment.CurrentDirectory = dllPath;
                                            }
                                            catch (Exception exChangePath)
                                            {
                                            }
                                        }

                                        try
                                        {
                                            if (result == null) result = new List<ISenderData>();
                                            result.Add((ISenderData)oAssembly.CreateInstance(singleFileType.FullName));
                                        }
                                        catch (Exception exCreateInstance)
                                        {
                                        }

                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                System.Environment.CurrentDirectory = currentPath;
                                            }
                                            catch (Exception exReturnPath)
                                            {
                                            }
                                        }
                                    }
                                    if (result != null) break;
                                }
                                if (result != null) break;
                            }
                            if (result != null) break;
                        }
                    }
                    catch (System.Reflection.ReflectionTypeLoadException exLoad)
                    {
                        errorLoadDll = exLoad;
                    }
                    catch (Exception ex)
                    {
                        errorLoadDll = ex;
                    }
                }
            }
            catch (Exception exGenericGetInstance)
            {
            }

            return result;

        }
        public static ISenderData GetSenderData(string nomeDll)
        {
            ISenderData result = null;
            System.Reflection.Assembly oAssembly = null;

            try
            {

                string path = GetDirApp();
                List<string> listDll = new List<string>() { nomeDll };
                string implementedSearchType = "libBordero.ISenderData";
                bool findFile = false;
                Exception errorLoadDll = null;
                foreach (string dllFileName in listDll)
                {
                    errorLoadDll = null;
                    try
                    {
                        string dllFileNameFull = path + @"\" + dllFileName;
                        if (System.IO.File.Exists(dllFileNameFull))
                        {
                            findFile = true;
                            oAssembly = System.Reflection.Assembly.LoadFile(dllFileNameFull);
                            Type[] FileTypes = oAssembly.GetTypes();
                            System.Diagnostics.FileVersionInfo AssemblyVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(dllFileNameFull);
                            string AssemblyName = AssemblyVersion.OriginalFilename;
                            foreach (Type singleFileType in FileTypes)
                            {
                                Type[] interfaceTypes = singleFileType.GetInterfaces();
                                for (int indexInterface = 0; indexInterface < interfaceTypes.Length; indexInterface++)
                                {
                                    Type implementedType = interfaceTypes[indexInterface];
                                    if (implementedType.FullName == implementedSearchType)
                                    {
                                        string currentPath = System.Environment.CurrentDirectory;
                                        FileInfo oFileInfo = new FileInfo(dllFileNameFull);
                                        string dllPath = oFileInfo.Directory.FullName;
                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                System.Environment.CurrentDirectory = dllPath;
                                            }
                                            catch (Exception exChangePath)
                                            {
                                            }
                                        }

                                        try
                                        {
                                            result = (ISenderData)oAssembly.CreateInstance(singleFileType.FullName);
                                        }
                                        catch (Exception exCreateInstance)
                                        {
                                        }

                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                System.Environment.CurrentDirectory = currentPath;
                                            }
                                            catch (Exception exReturnPath)
                                            {
                                            }
                                        }
                                    }
                                    if (result != null) break;
                                }
                                if (result != null) break;
                            }
                            if (result != null) break;
                        }
                    }
                    catch (System.Reflection.ReflectionTypeLoadException exLoad)
                    {
                        errorLoadDll = exLoad;
                    }
                    catch (Exception ex)
                    {
                        errorLoadDll = ex;
                    }
                }
            }
            catch (Exception exGenericGetInstance)
            {
            }

            return result;
        }

        private static string GetDirApp()
        {
            System.IO.FileInfo oFileInfoApp = new System.IO.FileInfo(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            string path = oFileInfoApp.Directory.FullName;
            return path;
        }
    }

}
