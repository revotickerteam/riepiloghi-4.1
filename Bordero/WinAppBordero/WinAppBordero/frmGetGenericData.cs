﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace WinAppBordero
{
    public partial class frmGetGenericData : WinAppBordero.frmBorderoBase
    {
        public List<clsItemGenericData> Data = new List<clsItemGenericData>();
        public ToolTip ToolTipImportant = null;

        private Control firstControlFocus = null;
        private delegateOnShownEvent DelegateOnShow { get; set; }

        public frmGetGenericData(string title, List<clsItemGenericData> data, delegateOnShownEvent delegateOnShow = null)
        {
            InitializeComponent();
            this.DelegateOnShow = delegateOnShow;
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.Text = title;
            this.Data = data;
            this.Font = new Font("Calibri", 12, FontStyle.Regular);
            this.AutoScroll = true;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.ControlBox = false;
            this.InitControlsData();
            this.Shown += FrmGetGenericData_Shown;
        }

        private void FrmGetGenericData_Shown(object sender, EventArgs e)
        {
            if (this.DelegateOnShow != null)
                this.DelegateOnShow(this);
            if (this.firstControlFocus != null)
                this.firstControlFocus.Focus();
        }

        private void InitControlsData()
        {
            this.ToolTipImportant = new ToolTip();
            this.ToolTipImportant.IsBalloon = true;
            Panel MainPanel = new Panel();
            this.Controls.Add(MainPanel);
            MainPanel.Dock = DockStyle.Top;
            MainPanel.BringToFront();

            foreach (clsItemGenericData item in this.Data)
            {
                if (item.Visible)
                    this.AddNewPanel(item, MainPanel);
            }

            int widthLbl = 0;
            int heightItem = 0;
            foreach (Panel pnl in MainPanel.Controls)
            {
                var linqLbl = from lbl in pnl.Controls.OfType<clsLabel>() select lbl;
                foreach (clsLabel lbl in linqLbl)
                {
                    System.Drawing.Graphics g = lbl.CreateGraphics();
                    Size size = g.MeasureString(lbl.Text, lbl.Font).ToSize();
                    widthLbl = System.Math.Max(size.Width + 5, widthLbl);
                    if (size.Height + 5 > lbl.Parent.Height)
                        lbl.Parent.Height = size.Height + 5;
                    g.Dispose();
                }
            }

            foreach (Panel pnl in MainPanel.Controls)
            {
                heightItem = System.Math.Max(heightItem, pnl.Height);
            }
            int nMaxBottom = 0;
            foreach (Panel pnl in MainPanel.Controls)
            {
                var linqLbl = from lbl in pnl.Controls.OfType<clsLabel>() select lbl;
                foreach (clsLabel lbl in linqLbl)
                {
                    lbl.Width = widthLbl;
                }
                nMaxBottom = System.Math.Max(nMaxBottom, pnl.Bottom);
            }

            MainPanel.Height = MainPanel.Controls.Count * (heightItem + 4);

            this.ClientSize = new Size(800, 400);
            MainPanel.AutoScroll = true;

            Panel pnlButtons = new Panel();
            pnlButtons.Dock = DockStyle.Top;
            pnlButtons.Padding = new Padding(3);
            pnlButtons.Height = 50;
            this.Controls.Add(pnlButtons);
            pnlButtons.BringToFront();

            clsButtonBase btn = new clsButtonBase();
            btn.Text = "Abbandona";
            btn.Click += (sender, e) => { this.DialogResult = DialogResult.Abort; };
            btn.Dock = DockStyle.Left;
            btn.Width = (pnlButtons.ClientSize.Width - pnlButtons.Padding.Horizontal) / 2;
            pnlButtons.Controls.Add(btn);
            btn.BringToFront();
            this.CancelButton = btn;

            btn = new clsButtonBase();
            btn.Text = "Conferma";
            btn.Click += (sender, e) => { this.DialogResult = DialogResult.OK; };
            btn.Dock = DockStyle.Fill;
            pnlButtons.Controls.Add(btn);
            btn.BringToFront();
            this.AcceptButton = btn;

            if (this.ClientSize.Height > pnlButtons.Bottom + this.Padding.Vertical)
            {
                this.ClientSize = new Size(this.ClientSize.Width, pnlButtons.Bottom + this.Padding.Vertical);
            }
        }

        private void AddNewPanel(clsItemGenericData item, Panel MainPanel)
        {
            Panel pnl;
            clsLabel lbl;
            TextBox txt;
            DateTimePicker dtp;

            pnl = new Panel();
            pnl.Dock = DockStyle.Top;
            pnl.Padding = new Padding(3);
            pnl.BorderStyle = BorderStyle.FixedSingle;
            MainPanel.Controls.Add(pnl);
            pnl.BringToFront();

            lbl = new clsLabel();
            lbl.Text = item.Description;
            lbl.TextAlign = ContentAlignment.MiddleLeft;
            lbl.AutoSize = false;
            lbl.AutoEllipsis = true;
            lbl.Dock = DockStyle.Left;
            pnl.Controls.Add(lbl);
            lbl.BringToFront();

            if (!string.IsNullOrEmpty(item.Important))
            {
                this.ToolTipImportant.SetToolTip(lbl, item.Important);
                lbl.Font = new Font(lbl.Font.FontFamily, lbl.Font.Size, FontStyle.Bold);
            }


            if (item.IsComboMode)
            {
                ComboBox comboBox = new ComboBox();
                if (this.firstControlFocus == null) this.firstControlFocus = comboBox;
                comboBox.Tag = item;
                comboBox.Enabled = !item.Readonly;
                comboBox.TabStop = comboBox.Enabled;
                comboBox.DropDownStyle = item.ComboStyle;
                comboBox.Dock = DockStyle.Fill;
                pnl.Controls.Add(comboBox);
                comboBox.BringToFront();

                if (!string.IsNullOrEmpty(item.Important))
                {
                    this.ToolTipImportant.SetToolTip(comboBox, item.Important);
                    comboBox.Font = new Font(comboBox.Font.FontFamily, comboBox.Font.Size, FontStyle.Bold);
                }

                foreach (clsItemGenericData itemInCombo in item.ComboValues)
                {
                    comboBox.Items.Add(itemInCombo);
                    if (item.Value != null && item.Value.ToString() == itemInCombo.Description)
                        comboBox.SelectedIndex = comboBox.Items.Count - 1;
                }

                pnl.Height = comboBox.Height + pnl.Padding.Vertical;

                if (!item.Readonly && item.ComboStyle != ComboBoxStyle.DropDownList)
                {

                    comboBox.TextChanged += (sender, e) =>
                    {
                        ComboBox cmbSender = (ComboBox)sender;
                        clsItemGenericData itemSender = (clsItemGenericData)cmbSender.Tag;
                        if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                        {
                            int valSender = 0;
                            clsUtility.CheckTryParseInt(cmbSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                        {
                            decimal valSender = 0;
                            clsUtility.CheckTryParseDecimal(cmbSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else
                            itemSender.Value = cmbSender.Text;
                    };

                    if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                    {
                        comboBox.KeyPress += (sender, e) =>
                        {
                            KeyPressEventArgs kE = (KeyPressEventArgs)e;
                            if (!"0123456789".Contains(kE.KeyChar.ToString()))
                            {
                                kE.KeyChar = '0';
                                kE.Handled = false;
                            }
                        };
                    }
                    else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                    {
                        comboBox.KeyPress += (sender, e) =>
                        {
                            KeyPressEventArgs kE = (KeyPressEventArgs)e;
                            if (kE.KeyChar.ToString() == ".")
                            {
                                kE.KeyChar = ',';
                                kE.Handled = false;
                            }
                        };
                    }
                }
                else if (!item.Readonly && item.ComboStyle == ComboBoxStyle.DropDownList)
                {
                    comboBox.SelectedValueChanged += (sender, e) =>
                    {
                        ComboBox cmbSender = (ComboBox)sender;
                        clsItemGenericData itemSender = (clsItemGenericData)cmbSender.Tag;
                        if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                        {
                            int valSender = 0;
                            clsUtility.CheckTryParseInt(cmbSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                        {
                            decimal valSender = 0;
                            clsUtility.CheckTryParseDecimal(cmbSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else
                            itemSender.Value = ((clsItemGenericData)cmbSender.SelectedItem).Value;
                    };
                }
            }
            else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeDate ||
                     item.DataType == clsItemGenericData.EnumGenericDataType.TypeDateTime)
            {
                dtp = new DateTimePicker();
                if (this.firstControlFocus == null) this.firstControlFocus = dtp;
                dtp.Format = DateTimePickerFormat.Long;
                if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeDateTime)
                {
                    dtp.CustomFormat = "dddd dd MMMM yyyy HH:mm";
                    dtp.Format = DateTimePickerFormat.Custom;
                    if (((DateTime)item.Value) < dtp.MinDate)
                        dtp.Value = DateTime.Now;
                    else
                        dtp.Value = ((DateTime)item.Value);
                }
                else
                {
                    if (((DateTime)item.Value).Date < dtp.MinDate)
                        dtp.Value = DateTime.Now.Date;
                    else
                        dtp.Value = ((DateTime)item.Value).Date;
                }
                dtp.Tag = item;
                dtp.Enabled = !item.Readonly;
                dtp.TabStop = dtp.Enabled;
                dtp.Dock = DockStyle.Fill;
                pnl.Controls.Add(dtp);
                dtp.BringToFront();

                if (!string.IsNullOrEmpty(item.Important))
                {
                    this.ToolTipImportant.SetToolTip(dtp, item.Important);
                    dtp.Font = new Font(dtp.Font.FontFamily, dtp.Font.Size, FontStyle.Bold);
                }

                pnl.Height = dtp.Height + pnl.Padding.Vertical;

                dtp.ValueChanged += (sender, e) =>
                {
                    try
                    {
                        DateTimePicker dtpSender = (DateTimePicker)sender;
                        clsItemGenericData itemSender = (clsItemGenericData)dtpSender.Tag;
                        itemSender.Value = dtpSender.Value;
                    }
                    catch (Exception)
                    {
                    }
                };
            }
            else
            {
                txt = new TextBox();
                if (this.firstControlFocus == null) this.firstControlFocus = txt;
                if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                    txt.Text = ((decimal)item.Value).ToString("#,##0.00");
                else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                    txt.Text = ((int)item.Value).ToString();
                else
                    txt.Text = item.Value.ToString();
                txt.Tag = item;
                txt.Enabled = !item.Readonly;
                txt.TabStop = txt.Enabled;
                txt.Dock = DockStyle.Fill;
                pnl.Controls.Add(txt);
                txt.BringToFront();

                if (!string.IsNullOrEmpty(item.Important))
                {
                    this.ToolTipImportant.SetToolTip(txt, item.Important);
                    txt.Font = new Font(txt.Font.FontFamily, txt.Font.Size, FontStyle.Bold);
                }

                pnl.Height = txt.Height + pnl.Padding.Vertical;

                if (!item.Readonly)
                {

                    txt.TextChanged += (sender, e) =>
                    {
                        TextBox txtSender = (TextBox)sender;
                        clsItemGenericData itemSender = (clsItemGenericData)txtSender.Tag;
                        if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                        {
                            int valSender = 0;
                            clsUtility.CheckTryParseInt(txtSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                        {
                            decimal valSender = 0;
                            clsUtility.CheckTryParseDecimal(txtSender.Text, out valSender);
                            itemSender.Value = valSender;
                        }
                        else
                            itemSender.Value = txtSender.Text;
                    };

                    txt.GotFocus += (sender, e) =>
                    {
                        TextBox txtSender = (TextBox)sender;
                        txtSender.SelectAll();
                    };

                    if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumInt)
                    {
                        txt.KeyPress += (sender, e) =>
                        {
                            KeyPressEventArgs kE = (KeyPressEventArgs)e;
                            if (!"0123456789".Contains(kE.KeyChar.ToString()))
                            {
                                kE.KeyChar = '0';
                                kE.Handled = false;
                            }
                        };
                    }
                    else if (item.DataType == clsItemGenericData.EnumGenericDataType.TypeNumDecimal)
                    {
                        txt.KeyPress += (sender, e) =>
                        {
                            KeyPressEventArgs kE = (KeyPressEventArgs)e;
                            if (kE.KeyChar.ToString() == ".")
                            {
                                kE.KeyChar = ',';
                                kE.Handled = false;
                            }
                        };
                    }
                }
            }


        }

        public static object MenuGenerico(string title, Dictionary<object, string> options, Point? startupPosition = null, Icon iconMenu = null, delegateOnShownEvent delegateOnShow = null)
        {
            object result = null;
            Form frmMenu = new Form();
            frmMenu.Font = new Font("Calibri", 12, FontStyle.Regular);
            if (startupPosition != null)
            {
                frmMenu.StartPosition = FormStartPosition.Manual;
                frmMenu.Location = (Point)startupPosition;
            }
            else
                frmMenu.StartPosition = FormStartPosition.CenterScreen;
            if (iconMenu != null)
                frmMenu.Icon = iconMenu;
            frmMenu.FormBorderStyle = FormBorderStyle.FixedDialog;
            frmMenu.ControlBox = true;
            frmMenu.MinimizeBox = false;
            frmMenu.MaximizeBox = false;
            frmMenu.AutoScroll = true;
            frmMenu.Size = new Size(300, 400);
            frmMenu.Text = title;
            clsButtonBase btn;
            bool findAbort = false;
            int height = frmMenu.Padding.Vertical;
            foreach (KeyValuePair<object, string> item in options)
            {
                btn = new clsButtonBase();
                btn.Text = item.Value;
                btn.Dock = DockStyle.Top;
                btn.Size = new Size(100, 50);
                height += btn.Height;
                findAbort = item.Key == null;
                btn.Tag = item.Key;
                frmMenu.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (sender, e) =>
                {
                    clsButtonBase btnSender = (clsButtonBase)sender;
                    ((Form)btnSender.Parent).Tag = btnSender.Tag;
                    if (((Form)btnSender.Parent).Tag != null)
                        ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                };
            }



            if (!findAbort)
            {
                btn = new clsButtonBase();
                btn.Text = "Abbandona";
                btn.Dock = DockStyle.Top;
                btn.Size = new Size(100, 50);
                height += btn.Height;
                btn.Tag = null;
                frmMenu.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (sender, e) =>
                {
                    clsButtonBase btnSender = (clsButtonBase)sender;
                    ((Form)btnSender.Parent).Tag = null;
                    ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                };
            }

            if (frmMenu.ClientSize.Height > height)
            {
                frmMenu.ClientSize = new Size(frmMenu.ClientSize.Width, height);
            }

            if (delegateOnShow != null)
                frmMenu.Shown += (o, e) => { delegateOnShow(frmMenu); };

            if (frmMenu.ShowDialog() == DialogResult.OK)
            {
                result = frmMenu.Tag;
            }
            frmMenu.Close();
            frmMenu.Dispose();

            return result;
        }
    }

}

