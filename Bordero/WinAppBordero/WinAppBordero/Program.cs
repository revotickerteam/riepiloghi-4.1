﻿using libBordero;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAppBordero
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                bool lcontinue = true;
                
                foreach (string line in args)
                {
                    if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#") && line == "MONTHLY")
                    {
                        if (DateTime.Now.Day != 1)
                        {
                            lcontinue = false;
                            BorderoAuto.WriteAutoLogExecution(false, "Da eseguire ogni primo del mese: oggi NO");
                            break;
                        }
                    }
                }

                if (lcontinue)
                {
                    BorderoAuto.StartExecutionLog();
                    BorderoAuto borderoAuto = new BorderoAuto();
                    libBordero.clsBorderoServiceConfig serviceConfig = libBordero.clsBorderoServiceConfigUtility.GetServiceConfig();
                    if (borderoAuto.Init(args, true, serviceConfig == null))
                    {
                        Exception oError = null;
                        bool findData = false;
                        borderoAuto.Esecuzione(out oError, out findData, false, null, serviceConfig);
                    }
                }
            }
            else 
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmStartBordero());
            }
        }
    }
}
