﻿
namespace WinAppBordero
{
    partial class frmLogin
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.lblLogin = new WinAppBordero.clsLabel();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.pnlPsw = new System.Windows.Forms.Panel();
            this.txtPsw = new System.Windows.Forms.TextBox();
            this.lblPsw = new WinAppBordero.clsLabel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnAbort = new WinAppBordero.clsButtonBase();
            this.btnOK = new WinAppBordero.clsButtonBase();
            this.pnlLogin.SuspendLayout();
            this.pnlPsw.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLogin
            // 
            this.pnlLogin.Controls.Add(this.txtLogin);
            this.pnlLogin.Controls.Add(this.lblLogin);
            this.pnlLogin.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogin.Location = new System.Drawing.Point(0, 0);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Padding = new System.Windows.Forms.Padding(3);
            this.pnlLogin.Size = new System.Drawing.Size(382, 33);
            this.pnlLogin.TabIndex = 0;
            // 
            // lblLogin
            // 
            this.lblLogin.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblLogin.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblLogin.Icon = null;
            this.lblLogin.ImageIcon = null;
            this.lblLogin.Location = new System.Drawing.Point(3, 3);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(81, 27);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Login:";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblLogin.VerticalText = "";
            // 
            // txtLogin
            // 
            this.txtLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogin.Location = new System.Drawing.Point(84, 3);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(295, 27);
            this.txtLogin.TabIndex = 1;
            this.txtLogin.WordWrap = false;
            // 
            // pnlPsw
            // 
            this.pnlPsw.Controls.Add(this.txtPsw);
            this.pnlPsw.Controls.Add(this.lblPsw);
            this.pnlPsw.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPsw.Location = new System.Drawing.Point(0, 33);
            this.pnlPsw.Name = "pnlPsw";
            this.pnlPsw.Padding = new System.Windows.Forms.Padding(3);
            this.pnlPsw.Size = new System.Drawing.Size(382, 33);
            this.pnlPsw.TabIndex = 1;
            // 
            // txtPsw
            // 
            this.txtPsw.AcceptsReturn = true;
            this.txtPsw.AcceptsTab = true;
            this.txtPsw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPsw.Location = new System.Drawing.Point(84, 3);
            this.txtPsw.Name = "txtPsw";
            this.txtPsw.PasswordChar = '*';
            this.txtPsw.Size = new System.Drawing.Size(295, 27);
            this.txtPsw.TabIndex = 1;
            this.txtPsw.WordWrap = false;
            // 
            // lblPsw
            // 
            this.lblPsw.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblPsw.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPsw.Icon = null;
            this.lblPsw.ImageIcon = null;
            this.lblPsw.Location = new System.Drawing.Point(3, 3);
            this.lblPsw.Name = "lblPsw";
            this.lblPsw.Size = new System.Drawing.Size(81, 27);
            this.lblPsw.TabIndex = 0;
            this.lblPsw.Text = "Password:";
            this.lblPsw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPsw.VerticalText = "";
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Controls.Add(this.btnAbort);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(0, 66);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(382, 49);
            this.pnlButtons.TabIndex = 2;
            // 
            // btnAbort
            // 
            this.btnAbort.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnAbort.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(3, 3);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(105, 43);
            this.btnAbort.TabIndex = 1;
            this.btnAbort.Text = "Abbandona";
            this.btnAbort.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOK.Location = new System.Drawing.Point(274, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(105, 43);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "Accedi";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // frmLogin
            // 
            this.CancelButton = this.btnAbort;
            this.ClientSize = new System.Drawing.Size(382, 115);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.pnlPsw);
            this.Controls.Add(this.pnlLogin);
            this.Name = "frmLogin";
            this.Text = "Login";
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.pnlPsw.ResumeLayout(false);
            this.pnlPsw.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLogin;
        private clsLabel lblLogin;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Panel pnlPsw;
        private System.Windows.Forms.TextBox txtPsw;
        private clsLabel lblPsw;
        private System.Windows.Forms.Panel pnlButtons;
        private clsButtonBase btnOK;
        private clsButtonBase btnAbort;
    }
}
