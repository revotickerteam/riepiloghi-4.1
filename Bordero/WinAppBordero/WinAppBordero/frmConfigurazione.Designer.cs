﻿
namespace WinAppBordero
{
    partial class frmConfigurazione
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfigurazione));
            this.pnlConfigurazione = new System.Windows.Forms.Panel();
            this.pnlALTRO = new System.Windows.Forms.Panel();
            this.pnlALTROItems = new System.Windows.Forms.Panel();
            this.pnlALTROMenu = new System.Windows.Forms.Panel();
            this.lblAltro = new WinAppBordero.clsLabel();
            this.btnALTROminus = new WinAppBordero.clsButtonBase();
            this.imageListAddRemove = new System.Windows.Forms.ImageList(this.components);
            this.btnALTROplus = new WinAppBordero.clsButtonBase();
            this.btnExpandCollapseALTRO = new WinAppBordero.clsButtonBase();
            this.imageListOpenClose = new System.Windows.Forms.ImageList(this.components);
            this.pnlDEM = new System.Windows.Forms.Panel();
            this.pnlDEMItems = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDEM = new WinAppBordero.clsLabel();
            this.btnDEMminus = new WinAppBordero.clsButtonBase();
            this.btnDEMplus = new WinAppBordero.clsButtonBase();
            this.btnExpandCollapseDEM = new WinAppBordero.clsButtonBase();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnImport = new WinAppBordero.clsButtonBase();
            this.btnExport = new WinAppBordero.clsButtonBase();
            this.btnABORT = new WinAppBordero.clsButtonBase();
            this.btnSAVE = new WinAppBordero.clsButtonBase();
            this.pnlGeneralParameters = new System.Windows.Forms.Panel();
            this.chkAsteriscoSupplementi = new System.Windows.Forms.CheckBox();
            this.chkTuttiTitoli = new System.Windows.Forms.CheckBox();
            this.lblInizioEventi = new WinAppBordero.clsLabel();
            this.oraInizioEventi = new System.Windows.Forms.NumericUpDown();
            this.chkUnBorderoPerSpettacolo = new System.Windows.Forms.CheckBox();
            this.pnlConfigurazione.SuspendLayout();
            this.pnlALTRO.SuspendLayout();
            this.pnlALTROMenu.SuspendLayout();
            this.pnlDEM.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlGeneralParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oraInizioEventi)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlConfigurazione
            // 
            this.pnlConfigurazione.AutoScroll = true;
            this.pnlConfigurazione.Controls.Add(this.pnlALTRO);
            this.pnlConfigurazione.Controls.Add(this.pnlDEM);
            this.pnlConfigurazione.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConfigurazione.Location = new System.Drawing.Point(0, 88);
            this.pnlConfigurazione.Name = "pnlConfigurazione";
            this.pnlConfigurazione.Size = new System.Drawing.Size(784, 473);
            this.pnlConfigurazione.TabIndex = 2;
            this.pnlConfigurazione.Visible = false;
            // 
            // pnlALTRO
            // 
            this.pnlALTRO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlALTRO.Controls.Add(this.pnlALTROItems);
            this.pnlALTRO.Controls.Add(this.pnlALTROMenu);
            this.pnlALTRO.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlALTRO.Location = new System.Drawing.Point(0, 126);
            this.pnlALTRO.Name = "pnlALTRO";
            this.pnlALTRO.Size = new System.Drawing.Size(784, 126);
            this.pnlALTRO.TabIndex = 2;
            // 
            // pnlALTROItems
            // 
            this.pnlALTROItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlALTROItems.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlALTROItems.Location = new System.Drawing.Point(0, 40);
            this.pnlALTROItems.Name = "pnlALTROItems";
            this.pnlALTROItems.Size = new System.Drawing.Size(782, 39);
            this.pnlALTROItems.TabIndex = 0;
            this.pnlALTROItems.Visible = false;
            // 
            // pnlALTROMenu
            // 
            this.pnlALTROMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlALTROMenu.Controls.Add(this.lblAltro);
            this.pnlALTROMenu.Controls.Add(this.btnALTROminus);
            this.pnlALTROMenu.Controls.Add(this.btnALTROplus);
            this.pnlALTROMenu.Controls.Add(this.btnExpandCollapseALTRO);
            this.pnlALTROMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlALTROMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlALTROMenu.Name = "pnlALTROMenu";
            this.pnlALTROMenu.Size = new System.Drawing.Size(782, 40);
            this.pnlALTROMenu.TabIndex = 5;
            // 
            // lblAltro
            // 
            this.lblAltro.BackColor = System.Drawing.Color.Gainsboro;
            this.lblAltro.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblAltro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAltro.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltro.ForeColor = System.Drawing.Color.Black;
            this.lblAltro.Icon = null;
            this.lblAltro.ImageIcon = null;
            this.lblAltro.Location = new System.Drawing.Point(275, 0);
            this.lblAltro.Name = "lblAltro";
            this.lblAltro.Size = new System.Drawing.Size(505, 38);
            this.lblAltro.TabIndex = 3;
            this.lblAltro.Text = "Altre quote";
            this.lblAltro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAltro.VerticalText = "";
            // 
            // btnALTROminus
            // 
            this.btnALTROminus.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnALTROminus.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnALTROminus.ImageIndex = 1;
            this.btnALTROminus.ImageList = this.imageListAddRemove;
            this.btnALTROminus.Location = new System.Drawing.Point(187, 0);
            this.btnALTROminus.Name = "btnALTROminus";
            this.btnALTROminus.Size = new System.Drawing.Size(88, 38);
            this.btnALTROminus.TabIndex = 2;
            this.btnALTROminus.Text = "Rimuovi";
            this.btnALTROminus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnALTROminus, "Rimuovi quota");
            this.btnALTROminus.UseVisualStyleBackColor = true;
            this.btnALTROminus.Visible = false;
            this.btnALTROminus.Click += new System.EventHandler(this.btnALTROminus_Click);
            // 
            // imageListAddRemove
            // 
            this.imageListAddRemove.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListAddRemove.ImageStream")));
            this.imageListAddRemove.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListAddRemove.Images.SetKeyName(0, "miniplus.ico");
            this.imageListAddRemove.Images.SetKeyName(1, "miniminus.ico");
            // 
            // btnALTROplus
            // 
            this.btnALTROplus.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnALTROplus.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnALTROplus.ImageIndex = 0;
            this.btnALTROplus.ImageList = this.imageListAddRemove;
            this.btnALTROplus.Location = new System.Drawing.Point(94, 0);
            this.btnALTROplus.Name = "btnALTROplus";
            this.btnALTROplus.Size = new System.Drawing.Size(93, 38);
            this.btnALTROplus.TabIndex = 1;
            this.btnALTROplus.Text = "Aggiungi";
            this.btnALTROplus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnALTROplus, "Aggiungi quota");
            this.btnALTROplus.UseVisualStyleBackColor = true;
            this.btnALTROplus.Visible = false;
            this.btnALTROplus.Click += new System.EventHandler(this.btnALTROplus_Click);
            // 
            // btnExpandCollapseALTRO
            // 
            this.btnExpandCollapseALTRO.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnExpandCollapseALTRO.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExpandCollapseALTRO.ImageIndex = 0;
            this.btnExpandCollapseALTRO.ImageList = this.imageListOpenClose;
            this.btnExpandCollapseALTRO.Location = new System.Drawing.Point(0, 0);
            this.btnExpandCollapseALTRO.Name = "btnExpandCollapseALTRO";
            this.btnExpandCollapseALTRO.Size = new System.Drawing.Size(94, 38);
            this.btnExpandCollapseALTRO.TabIndex = 0;
            this.btnExpandCollapseALTRO.Text = "Espandi";
            this.btnExpandCollapseALTRO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnExpandCollapseALTRO, "Espandi Riduci");
            this.btnExpandCollapseALTRO.UseVisualStyleBackColor = true;
            this.btnExpandCollapseALTRO.Click += new System.EventHandler(this.btnExpandCollapseALTRO_Click);
            // 
            // imageListOpenClose
            // 
            this.imageListOpenClose.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListOpenClose.ImageStream")));
            this.imageListOpenClose.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListOpenClose.Images.SetKeyName(0, "arrowDn.png");
            this.imageListOpenClose.Images.SetKeyName(1, "arrowUp.png");
            // 
            // pnlDEM
            // 
            this.pnlDEM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDEM.Controls.Add(this.pnlDEMItems);
            this.pnlDEM.Controls.Add(this.panel1);
            this.pnlDEM.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDEM.Location = new System.Drawing.Point(0, 0);
            this.pnlDEM.Name = "pnlDEM";
            this.pnlDEM.Size = new System.Drawing.Size(784, 126);
            this.pnlDEM.TabIndex = 1;
            // 
            // pnlDEMItems
            // 
            this.pnlDEMItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDEMItems.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDEMItems.Location = new System.Drawing.Point(0, 40);
            this.pnlDEMItems.Name = "pnlDEMItems";
            this.pnlDEMItems.Size = new System.Drawing.Size(782, 26);
            this.pnlDEMItems.TabIndex = 0;
            this.pnlDEMItems.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblDEM);
            this.panel1.Controls.Add(this.btnDEMminus);
            this.panel1.Controls.Add(this.btnDEMplus);
            this.panel1.Controls.Add(this.btnExpandCollapseDEM);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 40);
            this.panel1.TabIndex = 5;
            // 
            // lblDEM
            // 
            this.lblDEM.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDEM.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblDEM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDEM.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDEM.ForeColor = System.Drawing.Color.Black;
            this.lblDEM.Icon = null;
            this.lblDEM.ImageIcon = null;
            this.lblDEM.Location = new System.Drawing.Point(275, 0);
            this.lblDEM.Name = "lblDEM";
            this.lblDEM.Size = new System.Drawing.Size(505, 38);
            this.lblDEM.TabIndex = 3;
            this.lblDEM.Text = "Diritto d\'autore (DEM)";
            this.lblDEM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDEM.VerticalText = "";
            // 
            // btnDEMminus
            // 
            this.btnDEMminus.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnDEMminus.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDEMminus.ImageIndex = 1;
            this.btnDEMminus.ImageList = this.imageListAddRemove;
            this.btnDEMminus.Location = new System.Drawing.Point(187, 0);
            this.btnDEMminus.Name = "btnDEMminus";
            this.btnDEMminus.Size = new System.Drawing.Size(88, 38);
            this.btnDEMminus.TabIndex = 2;
            this.btnDEMminus.Text = "Rimuovi";
            this.btnDEMminus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnDEMminus, "Rimuovi quota");
            this.btnDEMminus.UseVisualStyleBackColor = true;
            this.btnDEMminus.Visible = false;
            this.btnDEMminus.Click += new System.EventHandler(this.btnDEMminus_Click);
            // 
            // btnDEMplus
            // 
            this.btnDEMplus.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnDEMplus.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDEMplus.ImageIndex = 0;
            this.btnDEMplus.ImageList = this.imageListAddRemove;
            this.btnDEMplus.Location = new System.Drawing.Point(94, 0);
            this.btnDEMplus.Name = "btnDEMplus";
            this.btnDEMplus.Size = new System.Drawing.Size(93, 38);
            this.btnDEMplus.TabIndex = 1;
            this.btnDEMplus.Text = "Aggiundi";
            this.btnDEMplus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnDEMplus, "Aggiungi quota");
            this.btnDEMplus.UseVisualStyleBackColor = true;
            this.btnDEMplus.Visible = false;
            this.btnDEMplus.Click += new System.EventHandler(this.btnDEMplus_Click);
            // 
            // btnExpandCollapseDEM
            // 
            this.btnExpandCollapseDEM.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnExpandCollapseDEM.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExpandCollapseDEM.ImageIndex = 0;
            this.btnExpandCollapseDEM.ImageList = this.imageListOpenClose;
            this.btnExpandCollapseDEM.Location = new System.Drawing.Point(0, 0);
            this.btnExpandCollapseDEM.Name = "btnExpandCollapseDEM";
            this.btnExpandCollapseDEM.Size = new System.Drawing.Size(94, 38);
            this.btnExpandCollapseDEM.TabIndex = 0;
            this.btnExpandCollapseDEM.Text = "Espandi";
            this.btnExpandCollapseDEM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExpandCollapseDEM.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnExpandCollapseDEM, "Espandi Riduci");
            this.btnExpandCollapseDEM.UseVisualStyleBackColor = true;
            this.btnExpandCollapseDEM.Click += new System.EventHandler(this.btnExpandCollapseDEM_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // btnImport
            // 
            this.btnImport.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnImport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.Location = new System.Drawing.Point(493, 3);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(87, 80);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "Importa";
            this.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnImport, "Importa Configurazione da file .INI o .JSON");
            this.btnImport.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.Location = new System.Drawing.Point(580, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(87, 80);
            this.btnExport.TabIndex = 7;
            this.btnExport.Text = "Esporta";
            this.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnExport, "Esporta Configurazione su file JSON");
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnABORT
            // 
            this.btnABORT.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnABORT.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnABORT.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnABORT.Image = ((System.Drawing.Image)(resources.GetObject("btnABORT.Image")));
            this.btnABORT.Location = new System.Drawing.Point(3, 3);
            this.btnABORT.Name = "btnABORT";
            this.btnABORT.Size = new System.Drawing.Size(81, 80);
            this.btnABORT.TabIndex = 0;
            this.btnABORT.Text = "Chiudi";
            this.btnABORT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnABORT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnABORT, "Chiudi");
            this.btnABORT.UseVisualStyleBackColor = true;
            // 
            // btnSAVE
            // 
            this.btnSAVE.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnSAVE.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSAVE.Image = ((System.Drawing.Image)(resources.GetObject("btnSAVE.Image")));
            this.btnSAVE.Location = new System.Drawing.Point(667, 3);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(112, 80);
            this.btnSAVE.TabIndex = 5;
            this.btnSAVE.Text = "Salva";
            this.btnSAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSAVE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnSAVE, "Salva Configurazione");
            this.btnSAVE.UseVisualStyleBackColor = true;
            // 
            // pnlGeneralParameters
            // 
            this.pnlGeneralParameters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGeneralParameters.Controls.Add(this.chkAsteriscoSupplementi);
            this.pnlGeneralParameters.Controls.Add(this.btnImport);
            this.pnlGeneralParameters.Controls.Add(this.btnExport);
            this.pnlGeneralParameters.Controls.Add(this.btnABORT);
            this.pnlGeneralParameters.Controls.Add(this.btnSAVE);
            this.pnlGeneralParameters.Controls.Add(this.chkTuttiTitoli);
            this.pnlGeneralParameters.Controls.Add(this.lblInizioEventi);
            this.pnlGeneralParameters.Controls.Add(this.oraInizioEventi);
            this.pnlGeneralParameters.Controls.Add(this.chkUnBorderoPerSpettacolo);
            this.pnlGeneralParameters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGeneralParameters.Location = new System.Drawing.Point(0, 0);
            this.pnlGeneralParameters.Name = "pnlGeneralParameters";
            this.pnlGeneralParameters.Padding = new System.Windows.Forms.Padding(3);
            this.pnlGeneralParameters.Size = new System.Drawing.Size(784, 88);
            this.pnlGeneralParameters.TabIndex = 3;
            // 
            // chkAsteriscoSupplementi
            // 
            this.chkAsteriscoSupplementi.AutoSize = true;
            this.chkAsteriscoSupplementi.Location = new System.Drawing.Point(90, 59);
            this.chkAsteriscoSupplementi.Name = "chkAsteriscoSupplementi";
            this.chkAsteriscoSupplementi.Size = new System.Drawing.Size(400, 33);
            this.chkAsteriscoSupplementi.TabIndex = 8;
            this.chkAsteriscoSupplementi.Text = "Evidenzia i biglietti con supplemento";
            this.chkAsteriscoSupplementi.UseVisualStyleBackColor = true;
            // 
            // chkTuttiTitoli
            // 
            this.chkTuttiTitoli.AutoSize = true;
            this.chkTuttiTitoli.Location = new System.Drawing.Point(90, 32);
            this.chkTuttiTitoli.Name = "chkTuttiTitoli";
            this.chkTuttiTitoli.Size = new System.Drawing.Size(360, 33);
            this.chkTuttiTitoli.TabIndex = 2;
            this.chkTuttiTitoli.Text = "Visualizza tutti i titoli dell\'evento";
            this.chkTuttiTitoli.UseVisualStyleBackColor = true;
            // 
            // lblInizioEventi
            // 
            this.lblInizioEventi.AutoSize = true;
            this.lblInizioEventi.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblInizioEventi.Icon = null;
            this.lblInizioEventi.ImageIcon = null;
            this.lblInizioEventi.Location = new System.Drawing.Point(336, 12);
            this.lblInizioEventi.Name = "lblInizioEventi";
            this.lblInizioEventi.Size = new System.Drawing.Size(205, 29);
            this.lblInizioEventi.TabIndex = 3;
            this.lblInizioEventi.Text = "Orario Inizio Eventi:";
            this.lblInizioEventi.VerticalText = "";
            // 
            // oraInizioEventi
            // 
            this.oraInizioEventi.Location = new System.Drawing.Point(367, 35);
            this.oraInizioEventi.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.oraInizioEventi.Name = "oraInizioEventi";
            this.oraInizioEventi.Size = new System.Drawing.Size(65, 37);
            this.oraInizioEventi.TabIndex = 4;
            // 
            // chkUnBorderoPerSpettacolo
            // 
            this.chkUnBorderoPerSpettacolo.AutoSize = true;
            this.chkUnBorderoPerSpettacolo.Location = new System.Drawing.Point(90, 5);
            this.chkUnBorderoPerSpettacolo.Name = "chkUnBorderoPerSpettacolo";
            this.chkUnBorderoPerSpettacolo.Size = new System.Drawing.Size(298, 33);
            this.chkUnBorderoPerSpettacolo.TabIndex = 1;
            this.chkUnBorderoPerSpettacolo.Text = "Un bordero per spettacolo";
            this.chkUnBorderoPerSpettacolo.UseVisualStyleBackColor = true;
            // 
            // frmConfigurazione
            // 
            this.CancelButton = this.btnABORT;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlConfigurazione);
            this.Controls.Add(this.pnlGeneralParameters);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfigurazione";
            this.ShowInTaskbar = false;
            this.Text = "Configurazione";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlConfigurazione.ResumeLayout(false);
            this.pnlALTRO.ResumeLayout(false);
            this.pnlALTROMenu.ResumeLayout(false);
            this.pnlDEM.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlGeneralParameters.ResumeLayout(false);
            this.pnlGeneralParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oraInizioEventi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlConfigurazione;
        private System.Windows.Forms.Panel pnlDEM;
        private System.Windows.Forms.Panel pnlDEMItems;
        private System.Windows.Forms.Panel pnlALTRO;
        private System.Windows.Forms.Panel pnlALTROItems;
        private System.Windows.Forms.Panel pnlALTROMenu;
        private clsButtonBase btnALTROplus;
        private clsButtonBase btnALTROminus;
        private clsLabel lblAltro;
        private System.Windows.Forms.Panel panel1;
        private clsButtonBase btnDEMplus;
        private clsLabel lblDEM;
        private clsButtonBase btnDEMminus;
        private clsButtonBase btnExpandCollapseDEM;
        private clsButtonBase btnExpandCollapseALTRO;
        private System.Windows.Forms.ImageList imageListAddRemove;
        private System.Windows.Forms.ImageList imageListOpenClose;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel pnlGeneralParameters;
        private clsButtonBase btnImport;
        private clsButtonBase btnExport;
        private clsButtonBase btnABORT;
        private clsButtonBase btnSAVE;
        private System.Windows.Forms.CheckBox chkTuttiTitoli;
        private clsLabel lblInizioEventi;
        private System.Windows.Forms.NumericUpDown oraInizioEventi;
        private System.Windows.Forms.CheckBox chkUnBorderoPerSpettacolo;
        private System.Windows.Forms.CheckBox chkAsteriscoSupplementi;
    }
}
