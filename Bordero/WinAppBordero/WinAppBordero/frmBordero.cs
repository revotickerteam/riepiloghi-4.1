﻿using libBordero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class frmBordero : WinAppBordero.frmBorderoBase
    {
        private string NomeConnessione { get; set; }
        private string DescConnessione { get; set; }

        private BorderoFileConfig BorderoFileConfig { get; set; }

        List<Bordero.clsBordQuota> quoteOpzionali { get; set; }

        private libBordero.clsBorderoServiceConfig ServiceConfig { get; set; }
        private libBordero.clsConfigurazioneGruppi configurazioneGruppi = null;

        public frmBordero(string nomeConnessione, string descConnessione, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            InitializeComponent();

            this.NomeConnessione = nomeConnessione;
            this.DescConnessione = descConnessione;
            this.ServiceConfig = serviceConfig;
            if (!string.IsNullOrEmpty(this.DescConnessione) && !string.IsNullOrWhiteSpace(this.DescConnessione))
                this.Text += " " + this.DescConnessione;
            this.BorderoFileConfig = WinAppBordero.BorderoFileConfig.GetBorderoFileConfigFromFile();
            this.Shown += FrmBordero_Shown;
            this.HelpButtonClicked += Bordero_HelpButtonClicked;
        }

        private void Bordero_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            clsBorderoHelp.ShowHelp("Elaborazione");
        }

        private void FrmBordero_Shown(object sender, EventArgs e)
        {
            this.SetModalita();
            this.InitFilterMulti();
            this.radioGIORNO.CheckedChanged += RadioGIORNO_PERIODO_CheckedChanged;
            this.radioPERIODO.CheckedChanged += RadioGIORNO_PERIODO_CheckedChanged;
            this.btnReadData.Click += BtnReadData_Click;

            try
            {
                Exception error = null;
                configurazioneGruppi = libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error);
                
            }
            catch (Exception)
            {
            }

            try
            {
                List<ISenderData> senders = FinderInterfaceSenderData.GetSendersList();
                if (senders != null && senders.Count > 0)
                {
                    if (configurazioneGruppi == null)
                        configurazioneGruppi = new clsConfigurazioneGruppi();
                    senders.ForEach(senderData =>
                    {
                        clsItemGruppo gruppo = senderData.GetGruppo(null);
                        if (gruppo != null && configurazioneGruppi.Gruppi.FirstOrDefault(g => g.NomeDll.Equals(gruppo.NomeDll)) == null)
                        {
                            gruppo.NomeConnessione = this.NomeConnessione;
                            configurazioneGruppi.Gruppi.Add(gruppo);
                        }
                    });
                }
            }
            catch (Exception)
            {
            }


            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("GRUPPI"))
            {
                btnGruppi.Enabled = (configurazioneGruppi != null && configurazioneGruppi.Gruppi != null && configurazioneGruppi.Gruppi.Count > 0);
                if (configurazioneGruppi != null && configurazioneGruppi.Gruppi != null && configurazioneGruppi.Gruppi.Count == 1)
                    btnGruppi.Text = $"Gruppi:{configurazioneGruppi.Gruppi[0].Descrizione}";
            }
            btnGruppi.Visible = btnGruppi.Enabled;
            if (btnGruppi.Enabled)
                this.btnGruppi.Click += BtnGruppi_Click;
            this.chkMulti.CheckedChanged += ChkMulti_CheckedChanged;
            this.chkFiltri.CheckedChanged += ChkMulti_CheckedChanged;
            this.chkOpzionali.Checked = false;
            this.pnlQuoteOpzionali.ClientSize = new Size(this.pnlQuoteOpzionali.Width, this.chkOpzionali.Height + this.pnlQuoteOpzionali.Padding.Vertical);
            this.ClientSize = new Size(this.ClientSize.Width, this.pnlPeriodo.Height + this.pnlQuoteOpzionali.Height + this.pnlMULTI.Height + this.pnlFILTRI.Height);
            this.chkListOpzionali.Visible = false;
            this.chkOpzionali.CheckedChanged += ChkOpzionali_CheckedChanged;
            this.chkListOpzionali.MouseClick += ChkListOpzionali_MouseClick;
            this.CenterToScreen();
            this.dtINIZIO.Select();
        }

        private void ChkListOpzionali_MouseClick(object sender, MouseEventArgs e)
        {
            var where = this.chkListOpzionali.HitTest(e.Location);
            if (where.Location == ListViewHitTestLocations.Label)
                where.Item.Checked = !where.Item.Checked;
        }

        private void ChkOpzionali_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkOpzionali.Enabled)
            {
                if (this.quoteOpzionali == null)
                {
                    this.chkOpzionali.Enabled = false;
                    this.quoteOpzionali = this.GetQuoteOpzionali();
                    bool existQuoteOpzionali = (quoteOpzionali != null && quoteOpzionali.Count > 0);
                    if (existQuoteOpzionali)
                    {
                        //this.chkListOpzionali.SuspendLayout();
                        this.quoteOpzionali.ForEach(q =>
                        {
                            ListViewItem item = new ListViewItem(q.Descr);
                            item.Tag = q;
                            this.chkListOpzionali.Items.Add(item);
                        });
                        this.chkOpzionali.Checked = true;
                        this.chkTe.Checked = true;
                        this.chkSI.Checked = true;
                        this.pnlQuoteOpzionali.Visible = true;
                        this.chkListOpzionali.Visible = true;
                        this.pnlQuoteOpzionali.Height = 128;
                        this.chkListOpzionali.Enabled = true;
                        this.ClientSize = new Size(this.ClientSize.Width, this.pnlPeriodo.Height + this.pnlQuoteOpzionali.Height + this.pnlMULTI.Height + this.pnlFILTRI.Height);
                        this.chkOpzionali.Enabled = true;
                        this.chkListOpzionali.View = View.SmallIcon;
                        this.chkListOpzionali.ResumeLayout();
                        this.chkListOpzionali.PerformLayout();
                        this.chkListOpzionali.Refresh();
                        this.Refresh();
                        this.CenterToScreen();
                        Application.DoEvents();
                    }
                    else
                    {
                        this.chkOpzionali.Enabled = false;
                        this.chkListOpzionali.Items.Clear();
                        this.chkListOpzionali.Enabled = false;
                        this.pnlQuoteOpzionali.Visible = false;
                        this.ClientSize = new Size(this.ClientSize.Width, this.pnlPeriodo.Height + this.pnlMULTI.Height + this.pnlFILTRI.Height);
                        this.CenterToScreen();
                        MessageBox.Show("Nessuna quota opzionale impostata", "Quote Opzionali", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    if (!this.chkOpzionali.Checked && this.chkListOpzionali.Items.Count > 0)
                    {
                        foreach (ListViewItem item in this.chkListOpzionali.Items)
                        {
                            item.Checked = false;
                        }
                        this.pnlQuoteOpzionali.ClientSize = new Size(this.pnlQuoteOpzionali.Width, this.chkOpzionali.Height + this.pnlQuoteOpzionali.Padding.Vertical);
                    }
                    else
                    {
                        this.pnlQuoteOpzionali.Height = 128;
                        this.chkTe.Checked = true;
                        this.chkSI.Checked = true;
                    }
                    this.chkListOpzionali.PerformLayout();
                    this.chkListOpzionali.Refresh();
                    this.ClientSize = new Size(this.ClientSize.Width, this.pnlPeriodo.Height + this.pnlQuoteOpzionali.Height + this.pnlMULTI.Height + this.pnlFILTRI.Height);
                    this.Refresh();
                    this.CenterToScreen();
                    Application.DoEvents();
                }
            }
        }

        private void BtnGruppi_Click(object sender, EventArgs e)
        {
            this.SetAuto(this.configurazioneGruppi);
        }

        private void ChkMulti_CheckedChanged(object sender, EventArgs e)
        {
            this.SetModalita();
        }

        private void BtnReadData_Click(object sender, EventArgs e)
        {
            this.btnReadData.Enabled = false;
            System.Data.DataSet oDataSetBordero = null;
            Int64 IdRichiestaBordero = 0;
            libBordero.Bordero.clsBordQuoteConfig config = null;
            if (CheckSelections())
            {
                if (this.ServiceConfig == null)
                {
                    if (this.GetData(out oDataSetBordero, out IdRichiestaBordero, out config))
                        this.ShowData(oDataSetBordero, IdRichiestaBordero, config);
                }
                else
                {
                    if (this.GetDataFromService(out oDataSetBordero, out IdRichiestaBordero, out config))
                        this.ShowData(oDataSetBordero, IdRichiestaBordero, config);
                }
            }
            this.btnReadData.Enabled = true;
        }

        private void RadioGIORNO_PERIODO_CheckedChanged(object sender, EventArgs e)
        {
            this.SetModalita();
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        #region Modalita giorno o periodico

        private void SetAuto(libBordero.clsConfigurazioneGruppi parConfigGruppi = null)
        {
            Exception error = null;
            BorderoAuto borderoAuto = new BorderoAuto();
            Dictionary<object, string> items = new Dictionary<object, string>();
            libBordero.clsConfigurazioneGruppi configurazioneGruppiMenu = (parConfigGruppi != null ? parConfigGruppi : libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error));
            foreach (libBordero.clsItemGruppo gruppo in configurazioneGruppiMenu.Gruppi)
                items.Add(gruppo.Descrizione, gruppo.Descrizione);
            object value = null;
            if (items.Count == 1 || frmMenuBordero.GetMenuCheck("Gruppi da eseguire", items, ref value))
            {
                Dictionary<object, string> lista = (items.Count == 1 ? items : (Dictionary<object, string>)value);
                string descDateSelezionate = $"{(this.radioPERIODO.Checked && !this.dtINIZIO.Value.Date.Equals(this.dtFINE.Value.Date) ? "periodo manuale da " + this.dtINIZIO.Value.Date.ToShortDateString() + " a " + this.dtFINE.Value.Date.ToShortDateString() : "il giorno selezionato " + this.dtINIZIO.Value.Date.ToShortDateString())}";
                bool lDateManuali = false;
                bool lAskOgniGruppo = false;

                clsItemGruppo gruppoSingoloSelezionato = null;

                if (lista.Count == 1)
                    gruppoSingoloSelezionato = configurazioneGruppiMenu.Gruppi.FirstOrDefault(g => g.Descrizione == lista.First().Value);
                value = null;
                items = new Dictionary<object, string>();

                items.Add("AUTO", $"{(lista.Count == 1 && gruppoSingoloSelezionato != null ? gruppoSingoloSelezionato.Descrizione + " " + gruppoSingoloSelezionato.DescrizioneModalitaPeriodo : "Tutti i gruppi in base alla propria configurazione")}");
                items.Add("MANUALE", $"{(lista.Count == 1 && gruppoSingoloSelezionato != null ? gruppoSingoloSelezionato.Descrizione : "Tutti i gruppi")} {descDateSelezionate}");
                if (lista.Count > 1)
                    items.Add("ASK", $"Ripeti la richiesta per ogni gruppo");
                if (frmMenuBordero.GetMenu($"Modalità di calcolo {(lista.Count == 1 ? "del gruppo" : "dei gruppi")}", items, ref value))
                {
                    lDateManuali = (value.ToString() == "MANUALE");
                    lAskOgniGruppo = (value.ToString() == "ASK");

                    libBordero.clsConfigurazioneGruppi configurazioneGruppiEsecuzione = new libBordero.clsConfigurazioneGruppi();
                    foreach (KeyValuePair<object, string> gruppo in lista)
                    {
                        libBordero.clsItemGruppo gruppoEsecuzione = configurazioneGruppiMenu.Gruppi.Find(g => g.Descrizione == gruppo.Value);
                        if (gruppoEsecuzione != null)
                        {
                            bool appendGruppo = true;
                            if (lAskOgniGruppo)
                            {
                                value = null;
                                items = new Dictionary<object, string>();
                                items.Add("AUTO", $"{gruppoEsecuzione.Descrizione} {gruppoEsecuzione.DescrizioneModalitaPeriodo}");
                                items.Add("MANUALE", $"{gruppoEsecuzione.Descrizione} {descDateSelezionate}");
                                appendGruppo = frmMenuBordero.GetMenu($"modalità di calcolo {(lista.Count == 1 ? "del gruppo" : "dei gruppi")}", items, ref value);
                                if (appendGruppo)
                                    lDateManuali = (value.ToString() == "MANUALE");
                                else if (MessageBox.Show("Abbandonare l'esecuzione dei gruppi ?", "Gruppi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    configurazioneGruppi.Gruppi.Clear();
                                    break;
                                }
                            }

                            if (appendGruppo)
                            {
                                if (lDateManuali)
                                {
                                    gruppoEsecuzione.PeriodoEsterno = true;
                                    gruppoEsecuzione.Inizio = this.dtINIZIO.Value.Date;
                                    if (this.radioGIORNO.Checked)
                                        gruppoEsecuzione.Fine = this.dtINIZIO.Value.Date;
                                    else
                                        gruppoEsecuzione.Fine = this.dtFINE.Value.Date;
                                }
                                else
                                    gruppoEsecuzione.PeriodoEsterno = false;
                                configurazioneGruppiEsecuzione.Gruppi.Add(gruppoEsecuzione);
                            }
                        }
                    }

                    if (configurazioneGruppi.Gruppi != null && configurazioneGruppi.Gruppi.Count > 0 && borderoAuto.Init(configurazioneGruppiEsecuzione, this.ServiceConfig == null))
                    {
                        Exception oError = null;
                        bool findData = false;
                        Func<clsItemFullData> funcGetData = null;
                        if (this.ServiceConfig != null)
                        {
                            funcGetData = GetDataFromServiceInAuto;
                        }
                        borderoAuto.Esecuzione(out oError, out findData, true, funcGetData, this.ServiceConfig);
                    }
                }


                
            }
        }

        private void SetModalita()
        {
            if (this.radioGIORNO.Checked)
            {
                this.lblDal.Text = "Giorno:";
                this.lblAl.Visible = false;
                this.dtFINE.Visible = false;
                this.dtFINE.Enabled = false;
            }
            else
            {
                this.lblDal.Text = "Dal:";
                this.lblAl.Visible = true;
                this.dtFINE.Visible = true;
                this.dtFINE.Enabled = true;
            }

            if (!this.chkFiltri.Checked)
            {
                foreach (clsCheckBox itemCheck in this.pnlFilterTags.Controls)
                {
                    itemCheck.Checked = false;
                }
            }
            this.pnlFilterTags.Enabled = (this.chkFiltri.Checked);

            if (!this.chkMulti.Checked)
            {
                foreach (clsCheckBox itemCheck in this.pnlMultiTags.Controls)
                {
                    itemCheck.Checked = false;
                }
            }
            this.pnlMultiTags.Enabled = (this.chkMulti.Checked);
            this.InitFilterMulti();
        }

        #endregion

        #region Impostazioni ulteriori

        private void InitFilterMulti()
        {
            this.pnlMultiTags.Controls.Clear();
            this.pnlFilterTags.Controls.Clear();
            foreach (string key in GetFiltersKeys())
            {
                string descr = GetFilterKeyDescr(key);
                if (!string.IsNullOrEmpty(descr))
                {
                    clsCheckBox checkBox = new clsCheckBox();
                    checkBox.AutoSize = true;
                    checkBox.Tag = key;
                    checkBox.Text = descr;
                    this.pnlMultiTags.Controls.Add(checkBox);
                    checkBox.BringToFront();


                    checkBox = new clsCheckBox();
                    checkBox.AutoSize = true;
                    checkBox.Tag = key;
                    checkBox.Text = descr;
                    this.pnlFilterTags.Controls.Add(checkBox);
                    checkBox.BringToFront();
                }
                else if (this.radioPERIODO.Checked && key == "BORDERO_FILT_GIORNI")
                {
                    clsCheckBox checkBox = new clsCheckBox();
                    checkBox.AutoSize = true;
                    checkBox.Tag = "BORDERO_FILT_GIORNI";
                    checkBox.Text = "Giorni";
                    this.pnlMultiTags.Controls.Add(checkBox);
                    checkBox.BringToFront();
                }
            }

        }

        private string[] GetFiltersKeys()
        {
            string[] aListaTabelleFiltro = new  string[] { "BORDERO_FILT_GIORNI", "BORDERO_FILT_ORGANIZZATORI", "BORDERO_FILT_TIPI_EVENTO", "BORDERO_FILT_SALE", "BORDERO_FILT_TITOLI", "BORDERO_FILT_PRODUTTORI", "BORDERO_FILT_NOLEGGIATORI" };
            return aListaTabelleFiltro;
        }

        public static string GetFilterKeyDescr(string filterKey)
        {
            string result = "";
            switch (filterKey)
            {
                case "BORDERO_FILT_ORGANIZZATORI": { result = "Organizzatori"; break; }
                case "BORDERO_FILT_TIPI_EVENTO": { result = "Tipi Evento"; break; }
                case "BORDERO_FILT_SALE": { result = "Sale/Luoghi Manifestazione"; break; }
                case "BORDERO_FILT_TITOLI": { result = "Titoli eventi"; break; }
                case "BORDERO_FILT_PRODUTTORI": { result = "Produttori"; break; }
                case "BORDERO_FILT_NOLEGGIATORI": { result = "Distributori"; break; }
            }
            return result;
        }

        private string GetStringMulti()
        {
            string result = "";
            if (this.chkMulti.Checked)
            {
                foreach (clsCheckBox checkItem in this.pnlMultiTags.Controls)
                {
                    if (checkItem.Checked)
                        result += (string.IsNullOrEmpty(result) ? "" : ",") + checkItem.Tag.ToString();
                }
            }
            return result;
        }

        #endregion

        #region Elaborazione

        private bool CheckSelections()
        {
            bool result = true;
            if (chkMulti.Checked)
            {
                if (string.IsNullOrEmpty(GetStringMulti()))
                {
                    result = false;
                    MessageBox.Show("Impostare come suddividere i bordero.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return result;
                }
            }
            if (this.radioPERIODO.Checked)
            {
                result = this.dtFINE.Value.Date >= this.dtINIZIO.Value.Date;
                if (!result)
                {
                    MessageBox.Show("Impostare un periodo valido.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return result;
                }
            }

            return result;
        }

        private List<Bordero.clsBordQuota> GetQuoteOpzionali()
        {
            List<Bordero.clsBordQuota> result = new List<Bordero.clsBordQuota>();
            frmWait oFrmWait = new frmWait();
            Bordero.clsBordQuoteConfig configurazioneQuoteOpzionali = null;
            oFrmWait.Message = "Lettura Configurazione...";
            oFrmWait.Show();
            Application.DoEvents();

            
            Exception oError = null;

            IConnection conn = null;
            try
            {
                if (this.ServiceConfig == null)
                {
                    conn = libBordero.Bordero.GetConnection(this.NomeConnessione, out oError);
                    configurazioneQuoteOpzionali = libBordero.Bordero.GetConfigurazione(conn, 1);
                }
                else
                {
                    PayLoadRiepiloghi oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token };
                    ResponseRiepiloghi oResponse = libBordero.clsBorderoServiceConfigUtility.GetConfigurazioneFromService(oRequest, this.ServiceConfig.Url);
                    string errDescr = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "ConfigurazioneBordero");
                    if (!string.IsNullOrEmpty(errDescr))
                        throw new Exception(errDescr);
                    configurazioneQuoteOpzionali = oResponse.Data.ConfigurazioneBordero;
                }

                if (configurazioneQuoteOpzionali != null && configurazioneQuoteOpzionali.QuoteDEM != null)
                {
                    configurazioneQuoteOpzionali.QuoteDEM.ForEach(q =>
                    {
                        result.Add(q);
                    });
                }

                if (configurazioneQuoteOpzionali != null && configurazioneQuoteOpzionali.AltreQuote != null)
                {
                    configurazioneQuoteOpzionali.AltreQuote.ForEach(q =>
                    {
                        result.Add(q);
                    });
                }
            }
            catch (Exception exConnection)
            {
                oError = exConnection;
            }

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

            oFrmWait.Close();
            oFrmWait.Dispose();
            if (oError != null)
            {
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        private bool GetData(out System.Data.DataSet oDataSetBordero, out Int64 IdRichiestaBordero, out libBordero.Bordero.clsBordQuoteConfig config)
        {
            frmWait oFrmWait = new frmWait();
            config = null;
            oFrmWait.Message = "Calcolo in corso...";
            oFrmWait.Show();
            Application.DoEvents();

            bool result = false;
            Exception oError = null;

            IdRichiestaBordero = 0;
            oDataSetBordero = null;
            IConnection conn = null;
            try
            {
                oFrmWait.Message = "Connessione...";
                conn = libBordero.Bordero.GetConnection(this.NomeConnessione, out oError);
                libBordero.Bordero.DeleteDatiRichiestaBorderoFull(conn, false, 1);
                conn.BeginTransaction();

                try
                {
                    oFrmWait.Message = "Calcolo...";
                    DateTime dInizio = this.dtINIZIO.Value.Date;
                    DateTime dFine = (this.radioPERIODO.Checked ? this.dtFINE.Value.Date : this.dtINIZIO.Value.Date);

                    config = libBordero.Bordero.GetConfigurazione(conn, 1);

                    if (this.chkOpzionali.Checked)
                    {
                        List<string> tabelleDaCancellareNotChecked = new List<string>() { "BORD_QUOTE_TE", "BORD_QUOTE_SPETINTRA", "BORD_QUOTE_DEM", "BORD_QUOTE_ALTRO", "BORD_QUOTE" };
                        foreach (ListViewItem item in this.chkListOpzionali.Items)
                        {
                            long idQuota = ((Bordero.clsBordQuota)item.Tag).IdQuota;
                            if (!item.Checked)
                            {
                                // (esiste sempre RollBackFinale)
                                // ELIMINO LA QUOTA IN MODO CHE NON POSSA ESSERE CALCOLATA
                                foreach (string tabellaQuote in tabelleDaCancellareNotChecked)
                                {
                                    conn.ExecuteNonQuery(string.Format("DELETE FROM {0} WHERE IDQUOTA = :pIDQUOTA", tabellaQuote), new clsParameters(":pIDQUOTA", idQuota));
                                }
                            }
                            else
                            {
                                // (esiste sempre RollBackFinale)
                                // IMPOSTO LA QUOTA PER QUALSIASI TIPO EVENTO IN MODO CHE SIA IL PIU' APPLICABILE POSSIBILE
                                // VERRA' CALCOLATA SE LE REGOLE DELLE RIGHE SONO COMPATIBILI
                                if (!this.chkTe.Checked)
                                {
                                    conn.ExecuteNonQuery("DELETE FROM BORD_QUOTE_TE WHERE IDQUOTA = :pIDQUOTA", new clsParameters(":pIDQUOTA", idQuota));
                                }
                                if (!this.chkSI.Checked)
                                {
                                    conn.ExecuteNonQuery("UPDATE BORD_QUOTE_SPETINTRA SET SPETTACOLO_INTRATTENIMENTO = 'X' WHERE IDQUOTA = :pIDQUOTA", new clsParameters(":pIDQUOTA", idQuota));
                                }
                            }
                        }

                    }
                    oDataSetBordero = libBordero.Bordero.GetBordero(conn, 1, dInizio, dFine, ref IdRichiestaBordero, out oError);

                    result = oDataSetBordero != null && oDataSetBordero.Tables != null && oDataSetBordero.Tables.Count > 0 && 
                             oDataSetBordero.Tables.Contains("BORDERO_DATI") && 
                             oDataSetBordero.Tables.Contains("BORDERO_ABBONAMENTI") && 
                             (oDataSetBordero.Tables["BORDERO_DATI"].Rows.Count > 0 || oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0) && 
                             oError == null;

                    if (!result && oError == null)
                    {
                        MessageBox.Show("Nessun dato.", "Elaborazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    oError = ex;
                    result = false;
                }

                conn.RollBack();    // BUTTO SEMPRE VIA TUTTO
            }
            catch (Exception exConnection)
            {
                oError = exConnection;
                result = false;
            }

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

            oFrmWait.Close();
            oFrmWait.Dispose();
            if (oError != null)
            {
                result = false;
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;
        }

        private clsItemFullData GetDataFromServiceInAuto()
        {
            clsItemFullData data = new clsItemFullData();
            System.Data.DataSet oDataSetBordero = null;
            Int64 IdRichiestaBordero = 0;
            libBordero.Bordero.clsBordQuoteConfig config = null;
            if (this.GetDataFromService(out oDataSetBordero, out IdRichiestaBordero, out config))
            {
                data.DataSetBordero = oDataSetBordero;
                data.IdRichiestaBordero = IdRichiestaBordero; 
                data.Config = config;
                data.Result = true;
            }
            else
                data.Result =false;
            return data;
        }

        private bool GetDataFromService(out System.Data.DataSet oDataSetBordero, out Int64 IdRichiestaBordero, out libBordero.Bordero.clsBordQuoteConfig config)
        {
            oDataSetBordero = null;
            IdRichiestaBordero = 0;
            config = null;

            frmWait oFrmWait = new frmWait();
            config = null;
            oFrmWait.Message = "Calcolo in corso...";
            oFrmWait.Show();
            Application.DoEvents();

            bool result = false;
            Exception oError = null;

            PayLoadRiepiloghi oRequest = null;
            ResponseRiepiloghi oResponse = null;

            List<long> quoteManualiEscluse = null;
            List<long> quoteManualiTe = null;
            List<long> quoteManualiSi = null;

            if (this.chkOpzionali.Checked)
            {
                oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token };
                oResponse = libBordero.clsBorderoServiceConfigUtility.GetConfigurazioneFromService(oRequest, this.ServiceConfig.Url);
                string err = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "ConfigurazioneBordero");

                if (string.IsNullOrEmpty(err))
                    config = oResponse.Data.ConfigurazioneBordero;

                foreach (ListViewItem item in this.chkListOpzionali.Items)
                {
                    long idQuota = ((Bordero.clsBordQuota)item.Tag).IdQuota;
                    if (!item.Checked)
                    {
                        if (quoteManualiEscluse == null) quoteManualiEscluse = new List<long>();
                        quoteManualiEscluse.Add(idQuota);
                    }
                    else
                    {
                        if (!this.chkTe.Checked)
                        {
                            if (quoteManualiTe == null) quoteManualiTe = new List<long>();
                            quoteManualiTe.Add(idQuota);
                        }
                        if (!this.chkSI.Checked)
                        {
                            if (quoteManualiSi == null) quoteManualiSi = new List<long>();
                            quoteManualiSi.Add(idQuota);
                        }
                    }
                }

            }


            DateTime dInizio = this.dtINIZIO.Value.Date;
            DateTime dFine = (this.radioPERIODO.Checked ? this.dtFINE.Value.Date : this.dtINIZIO.Value.Date);
            oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token, DataInizio = dInizio, DataFine = dFine, QuoteManualiEscluse = quoteManualiEscluse, QuoteManualiTe = quoteManualiTe, QuoteManualiSi = quoteManualiSi };
            oResponse = libBordero.clsBorderoServiceConfigUtility.CalcBordero(oRequest, this.ServiceConfig.Url);
            string errCalc = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "DataSetBordero");
            if (!string.IsNullOrEmpty(errCalc))
            {
                MessageBox.Show(errCalc, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                oDataSetBordero = oResponse.Data.DataSetBordero;
                config = oResponse.Data.ConfigurazioneBordero;
                IdRichiestaBordero = oResponse.Data.IdRichiestaBordero;

                if (oDataSetBordero != null && oDataSetBordero.Tables != null && oDataSetBordero.Tables.Count > 0 &&
                    ((oDataSetBordero.Tables.Contains("BORDERO_DATI") && oDataSetBordero.Tables["BORDERO_DATI"].Rows.Count > 0)
                     ||
                     (oDataSetBordero.Tables.Contains("BORDERO_ABBONAMENTI") && oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0)))
                {
                    result = true;
                }
            }
            oFrmWait.Close();
            oFrmWait.Dispose();
            if (oError != null)
            {
                result = false;
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!result)
                MessageBox.Show("Nessun dato.", "Elaborazione", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return result;
        }

        private void ShowData(System.Data.DataSet oDataSetBordero, Int64 IdRichiestaBordero, libBordero.Bordero.clsBordQuoteConfig config)
        {
            frmWait oFrmWait = new frmWait();
            oFrmWait.Message = "Generazione...";
            oFrmWait.Show();
            Application.DoEvents();

            Exception oError = null;
            bool noData = false;
            List<clsItemPdfBordero> ItemsPdfBordero = new List<clsItemPdfBordero>();
            string cTipoBorderoMultiplo = this.GetStringMulti();
            List<libBordero.Bordero.clsInfoDataSet> listaDati = null;

            try
            {
                /* vincoli se this.BorderoFileConfig != null */
                List<string> listaNomiFiltriConfig = new List<string>();
                if (this.BorderoFileConfig != null)
                {
                    if (this.BorderoFileConfig.Organizzatori != null)
                        listaNomiFiltriConfig.Add("BORDERO_FILT_ORGANIZZATORI");
                    if (this.BorderoFileConfig.CodiciLocali != null)
                        listaNomiFiltriConfig.Add("BORDERO_FILT_SALE");
                }
                if (this.chkFiltri.Checked)
                {
                    List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();

                    foreach (clsCheckBox itemCheckFilter in this.pnlFilterTags.Controls)
                    {
                        if (itemCheckFilter.Checked)
                        {
                            DataTable table = oDataSetBordero.Tables[itemCheckFilter.Tag.ToString()];
                            if (this.BorderoFileConfig != null && listaNomiFiltriConfig.Contains(itemCheckFilter.Tag.ToString()))
                            {
                                table = new DataTable();
                                foreach (DataColumn col in oDataSetBordero.Tables[itemCheckFilter.Tag.ToString()].Columns)
                                {
                                    table.Columns.Add(new DataColumn(col.ColumnName, col.DataType));
                                }
                                foreach (DataRow row in oDataSetBordero.Tables[itemCheckFilter.Tag.ToString()].Rows)
                                {
                                    string fieldName = "";
                                    List<string> listToCheck = null;
                                    bool append = false;
                                    switch (itemCheckFilter.Tag.ToString())
                                    {
                                        case "BORDERO_FILT_ORGANIZZATORI": { fieldName = "CODICE_FISCALE_ORGANIZZATORE"; listToCheck = this.BorderoFileConfig.Organizzatori; break; }
                                        case "BORDERO_FILT_SALE": { fieldName = "CODICE_LOCALE"; listToCheck = this.BorderoFileConfig.CodiciLocali; break; }
                                    }
                                    append = (!string.IsNullOrEmpty(fieldName) &&
                                      oDataSetBordero.Tables[itemCheckFilter.Tag.ToString()].Columns.Contains(fieldName) &&
                                      row[fieldName] != null && row[fieldName] != System.DBNull.Value &&
                                      listToCheck.Contains(row[fieldName].ToString())
                                     );
                                    if (append)
                                    {
                                        DataRow rowToAdd = table.NewRow();
                                        foreach (DataColumn col in table.Columns)
                                        {
                                            rowToAdd[col.ColumnName] = row[col.ColumnName];
                                        }
                                        table.Rows.Add(rowToAdd);
                                    }
                                }
                            }
                            if (table != null && table.Rows != null && table.Rows.Count > 0)
                            {
                                frmFiltri frmFilter = new frmFiltri(table, itemCheckFilter.Tag.ToString(), this.BorderoFileConfig == null || !listaNomiFiltriConfig.Contains(itemCheckFilter.Tag.ToString()));
                                if (frmFilter.ShowDialog() == DialogResult.OK)
                                {
                                    List<DataRow> selectedRows = frmFilter.SelectedRows();
                                    if (selectedRows != null)
                                    {
                                        libBordero.Bordero.clsFilterBordero oFiltro = new libBordero.Bordero.clsFilterBordero(itemCheckFilter.Tag.ToString());
                                        foreach (DataRow oRow in selectedRows)
                                        {
                                            oFiltro.RigheFiltro.Add(oRow.ItemArray);
                                        }
                                        oFiltri.Add(oFiltro);
                                    }
                                }
                                frmFilter.Close();
                                frmFilter.Dispose();
                            }
                            else
                            {
                                noData = true;
                                break;
                            }
                            
                        }
                        else if (this.BorderoFileConfig != null && listaNomiFiltriConfig.Contains(itemCheckFilter.Tag.ToString()))
                        {
                            libBordero.Bordero.clsFilterBordero oFiltro = null;

                            listaNomiFiltriConfig.ForEach(nomeFiltro =>
                            {
                                if (nomeFiltro == itemCheckFilter.Tag.ToString())
                                {
                                    oFiltro = new libBordero.Bordero.clsFilterBordero(nomeFiltro);
                                    foreach (DataRow row in oDataSetBordero.Tables[nomeFiltro].Rows)
                                    {
                                        string fieldName = "";
                                        List<string> listToCheck = null;
                                        bool append = false;
                                        switch (nomeFiltro)
                                        {
                                            case "BORDERO_FILT_ORGANIZZATORI": { fieldName = "CODICE_FISCALE_ORGANIZZATORE"; listToCheck = this.BorderoFileConfig.Organizzatori; break; }
                                            case "BORDERO_FILT_SALE": { fieldName = "CODICE_LOCALE"; listToCheck = this.BorderoFileConfig.CodiciLocali; break; }
                                        }
                                        append = (!string.IsNullOrEmpty(fieldName) &&
                                                  oDataSetBordero.Tables[nomeFiltro].Columns.Contains(fieldName) &&
                                                  row[fieldName] != null && row[fieldName] != System.DBNull.Value &&
                                                  listToCheck.Contains(row[fieldName].ToString())
                                                 );
                                        if (append)
                                            oFiltro.RigheFiltro.Add(row.ItemArray);
                                    }
                                    oFiltri.Add(oFiltro);
                                }
                            });
                        }
                    }

                    if (oFiltri.Count > 0)
                    {
                        if (this.BorderoFileConfig != null &&
                            (this.BorderoFileConfig.Organizzatori != null || this.BorderoFileConfig.CodiciLocali != null) &&
                            oFiltri.Find(f => f.RigheFiltro == null || f.RigheFiltro.Count == 0) != null)
                            noData = true;
                        else
                            libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);
                    }
                }
                else if (this.BorderoFileConfig != null && (this.BorderoFileConfig.Organizzatori != null || this.BorderoFileConfig.CodiciLocali != null))
                {
                    List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();
                    libBordero.Bordero.clsFilterBordero oFiltro = null;

                    listaNomiFiltriConfig.ForEach(nomeFiltro =>
                    {
                        oFiltro = new libBordero.Bordero.clsFilterBordero(nomeFiltro);
                        foreach (DataRow row in oDataSetBordero.Tables[nomeFiltro].Rows)
                        {
                            string fieldName = "";
                            List<string> listToCheck = null;
                            bool append = false;
                            switch (nomeFiltro)
                            {
                                case "BORDERO_FILT_ORGANIZZATORI": { fieldName = "CODICE_FISCALE_ORGANIZZATORE"; listToCheck = this.BorderoFileConfig.Organizzatori; break; }
                                case "BORDERO_FILT_SALE": { fieldName = "CODICE_LOCALE"; listToCheck = this.BorderoFileConfig.CodiciLocali; break; }
                            }
                            append = (!string.IsNullOrEmpty(fieldName) &&
                                      oDataSetBordero.Tables[nomeFiltro].Columns.Contains(fieldName) &&
                                      row[fieldName] != null && row[fieldName] != System.DBNull.Value &&
                                      listToCheck.Contains(row[fieldName].ToString())
                                     );
                            if (append)
                                oFiltro.RigheFiltro.Add(row.ItemArray);
                        }
                        oFiltri.Add(oFiltro);
                    });

                    if (oFiltri.Count > 0)
                    {
                        if (oFiltri.Find(f => f.RigheFiltro == null || f.RigheFiltro.Count == 0) != null)
                            noData = true;
                        else
                            libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);
                    }
                    else
                        noData = true;
                }

                if (oError == null && !noData)
                {
                    listaDati = libBordero.Bordero.MultiBorderoStream(cTipoBorderoMultiplo, this.dtINIZIO.Value, this.dtFINE.Value, oDataSetBordero, "", config);

                    foreach (libBordero.Bordero.clsInfoDataSet oInfoDataSet in listaDati)
                    {
                        if (oInfoDataSet.StreamPdf != null)
                        {
                            byte[] data = oInfoDataSet.StreamPdf.ToArray();
                            oInfoDataSet.StreamPdf.Dispose();
                            string cDescFiltro = "";
                            if (!string.IsNullOrEmpty(cTipoBorderoMultiplo))
                            {
                                foreach (KeyValuePair<string, string> itemMult in oInfoDataSet.InfoFiltri)
                                {
                                    cDescFiltro += (string.IsNullOrEmpty(cDescFiltro) ? "" : "\r\n") + itemMult.Value.Trim();
                                }
                            }
                            clsItemPdfBordero itemFile = new clsItemPdfBordero();
                            itemFile.Descrizione = cDescFiltro;
                            itemFile.Buffer = data;
                            ItemsPdfBordero.Add(itemFile);
                        }
                    }
                }
            }
            catch (Exception exGen)
            {
                oError = exGen;
            }

            oFrmWait.Close();
            oFrmWait.Dispose();

            if (oError == null && ItemsPdfBordero != null && ItemsPdfBordero.Count > 0 && !noData)
            {
                if (ItemsPdfBordero.Count > 1)
                {
                    frmBorderoBase frmMenuPdf = new frmBorderoBase();
                    frmMenuPdf.Size = new Size(800, 600);
                    frmMenuPdf.AutoScroll = true;
                    frmMenuPdf.Text = "Borderò";
                    clsButtonBase btn = null;
                    int sizeLine = 40;
                    foreach (clsItemPdfBordero itemPdf in ItemsPdfBordero)
                    {
                        btn = new clsButtonBase();
                        int cntRow = (itemPdf.Descrizione.Contains("\r") ? itemPdf.Descrizione.Split('\r').Length : 1);
                        btn.MinimumSize = new Size(100, sizeLine * cntRow);
                        btn.Text = itemPdf.Descrizione;
                        btn.Tag = itemPdf;
                        btn.Dock = DockStyle.Top;
                        frmMenuPdf.Controls.Add(btn);
                        btn.BringToFront();
                        btn.Click += (s, e) =>
                        {
                            clsItemPdfBordero itemPdf_sender = (clsItemPdfBordero)((Control)s).Tag;
                            System.IO.MemoryStream stream = new System.IO.MemoryStream(itemPdf.Buffer);
                            frmViewPdf frmPdf = new frmViewPdf(stream);
                            frmPdf.Text = itemPdf_sender.Descrizione;
                            frmPdf.ShowDialog();
                            frmPdf.Close();
                            frmPdf.Dispose();
                        };
                    }
                    btn = new clsButtonBase();
                    btn.MinimumSize = new Size(100, 40);
                    btn.Text = "Abbandona";
                    btn.Tag = null;
                    btn.DialogResult = DialogResult.Abort;
                    btn.Dock = DockStyle.Top;
                    frmMenuPdf.Controls.Add(btn);
                    btn.BringToFront();
                    frmMenuPdf.ShowDialog();
                    frmMenuPdf.Close();
                    frmMenuPdf.Dispose();
                }
                else
                {
                    foreach (clsItemPdfBordero itemPdf in ItemsPdfBordero)
                    {
                        System.IO.MemoryStream stream = new System.IO.MemoryStream(itemPdf.Buffer);
                        frmViewPdf frmPdf = new frmViewPdf(stream);
                        if (ItemsPdfBordero.Count > 1)
                            frmPdf.Text = itemPdf.Descrizione;
                        frmPdf.ShowDialog();
                        frmPdf.Close();
                        frmPdf.Dispose();
                    }
                }
            }

            if (oError != null)
            {
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (noData)
                MessageBox.Show("Nessun dato.", "Elaborazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        
    }
}
