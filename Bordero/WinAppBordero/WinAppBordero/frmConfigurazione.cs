﻿using libBordero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class frmConfigurazione : WinAppBordero.frmBorderoBase
    {
        private string NomeConnessione { get; set; }
        private string DescConnessione { get; set; }
        libBordero.Bordero.clsBordQuoteConfig Configurazione { get; set; }

        private usrQuota UsrQuotaSelected { get; set; }

        private string Version = "";

        private libBordero.clsBorderoServiceConfig ServiceConfig { get; set; }

        public frmConfigurazione(string nomeConnessione, string descConnessione, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.NomeConnessione = nomeConnessione;
            this.DescConnessione = descConnessione;
            this.ServiceConfig = serviceConfig;

            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                this.Version = assembly.GetName().Version.ToString();
                this.Text += " " + this.Version;
            }
            catch (Exception)
            {

            }

            if (!string.IsNullOrEmpty(this.DescConnessione) && !string.IsNullOrWhiteSpace(this.DescConnessione))
                this.Text += " " + this.DescConnessione;
            this.Shown += FrmConfigurazione_Shown;
            this.SizeChanged += FrmConfigurazione_SizeChanged;
            this.ClientSizeChanged += FrmConfigurazione_ClientSizeChanged;
            this.HelpButton = true;
            this.HelpButtonClicked += Bordero_HelpButtonClicked;
        }

        private void Bordero_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            clsBorderoHelp.ShowHelp("Configurazione");
        }

        private void FrmConfigurazione_ClientSizeChanged(object sender, EventArgs e)
        {
            if (this.pnlDEMItems.Visible)
            {
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                    usrQuota.ResizeQuota();
            }
            if (this.pnlALTROItems.Visible)
            {
                foreach (usrQuota usrQuota in this.pnlALTROItems.Controls)
                    usrQuota.ResizeQuota();
            }
            this.CenterToScreen();
        }

        private void FrmConfigurazione_SizeChanged(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void FrmConfigurazione_Shown(object sender, EventArgs e)
        {
            this.pnlDEM.AutoSize = true;
            this.pnlDEM.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.pnlDEMItems.AutoSize = true;
            this.pnlDEMItems.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.pnlALTRO.AutoSize = true;
            this.pnlALTRO.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.pnlALTROItems.AutoSize = true;
            this.pnlALTROItems.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            if (this.ReadConfig())
            {
                this.InitDataControls();
            }
            else
            {
                this.Close();
            }
            this.btnSAVE.Click += btnSAVE_Click;
            this.btnExport.Click += btnExport_Click;
            this.btnABORT.Click += btnABORT_Click;
            this.btnImport.Click += btnImport_Click;
        }

        private bool ReadConfig(string File = "")
        {
            bool result = false;
            frmWait oFrmWait = new frmWait();
            oFrmWait.Message = "Configurazione...";
            oFrmWait.Show();
            Application.DoEvents();
            IConnection conn = null;
            Exception oError = null;
            try
            {
                oFrmWait.Message = "Connessione...";
                if (this.ServiceConfig == null)
                    conn = libBordero.Bordero.GetConnection(this.NomeConnessione, out oError);
                oFrmWait.Message = "Lettura configurazione...";
                libBordero.Bordero.clsBordQuoteConfig configurazioneBase = null;
                if (conn != null)
                    configurazioneBase = libBordero.Bordero.GetConfigurazione(conn, 1);
                else
                {
                    PayLoadRiepiloghi oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token };
                    ResponseRiepiloghi oResponse = libBordero.clsBorderoServiceConfigUtility.GetConfigurazioneFromService(oRequest, this.ServiceConfig.Url);
                    string errCalc = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "ConfigurazioneBordero");
                    if (string.IsNullOrEmpty(errCalc))
                        configurazioneBase = oResponse.Data.ConfigurazioneBordero;
                    else
                        throw new Exception(errCalc);
                }

                if (!string.IsNullOrEmpty(File))
                {
                    oFrmWait.Message = "Lettura configurazione da file...";
                    this.Configurazione = clsUtility.ImportFromFile(File, out oError, configurazioneBase.DescrCodiciLocali);
                    this.Configurazione.DescrCampiBordero = new List<libBordero.Bordero.clsDescriptorField>(configurazioneBase.DescrCampiBordero);
                    this.Configurazione.DescrCodiciLocali = new List<libBordero.Bordero.clsDescriptorField>(configurazioneBase.DescrCodiciLocali);
                    this.Configurazione.DescrTipiEvento = new List<libBordero.Bordero.clsDescriptorField>(configurazioneBase.DescrTipiEvento);
                }
                else
                {
                    this.Configurazione = configurazioneBase;
                }
                result = this.Configurazione != null;
            }
            catch (Exception exConn)
            {
                oError = exConn;
            }
            oFrmWait.Close();
            oFrmWait.Dispose();

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

            if (oError != null)
            {
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        

        private bool SaveConfig()
        {
            bool result = false;
            frmWait oFrmWait = new frmWait();
            oFrmWait.Message = "Configurazione...";
            oFrmWait.Show();
            Application.DoEvents();
            IConnection conn = null;
            Exception oError = null;
            try
            {
                oFrmWait.Message = "Connessione...";
                if (this.ServiceConfig == null)
                    conn = libBordero.Bordero.GetConnection(this.NomeConnessione, out oError);
                oFrmWait.Message = "Salvataggio configurazione...";
                libBordero.Bordero.clsBordQuoteConfig configurazione = null;
                if (conn != null)
                    configurazione = libBordero.Bordero.SetConfigurazione(conn, 1, this.Configurazione, out oError);
                else
                {
                    PayLoadRiepiloghi oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token, ConfigurazioneBordero = this.Configurazione };
                    ResponseRiepiloghi oResponse = libBordero.clsBorderoServiceConfigUtility.SetConfigurazioneFromService(oRequest, this.ServiceConfig.Url);
                    string errCalc = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "ConfigurazioneBordero");
                    if (string.IsNullOrEmpty(errCalc))
                        configurazione = oResponse.Data.ConfigurazioneBordero;
                    else
                        throw new Exception(errCalc);
                    //configurazione = libBordero.clsBorderoServiceConfigUtility.SetConfigurazioneFromService(this.Configurazione, 1, out oError);
                }

                result = oError == null;
                if (result)
                    this.Configurazione = configurazione;
            }
            catch (Exception exConn)
            {
                oError = exConn;
            }
            oFrmWait.Close();
            oFrmWait.Dispose();

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

            if (oError != null)
            {
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }

        

        private void ClearControls()
        {
            this.pnlDEMItems.Controls.Clear();
            this.pnlALTROItems.Controls.Clear();
            this.pnlConfigurazione.Visible = false;
        }

        private void InitDataControls()
        {
            this.ClearControls();
            this.ExpandCollapseAltro(false);
            this.ExpandCollapseDEM(false);
            this.pnlConfigurazione.Visible = true;
            this.chkUnBorderoPerSpettacolo.Checked = this.Configurazione.BorderoPerSpettacolo;
            this.chkTuttiTitoli.Checked = !this.Configurazione.TitoloUnico;
            this.chkAsteriscoSupplementi.Checked = this.Configurazione.AsteriscoSupplementi;

            if (this.Configurazione.OraInizioEventi >= this.oraInizioEventi.Minimum &&
                this.Configurazione.OraInizioEventi <= this.oraInizioEventi.Maximum)
                this.oraInizioEventi.Value = this.Configurazione.OraInizioEventi;
            int countDEM = 0;
            int countALTRO = 0;
            if (this.Configurazione.QuoteDEM != null && this.Configurazione.QuoteDEM.Count > 0)
            {
                int index = 0;
                foreach (libBordero.Bordero.clsBordQuota quota in this.Configurazione.QuoteDEM)
                {
                    countDEM += 1;
                    usrQuota usrQuota = new usrQuota(quota, this.Configurazione, this.NomeConnessione, this.ServiceConfig);
                    usrQuota.Dock = DockStyle.Top;
                    usrQuota.AutoSize = true;
                    usrQuota.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    usrQuota.QuotaSelected += UsrQuota_QuotaSelected;
                    usrQuota.QuotaImported += UsrQuota_QuotaImported;
                    index += 1;
                    usrQuota.SetIndex(index);
                    this.pnlDEMItems.Controls.Add(usrQuota);
                    usrQuota.BringToFront();
                    usrQuota.ResizeQuota();
                }
            }

            if (this.Configurazione.AltreQuote != null && this.Configurazione.AltreQuote.Count > 0)
            {
                int index = 0;
                foreach (libBordero.Bordero.clsBordQuota quota in this.Configurazione.AltreQuote)
                {
                    countALTRO += 1;
                    usrQuota usrQuota = new usrQuota(quota, this.Configurazione, this.NomeConnessione, this.ServiceConfig);
                    usrQuota.Dock = DockStyle.Top;
                    usrQuota.AutoSize = true;
                    usrQuota.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    usrQuota.QuotaSelected += UsrQuota_QuotaSelected;
                    usrQuota.QuotaImported += UsrQuota_QuotaImported;
                    index += 1;
                    usrQuota.SetIndex(index);
                    this.pnlALTROItems.Controls.Add(usrQuota);
                    usrQuota.BringToFront();
                    usrQuota.ResizeQuota();
                }
            }

            this.chkUnBorderoPerSpettacolo.CheckedChanged += AssignValuesToConfigurazione;
            this.oraInizioEventi.ValueChanged += AssignValuesToConfigurazione;
            this.chkAsteriscoSupplementi.CheckedChanged += AssignValuesToConfigurazione;
            this.RenumQuote();
            if (countDEM > 0)
                this.ExpandCollapseDEM(true);
            if (countALTRO > 0)
                this.ExpandCollapseAltro(true);
            
        }

        private void UsrQuota_QuotaImported(libBordero.Bordero.clsBordQuota quota)
        {
            this.AddQuota(quota);
        }

        private void UsrQuota_QuotaSelected(usrQuota usrQuotaSelected)
        {
            this.UsrQuotaSelected = null;
            if (usrQuotaSelected != null)
            {
                this.UsrQuotaSelected = usrQuotaSelected;
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                {
                    if (!usrQuota.Equals(usrQuotaSelected))
                        usrQuota.DeleselectQuota();
                }
                foreach (usrQuota usrQuota in this.pnlALTROItems.Controls)
                {
                    if (!usrQuota.Equals(usrQuotaSelected))
                        usrQuota.DeleselectQuota();
                }
            }
        }

        private void RenumQuote()
        {
            int index = 0;
            List<usrQuota> indexFatti = new List<usrQuota>();
            foreach (usrQuota usrQuotaMain in this.pnlDEMItems.Controls)
            {
                int indexMin = -1;
                usrQuota usrQuotaMin = null;
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                {
                    if (usrQuota.Index > 0 && (indexMin == -1 || usrQuota.Index < indexMin) && !indexFatti.Contains(usrQuota))
                    {
                        indexMin = usrQuota.Index;
                        usrQuotaMin = usrQuota;
                    }
                }
                if (usrQuotaMin == null)
                {
                    foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                    {
                        if (usrQuota.Index == 0 && !indexFatti.Contains(usrQuota))
                        {
                            indexMin = usrQuota.Index;
                            usrQuotaMin = usrQuota;
                            break;
                        }
                    }
                }
                if (usrQuotaMin != null)
                {
                    index += 1;
                    usrQuotaMin.SetIndex(index);
                    indexFatti.Add(usrQuotaMin);
                }
            }

            index = 0;
            indexFatti = new List<usrQuota>();
            foreach (usrQuota usrQuotaMain in this.pnlALTROItems.Controls)
            {
                int indexMin = -1;
                usrQuota usrQuotaMin = null;
                foreach (usrQuota usrQuota in this.pnlALTROItems.Controls)
                {
                    if (usrQuota.Index > 0 && (indexMin == -1 || usrQuota.Index < indexMin) && !indexFatti.Contains(usrQuota))
                    {
                        indexMin = usrQuota.Index;
                        usrQuotaMin = usrQuota;
                    }
                }
                if (usrQuotaMin == null)
                {
                    foreach (usrQuota usrQuota in this.pnlALTROItems.Controls)
                    {
                        if (usrQuota.Index == 0 && !indexFatti.Contains(usrQuota))
                        {
                            indexMin = usrQuota.Index;
                            usrQuotaMin = usrQuota;
                            break;
                        }
                    }
                }
                if (usrQuotaMin != null)
                {
                    index += 1;
                    usrQuotaMin.SetIndex(index);
                    indexFatti.Add(usrQuotaMin);
                }
            }

        }

        private void AssignValuesToConfigurazione(object sender, EventArgs e)
        {
            this.Configurazione.BorderoPerSpettacolo = this.chkUnBorderoPerSpettacolo.Checked;
            this.Configurazione.TitoloUnico = !this.chkTuttiTitoli.Checked;
            this.Configurazione.OraInizioEventi = (long)this.oraInizioEventi.Value;
            this.Configurazione.AsteriscoSupplementi = this.chkAsteriscoSupplementi.Checked;
        }

        private void btnSAVE_Click(object sender, EventArgs e)
        {
            if (this.SaveConfig())
                this.DialogResult = DialogResult.OK;
        }

        private void btnDEMplus_Click(object sender, EventArgs e)
        {
            libBordero.Bordero.clsBordQuota quota = new libBordero.Bordero.clsBordQuota();
            quota.IsQuotaDEM = true;
            quota.Descr = "";
            quota.SpettIntra = "X";
            quota.RigheDEM = new List<libBordero.Bordero.clsBordQuotaRigaDEM>();
            quota.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
            quota.SpettIntra = "";
            quota.TipiEvento = new List<libBordero.Bordero.clsBordQuotaTE>();
            this.AddQuota(quota);
        }

        private void AddQuota(libBordero.Bordero.clsBordQuota quota)
        {
            if (quota.IsQuotaDEM)
            {
                if (this.Configurazione.QuoteDEM == null)
                    this.Configurazione.QuoteDEM = new List<libBordero.Bordero.clsBordQuota>();
                this.Configurazione.QuoteDEM.Add(quota);
            }
            else
            {
                if (this.Configurazione.AltreQuote == null)
                    this.Configurazione.AltreQuote = new List<libBordero.Bordero.clsBordQuota>();
                this.Configurazione.AltreQuote.Add(quota);
            }
            usrQuota usrQuota = new usrQuota(quota, this.Configurazione, this.NomeConnessione, this.ServiceConfig);
            usrQuota.QuotaSelected += UsrQuota_QuotaSelected;
            usrQuota.QuotaImported += UsrQuota_QuotaImported;
            usrQuota.Dock = DockStyle.Top;
            usrQuota.AutoSize = true;
            usrQuota.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            if (quota.IsQuotaDEM)
            {
                usrQuota.TabIndex = this.pnlDEMItems.Controls.Count;
                this.pnlDEMItems.Controls.Add(usrQuota);
            }
            else
            {
                usrQuota.TabIndex = this.pnlALTROItems.Controls.Count;
                this.pnlALTROItems.Controls.Add(usrQuota);
            }
            usrQuota.BringToFront();
            usrQuota.ResizeQuota();
            this.RenumQuote();
            usrQuota.SetRigheQuotaVisible();
            usrQuota.SetFocusOnDescQuota();
        }

        private void btnDEMminus_Click(object sender, EventArgs e)
        {
            if (this.UsrQuotaSelected != null)
            {
                if (this.Configurazione.QuoteDEM.Contains(this.UsrQuotaSelected.Quota))
                    this.Configurazione.QuoteDEM.Remove(this.UsrQuotaSelected.Quota);
                if (this.pnlDEMItems.Controls.Contains(this.UsrQuotaSelected))
                    this.pnlDEMItems.Controls.Remove(this.UsrQuotaSelected);
                this.UsrQuotaSelected = null;
            }
            this.RenumQuote();
        }

        private void btnALTROplus_Click(object sender, EventArgs e)
        {
            libBordero.Bordero.clsBordQuota quota = new libBordero.Bordero.clsBordQuota();
            quota.IsQuotaDEM = false;
            quota.Descr = "";
            quota.SpettIntra = "X";
            quota.RigheDEM = new List<libBordero.Bordero.clsBordQuotaRigaDEM>();
            quota.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
            quota.SpettIntra = "";
            quota.TipiEvento = new List<libBordero.Bordero.clsBordQuotaTE>();
            this.AddQuota(quota);
        }

        private void btnALTROminus_Click(object sender, EventArgs e)
        {
            if (this.UsrQuotaSelected != null)
            {
                if (this.Configurazione.AltreQuote.Contains(this.UsrQuotaSelected.Quota))
                    this.Configurazione.AltreQuote.Remove(this.UsrQuotaSelected.Quota);
                if (this.pnlALTROItems.Controls.Contains(this.UsrQuotaSelected))
                    this.pnlALTROItems.Controls.Remove(this.UsrQuotaSelected);
                this.UsrQuotaSelected = null;
            }
            this.RenumQuote();
        }

        private void btnExpandCollapseDEM_Click(object sender, EventArgs e)
        {
            this.ExpandCollapseDEM(!this.pnlDEMItems.Visible);
        }
        
        private void ExpandCollapseDEM(bool expand)
        {
            this.pnlDEMItems.Visible = expand;
            this.btnDEMminus.Visible = this.pnlDEMItems.Visible;
            this.btnDEMplus.Visible = this.pnlDEMItems.Visible;
            if (this.pnlDEMItems.Visible)
            {
                this.pnlDEMItems.SuspendLayout();
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                {
                    usrQuota.SuspendLayout();
                }
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                {
                    usrQuota.SetRigheQuotaVisible();
                }
                foreach (usrQuota usrQuota in this.pnlDEMItems.Controls)
                {
                    usrQuota.ResumeLayout();
                }
                this.pnlDEMItems.ResumeLayout();
            }
            this.btnExpandCollapseDEM.Text = (this.pnlDEMItems.Visible ? "Riduci" : "Espandi");
            this.btnExpandCollapseDEM.SendToBack();
            this.btnExpandCollapseDEM.ImageIndex = (this.pnlDEMItems.Visible ? 1 : 0);
        }
        private void btnExpandCollapseALTRO_Click(object sender, EventArgs e)
        {
            this.ExpandCollapseAltro(!this.pnlALTROItems.Visible);
        }

        private void ExpandCollapseAltro(bool expand)
        {
            this.pnlALTROItems.Visible = expand;
            this.btnALTROminus.Visible = this.pnlALTROItems.Visible;
            this.btnALTROplus.Visible = this.pnlALTROItems.Visible;
            if (this.pnlALTROItems.Visible)
            {
                foreach (usrQuota usrQuota in this.pnlALTROItems.Controls)
                    usrQuota.SetRigheQuotaVisible();
            }
            this.btnExpandCollapseALTRO.Text = (this.pnlALTROItems.Visible ? "Riduci" : "Espandi");
            this.btnExpandCollapseALTRO.SendToBack();
            this.btnExpandCollapseALTRO.ImageIndex = (this.pnlALTROItems.Visible ? 1 : 0);
        }

        private void btnABORT_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            bool openFile = true;
            if (clsUtility.GetQuoteBase() != null)
            {
                Dictionary<object, string> menuItems = new Dictionary<object, string>() { { 1, "Da file" }, { 2, "Da un modello" } };
                object value = clsUtility.MenuGenerico("Opzioni di importazione", menuItems);
                if (value != null)
                {
                    if ((int)value == 2)
                    {
                        openFile = false;
                        libBordero.Bordero.clsBordQuota quota = clsUtility.GetQuoteBaseMenu();
                        if (quota != null)
                        {
                            if (!quota.IsQuotaDEM)
                            {
                                if (quota.Righe != null && quota.Righe.Count > 0 && quota.Righe[0].CodiceLocale == "*" && this.Configurazione.DescrCodiciLocali != null && this.Configurazione.DescrCodiciLocali.Count > 0)
                                {
                                    // da appendere per tutte le sale
                                    List<string> codiciToAppend = new List<string>();
                                    this.Configurazione.DescrCodiciLocali.ForEach(itemCodiceLocale => { if (!codiciToAppend.Contains(itemCodiceLocale.Key)) codiciToAppend.Add(itemCodiceLocale.Key); });
                                    codiciToAppend.Sort();
                                    libBordero.Bordero.clsBordQuota quotaToAppend = quota.Clone();
                                    quotaToAppend.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
                                    quota.Righe.ForEach(riga =>
                                    {
                                        libBordero.Bordero.clsBordQuotaRiga rigaToAppend = riga.Clone();
                                        rigaToAppend.CodiceLocale = "";
                                        codiciToAppend.ForEach(codice =>
                                        {
                                            if (rigaToAppend.CodiceLocale == "" && !quotaToAppend.Righe.Exists(rq => rq.CodiceLocale == codice))
                                                rigaToAppend.CodiceLocale = codice;
                                        });
                                        if (rigaToAppend.CodiceLocale != "")
                                            quotaToAppend.Righe.Add(rigaToAppend);
                                    });
                                    this.AddQuota(quotaToAppend);
                                }
                                else
                                    this.AddQuota(quota);
                            }
                            else
                                this.AddQuota(quota);
                        }
                    }
                }
                else
                {
                    openFile = false;
                }
            }


            if (openFile)
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                string file = "";
                fileDialog.Title = "Selezione il File di importazione";
                fileDialog.Filter = "ini files (*.ini)|*.ini|json files (*.json)|*.json|All files (*.*)|*.*";
                fileDialog.CheckFileExists = true;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    file = fileDialog.FileName;
                }
                fileDialog.Dispose();
                if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(file))
                {
                    if (this.ReadConfig(file))
                        this.InitDataControls();
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            string file = "";
            fileDialog.Title = "Selezione il File di esportazione";
            fileDialog.Filter = "json files (*.json)|*.json";
            fileDialog.CheckFileExists = false;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                file = fileDialog.FileName;
            }
            fileDialog.Dispose();
            if (!string.IsNullOrEmpty(file))
            {
                Exception oError = null;
                clsUtility.ExportToFile(file, this.Configurazione, out oError);
            }
        }
    }
}
