﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class frmWait : WinAppBordero.frmBorderoBase
    {
        public frmWait()
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
        }

        public string Message
        {
            get { return this.label1.Text; }
            set { this.label1.Text = value; this.Refresh(); Application.DoEvents(); }
        }
    }
}
