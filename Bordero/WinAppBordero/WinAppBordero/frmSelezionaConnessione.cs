﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class frmSelezionaConnessione : WinAppBordero.frmBorderoBase
    {

        
        public string NomeConnessione { get; set; }
        public bool Restart = false;
        public string DescConnessione { get; set; }
        private string singolaConnTNS = "";
        private string singolaConnDES = "";
        private int count = 0;
        private bool EditConnections { get; set; }
        private bool ConfigConnections { get; set; }
        private bool connectionsModified = false;
        private BorderoMenuConnessione borderoMenuConnessione { get; set; }
        private BorderoMenuConnessione parent { get; set; }
        Stack<BorderoMenuConnessione> stack = new Stack<BorderoMenuConnessione>();

        public frmSelezionaConnessione(bool editConnections = false, bool inConfiguration = false, BorderoMenuConnessione parBorderoMenuConnessione = null)
        {
            InitializeComponent();

            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.NomeConnessione = "";
            this.EditConnections = editConnections;
            this.ConfigConnections = inConfiguration;
            this.borderoMenuConnessione = parBorderoMenuConnessione;
            this.parent = this.borderoMenuConnessione;
            this.InitConnections();
            this.Shown += FrmSelezionaConnessione_Shown;
            this.HelpButtonClicked += Bordero_HelpButtonClicked;
            this.txtSearch.KeyDown += TxtSearch_KeyDown;
        }

        private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()) && (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return))
            {
                string search = this.txtSearch.Text.Trim().ToLower();
                List<BorderoMenuConnessione> lista = new List<BorderoMenuConnessione>();
                frmWait oFrmWait = new frmWait();
                oFrmWait.Message = "ricerca in corso...";
                oFrmWait.Show();
                Application.DoEvents();
                Search(search, this.parent, lista, oFrmWait);
                oFrmWait.Close();
                oFrmWait.Dispose();
                if (lista.Count > 0)
                {
                    lista.Sort((a, b) => b.Descrizione.CompareTo(a.Descrizione));
                    
                    Dictionary<object, string> options = new Dictionary<object, string>();
                    lista.ForEach(c => options.Add(c, c.Descrizione));
                    object result = frmGetGenericData.MenuGenerico("Ricerca", options);
                    if (result != null)
                    {
                        BorderoMenuConnessione b = (BorderoMenuConnessione)result;

                        if (b.Connessioni != null && b.Connessioni.Count > 0)
                        {
                            while (b != null && b.Connessioni != null && b.Connessioni.Count > 0)
                            {
                                options = new Dictionary<object, string>();
                                b.Connessioni.ForEach(c => options.Add(c, c.Descrizione));
                                b = null;
                                result = frmGetGenericData.MenuGenerico("Ricerca", options);
                                if (result != null)
                                {
                                    b = (BorderoMenuConnessione)result;
                                    if (b.Connessioni != null && b.Connessioni.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        this.DescConnessione = b.Descrizione;
                                        this.NomeConnessione = b.Codice;
                                        this.DialogResult = DialogResult.OK;
                                        break;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            this.DescConnessione = b.Descrizione;
                            this.NomeConnessione = b.Codice;
                            this.DialogResult = DialogResult.OK;
                        }
                        this.Close();
                    }
                }
                
            }
        }

        private void Search(string search, BorderoMenuConnessione parent, List<BorderoMenuConnessione> lista, frmWait oFrmWait)
        {
            if (parent.Descrizione.Trim().ToLower().Contains(search))
            {
                lista.Add(parent);
                oFrmWait.Message = parent.Descrizione;
                Application.DoEvents();
            }
            if (parent.Connessioni != null)
                parent.Connessioni.ForEach(c => Search(search, c, lista, oFrmWait));
        }

        private void Bordero_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            clsBorderoHelp.ShowHelp("Gestione Connessioni");
        }

        private void FrmSelezionaConnessione_Shown(object sender, EventArgs e)
        {
            if (!this.EditConnections && this.count <= 1 && this.borderoMenuConnessione == null)
            {
                this.DescConnessione = this.singolaConnDES;
                this.NomeConnessione = this.singolaConnTNS;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                if (this.txtSearch.Enabled && this.txtSearch.Visible)
                    this.txtSearch.Select();
            }
        }

        private void ResetConnections()
        {
            foreach (clsButtonBase btn in this.pnlConnections.Controls)
                btn.Click -= Btn_Click;
            this.pnlConnections.Controls.Clear();
        }

        private void InitConnections()
        {
            frmWait oFrmWait = new frmWait();
            oFrmWait.TopMost = true;
            oFrmWait.Message = "caricamento in corso...";
            oFrmWait.Show();
            Application.DoEvents();
            this.SuspendLayout();
            this.pnlConnections.SuspendLayout();

            this.ResetConnections();
            this.singolaConnTNS = "";
            this.singolaConnDES = "";
            this.count = 0;
            this.ClientSize = new Size(this.ClientSize.Width, this.Padding.Vertical + (this.EditConnections ? this.lblDefaultConnection.Height : 0) + this.pnlSearch.Height);
            this.CenterToScreen();

            if (this.parent == null && (CheckTnsNames() || this.EditConnections))
            {
                clsButtonBase btn = null;
                if (CheckTnsNames())
                {
                    foreach (string line in System.IO.File.ReadAllLines(FileNameTnsIni()))
                    {
                        if (line.StartsWith("[") && line.Contains("]:"))
                        {
                            this.count += 1;
                            string des = line.Replace("[", "").Replace("]", "").Split(':')[0];
                            string tns = line.Replace("[", "").Replace("]", "").Split(':')[1];
                            this.singolaConnTNS = tns;
                            this.singolaConnDES = des;
                            btn = new clsButtonBase();
                            btn.Tag = tns;
                            btn.Text = des;
                            btn.Dock = DockStyle.Top;
                            btn.Height = 50;
                            btn.Image = WinAppBordero.Properties.Resources.database;
                            btn.ImageAlign = ContentAlignment.MiddleLeft;
                            this.pnlConnections.Controls.Add(btn);
                            btn.BringToFront();
                            btn.Click += Btn_Click;
                        }
                    }
                }
                if (this.EditConnections)
                {
                    btn = new clsButtonBase();
                    btn.Tag = "_ADD_";
                    btn.Text = "Aggiungi";
                    btn.Dock = DockStyle.Top;
                    btn.Height = 50;
                    btn.Image = WinAppBordero.Properties.Resources.miniplus.ToBitmap();
                    btn.ImageAlign = ContentAlignment.MiddleLeft;
                    this.pnlConnections.Controls.Add(btn);
                    btn.BringToFront();
                    btn.Click += btnAddConnessione;

                }
                btn = new clsButtonBase();
                btn.Tag = "_CLOSE_";
                btn.Text = "Chiudi";
                btn.Dock = DockStyle.Top;
                btn.Image = WinAppBordero.Properties.Resources.close;
                btn.ImageAlign = ContentAlignment.MiddleLeft;
                btn.Height = 50;
                btn.DialogResult = DialogResult.Abort;
                this.CancelButton = btn;
                this.pnlConnections.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += Btn_Click;

                int nHeightButtons = 0;
                foreach (Control ctl in this.pnlConnections.Controls)
                    nHeightButtons += ctl.Height;
                nHeightButtons += this.pnlConnections.Padding.Vertical + 2;

                this.lblDefaultConnection.Visible = this.EditConnections && this.count == 0;
                this.ClientSize = new Size(this.ClientSize.Width, this.Padding.Vertical + (this.EditConnections ? this.lblDefaultConnection.Height : 0) + nHeightButtons + this.pnlSearch.Height);
            }
            else if (this.parent != null)
            {

                clsButtonBase btn = null;
                if (stack.Count > 0)
                {
                    btn = new clsButtonBase();
                    btn.Tag = stack.Peek();
                    btn.Text = stack.Peek().Descrizione;
                    btn.Dock = DockStyle.Top;
                    btn.Image = WinAppBordero.Properties.Resources._06_01_;
                    btn.ImageAlign = ContentAlignment.MiddleLeft;
                    btn.Height = 50;
                    this.pnlConnections.Controls.Add(btn);
                    btn.BringToFront();
                    btn.Click += (o, e) =>
                    {
                        this.DescConnessione = "";
                        this.NomeConnessione = "";
                        this.parent = stack.Pop();
                        clsUtility.RemoveControlsInside(this.pnlConnections);
                        this.InitConnections();
                        this.CenterToScreen();
                    };
                }

                this.count = 0;
                parent.Connessioni.ForEach(c =>
                {
                    this.count += 1;
                    oFrmWait.Message = string.Format("caricamento in corso...\r\n{0} di {1}", this.count, parent.Connessioni.Count);

                    btn = new clsButtonBase();
                    btn.Tag = c;
                    btn.Text = c.Descrizione + (c.Connessioni == null || c.Connessioni.Count == 0 ? "" : string.Format(" ({0})", c.Connessioni.Count));
                    btn.Dock = DockStyle.Top;
                    btn.Height = 50;
                    btn.Image = (c.Connessioni == null || c.Connessioni.Count == 0 ? WinAppBordero.Properties.Resources.database : WinAppBordero.Properties.Resources._26_29_);
                    btn.ImageAlign = ContentAlignment.MiddleLeft;
                    this.pnlConnections.Controls.Add(btn);
                    btn.BringToFront();
                    if (this.EditConnections)
                    {
                        btn.MouseClick += (o, e) =>
                        {
                            MouseEventArgs ev = (MouseEventArgs)e;
                            if (ev.Button == MouseButtons.Left)
                            {
                                if (c.Connessioni != null && c.Connessioni.Count > 0)
                                {
                                    this.stack.Push(this.parent);
                                    this.parent = c;
                                    this.InitConnections();
                                    this.CenterToScreen();
                                }
                                else
                                {
                                    clsButtonBase b = (clsButtonBase)o;
                                    this.DescConnessione = b.Text;
                                    this.NomeConnessione = ((BorderoMenuConnessione)b.Tag).Codice;
                                    this.DialogResult = DialogResult.OK;
                                    this.Close();
                                }
                            }
                            else
                            {
                                // todo: menu per edit o delete connessione da oggetto e poi risalvare in json e restart
                                this.Restart = true;
                                this.DialogResult = DialogResult.Abort;
                            }
                        };
                    }
                    else
                    {
                        btn.Click += (o, e) =>
                        {
                            if (c.Connessioni != null && c.Connessioni.Count > 0)
                            {
                                this.stack.Push(this.parent);
                                this.parent = c;
                                this.InitConnections();
                                this.CenterToScreen();
                            }
                            else
                            {
                                clsButtonBase b = (clsButtonBase)o;
                                this.DescConnessione = b.Text;
                                this.NomeConnessione = ((BorderoMenuConnessione)b.Tag).Codice;
                                this.DialogResult = DialogResult.OK;
                                this.Close();
                            }
                        };
                    }
                });

                if (this.EditConnections)
                {
                    btn = new clsButtonBase();
                    btn.Tag = "_ADD_";
                    btn.Text = "Aggiungi";
                    btn.Dock = DockStyle.Top;
                    btn.Height = 50;
                    btn.Image = WinAppBordero.Properties.Resources.miniplus.ToBitmap();
                    btn.ImageAlign = ContentAlignment.MiddleLeft;
                    this.pnlConnections.Controls.Add(btn);
                    btn.BringToFront();
                    btn.Click += (o, e) =>
                    {
                        string tns = "";
                        string descr = "";
                        if (GetValoriNuovaConnessione(ref tns, ref descr))
                        {
                            BorderoMenuConnessione b = new BorderoMenuConnessione() { Codice = tns, Descrizione = descr };
                            if (MessageBox.Show("Si tratta di un insieme ?", "Nuova connessione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                b.Connessioni = new List<BorderoMenuConnessione>();
                                tns = "";
                                descr = "";
                                MessageBox.Show("Impostare la prima connessione dell'insieme");
                                if (GetValoriNuovaConnessione(ref tns, ref descr))
                                {
                                    b.Connessioni.Add(new BorderoMenuConnessione() { Codice = tns, Descrizione = descr });
                                    parent.Connessioni.Add(b);
                                }
                            }
                            else
                                parent.Connessioni.Add(b);
                            this.Restart = true;
                            this.DialogResult = DialogResult.Abort;
                        }
                    };
                }

                btn = new clsButtonBase();
                btn.Tag = "_CLOSE_";
                btn.Text = "Chiudi";
                btn.Dock = DockStyle.Top;
                btn.Image = WinAppBordero.Properties.Resources.close;
                btn.ImageAlign = ContentAlignment.MiddleLeft;
                btn.Height = 50;
                btn.DialogResult = DialogResult.Abort;
                this.CancelButton = btn;
                this.pnlConnections.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (o, e) =>
                {
                    this.DescConnessione = "";
                    this.NomeConnessione = "";
                    this.DialogResult = DialogResult.Abort;
                    this.Close();
                };

                int nHeightButtons = 0;
                foreach (Control ctl in this.pnlConnections.Controls)
                    nHeightButtons += ctl.Height;
                nHeightButtons += this.pnlConnections.Padding.Vertical + 2;
                this.ClientSize = new Size(this.ClientSize.Width, this.Padding.Vertical + (this.EditConnections ? this.lblDefaultConnection.Height : 0) + nHeightButtons + this.pnlSearch.Height);
            }
            oFrmWait.Close();
            oFrmWait.Dispose();
            this.ResumeLayout();
            this.pnlConnections.ResumeLayout();
        }

        private void Btn_ClickBorderoConnessione(object sender, EventArgs e)
        {
            clsButtonBase btn = (clsButtonBase)sender;
            if (btn.DialogResult == DialogResult.Abort)
            {
                this.DescConnessione = "";
                this.NomeConnessione = "";
                this.DialogResult = DialogResult.Abort;
                this.Close();
                if (this.EditConnections && this.connectionsModified)
                    Restart = MessageBox.Show("Riavviare l'applicazione ?", "Riavvio", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            }
            else if (btn.DialogResult == DialogResult.Cancel)
            {

            }
            else
            {

            }
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            clsButtonBase btn = (clsButtonBase)sender;
            if (btn.DialogResult == DialogResult.Abort)
            {
                this.DescConnessione = "";
                this.NomeConnessione = "";
                this.DialogResult = DialogResult.Abort;
                this.Close();
                if (this.EditConnections && this.connectionsModified)
                    Restart = MessageBox.Show("Riavviare l'applicazione ?", "Riavvio", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            }
            else
            {
                if (this.EditConnections && this.ConfigConnections)
                {
                    string old_tnsValue = btn.Tag.ToString();
                    string old_desValue = btn.Text;

                    Dictionary<object, string> Opzioni = new Dictionary<object, string>();
                    Opzioni.Add("EDIT", "Modifica");
                    Opzioni.Add("DELETE", "Elimina");
                    object result = frmGetGenericData.MenuGenerico("Connessione", Opzioni);
                    if (result != null)
                    {
                        if (result.ToString() == "EDIT")
                        {
                            string tnsValue = old_tnsValue;
                            string desValue = old_desValue;
                            bool lContinue = true;
                            while (lContinue)
                            {
                                lContinue = false;
                                if (GetValoriNuovaConnessione(ref tnsValue, ref desValue))
                                {
                                    if (string.Format("[{0}]:{1}", old_desValue, old_tnsValue) != string.Format("[{0}]:{1}", desValue, tnsValue))
                                    {
                                        bool lSave = true;
                                        if (MessageBox.Show(string.Format("Verificare la connessione\r\n{0} ?", tnsValue), "Verifica connessione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                            lSave = VerificaConnessione(tnsValue);
                                        if (lSave)
                                        {
                                            if (!System.IO.File.Exists(frmSelezionaConnessione.FileNameTnsIni()))
                                            {
                                                System.IO.File.WriteAllText(frmSelezionaConnessione.FileNameTnsIni(), "");
                                            }
                                            List<string> lines = new List<string>();
                                            foreach (string line in System.IO.File.ReadAllLines(FileNameTnsIni()))
                                            {
                                                string oldLine = string.Format("[{0}]:{1}", old_desValue, old_tnsValue);
                                                if (oldLine == line)
                                                    lines.Add(string.Format("[{0}]:{1}", desValue, tnsValue));
                                                else
                                                    lines.Add(line);
                                            }
                                            System.IO.File.WriteAllLines(FileNameTnsIni(), lines.ToArray());
                                            this.connectionsModified = true;
                                            this.InitConnections();
                                            this.CenterToScreen();
                                        }
                                    }
                                    else
                                        MessageBox.Show("Nessuna modifica", "Modifica connessione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                        else if (result.ToString() == "DELETE")
                        {
                            if (MessageBox.Show(string.Format("Eliminare la connessione\r\n{0} ?", old_desValue), "Elimina connessione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                List<string> lines = new List<string>();
                                if (!System.IO.File.Exists(frmSelezionaConnessione.FileNameTnsIni()))
                                {
                                    System.IO.File.WriteAllText(frmSelezionaConnessione.FileNameTnsIni(), "");
                                }
                                foreach (string line in System.IO.File.ReadAllLines(FileNameTnsIni()))
                                {
                                    string oldLine = string.Format("[{0}]:{1}", old_desValue, old_tnsValue);
                                    if (oldLine != line)
                                        lines.Add(line);
                                }

                                if (lines.Count > 0)
                                    System.IO.File.WriteAllLines(FileNameTnsIni(), lines.ToArray());
                                else if (System.IO.File.Exists(frmSelezionaConnessione.FileNameTnsIni()))
                                {
                                    System.IO.File.Delete(frmSelezionaConnessione.FileNameTnsIni());
                                }
                                this.connectionsModified = true;
                                this.InitConnections();
                                this.CenterToScreen();
                            }
                        }
                    }
                }
                else
                {
                    this.DescConnessione = btn.Text;
                    this.NomeConnessione = btn.Tag.ToString();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }



        private void btnAddConnessione(object sender, EventArgs e)
        {
            string tnsValue = "";
            string desValue = "";
            if (GetValoriNuovaConnessione(ref tnsValue, ref desValue))
            {
                bool lSave = true;
                if (MessageBox.Show(string.Format("Verificare la connessione\r\n{0} ?", tnsValue), "Verifica connessione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    lSave = VerificaConnessione(tnsValue);
                if (lSave)
                {
                    List<string> lines = new List<string>();
                    if (!System.IO.File.Exists(frmSelezionaConnessione.FileNameTnsIni()))
                    {
                        System.IO.File.WriteAllText(frmSelezionaConnessione.FileNameTnsIni(), "");
                    }
                    foreach (string line in System.IO.File.ReadAllLines(FileNameTnsIni()))
                        lines.Add(line);
                    lines.Add(string.Format("[{0}]:{1}", desValue, tnsValue));
                    System.IO.File.WriteAllLines(FileNameTnsIni(), lines.ToArray());
                    this.connectionsModified = true;
                    this.InitConnections();
                    this.CenterToScreen();
                }
            }
        }

        private bool VerificaConnessione(string tnsValue)
        {
            frmWait oFrmWait = new frmWait();
            oFrmWait.Message = "Test connessione in corso...";
            oFrmWait.Show();
            Application.DoEvents();
            IConnection conn = null;
            Exception oError = null;
            bool result = false;
            try
            {
                conn = libBordero.Bordero.GetConnection(tnsValue, out oError);
                result = conn.State == ConnectionState.Open;
                if (!result)
                    oError = new Exception("Connessione non aperta");
            }
            catch (Exception exConnection)
            {
                oError = exConnection;
                result = false;
            }

            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

            oFrmWait.Close();
            oFrmWait.Dispose();
            Application.DoEvents();

            if (oError != null)
            {
                if (MessageBox.Show(string.Format("ERRORE:\r\n{0}\r\nVisualizzare i dettagli ?", oError.Message), "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    MessageBox.Show(oError.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = MessageBox.Show(string.Format("{0}\r\nProseguire comunque ?", oError.Message), "Errore Connessione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            }

            return result;
        }

        private bool GetValoriNuovaConnessione(ref string tnsValue, ref string desValue)
        {
            bool result = false;
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData("DES", "Descrizione", clsItemGenericData.EnumGenericDataType.TypeString, desValue, false, true));
            items.Add(new clsItemGenericData("TNS", "Nome connesione tnanames.ora", clsItemGenericData.EnumGenericDataType.TypeString, tnsValue, false, true));
            frmGetGenericData fGen = new frmGetGenericData("Connessione", items);
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == "DES")
                        desValue = item.Value.ToString();
                    if (item.Code == "TNS")
                        tnsValue = item.Value.ToString();
                }
                result = true;
            }
            fGen.Dispose();
            return result;
        }

        public static string FileNameTnsIni()
        {
            return string.Format(@"{0}\tnsnames.ini", clsUtility.ApplicationPath); 
        }
        private static bool CheckTnsNames()
        {
            bool result = false;
            if (System.IO.File.Exists(FileNameTnsIni()))
            {
                List<string> lines = new List<string>(System.IO.File.ReadAllLines(FileNameTnsIni()));
                lines.ForEach(l =>
                {
                    result = result || (l.StartsWith("[") && l.Contains("]:"));
                });
            }
            return result;
        }

        public static string GetNomeConnessione(out string descConnessione, out bool lContinue, out bool restart)
        {
            lContinue = false;
            restart = false;
            descConnessione = "";
            string result = "SERVERX";
            BorderoMenuConnessione bordMenuConnessioni = BorderoMenuConnessione.GetBorderoMenuConnessioneFromFile();
            if (CheckTnsNames() || bordMenuConnessioni != null)
            {
                result = "";
                frmSelezionaConnessione frmConnessione = new frmSelezionaConnessione(false, false, bordMenuConnessioni);
                if (frmConnessione.ShowDialog() == DialogResult.OK)
                {
                    lContinue = true;
                    restart = frmConnessione.Restart;
                    if (!string.IsNullOrEmpty(frmConnessione.NomeConnessione))
                        result = frmConnessione.NomeConnessione;
                    else
                        result = "SERVERX";
                    descConnessione = frmConnessione.DescConnessione;
                }
                frmConnessione.Close();
                frmConnessione.Dispose();
            }
            else
                lContinue = true;

            return result;
        }

        public static bool SetNomiConnessione(out bool restart)
        {
            restart = false;
            bool result = false;
            BorderoMenuConnessione bordMenuConnessioni = BorderoMenuConnessione.GetBorderoMenuConnessioneFromFile();
            //if (CheckTnsNames())
            //{

            //}
            frmSelezionaConnessione frmConnessione = new frmSelezionaConnessione(true, true, bordMenuConnessioni);
            if (frmConnessione.ShowDialog() == DialogResult.OK)
            {
                result = true;
            }
            else
                restart = frmConnessione.Restart;
            frmConnessione.Close();
            frmConnessione.Dispose();
            return result;
        }
    }
}
