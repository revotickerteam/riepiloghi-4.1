﻿
namespace WinAppBordero
{
    partial class frmSearchEvento
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEventi = new System.Windows.Forms.DataGridView();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnOK = new WinAppBordero.clsButtonBase();
            this.btnAbort = new WinAppBordero.clsButtonBase();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventi)).BeginInit();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvEventi
            // 
            this.dgvEventi.AllowUserToAddRows = false;
            this.dgvEventi.AllowUserToDeleteRows = false;
            this.dgvEventi.AllowUserToResizeColumns = false;
            this.dgvEventi.AllowUserToResizeRows = false;
            this.dgvEventi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEventi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEventi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEventi.Location = new System.Drawing.Point(0, 0);
            this.dgvEventi.Name = "dgvEventi";
            this.dgvEventi.ReadOnly = true;
            this.dgvEventi.RowHeadersVisible = false;
            this.dgvEventi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEventi.Size = new System.Drawing.Size(604, 349);
            this.dgvEventi.TabIndex = 0;
            // 
            // pnlButtons
            // 
            this.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Controls.Add(this.btnAbort);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 349);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(604, 65);
            this.pnlButtons.TabIndex = 2;
            // 
            // btnOK
            // 
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOK.Location = new System.Drawing.Point(461, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(138, 57);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Conferma";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAbort
            // 
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(3, 3);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(138, 57);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Abbandona";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // frmSearchEvento
            // 
            this.ClientSize = new System.Drawing.Size(604, 414);
            this.Controls.Add(this.dgvEventi);
            this.Controls.Add(this.pnlButtons);
            this.Name = "frmSearchEvento";
            this.ShowInTaskbar = false;
            this.Text = "Ricerca Evento";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventi)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEventi;
        private System.Windows.Forms.Panel pnlButtons;
        private clsButtonBase btnOK;
        private clsButtonBase btnAbort;
    }
}
