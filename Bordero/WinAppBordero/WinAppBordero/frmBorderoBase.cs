﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class frmBorderoBase : Form
    {
        public frmBorderoBase()
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
        }

        public static Font GetRightFont(Font font, bool show = false)
        {
            //Font result = null;
            //if (font.Size != font.SizeInPoints)
            //{
            //    float fontSizeInPixelsInDPI96 = font.Size;
            //    float newFontSizeInPoints = fontSizeInPixelsInDPI96 * 72f / 96; // calculate the size in points
            //    result = new System.Drawing.Font(font.FontFamily, newFontSizeInPoints, font.Style, System.Drawing.GraphicsUnit.Point);
            //}
            //else
            //    result = font;
            return font;
        }
    }
}
