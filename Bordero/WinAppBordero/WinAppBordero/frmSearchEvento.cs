﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class frmSearchEvento : WinAppBordero.frmBorderoBase
    {

        public frmSearchEvento(List<libBordero.Bordero.clsSearchSingoloEvento> eventi)
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            BindingSource bindingSource = new BindingSource();
            foreach (libBordero.Bordero.clsSearchSingoloEvento evento in eventi)
                bindingSource.Add(evento);
            this.dgvEventi.DataSource = bindingSource;
        }

        public libBordero.Bordero.clsSearchSingoloEvento EventoSelezionato
        {
            get
            {
                if (this.dgvEventi.SelectedRows.Count > 0)
                    return (libBordero.Bordero.clsSearchSingoloEvento)this.dgvEventi.SelectedRows[0].DataBoundItem;
                else
                    return null;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
        }
    }
}
