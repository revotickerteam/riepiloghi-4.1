﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class usrQuota : UserControl
    {

        public int Index = 0;
        public delegate void delegateQuotaSelected(usrQuota quota);
        public event delegateQuotaSelected QuotaSelected;

        public delegate void delegateQuotaImported(libBordero.Bordero.clsBordQuota quota);
        public event delegateQuotaImported QuotaImported;

        public class clsFlowLayoutPanelLista : FlowLayoutPanel
        {
            public delegate void delegateItemCheck(clsFlowLayoutPanelLista lista, clsItemLista item);
            public event delegateItemCheck ItemCheck;
            public clsFlowLayoutPanelLista()
                : base()
            {
                this.ControlAdded += ClsLista_ControlAdded;
                this.ControlRemoved += ClsLista_ControlRemoved;
            }

            private void ClsLista_ControlRemoved(object sender, ControlEventArgs e)
            {
                ((clsItemLista)e.Control).ItemCheck -= ClsLista_ItemCheck;
            }

            private void ClsLista_ControlAdded(object sender, ControlEventArgs e)
            {
                e.Control.Visible = true;
                ((clsItemLista)e.Control).ItemCheck += ClsLista_ItemCheck;
            }

            private void ClsLista_ItemCheck(clsItemLista item)
            {
                if (this.ItemCheck != null)
                    this.ItemCheck(this, item);
            }
        }
        public class clsItemLista : CheckBox
        {
            public libBordero.Bordero.clsDescriptorField Descriptor { get; set; }
            public delegate void delegateItemCheck(clsItemLista item);
            public event delegateItemCheck ItemCheck;
            public clsItemLista(libBordero.Bordero.clsDescriptorField descrField, bool check)
            {
                this.Descriptor = new libBordero.Bordero.clsDescriptorField();
                this.Descriptor.Key = descrField.Key;
                this.Descriptor.Value = descrField.Value;
                this.Text = this.Descriptor.Key + " " + this.Descriptor.Value;
                this.Checked = check;
                this.CheckedChanged += ClsItemLista_CheckedChanged;
                this.AutoSize = true;
            }

            private void ClsItemLista_CheckedChanged(object sender, EventArgs e)
            {
                if (this.ItemCheck != null)
                    this.ItemCheck(this);
            }

            public override string ToString()
            {
                return (this.Descriptor != null ? this.Descriptor.Key + " " + this.Descriptor.Value : "");
            }

            
        }

        public void SetFocusOnDescQuota()
        {
            this.txtDescr.Focus();
        }

        public libBordero.Bordero.clsBordQuota Quota { get; set; }
        public libBordero.Bordero.clsBordQuoteConfig Configurazione { get; set; }

        public string NomeConnessione { get; set; }
        private libBordero.clsBorderoServiceConfig ServiceConfig { get; set; }

        public usrQuota(libBordero.Bordero.clsBordQuota quota, libBordero.Bordero.clsBordQuoteConfig configurazione, string nomeConnessione, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            InitializeComponent();
            this.Quota = quota;
            this.Configurazione = configurazione;
            this.NomeConnessione = nomeConnessione;
            this.ServiceConfig = serviceConfig;
            this.InitData();
        }

        public void SetIndex(int index)
        {
            this.Index = index;
            this.lblIndex.Text = index.ToString();
            if (this.Quota != null)
                this.lblIndex.VerticalText = this.Quota.Descr;
        }

        private void InitData()
        {
            this.txtDescr.Text = this.Quota.Descr;
            if (this.Quota.SpettIntra == "")
                this.Quota.SpettIntra = "X";
            
            this.radioSI_ALL.Checked = this.Quota.SpettIntra == "X";
            this.radioSI_S.Checked = this.Quota.SpettIntra == "S";
            this.radioSI_I.Checked = this.Quota.SpettIntra == "I";
            this.chkLstTE.SuspendLayout();
            foreach (libBordero.Bordero.clsDescriptorField oDescrField in this.Configurazione.DescrTipiEvento.OrderBy(i => i.Key))
            {
                bool lExist = (this.Quota.TipiEvento != null && this.Quota.TipiEvento.Where(w => w.TE == oDescrField.Key).Any());
                if (lExist)
                    this.radioTE_SPECIFICI.Checked = true;
                this.chkLstTE.Controls.Add(new clsItemLista(oDescrField, lExist));
            }
            this.chkLstTE.ResumeLayout();
            this.usrRigheQuote.AssignData(this.Quota, this.Configurazione, this.NomeConnessione, this.ServiceConfig);
            this.SetSizeTipiEvento();

            this.txtDescr.TextChanged += TxtDescr_TextChanged;
            this.radioSI_ALL.CheckedChanged += RadioSI_ALL_CheckedChanged;
            this.radioSI_S.CheckedChanged += RadioSI_S_CheckedChanged;
            this.radioSI_I.CheckedChanged += RadioSI_I_CheckedChanged;
            this.radioTE_ALL.CheckedChanged += RadioTE_ALL_CheckedChanged;
            this.radioTE_SPECIFICI.CheckedChanged += RadioTE_SPECIFICI_CheckedChanged;
            this.chkLstTE.ItemCheck += ChkLstTE_ItemCheck;
            this.lblIndex.MouseClick += LblIndex_Click;
            //this.lblIndex.ImageIcon = WinAppBordero.Properties.Resources.arrowDn;

        }

        private void RadioSI_I_CheckedChanged(object sender, EventArgs e)
        {
            this.Quota.SpettIntra = "I";
        }

        private void RadioSI_S_CheckedChanged(object sender, EventArgs e)
        {
            this.Quota.SpettIntra = "S";
        }

        private void RadioSI_ALL_CheckedChanged(object sender, EventArgs e)
        {
            this.Quota.SpettIntra = "X";
        }

        public void SetRigheQuotaVisible()
        {
            this.usrRigheQuote.Visible = true;
            //this.lblIndex.ImageIcon = (this.usrRigheQuote.Visible ? WinAppBordero.Properties.Resources.arrowUp : WinAppBordero.Properties.Resources.arrowDn);
            this.ResizeQuota();
        }

        private void LblIndex_Click(object sender, MouseEventArgs e)
        {
            if (this.QuotaSelected != null)
                this.QuotaSelected(this);
            this.lblIndex.BackColor = Color.Gold;

            //this.usrRigheQuote.Visible = !this.usrRigheQuote.Visible;
            //this.lblIndex.ImageIcon = (this.usrRigheQuote.Visible ? WinAppBordero.Properties.Resources.arrowUp : WinAppBordero.Properties.Resources.arrowDn);
            //TODO?this.ResizeQuota();

            //Rectangle rectangle = this.lblIndex.GetRectIcon();
            //if (rectangle.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
            //{

            //}
            //else
            //{

            //}
        }

        public void DeleselectQuota()
        {
            this.lblIndex.BackColor = Color.Transparent;
        }

        private void ChkLstTE_ItemCheck(clsFlowLayoutPanelLista lista, clsItemLista item)
        {
            
            if (item.CheckState == CheckState.Checked && !this.Quota.TipiEvento.Where(w => w.TE == item.Descriptor.Key).Any())
            {
                libBordero.Bordero.clsBordQuotaTE itemTe = new libBordero.Bordero.clsBordQuotaTE();
                itemTe.TE = item.Descriptor.Key;
                itemTe.Descr = item.Descriptor.Value;
                this.Quota.TipiEvento.Add(itemTe);
            }
            else if (item.CheckState == CheckState.Unchecked && this.Quota.TipiEvento.Where(w => w.TE == item.Descriptor.Key).Any())
            {
                this.Quota.TipiEvento.Remove(this.Quota.TipiEvento.Where(w => w.TE == item.Descriptor.Key).FirstOrDefault());
            }
        }

        

        private void RadioTE_SPECIFICI_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioTE_SPECIFICI.Enabled)
            {
                this.radioTE_ALL.Enabled = false;
                this.radioTE_SPECIFICI.Enabled = false;
                this.SetSizeTipiEvento();
                this.radioTE_ALL.Enabled = true;
                this.radioTE_SPECIFICI.Enabled = true;
            }
        }

        private void RadioTE_ALL_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioTE_ALL.Enabled)
            {
                this.radioTE_ALL.Enabled = false;
                this.radioTE_SPECIFICI.Enabled = false;
                this.SetSizeTipiEvento();
                this.radioTE_ALL.Enabled = true;
                this.radioTE_SPECIFICI.Enabled = true;
            }
        }

        private void TxtDescr_TextChanged(object sender, EventArgs e)
        {
            this.Quota.Descr = this.txtDescr.Text;
            this.lblIndex.VerticalText = this.Quota.Descr;
            this.usrRigheQuote.AssignColHeaders();
            this.usrRigheQuote.ResizeGrid();
        }

        public void ResizeQuota()
        {
            //this.usrRigheQuote.AssignColHeaders();
            this.usrRigheQuote.ResizeGrid();
            this.ClientSize = new Size(this.ClientSize.Width, this.pnlDescr.Height + this.pnlSI.Height + this.pnlTE.Height + (this.usrRigheQuote.Visible ? this.usrRigheQuote.Height : 0) + 1);
        }

        private void btnExpandCollapse_Click(object sender, EventArgs e)
        {
            int smallSize = this.pnlTopTE.Height + this.pnlTopTE.Padding.Vertical;
            if (this.radioTE_SPECIFICI.Checked)
            {
                smallSize += this.chkLstTE.MinimumSize.Height;
            }
            int fullSize = 300;
            if (this.pnlTE.ClientSize.Height == smallSize)
                this.pnlTE.ClientSize = new Size(this.pnlTE.ClientSize.Width, fullSize);
            else
                this.pnlTE.ClientSize = new Size(this.pnlTE.ClientSize.Width, smallSize);
            this.btnExpandCollapse.ImageIndex = (this.pnlTE.ClientSize.Height == smallSize ? 0 : 1);
        }

        private void SetSizeTipiEvento()
        {
            this.chkLstTE.Visible = this.radioTE_SPECIFICI.Checked;
            this.btnExpandCollapse.Visible = this.radioTE_SPECIFICI.Checked;
            int height = this.pnlTopTE.Height + this.pnlTopTE.Padding.Vertical;
            if (this.radioTE_SPECIFICI.Checked)
            {
                height += this.chkLstTE.Height;
            }
            this.pnlTE.ClientSize = new Size(this.pnlTE.ClientSize.Width, height);
            this.chkLstTE.PerformLayout();
            this.ResizeQuota();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            string file = "";
            fileDialog.Title = "Selezione il File di importazione";
            fileDialog.Filter = "ini files (*.ini)|*.ini|json files (*.json)|*.json|All files (*.*)|*.*";
            fileDialog.CheckFileExists = true;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                file = fileDialog.FileName;
            }
            fileDialog.Dispose();
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(file))
            {
                Exception oError;
                libBordero.Bordero.clsBordQuoteConfig configurazione = clsUtility.ImportFromFile(file, out oError, this.Configurazione.DescrCodiciLocali);

                if (this.Quota.IsQuotaDEM && configurazione.QuoteDEM != null)
                {
                    Dictionary<object, string> items = new Dictionary<object, string>();
                    foreach (libBordero.Bordero.clsBordQuota quota in configurazione.QuoteDEM)
                    {
                        items.Add(quota, quota.Descr);
                    }
                    object value = clsUtility.MenuGenerico("Seleziona una quota", items);
                    if (value != null && this.QuotaImported != null)
                        this.QuotaImported((libBordero.Bordero.clsBordQuota)value);
                }
                else if (!this.Quota.IsQuotaDEM && configurazione.AltreQuote != null)
                {
                    Dictionary<object, string> items = new Dictionary<object, string>();
                    foreach (libBordero.Bordero.clsBordQuota quota in configurazione.AltreQuote)
                    {
                        items.Add(quota, quota.Descr);
                    }
                    object value = clsUtility.MenuGenerico("Seleziona una quota", items);
                    if (value != null && this.QuotaImported != null)
                        this.QuotaImported((libBordero.Bordero.clsBordQuota)value);
                }

            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            string file = "";
            fileDialog.Title = "Selezione il File di esportazione";
            fileDialog.Filter = "json files (*.json)|*.json";
            fileDialog.CheckFileExists = false;
            fileDialog.FileName = this.Quota.Descr + ".json";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                file = fileDialog.FileName;
            }
            fileDialog.Dispose();
            if (!string.IsNullOrEmpty(file))
            {
                Exception oError = null;
                clsUtility.ExportToFile(file, this.Quota, out oError);
            }
        }
    }
}
