﻿using libBordero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAppBordero
{
    //[System.Diagnostics.DebuggerStepThrough]
    public class clsButtonBase : Button
    {

        private CheckState checkSt = CheckState.Indeterminate;
        public Color InteriorBorderColor = Color.Transparent;
        
        public clsButtonBase()
        {
            this.DoubleBuffered = true;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            base.OnPaint(e);
            if (this.checkSt == CheckState.Checked || this.checkSt == CheckState.Unchecked)
            {
                Point p = new Point(3, (this.ClientSize.Height - System.Windows.Forms.CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal).Height) / 2);
                System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(e.Graphics, p, (this.checkSt == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal));
            }
            if (InteriorBorderColor != Color.Transparent)
            {
                Pen pen = new Pen(InteriorBorderColor);
                e.Graphics.DrawRectangle(pen, new Rectangle(0, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1));
            }
        }

        public CheckState CheckState
        {
            get { return this.checkSt; }
            set { this.checkSt = value; this.Invalidate(); }
        }

    }

    public class clsCheckBox : CheckBox
    {
        protected override void OnPaint(PaintEventArgs pevent)
        {
            pevent.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            base.OnPaint(pevent);
            //pevent.Graphics.DrawRectangle(Pens.Black, new Rectangle(0, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1));
        }
    }

    public delegate void delegateOnShownEvent(Form form);

    public static class clsUtility
    {
        public static string ApplicationPath
        {
            get
            {
                System.IO.FileInfo ofileInfoApp = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string result = ofileInfoApp.Directory.FullName;

                return result;
            }
        }

        public static bool CheckTryParseDecimal(string text, out decimal value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else if (text.Contains(".") && text.Contains(","))
            {
                result = decimal.TryParse(text, out value);
            }
            else if (text.Contains(".") && !text.Contains(","))
            {
                result = decimal.TryParse(text.Replace(".",","), out value);
            }
            else
                result = decimal.TryParse(text, out value);
            return result;
        }

        public static bool CheckTryParseInt(string text, out int value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else
                result = int.TryParse(text.Replace(".", "").Replace(".", ""), out value);
            return result;
        }

        public static StringFormat GetStringFormatFromContentAlign(ContentAlignment align)
        {
            StringFormat result = new StringFormat();
            result.Alignment = StringAlignment.Center;
            result.LineAlignment = StringAlignment.Center;
            switch (align)
            {
                case ContentAlignment.TopLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.MiddleLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.BottomLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.TopRight: { result.Alignment = StringAlignment.Far; break; }
                case ContentAlignment.MiddleRight: { result.Alignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomRight: { result.Alignment = StringAlignment.Far; break; }
            }
            switch (align)
            {
                case ContentAlignment.TopLeft: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.TopRight: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.TopCenter: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.BottomLeft: { result.LineAlignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomRight: { result.LineAlignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomCenter: { result.LineAlignment = StringAlignment.Far; break; }
            }
            return result;
        }

        public static object MenuGenerico(string title, Dictionary<object, string> options, Point? startupPosition = null, Icon iconMenu = null, bool checkBox = false, Dictionary<object, bool> itemschecked = null, delegateOnShownEvent delegateOnShow = null)
        {
            object result = null;
            frmWait oFrmWait = null;
            oFrmWait = new frmWait();
            oFrmWait.TopMost = true;
            oFrmWait.Message = "Inizializzazione...";

            Stopwatch stopW = new Stopwatch();
            stopW.Start();

            clsButtonBase btnFirstFocus = null;
            
            try
            {
                Form frmMenu = new Form();
                frmMenu.Size = new Size(800, 600);
                frmMenu.TopMost = true;
                frmMenu.Font = new Font("Calibri", 12, FontStyle.Regular);
                if (startupPosition != null)
                {
                    frmMenu.StartPosition = FormStartPosition.Manual;
                    frmMenu.Location = (Point)startupPosition;
                }
                else
                    frmMenu.StartPosition = FormStartPosition.CenterScreen;
                if (iconMenu != null)
                    frmMenu.Icon = iconMenu;
                frmMenu.FormBorderStyle = FormBorderStyle.FixedDialog;
                frmMenu.ControlBox = true;
                frmMenu.MinimizeBox = false;
                frmMenu.MaximizeBox = false;
                frmMenu.AutoScroll = true;
                frmMenu.Size = new Size(300, 800);
                frmMenu.Text = title;
                clsButtonBase btn;
                bool findAbort = false;
                int height = frmMenu.Padding.Vertical;

                Graphics g;
                if (checkBox)
                {
                    clsLabel lbl = new clsLabel();
                    lbl.CheckState = CheckState.Unchecked;
                    lbl.AutoSize = false;

                    lbl.Text = "";
                    lbl.Dock = DockStyle.Top;
                    lbl.Size = new Size(100, 20);
                    height += lbl.Height;
                    lbl.Tag = "_aLL_";
                    frmMenu.Controls.Add(lbl);
                    lbl.BringToFront();

                    lbl.Click += (sender, e) =>
                    {
                        clsLabel lblSender = (clsLabel)sender;
                        lblSender.CheckState = (lblSender.CheckState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
                        foreach (Control c in frmMenu.Controls)
                        {
                            try
                            {
                                clsButtonBase btnOther = (clsButtonBase)c;
                                if (btnOther.Tag != null)
                                    btnOther.CheckState = lblSender.CheckState;
                            }
                            catch (Exception)
                            {
                            }
                        }

                    };
                }
                clsButtonBase btnPreSelected = null;
                foreach (KeyValuePair<object, string> item in options)
                {
                    if (stopW.ElapsedMilliseconds > 1000 && stopW.IsRunning)
                    {
                        stopW.Stop();
                        oFrmWait.Show();
                        Application.DoEvents();
                    }
                    if (item.Key == System.DBNull.Value)
                    {
                        clsLabel label = new clsLabel();
                        label.AutoSize = findAbort;
                        label.AutoEllipsis = true;
                        label.Text = item.Value;
                        label.Dock = DockStyle.Top;
                        label.Size = new Size(100, 50);
                        height += label.Height;
                        frmMenu.Controls.Add(label);
                        label.BringToFront();
                    }
                    else
                    {
                        btn = new clsButtonBase();
                        if (btnFirstFocus == null)
                            btnFirstFocus = btn;
                        btn.Text = item.Value;
                        btn.Dock = DockStyle.Top;
                        btn.Size = new Size(100, 50);
                        findAbort = item.Key == null;
                        btn.Tag = item.Key;
                        g = btn.CreateGraphics();
                        Size sizeText = g.MeasureString(btn.Text, frmMenu.Font, frmMenu.ClientSize.Width - frmMenu.Padding.Horizontal).ToSize();
                        if (sizeText.Height > btn.Height)
                        {
                            btn.Size = new Size(100, sizeText.Height + 20);
                        }
                        if (checkBox)
                        {
                            btn.CheckState = ((itemschecked != null && itemschecked.ContainsKey(item.Key) && itemschecked[item.Key]) ? CheckState.Checked : CheckState.Unchecked);
                        }
                        else
                        {
                            if (itemschecked != null && itemschecked.ContainsKey(item.Key))
                            {
                                // preselezione
                                btnPreSelected = btn;
                            }
                        }
                        height += btn.Height;
                        frmMenu.Controls.Add(btn);
                        btn.BringToFront();

                        if (btn.Tag != System.DBNull.Value)
                        {
                            btn.Click += (sender, e) =>
                            {
                                clsButtonBase btnSender = (clsButtonBase)sender;
                                if (!checkBox)
                                {
                                    ((Form)btnSender.Parent).Tag = btnSender.Tag;
                                    if (((Form)btnSender.Parent).Tag != null)
                                        ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                                }
                                else
                                {
                                    btnSender.CheckState = (btnSender.CheckState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
                                }
                            };
                        }
                        else
                        {
                            btn.InteriorBorderColor = Color.DarkOrange;
                        }
                    }
                }

                if (checkBox)
                {
                    btn = new clsButtonBase();
                    btn.Text = "Conferma";
                    btn.Dock = DockStyle.Top;
                    btn.Size = new Size(100, 50);
                    height += btn.Height;
                    frmMenu.Controls.Add(btn);
                    btn.BringToFront();
                    btnPreSelected = btn;
                    btn.Click += (sender, e) =>
                    {
                        clsButtonBase btnSender = (clsButtonBase)sender;
                        Dictionary<object, string> selected = new Dictionary<object, string>();
                        foreach (Control c in frmMenu.Controls)
                        {
                            try
                            {
                                clsButtonBase btnCheckd = (clsButtonBase)c;
                                if (btnCheckd.CheckState == CheckState.Checked)
                                {
                                    string item = options.Where(w => w.Key == btnCheckd.Tag.ToString()).FirstOrDefault().Value;
                                    if (item != null)
                                        selected.Add(btnCheckd.Tag, item);
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        ((Form)btnSender.Parent).Tag = selected;
                        ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                    };
                }

                if (!findAbort)
                {
                    btn = new clsButtonBase();
                    btn.Text = "Abbandona";
                    btn.Dock = DockStyle.Top;
                    btn.Size = new Size(100, 50);
                    height += btn.Height;
                    frmMenu.Controls.Add(btn);
                    btn.BringToFront();
                    btn.Click += (sender, e) =>
                    {
                        clsButtonBase btnSender = (clsButtonBase)sender;
                        ((Form)btnSender.Parent).Tag = null;
                        ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                    };
                }

                if (frmMenu.ClientSize.Height > height)
                {
                    frmMenu.ClientSize = new Size(frmMenu.ClientSize.Width, height);
                }

                if (btnPreSelected != null || delegateOnShow != null)
                {
                    frmMenu.Shown += (sender, e) =>
                    {
                        if (delegateOnShow != null) delegateOnShow(frmMenu);
                        if (btnPreSelected != null)
                        {
                            btnPreSelected.Font = new Font(btnPreSelected.Font.FontFamily, btnPreSelected.Font.Size, FontStyle.Bold);
                            frmMenu.ScrollControlIntoView(btnPreSelected);
                            btnPreSelected.Focus();
                        }
                        else if (btnFirstFocus != null)
                        {
                            btnFirstFocus.Focus();
                        }
                    };
                }

                if (stopW.IsRunning)
                    stopW.Stop();

                if (oFrmWait != null)
                {
                    oFrmWait.Close();
                    oFrmWait.Dispose();
                    oFrmWait = null;
                }

                if (frmMenu.ShowDialog() == DialogResult.OK)
                {
                    result = frmMenu.Tag;
                }
                frmMenu.Close();
                frmMenu.Dispose();

            }
            catch (Exception)
            {

            }

            if (stopW.IsRunning)
                stopW.Stop();

            if (oFrmWait != null)
            {
                oFrmWait.Close();
                oFrmWait.Dispose();
                oFrmWait = null;
            }

            return result;
        }

        

        public static libBordero.Bordero.clsBordQuoteConfig ImportFromFile(string file, out Exception oError, List<Bordero.clsDescriptorField> descrCodiciLocali)
        {
            oError = null;
            libBordero.Bordero.clsBordQuoteConfig result = null;
            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(file);
                if (fileInfo.Exists && fileInfo.Extension.Trim().ToUpper() == ".INI")
                    result = ImportFromFileINI(file, out oError);
                else if (fileInfo.Exists && fileInfo.Extension.Trim().ToUpper() == ".JSON")
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<libBordero.Bordero.clsBordQuoteConfig>(System.IO.File.ReadAllText(file));
                if (result != null)
                {
                    if (result.QuoteDEM != null)
                    {
                        result.QuoteDEM.ForEach(q =>
                        {
                            q.IdQuota = 0;
                            if (q.RigheDEM != null)
                            {
                                long indexR = 0;
                                q.RigheDEM.ForEach(r =>
                                {
                                    indexR += 1;
                                    r.Riga = indexR;
                                    r.IdSala = 0;
                                });
                            }
                        });
                    }

                    if (result.AltreQuote != null)
                    {
                        result.AltreQuote.ForEach(q =>
                        {
                            q.IdQuota = 0;
                            if (q.Righe != null)
                            {
                                long indexR = 0;
                                q.Righe.ForEach(r =>
                                {
                                    indexR += 1;
                                    r.Riga = indexR;
                                });
                            }
                        });
                    }

                    // sostituzione codice locale e/o idsala
                    if (descrCodiciLocali != null && descrCodiciLocali.Count > 0)
                    {
                        Dictionary<string, string> dictCodiceLocale = new Dictionary<string, string>();

                        if (result.QuoteDEM != null)
                        {
                            result.QuoteDEM.Where(q => q.RigheDEM != null
                                                       && q.RigheDEM.Count > 0 
                                                       && q.RigheDEM.FirstOrDefault(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                                                         && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                                                         && !dictCodiceLocale.ContainsKey(r.CodiceLocale)) != null).ToList()
                            .ForEach(q =>
                            {
                                q.RigheDEM.Where(r => !string.IsNullOrEmpty(r.CodiceLocale) 
                                                       && !dictCodiceLocale.ContainsKey(r.CodiceLocale)).ToList().ForEach(r =>
                                {
                                    if (!dictCodiceLocale.ContainsKey(r.CodiceLocale))
                                        dictCodiceLocale.Add(r.CodiceLocale, "");
                                });
                            });
                        }

                        if (result.AltreQuote != null)
                        {
                            result.AltreQuote.Where(q => q.Righe != null 
                                                         && q.Righe.Count > 0 
                                                         && q.Righe.FirstOrDefault(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                                                        && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                                                        && !dictCodiceLocale.ContainsKey(r.CodiceLocale)) != null).ToList()
                            .ForEach(q =>
                            {
                                q.Righe.Where(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                   && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                   && !dictCodiceLocale.ContainsKey(r.CodiceLocale)).ToList().ForEach(r =>
                                {
                                    if (!dictCodiceLocale.ContainsKey(r.CodiceLocale))
                                        dictCodiceLocale.Add(r.CodiceLocale, "");
                                });
                            });
                        }

                        bool continueReplaceCodiceLocale = true;
                        Dictionary<string, string> dictToDo = new Dictionary<string, string>();
                        dictCodiceLocale.Where(x => string.IsNullOrEmpty(x.Value)).ToList().ForEach(x => dictToDo.Add(x.Key, x.Value));

                        dictToDo.Where(x => string.IsNullOrEmpty(x.Value)).ToList().ForEach(x =>
                        {
                            if (continueReplaceCodiceLocale)
                            {
                                DialogResult diagResult = MessageBox.Show($"Sostituire il codice locale [{x.Key}] ?", "Sostituzione codice locale", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                                continueReplaceCodiceLocale = diagResult != DialogResult.Cancel;
                                if (diagResult == DialogResult.Yes)
                                {

                                    object value = null;
                                    Dictionary<object, string> items = new Dictionary<object, string>();
                                    foreach (libBordero.Bordero.clsDescriptorField item in descrCodiciLocali)
                                    {
                                        if (!items.ContainsKey(item.Key))
                                            items.Add(item.Key, item.Value);
                                    }
                                    value = clsUtility.MenuGenerico($"Sostituzione codice locale [{x.Key}] ?", items, null, null, false);

                                    if (value != null)
                                    {
                                        foreach (libBordero.Bordero.clsDescriptorField item in descrCodiciLocali)
                                        {
                                            if (item.Key == value.ToString())
                                            {
                                                dictCodiceLocale[x.Key] = value.ToString();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        });

                        dictCodiceLocale.Where(x => !string.IsNullOrEmpty(x.Value)
                                                    && descrCodiciLocali.FirstOrDefault(z => z.Key == x.Value) != null).ToList().ForEach(x =>
                        {
                            result.QuoteDEM.Where(q => q.RigheDEM != null
                                                       && q.RigheDEM.Count > 0
                                                       && q.RigheDEM.FirstOrDefault(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                                                         && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                                                         && x.Key == r.CodiceLocale) != null).ToList()
                            .ForEach(q =>
                            {
                                q.RigheDEM.Where(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                      && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                      && x.Key == r.CodiceLocale).ToList().ForEach(r =>
                                                       {
                                                           r.CodiceLocale = x.Value;
                                                       });
                            });

                            result.AltreQuote.Where(q => q.Righe != null
                                                         && q.Righe.Count > 0
                                                         && q.Righe.FirstOrDefault(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                                                        && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                                                        && x.Key == r.CodiceLocale) != null).ToList()
                            .ForEach(q =>
                            {
                                q.Righe.Where(r => !string.IsNullOrEmpty(r.CodiceLocale)
                                                   && descrCodiciLocali.FirstOrDefault(z => z.Key == r.CodiceLocale) == null
                                                   && x.Key == r.CodiceLocale).ToList().ForEach(r =>
                                                   {
                                                        r.CodiceLocale = x.Value;
                                                   });
                            });
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
                oError = ex;
            }
            return result;
        }

        public static bool ExportToFile(string file, libBordero.Bordero.clsBordQuoteConfig config, out Exception oError)
        {
            bool result = false;
            oError = null;
            try
            {
                string value = Newtonsoft.Json.JsonConvert.SerializeObject(config, Newtonsoft.Json.Formatting.Indented);
                System.IO.File.WriteAllText(file, value);
                result = true;
            }
            catch (Exception ex)
            {
                oError = ex;
                result = true;
            }
            return result;
        }

        public static bool ExportToFile(string file, libBordero.Bordero.clsBordQuota config, out Exception oError)
        {
            bool result = false;
            oError = null;
            try
            {
                string value = Newtonsoft.Json.JsonConvert.SerializeObject(config, Newtonsoft.Json.Formatting.Indented);
                System.IO.File.WriteAllText(file, value);
                result = true;
            }
            catch (Exception ex)
            {
                oError = ex;
                result = true;
            }
            return result;
        }


        public static libBordero.Bordero.clsBordQuoteConfig GetQuoteBase()
        {
            libBordero.Bordero.clsBordQuoteConfig result = null;
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                string jSonFileBasicQuote = clsUtility.ApplicationPath + @"\UtilityBorderoQuoteBase.json";
                if (System.IO.File.Exists(jSonFileBasicQuote))
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<libBordero.Bordero.clsBordQuoteConfig>(System.IO.File.ReadAllText(jSonFileBasicQuote));
            }
            catch (Exception)
            {
                result = null;
            }
            return result;
        }
        public static libBordero.Bordero.clsBordQuota GetQuoteBaseMenu()
        {
            libBordero.Bordero.clsBordQuota result = null;
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                string jSonFileBasicQuote = clsUtility.ApplicationPath + @"\UtilityBorderoQuoteBase.json";
                if (System.IO.File.Exists(jSonFileBasicQuote))
                {
                    libBordero.Bordero.clsBordQuoteConfig quote = GetQuoteBase();
                    if (quote != null)
                    {
                        Dictionary<object, string> items = new Dictionary<object, string>();
                        object value = null;
                        if (quote.QuoteDEM != null && quote.QuoteDEM.Count > 0)
                        {
                            quote.QuoteDEM.OrderBy(q => q.IdQuota).ToList().ForEach(q =>
                            {
                                items.Add(q, q.Descr);
                            });
                        }
                        if (quote.AltreQuote != null && quote.AltreQuote.Count > 0)
                        {
                            quote.AltreQuote.OrderBy(q => q.IdQuota).ToList().ForEach(q =>
                            {
                                items.Add(q, q.Descr);
                            });
                        }
                        
                        value = clsUtility.MenuGenerico("Selezionare una quota", items);
                        if (value != null)
                            result = (libBordero.Bordero.clsBordQuota)value;
                    }
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static libBordero.Bordero.clsBordQuoteConfig ImportFromFileINI(string file, out Exception oError)
        {
            oError = null;
            libBordero.Bordero.clsBordQuoteConfig result = null;
            try
            {
                result = new libBordero.Bordero.clsBordQuoteConfig();
                libBordero.Bordero.clsBordQuota quotaDEM = null;

                string[] lines = System.IO.File.ReadAllLines(file);
                string value = "";
                decimal decValue = 0;

                #region GENERALI

                foreach (string line in lines)
                {
                    if (GetValueLineaIni(line, "[TITOLO_UNICO]", out value))
                        result.TitoloUnico = value == "ABILITATO";

                    if (GetValueLineaIni(line, "[BORDERO_PER_SPETTACOLO]", out value))
                        result.BorderoPerSpettacolo = value == "ABILITATO";

                    if (GetValueLineaIni(line, "[ORA_INIZIO_SPETTACOLI]", out value))
                    {
                        Regex rOrario = new Regex(@"\d{2}.\d{2}");
                        if (rOrario.Match(value).Success)
                        {
                            result.OraInizioEventi = long.Parse(value.Substring(0, 2));
                        }
                    }
                }

                #endregion

                #region DEM

                // DEM -----------------------
                foreach (string line in lines)
                {
                    if (line.StartsWith("[PERCENTUALE_DEM]"))
                    {
                        quotaDEM = new libBordero.Bordero.clsBordQuota();
                        quotaDEM.IdQuota = 0;
                        quotaDEM.Descr = "DEM";
                        quotaDEM.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
                        quotaDEM.SpettIntra = "S";
                        quotaDEM.TipiEvento = new List<libBordero.Bordero.clsBordQuotaTE>();
                        quotaDEM.RigheDEM = new List<libBordero.Bordero.clsBordQuotaRigaDEM>();
                        quotaDEM.IsQuotaDEM = true;
                        libBordero.Bordero.clsBordQuotaRigaDEM riga = new libBordero.Bordero.clsBordQuotaRigaDEM();
                        riga.Riga = 1;
                        riga.PercentualeDEM = 2;
                        riga.PercentualeIncasso = 100;
                        riga.PercentualeOmaggi = 20;
                        riga.PercentualeSupplemento = 15;
                        riga.FlgSupplDemEcc = 1;
                        quotaDEM.RigheDEM.Add(riga);
                    }
                }


                foreach (string line in lines)
                {
                    if (GetValueLineaIni(line, "[DESCRIZIONE_DEM]", out value) && quotaDEM != null)
                        quotaDEM.Descr = value;
                    if (GetValueLineaIni(line, "[DEM_NETTO_O_LORDO]", out value) && quotaDEM != null)
                        quotaDEM.RigheDEM[0].FlagLordoNetto = (value == "NETTO" ? "N" : "L");
                    if (GetValueLineaIniDecimal(line, "[PERCENTUALE_DEM]", out decValue) && quotaDEM != null)
                        quotaDEM.RigheDEM[0].PercentualeDEM = decValue;
                    if (GetValueLineaIniDecimal(line, "[PERCENTUALE_BIGLIETTO_X_DEM]", out decValue) && quotaDEM != null)
                        quotaDEM.RigheDEM[0].PercentualeIncasso = decValue;
                    if (GetValueLineaIniDecimal(line, "[PERC_APPLICA_DEM_SUPPLEMENTO]", out decValue) && quotaDEM != null)
                        quotaDEM.RigheDEM[0].PercentualeSupplemento = decValue;
                    if (GetValueLineaIniDecimal(line, "[PERC_OMAGGI_RICONOSCIUTI_X_DEM]", out decValue) && quotaDEM != null)
                        quotaDEM.RigheDEM[0].PercentualeOmaggi = decValue;
                }

                #endregion


                #region AGIS

                libBordero.Bordero.clsBordQuota quotaAGIS = null;
                libBordero.Bordero.clsBordQuotaRiga riga_AGIS = null;

                string cAGIS_PERCENTUALE_VALORE = "";
                bool lAGIS_NAZIONALE_UNICA = false;
                decimal nAGIS_NAZIONALE_VALORE = 0;
                bool lAGIS_LORDO = false;
                bool lAGIS_NAZIONALE_SABATO = false;
                bool lAGIS_NAZIONALE_DOMENICA = false;
                bool lAGIS_NAZIONALE_FESTIVI = false;
                bool lFLAG_AGIS_PRIMO_SPETTACOLO = false;

                foreach (string line in lines)
                {
                    if (line.StartsWith("[AGIS_REGIONALE]") || line.StartsWith("[AGIS_NAZIONALE]"))
                    {
                        quotaAGIS = new libBordero.Bordero.clsBordQuota();
                        quotaAGIS.IdQuota = 0;
                        quotaAGIS.Descr = "";
                        quotaAGIS.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
                        quotaAGIS.SpettIntra = "S";
                        quotaAGIS.TipiEvento = new List<libBordero.Bordero.clsBordQuotaTE>();
                        libBordero.Bordero.clsBordQuotaTE itemTe = new libBordero.Bordero.clsBordQuotaTE();
                        itemTe.TE = "01";
                        itemTe.Descr = "CINEMA";
                        quotaAGIS.TipiEvento.Add(itemTe);
                        quotaAGIS.RigheDEM = new List<libBordero.Bordero.clsBordQuotaRigaDEM>();
                        quotaAGIS.IsQuotaDEM = false;
                    }

                    if (GetValueLineaIni(line, "[AGIS_PERCENTUALE_VALORE]", out value))
                        cAGIS_PERCENTUALE_VALORE = (value == "PERCENTUALE" ? "P" : "F");

                    if (GetValueLineaIni(line, "[AGIS_NAZIONALE_UNICA]", out value))
                        lAGIS_NAZIONALE_UNICA = (value == "ABILITATO");

                    if (GetValueLineaIni(line, "[AGIS_LORDO]", out value))
                        lAGIS_LORDO = (value == "ABILITATO");

                    if (GetValueLineaIni(line, "[AGIS_NAZIONALE_SABATO]", out value))
                        lAGIS_NAZIONALE_SABATO = (value == "ABILITATO");

                    if (GetValueLineaIni(line, "[AGIS_NAZIONALE_DOMENICA]", out value))
                        lAGIS_NAZIONALE_DOMENICA = (value == "ABILITATO");

                    if (GetValueLineaIni(line, "[AGIS_NAZIONALE_FESTIVI]", out value))
                        lAGIS_NAZIONALE_FESTIVI = (value == "ABILITATO");

                    if (GetValueLineaIniDecimal(line, "[AGIS_NAZIONALE_VALORE]", out decValue) && quotaDEM != null)
                        nAGIS_NAZIONALE_VALORE = decValue;

                    if (GetValueLineaIni(line, "[FLAG_AGIS_PRIMO_SPETTACOLO]", out value))
                        lFLAG_AGIS_PRIMO_SPETTACOLO = (value == "ABILITATO");
                }

                riga_AGIS = new libBordero.Bordero.clsBordQuotaRiga();
                riga_AGIS.Riga = 1;
                riga_AGIS.CampoCalcolo = (lAGIS_LORDO ? "MXB_LORDO_TITOLO" : "MXB_NETTO_TITOLO");
                riga_AGIS.CampoConfronto = "";
                riga_AGIS.CampoConfrontoMIN = 0;
                riga_AGIS.CampoConfrontoMAX = 0;
                riga_AGIS.CodiceLocale = "";
                riga_AGIS.NumeroEventoPerSala = (lFLAG_AGIS_PRIMO_SPETTACOLO ? 1 : 0);
                riga_AGIS.Percentuale = 0;
                riga_AGIS.TipoCalcolo = cAGIS_PERCENTUALE_VALORE;
                riga_AGIS.ValoreFisso = 0;

                libBordero.Bordero.clsBordQuota quotaAGIS_REG = null;
                libBordero.Bordero.clsBordQuota quotaAGIS_NAZ = null;

                foreach (string line in lines)
                {
                    if (GetValueLineaIni(line, "[AGIS_REGIONALE]", out value))
                    {
                        Regex rAgis = new Regex(@"\d{13}|\d+");
                        if (rAgis.Match(value).Success)
                        {
                            if (quotaAGIS_REG == null)
                            {
                                quotaAGIS_REG = quotaAGIS.Clone();
                                quotaAGIS_REG.Descr = "Agis Reg.";
                            }
                            quotaAGIS_REG.Righe.Add(riga_AGIS.Clone());
                            
                            quotaAGIS_REG.Righe[quotaAGIS_REG.Righe.Count - 1].Riga = quotaAGIS_REG.Righe.Count;
                            quotaAGIS_REG.Righe[quotaAGIS_REG.Righe.Count - 1].CodiceLocale = value.Split('|')[0];
                            if (riga_AGIS.TipoCalcolo == "P")
                                quotaAGIS_REG.Righe[quotaAGIS_REG.Righe.Count - 1].Percentuale = decimal.Parse(value.Split('|')[1].Replace(".", ","));
                            else
                                quotaAGIS_REG.Righe[quotaAGIS_REG.Righe.Count - 1].ValoreFisso = decimal.Parse(value.Split('|')[1].Replace(".", ","));
                            quotaAGIS_REG.Righe[quotaAGIS_REG.Righe.Count - 1].GiorniSettimana = "11111000";
                        }
                    }

                    if (GetValueLineaIni(line, "[AGIS_NAZIONALE]", out value))
                    {
                        Regex rAgis = new Regex(@"\d{13}|\d+");
                        if (rAgis.Match(value).Success)
                        {
                            if (quotaAGIS_NAZ == null)
                            {
                                quotaAGIS_NAZ = quotaAGIS.Clone();
                                quotaAGIS_NAZ.Descr = "Agis Naz.";
                            }
                            quotaAGIS_NAZ.Righe.Add(riga_AGIS.Clone());
                            quotaAGIS_NAZ.Righe[quotaAGIS_NAZ.Righe.Count - 1].Riga = quotaAGIS_NAZ.Righe.Count;
                            quotaAGIS_NAZ.Righe[quotaAGIS_NAZ.Righe.Count - 1].CodiceLocale = value.Split('|')[0];
                            if (riga_AGIS.TipoCalcolo == "P")
                                quotaAGIS_NAZ.Righe[quotaAGIS_NAZ.Righe.Count - 1].Percentuale = decimal.Parse(value.Split('|')[1].Replace(".", ","));
                            else
                                quotaAGIS_NAZ.Righe[quotaAGIS_NAZ.Righe.Count - 1].ValoreFisso = decimal.Parse(value.Split('|')[1].Replace(".", ","));
                            quotaAGIS_NAZ.Righe[quotaAGIS_NAZ.Righe.Count - 1].GiorniSettimana = string.Format("00000{0}{1}{2}", (lAGIS_NAZIONALE_SABATO ? "1" : "0"), (lAGIS_NAZIONALE_DOMENICA ? "1" : "0"), (lAGIS_NAZIONALE_FESTIVI ? "1" : "0"));
                        }
                    }
                }

                if (lAGIS_NAZIONALE_UNICA && quotaAGIS_NAZ != null)
                {
                    if (nAGIS_NAZIONALE_VALORE > 0)
                    {
                        quotaAGIS_NAZ.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
                        quotaAGIS_NAZ.Righe.Add(riga_AGIS.Clone());
                        quotaAGIS_NAZ.Righe[0].TipoCalcolo = "F";
                        quotaAGIS_NAZ.Righe[0].Percentuale = 0;
                        quotaAGIS_NAZ.Righe[0].ValoreFisso = nAGIS_NAZIONALE_VALORE;
                    }
                    else
                        quotaAGIS_NAZ = null;
                }

                #endregion

                #region ALTRO

                /*                             0       1           2   3        4    5      6     7             8
                 [SOMME_DA_VERSARE_RANGE]ASSIC.FILM | Netto Sale | 0 | 999999 | F |  0.41 |     |             |   |
                 [SOMME_DA_VERSARE_RANGE]QUOTA      | Lordo Sale | 0 | 999999 | P |  10   | 100 |             | S |
                 [SOMME_DA_VERSARE_RANGE]PIPPO      |Lordo Sale  | 0 |123456  | F | 0.55  |     |1444444444444|   |

                */
                Dictionary<string, libBordero.Bordero.clsBordQuota> quoteAltro = new Dictionary<string, libBordero.Bordero.clsBordQuota>();
                foreach (string line in lines)
                {
                    if (GetValueLineaIni(line, "[SOMME_DA_VERSARE_RANGE]", out value))
                    {
                        if (value.Contains("|"))
                        {
                            try
                            {
                                string descr = value.Split('|')[0];
                                libBordero.Bordero.clsBordQuota quota = null;
                                if (!quoteAltro.ContainsKey(descr))
                                {
                                    quota = new libBordero.Bordero.clsBordQuota();
                                    quota.Descr = descr;
                                    quota.IsQuotaDEM = false;
                                    quota.Righe = new List<libBordero.Bordero.clsBordQuotaRiga>();
                                }
                                else
                                    quota = quoteAltro[descr];

                                quota.SpettIntra = (string.IsNullOrEmpty(value.Split('|')[8]) ? "X" : value.Split('|')[8]);

                                libBordero.Bordero.clsBordQuotaRiga riga = new libBordero.Bordero.clsBordQuotaRiga();

                                riga.TipoCalcolo = value.Split('|')[4];
                                decimal valueQuota = 0;
                                if (clsUtility.CheckTryParseDecimal(value.Split('|')[2], out valueQuota))
                                    riga.CampoConfrontoMIN = valueQuota;
                                if (clsUtility.CheckTryParseDecimal(value.Split('|')[3], out valueQuota))
                                    riga.CampoConfrontoMAX = valueQuota;
                                if (riga.TipoCalcolo == "F")
                                {
                                    if (clsUtility.CheckTryParseDecimal(value.Split('|')[5], out valueQuota))
                                        riga.ValoreFisso = valueQuota;
                                }

                                if (value.Split('|')[1].Trim().ToUpper() == "LORDO")
                                {
                                    riga.CampoConfronto = "B_TOT_LORDO_BORD";
                                    riga.CampoCalcolo = riga.CampoConfronto;
                                }
                                else if (value.Split('|')[1].Trim().ToUpper() == "NETTO")
                                { 
                                    riga.CampoConfronto = "B_TOT_NETTO_BORD";
                                    riga.CampoCalcolo = riga.CampoConfronto;
                                }
                                //else if (value.Split('|')[1].Trim().ToUpper() == "NETTO DEM")
                                //    riga.CampoConfronto = "B_TOT_NETTIS_BORD";
                                else if (value.Split('|')[1].Trim().ToUpper() == "LORDO SALE")
                                { 
                                    riga.CampoConfronto = "D_TOT_LORDO_BORD";
                                    riga.CampoCalcolo = riga.CampoConfronto;
                                }
                                else if (value.Split('|')[1].Trim().ToUpper() == "NETTO SALE")
                                { 
                                    riga.CampoConfronto = "D_TOT_NETTO_BORD";
                                    riga.CampoCalcolo = riga.CampoConfronto;
                                }


                                riga.Riga = quota.Righe.Count + 1;
                                quota.Righe.Add(riga);

                                if (!quoteAltro.ContainsKey(descr))
                                    quoteAltro.Add(descr, quota);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }

                #endregion

                if (quotaDEM != null)
                    result.QuoteDEM = new List<libBordero.Bordero.clsBordQuota>() { quotaDEM };

                if (quotaAGIS_REG != null || quotaAGIS_NAZ != null)
                    result.AltreQuote = new List<libBordero.Bordero.clsBordQuota>();
                if (quotaAGIS_REG != null)
                    result.AltreQuote.Add(quotaAGIS_REG);
                if (quotaAGIS_NAZ != null)
                    result.AltreQuote.Add(quotaAGIS_NAZ);

                if (quoteAltro.Count > 0)
                {
                    if (result.AltreQuote == null)
                        result.AltreQuote = new List<libBordero.Bordero.clsBordQuota>();
                    foreach (KeyValuePair<string, libBordero.Bordero.clsBordQuota> item_quota in quoteAltro)
                        result.AltreQuote.Add(item_quota.Value);
                }

            }
            catch (Exception ex)
            {
                oError = ex;
                result = null;
            }
            return result;
        }

        private static bool GetValueLineaIni(string line, string param, out string value)
        {
            value = "";
            bool result = false;
            if (line.StartsWith(param))
            {
                value = line.Substring(param.Length);
                result = !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value);
            }
            return result;
        }

        private static bool GetValueLineaIniDecimal(string line, string param, out decimal value)
        {
            value = 0;
            bool result = false;
            if (line.StartsWith(param))
            {
                string s_value = line.Substring(param.Length);
                if (!string.IsNullOrEmpty(s_value) && !string.IsNullOrWhiteSpace(s_value))
                {
                    if (s_value.Contains("."))
                        s_value = s_value.Replace(".", ",");
                    result = decimal.TryParse(s_value, out value);
                }
            }
            return result;
        }

        public static void RemoveEvents(Control T)
        {
            var propInfo = T.GetType().GetProperty("Events", BindingFlags.NonPublic | BindingFlags.Instance);
            var list = (EventHandlerList)propInfo.GetValue(T, null);
            list.Dispose();
        }

        public static void RemoveControlsInside(Control T)
        {
            RemoveEvents(T);
            if (T.HasChildren)
            {
                foreach (Control item in T.Controls)
                {
                    RemoveControlsInside(item);
                }
                T.Controls.Clear();
            }
        }
    }
    public class clsLabel : Label
    {
        //public bool DrawArrow = true;
        private Icon icon = null;
        private Image imageIcon = null;
        private string verticalText = "";

        private CheckState checkSt = CheckState.Indeterminate;
        public CheckState CheckState
        {
            get { return this.checkSt; }
            set { this.checkSt = value; this.Invalidate(); }
        }

        public System.Drawing.Rectangle GetRectIcon()
        {
            if (this.icon == null)
            {
                if (this.imageIcon == null)
                    return new Rectangle(0, 0, 0, 0);
                else
                    return new Rectangle(new Point(2, this.ClientSize.Height - this.imageIcon.Height - 2), new Size(this.imageIcon.Width, this.imageIcon.Height));
            }
            else
            {
                return new Rectangle(new Point(2, this.ClientSize.Height - this.icon.Height - 2), new Size(this.icon.Width, this.icon.Height));
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            StringFormat st = clsUtility.GetStringFormatFromContentAlign(base.TextAlign);
            //if (DrawArrow)
            //{
            //    int middle = this.ClientSize.Height / 2;
            //    int sizeArrow = 8;
            //    Rectangle rectArror = new Rectangle(this.ClientSize.Width - sizeArrow - 1, (this.ClientSize.Height - sizeArrow) / 2, sizeArrow, sizeArrow);
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left, middle), new Point(rectArror.Right, middle));
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left + (sizeArrow / 2), rectArror.Top), new Point(rectArror.Right, middle));
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left + (sizeArrow / 2), rectArror.Bottom), new Point(rectArror.Right, middle));
            //    Rectangle rectText = new Rectangle(0, 0, this.ClientSize.Width - rectArror.Width - 1, this.ClientSize.Height);
            //    e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), rectText, st);
            //}
            //else
            e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), this.ClientRectangle, st);

            if (this.verticalText != "")
            {
                e.Graphics.RotateTransform(-90);
                Font vFont = new Font(this.Font.FontFamily, this.Font.Size - 2);
                Size sizeText = e.Graphics.MeasureString(this.verticalText, vFont).ToSize();
                e.Graphics.DrawString(this.verticalText, vFont, new SolidBrush(this.ForeColor), -(sizeText.Width), sizeText.Height - 1);
                e.Graphics.ResetTransform();
            }

            if (this.icon != null)
                e.Graphics.DrawIcon(this.icon, this.GetRectIcon().X, this.GetRectIcon().Y);
            else if (this.imageIcon != null)
                e.Graphics.DrawImage(this.imageIcon, this.GetRectIcon().X, this.GetRectIcon().Y);

            if (this.checkSt == CheckState.Checked || this.checkSt == CheckState.Unchecked)
            {
                Point p = new Point(3, (this.ClientSize.Height - System.Windows.Forms.CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal).Height) / 2);
                System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(e.Graphics, p, (this.checkSt == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal));
            }
        }

        public Icon Icon
        {
            get { return this.icon; }
            set { this.icon = value; this.Invalidate(); }
        }

        public Image ImageIcon
        {
            get { return this.imageIcon; }
            set { this.imageIcon = value; this.Invalidate(); }
        }

        [System.ComponentModel.Browsable(false)]
        public string VerticalText
        {
            get { return this.verticalText; }
            set { this.verticalText = value; this.Invalidate(); }
        }
    }

    public class clsItemGenericData
    {
        public enum EnumGenericDataType : int
        {
            TypeNumInt = 1,
            TypeNumDecimal = 2,
            TypeString = 3,
            TypeDate = 4,
            TypeDateTime = 5
        }

        private object value { get; set; }

        public object Value
        {
            get
            {
                switch (this.DataType)
                {
                    case EnumGenericDataType.TypeNumInt:
                        {
                            int val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    int.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeNumDecimal:
                        {
                            decimal val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    decimal.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDate:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDateTime:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeString:
                        {
                            string val = "";
                            if (this.value != null)
                            {
                                try
                                {
                                    val = this.value.ToString();
                                }
                                catch (Exception)
                                {
                                    val = "";
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                }
                return null;
            }
            set
            {
                this.value = value;
            }
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public bool Readonly { get; set; }
        public bool Visible { get; set; }

        public string Important { get; set; }

        public EnumGenericDataType DataType { get; set; }

        public List<clsItemGenericData> ComboValues { get; set; }
        public System.Windows.Forms.ComboBoxStyle ComboStyle = System.Windows.Forms.ComboBoxStyle.DropDown;

        public bool IsComboMode => ComboValues != null && ComboValues.Count > 0;

        public clsItemGenericData(string code, string description, EnumGenericDataType dataType, object value, bool readOnly, bool visible = true, string important = "", List<clsItemGenericData> comboValues = null, System.Windows.Forms.ComboBoxStyle comboStyle = System.Windows.Forms.ComboBoxStyle.DropDown)
        {
            this.Code = code;
            this.Description = description;
            this.DataType = dataType;
            this.Value = value;
            this.Readonly = readOnly;
            this.Visible = visible;
            this.Important = important;
            this.ComboValues = comboValues;
            this.ComboStyle = comboStyle;
        }



        public static clsItemGenericData GetItemFromListByDescr(string description, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Description == description)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public static clsItemGenericData GetItemFromListBycODE(string code, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Code == code)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }
        public override string ToString()
        {
            return this.Description;
        }
    }

    public static class clsBorderoHelp
    {
        public static void ShowHelp(string argument)
        {
            System.IO.FileInfo oAppInfo = new System.IO.FileInfo(Application.ExecutablePath);
            string filePdf = oAppInfo.Directory.FullName + @"\Bordero4.1.pdf";
            if (!System.IO.File.Exists(filePdf))
                MessageBox.Show("Help non trovato", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                frmViewPdf frmPdf = new frmViewPdf(filePdf);
                frmPdf.Text = "Help";
                if (!string.IsNullOrEmpty(argument))
                    frmPdf.FindArgumentInPdf = argument;
                frmPdf.ShowDialog();
                frmPdf.Close();
                frmPdf.Dispose();
            }
        }
    }
}
