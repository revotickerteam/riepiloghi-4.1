﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class frmStartBordero : frmBorderoBase
    {
        public frmStartBordero()
        {
            InitializeComponent();

            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.lblMessage.Text = "Avvio Bordero...";
            this.Shown += FrmStartBordero_Shown;

        }

        private void FrmStartBordero_Shown(object sender, EventArgs e)
        {
            bool restart = false;
            Application.DoEvents();
            System.Threading.Thread.Sleep(200);
            this.Opacity = 0;
            Application.DoEvents();
            string DescConnessione = "";
            bool lContinue = false;
            libBordero.clsBorderoServiceConfig serviceConfig = libBordero.clsBorderoServiceConfigUtility.GetServiceConfig();
            string NomeConnessione = "";

            if (serviceConfig == null)
                NomeConnessione = frmSelezionaConnessione.GetNomeConnessione(out DescConnessione, out lContinue, out restart);
            else
            {
                serviceConfig = ServiceLogin(serviceConfig, out lContinue);
                if (lContinue)
                {
                    DescConnessione = serviceConfig.CodiceSistema.Descrizione;
                }
            }

            Exception error = null;
            bool connectionChecked = false;
            bool connectionFailed = false;
            if (lContinue)
            {
                if (CheckUpdates(NomeConnessione, DescConnessione, serviceConfig, out error, out connectionChecked, out connectionFailed))
                {
                    Application.Exit();
                }
                else if (error == null)
                {
                    if (serviceConfig != null || !string.IsNullOrEmpty(NomeConnessione))
                    {
                        if (serviceConfig != null || connectionChecked || this.CheckConnection(NomeConnessione, DescConnessione, out error))
                        {
                            frmMenuBordero frmMenu = new frmMenuBordero(NomeConnessione, DescConnessione, serviceConfig);
                            frmMenu.ShowDialog();
                            frmMenu.Close();
                            restart = frmMenu.Restart;
                            frmMenu.Dispose();
                        }
                        else
                            connectionFailed = true;
                    }
                    if (connectionFailed)
                    {
                        if (MessageBox.Show(string.Format("Errore di connessione:\r\n{0}\r\nImpostare la connessione ?", error.Message), "Connessione fallita", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            frmSelezionaConnessione.SetNomiConnessione(out restart);
                        }
                    }
                }
                else if (connectionFailed)
                {
                    if (MessageBox.Show(string.Format("Errore di connessione:\r\n{0}\r\nImpostare la connessione ?", error.Message), "Connessione fallita", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        frmSelezionaConnessione.SetNomiConnessione(out restart);
                    }
                    
                }
            }
            this.lblMessage.Text = (restart ? "Riavvio Bordero..." : "Chiusura Bordero...");
            this.Opacity = 1;
            Application.DoEvents();
            this.Close();
            this.Dispose();

            Application.Exit();

            if (restart && !System.Diagnostics.Debugger.IsAttached)
                Application.Restart();
        }

        private libBordero.clsBorderoServiceConfig ServiceLogin(libBordero.clsBorderoServiceConfig serviceConfig, out bool lContinue)
        {
            frmWait oFrmWait = new frmWait();
            serviceConfig.Token = "";
            serviceConfig.Account = null;
            serviceConfig.CodiceSistema = null;
            lContinue = false;
            DialogResult dialogLogin = DialogResult.OK;
            while (dialogLogin != DialogResult.Abort && !lContinue)
            {
                

                frmLogin frmLogin = new frmLogin();
                dialogLogin = frmLogin.ShowDialog();
                string login = frmLogin.Login;
                string password = frmLogin.Password;
                frmLogin.Close();
                frmLogin.Dispose();
                if (dialogLogin == DialogResult.OK)
                {
                    oFrmWait.Message = "Login in corso...";
                    oFrmWait.Show();
                    Application.DoEvents();

                    string errorMessage = "";
                    libBordero.PayLoadRiepiloghi oRequest = new libBordero.PayLoadRiepiloghi() { Login = login, Password = password };

                    BorderoAuto.serviceUserInfo = libBordero.Bordero.EncryptGen(Newtonsoft.Json.JsonConvert.SerializeObject(oRequest));

                    libBordero.ResponseRiepiloghi oResponse = libBordero.clsBorderoServiceConfigUtility.Login(oRequest, serviceConfig.Url);
                    
                    oFrmWait.Hide();

                    errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Token");
                    if (!string.IsNullOrEmpty(errorMessage))
                        MessageBox.Show(errorMessage, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        oFrmWait.Message = "Recupero Account in corso...";
                        oFrmWait.Show();
                        Application.DoEvents();

                        string token = oResponse.Data.Token;
                        oRequest = new libBordero.PayLoadRiepiloghi() { Token = token };
                        oResponse = libBordero.clsBorderoServiceConfigUtility.GetAccount(oRequest, serviceConfig.Url);

                        oFrmWait.Hide();

                        libBordero.clsAccount account = null;

                        errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Account");
                        if (!string.IsNullOrEmpty(errorMessage))
                            MessageBox.Show(errorMessage, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                        {
                            libBordero.clsCodiceSistema codiceSistema = null;
                            if (oResponse.Data.Account.CodiciSistema != null)
                            {
                                account = oResponse.Data.Account;
                                if (account.CodiciSistema.Count == 1)
                                    codiceSistema = oResponse.Data.Account.CodiciSistema[0];
                                else
                                {
                                    Dictionary<object, string> Opzioni = new Dictionary<object, string>();
                                    account.CodiciSistema.ForEach(cs =>
                                    {
                                        Opzioni.Add(cs, cs.Descrizione);
                                    });
                                    object result = frmGetGenericData.MenuGenerico(account.Cognome + " " + account.Nome, Opzioni);
                                    if (result != null)
                                        codiceSistema = (libBordero.clsCodiceSistema)result;
                                }
                            }
                            else
                            {
                                errorMessage = "Nessun codice sistema valido";
                                MessageBox.Show(errorMessage, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            
                            if (codiceSistema != null)
                            {
                                
                                oFrmWait.Message = "Recupero Codice Sistema in corso...";
                                oFrmWait.Show();
                                Application.DoEvents();

                                oRequest = new libBordero.PayLoadRiepiloghi() { Token = token, CodiceSistema = codiceSistema.CodiceSistema };
                                oResponse = libBordero.clsBorderoServiceConfigUtility.GetTokenCodiceSistema(oRequest, serviceConfig.Url);

                                oFrmWait.Hide();

                                errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Token");
                                if (!string.IsNullOrEmpty(errorMessage))
                                    MessageBox.Show(errorMessage, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                else
                                {
                                    serviceConfig.Account = account;
                                    serviceConfig.CodiceSistema = codiceSistema;
                                    if (serviceConfig.CodiceSistema != null && serviceConfig.CodiceSistema.CodiceSistema.Length > 8 && serviceConfig.CodiceSistema.CodiceSistema.Contains(".") && serviceConfig.CodiceSistema.CodiceSistema.Split('.')[0].Length == 8)
                                    {
                                        long idCinema = long.Parse(serviceConfig.CodiceSistema.CodiceSistema.Split('.')[1]);
                                        string descrCinema = (account != null && account.CodiciSistema != null && account.CodiciSistema.Count > 0 && account.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == serviceConfig.CodiceSistema.CodiceSistema) != null ? account.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == serviceConfig.CodiceSistema.CodiceSistema).Descrizione : "");
                                        serviceConfig.CodiceSistema.MultiCinema = new libBordero.clsMultiCinema() { IdCinema = idCinema, Descrizione = descrCinema };
                                    }
                                    serviceConfig.Token = oResponse.Data.Token;
                                    lContinue = true;
                                }
                            }
                        }
                    }
                }
            }
            oFrmWait.Close();
            oFrmWait.Dispose();
            if (!lContinue)
            {
                serviceConfig.Token = "";
                serviceConfig.Account = null;
            }
            return serviceConfig;
        }

        

        private bool CheckUpdates(string NomeConnessione, string DescConnessione, libBordero.clsBorderoServiceConfig serviceConfig, out Exception error, out bool connectionChecked, out bool connectionFailed)
        {
            bool lUpdates = false;
            error = null;
            connectionChecked = false;
            connectionFailed = false;
            if (!System.Diagnostics.Debugger.IsAttached)
            {

                libUpdater.frmWait.OpenWait(string.Format("Connessione {0}...", DescConnessione));
                System.Threading.Thread.Sleep(100);
                IConnection conn = null;

                if (serviceConfig == null)
                {
                    try
                    {
                        conn = libBordero.Bordero.GetConnection(NomeConnessione, out error);
                    }
                    catch (Exception exConnection)
                    {
                        error = new Exception("Errore di connessione " + exConnection.Message);
                        connectionFailed = true;
                    }
                }

                if (error == null)
                {
                    libUpdater.frmWait.OpenWait("verifica aggiornamenti");
                    connectionChecked = true;
                    try
                    {
                        if (conn != null)
                        {
                            libUpdater.Updater.CheckOracleConnectionString32x64 = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", NomeConnessione);
                            lUpdates = libUpdater.Updater.CheckForUpdatesAndUpdateRunningApplication(conn, "RP_APP_VERSION", null);
                            if (lUpdates)
                            {
                                this.Close();
                                this.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        error = new Exception("Errore durante la verifica di aggiornamenti " + ex.Message);
                    }

                }
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                libUpdater.frmWait.CloseWait();
            }
            return lUpdates;
        }

        private bool CheckConnection(string NomeConnessione, string DescConnessione, out Exception error)
        {
            error = null;
            IConnection conn = null;
            libUpdater.frmWait.OpenWait(string.Format("Connessione {0}...", DescConnessione));
            try
            {
                conn = libBordero.Bordero.GetConnection(NomeConnessione, out error);
            }
            catch (Exception exConnection)
            {
                error = new Exception("Errore di connessione " + exConnection.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            libUpdater.frmWait.CloseWait();
            return error == null;
        }
    }
}
