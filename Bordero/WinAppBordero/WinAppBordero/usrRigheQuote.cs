﻿using libBordero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class usrRigheQuote : UserControl
    {
        public libBordero.Bordero.clsBordQuota Quota { get; set; }
        public libBordero.Bordero.clsBordQuoteConfig Configurazione { get; set; }
        public string NomeConnessione { get; set; }

        private libBordero.clsBorderoServiceConfig ServiceConfig { get; set; }

        private BindingSource bindingSource { get; set; }

        public class clsRigaQuotaView : libBordero.Bordero.clsBordQuotaRiga
        {

            //private libBordero.Bordero.clsBordQuotaRiga subObject { get; set; }
            public long Riga { get { return base.Riga; } set { base.Riga = value; } }
            public string CodiceLocale { get { return base.CodiceLocale; } set { base.CodiceLocale = value; } }
            
            [System.ComponentModel.Browsable(false)]
            public string GiorniSettimana { get { return base.GiorniSettimana; } set { base.GiorniSettimana = value; } }
            
            public string DescGiorniSettimana { get; set; }
            public long NumeroEventoPerSala { get { return base.NumeroEventoPerSala; } set { base.NumeroEventoPerSala = value; } }
            [System.ComponentModel.Browsable(false)]
            public string CampoConfronto { get { return base.CampoConfronto; } set { base.CampoConfronto = value; } }
            public string DescCampoConfronto { get; set; }
            public decimal CampoConfrontoMIN { get { return base.CampoConfrontoMIN; } set { base.CampoConfrontoMIN = value; } }
            public decimal CampoConfrontoMAX { get { return base.CampoConfrontoMAX; } set { base.CampoConfrontoMAX = value; } }
            [System.ComponentModel.Browsable(false)]
            public string CampoCalcolo { get { return base.CampoCalcolo; } set { base.CampoCalcolo = value; } }
            public string TipoCalcolo { get { return base.TipoCalcolo; } set { base.TipoCalcolo = value; } }
            public string DescCampoCalcolo { set; get; }
            public decimal ValoreFisso { get { return base.ValoreFisso; } set { base.ValoreFisso = value; } }
            public decimal Percentuale { get { return base.Percentuale; } set { base.Percentuale = value; } }

            private clsRigaQuotaView()
                :base()
            {

            }
            public clsRigaQuotaView(libBordero.Bordero.clsBordQuotaRiga riga)
                : this()
            {
                //this.subObject = riga;
                this.Riga = riga.Riga;
                this.CodiceLocale = riga.CodiceLocale;
                this.GiorniSettimana = riga.GiorniSettimana;
                this.NumeroEventoPerSala = riga.NumeroEventoPerSala;
                this.CampoConfronto = riga.CampoConfronto;
                this.CampoConfrontoMIN = riga.CampoConfrontoMIN;
                this.CampoConfrontoMAX = riga.CampoConfrontoMAX;
                this.CampoCalcolo = riga.CampoCalcolo;
                this.TipoCalcolo = riga.TipoCalcolo;
                this.ValoreFisso = riga.ValoreFisso;
                this.Percentuale = riga.Percentuale;
            }

        }

        public usrRigheQuote()
        {
            InitializeComponent();
            Type dgvType = this.dgvRighe.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(this.dgvRighe, true, null);
        }

        private string GetDescCampoToView(string key)
        {
            if (this.Configurazione != null)
            {
                libBordero.Bordero.clsDescriptorField f = this.Configurazione.DescrCampiBordero.Where(q => q.Key == key).FirstOrDefault();
                if (f != null)
                    return f.Value;
                else
                    return "";
            }
            else
                return "";
        }

        private string GetDescSettimana(string value)
        {
            string result = "";
            if (value == "00000000")
                result = "mai";
            else if (value == "00000001")
                result = "solo festivi";
            else if (value == "00000110")
                result = "w.e.";
            else if (value == "00000111")
                result = "w.e. e festivi";
            else if (value == "11111111")
                result = "sempre";
            else
            {
                result += (value.Substring(0, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Lu");
                result += (value.Substring(1, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Ma");
                result += (value.Substring(2, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Me");
                result += (value.Substring(3, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Gi");
                result += (value.Substring(4, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Ve");
                result += (value.Substring(5, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Sa");
                result += (value.Substring(6, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Do");
                result += (value.Substring(7, 1) == "0" ? "" : (result.Trim() == "" ? "" : ",") + "Fs");
            }
            return result;
        }

        public void AssignData(libBordero.Bordero.clsBordQuota quota, libBordero.Bordero.clsBordQuoteConfig configurazione, string nomeConnessione, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            this.Quota = quota;
            this.Configurazione = configurazione;
            this.NomeConnessione = nomeConnessione;
            this.ServiceConfig = serviceConfig;
            if (this.Quota.IsQuotaDEM)
            {
                this.bindingSource = new BindingSource();
                foreach (libBordero.Bordero.clsBordQuotaRigaDEM riga in this.Quota.RigheDEM)
                {
                    this.bindingSource.Add(riga);
                }
                this.dgvRighe.DataSource = this.bindingSource;
                this.AssignColHeaders();
            }
            else
            {
                this.bindingSource = new BindingSource();
                foreach (libBordero.Bordero.clsBordQuotaRiga riga in this.Quota.Righe)
                {
                    clsRigaQuotaView rigaView = new clsRigaQuotaView(riga);
                    rigaView.DescCampoCalcolo = (rigaView.CampoCalcolo != null && rigaView.CampoCalcolo != "" ? GetDescCampoToView(rigaView.CampoCalcolo) : "");
                    rigaView.DescCampoConfronto = (rigaView.CampoCalcolo != null && rigaView.CampoConfronto != "" ? GetDescCampoToView(rigaView.CampoConfronto) : "");
                    rigaView.DescGiorniSettimana = (rigaView.GiorniSettimana != null && rigaView.GiorniSettimana != "" ? GetDescSettimana(rigaView.GiorniSettimana): "");
                    this.bindingSource.Add(rigaView);
                }
                this.dgvRighe.DataSource = this.bindingSource;
                this.AssignColHeaders();
            }
            this.ResizeGrid();
            this.dgvRighe.CellClick += DgvRighe_CellClick;
        }

        private void DgvRighe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.EditCell(e.RowIndex, e.ColumnIndex);
        }

        private int NextEditColumnIndex(int columnIndex)
        {
            int result = -1;
            for (int indexCol = columnIndex + 1; indexCol < this.dgvRighe.Columns.Count; indexCol++)
            {
                if (this.dgvRighe.Columns[indexCol].Visible)
                {
                    result = indexCol;
                    break;
                }
            }
            return result;
        }

        private void EditCell(int rowIndex, int columnIndex, bool autoEdit = false)
        {
            if (rowIndex >= 0)
            {
                bool valueEdited = false;
                bool goToNextCol = false;
                DataGridViewColumn col = this.dgvRighe.Columns[columnIndex];
                if (this.Quota.IsQuotaDEM)
                {
                    libBordero.Bordero.clsBordQuotaRigaDEM rowData = (libBordero.Bordero.clsBordQuotaRigaDEM)this.dgvRighe.Rows[rowIndex].DataBoundItem;
                    switch (col.DataPropertyName)
                    {
                        case "Riga": { col.HeaderText = "Riga"; break; }
                        case "FlagLordoNetto": { object value = rowData.FlagLordoNetto; if (GetMenu(col.HeaderText, new Dictionary<object, string>() { { "L", "Lordo" }, { "N", "Netto" } }, ref value, value)) { rowData.FlagLordoNetto = value.ToString(); valueEdited = true; } break; }
                        case "PercentualeIncasso": { decimal value = rowData.PercentualeIncasso; if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value)) { rowData.PercentualeIncasso = value; valueEdited = true; } break; }
                        case "PercentualeDEM": { decimal value = rowData.PercentualeDEM; if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value)) { rowData.PercentualeDEM = value; valueEdited = true; } break; }
                        case "PercentualeSupplemento": { decimal value = rowData.PercentualeSupplemento; if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value)) { rowData.PercentualeSupplemento = value; valueEdited = true; } break; }
                        case "FlgSupplDemEcc": 
                            {
                                object value = rowData.FlgSupplDemEcc.ToString();
                                Dictionary<object, string> optionsSupplPercEcc = new Dictionary<object, string>();
                                optionsSupplPercEcc.Add("0", "Mai");
                                if (rowData.PercentualeSupplemento > 0)
                                    optionsSupplPercEcc.Add("1", $"Se il valore del supplemento supera il {rowData.PercentualeSupplemento}% del prezzo del biglietto");
                                if (rowData.PercentualeSupplemento > 0)
                                    optionsSupplPercEcc.Add("2", $"Se il valore del supplemento supera il {rowData.PercentualeSupplemento}% del prezzo del biglietto\r\nSOLO PER LA PARTE ECCEDENTE IL {rowData.PercentualeSupplemento}%");
                                optionsSupplPercEcc.Add("3", "Sempre");
                                if (GetMenu(col.HeaderText, optionsSupplPercEcc, ref value, value))
                                {
                                    rowData.FlgSupplDemEcc = long.Parse(value.ToString());
                                    valueEdited = true;
                                }
                                break; 
                            }
                        case "PercentualeOmaggi": { decimal value = rowData.PercentualeOmaggi; if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value)) { rowData.PercentualeOmaggi = value; valueEdited = true; } break; }
                        case "ValoreMinimo": { decimal value = rowData.ValoreMinimo; if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value)) { rowData.ValoreMinimo = value; valueEdited = true; } break; }
                        case "FlagScontoP10": 
                            {
                                rowData.FlagScontoP10 = MessageBox.Show("Sconto Prestazioni Complementari P10", "Dem sconto P10", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                                valueEdited = true;
                                break; 
                            }
                        case "DataEvento": 
                            {
                                if (!autoEdit)
                                {
                                    DateTime value = rowData.DataEvento;
                                    if (GetValueDate(col.DataPropertyName, col.HeaderText, ref value))
                                    {
                                        rowData.DataEvento = value;
                                        valueEdited = true;
                                    }
                                }
                                break; 
                            }
                        case "FlagDataEvento": { col.Visible = false; break; }
                        case "TitoloEvento": { col.HeaderText = "un Titolo Evento"; break; }
                        case "FlagTitoloEvento": { col.Visible = false; break; }
                        case "CodiceLocale": { col.Visible = false; break; }
                        case "FlagCodiceLocale": { col.HeaderText = "FlagCodiceLocale"; col.Visible = false; break; }
                        case "IdSala": { col.HeaderText = "una sala specifica"; break; }
                        case "DescrCodiceLocale":
                            {
                                if (!autoEdit)
                                {
                                    object value = null;
                                    Dictionary<object, string> items = new Dictionary<object, string>();
                                    foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
                                    {
                                        if (!items.ContainsKey(item.Key))
                                            items.Add(item.Key, item.Value);
                                    }
                                    if (GetMenu(col.HeaderText, items, ref value, value))
                                    {
                                        foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
                                        {
                                            if (item.Key == value.ToString())
                                            {
                                                rowData.CodiceLocale = item.Key;
                                                rowData.DescrCodiceLocale = item.Value;
                                                valueEdited = true;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        case "DataOraEvento": 
                            {
                                if (!autoEdit)
                                {
                                    DateTime value = rowData.DataOraEvento;
                                    if (GetValueDateTime(col.DataPropertyName, col.HeaderText, ref value))
                                    {
                                        rowData.DataOraEvento = value;
                                        valueEdited = true;
                                    }
                                }
                                break; 
                            }
                        case "FlagEventoSpecifico":
                            {
                                if (!autoEdit)
                                {
                                    libBordero.Bordero.clsSearchSingoloEvento eventoSelezionato = null;
                                    if (this.GetEventoSpecifico(out eventoSelezionato))
                                    {
                                        //rowData.FlagDataEvento = false;
                                        rowData.FlagCodiceLocale = false;
                                        //rowData.FlagTitoloEvento = false;
                                        rowData.FlagEventoSpecifico = false;
                                        rowData.DataEvento = DateTime.MinValue;
                                        rowData.DataOraEvento = DateTime.MinValue;
                                        rowData.CodiceLocale = null;
                                        rowData.DescrCodiceLocale = null;
                                        rowData.TitoloEvento = null;

                                        if (eventoSelezionato.DataOraEvento != null)
                                        {
                                            if (eventoSelezionato.DataOraEvento == eventoSelezionato.DataOraEvento.Date)
                                            {
                                                rowData.DataEvento = eventoSelezionato.DataOraEvento;
                                                //rowData.FlagDataEvento = true;
                                            }
                                            else
                                            {
                                                rowData.DataOraEvento = eventoSelezionato.DataOraEvento;
                                                //rowData.FlagDataEvento = true;
                                            }
                                        }

                                        if (eventoSelezionato.CodiceLocale != null)
                                        {

                                            foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
                                            {
                                                if (eventoSelezionato.CodiceLocale == item.Key)
                                                {
                                                    rowData.DescrCodiceLocale = item.Value;
                                                    rowData.CodiceLocale = eventoSelezionato.CodiceLocale;
                                                    rowData.FlagCodiceLocale = true;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                        }

                                        if (eventoSelezionato.TitoloEvento != null && !string.IsNullOrEmpty(eventoSelezionato.TitoloEvento) && !string.IsNullOrWhiteSpace(eventoSelezionato.TitoloEvento))
                                            rowData.TitoloEvento = eventoSelezionato.TitoloEvento;
                                        else
                                            rowData.TitoloEvento = null;

                                        rowData.FlagEventoSpecifico = rowData.DataOraEvento != DateTime.MinValue && rowData.TitoloEvento != "" && rowData.CodiceLocale != "";
                                        valueEdited = true;
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    SetHelpForm(true, col.DataPropertyName);
                    delegateOnShownEvent delegateOnShown = new delegateOnShownEvent(DelegateSetHelpForm);

                    clsRigaQuotaView rowData = (clsRigaQuotaView)this.dgvRighe.Rows[rowIndex].DataBoundItem;
                    switch (col.DataPropertyName)
                    {
                        case "Riga": { col.HeaderText = "Riga"; break; }
                        case "CodiceLocale":
                            {
                                object value = null;
                                Dictionary<object, string> items = new Dictionary<object, string>();
                                foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
                                {
                                    if (!items.ContainsKey(item.Key))
                                        items.Add(item.Key, item.Value);
                                }
                                items.Add("", "Nessun codice locale");
                                if (GetMenu("Applicare il calcolo solo per una sala ?", items, ref value, rowData.CodiceLocale, delegateOnShown))
                                {
                                    if (value == "")
                                    {
                                        valueEdited = true;
                                    }
                                    else
                                    {
                                        foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
                                        {
                                            if (item.Key == value.ToString())
                                            {
                                                rowData.CodiceLocale = item.Key;
                                                valueEdited = true;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        case "GiorniSettimana":
                        case "DescGiorniSettimana":
                            {
                                object value = null;
                                Dictionary<object, string> items = new Dictionary<object, string>();
                                items.Add("1", "Lunedi");
                                items.Add("2", "Martedi");
                                items.Add("3", "Mercoledi");
                                items.Add("4", "Giovedi");
                                items.Add("5", "Venerdi");
                                items.Add("6", "Sabato");
                                items.Add("7", "Domenica");
                                items.Add("8", "Festivi");

                                Dictionary<object, bool> itemsChecked = null;
                                if (rowData.GiorniSettimana != null && !string.IsNullOrEmpty(rowData.GiorniSettimana) && !string.IsNullOrWhiteSpace(rowData.GiorniSettimana))
                                {
                                    itemsChecked = new Dictionary<object, bool>();
                                    for (int indxDay = 0; indxDay < rowData.GiorniSettimana.Length; indxDay++)
                                    {
                                        if (rowData.GiorniSettimana.Substring(indxDay, 1) == "1")
                                            itemsChecked.Add((indxDay + 1).ToString(), true);
                                    }
                                }

                                if (GetMenuCheck("Indicare in quali giorni della settimana appicare il calcolo", items, ref value, itemsChecked, delegateOnShown))
                                {
                                    valueEdited = true;
                                    try
                                    {
                                        Dictionary<object, string> lista = (Dictionary<object, string>)value;
                                        string settimana = "";
                                        foreach (KeyValuePair<object, string> giornoS in items)
                                        {
                                            if (lista.Where(w => w.Key == giornoS.Key).Any())
                                                settimana += "1";
                                            else
                                                settimana += "0";
                                        }
                                        rowData.GiorniSettimana = settimana;
                                        rowData.DescGiorniSettimana = GetDescSettimana(rowData.GiorniSettimana);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                break;
                            }
                        case "NumeroEventoPerSala": 
                            { 
                                long value = rowData.NumeroEventoPerSala;
                                if (GetValueLong(col.DataPropertyName, col.HeaderText + " (0 su tutti gli spettacoli, 1 se per es. sul 1° evento nella sala)", ref value, delegateOnShown))
                                {
                                    rowData.NumeroEventoPerSala = value;
                                    valueEdited = true;
                                }
                                break; 
                            }
                        case "TipoCalcolo": 
                            { 
                                object value = rowData.TipoCalcolo;
                                if (GetMenu("E' un valore fisso o una percentuale di ... ?", new Dictionary<object, string>() { { "P", "Percentuale" }, { "F", "Fisso" } }, ref value, value, delegateOnShown))
                                {
                                    rowData.TipoCalcolo = value.ToString();
                                    if (rowData.TipoCalcolo == "P")
                                    {
                                        rowData.ValoreFisso = 0;
                                    }
                                    else if (rowData.TipoCalcolo == "F")
                                    {
                                        rowData.Percentuale = 0;
                                        rowData.CampoCalcolo = "";
                                        rowData.DescCampoCalcolo = "";
                                    }
                                    valueEdited = true;
                                }
                                break; 
                            }
                        case "CampoConfrontoMAX": 
                            {
                                if (!string.IsNullOrEmpty(rowData.CampoConfronto) && !string.IsNullOrWhiteSpace(rowData.CampoConfronto))
                                {
                                    decimal value = rowData.CampoConfrontoMAX;
                                    if (GetValueDecimal(col.DataPropertyName, col.HeaderText + " di " + rowData.DescCampoConfronto, ref value, delegateOnShown))
                                    {
                                        rowData.CampoConfrontoMAX = value;
                                        valueEdited = true;
                                    }
                                }
                                else
                                    goToNextCol = true;
                                break; 
                            }
                        case "CampoConfrontoMIN": 
                            {
                                if (!string.IsNullOrEmpty(rowData.CampoConfronto) && !string.IsNullOrWhiteSpace(rowData.CampoConfronto))
                                {
                                    decimal value = rowData.CampoConfrontoMIN;
                                    if (GetValueDecimal(col.DataPropertyName, col.HeaderText + " di " + rowData.DescCampoConfronto, ref value, delegateOnShown))
                                    {
                                        rowData.CampoConfrontoMIN = value;
                                        valueEdited = true;
                                    }
                                }
                                else
                                    goToNextCol = true;
                                break; 
                            }
                        case "ValoreFisso": 
                            {
                                if (rowData.TipoCalcolo == "F")
                                {
                                    decimal value = rowData.ValoreFisso;
                                    if (GetValueDecimal(col.DataPropertyName, col.HeaderText, ref value, delegateOnShown))
                                    {
                                        rowData.ValoreFisso = value;
                                        valueEdited = true;
                                    }
                                }
                                else
                                    goToNextCol = true;
                                break; 
                            }
                        case "Percentuale": 
                            {
                                if (rowData.TipoCalcolo == "P" && !string.IsNullOrEmpty(rowData.CampoCalcolo) && !string.IsNullOrWhiteSpace(rowData.CampoCalcolo))
                                {
                                    decimal value = rowData.Percentuale;
                                    if (GetValueDecimal(col.DataPropertyName, col.HeaderText + " di " + rowData.DescCampoCalcolo, ref value, delegateOnShown))
                                    {
                                        rowData.Percentuale = value;
                                        valueEdited = true;
                                    }
                                }
                                else
                                    goToNextCol = true;
                                break; 
                            }
                        case "DescCampoConfronto":
                        case "DescCampoCalcolo":
                        case "CampoConfronto":
                        case "CampoCalcolo":
                            {
                                bool lContinue = true;
                                bool proceedToNextControl = true;
                                string corrCampo = "";
                                string corrCampoDes = "";
                                if (lContinue)
                                {
                                    if (col.DataPropertyName == "DescCampoConfronto" || col.DataPropertyName == "CampoConfronto")
                                    {
                                        object value = (!string.IsNullOrEmpty(rowData.CampoConfronto) && !string.IsNullOrWhiteSpace(rowData.CampoConfronto) ? rowData.CampoConfronto : null);
                                        Dictionary<object, string> items = new Dictionary<object, string>();

                                        items.Add(System.DBNull.Value, "Per applicare questa riga quota verificare un valore del Borderò ?");

                                        if (value != null)
                                        {
                                            items.Add(value, "Utilizzare il campo di confronto attuale: " + rowData.DescCampoConfronto);
                                            items.Add("SI", "Utilizzare un altro campo di confronto");
                                        }
                                        else
                                            items.Add("SI", "Utilizzare un campo di confronto");

                                        items.Add("NO", "Non utilizzare un campo di confronto per applicare questa riga");

                                        lContinue = GetMenu("Condizione di calcolo", items, ref value, value, delegateOnShown);

                                        //lContinue = MessageBox.Show(string.Format("Applicare questa quota in base ad un valore del Borderò ?{0}",
                                        //                                          (!string.IsNullOrEmpty(rowData.CampoConfronto) && !string.IsNullOrWhiteSpace(rowData.CampoConfronto) ? "\r\nCampo di confronto attuale:\r\n" + rowData.DescCampoConfronto : "")),
                                        //                                          "Calcolo condizionale", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (!string.IsNullOrEmpty(rowData.CampoConfronto) ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes;
                                        proceedToNextControl = lContinue;
                                        if (lContinue && value != null)
                                        {
                                            if (value.ToString() == "NO")
                                            {
                                                rowData.CampoConfrontoMIN = 0;
                                                rowData.CampoConfrontoMAX = 0;
                                                rowData.CampoConfronto = "";
                                                lContinue = false;
                                            }
                                            else if (value.ToString() == "SI")
                                            {
                                                corrCampo = "";
                                                corrCampoDes = "";
                                            }
                                            else if (!string.IsNullOrEmpty(rowData.CampoConfronto) && !string.IsNullOrWhiteSpace(rowData.CampoConfronto) && value.ToString() == rowData.CampoConfronto)
                                            {
                                                corrCampo = rowData.CampoConfronto;
                                                corrCampoDes = rowData.DescCampoConfronto;
                                            }
                                        }
                                        //if (!lContinue || (value != null && value.ToString() == "NO"))
                                        //{
                                        //    rowData.CampoConfrontoMIN = 0;
                                        //    rowData.CampoConfrontoMAX = 0;
                                        //    rowData.CampoConfronto = "";
                                        //}
                                        //else
                                        //{
                                        //    corrCampo = rowData.CampoConfronto;
                                        //    corrCampoDes = rowData.DescCampoConfronto;
                                        //    if (!string.IsNullOrEmpty(corrCampo) && MessageBox.Show("Utilizzare sempre il valore del Borderò " + corrCampoDes + " ?", "Calcolo condizionale", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                        //    {
                                        //        corrCampo = "";
                                        //        corrCampoDes = "";
                                        //    }
                                        //}
                                    }
                                }

                                if (lContinue)
                                {
                                    lContinue = ((col.DataPropertyName != "DescCampoCalcolo" && col.DataPropertyName != "CampoCalcolo") || rowData.TipoCalcolo == "P");

                                    if (lContinue && (col.DataPropertyName == "CampoCalcolo" || col.DataPropertyName == "DescCampoCalcolo"))
                                    {
                                        corrCampo = rowData.CampoCalcolo;
                                        corrCampoDes = rowData.DescCampoCalcolo;
                                        if (!string.IsNullOrEmpty(corrCampo))
                                        {
                                            object value = corrCampo;
                                            Dictionary<object, string> items = new Dictionary<object, string>();

                                            items.Add(value, "Utilizzare sempre il valore del Borderò " + corrCampoDes);
                                            items.Add("SI", "Utilizzare un altro valore del Borderò");

                                            lContinue = GetMenu("Calcolo Percentuale", items, ref value, value, delegateOnShown);
                                            proceedToNextControl = lContinue;
                                            if (lContinue && value != null && value.ToString() == "SI")
                                            {
                                                corrCampo = "";
                                                corrCampoDes = "";
                                            }
                                        }

                                        //if (!string.IsNullOrEmpty(corrCampo) && MessageBox.Show("Utilizzare sempre il valore del Borderò " + corrCampoDes + " ?", "Calcolo percentuale", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                        //{
                                        //    corrCampo = "";
                                        //    corrCampoDes = "";
                                        //}
                                    }
                                }

                                if (lContinue)
                                {
                                    object value = null;
                                    Dictionary<object, string> items = new Dictionary<object, string>();

                                    object filterCampi = "";
                                    items.Add("S", "Valori unitari");
                                    items.Add("B", "Totali Bordero");
                                    items.Add("D", "Totali Giornata");

                                    if (corrCampo != null && corrCampo != "")
                                    {
                                        if (col.DataPropertyName == "CampoConfronto" || col.DataPropertyName == "DescCampoConfronto")
                                        {
                                            rowData.CampoConfronto = corrCampo;
                                            rowData.DescCampoConfronto = corrCampoDes;
                                        }
                                        else if (col.DataPropertyName == "CampoCalcolo" || col.DataPropertyName == "DescCampoCalcolo")
                                        {
                                            rowData.CampoCalcolo = corrCampo;
                                            rowData.DescCampoCalcolo = corrCampoDes;
                                        }
                                        valueEdited = true;
                                    }
                                    else
                                    {
                                        if (GetMenu(col.HeaderText, items, ref filterCampi, null, delegateOnShown))
                                        {
                                            items = new Dictionary<object, string>();
                                            //var query = this.Configurazione.DescrCampiBordero.Where(q => !q.Key.StartsWith("B_") && !q.Key.StartsWith("D_"));

                                            List<libBordero.Bordero.clsDescriptorField> listaCampi = null;

                                            if (filterCampi.ToString() == "S")
                                                listaCampi = this.Configurazione.DescrCampiBordero.Where(q => !q.Key.StartsWith("B_") && !q.Key.StartsWith("D_")).ToList();
                                            else if (filterCampi.ToString() == "B")
                                                listaCampi = this.Configurazione.DescrCampiBordero.Where(q => q.Key.StartsWith("B_")).ToList();
                                            else
                                                listaCampi = this.Configurazione.DescrCampiBordero.Where(q => q.Key.StartsWith("D_")).ToList();

                                            foreach (libBordero.Bordero.clsDescriptorField item in listaCampi)
                                            {
                                                if (!items.ContainsKey(item.Key))
                                                    items.Add(item.Key, item.Value);
                                            }
                                            if (GetMenu(col.HeaderText, items, ref value, value, delegateOnShown))
                                            {
                                                libBordero.Bordero.clsDescriptorField item = this.Configurazione.DescrCampiBordero.Where(w => w.Key == value.ToString()).FirstOrDefault();
                                                if (item != null)
                                                {
                                                    if (col.DataPropertyName == "CampoConfronto" || col.DataPropertyName == "DescCampoConfronto")
                                                    {
                                                        rowData.CampoConfronto = item.Key;
                                                        rowData.DescCampoConfronto = item.Value;
                                                        valueEdited = true;
                                                    }
                                                    else if (col.DataPropertyName == "CampoCalcolo" || col.DataPropertyName == "DescCampoCalcolo")
                                                    {
                                                        rowData.CampoCalcolo = item.Key;
                                                        rowData.DescCampoCalcolo = item.Value;
                                                        valueEdited = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                            }
                                        }
                                        else
                                            proceedToNextControl = false;
                                    }
                                }
                                else
                                    goToNextCol = proceedToNextControl;
                                break;
                            }
                    }
                    if (valueEdited)
                    {
                        this.dgvRighe.Refresh();
                        foreach (libBordero.Bordero.clsBordQuotaRiga item in this.Quota.Righe)
                        {
                            if (item.Riga == rowData.Riga)
                            {
                                item.SetFromOther(rowData);
                                break;
                            }
                        }
                        this.ResizeGrid();
                    }
                }
                if (valueEdited || goToNextCol)
                {
                    int nextColEdit = this.NextEditColumnIndex(columnIndex);
                    if (nextColEdit >= 0)
                        this.EditCell(rowIndex, nextColEdit, true);
                }
            }
            SetHelpForm(false, "");
        }

        private class formHelpForm : frmBorderoBase
        { 
            public Form PositionForm { get; set; }

            public formHelpForm()
                :base()
            {
                SetStyle(ControlStyles.Selectable, false);
                this.TopMost = true;
                this.ShowInTaskbar = false;
            }
        }

        private formHelpForm helpForm;

        private class ArrowStep : Panel
        {
            private bool isBeg = false;
            private bool IsEnd = false;
            private bool IsCorr = false;

            public ArrowStep(int index, bool isCorr)
            {
                this.isBeg = index == 0;
                this.IsEnd = index == -1;
                this.IsCorr = isCorr;
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int middle = this.Width / 2;
                int dim = 7;
                Pen pen = (this.Enabled ? Pens.DarkGreen : Pens.LightGray);
                Brush brush  = (this.Enabled ? Brushes.DarkGreen : Brushes.LightGray);
                System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                
                path.AddEllipse(new RectangleF((this.ClientSize.Width - dim) / 2 - .2f , (this.ClientSize.Height - dim) / 2, dim, dim));
                e.Graphics.FillPath(brush, path);
                path.Dispose();
                path = new System.Drawing.Drawing2D.GraphicsPath();
                path.AddLine(new Point(middle, (this.isBeg ? 10 : 0)), new Point(middle, this.ClientSize.Height - (this.IsEnd ? 10 : 0)));
                e.Graphics.DrawPath(pen, path);
                path.Dispose();

                if (!IsEnd && IsCorr)
                {
                    path = new System.Drawing.Drawing2D.GraphicsPath();
                    path.AddLine(new Point(middle - 5, this.ClientSize.Height - 5), new Point(middle, this.ClientSize.Height - 1));
                    path.AddLine(new Point(middle + 5, this.ClientSize.Height - 5), new Point(middle, this.ClientSize.Height - 1));
                    e.Graphics.DrawPath(pen, path);
                }

            }
        }

        private void DelegateSetHelpForm(Form positionForm)
        {
            if (helpForm != null)
            {
                helpForm.PositionForm = positionForm;
                helpForm.Show();
                helpForm.BringToFront();
            }
        }

        private void SetHelpForm(bool show, string dataPropertyName)
        {
            if (helpForm != null)
            {
                helpForm.Close();
                helpForm.Dispose();
                helpForm = null;
            }
            if (show)
            {
                //int height = 0;
                helpForm = new formHelpForm();
                helpForm.StartPosition = FormStartPosition.Manual;
                helpForm.ControlBox = false;
                helpForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                helpForm.Text = "";
                helpForm.Font = new Font("Calibri", 10, FontStyle.Regular);

                clsLabel lblTitle = new clsLabel();
                lblTitle.Text = "Configurazione riga";
                lblTitle.TextAlign = ContentAlignment.MiddleCenter;
                lblTitle.AutoSize = false;
                lblTitle.BorderStyle = BorderStyle.FixedSingle;
                lblTitle.Dock = DockStyle.Top;
                helpForm.Controls.Add(lblTitle);
                lblTitle.BringToFront();

                Panel pnlVert = new Panel();
                pnlVert.Width = 50;
                //pnlVert.BorderStyle = BorderStyle.FixedSingle;
                pnlVert.Dock = DockStyle.Left;
                helpForm.Controls.Add(pnlVert);
                pnlVert.BringToFront();

                Panel pnlSteps = new Panel();
                pnlSteps.Width = 25;
                pnlSteps.BorderStyle = BorderStyle.FixedSingle;
                pnlSteps.Dock = DockStyle.Left;
                helpForm.Controls.Add(pnlSteps);
                pnlSteps.BringToFront();

                Panel pnlData = new Panel();
                pnlData.Dock = DockStyle.Fill;
                helpForm.Controls.Add(pnlData);
                pnlData.BringToFront();

                Panel pnlItemsCond = new Panel();
                pnlItemsCond.Height = pnlItemsCond.Padding.Vertical;
                pnlItemsCond.Dock = DockStyle.Top;
                pnlItemsCond.BorderStyle = BorderStyle.FixedSingle;
                pnlData.Controls.Add(pnlItemsCond);
                pnlItemsCond.BringToFront();

                Panel pnlItemsCalc = new Panel();
                pnlItemsCalc.Height = pnlItemsCalc.Padding.Vertical;
                pnlItemsCalc.Dock = DockStyle.Fill;
                pnlItemsCalc.BorderStyle = BorderStyle.FixedSingle;
                pnlData.Controls.Add(pnlItemsCalc);
                pnlItemsCalc.BringToFront();

                clsLabel lblCondizione = new clsLabel();
                lblCondizione.Font = new Font(helpForm.Font.FontFamily, helpForm.Font.Size + 2);
                lblCondizione.BorderStyle = BorderStyle.FixedSingle;
                lblCondizione.VerticalText = "Quando calcolare\r\n";
                lblCondizione.Dock = DockStyle.Top;
                pnlVert.Controls.Add(lblCondizione);
                lblCondizione.BringToFront();

                clsLabel lblCalcolo = new clsLabel();
                lblCalcolo.Font = new Font(helpForm.Font.FontFamily, helpForm.Font.Size + 2);
                lblCalcolo.BorderStyle = BorderStyle.FixedSingle;
                lblCalcolo.VerticalText = "Come calcolare ";
                lblCalcolo.Dock = DockStyle.Fill;
                pnlVert.Controls.Add(lblCalcolo);
                lblCalcolo.BringToFront();

                int modeLabel = 0;
                bool isCalc = false;
                foreach (DataGridViewColumn col in this.dgvRighe.Columns)
                {
                    if (col.Visible && col.DataPropertyName.Trim().ToUpper() != "RIGA")
                    {
                        if (col.DataPropertyName == "TipoCalcolo")
                            isCalc = true;
                        if (col.DataPropertyName == dataPropertyName)
                        {
                            modeLabel = 1;
                            if (isCalc)
                                lblCalcolo.BackColor = Color.LightGray;
                            else
                                lblCondizione.BackColor = Color.LightGray;
                        }
                        else if (modeLabel == 1)
                            modeLabel = 2;
                        clsLabel lbl = new clsLabel();
                        lbl.Tag = modeLabel;
                        lbl.Dock = DockStyle.Top;
                        lbl.Text = col.HeaderText;
                        lbl.Font = new Font(helpForm.Font.FontFamily, 10, (modeLabel == 0 ? FontStyle.Regular : (modeLabel == 1 ? FontStyle.Bold : FontStyle.Italic)));
                        if (modeLabel == 2)
                            lbl.ForeColor = Color.DarkGray;
                        if (!isCalc)
                            pnlItemsCond.Controls.Add(lbl);
                        else
                            pnlItemsCalc.Controls.Add(lbl);
                        lbl.BringToFront();
                    }
                }
                
                
                helpForm.Shown += (o, e) =>
                {
                    helpForm.Refresh();
                    int height = 0;
                    int maxBottom = 0;
                    foreach (clsLabel lblCheck in pnlItemsCond.Controls)
                    {
                        maxBottom = System.Math.Max(maxBottom, lblCheck.Bottom);
                        ArrowStep ar = new ArrowStep((lblCheck.Top == 0 ? 0 : 1), lblCheck.Font.Style == FontStyle.Bold);
                        ar.Enabled = lblCheck.Font.Style != FontStyle.Italic;
                        ar.Height = lblCheck.Height;
                        ar.Width = pnlSteps.Width;
                        pnlSteps.Controls.Add(ar);
                        ar.BringToFront();
                        ar.Location = new Point(0, lblCheck.Top);
                        ar.Invalidate();
                    }
                    height = maxBottom + pnlItemsCond.Padding.Vertical;

                    pnlItemsCond.Height = height;
                    lblCondizione.Height = height;

                    maxBottom = 0;
                    foreach (clsLabel lblCheck in pnlItemsCalc.Controls)
                        maxBottom = System.Math.Max(maxBottom, lblCheck.Bottom);

                    foreach (clsLabel lblCheck in pnlItemsCalc.Controls)
                    {
                        ArrowStep ar = new ArrowStep((maxBottom == lblCheck.Bottom ? -1 : 1), lblCheck.Font.Style == FontStyle.Bold);
                        ar.Enabled = lblCheck.Font.Style != FontStyle.Italic;
                        ar.Height = lblCheck.Height;
                        ar.Width = pnlSteps.Width;
                        pnlSteps.Controls.Add(ar);
                        ar.BringToFront();
                        ar.Location = new Point(0, lblCheck.Top + pnlItemsCond.Bottom);
                        ar.Invalidate();
                    }
                    height += lblTitle.Bottom + maxBottom + pnlItemsCalc.Padding.Vertical + 3;
                    helpForm.ClientSize = new Size(300, height);

                    if (helpForm.PositionForm != null)
                    {
                        helpForm.Left = helpForm.PositionForm.Left - helpForm.Width;
                        helpForm.Top = (helpForm.PositionForm.Top + helpForm.PositionForm.Height / 2) - helpForm.Height / 2;
                    }
                    else
                        helpForm.Location = new Point(1, 1);
                };
            }
        }

        public void ResizeGrid()
        {
            if (this.dgvRighe.AutoSizeColumnsMode == DataGridViewAutoSizeColumnsMode.None)
            {
                this.dgvRighe.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                this.dgvRighe.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                this.dgvRighe.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells);
                this.dgvRighe.Refresh();
            }
            int height = this.dgvRighe.ColumnHeadersHeight;
            height += this.dgvRighe.RowTemplate.Height * (this.dgvRighe.Rows.Count + 1);
            //foreach (DataGridViewRow dataGridRow in this.dgvRighe.Rows)
            //{
            //    height += this.dgvRighe.RowTemplate.Height * this.dgvRighe.Rows;
            //}
            //if (this.dgvRighe.Controls.OfType<HScrollBar>().First().Visible)
            //{
            //    height += this.dgvRighe.Controls.OfType<HScrollBar>().First().Height;
            //}
            if (this.dgvRighe.Height != height)
                this.dgvRighe.Height = height;
            if (this.ClientSize.Height != this.pnlMenu.Height + this.dgvRighe.Height + 4)
                this.ClientSize = new Size(this.ClientSize.Width, this.pnlMenu.Height + this.dgvRighe.Height + 4);
        }

        public void AssignColHeaders()
        {
            if (this.Quota.IsQuotaDEM)
            {
                foreach (DataGridViewColumn col in this.dgvRighe.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    switch (col.DataPropertyName)
                    {
                        case "Riga": { col.HeaderText = "Riga"; break; }
                        case "FlagLordoNetto": { col.HeaderText = "calcolo sul Lordo o Netto"; break; }
                        case "PercentualeIncasso": { col.HeaderText = "% Incasso da considerare"; break; }
                        case "PercentualeDEM": { col.HeaderText = "% " + this.Quota.Descr; break; }
                        case "PercentualeSupplemento": { col.HeaderText = string.Format("applica {0} se il Supplemento supera una % del prezzo del biglietto", this.Quota.Descr); break; }
                        case "FlgSupplDemEcc": { col.HeaderText = string.Format("Modalità calcolo {0} sul Supplemento", this.Quota.Descr); break; }
                        case "PercentualeOmaggi": { col.HeaderText = string.Format("% Omaggi ai fini {0}", this.Quota.Descr); break; }
                        case "ValoreMinimo": { col.HeaderText = string.Format("Valore Minimo {0} da pagare comunque", this.Quota.Descr); break; }
                        case "FlagScontoP10": { col.HeaderText = "Sconto prestaz. Compl. P10"; break; }
                        case "DataEvento": { col.HeaderText = "una Data Evento"; break; }
                        case "FlagDataEvento": { col.Visible = false; break; }
                        case "TitoloEvento": { col.HeaderText = "un Titolo Evento"; break; }
                        case "FlagTitoloEvento": { col.Visible = false; break; }
                        case "CodiceLocale": { col.Visible = false; break; }
                        case "FlagCodiceLocale": { col.HeaderText = "FlagCodiceLocale"; col.Visible = false; break; }
                        case "IdSala": { col.HeaderText = "una sala specifica"; break; }
                        case "DescrCodiceLocale": { col.HeaderText = "un Codice Locale"; break; }
                        case "DataOraEvento": { col.HeaderText = "una data e ora Evento"; break; }
                        case "FlagEventoSpecifico": { col.HeaderText = "Ricerca evento"; break; }
                    }
                }
            }
            else
            {
                foreach (DataGridViewColumn col in this.dgvRighe.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    switch (col.DataPropertyName)
                    {
                        case "Riga": { col.HeaderText = "Riga"; break; }
                        case "CodiceLocale": { col.HeaderText = "un Codice locale"; break; }
                        case "DescGiorniSettimana": { col.HeaderText = "Giorni Settimana"; break; }
                        case "NumeroEventoPerSala": { col.HeaderText = "Numero Evento per sala"; break; }
                        case "DescCampoConfronto": { col.HeaderText = "Campo Confronto"; break; }
                        case "CampoConfrontoMIN": { col.HeaderText = "Valore Minimo"; break; }
                        case "CampoConfrontoMAX": { col.HeaderText = "Valore Massimo"; break; }
                        case "DescCampoCalcolo": { col.HeaderText = "Calcola una % di"; break; }
                        case "TipoCalcolo": { col.HeaderText = "Calcola % o applica valore fisso"; break; }
                        case "Percentuale": { col.HeaderText = "Percentuale"; break; }
                    }
                }
            }
        }

        public bool GetValueDecimal(string Code, string descr, ref decimal value, delegateOnShownEvent delegateOnShown = null)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeNumDecimal, value, false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items, delegateOnShown);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = true;
                        value = (decimal)item.Value;
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public bool GetValueLong(string Code, string descr, ref long value, delegateOnShownEvent delegateOnShown = null)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeNumInt, value, false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items, delegateOnShown);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = long.TryParse(item.Value.ToString(), out value);
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public bool GetValueDate(string Code, string descr, ref DateTime value, delegateOnShownEvent delegateOnShown = null)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeDate, value, false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items, delegateOnShown);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = true;
                        value = (DateTime)item.Value;
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public bool GetValueDateTime(string Code, string descr, ref DateTime value, delegateOnShownEvent delegateOnShown = null)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeDateTime, (value == null ? DateTime.Now : value), false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items, delegateOnShown);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = true;
                        value = (DateTime)item.Value;
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public bool GetMenu(string descr, Dictionary<object, string> items, ref object value, object preferredValue = null, delegateOnShownEvent delegateOnShown = null)
        {
            Dictionary<object, bool> checkedPreferred = null;
            if (preferredValue != null)
            {
                if (items.ContainsKey(preferredValue))
                {
                    checkedPreferred = new Dictionary<object, bool>();
                    checkedPreferred.Add(preferredValue, true);
                }
            }
            value = null;
            if (checkedPreferred != null)
                value = clsUtility.MenuGenerico(descr, items, null, null, false, checkedPreferred, delegateOnShown);
            else
                value = clsUtility.MenuGenerico(descr, items, null, null, false, null, delegateOnShown);
            return value != null;
        }
        public bool GetMenuCheck(string descr, Dictionary<object, string> items, ref object value, Dictionary<object, bool> itemsChecked = null, delegateOnShownEvent delegateOnShown = null)
        {
            value = null;
            value = clsUtility.MenuGenerico(descr, items, null, null, true, itemsChecked, delegateOnShown);
            return value != null;
        }

        public bool GetEventoSpecifico(out libBordero.Bordero.clsSearchSingoloEvento eventoSelezionato)
        {
            eventoSelezionato = null;
            bool result = false;
            clsItemGenericData itemCombo = null;

            List<clsItemGenericData> itemsFilterSearch = new List<clsItemGenericData>();

            itemsFilterSearch.Add(new clsItemGenericData("INIZIO_DATA_EVENTO", "Dalla data", clsItemGenericData.EnumGenericDataType.TypeDate, DateTime.Now.Date, false));
            itemsFilterSearch.Add(new clsItemGenericData("FINE_DATA_EVENTO", "Alla data", clsItemGenericData.EnumGenericDataType.TypeDate, DateTime.Now.Date, false));

            List<clsItemGenericData> codiciLocali = new List<clsItemGenericData>();
            foreach (libBordero.Bordero.clsDescriptorField item in this.Configurazione.DescrCodiciLocali)
            {
                itemCombo = new clsItemGenericData(item.Key, item.Value, clsItemGenericData.EnumGenericDataType.TypeString, item.Key, true);
                codiciLocali.Add(itemCombo);
            }
            itemCombo = new clsItemGenericData("", "tutti", clsItemGenericData.EnumGenericDataType.TypeString, "", true);
            codiciLocali.Add(itemCombo);

            clsItemGenericData searchCodiceLocale = new clsItemGenericData("CODICE_LOCALE", "Codice Locale", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true, "", codiciLocali, ComboBoxStyle.DropDownList);
            itemsFilterSearch.Add(searchCodiceLocale);

            itemsFilterSearch.Add(new clsItemGenericData("TITOLO_EVENTO", "Titolo", clsItemGenericData.EnumGenericDataType.TypeString, "", false));

            frmGetGenericData frmGetDataSearch = new frmGetGenericData("Ricerca un evento", itemsFilterSearch);

            if (frmGetDataSearch.ShowDialog() == DialogResult.OK)
            {
                itemsFilterSearch = frmGetDataSearch.Data;
                libBordero.Bordero.clsSearchEvento search = new libBordero.Bordero.clsSearchEvento();
                foreach (clsItemGenericData item in itemsFilterSearch)
                {
                    if (item.Code == "INIZIO_DATA_EVENTO")
                    {
                        search.FlagDataEvento = true;
                        search.InizioDataEvento = ((DateTime)item.Value).Date;
                    }
                    else if (item.Code == "FINE_DATA_EVENTO")
                    {
                        search.FlagDataEvento = true;
                        search.FineDataEvento = ((DateTime)item.Value).Date;
                    }
                    else if (item.Code == "CODICE_LOCALE")
                    {
                        if (item.Value != null && !string.IsNullOrEmpty(item.Value.ToString()))
                        {
                            search.FlagCodiceLocale = true;
                            search.CodiceLocale = item.Value.ToString();
                        }
                    }
                    else if (item.Code == "TITOLO_EVENTO")
                    {
                        if (item.Value != null && !string.IsNullOrEmpty(item.Value.ToString()))
                        {
                            search.FlagTitolo = true;
                            search.Titolo = item.Value.ToString();
                        }
                    }
                }
                
                try
                {
                    frmWait oFrmWait = new frmWait();
                    oFrmWait.Message = "Ricerca in corso...";
                    oFrmWait.Show();
                    Application.DoEvents();

                    Exception oError = null;
                    List<libBordero.Bordero.clsSearchSingoloEvento> eventi = null;
                    try
                    {
                        if (this.Quota.IsQuotaDEM)
                            search.TipoRicerca = "RicercaEventoDem";
                        else
                            search.TipoRicerca = "RicercaTitoloDem";
                        if (this.ServiceConfig == null)
                        {
                            IConnection conn = null;
                            try
                            {
                                conn = libBordero.Bordero.GetConnection(this.NomeConnessione, out oError);
                                eventi = libBordero.Bordero.RicercaEventi(conn, 1, search, 0);
                            }
                            catch (Exception)
                            {
                            }
                            conn.Close();
                            conn.Dispose();
                        }
                        else
                        {
                            PayLoadRiepiloghi oRequest = null;
                            ResponseRiepiloghi oResponse = null;
                            oRequest = new PayLoadRiepiloghi() { Token = this.ServiceConfig.Token, BorderoSearchEvento = search };
                            oResponse = libBordero.clsBorderoServiceConfigUtility.BorderoSearchEvento(oRequest, this.ServiceConfig.Url);
                            string err = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "RichiestaEventi");
                            if (!string.IsNullOrEmpty(err))
                                oError = new Exception(err);
                            else
                            {
                                eventi = oResponse.Data.RichiestaEventi.Eventi;
                            }
                        }

                    }
                    catch (Exception)
                    {
                    }

                    oFrmWait.Close();
                    oFrmWait.Dispose();

                    if (oError == null && eventi != null && eventi.Count > 0)
                    {
                        libBordero.Bordero.clsSearchSingoloEvento evento = null;
                        frmSearchEvento frmSearchEvento = new frmSearchEvento(eventi);
                        if (frmSearchEvento.ShowDialog() == DialogResult.OK && frmSearchEvento.EventoSelezionato != null)
                        {
                            evento = frmSearchEvento.EventoSelezionato;
                        }
                        frmSearchEvento.Close();
                        frmSearchEvento.Dispose();

                        if (evento != null)
                        {
                            List<clsItemGenericData> itemsFilterSelect = new List<clsItemGenericData>();
                            itemsFilterSelect.Add(new clsItemGenericData("TITLE", "Selezionare i dati necessari alle impostazioni della regola", clsItemGenericData.EnumGenericDataType.TypeString, null, true));

                            List<clsItemGenericData> comboSelezione = null;
                            comboSelezione = new List<clsItemGenericData>();
                            comboSelezione.Add(new clsItemGenericData("DATA_EVENTO", evento.DataOraEvento.Date.ToLongDateString(), clsItemGenericData.EnumGenericDataType.TypeDate, evento.DataOraEvento.Date, false));
                            comboSelezione.Add(new clsItemGenericData("DATA_EVENTO", "NESSUNA DATA", clsItemGenericData.EnumGenericDataType.TypeDate, null, false));
                            itemsFilterSelect.Add(new clsItemGenericData("DATA_EVENTO", "Data evento", clsItemGenericData.EnumGenericDataType.TypeDate, evento.DataOraEvento.Date, false, true, "", comboSelezione, ComboBoxStyle.DropDownList));

                            comboSelezione = new List<clsItemGenericData>();
                            comboSelezione.Add(new clsItemGenericData("DATA_ORA_EVENTO", evento.DataOraEvento.Date.ToLongDateString() + " " + evento.DataOraEvento.ToLongTimeString(), clsItemGenericData.EnumGenericDataType.TypeDateTime, evento.DataOraEvento, false));
                            comboSelezione.Add(new clsItemGenericData("DATA_ORA_EVENTO", "NESSUNA DATA/ORA", clsItemGenericData.EnumGenericDataType.TypeDateTime, null, false));
                            itemsFilterSelect.Add(new clsItemGenericData("DATA_ORA_EVENTO", "Data/Ora evento", clsItemGenericData.EnumGenericDataType.TypeDateTime, evento.DataOraEvento, false, true, "", comboSelezione, ComboBoxStyle.DropDownList));

                            comboSelezione = new List<clsItemGenericData>();
                            comboSelezione.Add(new clsItemGenericData("CODICE_LOCALE", evento.CodiceLocale, clsItemGenericData.EnumGenericDataType.TypeString, evento.CodiceLocale, false));
                            comboSelezione.Add(new clsItemGenericData("CODICE_LOCALE", "NESSUN CODICE LOCALE", clsItemGenericData.EnumGenericDataType.TypeString, null, false));
                            itemsFilterSelect.Add(new clsItemGenericData("CODICE_LOCALE", "Codice Locale", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true, "", comboSelezione, ComboBoxStyle.DropDownList));

                            comboSelezione = new List<clsItemGenericData>();
                            comboSelezione.Add(new clsItemGenericData("TITOLO_EVENTO", evento.TitoloEvento, clsItemGenericData.EnumGenericDataType.TypeString, evento.TitoloEvento, false));
                            comboSelezione.Add(new clsItemGenericData("TITOLO_EVENTO", "NESSUN TITOLO EVENTO", clsItemGenericData.EnumGenericDataType.TypeString, null, false));
                            itemsFilterSelect.Add(new clsItemGenericData("TITOLO_EVENTO", "Titolo Evento", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true, "", comboSelezione, ComboBoxStyle.DropDownList));

                            frmGetDataSearch = new frmGetGenericData("Seleziona dati evento", itemsFilterSelect);

                            if (frmGetDataSearch.ShowDialog() == DialogResult.OK)
                            {
                                itemsFilterSelect = frmGetDataSearch.Data;
                                foreach (clsItemGenericData item in itemsFilterSelect)
                                {
                                    if (item.Code == "DATA_EVENTO" && item.Value != null)
                                    {
                                        if (eventoSelezionato == null)
                                            eventoSelezionato = new libBordero.Bordero.clsSearchSingoloEvento();
                                        eventoSelezionato.DataOraEvento = ((DateTime)item.Value).Date;
                                    }
                                    else if (item.Code == "DATA_ORA_EVENTO" && item.Value != null)
                                    {
                                        if (eventoSelezionato == null)
                                            eventoSelezionato = new libBordero.Bordero.clsSearchSingoloEvento();
                                        eventoSelezionato.DataOraEvento = ((DateTime)item.Value);
                                    }
                                    else if (item.Code == "CODICE_LOCALE" && item.Value != null)
                                    {
                                        if (eventoSelezionato == null)
                                            eventoSelezionato = new libBordero.Bordero.clsSearchSingoloEvento();
                                        eventoSelezionato.CodiceLocale = item.Value.ToString();
                                    }
                                    else if (item.Code == "TITOLO_EVENTO" && item.Value != null)
                                    {
                                        if (eventoSelezionato == null)
                                            eventoSelezionato = new libBordero.Bordero.clsSearchSingoloEvento();
                                        eventoSelezionato.TitoloEvento = item.Value.ToString();
                                    }
                                }
                            }
                            frmGetDataSearch.Close();
                            frmGetDataSearch.Dispose();
                            result = eventoSelezionato != null;

                        }
                    }
                    else
                    {
                        if (oError != null)
                            MessageBox.Show(oError.Message, "Errore");
                    }
                }
                catch (Exception exception)
                {

                }
            }

            return result;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            long numRiga = -1;
            if (this.Quota.IsQuotaDEM)
            {
                libBordero.Bordero.clsBordQuotaRigaDEM riga = new libBordero.Bordero.clsBordQuotaRigaDEM();
                foreach (libBordero.Bordero.clsBordQuotaRigaDEM item in this.Quota.RigheDEM.OrderBy(r => r.Riga))
                {
                    riga.FlagLordoNetto = item.FlagLordoNetto;
                    riga.FlagScontoP10 = item.FlagScontoP10;
                    riga.FlagLordoNetto = item.FlagLordoNetto;
                    riga.PercentualeIncasso = item.PercentualeIncasso;
                    riga.PercentualeOmaggi = item.PercentualeOmaggi;
                    riga.PercentualeSupplemento = item.PercentualeSupplemento;
                    riga.FlgSupplDemEcc = item.FlgSupplDemEcc;
                    riga.ValoreMinimo = item.ValoreMinimo;
                }
                riga.Riga = (this.Quota.RigheDEM != null && this.Quota.RigheDEM.Count > 0 ? this.Quota.RigheDEM.Max(w => w.Riga) + 1 : 1);
                numRiga = riga.Riga;
                this.Quota.RigheDEM.Add(riga);
                this.bindingSource.Add(riga);
            }
            else
            {
                libBordero.Bordero.clsBordQuotaRiga rigaDati = new libBordero.Bordero.clsBordQuotaRiga();
                if (this.Quota.Righe != null && this.Quota.Righe.Count > 0)
                {
                    foreach (libBordero.Bordero.clsBordQuotaRiga item in this.Quota.Righe.OrderBy(r => r.Riga))
                    {
                        rigaDati.CampoCalcolo = item.CampoCalcolo;
                        rigaDati.CampoConfronto = item.CampoConfronto;
                        rigaDati.GiorniSettimana = item.GiorniSettimana;
                        rigaDati.NumeroEventoPerSala = item.NumeroEventoPerSala;
                        rigaDati.TipoCalcolo = item.TipoCalcolo;
                        rigaDati.CampoConfrontoMIN = item.CampoConfrontoMIN;
                        rigaDati.CampoConfrontoMAX = item.CampoConfrontoMAX;
                        rigaDati.Percentuale = item.Percentuale;
                        rigaDati.ValoreFisso = item.ValoreFisso;
                    }
                }
                clsRigaQuotaView riga = new clsRigaQuotaView(rigaDati);
                riga.DescCampoCalcolo = (riga.CampoCalcolo != null && riga.CampoCalcolo != "" ? GetDescCampoToView(riga.CampoCalcolo) : "");
                riga.DescCampoConfronto = (riga.CampoCalcolo != null && riga.CampoConfronto != "" ? GetDescCampoToView(riga.CampoConfronto) : "");
                riga.DescGiorniSettimana = (riga.GiorniSettimana != null && riga.GiorniSettimana != "" ? GetDescSettimana(riga.GiorniSettimana) : "");
                riga.Riga = (this.Quota.Righe != null && this.Quota.Righe.Count > 0 ? this.Quota.Righe.Max(w => w.Riga) + 1 : 1);
                numRiga = riga.Riga;
                this.Quota.Righe.Add(riga);
                this.bindingSource.Add(riga);
            }
            this.AssignColHeaders();
            this.ResizeGrid();
            try
            {
                int rowIndex = -1;
                this.dgvRighe.ClearSelection();
                foreach (DataGridViewRow row in this.dgvRighe.Rows)
                {
                    if (this.Quota.IsQuotaDEM)
                    {
                        libBordero.Bordero.clsBordQuotaRigaDEM rigaDati = (libBordero.Bordero.clsBordQuotaRigaDEM)row.DataBoundItem;
                        if (rigaDati.Riga == numRiga)
                        {
                            rowIndex = row.Index;
                            break;
                        }
                    }
                    else
                    {
                        libBordero.Bordero.clsBordQuotaRiga rigaDati = (libBordero.Bordero.clsBordQuotaRiga)row.DataBoundItem;
                        if (rigaDati.Riga == numRiga)
                        {
                            rowIndex = row.Index;
                            break;
                        }
                    }
                }
                if (rowIndex >= 0)
                {
                    this.dgvRighe.Rows[rowIndex].Cells[0].Selected = true;
                    this.EditCell(rowIndex, 1);
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (this.dgvRighe.SelectedCells.Count > 0)
            {
                int rowIndex = this.dgvRighe.SelectedCells[0].RowIndex;
                if (this.Quota.IsQuotaDEM)
                {
                    libBordero.Bordero.clsBordQuotaRigaDEM riga = (libBordero.Bordero.clsBordQuotaRigaDEM)this.dgvRighe.Rows[rowIndex].DataBoundItem;
                    libBordero.Bordero.clsBordQuotaRigaDEM rigaToDelete = null;
                    foreach (libBordero.Bordero.clsBordQuotaRigaDEM item in this.Quota.RigheDEM)
                    {
                        if (item.Riga == riga.Riga)
                        {
                            rigaToDelete = item;
                            break;
                        }
                    }
                    if (rigaToDelete != null)
                    {
                        this.Quota.RigheDEM.Remove(rigaToDelete);
                        this.bindingSource.Remove(riga);
                        this.dgvRighe.Refresh();
                    }
                }
                else
                {
                    libBordero.Bordero.clsBordQuotaRiga riga = (libBordero.Bordero.clsBordQuotaRiga)this.dgvRighe.Rows[rowIndex].DataBoundItem;
                    libBordero.Bordero.clsBordQuotaRiga rigaToDelete = null;
                    foreach (libBordero.Bordero.clsBordQuotaRiga item in this.Quota.Righe)
                    {
                        if (item.Riga == riga.Riga)
                        {
                            rigaToDelete = item;
                            break;
                        }
                    }
                    if (rigaToDelete != null)
                    {
                        this.Quota.Righe.Remove(rigaToDelete);
                        this.bindingSource.Remove(riga);
                        
                    }
                }
            }
            this.ResizeGrid();
        }
    }
}
