﻿
namespace WinAppBordero
{
    partial class frmMenuBordero
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenuBordero));
            this.btnBordero = new WinAppBordero.clsButtonBase();
            this.btnConfig = new WinAppBordero.clsButtonBase();
            this.btnClose = new WinAppBordero.clsButtonBase();
            this.btnConnessioni = new WinAppBordero.clsButtonBase();
            this.btnGruppi = new WinAppBordero.clsButtonBase();
            this.SuspendLayout();
            // 
            // btnBordero
            // 
            this.btnBordero.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnBordero.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBordero.Image = ((System.Drawing.Image)(resources.GetObject("btnBordero.Image")));
            this.btnBordero.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBordero.Location = new System.Drawing.Point(0, 0);
            this.btnBordero.MaximumSize = new System.Drawing.Size(370, 50);
            this.btnBordero.Name = "btnBordero";
            this.btnBordero.Size = new System.Drawing.Size(370, 50);
            this.btnBordero.TabIndex = 0;
            this.btnBordero.Text = "&Bordero";
            this.btnBordero.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBordero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBordero.UseVisualStyleBackColor = true;
            // 
            // btnConfig
            // 
            this.btnConfig.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConfig.Image = ((System.Drawing.Image)(resources.GetObject("btnConfig.Image")));
            this.btnConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfig.Location = new System.Drawing.Point(0, 50);
            this.btnConfig.MaximumSize = new System.Drawing.Size(370, 50);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(370, 50);
            this.btnConfig.TabIndex = 1;
            this.btnConfig.Text = "&Configurazione";
            this.btnConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(0, 200);
            this.btnClose.MaximumSize = new System.Drawing.Size(370, 50);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(370, 50);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Chiudi";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnConnessioni
            // 
            this.btnConnessioni.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnConnessioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConnessioni.Image = global::WinAppBordero.Properties.Resources.database;
            this.btnConnessioni.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnessioni.Location = new System.Drawing.Point(0, 100);
            this.btnConnessioni.MaximumSize = new System.Drawing.Size(370, 50);
            this.btnConnessioni.Name = "btnConnessioni";
            this.btnConnessioni.Size = new System.Drawing.Size(370, 50);
            this.btnConnessioni.TabIndex = 2;
            this.btnConnessioni.Text = "Co&nnessioni";
            this.btnConnessioni.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnessioni.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConnessioni.UseVisualStyleBackColor = true;
            // 
            // btnGruppi
            // 
            this.btnGruppi.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnGruppi.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGruppi.Image = global::WinAppBordero.Properties.Resources.database;
            this.btnGruppi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGruppi.Location = new System.Drawing.Point(0, 150);
            this.btnGruppi.MaximumSize = new System.Drawing.Size(370, 50);
            this.btnGruppi.Name = "btnGruppi";
            this.btnGruppi.Size = new System.Drawing.Size(370, 50);
            this.btnGruppi.TabIndex = 3;
            this.btnGruppi.Text = "&Gruppi automatici";
            this.btnGruppi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGruppi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGruppi.UseVisualStyleBackColor = true;
            // 
            // frmMenuBordero
            // 
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(370, 252);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnGruppi);
            this.Controls.Add(this.btnConnessioni);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnBordero);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMenuBordero";
            this.Text = "Bordero: Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private clsButtonBase btnBordero;
        private clsButtonBase btnConfig;
        private clsButtonBase btnClose;
        private clsButtonBase btnConnessioni;
        private clsButtonBase btnGruppi;
    }
}
