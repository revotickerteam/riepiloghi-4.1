﻿
namespace WinAppBordero
{
    partial class frmSelezionaConnessione
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelezionaConnessione));
            this.pnlConnections = new System.Windows.Forms.Panel();
            this.lblDefaultConnection = new WinAppBordero.clsLabel();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.clsLabel1 = new WinAppBordero.clsLabel();
            this.pnlSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlConnections
            // 
            this.pnlConnections.AutoScroll = true;
            this.pnlConnections.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConnections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConnections.Location = new System.Drawing.Point(0, 67);
            this.pnlConnections.Name = "pnlConnections";
            this.pnlConnections.Size = new System.Drawing.Size(382, 54);
            this.pnlConnections.TabIndex = 0;
            // 
            // lblDefaultConnection
            // 
            this.lblDefaultConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDefaultConnection.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblDefaultConnection.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDefaultConnection.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefaultConnection.Icon = null;
            this.lblDefaultConnection.ImageIcon = null;
            this.lblDefaultConnection.Location = new System.Drawing.Point(0, 31);
            this.lblDefaultConnection.Name = "lblDefaultConnection";
            this.lblDefaultConnection.Size = new System.Drawing.Size(382, 36);
            this.lblDefaultConnection.TabIndex = 1;
            this.lblDefaultConnection.Text = "Connessione di default SERVERX";
            this.lblDefaultConnection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDefaultConnection.VerticalText = "";
            this.lblDefaultConnection.Visible = false;
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Controls.Add(this.clsLabel1);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Padding = new System.Windows.Forms.Padding(1);
            this.pnlSearch.Size = new System.Drawing.Size(382, 31);
            this.pnlSearch.TabIndex = 2;
            // 
            // txtSearch
            // 
            this.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSearch.Location = new System.Drawing.Point(66, 1);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(313, 27);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TabStop = false;
            // 
            // clsLabel1
            // 
            this.clsLabel1.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.clsLabel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.clsLabel1.Icon = null;
            this.clsLabel1.ImageIcon = null;
            this.clsLabel1.Location = new System.Drawing.Point(1, 1);
            this.clsLabel1.Name = "clsLabel1";
            this.clsLabel1.Size = new System.Drawing.Size(65, 27);
            this.clsLabel1.TabIndex = 2;
            this.clsLabel1.Text = "Ricerca:";
            this.clsLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.clsLabel1.VerticalText = "";
            // 
            // frmSelezionaConnessione
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(382, 121);
            this.Controls.Add(this.pnlConnections);
            this.Controls.Add(this.lblDefaultConnection);
            this.Controls.Add(this.pnlSearch);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 600);
            this.MinimizeBox = false;
            this.Name = "frmSelezionaConnessione";
            this.ShowInTaskbar = false;
            this.Text = "Bordero: Connessione";
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlConnections;
        private clsLabel lblDefaultConnection;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private clsLabel clsLabel1;
    }
}
