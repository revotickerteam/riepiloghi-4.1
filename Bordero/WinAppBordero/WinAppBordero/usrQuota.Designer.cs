﻿
namespace WinAppBordero
{
    partial class usrQuota
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usrQuota));
            this.lblDescr = new WinAppBordero.clsLabel();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.pnlDescr = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.pnlSI = new System.Windows.Forms.Panel();
            this.radioSI_I = new System.Windows.Forms.RadioButton();
            this.radioSI_S = new System.Windows.Forms.RadioButton();
            this.radioSI_ALL = new System.Windows.Forms.RadioButton();
            this.pnlTE = new System.Windows.Forms.Panel();
            this.chkLstTE = new WinAppBordero.usrQuota.clsFlowLayoutPanelLista();
            this.pnlTopTE = new System.Windows.Forms.Panel();
            this.btnExpandCollapse = new System.Windows.Forms.Button();
            this.imageListOpenClose = new System.Windows.Forms.ImageList(this.components);
            this.radioTE_SPECIFICI = new System.Windows.Forms.RadioButton();
            this.radioTE_ALL = new System.Windows.Forms.RadioButton();
            this.lblIndex = new WinAppBordero.clsLabel();
            this.usrRigheQuote = new WinAppBordero.usrRigheQuote();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlDescr.SuspendLayout();
            this.pnlSI.SuspendLayout();
            this.pnlTE.SuspendLayout();
            this.pnlTopTE.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDescr
            // 
            this.lblDescr.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblDescr.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDescr.Icon = null;
            this.lblDescr.ImageIcon = null;
            this.lblDescr.Location = new System.Drawing.Point(3, 3);
            this.lblDescr.Name = "lblDescr";
            this.lblDescr.Size = new System.Drawing.Size(88, 31);
            this.lblDescr.TabIndex = 0;
            this.lblDescr.Text = "Descrizione:";
            this.lblDescr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDescr.VerticalText = "";
            // 
            // txtDescr
            // 
            this.txtDescr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescr.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(91, 3);
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(446, 33);
            this.txtDescr.TabIndex = 1;
            // 
            // pnlDescr
            // 
            this.pnlDescr.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDescr.Controls.Add(this.txtDescr);
            this.pnlDescr.Controls.Add(this.btnImport);
            this.pnlDescr.Controls.Add(this.btnExport);
            this.pnlDescr.Controls.Add(this.lblDescr);
            this.pnlDescr.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDescr.Location = new System.Drawing.Point(44, 0);
            this.pnlDescr.Name = "pnlDescr";
            this.pnlDescr.Padding = new System.Windows.Forms.Padding(3);
            this.pnlDescr.Size = new System.Drawing.Size(616, 39);
            this.pnlDescr.TabIndex = 2;
            // 
            // btnImport
            // 
            this.btnImport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.Location = new System.Drawing.Point(537, 3);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(37, 31);
            this.btnImport.TabIndex = 4;
            this.toolTip1.SetToolTip(this.btnImport, "Importa");
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.Location = new System.Drawing.Point(574, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(37, 31);
            this.btnExport.TabIndex = 3;
            this.toolTip1.SetToolTip(this.btnExport, "Esporta");
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // pnlSI
            // 
            this.pnlSI.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlSI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSI.Controls.Add(this.radioSI_I);
            this.pnlSI.Controls.Add(this.radioSI_S);
            this.pnlSI.Controls.Add(this.radioSI_ALL);
            this.pnlSI.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSI.Location = new System.Drawing.Point(44, 39);
            this.pnlSI.Name = "pnlSI";
            this.pnlSI.Padding = new System.Windows.Forms.Padding(4, 1, 1, 1);
            this.pnlSI.Size = new System.Drawing.Size(616, 28);
            this.pnlSI.TabIndex = 3;
            // 
            // radioSI_I
            // 
            this.radioSI_I.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioSI_I.Location = new System.Drawing.Point(437, 1);
            this.radioSI_I.Name = "radioSI_I";
            this.radioSI_I.Size = new System.Drawing.Size(176, 24);
            this.radioSI_I.TabIndex = 2;
            this.radioSI_I.Text = "solo per Intrattenimento";
            this.radioSI_I.UseVisualStyleBackColor = true;
            // 
            // radioSI_S
            // 
            this.radioSI_S.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioSI_S.Location = new System.Drawing.Point(284, 1);
            this.radioSI_S.Name = "radioSI_S";
            this.radioSI_S.Size = new System.Drawing.Size(153, 24);
            this.radioSI_S.TabIndex = 1;
            this.radioSI_S.Text = "solo per Spettacolo";
            this.radioSI_S.UseVisualStyleBackColor = true;
            // 
            // radioSI_ALL
            // 
            this.radioSI_ALL.Checked = true;
            this.radioSI_ALL.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioSI_ALL.Location = new System.Drawing.Point(4, 1);
            this.radioSI_ALL.Name = "radioSI_ALL";
            this.radioSI_ALL.Size = new System.Drawing.Size(280, 24);
            this.radioSI_ALL.TabIndex = 0;
            this.radioSI_ALL.TabStop = true;
            this.radioSI_ALL.Text = "Sia per Spettacolo che Intrattenimento";
            this.radioSI_ALL.UseVisualStyleBackColor = true;
            // 
            // pnlTE
            // 
            this.pnlTE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTE.Controls.Add(this.chkLstTE);
            this.pnlTE.Controls.Add(this.pnlTopTE);
            this.pnlTE.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTE.Location = new System.Drawing.Point(44, 67);
            this.pnlTE.Name = "pnlTE";
            this.pnlTE.Padding = new System.Windows.Forms.Padding(3);
            this.pnlTE.Size = new System.Drawing.Size(616, 84);
            this.pnlTE.TabIndex = 5;
            // 
            // chkLstTE
            // 
            this.chkLstTE.AutoScroll = true;
            this.chkLstTE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkLstTE.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.chkLstTE.ForeColor = System.Drawing.Color.Black;
            this.chkLstTE.Location = new System.Drawing.Point(3, 31);
            this.chkLstTE.MinimumSize = new System.Drawing.Size(0, 50);
            this.chkLstTE.Name = "chkLstTE";
            this.chkLstTE.Size = new System.Drawing.Size(608, 50);
            this.chkLstTE.TabIndex = 6;
            // 
            // pnlTopTE
            // 
            this.pnlTopTE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlTopTE.Controls.Add(this.btnExpandCollapse);
            this.pnlTopTE.Controls.Add(this.radioTE_SPECIFICI);
            this.pnlTopTE.Controls.Add(this.radioTE_ALL);
            this.pnlTopTE.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopTE.Location = new System.Drawing.Point(3, 3);
            this.pnlTopTE.Name = "pnlTopTE";
            this.pnlTopTE.Padding = new System.Windows.Forms.Padding(1);
            this.pnlTopTE.Size = new System.Drawing.Size(608, 28);
            this.pnlTopTE.TabIndex = 5;
            // 
            // btnExpandCollapse
            // 
            this.btnExpandCollapse.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExpandCollapse.ImageIndex = 0;
            this.btnExpandCollapse.ImageList = this.imageListOpenClose;
            this.btnExpandCollapse.Location = new System.Drawing.Point(506, 1);
            this.btnExpandCollapse.Name = "btnExpandCollapse";
            this.btnExpandCollapse.Size = new System.Drawing.Size(101, 26);
            this.btnExpandCollapse.TabIndex = 2;
            this.btnExpandCollapse.Text = "Tipi Evento";
            this.btnExpandCollapse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnExpandCollapse, "Espandi/Riduci Tipi evento");
            this.btnExpandCollapse.UseVisualStyleBackColor = true;
            this.btnExpandCollapse.Click += new System.EventHandler(this.btnExpandCollapse_Click);
            // 
            // imageListOpenClose
            // 
            this.imageListOpenClose.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListOpenClose.ImageStream")));
            this.imageListOpenClose.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListOpenClose.Images.SetKeyName(0, "arrowDn.png");
            this.imageListOpenClose.Images.SetKeyName(1, "arrowUp.png");
            // 
            // radioTE_SPECIFICI
            // 
            this.radioTE_SPECIFICI.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioTE_SPECIFICI.Location = new System.Drawing.Point(177, 1);
            this.radioTE_SPECIFICI.Name = "radioTE_SPECIFICI";
            this.radioTE_SPECIFICI.Size = new System.Drawing.Size(185, 26);
            this.radioTE_SPECIFICI.TabIndex = 1;
            this.radioTE_SPECIFICI.Text = "solo Alcuni Tipi Evento";
            this.radioTE_SPECIFICI.UseVisualStyleBackColor = true;
            // 
            // radioTE_ALL
            // 
            this.radioTE_ALL.Checked = true;
            this.radioTE_ALL.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioTE_ALL.Location = new System.Drawing.Point(1, 1);
            this.radioTE_ALL.Name = "radioTE_ALL";
            this.radioTE_ALL.Size = new System.Drawing.Size(176, 26);
            this.radioTE_ALL.TabIndex = 0;
            this.radioTE_ALL.TabStop = true;
            this.radioTE_ALL.Text = "per Tutti i Tipi Evento";
            this.radioTE_ALL.UseVisualStyleBackColor = true;
            // 
            // lblIndex
            // 
            this.lblIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIndex.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblIndex.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndex.Icon = null;
            this.lblIndex.ImageIcon = null;
            this.lblIndex.Location = new System.Drawing.Point(0, 0);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(44, 457);
            this.lblIndex.TabIndex = 8;
            this.lblIndex.Text = "0";
            this.lblIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblIndex.VerticalText = "";
            // 
            // usrRigheQuote
            // 
            this.usrRigheQuote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.usrRigheQuote.Configurazione = null;
            this.usrRigheQuote.Dock = System.Windows.Forms.DockStyle.Top;
            this.usrRigheQuote.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usrRigheQuote.Location = new System.Drawing.Point(44, 151);
            this.usrRigheQuote.Name = "usrRigheQuote";
            this.usrRigheQuote.NomeConnessione = null;
            this.usrRigheQuote.Quota = null;
            this.usrRigheQuote.Size = new System.Drawing.Size(616, 250);
            this.usrRigheQuote.TabIndex = 7;
            this.usrRigheQuote.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // usrQuota
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.usrRigheQuote);
            this.Controls.Add(this.pnlTE);
            this.Controls.Add(this.pnlSI);
            this.Controls.Add(this.pnlDescr);
            this.Controls.Add(this.lblIndex);
            this.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "usrQuota";
            this.Size = new System.Drawing.Size(660, 457);
            this.pnlDescr.ResumeLayout(false);
            this.pnlDescr.PerformLayout();
            this.pnlSI.ResumeLayout(false);
            this.pnlTE.ResumeLayout(false);
            this.pnlTopTE.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private clsLabel lblDescr;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.Panel pnlDescr;
        private System.Windows.Forms.Panel pnlSI;
        private System.Windows.Forms.RadioButton radioSI_I;
        private System.Windows.Forms.RadioButton radioSI_S;
        private System.Windows.Forms.RadioButton radioSI_ALL;
        private System.Windows.Forms.Panel pnlTE;
        private clsFlowLayoutPanelLista chkLstTE;
        private System.Windows.Forms.Panel pnlTopTE;
        private System.Windows.Forms.RadioButton radioTE_SPECIFICI;
        private System.Windows.Forms.RadioButton radioTE_ALL;
        private usrRigheQuote usrRigheQuote;
        private clsLabel lblIndex;
        private System.Windows.Forms.Button btnExpandCollapse;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ImageList imageListOpenClose;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnImport;
    }
}
