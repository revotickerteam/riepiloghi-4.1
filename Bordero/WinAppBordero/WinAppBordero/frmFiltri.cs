﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class frmFiltri : WinAppBordero.frmBorderoBase
    {
        public frmFiltri(DataTable table, string filterKey, bool canAbort)
        {
            InitializeComponent();
            this.Text = "Filtri";
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.btnOK.Click += BtnOK_Click;
            this.btnAbort.Click += BtnAbort_Click;
            this.btnAbort.Enabled = canAbort;
            this.lblTitle.Text = "Seleziona una o più righe: " + frmBordero.GetFilterKeyDescr(filterKey);
            this.dgv.DataSource = table;
            foreach (DataGridViewColumn col in this.dgv.Columns)
            {
                if (col.Index == 0)
                    col.Visible = false;
            }
        }

        private void BtnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        public List<DataRow> SelectedRows()
        {
            List<DataRow> result = new List<DataRow>();
            foreach (DataGridViewRow row in this.dgv.SelectedRows)
            {
                DataRow dataRow = ((DataRowView)row.DataBoundItem).Row;
                result.Add(dataRow);
            }
            return result;
        }
    }
}
