﻿
namespace WinAppBordero
{
    partial class frmBordero
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBordero));
            this.pnlPeriodo = new System.Windows.Forms.Panel();
            this.panelPeriodo = new System.Windows.Forms.Panel();
            this.panelAL = new System.Windows.Forms.Panel();
            this.lblAl = new System.Windows.Forms.Label();
            this.dtFINE = new System.Windows.Forms.DateTimePicker();
            this.panelDAL = new System.Windows.Forms.Panel();
            this.lblDal = new System.Windows.Forms.Label();
            this.dtINIZIO = new System.Windows.Forms.DateTimePicker();
            this.panelGiornoPeriodo = new System.Windows.Forms.Panel();
            this.radioPERIODO = new System.Windows.Forms.RadioButton();
            this.radioGIORNO = new System.Windows.Forms.RadioButton();
            this.btnAbort = new WinAppBordero.clsButtonBase();
            this.btnReadData = new WinAppBordero.clsButtonBase();
            this.pnlFILTRI = new System.Windows.Forms.Panel();
            this.pnlFilterTags = new System.Windows.Forms.FlowLayoutPanel();
            this.chkFiltri = new WinAppBordero.clsCheckBox();
            this.pnlMULTI = new System.Windows.Forms.Panel();
            this.pnlMultiTags = new System.Windows.Forms.FlowLayoutPanel();
            this.panelCheckGruppi = new System.Windows.Forms.Panel();
            this.chkMulti = new WinAppBordero.clsCheckBox();
            this.btnGruppi = new WinAppBordero.clsButtonBase();
            this.pnlQuoteOpzionali = new System.Windows.Forms.Panel();
            this.chkListOpzionali = new System.Windows.Forms.ListView();
            this.pnlTeSpetIntra = new System.Windows.Forms.Panel();
            this.chkSI = new WinAppBordero.clsCheckBox();
            this.chkTe = new WinAppBordero.clsCheckBox();
            this.lblVincoliTeSi = new WinAppBordero.clsLabel();
            this.chkOpzionali = new WinAppBordero.clsCheckBox();
            this.pnlPeriodo.SuspendLayout();
            this.panelPeriodo.SuspendLayout();
            this.panelAL.SuspendLayout();
            this.panelDAL.SuspendLayout();
            this.panelGiornoPeriodo.SuspendLayout();
            this.pnlFILTRI.SuspendLayout();
            this.pnlMULTI.SuspendLayout();
            this.panelCheckGruppi.SuspendLayout();
            this.pnlQuoteOpzionali.SuspendLayout();
            this.pnlTeSpetIntra.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPeriodo
            // 
            this.pnlPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPeriodo.Controls.Add(this.panelPeriodo);
            this.pnlPeriodo.Controls.Add(this.btnAbort);
            this.pnlPeriodo.Controls.Add(this.btnReadData);
            this.pnlPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPeriodo.Location = new System.Drawing.Point(0, 0);
            this.pnlPeriodo.Name = "pnlPeriodo";
            this.pnlPeriodo.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPeriodo.Size = new System.Drawing.Size(701, 79);
            this.pnlPeriodo.TabIndex = 0;
            // 
            // panelPeriodo
            // 
            this.panelPeriodo.Controls.Add(this.panelAL);
            this.panelPeriodo.Controls.Add(this.panelDAL);
            this.panelPeriodo.Controls.Add(this.panelGiornoPeriodo);
            this.panelPeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPeriodo.Location = new System.Drawing.Point(114, 5);
            this.panelPeriodo.Name = "panelPeriodo";
            this.panelPeriodo.Size = new System.Drawing.Size(459, 67);
            this.panelPeriodo.TabIndex = 3;
            // 
            // panelAL
            // 
            this.panelAL.Controls.Add(this.lblAl);
            this.panelAL.Controls.Add(this.dtFINE);
            this.panelAL.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAL.Location = new System.Drawing.Point(109, 33);
            this.panelAL.Name = "panelAL";
            this.panelAL.Padding = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.panelAL.Size = new System.Drawing.Size(350, 33);
            this.panelAL.TabIndex = 2;
            // 
            // lblAl
            // 
            this.lblAl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAl.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAl.Location = new System.Drawing.Point(10, 2);
            this.lblAl.Name = "lblAl";
            this.lblAl.Size = new System.Drawing.Size(51, 29);
            this.lblAl.TabIndex = 0;
            this.lblAl.Text = "Al:";
            this.lblAl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtFINE
            // 
            this.dtFINE.Dock = System.Windows.Forms.DockStyle.Right;
            this.dtFINE.Location = new System.Drawing.Point(61, 2);
            this.dtFINE.Name = "dtFINE";
            this.dtFINE.Size = new System.Drawing.Size(279, 27);
            this.dtFINE.TabIndex = 1;
            // 
            // panelDAL
            // 
            this.panelDAL.Controls.Add(this.lblDal);
            this.panelDAL.Controls.Add(this.dtINIZIO);
            this.panelDAL.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDAL.Location = new System.Drawing.Point(109, 0);
            this.panelDAL.Name = "panelDAL";
            this.panelDAL.Padding = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.panelDAL.Size = new System.Drawing.Size(350, 33);
            this.panelDAL.TabIndex = 1;
            // 
            // lblDal
            // 
            this.lblDal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDal.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDal.Location = new System.Drawing.Point(10, 2);
            this.lblDal.Name = "lblDal";
            this.lblDal.Size = new System.Drawing.Size(51, 29);
            this.lblDal.TabIndex = 0;
            this.lblDal.Text = "Dal:";
            this.lblDal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtINIZIO
            // 
            this.dtINIZIO.Dock = System.Windows.Forms.DockStyle.Right;
            this.dtINIZIO.Location = new System.Drawing.Point(61, 2);
            this.dtINIZIO.Name = "dtINIZIO";
            this.dtINIZIO.Size = new System.Drawing.Size(279, 27);
            this.dtINIZIO.TabIndex = 1;
            // 
            // panelGiornoPeriodo
            // 
            this.panelGiornoPeriodo.Controls.Add(this.radioPERIODO);
            this.panelGiornoPeriodo.Controls.Add(this.radioGIORNO);
            this.panelGiornoPeriodo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelGiornoPeriodo.Location = new System.Drawing.Point(0, 0);
            this.panelGiornoPeriodo.Name = "panelGiornoPeriodo";
            this.panelGiornoPeriodo.Padding = new System.Windows.Forms.Padding(4, 2, 2, 2);
            this.panelGiornoPeriodo.Size = new System.Drawing.Size(109, 67);
            this.panelGiornoPeriodo.TabIndex = 0;
            // 
            // radioPERIODO
            // 
            this.radioPERIODO.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioPERIODO.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioPERIODO.Location = new System.Drawing.Point(4, 31);
            this.radioPERIODO.Name = "radioPERIODO";
            this.radioPERIODO.Size = new System.Drawing.Size(103, 31);
            this.radioPERIODO.TabIndex = 1;
            this.radioPERIODO.Text = "Per periodo";
            this.radioPERIODO.UseVisualStyleBackColor = true;
            // 
            // radioGIORNO
            // 
            this.radioGIORNO.Checked = true;
            this.radioGIORNO.Dock = System.Windows.Forms.DockStyle.Top;
            this.radioGIORNO.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioGIORNO.Location = new System.Drawing.Point(4, 2);
            this.radioGIORNO.Name = "radioGIORNO";
            this.radioGIORNO.Size = new System.Drawing.Size(103, 29);
            this.radioGIORNO.TabIndex = 0;
            this.radioGIORNO.TabStop = true;
            this.radioGIORNO.Text = "Un Giorno";
            this.radioGIORNO.UseVisualStyleBackColor = true;
            // 
            // btnAbort
            // 
            this.btnAbort.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnAbort.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Image = ((System.Drawing.Image)(resources.GetObject("btnAbort.Image")));
            this.btnAbort.Location = new System.Drawing.Point(5, 5);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(109, 67);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Chiudi";
            this.btnAbort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAbort.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnReadData
            // 
            this.btnReadData.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnReadData.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnReadData.Image = ((System.Drawing.Image)(resources.GetObject("btnReadData.Image")));
            this.btnReadData.Location = new System.Drawing.Point(573, 5);
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.Size = new System.Drawing.Size(121, 67);
            this.btnReadData.TabIndex = 1;
            this.btnReadData.Text = "Elabora";
            this.btnReadData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReadData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReadData.UseVisualStyleBackColor = true;
            // 
            // pnlFILTRI
            // 
            this.pnlFILTRI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFILTRI.Controls.Add(this.pnlFilterTags);
            this.pnlFILTRI.Controls.Add(this.chkFiltri);
            this.pnlFILTRI.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFILTRI.Location = new System.Drawing.Point(0, 329);
            this.pnlFILTRI.Name = "pnlFILTRI";
            this.pnlFILTRI.Padding = new System.Windows.Forms.Padding(3);
            this.pnlFILTRI.Size = new System.Drawing.Size(701, 122);
            this.pnlFILTRI.TabIndex = 1;
            // 
            // pnlFilterTags
            // 
            this.pnlFilterTags.AutoSize = true;
            this.pnlFilterTags.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlFilterTags.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilterTags.Enabled = false;
            this.pnlFilterTags.Location = new System.Drawing.Point(3, 26);
            this.pnlFilterTags.Name = "pnlFilterTags";
            this.pnlFilterTags.Padding = new System.Windows.Forms.Padding(5);
            this.pnlFilterTags.Size = new System.Drawing.Size(693, 10);
            this.pnlFilterTags.TabIndex = 1;
            // 
            // chkFiltri
            // 
            this.chkFiltri.AutoSize = true;
            this.chkFiltri.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkFiltri.Location = new System.Drawing.Point(3, 3);
            this.chkFiltri.Name = "chkFiltri";
            this.chkFiltri.Size = new System.Drawing.Size(693, 23);
            this.chkFiltri.TabIndex = 0;
            this.chkFiltri.Text = "Filtri:";
            this.chkFiltri.UseVisualStyleBackColor = true;
            // 
            // pnlMULTI
            // 
            this.pnlMULTI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMULTI.Controls.Add(this.pnlMultiTags);
            this.pnlMULTI.Controls.Add(this.panelCheckGruppi);
            this.pnlMULTI.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMULTI.Location = new System.Drawing.Point(0, 207);
            this.pnlMULTI.Name = "pnlMULTI";
            this.pnlMULTI.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMULTI.Size = new System.Drawing.Size(701, 122);
            this.pnlMULTI.TabIndex = 2;
            // 
            // pnlMultiTags
            // 
            this.pnlMultiTags.AutoSize = true;
            this.pnlMultiTags.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlMultiTags.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMultiTags.Enabled = false;
            this.pnlMultiTags.Location = new System.Drawing.Point(3, 33);
            this.pnlMultiTags.Name = "pnlMultiTags";
            this.pnlMultiTags.Padding = new System.Windows.Forms.Padding(5);
            this.pnlMultiTags.Size = new System.Drawing.Size(693, 10);
            this.pnlMultiTags.TabIndex = 1;
            // 
            // panelCheckGruppi
            // 
            this.panelCheckGruppi.Controls.Add(this.chkMulti);
            this.panelCheckGruppi.Controls.Add(this.btnGruppi);
            this.panelCheckGruppi.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCheckGruppi.Location = new System.Drawing.Point(3, 3);
            this.panelCheckGruppi.Name = "panelCheckGruppi";
            this.panelCheckGruppi.Padding = new System.Windows.Forms.Padding(1, 1, 3, 1);
            this.panelCheckGruppi.Size = new System.Drawing.Size(693, 30);
            this.panelCheckGruppi.TabIndex = 2;
            // 
            // chkMulti
            // 
            this.chkMulti.AutoSize = true;
            this.chkMulti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkMulti.Location = new System.Drawing.Point(1, 1);
            this.chkMulti.Name = "chkMulti";
            this.chkMulti.Size = new System.Drawing.Size(328, 28);
            this.chkMulti.TabIndex = 1;
            this.chkMulti.Text = "Suddividere Bordero per:";
            this.chkMulti.UseVisualStyleBackColor = true;
            // 
            // btnGruppi
            // 
            this.btnGruppi.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnGruppi.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnGruppi.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGruppi.Location = new System.Drawing.Point(329, 1);
            this.btnGruppi.Name = "btnGruppi";
            this.btnGruppi.Size = new System.Drawing.Size(361, 28);
            this.btnGruppi.TabIndex = 2;
            this.btnGruppi.Text = "Gruppi Automatici";
            this.btnGruppi.UseVisualStyleBackColor = true;
            // 
            // pnlQuoteOpzionali
            // 
            this.pnlQuoteOpzionali.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlQuoteOpzionali.Controls.Add(this.chkListOpzionali);
            this.pnlQuoteOpzionali.Controls.Add(this.pnlTeSpetIntra);
            this.pnlQuoteOpzionali.Controls.Add(this.chkOpzionali);
            this.pnlQuoteOpzionali.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlQuoteOpzionali.Location = new System.Drawing.Point(0, 79);
            this.pnlQuoteOpzionali.Name = "pnlQuoteOpzionali";
            this.pnlQuoteOpzionali.Padding = new System.Windows.Forms.Padding(3);
            this.pnlQuoteOpzionali.Size = new System.Drawing.Size(701, 128);
            this.pnlQuoteOpzionali.TabIndex = 3;
            // 
            // chkListOpzionali
            // 
            this.chkListOpzionali.CheckBoxes = true;
            this.chkListOpzionali.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkListOpzionali.HideSelection = false;
            this.chkListOpzionali.Location = new System.Drawing.Point(3, 58);
            this.chkListOpzionali.Name = "chkListOpzionali";
            this.chkListOpzionali.ShowGroups = false;
            this.chkListOpzionali.Size = new System.Drawing.Size(693, 65);
            this.chkListOpzionali.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.chkListOpzionali.TabIndex = 1;
            this.chkListOpzionali.UseCompatibleStateImageBehavior = false;
            this.chkListOpzionali.View = System.Windows.Forms.View.Details;
            // 
            // pnlTeSpetIntra
            // 
            this.pnlTeSpetIntra.Controls.Add(this.chkSI);
            this.pnlTeSpetIntra.Controls.Add(this.chkTe);
            this.pnlTeSpetIntra.Controls.Add(this.lblVincoliTeSi);
            this.pnlTeSpetIntra.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTeSpetIntra.Location = new System.Drawing.Point(3, 28);
            this.pnlTeSpetIntra.Name = "pnlTeSpetIntra";
            this.pnlTeSpetIntra.Padding = new System.Windows.Forms.Padding(3);
            this.pnlTeSpetIntra.Size = new System.Drawing.Size(693, 30);
            this.pnlTeSpetIntra.TabIndex = 3;
            // 
            // chkSI
            // 
            this.chkSI.AutoSize = true;
            this.chkSI.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkSI.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSI.Location = new System.Drawing.Point(248, 3);
            this.chkSI.Name = "chkSI";
            this.chkSI.Size = new System.Drawing.Size(201, 24);
            this.chkSI.TabIndex = 5;
            this.chkSI.Text = "Spettacolo Intrattenimento";
            this.chkSI.UseVisualStyleBackColor = true;
            // 
            // chkTe
            // 
            this.chkTe.AutoSize = true;
            this.chkTe.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkTe.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTe.Location = new System.Drawing.Point(144, 3);
            this.chkTe.Name = "chkTe";
            this.chkTe.Size = new System.Drawing.Size(104, 24);
            this.chkTe.TabIndex = 3;
            this.chkTe.Text = "Tipo evento";
            this.chkTe.UseVisualStyleBackColor = true;
            // 
            // lblVincoliTeSi
            // 
            this.lblVincoliTeSi.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblVincoliTeSi.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblVincoliTeSi.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVincoliTeSi.Icon = null;
            this.lblVincoliTeSi.ImageIcon = null;
            this.lblVincoliTeSi.Location = new System.Drawing.Point(3, 3);
            this.lblVincoliTeSi.Name = "lblVincoliTeSi";
            this.lblVincoliTeSi.Size = new System.Drawing.Size(141, 24);
            this.lblVincoliTeSi.TabIndex = 4;
            this.lblVincoliTeSi.Text = "Mantenere i vincoli:";
            this.lblVincoliTeSi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblVincoliTeSi.VerticalText = "";
            // 
            // chkOpzionali
            // 
            this.chkOpzionali.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkOpzionali.Location = new System.Drawing.Point(3, 3);
            this.chkOpzionali.Name = "chkOpzionali";
            this.chkOpzionali.Size = new System.Drawing.Size(693, 25);
            this.chkOpzionali.TabIndex = 2;
            this.chkOpzionali.Text = "Quote manuali";
            this.chkOpzionali.UseVisualStyleBackColor = true;
            // 
            // frmBordero
            // 
            this.AcceptButton = this.btnReadData;
            this.CancelButton = this.btnAbort;
            this.ClientSize = new System.Drawing.Size(701, 451);
            this.Controls.Add(this.pnlFILTRI);
            this.Controls.Add(this.pnlMULTI);
            this.Controls.Add(this.pnlQuoteOpzionali);
            this.Controls.Add(this.pnlPeriodo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBordero";
            this.ShowInTaskbar = false;
            this.Text = "Bordero";
            this.pnlPeriodo.ResumeLayout(false);
            this.panelPeriodo.ResumeLayout(false);
            this.panelAL.ResumeLayout(false);
            this.panelDAL.ResumeLayout(false);
            this.panelGiornoPeriodo.ResumeLayout(false);
            this.pnlFILTRI.ResumeLayout(false);
            this.pnlFILTRI.PerformLayout();
            this.pnlMULTI.ResumeLayout(false);
            this.pnlMULTI.PerformLayout();
            this.panelCheckGruppi.ResumeLayout(false);
            this.panelCheckGruppi.PerformLayout();
            this.pnlQuoteOpzionali.ResumeLayout(false);
            this.pnlTeSpetIntra.ResumeLayout(false);
            this.pnlTeSpetIntra.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPeriodo;
        private System.Windows.Forms.Label lblAl;
        private System.Windows.Forms.Label lblDal;
        private System.Windows.Forms.DateTimePicker dtFINE;
        private System.Windows.Forms.DateTimePicker dtINIZIO;
        private System.Windows.Forms.Panel pnlFILTRI;
        private clsButtonBase btnReadData;
        private System.Windows.Forms.Panel pnlMULTI;
        private clsCheckBox chkFiltri;
        private System.Windows.Forms.FlowLayoutPanel pnlFilterTags;
        private System.Windows.Forms.Panel panelPeriodo;
        private System.Windows.Forms.Panel panelAL;
        private System.Windows.Forms.Panel panelDAL;
        private clsButtonBase btnAbort;
        private System.Windows.Forms.Panel panelGiornoPeriodo;
        private System.Windows.Forms.RadioButton radioPERIODO;
        private System.Windows.Forms.RadioButton radioGIORNO;
        private System.Windows.Forms.FlowLayoutPanel pnlMultiTags;
        private System.Windows.Forms.Panel panelCheckGruppi;
        private clsCheckBox chkMulti;
        private clsButtonBase btnGruppi;
        private System.Windows.Forms.Panel pnlQuoteOpzionali;
        private System.Windows.Forms.ListView chkListOpzionali;
        private clsCheckBox chkOpzionali;
        private System.Windows.Forms.Panel pnlTeSpetIntra;
        private clsCheckBox chkSI;
        private clsCheckBox chkTe;
        private clsLabel lblVincoliTeSi;
    }
}
