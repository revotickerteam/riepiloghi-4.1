﻿using libBordero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public partial class frmMenuBordero : WinAppBordero.frmBorderoBase
    {
        public bool Restart = false;
        private string NomeConnessione { get; set; }
        private string DescConnessione { get; set; }

        private BorderoFileConfig BorderoFileConfig { get; set; }

        private libBordero.clsBorderoServiceConfig ServiceConfig { get; set; }

        public frmMenuBordero(string nomeConnessione, string descConnessione, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.NomeConnessione = nomeConnessione;
            this.DescConnessione = descConnessione;
            this.ServiceConfig = serviceConfig;
            if (!string.IsNullOrEmpty(this.DescConnessione) && !string.IsNullOrWhiteSpace(this.DescConnessione))
                this.Text += " " + this.DescConnessione;
            this.BorderoFileConfig = WinAppBordero.BorderoFileConfig.GetBorderoFileConfigFromFile();

            this.btnBordero.Click += BtnBordero_Click;

            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("CONFIG"))
                this.btnConfig.Click += BtnConfig_Click;
            else
                this.btnConfig.Enabled = false;

            this.btnClose.Click += BtnClose_Click;

            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("CONNESSIONI"))
                this.btnConnessioni.Click += BtnConnessioni_Click;
            else
                this.btnConnessioni.Enabled = false;

            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("GRUPPI"))
                this.btnGruppi.Click += BtnGruppi_Click;
            else
                this.btnGruppi.Enabled = false;

            if (this.ServiceConfig != null)
                this.btnConnessioni.Enabled = false;

            this.btnConfig.Visible = this.btnConfig.Enabled;
            this.btnConnessioni.Visible = this.btnConnessioni.Enabled;
            this.btnGruppi.Visible = this.btnGruppi.Enabled;

            this.ClientSizeChanged += FrmMenuBordero_ClientSizeChanged;
            this.Shown += FrmMenuBordero_Shown;
            this.HelpButtonClicked += Bordero_HelpButtonClicked;
        }

        private void Bordero_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            clsBorderoHelp.ShowHelp("");
        }

        private void FrmMenuBordero_Shown(object sender, EventArgs e)
        {
            this.ResizeButtons();
            int height = 0;
            foreach (clsButtonBase btn in this.Controls)
            {
                if (btn.Visible)
                    height += btn.Height;
            }
            this.ClientSize = new Size(this.ClientSize.Width, height);
            this.CenterToScreen();
        }

        private void FrmMenuBordero_ClientSizeChanged(object sender, EventArgs e)
        {
            this.ResizeButtons();
        }

        private void ResizeButtons()
        {
            int count = 0;
            foreach (clsButtonBase btn in this.Controls)
            {
                if (btn.Visible)
                    count += 1;
            }
            foreach (clsButtonBase btn in this.Controls)
            {
                btn.Height = (this.ClientSize.Height - this.Padding.Vertical) / count;
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void BtnConfig_Click(object sender, EventArgs e)
        {
            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("CONFIG"))
            {
                frmConfigurazione frmConfigurazione = new frmConfigurazione(this.NomeConnessione, this.DescConnessione, this.ServiceConfig);
                frmConfigurazione.ShowDialog();
                frmConfigurazione.Close();
                frmConfigurazione.Dispose();
            }
        }

        private void BtnBordero_Click(object sender, EventArgs e)
        {
            frmBordero frmBordero = new frmBordero(this.NomeConnessione, this.DescConnessione, this.ServiceConfig);
            frmBordero.ShowDialog();
            frmBordero.Close();
            frmBordero.Dispose();
        }

        private void BtnConnessioni_Click(object sender, EventArgs e)
        {
            bool restart = false;
            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("CONNESSIONI"))
            {
                //if (!System.IO.File.Exists(frmSelezionaConnessione.FileNameTnsIni()))
                //{
                //    System.IO.File.WriteAllText(frmSelezionaConnessione.FileNameTnsIni(), "");
                //}
                frmSelezionaConnessione.SetNomiConnessione(out restart);
                this.Restart = restart;
                if (this.Restart)
                    this.Close();
            }
        }
        private void BtnGruppi_Click(object sender, EventArgs e)
        {
            if (this.BorderoFileConfig == null || this.BorderoFileConfig.Funzioni == null || this.BorderoFileConfig.Funzioni.Contains("GRUPPI"))
                EditGruppiAuto();
        }

        private void EditGruppiAuto()
        {
            libBordero.clsBorderoServiceConfig serviceConfig = libBordero.clsBorderoServiceConfigUtility.GetServiceConfig();
            Exception error = null;
            libBordero.clsConfigurazioneGruppi.ExternalFileName = "";
            string nomeExt = "";
            int nHeightButtons = 50;
            try
            {
                libBordero.clsConfigurazioneGruppi configurazione = null;
                if (libBordero.clsConfigurazioneGruppi.CheckExistsFileGruppi())
                    configurazione = libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error);
                else
                    configurazione = new libBordero.clsConfigurazioneGruppi();

                try
                {
                    List<ISenderData> senders = FinderInterfaceSenderData.GetSendersList();
                    if (senders != null && senders.Count > 0)
                    {
                        if (configurazione == null)
                            configurazione = new clsConfigurazioneGruppi();
                        senders.ForEach(senderData =>
                        {
                            clsItemGruppo gruppo = senderData.GetGruppo(null);
                            if (gruppo != null && configurazione.Gruppi.FirstOrDefault(g => g.NomeDll.Equals(gruppo.NomeDll)) == null)
                            {
                                configurazione.Gruppi.Add(gruppo);
                            }
                        });
                    }
                }
                catch (Exception)
                {
                }

                if (error != null)
                    MessageBox.Show(error.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    bool lcontinue = true;
                    while (lcontinue)
                    {
                        bool lOpenEsecuzione = false;
                        bool lCreatePlannedTask = false;

                        frmBorderoBase menuGruppi = new frmBorderoBase();
                        menuGruppi.MinimizeBox = false;
                        menuGruppi.MaximizeBox = false;
                        menuGruppi.HelpButton = true;
                        menuGruppi.HelpButtonClicked += (o, e) => { e.Cancel = true; clsBorderoHelp.ShowHelp("Gruppi automatici"); };
                        menuGruppi.Text = "Gestione gruppi automatici";
                        clsButtonBase button = null;
                        libBordero.clsItemGruppo gruppoSelezionato = null;
                        foreach (libBordero.clsItemGruppo gruppo in configurazione.Gruppi)
                        {
                            button = new clsButtonBase();
                            button.Image = WinAppBordero.Properties.Resources.database;
                            button.ImageAlign = ContentAlignment.MiddleLeft;
                            button.Height = nHeightButtons;
                            button.Text = gruppo.Descrizione;
                            button.Tag = gruppo;
                            button.Dock = DockStyle.Top;
                            button.Click += (o, e) =>
                            {
                                gruppoSelezionato = (libBordero.clsItemGruppo)((clsButtonBase)o).Tag;
                                ((clsButtonBase)o).FindForm().DialogResult = DialogResult.OK;
                            };
                            menuGruppi.Controls.Add(button);
                            button.BringToFront();
                        }

                        button = new clsButtonBase();
                        button.Image = WinAppBordero.Properties.Resources.miniplus.ToBitmap();
                        button.ImageAlign = ContentAlignment.MiddleLeft;
                        button.Height = nHeightButtons;
                        button.Text = "Nuovo";
                        button.Tag = null;
                        button.Dock = DockStyle.Top;
                        button.Click += (o, e) =>
                        {
                            ((clsButtonBase)o).FindForm().DialogResult = DialogResult.OK;
                        };
                        menuGruppi.Controls.Add(button);
                        button.BringToFront();

                        if (configurazione.Gruppi.Count > 0)
                        {
                            button = new clsButtonBase();
                            button.Image = WinAppBordero.Properties.Resources.arrowDn;
                            button.ImageAlign = ContentAlignment.MiddleLeft;
                            button.Height = nHeightButtons;
                            button.Text = "Esegui";
                            button.Tag = null;
                            button.Dock = DockStyle.Top;
                            button.Click += (o, e) =>
                            {
                                lOpenEsecuzione = true;
                                ((clsButtonBase)o).FindForm().DialogResult = DialogResult.Cancel;
                            };
                            menuGruppi.Controls.Add(button);
                            button.BringToFront();
                        }
                        

                        button = new clsButtonBase();
                        button.Image = WinAppBordero.Properties.Resources.arrowDn;
                        button.ImageAlign = ContentAlignment.MiddleLeft;
                        button.Height = nHeightButtons;
                        button.Text = "Gestisci un nuovo file di gruppi" + "\r\n" + 
                                      "in uso: " + (libBordero.clsConfigurazioneGruppi.ExternalFileName == "" ? "default" : nomeExt);
                        button.Tag = null;
                        button.Dock = DockStyle.Top;
                        button.Click += (o, e) =>
                        {
                            ((clsButtonBase)o).FindForm().DialogResult = DialogResult.No;
                        };
                        menuGruppi.Controls.Add(button);
                        button.BringToFront();


                        button = new clsButtonBase();
                        button.Image = WinAppBordero.Properties.Resources.close;
                        button.ImageAlign = ContentAlignment.MiddleLeft;
                        button.Height = nHeightButtons;
                        button.Text = "Chiudi";
                        button.Tag = null;
                        button.Dock = DockStyle.Top;
                        button.Click += (o, e) =>
                        {
                            ((clsButtonBase)o).FindForm().DialogResult = DialogResult.Abort;
                        };
                        menuGruppi.Controls.Add(button);
                        button.BringToFront();

                        clsLabel labelPlannedTask = new clsLabel();
                        labelPlannedTask.AutoSize = false;
                        labelPlannedTask.Dock = DockStyle.Top;
                        labelPlannedTask.Height = nHeightButtons / 2;
                        labelPlannedTask.Text = "Crea attività pianificata";
                        labelPlannedTask.TextAlign = ContentAlignment.MiddleRight;
                        labelPlannedTask.Font = new Font(menuGruppi.Font.FontFamily, menuGruppi.Font.Size, FontStyle.Underline);
                        labelPlannedTask.Tag = "PLANNED";
                        labelPlannedTask.Click += (o, e) =>
                        {
                            lCreatePlannedTask = true;
                            ((clsLabel)o).FindForm().DialogResult = DialogResult.Cancel;
                        };
                        menuGruppi.Controls.Add(labelPlannedTask);
                        labelPlannedTask.BringToFront();

                        DialogResult dialogResult = menuGruppi.ShowDialog();
                        lcontinue = (dialogResult == DialogResult.OK);

                        

                        if (lcontinue)
                        {
                            libBordero.clsItemGruppo backup = null;
                            if (gruppoSelezionato == null)
                                gruppoSelezionato = new libBordero.clsItemGruppo();
                            else
                            {
                                Dictionary<object, string> Opzioni = new Dictionary<object, string>();
                                Opzioni.Add("EDIT", "Modifica");
                                Opzioni.Add("DELETE", "Elimina");
                                object result = frmGetGenericData.MenuGenerico(gruppoSelezionato.Descrizione, Opzioni);
                                if (result != null && result.ToString() == "DELETE")
                                {
                                    configurazione.Gruppi.Remove(gruppoSelezionato);
                                    gruppoSelezionato = null;
                                    // salvataggio
                                    configurazione = libBordero.clsConfigurazioneGruppi.SaveConfigFromFile(configurazione, out error);
                                    lcontinue = error == null;
                                }
                                else if (result != null && result.ToString() == "EDIT")
                                {
                                    backup = gruppoSelezionato;
                                    gruppoSelezionato = gruppoSelezionato.Clone();
                                }
                                else
                                    gruppoSelezionato = null;
                            }

                            if (gruppoSelezionato != null)
                            {
                                bool lEdit = false;
                                string paramValue = "";


                                paramValue = gruppoSelezionato.Descrizione;
                                lEdit = GetValueString("DESCRIZIONE", "Descrizione", ref paramValue);
                                if (lEdit)
                                    gruppoSelezionato.Descrizione = paramValue;

                                if (lEdit)
                                {
                                    if (serviceConfig == null)
                                    {
                                        paramValue = gruppoSelezionato.NomeConnessione;
                                        lEdit = GetValueString("CONNESSIONE", "Connessione", ref paramValue);
                                        if (lEdit)
                                            gruppoSelezionato.NomeConnessione = paramValue;
                                    }
                                    else
                                        gruppoSelezionato.NomeConnessione = "";
                                }

                                if (lEdit)
                                {
                                    object valueGiorno = "";
                                    lEdit = GetMenu("Giorno da elaborare:", new Dictionary<object, string>() { { "IERI", "Ieri" }, { "OGGI", "Oggi" }, { "SETTIMANA_PREC", "Settimana precedente" }, { "MESE_PREC", "Mese precedente" } }, ref valueGiorno);
                                    if (lEdit)
                                        gruppoSelezionato.modalitaGiorno = valueGiorno.ToString();
                                }

                                if (lEdit)
                                {
                                    MessageBox.Show("Indicare se applicare all'elaborazione dei filtri." + "\r\n" + (gruppoSelezionato.isFiltered ? gruppoSelezionato.DescrizioneFiltri : "Nessuno impostato."));
                                    object filters = null;
                                    lEdit = GetMenuCheckFilters("Filtrare il borderò per valori specifici:", ref filters, gruppoSelezionato, false);
                                }

                                if (lEdit)
                                    gruppoSelezionato.generazionePdf = (MessageBox.Show("Generare i file .PDF del bordero ?", "Generazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.generazionePdf ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes);
                                if (lEdit)
                                    gruppoSelezionato.generazioneCsv = (MessageBox.Show("Generare iL .CSV del bordero ?", "Generazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.generazioneCsv ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes);

                                if (!gruppoSelezionato.generazionePdf)
                                {
                                    gruppoSelezionato.split_GIORNI = false;
                                    gruppoSelezionato.split_NOLEGGIATORE = false;
                                    gruppoSelezionato.split_ORGANIZZATORE = false;
                                    gruppoSelezionato.split_PRODUTTORE = false;
                                    gruppoSelezionato.split_SALA = false;
                                    gruppoSelezionato.split_TIPO_EVENTO = false;
                                    gruppoSelezionato.split_TITOLO = false;
                                }
                                else if (lEdit && gruppoSelezionato.generazionePdf)
                                {
                                    MessageBox.Show("Indicare se suddividere l'elaborazione in diversi file PDF." + "\r\n" + (gruppoSelezionato.isSplitted ? gruppoSelezionato.DescrizioneSplit : "Nessuna suddivisione"));
                                    object multi = null;
                                    lEdit = GetMenuCheckFilters("Suddividere il borderò per:", ref multi, gruppoSelezionato, true);
                                }

                                if (lEdit)
                                {
                                    if (MessageBox.Show("Inviare i file per email ?", "Invio", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.emailDests != "" ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.No)
                                    {
                                        gruppoSelezionato.emailDests = "";
                                        gruppoSelezionato.useEmailCodiceSistema = false;
                                        gruppoSelezionato.emailFrom = "";
                                        gruppoSelezionato.emailSmtp = "";
                                        gruppoSelezionato.emailPort = 0;
                                        gruppoSelezionato.emailUser = "";
                                        gruppoSelezionato.emailPassword = "";
                                        gruppoSelezionato.UseDefaultCredentials = true;
                                        gruppoSelezionato.EnableSsl = false;
                                    }
                                    else
                                    {
                                        gruppoSelezionato.useEmailCodiceSistema = (MessageBox.Show("Utilizzare l'account del codice sistema ?", "Invio", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.useEmailCodiceSistema ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes);

                                        if (!gruppoSelezionato.useEmailCodiceSistema && lEdit)
                                        {
                                            string valueEmailFrom = gruppoSelezionato.emailFrom;
                                            lEdit = GetValueString("FROM", "Mittente", ref valueEmailFrom);
                                            if (lEdit)
                                                gruppoSelezionato.emailFrom = valueEmailFrom;

                                            string valueEmail = "";
                                            if (lEdit)
                                            {
                                                valueEmail = gruppoSelezionato.emailSmtp;
                                                lEdit = GetValueString("SMTP", "Smtp Provider", ref valueEmail);
                                                if (lEdit)
                                                    gruppoSelezionato.emailSmtp = valueEmail;
                                            }
                                            if (lEdit)
                                            {
                                                int port = gruppoSelezionato.emailPort;
                                                lEdit = GetValueInt("PORT", "Porta", ref port);
                                                if (lEdit)
                                                    gruppoSelezionato.emailPort = port;
                                            }

                                            if (lEdit)
                                                gruppoSelezionato.UseDefaultCredentials = (MessageBox.Show("Utilizzare le credenziali di default ?", "Invio", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.UseDefaultCredentials ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes);
                                            if (lEdit)
                                                gruppoSelezionato.EnableSsl = (MessageBox.Show("Abilitare Ssl ?", "Invio", MessageBoxButtons.YesNo, MessageBoxIcon.Question, (gruppoSelezionato.EnableSsl ? MessageBoxDefaultButton.Button1 : MessageBoxDefaultButton.Button2)) == DialogResult.Yes);

                                            if (lEdit)
                                            {
                                                try
                                                {
                                                    valueEmail = libBordero.Bordero.DecryptGen(gruppoSelezionato.emailUser);
                                                }
                                                catch (Exception)
                                                {
                                                    valueEmail = gruppoSelezionato.emailUser;
                                                }
                                                
                                                lEdit = GetValueString("USER", "Nome utente di autenticazione", ref valueEmail);
                                                if (lEdit)
                                                    gruppoSelezionato.emailUser = libBordero.Bordero.EncryptGen(valueEmail);
                                            }

                                            if (lEdit)
                                            {
                                                try
                                                {
                                                    valueEmail = libBordero.Bordero.DecryptGen(gruppoSelezionato.emailPassword);
                                                }
                                                catch (Exception)
                                                {
                                                    valueEmail = gruppoSelezionato.emailPassword;
                                                }
                                                
                                                lEdit = GetValueString("USER", "Password di autenticazione", ref valueEmail);
                                                if (lEdit)
                                                    gruppoSelezionato.emailPassword = libBordero.Bordero.EncryptGen(valueEmail);
                                            }
                                        }
                                        if (lEdit)
                                        {
                                            string valueEmailDests = gruppoSelezionato.emailDests;
                                            lEdit = GetValueString("DESTS", "Destinatari (separati da ;)", ref valueEmailDests);
                                            if (lEdit)
                                                gruppoSelezionato.emailDests = valueEmailDests;
                                        }
                                    }
                                }

                                if (lEdit)
                                    gruppoSelezionato.serviceUserInfo = WinAppBordero.BorderoAuto.serviceUserInfo;

                                if (lEdit)
                                {
                                    if (backup != null)
                                        configurazione.Gruppi.Remove(backup);
                                    configurazione.Gruppi.Add(gruppoSelezionato);
                                    // salvataggio
                                    configurazione = libBordero.clsConfigurazioneGruppi.SaveConfigFromFile(configurazione, out error);
                                    lcontinue = error == null;
                                }
                            }
                        }
                        else
                        {
                            lcontinue = false;
                            if (dialogResult == DialogResult.Cancel && lOpenEsecuzione)
                            {
                                BorderoAuto borderoAuto = new BorderoAuto();
                                Dictionary<object, string> items = new Dictionary<object, string>();
                                libBordero.clsConfigurazioneGruppi configurazioneGruppiMenu = libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error);
                                if (error == null && configurazioneGruppiMenu != null && configurazioneGruppiMenu.Gruppi != null)
                                {
                                    foreach (libBordero.clsItemGruppo gruppo in configurazioneGruppiMenu.Gruppi)
                                        items.Add(gruppo.Descrizione, gruppo.Descrizione);
                                    object value = null;
                                    if (GetMenuCheck("Gruppi da eseguire", items, ref value))
                                    {
                                        Dictionary<object, string> lista = (Dictionary<object, string>)value;
                                        libBordero.clsConfigurazioneGruppi configurazioneGruppiEsecuzione = new libBordero.clsConfigurazioneGruppi();
                                        foreach (KeyValuePair<object, string> gruppo in lista)
                                        {
                                            libBordero.clsItemGruppo gruppoEsecuzione = configurazioneGruppiMenu.Gruppi.Find(g => g.Descrizione == gruppo.Value);
                                            if (gruppoEsecuzione != null)
                                                configurazioneGruppiEsecuzione.Gruppi.Add(gruppoEsecuzione);
                                        }
                                        if (borderoAuto.Init(configurazioneGruppiEsecuzione, serviceConfig == null))
                                        {
                                            Exception oError = null;

                                            bool findData = false;
                                            
                                            borderoAuto.Esecuzione(out oError, out findData, true, null, serviceConfig);


                                        }
                                    }
                                }
                            }
                            else if (dialogResult == DialogResult.Cancel && lCreatePlannedTask)
                            {
                                bool lWindows7 = MessageBox.Show("Sistema Operativo Windows 7 ?", "Tipo di sistema operativo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes;
                                bool lContinueCreatePlannedTask = false;
                                System.IO.FileInfo appInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                                List<string> linee = new List<string>(System.IO.File.ReadAllLines(appInfo.Directory.FullName + @"\ModelloScheduledTask.txt"));
                                if (lWindows7)
                                    linee = new List<string>(System.IO.File.ReadAllLines(appInfo.Directory.FullName + @"\ModelloScheduledTaskWin7.xml"));
                                Dictionary<object, string> Opzioni = new Dictionary<object, string>();
                                Opzioni.Add("AUTO", "Esegui tutti i gruppi di BorderoAuto.db (Default)");
                                if (configurazione.Gruppi.Count > 0)
                                {
                                    if (configurazione.Gruppi.Count == 1)
                                        Opzioni.Add("UN_GRUPPO", "Esegui il gruppo " + configurazione.Gruppi[0].Descrizione);
                                    else
                                        Opzioni.Add("UN_GRUPPO", "Esegui un gruppo a scelta" + (configurazione.Gruppi.Count == 1 ? "\r\n" + configurazione.Gruppi[0].Descrizione : ""));
                                }
                                Opzioni.Add("MAKE_FILE", "Seleziona un altro file di gruppi");
                                object result = frmGetGenericData.MenuGenerico("Creazione attività pianificata", Opzioni);

                                string cPath = appInfo.Directory.FullName;
                                string commandFile = "";
                                string cParams = "";
                                string cMakeFile = "";
                                string cModalita = "";
                                string cOrario = "";
                                string cDescrizione = "";
                                string cUser = "";
                                string cPassword = "";

                                clsItemGruppo gruppoSpecificoDaEseguire = null;
                                if (result != null && result.ToString() == "UN_GRUPPO")
                                {
                                    Opzioni = new Dictionary<object, string>();
                                    object autoResultGruppo = null;
                                    configurazione.Gruppi.ForEach(g =>
                                    {
                                        autoResultGruppo = g;
                                        Opzioni.Add(g, g.Descrizione);
                                    });
                                    object resultGruppo = null;
                                    if (Opzioni.Count > 0)
                                    {
                                        if (Opzioni.Count > 1)
                                            resultGruppo = frmGetGenericData.MenuGenerico("Gruppi per attività pianificata", Opzioni);
                                        else
                                            resultGruppo = autoResultGruppo;
                                    }
                                    if (resultGruppo != null)
                                        gruppoSpecificoDaEseguire = (clsItemGruppo)resultGruppo;
                                }

                                if (gruppoSpecificoDaEseguire != null)
                                {
                                    lContinueCreatePlannedTask = true;
                                    cParams = "AUTO=" + gruppoSpecificoDaEseguire.Descrizione.Trim().ToLower().Replace(" ", "_");
                                }
                                else if (result != null && result.ToString() == "AUTO")
                                {
                                    lContinueCreatePlannedTask = true;
                                    cParams = "AUTO";
                                }
                                else if (result != null && result.ToString() == "MAKE_FILE")
                                {
                                    lContinueCreatePlannedTask = true;
                                    cParams = "MAKE_FILE={0}";
                                }

                                if (lContinueCreatePlannedTask && cParams == "MAKE_FILE={0}")
                                {
                                    OpenFileDialog openFileDialog = new OpenFileDialog();
                                    openFileDialog.CheckFileExists = true;
                                    openFileDialog.Multiselect = false;
                                    openFileDialog.Title = "Selezionare il file dei gruppi";
                                    openFileDialog.RestoreDirectory = true;
                                    openFileDialog.CheckPathExists = true;
                                    openFileDialog.Filter = "db files (*.db)|*.txt|All files (*.*)|*.*";
                                    lContinueCreatePlannedTask = openFileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(openFileDialog.FileName) && System.IO.File.Exists(openFileDialog.FileName);
                                    if (lContinueCreatePlannedTask)
                                    {
                                        System.IO.FileInfo oMakeFileInfo = new System.IO.FileInfo(openFileDialog.FileName);
                                        cMakeFile = oMakeFileInfo.Name;
                                        cParams = string.Format(cParams, openFileDialog.FileName);
                                    }
                                    openFileDialog.Dispose();
                                }



                                //{FREQ_TASK}
                                //-Daily
                                //-Weekly -WeeksInterval 1 -DaysOfWeek Sunday
                                //
                                Opzioni = new Dictionary<object, string>();
                                Opzioni.Add("-Daily", "Ogni giorno per il giorno precedente");
                                Opzioni.Add("-Weekly -WeeksInterval 1 -DaysOfWeek {0}", "Una volta alla settimana per i sette giorni precedenti");
                                Opzioni.Add("-Monthly ", "Ogni primo del mese per il mese precedente");
                                object resultFreq = null;
                                object resultDay;
                                string keyDay = "";
                                string descrDay = "";
                                if (lContinueCreatePlannedTask && gruppoSpecificoDaEseguire == null)
                                {
                                    resultFreq = frmGetGenericData.MenuGenerico("Creazione attività pianificata", Opzioni);
                                    lContinueCreatePlannedTask = (resultFreq != null && !string.IsNullOrEmpty(resultFreq.ToString()));
                                }
                                else if (lContinueCreatePlannedTask && gruppoSpecificoDaEseguire != null)
                                {
                                    if (gruppoSpecificoDaEseguire.modalitaGiorno.Contains("IERI") || gruppoSpecificoDaEseguire.modalitaGiorno.Contains("OGGI"))
                                        resultFreq = "-Daily";
                                    else if (gruppoSpecificoDaEseguire.modalitaGiorno.Contains("SETTIMANA"))
                                        resultFreq = "-Weekly -WeeksInterval 1 -DaysOfWeek {0}";
                                    else if (gruppoSpecificoDaEseguire.modalitaGiorno.Contains("MESE"))
                                        resultFreq = "-Monthly ";
                                }

                                if (lContinueCreatePlannedTask && resultFreq.ToString().Trim() == "-Weekly -WeeksInterval 1 -DaysOfWeek {0}")
                                {
                                    Opzioni = new Dictionary<object, string>();
                                    foreach (System.DayOfWeek day in Enum.GetValues(typeof(System.DayOfWeek)))
                                    {
                                        GetDescrDayOfWeek(day, out keyDay, out descrDay);
                                        Opzioni.Add(keyDay, descrDay);
                                    }
                                    resultDay = frmGetGenericData.MenuGenerico("Creazione attività pianificata", Opzioni);
                                    lContinueCreatePlannedTask = (resultDay != null && !string.IsNullOrEmpty(resultDay.ToString()));
                                    keyDay = "";
                                    descrDay = "";
                                    if (lContinueCreatePlannedTask)
                                    {
                                        lContinueCreatePlannedTask = false;
                                        foreach (System.DayOfWeek day in Enum.GetValues(typeof(System.DayOfWeek)))
                                        {
                                            string kDay = "";
                                            string dDay = "";
                                            GetDescrDayOfWeek(day, out kDay, out dDay);
                                            if (kDay == resultDay.ToString())
                                            {
                                                keyDay = kDay;
                                                descrDay = dDay;
                                                lContinueCreatePlannedTask = true;
                                                cModalita = string.Format(resultFreq.ToString().Trim(), keyDay);
                                                if (lWindows7)
                                                {
                                                    cModalita = string.Format(@"<ScheduleByWeek><DaysOfWeek><{0} /></DaysOfWeek><WeeksInterval>1</WeeksInterval></ScheduleByWeek>", keyDay);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                else if (lContinueCreatePlannedTask && resultFreq.ToString().Trim() == "-Daily")
                                {
                                    cModalita = resultFreq.ToString().Trim();
                                    if (lWindows7)
                                        cModalita = string.Format(@"<ScheduleByDay><DaysInterval>{0}</DaysInterval></ScheduleByDay>", "1");
                                }
                                else if (lContinueCreatePlannedTask && resultFreq.ToString().Trim() == "-Monthly")
                                {
                                    cParams = "MONTHLY " + cParams;
                                    cModalita = "-Daily"; // l'applicazione viene eseguita tutti i giorni ma elabora solo il primo del mese
                                    if (lWindows7)
                                        cModalita = string.Format(@"<ScheduleByDay><DaysInterval>{0}</DaysInterval></ScheduleByDay>", "1");
                                }

                                if (lContinueCreatePlannedTask)
                                {
                                    while (lContinueCreatePlannedTask)
                                    {
                                        lContinueCreatePlannedTask = GetValueString("ORARIO", "Orario (HH:MI)", ref cOrario);
                                        if (lContinueCreatePlannedTask)
                                        {
                                            int nH = 0;
                                            int nM = 0;
                                            if (int.TryParse(cOrario.Substring(0, 2), out nH) && int.TryParse(cOrario.Substring(3, 2), out nM) &&
                                                nH >= 0 && nH <= 23 && nM >= 0 && nM <= 59)
                                                break;
                                            else
                                                MessageBox.Show("Formato ora errato.");
                                        }
                                    }
                                }

                                if (lContinueCreatePlannedTask)
                                {
                                    cDescrizione = string.Format("EsecuzioneAutomaticaBordero{0}Alle{1}", 
                                                                 (resultFreq.ToString().Trim() == "-Daily" ? "Giornaliero" : (resultFreq.ToString().Trim() == "-Monthly" ? "Mensile" : "Settimanale" + descrDay)), 
                                                                 cOrario.Replace(":","e"));
                                }

                                if (lContinueCreatePlannedTask)
                                    lContinueCreatePlannedTask = GetValueString("USER", "Utente con diritti Amministratore di macchina", ref cUser) && !string.IsNullOrEmpty(cUser);

                                if (lContinueCreatePlannedTask)
                                    lContinueCreatePlannedTask = GetValueString("PASSWORD", "Password con diritti Amministratore di macchina", ref cPassword) && !string.IsNullOrEmpty(cPassword);

                                if (lContinueCreatePlannedTask)
                                {
                                    commandFile = cPath + @"\BorderoAutoCreate.ps1";
                                    if (lWindows7)
                                        commandFile = cPath + @"\BorderoAutoCreate.xml";
                                    StringBuilder oSB = new StringBuilder();
                                    foreach (string linea in linee)
                                    {
                                        string value = linea;
                                        value = value.Replace("{NOME_TASK}", cDescrizione);
                                        value = value.Replace("{PATH_TASK}", cPath);
                                        value = value.Replace("{PARS_TASK}", cParams);
                                        value = value.Replace("{FREQ_TASK}", cModalita);
                                        value = value.Replace("{ORA_TASK}", cOrario);
                                        value = value.Replace("{NOME_TASK}", cDescrizione);
                                        value = value.Replace("{USER_TASK}", cUser);
                                        value = value.Replace("{PASSWORD_TASK}", cPassword);
                                        // UTILE SOLO PER WIN7 -----------------------------
                                        value = value.Replace("{NOME_PC}", Environment.MachineName);
                                        value = value.Replace("{DATA_OGGI}", string.Format("{0}-{1}-{2}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00")));
                                        value = value.Replace("{ORA_ADESSO}", string.Format("{0}:{1}:{2}.{3}", DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), DateTime.Now.Millisecond.ToString()));
                                        // -------------------------------------------------
                                        oSB.AppendLine(value);
                                    }
                                    System.IO.File.WriteAllText(commandFile, oSB.ToString());
                                    if (lWindows7)
                                        System.IO.File.WriteAllText(commandFile.Replace(".xml", ".bat"), string.Format(@"schtasks /Create /XML ""{0}"" /TN ""BorderoAutoCreate"" /RU ""{1}\{2}"" /RP ""{3}""", commandFile, Environment.MachineName, cUser, cPassword));
                                }

                                if (lContinueCreatePlannedTask)
                                { 
                                }

                                if (lContinueCreatePlannedTask)
                                {
                                    
                                    string command = @".\BorderoAutoCreate.ps1";
                                    if (lWindows7)
                                        command = @".\BorderoAutoCreate.bat";
                                    System.Windows.Forms.Clipboard.SetText(command);

                                    if (!lWindows7)
                                    {
                                        MessageBox.Show(string.Format(@"Eseguire PowerShell (come amministratore)" +
                                                                      "\r\n" + "Nella cartella" +
                                                                      "\r\n" + "{0}" +
                                                                      "\r\n" + "digitare:" +
                                                                      "\r\n" + command +
                                                                      "\r\n" + " (il comando precedente è stato inserito negli appunti potete incollarlo)" +
                                                                      "", cPath, cOrario));
                                    }
                                    else
                                    {
                                        MessageBox.Show(string.Format(@"Eseguire PowerShell (come amministratore)" +
                                                                      "\r\n" + "Nella cartella" +
                                                                      "\r\n" + "{0}" +
                                                                      "\r\n" + "digitare:" +
                                                                      "\r\n" + command +
                                                                      "\r\n" + " (il comando precedente è stato inserito negli appunti potete incollarlo)" +
                                                                      "", cPath, cOrario));
                                    }
                                    System.Diagnostics.Process.Start("explorer.exe", cPath);
                                }
                            }
                            else if (dialogResult == DialogResult.No)
                            {
                                lcontinue = true;
                                OpenFileDialog openFileDialog = new OpenFileDialog();
                                openFileDialog.CheckFileExists = false;
                                openFileDialog.Multiselect = false;
                                openFileDialog.Title = "Selezionare o creare un nuovo file";
                                openFileDialog.RestoreDirectory = true;
                                openFileDialog.CheckPathExists = false;
                                openFileDialog.Filter = "db files (*.db)|*.txt|All files (*.*)|*.*";
                                if (openFileDialog.ShowDialog() == DialogResult.OK)
                                {
                                    libBordero.clsConfigurazioneGruppi.ExternalFileName = openFileDialog.FileName;
                                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(libBordero.clsConfigurazioneGruppi.ExternalFileName);
                                    nomeExt = fileInfo.Name;

                                    if (libBordero.clsConfigurazioneGruppi.CheckExistsFileGruppi())
                                        configurazione = libBordero.clsConfigurazioneGruppi.GetConfigFromFile(out error);
                                    else
                                        configurazione = new libBordero.clsConfigurazioneGruppi();
                                }
                            }
                        }
                    }
                    if (error != null)
                        MessageBox.Show(error.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                libBordero.clsConfigurazioneGruppi.ExternalFileName = "";
            }
        }


        public void GetDescrDayOfWeek(System.DayOfWeek day, out string key, out string descr)
        {
            key = "";
            descr = "";
            switch (day)
            {
                case DayOfWeek.Monday: { key = "Monday"; descr = "Lunedi"; break; }
                case DayOfWeek.Tuesday: { key = "Tuesday"; descr = "Martedi"; break; }
                case DayOfWeek.Wednesday: { key = "Wednesday"; descr = "Mercoledi"; break; }
                case DayOfWeek.Thursday: { key = "Thursday"; descr = "Giovedi"; break; }
                case DayOfWeek.Friday: { key = "Friday"; descr = "Venerdi"; break; }
                case DayOfWeek.Saturday: { key = "Saturday"; descr = "Sabato"; break; }
                case DayOfWeek.Sunday: { key = "Sunday"; descr = "Domenica"; break; }
            }
        }
        public bool GetValueString(string Code, string descr, ref string value)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeString, value, false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = true;
                        value = item.Value.ToString();
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public bool GetValueInt(string Code, string descr, ref int value)
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            items.Add(new clsItemGenericData(Code, descr, clsItemGenericData.EnumGenericDataType.TypeNumInt, value, false, true));
            frmGetGenericData fGen = new frmGetGenericData(descr, items);
            bool result = false;
            if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                items = fGen.Data;
                foreach (clsItemGenericData item in items)
                {
                    if (item.Code == Code)
                    {
                        result = int.TryParse(item.Value.ToString(), out value);
                        break;
                    }
                }
            }
            fGen.Dispose();
            return result;
        }

        public static bool GetMenu(string descr, Dictionary<object, string> items, ref object value)
        {
            value = null;
            value = clsUtility.MenuGenerico(descr, items);
            return value != null;
        }

        private bool GetMenuCheckFilters(string descr, ref object value, libBordero.clsItemGruppo itemGruppo, bool isMulti)
        {
            value = null;
            libBordero.clsItemGruppo itemGruppoBackup = itemGruppo.Clone();
            string[] aListaTabelleFiltro = new string[] { "BORDERO_FILT_GIORNI", "BORDERO_FILT_ORGANIZZATORI", "BORDERO_FILT_TIPI_EVENTO", "BORDERO_FILT_SALE", "BORDERO_FILT_TITOLI", "BORDERO_FILT_PRODUTTORI", "BORDERO_FILT_NOLEGGIATORI" };
            string[] aListaTabelleFiltroDesc = new string[] { "Giorni", "Organizzatori", "Tipi Evento", "Sale", "Titoli", "Produttori", "Distributori" };
            string[] aListaCampiFiltro = new string[] { "", "CODICE_FISCALE_ORGANIZZATORE", "TIPO_EVENTO", "CODICE_LOCALE", "TITOLO_EVENTO", "PRODUTTORE", "NOLEGGIO" };
            string[] aListaValoriFiltro = new string[] { "", itemGruppo.value_ORGANIZZATORE, itemGruppo.value_TIPO_EVENTO, itemGruppo.value_SALA, itemGruppo.value_TITOLO, itemGruppo.value_PRODUTTORE, itemGruppo.value_NOLEGGIATORE };
            bool[] aListaMulti = new bool[] { itemGruppo.split_GIORNI, itemGruppo.split_ORGANIZZATORE, itemGruppo.split_TIPO_EVENTO, itemGruppo.split_SALA, itemGruppo.split_TITOLO, itemGruppo.split_PRODUTTORE, itemGruppo.split_NOLEGGIATORE };


            Dictionary<object, string> items = new Dictionary<object, string>();
            Dictionary<object, bool> itemschecked = new Dictionary<object, bool>();
            int index = 0;
            foreach (string key in aListaTabelleFiltro)
            {
                if (isMulti || key != "BORDERO_FILT_GIORNI")
                {
                    items.Add(key, aListaTabelleFiltroDesc[index]);
                    switch (key)
                    {
                        case "BORDERO_FILT_GIORNI": { itemschecked.Add(key, (isMulti ? itemGruppo.split_GIORNI : false)); break; }
                        case "BORDERO_FILT_ORGANIZZATORI": { itemschecked.Add(key, (isMulti ? itemGruppo.split_ORGANIZZATORE : itemGruppo.value_ORGANIZZATORE.Trim() != "")); break; }
                        case "BORDERO_FILT_TIPI_EVENTO": { itemschecked.Add(key, (isMulti ? itemGruppo.split_TIPO_EVENTO : itemGruppo.value_TIPO_EVENTO.Trim() != "")); break; }
                        case "BORDERO_FILT_SALE": { itemschecked.Add(key, (isMulti ? itemGruppo.split_SALA : itemGruppo.value_SALA.Trim() != "")); break; }
                        case "BORDERO_FILT_TITOLI": { itemschecked.Add(key, (isMulti ? itemGruppo.split_TITOLO : itemGruppo.value_TITOLO.Trim() != "")); break; }
                        case "BORDERO_FILT_PRODUTTORI": { itemschecked.Add(key, (isMulti ? itemGruppo.split_PRODUTTORE : itemGruppo.value_PRODUTTORE.Trim() != "")); break; }
                        case "BORDERO_FILT_NOLEGGIATORI": { itemschecked.Add(key, (isMulti ? itemGruppo.split_NOLEGGIATORE : itemGruppo.value_NOLEGGIATORE.Trim() != "")); break; }
                    }
                }
                index += 1;
            }

            if (GetMenuCheck(descr, items, ref value, itemschecked))
            {
                try
                {
                    Dictionary<object, string> lista = (Dictionary<object, string>)value;
                    index = 0;
                    if (isMulti)
                    {
                        itemGruppo.split_GIORNI = false;
                        itemGruppo.split_ORGANIZZATORE = false;
                        itemGruppo.split_TIPO_EVENTO= false;
                        itemGruppo.split_SALA = false;
                        itemGruppo.split_TITOLO = false;
                        itemGruppo.split_NOLEGGIATORE = false;
                        itemGruppo.split_PRODUTTORE = false;
                    }
                    else
                    {
                        itemGruppo.value_ORGANIZZATORE = "";
                        itemGruppo.value_TIPO_EVENTO = "";
                        itemGruppo.value_SALA= "";
                        itemGruppo.value_TITOLO = "";
                        itemGruppo.value_NOLEGGIATORE = "";
                        itemGruppo.value_PRODUTTORE = "";
                    }
                    foreach (string key in aListaTabelleFiltro)
                    {
                        if (isMulti)
                        {
                            switch (key)
                            {
                                case "BORDERO_FILT_GIORNI": { itemGruppo.split_GIORNI = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_ORGANIZZATORI": { itemGruppo.split_ORGANIZZATORE = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_TIPI_EVENTO": { itemGruppo.split_TIPO_EVENTO = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_SALE": { itemGruppo.split_SALA = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_TITOLI": { itemGruppo.split_TITOLO = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_PRODUTTORI": { itemGruppo.split_PRODUTTORE = lista.ContainsKey(key); break; }
                                case "BORDERO_FILT_NOLEGGIATORI": { itemGruppo.split_NOLEGGIATORE = lista.ContainsKey(key); break; }
                            }
                        }
                        else
                        {
                            string valueFiltro = "";
                            switch (key)
                            {
                                case "BORDERO_FILT_ORGANIZZATORI": { valueFiltro = itemGruppoBackup.value_ORGANIZZATORE; break; }
                                case "BORDERO_FILT_TIPI_EVENTO": { valueFiltro = itemGruppoBackup.value_TIPO_EVENTO; break; }
                                case "BORDERO_FILT_SALE": { valueFiltro = itemGruppoBackup.value_SALA; break; }
                                case "BORDERO_FILT_TITOLI": { valueFiltro = itemGruppoBackup.value_TITOLO; break; }
                                case "BORDERO_FILT_PRODUTTORI": { valueFiltro = itemGruppoBackup.value_PRODUTTORE; break; }
                                case "BORDERO_FILT_NOLEGGIATORI": { valueFiltro = itemGruppoBackup.value_NOLEGGIATORE; break; }
                            }
                            if (lista.ContainsKey(key))
                            {
                                while (true)
                                {
                                    bool lEditValue = GetValueString(key, aListaTabelleFiltroDesc[index], ref valueFiltro);
                                    if (lEditValue || MessageBox.Show(string.Format("Nessun filtro per {0} ?", aListaTabelleFiltroDesc[index]), aListaTabelleFiltroDesc[index], MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        if (!lEditValue) valueFiltro = "";
                                        break;
                                    }
                                }
                            }
                            else
                                valueFiltro = "";

                            switch (key)
                            {
                                case "BORDERO_FILT_ORGANIZZATORI": { itemGruppo.value_ORGANIZZATORE = valueFiltro; break; }
                                case "BORDERO_FILT_TIPI_EVENTO": { itemGruppo.value_TIPO_EVENTO = valueFiltro; break; }
                                case "BORDERO_FILT_SALE": { itemGruppo.value_SALA = valueFiltro; break; }
                                case "BORDERO_FILT_TITOLI": { itemGruppo.value_TITOLO = valueFiltro; break; }
                                case "BORDERO_FILT_PRODUTTORI": { itemGruppo.value_PRODUTTORE = valueFiltro; break; }
                                case "BORDERO_FILT_NOLEGGIATORI": { itemGruppo.value_NOLEGGIATORE = valueFiltro; break; }
                            }
                        }
                        index += 1;
                    }
                }
                catch (Exception)
                {
                    value = null;
                }
            }
            return value != null;
        }

        public static bool GetMenuCheck(string descr, Dictionary<object, string> items, ref object value, Dictionary<object, bool> itemschecked = null)
        {
            value = null;
            value = clsUtility.MenuGenerico(descr, items, null, null, true, itemschecked);
            return value != null;
        }

    }
}
