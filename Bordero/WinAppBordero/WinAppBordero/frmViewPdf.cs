﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PdfiumViewer;
namespace WinAppBordero
{
    public partial class frmViewPdf : WinAppBordero.frmBorderoBase
    {

        public string FindArgumentInPdf = "";
        public frmViewPdf(string file)
        {
            InitializeComponent();
            this.Font = frmBorderoBase.GetRightFont(this.Font);
            this.pdfViewer.Document?.Dispose();
            this.pdfViewer.Document = OpenDocument(file);
            this.KeyDown += FrmViewPdf_KeyDown;
            this.KeyPreview = true;
            this.Shown += FrmViewPdf_Shown;
        }

        private void FrmViewPdf_Shown(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FindArgumentInPdf))
                this.FindArgument(FindArgumentInPdf);
        }

        private void FrmViewPdf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }

        public frmViewPdf(System.IO.Stream stream)
        {
            InitializeComponent();
            this.pdfViewer.Document?.Dispose();
            this.pdfViewer.Document = OpenDocument(stream);
            this.KeyDown += FrmViewPdf_KeyDown;
            this.KeyPreview = true;
        }

        private PdfDocument OpenDocument(string fileName)
        {
            try
            {
                PdfDocument oPdfDoc = PdfDocument.Load(this, fileName);
                return oPdfDoc;
            }
            catch (Exception ex)
            {
                MessageBox.Show("errore\r\n" + ex.ToString());
                return null;
            }
        }

        private PdfDocument OpenDocument(System.IO.Stream stream)
        {
            try
            {
                PdfDocument oPdfDoc = PdfDocument.Load(this, stream);
                return oPdfDoc;
            }
            catch (Exception ex)
            {
                MessageBox.Show("errore\r\n" + ex.ToString());
                return null;
            }
        }

        public void FindArgument(string argument)
        {
            if (this.pdfViewer.Document != null)
            {
                PdfMatches matches = this.pdfViewer.Document.Search(argument, true, true);
                if (matches != null)
                {
                    foreach (PdfMatch m in matches.Items)
                    {
                        this.pdfViewer.Renderer.Page = m.Page;
                        break;
                    }
                }
            }
        }
    }
}
