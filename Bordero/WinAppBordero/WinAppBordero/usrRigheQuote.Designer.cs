﻿
namespace WinAppBordero
{
    partial class usrRigheQuote
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usrRigheQuote));
            this.dgvRighe = new System.Windows.Forms.DataGridView();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnDel = new WinAppBordero.clsButtonBase();
            this.imageListAddRemove = new System.Windows.Forms.ImageList(this.components);
            this.btnAdd = new WinAppBordero.clsButtonBase();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRighe)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvRighe
            // 
            this.dgvRighe.AllowUserToAddRows = false;
            this.dgvRighe.AllowUserToDeleteRows = false;
            this.dgvRighe.AllowUserToResizeColumns = false;
            this.dgvRighe.AllowUserToResizeRows = false;
            this.dgvRighe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRighe.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvRighe.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRighe.Location = new System.Drawing.Point(0, 37);
            this.dgvRighe.Name = "dgvRighe";
            this.dgvRighe.ReadOnly = true;
            this.dgvRighe.RowHeadersVisible = false;
            this.dgvRighe.RowHeadersWidth = 62;
            this.dgvRighe.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvRighe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvRighe.Size = new System.Drawing.Size(349, 29);
            this.dgvRighe.TabIndex = 0;
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.btnDel);
            this.pnlMenu.Controls.Add(this.btnAdd);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(349, 37);
            this.pnlMenu.TabIndex = 1;
            // 
            // btnDel
            // 
            this.btnDel.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnDel.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.ImageIndex = 1;
            this.btnDel.ImageList = this.imageListAddRemove;
            this.btnDel.Location = new System.Drawing.Point(36, 0);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(36, 37);
            this.btnDel.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnDel, "Rimuovi riga");
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // imageListAddRemove
            // 
            this.imageListAddRemove.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListAddRemove.ImageStream")));
            this.imageListAddRemove.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListAddRemove.Images.SetKeyName(0, "miniplus.ico");
            this.imageListAddRemove.Images.SetKeyName(1, "miniminus.ico");
            // 
            // btnAdd
            // 
            this.btnAdd.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ImageIndex = 0;
            this.btnAdd.ImageList = this.imageListAddRemove;
            this.btnAdd.Location = new System.Drawing.Point(0, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(36, 37);
            this.btnAdd.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnAdd, "Aggiungi riga");
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // usrRigheQuote
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.dgvRighe);
            this.Controls.Add(this.pnlMenu);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "usrRigheQuote";
            this.Size = new System.Drawing.Size(349, 148);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRighe)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRighe;
        private System.Windows.Forms.Panel pnlMenu;
        private clsButtonBase btnDel;
        private clsButtonBase btnAdd;
        private System.Windows.Forms.ImageList imageListAddRemove;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
