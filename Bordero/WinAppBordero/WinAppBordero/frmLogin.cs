﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinAppBordero
{
    public partial class frmLogin : WinAppBordero.frmBorderoBase
    {
        public frmLogin()
        {
            InitializeComponent();
            this.Shown += FrmLogin_Shown;
        }

        private void FrmLogin_Shown(object sender, EventArgs e)
        {
            this.txtLogin.GotFocus += TxtLogin_GotFocus;
            this.txtPsw.GotFocus += TxtPsw_GotFocus;
            this.txtLogin.KeyPress += TxtLogin_KeyPress; 
            this.txtPsw.KeyPress += TxtPsw_KeyPress;
            this.btnOK.Click += BtnOK_Click;
            this.txtLogin.Focus();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Login) && !string.IsNullOrEmpty(this.Password))
                this.DialogResult = DialogResult.OK;
        }

        private void TxtPsw_GotFocus(object sender, EventArgs e)
        {
            this.txtPsw.SelectAll();
        }

        private void TxtLogin_GotFocus(object sender, EventArgs e)
        {
            this.txtLogin.SelectAll();
        }



        private void TxtLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
                this.txtPsw.Select();
        }

        private void TxtPsw_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (!string.IsNullOrEmpty(this.Login) && !string.IsNullOrEmpty(this.Password))
                    this.DialogResult = DialogResult.OK;
            }
        }

        public string Login => this.txtLogin.Text;
        public string Password => this.txtPsw.Text;
    }
}
