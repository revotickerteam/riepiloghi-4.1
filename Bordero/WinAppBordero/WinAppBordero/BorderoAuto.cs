﻿using ICSharpCode.SharpZipLib.Zip;
using libBordero;
using libUpdater;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppBordero
{
    public class BorderoMenuConnessione
    {
        public string Codice { get; set; }
        public string Descrizione { get; set; }

        public List<BorderoMenuConnessione> Connessioni { get; set; }

        public static BorderoMenuConnessione GetBorderoMenuConnessioneFromFile(string file = "")
        {
            BorderoMenuConnessione result = null;
            if (string.IsNullOrEmpty(file))
            {
                System.IO.FileInfo oAppInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                file = oAppInfo.Directory.FullName + @"\BConnessioni.json";
            }
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(file))
            {
                try
                {
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<BorderoMenuConnessione>(System.IO.File.ReadAllText(file));
                }
                catch (Exception ex)
                {
                    result = null;
                }
            }
            return result;
        }
    }

    public class BorderoFileConfig
    {
        public List<string> Organizzatori { get; set; }
        public List<string> CodiciLocali { get; set; }

        public List<string> Funzioni { get; set; }

        public static BorderoFileConfig GetBorderoFileConfigFromFile(string file = "")
        {
            BorderoFileConfig result = null;
            if (string.IsNullOrEmpty(file))
            {
                System.IO.FileInfo oAppInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                file = oAppInfo.Directory.FullName + @"\BConfig.json";
            }
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(file))
            {
                try
                {
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<BorderoFileConfig>(System.IO.File.ReadAllText(file));
                }
                catch (Exception)
                {
                    result = null;
                }
            }
            return result;
        }
    }
    public class BorderoAuto
    {
        private string nomeConnessione = "SERVERX";

        public bool esecuzioneManuale = false;
        public string makeFile = "";
        public clsConfigurazioneGruppi ConfigurazioneGruppi = null;

        public static string serviceUserInfo = "";


        public BorderoAuto()
        { }
        
        //public bool Init(clsConfigurazioneGruppi configurazione, bool periodoEsterno = false
        public bool Init(clsConfigurazioneGruppi configurazione, bool connectToDb)
        {
            this.ConfigurazioneGruppi = configurazione;
            //return this.Init(null, false, periodoEsterno);
            return this.Init(null, false, connectToDb);
        }
        //public bool Init(string[] args, bool loadConfig = true, bool periodoEsterno = false)
        public bool Init(string[] args, bool loadConfig, bool connectToDb)
        {
            Exception error = null;
            string name = "";
            string value = "";
            bool monthly = false;
            string nomeGruppoSpecificoDaEseguire = "";

            if (args != null)
            {
                foreach (string line in args)
                {
                    if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#") && line.Contains("="))
                    {
                        name = line.Split('=')[0];
                        value = line.Split('=')[1];
                        if (name == "CONNESSIONE")
                        {
                            nomeConnessione = value;
                        }
                        else if (name == "MANUALE")
                        {
                            esecuzioneManuale = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE";
                        }
                        else if (name == "MAKE_FILE")
                        {
                            makeFile = value;
                        }
                        else if (name == "AUTO")
                        {
                            makeFile = clsConfigurazioneGruppi.GetNomeDb();
                            if (!string.IsNullOrEmpty(value))
                                nomeGruppoSpecificoDaEseguire = value.Trim().ToLower().Replace(" ","_");
                        }
                        else if (name == "MONTHLY")
                        {
                            makeFile = clsConfigurazioneGruppi.GetNomeDb();
                        }
                    }
                    else if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#") && line == "AUTO")
                        makeFile = clsConfigurazioneGruppi.GetNomeDb();
                    else if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#") && line == "MONTHLY")
                        monthly = true;
                }
            }

            if (esecuzioneManuale)
                return true;

            if (monthly)
            {
                if (DateTime.Now.Day != 1)
                {
                    WriteAutoLogExecution(false, "Da eseguire ogni primo del mese: oggi NO");
                    return false;
                }
                else
                    WriteAutoLogExecution(false, "Da eseguire ogni primo del mese: oggi SI");
            }

            WriteAutoLogExecution(false, "Init args");
            DateTime dValue;

            clsItemGruppo item = null;
            if (makeFile == "" && loadConfig)
            {
                this.ConfigurazioneGruppi = new clsConfigurazioneGruppi();
                item = new clsItemGruppo();
                ConfigurazioneGruppi.Gruppi.Add(item);
                item.Descrizione = "";
                item.NomeConnessione = nomeConnessione;

                try
                {
                    WriteAutoLogExecution(false, string.Format("connessione:{0}", item.NomeConnessione));
                    error = null;
                    item.conn = libBordero.Bordero.GetConnection(item.NomeConnessione, out error);
                }
                catch (Exception exConn)
                {
                    error = new Exception("Errore di connessione " + exConn.Message);
                    WriteAutoLogExecution(false, "errore", error);
                }

                if (error != null)
                    return false;

                try
                {
                    foreach (string line in args)
                    {
                        if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line) && !line.StartsWith("#") && line.Contains("="))
                        {
                            name = line.Split('=')[0];
                            value = line.Split('=')[1];
                            WriteAutoLogExecution(false, string.Format("{0}:{1}", name, value));
                            switch (name)
                            {
                                case "IERI":
                                    {
                                        item.modalitaGiorno = "IERI";
                                        item.Inizio = GetSysdate(item.conn, out error).AddDays(-1).Date;
                                        item.Fine = item.Inizio;
                                        WriteAutoLogExecution(false, string.Format("{0}:{1}", item.Inizio.ToShortDateString(), item.Fine.ToShortDateString()));
                                        break;
                                    }
                                case "GIORNO":
                                case "INIZIO":
                                    {
                                        if (exractDateFromString(value, out dValue))
                                        {
                                            item.Inizio = dValue;
                                            item.Fine = dValue;
                                        }
                                        else
                                            error = new Exception(string.Format("parametro {0} non valido YYYYMMDD", name));
                                        WriteAutoLogExecution(false, string.Format("{0}:{1}", item.Inizio.ToShortDateString(), item.Fine.ToShortDateString()), error);
                                        break;
                                    }
                                case "FINE":
                                    {
                                        if (exractDateFromString(value, out dValue))
                                            item.Fine = dValue;
                                        else
                                            error = new Exception(string.Format("parametro {0} non valido YYYYMMDD", name));
                                        WriteAutoLogExecution(false, string.Format("{0}:{1}", item.Inizio.ToShortDateString(), item.Fine.ToShortDateString()), error);
                                        break;
                                    }
                                case "SETTIMANA_PREC":
                                    {
                                        item.modalitaGiorno = "SETTIMANA_PREC";
                                        item.Fine = GetSysdate(item.conn, out error).AddDays(-1).Date;
                                        item.Inizio = item.Fine.AddDays(-6).Date;
                                        WriteAutoLogExecution(false, string.Format("{0}:{1}", item.Inizio.ToShortDateString(), item.Fine.ToShortDateString()), error);
                                        break;
                                    }
                                case "MESE_PREC":
                                    {
                                        item.modalitaGiorno = "MESE_PREC";
                                        DateTime today = GetSysdate(item.conn, out error);
                                        int year = today.Year;
                                        int month = today.Month - 1;
                                        if (today.Month == 1)
                                        {
                                            year -= 1;
                                            month = 12;
                                        }
                                        item.Inizio = new DateTime(year, month, 1);
                                        item.Fine = item.Inizio.AddMonths(1).AddDays(-1);
                                        WriteAutoLogExecution(false, string.Format("{0}:{1}", item.Inizio.ToShortDateString(), item.Fine.ToShortDateString()), error);
                                        break;
                                    }
                                case "SPLIT_ORGANIZZATORE": { item.split_ORGANIZZATORE = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "SPLIT_TIPO_EVENTO": { item.split_TIPO_EVENTO = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "SPLIT_SALA": { item.split_SALA = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "SPLIT_TITOLO": { item.split_TITOLO = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "SPLIT_PRODUTTORE": { item.split_PRODUTTORE = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "SPLIT_DISTRIBUTORE": { item.split_ORGANIZZATORE = value.Trim().ToUpper() == "ABILITATO" || value.Trim().ToUpper() == "TRUE"; break; }
                                case "ORGANIZZATORE": { item.value_ORGANIZZATORE = value; break; }
                                case "TIPO_EVENTO": { item.value_TIPO_EVENTO = value; break; }
                                case "SALA": { item.value_SALA = value; break; }
                                case "TITOLO": { item.value_TITOLO = value; break; }
                                case "PRODUTTORE": { item.value_PRODUTTORE = value; break; }
                                case "DISTRIBUTORE": { item.value_ORGANIZZATORE = value; break; }
                            }
                        }
                    }
                }
                catch (Exception exInitParametri)
                {
                    error = new Exception(string.Format("Errore inizializzazione parameri {0} {1} {2}", name, value, exInitParametri.Message));
                    WriteAutoLogExecution(false, "errore", error);
                }
            }
            else
            {
                WriteAutoLogExecution(false, "caricamento configurazione");
                if (loadConfig)
                    this.ConfigurazioneGruppi = clsConfigurazioneGruppi.GetConfigFromFile(out error);

                WriteAutoLogExecution(false, $"caricamento configurazione {(error == null ? "OK" : "CON ERRORI")}", error);
                if (error == null && this.ConfigurazioneGruppi != null)
                {
                    if (!string.IsNullOrEmpty(nomeGruppoSpecificoDaEseguire))
                    {
                        clsItemGruppo itemGruppoSpecifico = this.ConfigurazioneGruppi.Gruppi.FirstOrDefault(g => g.Descrizione.Trim().ToLower().Replace(" ", "_").Equals(nomeGruppoSpecificoDaEseguire));
                        WriteAutoLogExecution(false, $"gruppo specifico da eseguire {nomeGruppoSpecificoDaEseguire} {(itemGruppoSpecifico == null ? "NON TROVATO" : "OK")}", error);
                        if (itemGruppoSpecifico == null)
                            this.ConfigurazioneGruppi.Gruppi.Clear();
                        else
                            this.ConfigurazioneGruppi.Gruppi = new List<clsItemGruppo>() { itemGruppoSpecifico };
                    }

                    foreach (clsItemGruppo itemGruppo in this.ConfigurazioneGruppi.Gruppi)
                    {
                        if (connectToDb)
                        {
                            clsItemGruppo itemFindConn = this.ConfigurazioneGruppi.Gruppi.Find(g => !g.Equals(itemGruppo) && g.NomeConnessione == itemGruppo.NomeConnessione && g.modalitaGiorno == itemGruppo.modalitaGiorno && g.conn != null);

                            if (itemFindConn != null)
                                itemGruppo.conn = itemFindConn.conn;

                            if (itemGruppo.conn == null && connectToDb)
                            {
                                try
                                {
                                    WriteAutoLogExecution(false, string.Format("connessione: {0}", itemGruppo.NomeConnessione));
                                    itemGruppo.conn = libBordero.Bordero.GetConnection(itemGruppo.NomeConnessione, out error);
                                }
                                catch (Exception exConn)
                                {
                                    error = new Exception("Errore di connessione " + exConn.Message);
                                    WriteAutoLogExecution(false, "errore", error);
                                }
                            }
                        }

                        if (!itemGruppo.PeriodoEsterno)
                        {
                            if (itemGruppo.modalitaGiorno == "IERI")
                            {
                                itemGruppo.Inizio = GetSysdate(itemGruppo.conn, out error).AddDays(-1).Date;
                                itemGruppo.Fine = itemGruppo.Inizio;
                                WriteAutoLogExecution(false, string.Format("{0}:{1}", itemGruppo.Inizio.ToShortDateString(), itemGruppo.Fine.ToShortDateString()), error);
                            }
                            else if (itemGruppo.modalitaGiorno == "OGGI" || itemGruppo.modalitaGiorno == "")
                            {
                                itemGruppo.Inizio = GetSysdate(itemGruppo.conn, out error).Date;
                                itemGruppo.Fine = itemGruppo.Inizio;
                                WriteAutoLogExecution(false, string.Format("{0}:{1}", itemGruppo.Inizio.ToShortDateString(), itemGruppo.Fine.ToShortDateString()), error);
                            }
                            else if (itemGruppo.modalitaGiorno == "SETTIMANA_PREC")
                            {
                                itemGruppo.modalitaGiorno = "SETTIMANA_PREC";
                                itemGruppo.Fine = GetSysdate(itemGruppo.conn, out error).AddDays(-1).Date;
                                itemGruppo.Inizio = itemGruppo.Fine.AddDays(-6).Date;
                                WriteAutoLogExecution(false, string.Format("{0}:{1}", itemGruppo.Inizio.ToShortDateString(), itemGruppo.Fine.ToShortDateString()), error);
                                //break;
                            }
                            else if (itemGruppo.modalitaGiorno == "MESE_PREC")
                            {
                                itemGruppo.modalitaGiorno = "MESE_PREC";
                                DateTime today = GetSysdate(itemGruppo.conn, out error);
                                int year = today.Year;
                                int month = today.Month - 1;
                                if (today.Month == 1)
                                {
                                    year -= 1;
                                    month = 12;
                                }
                                itemGruppo.Inizio = new DateTime(year, month, 1);
                                itemGruppo.Fine = itemGruppo.Inizio.AddMonths(1).AddDays(-1);
                                WriteAutoLogExecution(false, string.Format("{0}:{1}", itemGruppo.Inizio.ToShortDateString(), itemGruppo.Fine.ToShortDateString()), error);
                                //break;
                            }
                        }
                    }
                }
            }
            
            return error == null;
        }

        private bool exractDateFromString(string value, out DateTime date)
        {
            date = DateTime.MinValue;
            bool result = false;
            try
            {
                int anno = 0;
                int mese = 0;
                int giorno = 0;
                if (value.Length == 8 &&
                    int.TryParse(value.Substring(0, 4), out anno) &&
                    int.TryParse(value.Substring(4, 2), out mese) &&
                    int.TryParse(value.Substring(6, 2), out giorno))
                {
                    date = new DateTime(anno, mese, giorno).Date;
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private DateTime GetSysdate(IConnection conn, out Exception error)
        {
            error = null;
            DateTime result = DateTime.MinValue;
            if (conn != null)
            {
                try
                {
                    IRecordSet oRS = (IRecordSet)conn.ExecuteQuery("SELECT SYSDATE AS DATA_SISTEMA FROM DUAL");
                    if (!oRS.EOF && !oRS.Fields("DATA_SISTEMA").IsNull)
                        result = (DateTime)oRS.Fields("DATA_SISTEMA").Value;
                    else
                        error = new Exception("Impossibile determinare la data odierna");
                    oRS.Close();
                }
                catch (Exception ex)
                {
                    error = new Exception(string.Format("Errore nella richiesta data odierna {0}", ex.Message));
                }
            }
            else
                result = DateTime.Now;
            return result;
        }

        public static clsCredenzialiCodiceSistema GetCredenzialiCodiceSistema(IConnection conn, out Exception error)
        {
            error = null;
            clsCredenzialiCodiceSistema result = new clsCredenzialiCodiceSistema();
            try
            {
                StringBuilder oSB = new StringBuilder();

                oSB.Append("SELECT ");
                oSB.Append(" SMART_CARD.EMAIL_TITOLARE");
                oSB.Append(" FROM SMART_CS, SMART_CARD, SMART_CODE ");
                oSB.Append(" WHERE ");
                oSB.Append(" SMART_CS.ENABLED = 1 AND ");
                oSB.Append(" SMART_CARD.ENABLED = 1 AND ");
                oSB.Append(" SMART_CARD.CODICE_SISTEMA = SMART_CS.CODICE_SISTEMA AND ");
                oSB.Append(" SMART_CARD.IDCARD = SMART_CODE.IDCARD(+)");
                IRecordSet oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());

                if (oRS.EOF || oRS.Fields("EMAIL_TITOLARE").IsNull)
                {
                    error = new Exception("Nessuna email titolare valida");
                    return null;
                }
                else
                    result.emailFrom = oRS.Fields("EMAIL_TITOLARE").Value.ToString();

                oRS.Close();

                oSB = new StringBuilder();
                oSB.Append("SELECT * FROM WTIC.PROPRIETA_RIEPILOGHI");
                oSB.Append(" WHERE PROPRIETA IN (");
                oSB.Append(" 'EMAIL_SMTP',");
                oSB.Append(" 'EMAIL_PORT_OUT',");
                oSB.Append(" 'EMAIL_USE_DEFAULT_CREDENTIALS',");
                oSB.Append(" 'EMAIL_USE_SSL',");
                oSB.Append(" 'EMAIL_USE_USER',");
                oSB.Append(" 'EMAIL_USE_PASSWORD')");
                oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    string proprieta = oRS.Fields("PROPRIETA").Value.ToString();
                    string valore = (oRS.Fields("VALORE").IsNull  ? "" : oRS.Fields("VALORE").Value.ToString());
                    switch (proprieta)
                    {
                        case "EMAIL_SMTP":
                            {
                                result.emailSmtp = valore;
                                break;
                            }
                        case "EMAIL_PORT_OUT":
                            {
                                int nPort = 0;
                                result.emailPort = nPort;
                                if (int.TryParse(valore, out nPort))
                                    result.emailPort = nPort;
                                break;
                            }
                        case "EMAIL_USE_DEFAULT_CREDENTIALS":
                            {
                                result.UseDefaultCredentials = (valore.Trim().ToUpper() == "ABILITATO");
                                break;
                            }
                        case "EMAIL_USE_SSL":
                            {
                                result.EnableSsl = (valore.Trim().ToUpper() == "ABILITATO");
                                break;
                            }
                        case "EMAIL_USE_USER":
                            {
                                result.emailUser = valore;
                                break;
                            }
                        case "EMAIL_USE_PASSWORD":
                            {
                                result.emailPassword = valore;
                                break;
                            }
                    }
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch (Exception ex)
            {
                error = ex;
                result = null;
            }
            return result;
        }


        private static string fileLog = "";
        public static void InitFileExecutionLog()
        {
            FileInfo oAppInfo = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            fileLog = oAppInfo.Directory.FullName + @"\log.txt";
        }
        public static void StartExecutionLog()
        {
            InitFileExecutionLog();
            System.IO.File.WriteAllText(fileLog, "");
        }
        public static void WriteAutoLogExecution(bool userInterface, string message, Exception error = null)
        {
            if (fileLog == "")
                InitFileExecutionLog();

            DateTime dNow = DateTime.Now;
            string cNow = string.Format("{0}-{1}-{2} {3}:{4}:{5}.{6}"
                                        , dNow.Year.ToString("0000")
                                        , dNow.Month.ToString("00")
                                        , dNow.Day.ToString("00")
                                        , dNow.Hour.ToString("00")
                                        , dNow.Minute.ToString("00")
                                        , dNow.Second.ToString("00")
                                        , dNow.Millisecond.ToString());
            System.IO.File.AppendAllText(fileLog, cNow + " " + message + "\r\n");
            if (error != null)
                System.IO.File.AppendAllText(fileLog, cNow + " " + error.ToString() + "\r\n");
        }

        public bool Esecuzione(out Exception oError, out bool findData, bool userInterface = false, Func<clsItemFullData> getDataFunc = null, libBordero.clsBorderoServiceConfig serviceConfig = null)
        {
            bool result = false;
            oError = null;
            findData = false;

            if (esecuzioneManuale)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmStartBordero());
            }
            else
            {
                WriteAutoLogExecution(userInterface, "Avvio");
                frmWait oFrmWait = null;
                
                if (userInterface)
                {
                    oFrmWait = new frmWait() { StartPosition = FormStartPosition.Manual };
                    oFrmWait.TopMost = true;
                    oFrmWait.Message = "Generazione...";
                    oFrmWait.Show();
                    oFrmWait.Location = new System.Drawing.Point(10, 10);
                    Application.DoEvents();
                }
                List<clsItemGruppo> gruppiEseguiti = new List<clsItemGruppo>();
                foreach (clsItemGruppo itemGruppoConfig in ConfigurazioneGruppi.Gruppi)
                {
                    clsItemGruppo itemGruppo = itemGruppoConfig.Clone();
                    gruppiEseguiti.Add(itemGruppo);
                    WriteAutoLogExecution(userInterface, string.Format("Gruppo: {0}", itemGruppo.Descrizione));

                    if (oFrmWait != null)
                        oFrmWait.Message = string.Format("Gruppo: {0}", itemGruppo.Descrizione);

                    if (itemGruppo.conn == null && serviceConfig == null)
                    {
                        if (getDataFunc == null)
                        {
                            clsItemGruppo itemFindConn = gruppiEseguiti.Find(g => !g.Equals(itemGruppo) &&
                                                                                  g.NomeConnessione == itemGruppo.NomeConnessione &&
                                                                                  g.conn != null);

                            if (itemFindConn != null)
                                itemGruppo.conn = itemFindConn.conn;

                            if (itemGruppo.conn == null)
                            {
                                WriteAutoLogExecution(userInterface, string.Format("connessione: {0}", itemGruppo.NomeConnessione));
                                try
                                {
                                    itemGruppo.conn = libBordero.Bordero.GetConnection(itemGruppo.NomeConnessione, out oError);
                                }
                                catch (Exception exConn)
                                {
                                    oError = new Exception("Errore di connessione " + exConn.Message);
                                    WriteAutoLogExecution(userInterface, "errore", oError);
                                    if (oFrmWait != null)
                                        oFrmWait.Message = $"Errore Gruppo: {itemGruppo.Descrizione} {oError.Message}";
                                }
                            }
                        }
                    }

                    if (oError != null)
                        break;

                    System.Data.DataSet oDataSetBordero = null;
                    Int64 IdRichiestaBordero = 0;
                    libBordero.Bordero.clsBordQuoteConfig config = null;
                    if (getDataFunc == null && serviceConfig == null)
                        config = libBordero.Bordero.GetConfigurazione(itemGruppo.conn, 1);

                    WriteAutoLogExecution(userInterface, "Elaborazione");
                    
                    if (oFrmWait != null)
                        oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} Elaborazione";

                    if (getDataFunc == null && serviceConfig == null)
                        libBordero.Bordero.DeleteDatiRichiestaBorderoFull(itemGruppo.conn, false, 1);

                    clsItemGruppo itemFindData = gruppiEseguiti.Find(g => !g.Equals(itemGruppo) 
                                                                          && g.NomeConnessione == itemGruppo.NomeConnessione 
                                                                          && g.modalitaGiorno == itemGruppo.modalitaGiorno 
                                                                          && g.Inizio == itemGruppo.Inizio
                                                                          && g.Fine == itemGruppo.Fine
                                                                          && g.conn != null 
                                                                          && g.datasetBordero != null);

                    if (itemFindData != null)
                    {
                        WriteAutoLogExecution(userInterface, "Dati in cache");

                        if (oFrmWait != null)
                            oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} Dati in cache";

                        itemGruppo.datasetBordero = itemFindData.datasetBordero.Copy();
                        oDataSetBordero = itemGruppo.datasetBordero;
                    }

                    if (oDataSetBordero == null)
                    {
                        WriteAutoLogExecution(userInterface, "Richiesta dati");

                        if (oFrmWait != null)
                            oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} Richiesta dati";

                        if (getDataFunc == null && serviceConfig == null)
                        {
                            itemGruppo.conn.BeginTransaction();

                            try
                            {
                                itemGruppo.datasetBordero = libBordero.Bordero.GetBordero(itemGruppo.conn, 1, itemGruppo.Inizio, itemGruppo.Fine, ref IdRichiestaBordero, out oError);
                                oDataSetBordero = itemGruppo.datasetBordero.Copy();
                                result = oDataSetBordero != null && oDataSetBordero.Tables != null && oDataSetBordero.Tables.Count > 0 &&
                                         ((oDataSetBordero.Tables.Contains("BORDERO_DATI") && oDataSetBordero.Tables["BORDERO_DATI"].Rows.Count > 0) ||
                                          (oDataSetBordero.Tables.Contains("BORDERO_ABBONAMENTI") && oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0)) && oError == null;


                                findData = (result && oError == null);

                                WriteAutoLogExecution(userInterface, string.Format("Dati:{0}", findData.ToString()), oError);
                            }
                            catch (Exception ex)
                            {
                                oError = ex;
                                result = false;
                                WriteAutoLogExecution(userInterface, "errore", oError);
                            }

                            itemGruppo.conn.RollBack();    // BUTTO SEMPRE VIA TUTTO

                            if (oError != null)
                                break;
                        }
                        else if (getDataFunc != null || serviceConfig != null)
                        {
                            clsItemFullData itemFullData = null;
                            if (getDataFunc != null)
                                itemFullData = getDataFunc();
                            else if (serviceConfig != null)
                                itemFullData = GetDataFromServiceInAuto(userInterface, itemGruppo, serviceConfig);

                            result = itemFullData != null && itemFullData.Result;
                            if (itemFullData.Result)
                            {
                                itemGruppo.datasetBordero = itemFullData.DataSetBordero;
                                IdRichiestaBordero = itemFullData.IdRichiestaBordero;
                                config = itemFullData.Config;
                                oDataSetBordero = itemGruppo.datasetBordero.Copy();
                            }


                            result = oDataSetBordero != null && oDataSetBordero.Tables != null && oDataSetBordero.Tables.Count > 0 &&
                                     ((oDataSetBordero.Tables.Contains("BORDERO_DATI") && oDataSetBordero.Tables["BORDERO_DATI"].Rows.Count > 0) ||
                                      (oDataSetBordero.Tables.Contains("BORDERO_ABBONAMENTI") && oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0)) && oError == null;


                            findData = (result && oError == null);

                            WriteAutoLogExecution(userInterface, string.Format("Dati:{0}", findData.ToString()), oError);

                            if (oFrmWait != null)
                                oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} {(findData ? "OK" : (oError == null ? "nessun dato" : "Errore " + oError.Message))}";

                            if (oError != null)
                                break;
                        }
                        
                    }

                    if (oError == null && findData)
                    {
                        WriteAutoLogExecution(userInterface, "creazione");
                        if (oFrmWait != null)
                            oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} creazione";

                        string[] aListaTabelleFiltro = new string[] { "BORDERO_FILT_GIORNI", "BORDERO_FILT_ORGANIZZATORI", "BORDERO_FILT_TIPI_EVENTO", "BORDERO_FILT_SALE", "BORDERO_FILT_TITOLI", "BORDERO_FILT_PRODUTTORI", "BORDERO_FILT_NOLEGGIATORI" };
                        string[] aListaCampiFiltro = new string[] { "", "CODICE_FISCALE_ORGANIZZATORE", "TIPO_EVENTO", "CODICE_LOCALE", "TITOLO_EVENTO", "PRODUTTORE", "NOLEGGIO" };
                        string[] aListaValoriFiltro = new string[] { "", itemGruppo.value_ORGANIZZATORE, itemGruppo.value_TIPO_EVENTO, itemGruppo.value_SALA, itemGruppo.value_TITOLO, itemGruppo.value_PRODUTTORE, itemGruppo.value_NOLEGGIATORE };
                        bool[] aListaMulti = new bool[] { itemGruppo.split_GIORNI, itemGruppo.split_ORGANIZZATORE, itemGruppo.split_TIPO_EVENTO, itemGruppo.split_SALA, itemGruppo.split_TITOLO, itemGruppo.split_PRODUTTORE, itemGruppo.split_NOLEGGIATORE };
                        List<clsItemPdfBordero> ItemsPdfBordero = new List<clsItemPdfBordero>();
                        string cTipoBorderoMultiplo = "";

                        int index = 0;
                        foreach (bool splitItem in aListaMulti)
                        {
                            if (splitItem)
                                cTipoBorderoMultiplo += (cTipoBorderoMultiplo == "" ? "" : ",") + aListaTabelleFiltro[index];
                            index += 1;
                        }

                        WriteAutoLogExecution(userInterface, string.Format("split {0}",(cTipoBorderoMultiplo.Contains(",") ? cTipoBorderoMultiplo.Split(',').Length.ToString() : "1")));

                        List<libBordero.Bordero.clsInfoDataSet> listaDati = null;

                        try
                        {
                            WriteAutoLogExecution(userInterface, string.Format("isFiltered:{0}", itemGruppo.isFiltered.ToString()));

                            bool lcontinue = true;
                            if (itemGruppo.isFiltered)
                            {
                                if (oFrmWait != null)
                                    oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} filtri";

                                List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();
                                index = 0;
                                int countFiltriImpostati = 0;
                                foreach (string tabFiltro in aListaTabelleFiltro)
                                {
                                    if (aListaValoriFiltro[index].Trim() != "")
                                    {
                                        countFiltriImpostati++;
                                        libBordero.Bordero.clsFilterBordero oFiltro = new libBordero.Bordero.clsFilterBordero(tabFiltro);
                                        foreach (DataRow oRow in oDataSetBordero.Tables[tabFiltro].Rows)
                                        {
                                            if (oRow[aListaCampiFiltro[index]].ToString() == aListaValoriFiltro[index])
                                                oFiltro.RigheFiltro.Add(oRow.ItemArray);
                                            else
                                            {
                                                List<string> multiFilter = new List<string>() { aListaValoriFiltro[index] };
                                                if (aListaValoriFiltro[index].Contains(";"))
                                                    multiFilter = aListaValoriFiltro[index].Split(';').ToList();
                                                bool appendRow = false;
                                                multiFilter.ForEach(xFilter =>
                                                {
                                                    if (!appendRow)
                                                        appendRow = oRow[aListaCampiFiltro[index]].ToString() == xFilter;
                                                    if (!appendRow)
                                                        appendRow = oRow[aListaCampiFiltro[index]].ToString().ToLower() == xFilter.ToLower();
                                                    if (!appendRow && xFilter.StartsWith("*") && xFilter.EndsWith("*"))
                                                        appendRow = oRow[aListaCampiFiltro[index]].ToString().ToLower().Contains(xFilter.ToLower().Replace("*",""));
                                                    if (!appendRow && xFilter.StartsWith("*") && !xFilter.EndsWith("*"))
                                                        appendRow = oRow[aListaCampiFiltro[index]].ToString().ToLower().EndsWith(xFilter.ToLower().Replace("*", ""));
                                                    if (!appendRow && !xFilter.StartsWith("*") && xFilter.EndsWith("*"))
                                                        appendRow = oRow[aListaCampiFiltro[index]].ToString().ToLower().StartsWith(xFilter.ToLower().Replace("*", ""));
                                                });
                                                if (appendRow)
                                                    oFiltro.RigheFiltro.Add(oRow.ItemArray);
                                            }
                                        }
                                        if (oFiltro.RigheFiltro.Count > 0)
                                            oFiltri.Add(oFiltro);
                                    }
                                    index += 1;
                                }

                                if (countFiltriImpostati > 0)
                                    lcontinue = oFiltri.Count > 0;

                                if (lcontinue && oFiltri.Count > 0)
                                    libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);
                            }

                            if (lcontinue)
                            {
                                if (oFrmWait != null)
                                    oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} creazione flusso";

                                WriteAutoLogExecution(userInterface, "stream");
                                listaDati = libBordero.Bordero.MultiBorderoStream(cTipoBorderoMultiplo, itemGruppo.Inizio, itemGruppo.Fine, oDataSetBordero, "", config);
                                WriteAutoLogExecution(userInterface, string.Format("listaDati {0}", listaDati.Count.ToString()));
                                foreach (libBordero.Bordero.clsInfoDataSet oInfoDataSet in listaDati)
                                {
                                    if (oInfoDataSet.StreamPdf != null)
                                    {
                                        byte[] data = oInfoDataSet.StreamPdf.ToArray();
                                        oInfoDataSet.StreamPdf.Dispose();
                                        string cDescFiltro = "";
                                        if (!string.IsNullOrEmpty(cTipoBorderoMultiplo))
                                        {
                                            foreach (KeyValuePair<string, string> itemMult in oInfoDataSet.InfoFiltri)
                                            {
                                                cDescFiltro += (string.IsNullOrEmpty(cDescFiltro) ? "" : "\r\n") + itemMult.Value.Trim();
                                            }
                                        }
                                        if (oInfoDataSet.DataOraEvento.HasValue)
                                            cDescFiltro += (string.IsNullOrEmpty(cDescFiltro) ? "" : "\r\n") + oInfoDataSet.DataOraEvento.Value.ToShortTimeString().Trim();
                                        clsItemPdfBordero itemFile = new clsItemPdfBordero();
                                        itemFile.Descrizione = cDescFiltro;
                                        itemFile.Buffer = data;
                                        itemFile.Table = oInfoDataSet.Dataset.Tables["BORDERO_DATI"];
                                        itemFile.TableAbbonamenti = oInfoDataSet.Dataset.Tables["BORDERO_ABBONAMENTI"];
                                        ItemsPdfBordero.Add(itemFile);
                                    }
                                }
                            }
                        }
                        catch (Exception exGen)
                        {
                            oError = exGen;
                            WriteAutoLogExecution(userInterface, "errore", oError);
                            if (oFrmWait != null)
                                oFrmWait.Message = $"Errore Gruppo: {itemGruppo.Descrizione} {oError.Message}";
                        }

                        if (oError == null && ItemsPdfBordero != null && ItemsPdfBordero.Count > 0)
                        {
                            WriteAutoLogExecution(userInterface, "items");
                            foreach (clsItemPdfBordero itemPdf in ItemsPdfBordero)
                            {
                                if (itemGruppo.ItemsPdfBordero == null)
                                    itemGruppo.ItemsPdfBordero = new List<clsItemPdfBordero>();
                                itemGruppo.ItemsPdfBordero.Add(itemPdf);
                            }
                        }

                        if (oError == null && itemGruppo.ItemsPdfBordero != null && itemGruppo.ItemsPdfBordero.Count > 0)
                        {
                            // salvo per dopo
                            foreach (clsItemPdfBordero itemPdf in itemGruppo.ItemsPdfBordero)
                            {
                                string name = "";
                                if (itemPdf.Descrizione.Trim() == "")
                                {
                                    if (itemGruppo.Inizio == itemGruppo.Fine)
                                    {
                                        name = itemGruppo.Inizio.ToLongDateString();
                                    }
                                    else
                                    {
                                        name = "da " + itemGruppo.Inizio.ToLongDateString() + " a " + itemGruppo.Fine.ToLongDateString();
                                    }
                                }
                                else
                                {
                                    name = itemPdf.Descrizione.Replace("\r\n", " ");
                                }
                                itemPdf.DescrizioneEstesa = $"{name}";
                            }

                            WriteAutoLogExecution(userInterface, string.Format("itemGruppo.ItemsPdfBordero.Count: {0}", itemGruppo.ItemsPdfBordero.Count.ToString()));
                            // invio per email
                            if (itemGruppo.emailDests != "")
                            {
                                if (oFrmWait != null)
                                    oFrmWait.Message = $"Gruppo: {itemGruppo.Descrizione} invio email";

                                WriteAutoLogExecution(userInterface, string.Format("itemGruppo.emailDests: {0}", itemGruppo.emailDests));
                                WriteAutoLogExecution(userInterface, string.Format("itemGruppo.useEmailCodiceSistema: {0}", itemGruppo.useEmailCodiceSistema.ToString()));

                                string userName = "";
                                string password = "";

                                if (itemGruppo.useEmailCodiceSistema && serviceConfig == null)
                                {
                                    clsCredenzialiCodiceSistema credenzialiCodiceSistema = null;
                                    clsItemGruppo itemFindEmailCS = gruppiEseguiti.Find(g => !g.Equals(itemGruppo) &&
                                                                                             g.NomeConnessione == itemGruppo.NomeConnessione &&
                                                                                             g.CredenzialiCodiceSistema != null);
                                    if (itemFindEmailCS != null)
                                    {
                                        credenzialiCodiceSistema = itemFindEmailCS.CredenzialiCodiceSistema;
                                    }
                                    else
                                    {
                                        credenzialiCodiceSistema = GetCredenzialiCodiceSistema(itemGruppo.conn, out oError);
                                    }

                                    if (oError != null)
                                        break;

                                    itemGruppo.CredenzialiCodiceSistema = credenzialiCodiceSistema;
                                    itemGruppo.emailFrom = credenzialiCodiceSistema.emailFrom;
                                    itemGruppo.emailSmtp = credenzialiCodiceSistema.emailSmtp;
                                    itemGruppo.emailPort = credenzialiCodiceSistema.emailPort;
                                    itemGruppo.UseDefaultCredentials = credenzialiCodiceSistema.UseDefaultCredentials;
                                    itemGruppo.EnableSsl = credenzialiCodiceSistema.EnableSsl;
                                    itemGruppo.emailUser = credenzialiCodiceSistema.emailUser;
                                    itemGruppo.emailPassword = credenzialiCodiceSistema.emailPassword;

                                    userName = itemGruppo.emailUser;
                                    password = itemGruppo.emailPassword;
                                }
                                else
                                {
                                    userName = libBordero.Bordero.DecryptGen(itemGruppo.emailUser);
                                    password = libBordero.Bordero.DecryptGen(itemGruppo.emailPassword);
                                }

                                Dictionary<string, byte[]> lista_pdf = new Dictionary<string, byte[]>();
                                Dictionary<string, DataTable> lista_csv = new Dictionary<string, DataTable>();
                                string body = (itemGruppo.generazionePdf && itemGruppo.ItemsPdfBordero.Count > 0 ? "Lista dei file Pdf:\r\n" : "");
                                foreach (clsItemPdfBordero itemPdf in itemGruppo.ItemsPdfBordero)
                                {
                                    string name = "";
                                    if (itemPdf.Descrizione.Trim() == "")
                                    {
                                        if (itemGruppo.Inizio == itemGruppo.Fine)
                                        {
                                            name = $"{itemGruppo.Descrizione} {itemGruppo.Inizio.ToLongDateString()}";
                                            body += (body == "" ? "" : "\r\n") + itemGruppo.Inizio.ToLongDateString();
                                        }
                                        else
                                        {
                                            name = $"{itemGruppo.Descrizione} da {itemGruppo.Inizio.ToLongDateString()} a {itemGruppo.Fine.ToLongDateString()}";
                                            body += (body == "" ? "" : "\r\n") + "da " + itemGruppo.Inizio.ToLongDateString() + " a " + itemGruppo.Fine.ToLongDateString();
                                        }
                                    }
                                    else
                                    {
                                        name = $"{itemGruppo.Descrizione} {itemPdf.Descrizione.Replace("\r\n", " ")}";
                                        body += (body == "" ? "" : "\r\n\r\n") + itemPdf.Descrizione;
                                    }

                                    string attachmentName = "";
                                    if (itemGruppo.generazionePdf)
                                    {
                                        //attachmentName = "PDF " + string.Format("Bordero {0}{1}", name, (itemGruppo.ItemsPdfBordero.Count == 1 ? "" : " " + (lista_pdf.Count + 1).ToString()));
                                        attachmentName = $"PDF Bordero {name}{(itemGruppo.ItemsPdfBordero.Count == 1 ? "" : " " + (lista_pdf.Count + 1).ToString())}";
                                        lista_pdf.Add(attachmentName, itemPdf.Buffer);
                                        WriteAutoLogExecution(userInterface, string.Format("attachmentName: {0}", attachmentName));
                                    }

                                    if (itemGruppo.generazioneCsv)
                                    {
                                        //attachmentName = "CSV " + string.Format("Bordero {0}{1}", name, (itemGruppo.ItemsPdfBordero.Count == 1 ? "" : " " + (lista_pdf.Count + 1).ToString()));
                                        attachmentName = $"CSV Bordero {name}{(itemGruppo.ItemsPdfBordero.Count == 1 ? "" : " " + (lista_pdf.Count + 1).ToString())}";
                                        lista_csv.Add(attachmentName, itemPdf.Table);
                                        WriteAutoLogExecution(userInterface, string.Format("attachmentName: {0}", attachmentName));
                                        attachmentName = "CSV " + string.Format("Bordero Abbonamenti {0}{1}", name, (itemGruppo.ItemsPdfBordero.Count == 1 ? "" : " " + (lista_pdf.Count + 1).ToString()));
                                        lista_csv.Add(attachmentName, itemPdf.TableAbbonamenti);
                                        WriteAutoLogExecution(userInterface, string.Format("attachmentName: {0}", attachmentName));
                                    }


                                }

                                if (itemGruppo.generazioneCsv)
                                    body += (body == "" ? "" : "\r\n\r\n") + "files CSV dati.";

                                InvioEmail(itemGruppo.emailFrom, itemGruppo.emailSmtp, itemGruppo.emailPort, itemGruppo.UseDefaultCredentials, itemGruppo.EnableSsl, userName, password, itemGruppo.emailDests, itemGruppo.Descrizione, body, lista_pdf, lista_csv);
                            }

                            if (!string.IsNullOrEmpty(itemGruppo.NomeDll))
                            {
                                ISenderData sender = FinderInterfaceSenderData.GetSenderData(itemGruppo.NomeDll);
                                if (sender != null)
                                    sender.Send(itemGruppo.conn, serviceConfig, itemGruppo, userInterface);
                            }
                        }

                        if (oError != null)
                        {
                            break;
                        }
                    }

                }

                WriteAutoLogExecution(userInterface, "chiusure connessioni");

                ConfigurazioneGruppi.Gruppi.ForEach(g =>
                {
                    if (g.conn != null)
                    {
                        g.conn.Close();
                        g.conn.Dispose();
                        g.conn = null;
                    }
                });

                if (oFrmWait != null)
                {
                    oFrmWait.Close();
                    oFrmWait.Dispose();
                }

                if (userInterface && oError != null)
                    MessageBox.Show(string.Format("Esecuzione termina con errori;\r\n{0}", oError.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (userInterface && oError == null && !findData)
                    MessageBox.Show("Esecuzione termina con successo ma senza alcun dato.", "Esecuzione terminata", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (userInterface && oError == null && findData)
                {
                    bool lContinueToView = true;

                    while (lContinueToView)
                    {
                        Dictionary<object, string> itemsGruppi = new Dictionary<object, string>();
                        foreach (clsItemGruppo itemGruppo in gruppiEseguiti)
                        {
                            itemsGruppi.Add(itemGruppo, string.Format("{0} ({1})", itemGruppo.Descrizione, (itemGruppo.ItemsPdfBordero != null ? itemGruppo.ItemsPdfBordero.Count.ToString() : "0")));
                        }
                        object resultView = null;
                        lContinueToView = frmMenuBordero.GetMenu("Gruppi", itemsGruppi, ref resultView);
                        if (lContinueToView)
                        {
                            clsItemGruppo gruppoSelezionato = (clsItemGruppo)resultView;
                            Dictionary<object, string> itemsSubGruppo = new Dictionary<object, string>();
                            foreach (clsItemPdfBordero itemPdf in gruppoSelezionato.ItemsPdfBordero)
                            {
                                itemsSubGruppo.Add(itemPdf, itemPdf.DescrizioneEstesa);
                            }
                            while (lContinueToView)
                            {
                                object resultSubView = null;
                                lContinueToView = frmMenuBordero.GetMenu("Elementi del gruppo " + gruppoSelezionato.Descrizione, itemsSubGruppo, ref resultSubView);
                                if (lContinueToView)
                                {
                                    System.IO.MemoryStream stream = new System.IO.MemoryStream(((clsItemPdfBordero)resultSubView).Buffer);
                                    frmViewPdf frmPdf = new frmViewPdf(stream);
                                    frmPdf.Text = ((clsItemPdfBordero)resultSubView).Descrizione;
                                    frmPdf.ShowDialog();
                                    frmPdf.Close();
                                    frmPdf.Dispose();
                                }
                            }
                            lContinueToView = gruppiEseguiti.Count > 1;
                        }
                    }
                }
            }
            WriteAutoLogExecution(userInterface, string.Format("FINE: {0}", result.ToString()));
            return result;
        }

        public clsItemFullData GetDataFromServiceInAuto(bool userInterface, clsItemGruppo itemGruppo, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            clsItemFullData data = new clsItemFullData();
            System.Data.DataSet oDataSetBordero = null;
            Int64 IdRichiestaBordero = 0;
            libBordero.Bordero.clsBordQuoteConfig config = null;

            WriteAutoLogExecution(userInterface, "Richiesta dati da servizio in automatico");

            bool result = false;
            Exception oError = null;
            if (LoginServiceInAuto(userInterface, itemGruppo, serviceConfig))
            {
                if (GetDataFromService(userInterface, out oDataSetBordero, out IdRichiestaBordero, out config, serviceConfig, itemGruppo))
                {
                    data.DataSetBordero = oDataSetBordero;
                    data.IdRichiestaBordero = IdRichiestaBordero;
                    data.Config = config;
                    data.Result = true;
                }
                else
                    data.Result = false;
            }
            else
                data.Result = false;
            return data;
        }

        private bool LoginServiceInAuto(bool userInterface, clsItemGruppo gruppo, libBordero.clsBorderoServiceConfig serviceConfig)
        {
            string errorMessage = "";
            bool resultLogin = false;

            WriteAutoLogExecution(userInterface, "Login da servizio in automatico");

            if (!string.IsNullOrEmpty(gruppo.serviceUserInfo))
            {
                libBordero.PayLoadRiepiloghi oRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<libBordero.PayLoadRiepiloghi>(libBordero.Bordero.DecryptGen(gruppo.serviceUserInfo));
                libBordero.ResponseRiepiloghi oResponse = libBordero.clsBorderoServiceConfigUtility.Login(oRequest, serviceConfig.Url);

                errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Token");
                if (string.IsNullOrEmpty(errorMessage))
                {
                    string token = oResponse.Data.Token;
                    oRequest = new libBordero.PayLoadRiepiloghi() { Token = token };
                    oResponse = libBordero.clsBorderoServiceConfigUtility.GetAccount(oRequest, serviceConfig.Url);

                    libBordero.clsAccount account = null;

                    errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Account");
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        WriteAutoLogExecution(userInterface, "Login da servizio errore: " + errorMessage);
                    }
                    else
                    {
                        libBordero.clsCodiceSistema codiceSistema = null;
                        if (oResponse.Data.Account.CodiciSistema != null)
                        {
                            account = oResponse.Data.Account;
                            if (account.CodiciSistema.Count == 1 || !userInterface)
                                codiceSistema = oResponse.Data.Account.CodiciSistema[0];
                            else
                            {
                                Dictionary<object, string> Opzioni = new Dictionary<object, string>();
                                account.CodiciSistema.ForEach(cs =>
                                {
                                    Opzioni.Add(cs, cs.Descrizione);
                                });
                                object result = frmGetGenericData.MenuGenerico(account.Cognome + " " + account.Nome, Opzioni);
                                if (result != null)
                                    codiceSistema = (libBordero.clsCodiceSistema)result;
                            }
                        }
                        else
                        {
                            errorMessage = "Nessun codice sistema valido";
                            WriteAutoLogExecution(userInterface, "Login da servizio errore: " + errorMessage);
                        }

                        if (codiceSistema != null)
                        {
                            oRequest = new libBordero.PayLoadRiepiloghi() { Token = token, CodiceSistema = codiceSistema.CodiceSistema };
                            oResponse = libBordero.clsBorderoServiceConfigUtility.GetTokenCodiceSistema(oRequest, serviceConfig.Url);
                            errorMessage = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "Token");
                            if (!string.IsNullOrEmpty(errorMessage))
                            {
                                WriteAutoLogExecution(userInterface, "Login da servizio errore: " + errorMessage);
                            }
                            else
                            {
                                serviceConfig.Account = account;
                                serviceConfig.CodiceSistema = codiceSistema;
                                if (serviceConfig.CodiceSistema != null && serviceConfig.CodiceSistema.CodiceSistema.Length > 8 && serviceConfig.CodiceSistema.CodiceSistema.Contains(".") && serviceConfig.CodiceSistema.CodiceSistema.Split('.')[0].Length == 8)
                                {
                                    long idCinema = long.Parse(serviceConfig.CodiceSistema.CodiceSistema.Split('.')[1]);
                                    string descrCinema = (account != null && account.CodiciSistema != null && account.CodiciSistema.Count > 0 && account.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == serviceConfig.CodiceSistema.CodiceSistema) != null ? account.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == serviceConfig.CodiceSistema.CodiceSistema).Descrizione : "");
                                    serviceConfig.CodiceSistema.MultiCinema = new libBordero.clsMultiCinema() { IdCinema = idCinema, Descrizione = descrCinema };
                                }
                                serviceConfig.Token = oResponse.Data.Token;
                                resultLogin = true;
                                WriteAutoLogExecution(userInterface, "Login da servizio: OK");
                            }
                        }
                    }
                    
                }
            }
            else
                WriteAutoLogExecution(userInterface, "Login da servizio errore: credenziali non impostate");
            return resultLogin;
        }

        private bool GetDataFromService(bool userInterface, out System.Data.DataSet oDataSetBordero, out Int64 IdRichiestaBordero, out libBordero.Bordero.clsBordQuoteConfig config, libBordero.clsBorderoServiceConfig _serviceConfig, clsItemGruppo itemGruppo)
        {
            oDataSetBordero = null;
            IdRichiestaBordero = 0;
            config = null;

            WriteAutoLogExecution(userInterface, "Lettura dati da servizio in automatico");

            bool result = false;
            Exception oError = null;

            PayLoadRiepiloghi oRequest = null;
            ResponseRiepiloghi oResponse = null;

            oRequest = new PayLoadRiepiloghi() { Token = _serviceConfig.Token };
            oResponse = libBordero.clsBorderoServiceConfigUtility.GetConfigurazioneFromService(oRequest, _serviceConfig.Url);
            string err = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "ConfigurazioneBordero");

            List<long> quoteManualiEscluse = null;
            List<long> quoteManualiTe = null;
            List<long> quoteManualiSi = null;

            if (string.IsNullOrEmpty(err))
                config = oResponse.Data.ConfigurazioneBordero;

            DateTime dInizio = itemGruppo.Inizio;
            DateTime dFine = itemGruppo.Fine;
            oRequest = new PayLoadRiepiloghi() { Token = _serviceConfig.Token, DataInizio = dInizio, DataFine = dFine, QuoteManualiEscluse = quoteManualiEscluse, QuoteManualiTe = quoteManualiTe, QuoteManualiSi = quoteManualiSi };
            oResponse = libBordero.clsBorderoServiceConfigUtility.CalcBordero(oRequest, _serviceConfig.Url);
            string errCalc = libBordero.clsBorderoServiceConfigUtility.GetDescResponseError(oResponse, true, "DataSetBordero");
            if (!string.IsNullOrEmpty(errCalc))
            {
                oError = new Exception(errCalc);
                WriteAutoLogExecution(userInterface, "Lettura dati da servizio errore: " + errCalc);
            }
            else
            {
                oDataSetBordero = oResponse.Data.DataSetBordero;
                config = oResponse.Data.ConfigurazioneBordero;
                IdRichiestaBordero = oResponse.Data.IdRichiestaBordero;

                if (oDataSetBordero != null && oDataSetBordero.Tables != null && oDataSetBordero.Tables.Count > 0 &&
                    ((oDataSetBordero.Tables.Contains("BORDERO_DATI") && oDataSetBordero.Tables["BORDERO_DATI"].Rows.Count > 0)
                     ||
                     (oDataSetBordero.Tables.Contains("BORDERO_ABBONAMENTI") && oDataSetBordero.Tables["BORDERO_ABBONAMENTI"].Rows.Count > 0)))
                {
                    result = true;
                    WriteAutoLogExecution(userInterface, "Lettura dati da servizio: OK");
                }
                else
                    WriteAutoLogExecution(userInterface, "Lettura dati da servizio: nessun dato");
            }
            if (oError != null)
            {
                result = false;
            }
            return result;
        }

        public static bool InvioEmail(string emailMitt, string smtp, int port, bool useDefaultCredential, bool enableSsl, string user, string password, string emailDests, string subject, string body, Dictionary<string, byte[]> lista_pdf, Dictionary<string, DataTable> lista_csv)
        {
            WriteAutoLogExecution(false, "invio email");

            bool result = false;
            System.Net.Mail.SmtpClient oClient = null;
            if (port == 0)
            {
                oClient = new System.Net.Mail.SmtpClient(smtp);
            }
            else
            {
                oClient = new System.Net.Mail.SmtpClient(smtp, port);
            }

            oClient.UseDefaultCredentials = useDefaultCredential;
            oClient.EnableSsl = enableSsl;

            if (user.Trim() != "" && password.Trim() != "")
            {
                oClient.Credentials = new System.Net.NetworkCredential(user, password);
            }
            oClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            oMessage.From = new System.Net.Mail.MailAddress(emailMitt);
            
            if (!emailDests.Contains(";"))
                oMessage.To.Add(emailDests);
            else
            {
                foreach (string dest in emailDests.Split(';'))
                    oMessage.To.Add(dest);
            }
            oMessage.IsBodyHtml = false;
            oMessage.Subject = $"Invio automatico Bordero {subject}";
            if (body.Trim() == "" && lista_pdf != null && lista_pdf.Count > 0)
            {
                foreach (KeyValuePair<string, byte[]> item in lista_pdf)
                {
                    body += (body == "" ? "" : "\r\n") + item.Key;
                }
            }
            oMessage.Body = body;

            if ((lista_pdf != null && lista_pdf.Count > 0) || (lista_csv != null && lista_csv.Count > 0))
            {
                if (lista_pdf != null)
                {
                    foreach (KeyValuePair<string, byte[]> item in lista_pdf)
                    {
                        System.Net.Mail.Attachment oAttachment = new System.Net.Mail.Attachment(new MemoryStream(item.Value), item.Key, MediaTypeNames.Application.Pdf);
                        oMessage.Attachments.Add(oAttachment);
                    }
                }

                if (lista_csv != null)
                {
                    foreach (KeyValuePair<string, DataTable> item in lista_csv)
                    {
                        clsExportCsv exportCsv = new clsExportCsv();
                        Exception error = null;
                        exportCsv.WriteDataTable(item.Value, out error);
                        System.Net.Mail.Attachment oAttachment = new System.Net.Mail.Attachment(new MemoryStream(exportCsv.buffer), item.Key + ".csv", MediaTypeNames.Application.Octet);
                        oMessage.Attachments.Add(oAttachment);
                    }
                }
            }

            WriteAutoLogExecution(false, "send");
            try
            {
                oClient.Send(oMessage);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                WriteAutoLogExecution(false, "errore invio", ex);
            }
            
            WriteAutoLogExecution(false, "fine invio");

            foreach (System.Net.Mail.Attachment attachment in oMessage.Attachments)
            {
                attachment.ContentStream.Dispose();
                attachment.Dispose();
            }

            oMessage.Dispose();
            oClient.Dispose();

            return result;
        }
    }
}
