<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes" method="xml" encoding="US-ASCII"/>
	<xsl:decimal-format name="it"  grouping-separator="" decimal-separator="." />
  <xsl:variable name="testata" select="//testata" />
  <xsl:variable name="opere" select="//opere" />
  <xsl:variable name="biglietti" select="//biglietti" />
    <xsl:template match="/">
	  <DistintaIncasso>
          <Titolare>
            <CodiceSistema>
              <xsl:value-of select="$testata/codice_sistema"/>
            </CodiceSistema>
          </Titolare>
		  <Organizzatore>
			  <Nome>
				  <xsl:value-of select="$testata/denominazione_organizzatore"/>
			  </Nome>
			  <CodiceFiscale>
				  <xsl:value-of select="$testata/codice_fiscale_organizzatore"/>
			  </CodiceFiscale>
			  <Tipo>
				  <xsl:attribute name="valore">
					  <xsl:value-of select="'G'"/>
				  </xsl:attribute>
			  </Tipo>
		  </Organizzatore>
		  <Locale>
			  <Nome>
				  <xsl:value-of select="$testata/denominazione_locale"/>
			  </Nome>
			  <CodiceSIAE>
				  <xsl:value-of select="$testata/codice_locale"/>
			  </CodiceSIAE>
			  <Impianto>
				  <xsl:value-of select="$testata/impianto"/>
			  </Impianto>
			  <Comune>
				  <xsl:value-of select="$testata/comune"/>
			  </Comune>
			  <Provincia>
				  <xsl:value-of select="$testata/provincia"/>
			  </Provincia>
			  <Nazione>
				  <xsl:value-of select="$testata/nazione"/>
			  </Nazione>
		  </Locale>
		  <Evento>
			  <Data>
				  <xsl:value-of select="$testata/data_evento"/>
			  </Data>
			  <OraInizio>
				  <xsl:value-of select="$testata/ora_inizio"/>
			  </OraInizio>
			  <NumeroSpettacoli>
				  <xsl:value-of select="$testata/numero_spettacoli"/>
			  </NumeroSpettacoli>
			  <Opere>
				  <xsl:apply-templates select="opere" />
		      </Opere>
		  </Evento>
		  <QuadroB>
			  <Biglietti>
				  <xsl:apply-templates select="biglietti" />
			  </Biglietti>
			  <Riepilogo>
				  <TotaleEmessi>
					  <xsl:value-of select="$testata/totale_emessi"/>
				  </TotaleEmessi>
				  <CorrispettivoTitoli>
					  <xsl:value-of select="$testata/totale_lordo"/>
				  </CorrispettivoTitoli>
				  <IvaTitoli>
					  <xsl:value-of select="$testata/totale_iva"/>
				  </IvaTitoli>
				  <NettoTitoli>
					  <xsl:value-of select="$testata/totale_netto"/>
				  </NettoTitoli>
				  <CorrispettivoPrevendita>
					  <xsl:value-of select="$testata/totale_prevendita"/>
				  </CorrispettivoPrevendita>
				  <IvaPrevendita>
					  <xsl:value-of select="$testata/totale_iva_prevendita"/>
				  </IvaPrevendita>
				  <NumeroOmaggiEccedenti>
					  <xsl:value-of select="$testata/omaggi_eccedenti"/>
				  </NumeroOmaggiEccedenti>
			  </Riepilogo>
		  </QuadroB>
		  <CalcoloNettissimo>
                <NettoOmaggiEccedenti>
					<xsl:value-of select="$testata/netto_omaggi_eccedenti"/>
				</NettoOmaggiEccedenti>
			    <NettoPrestazioniComplementari>
					<xsl:value-of select="$testata/netto_prestazioni_complementari"/>
				</NettoPrestazioniComplementari>
			    <NettoTitoli>
					<xsl:value-of select="$testata/base_dem"/>
				</NettoTitoli>
			    <DirittiAutore>
					<xsl:value-of select="$testata/dem"/>
				</DirittiAutore>
		  </CalcoloNettissimo>
	  </DistintaIncasso>
	</xsl:template>
  <xsl:template match="opere">
    <Opera>
      <Titolo>
        <xsl:value-of select="titolo"/>
      </Titolo>
      <Tipo>
        <xsl:value-of select="tipo_evento"/>
      </Tipo>
      <Noleggiatore>
        <xsl:value-of select="noleggiatore"/>
      </Noleggiatore>
      <ProduttoreCinema>
        <xsl:value-of select="produttore"/>
      </ProduttoreCinema>
      <Nazionalita>
        <xsl:value-of select="nazionalita"/>
      </Nazionalita>
      <Supporto>
        <xsl:value-of select="supporto"/>
      </Supporto>
	  <SiacId></SiacId>
    </Opera>
  </xsl:template>
  <xsl:template match="biglietti">
    <Biglietto>
		<IvaPreassolta>
			<xsl:value-of select="titolo_iva_preassolta"/>
		</IvaPreassolta>
		<TipoTitolo>
			<xsl:value-of select="tipo_titolo"/>
		</TipoTitolo>
		<Descrizione>
			<xsl:value-of select="descrizione"/>
		</Descrizione>
		<TotaleEmessi>
            <xsl:value-of select="qta_emessi"/>
        </TotaleEmessi>
		<CorrispettivoTitoli>
            <xsl:value-of select="corrispettivo_titolo"/>
        </CorrispettivoTitoli>
		<IvaTitoli>
			<xsl:value-of select="iva_titolo"/>
		</IvaTitoli>
		<NettoTitoli>
			<xsl:value-of select="corrispettivo_netto"/>
		</NettoTitoli>
        <CorrispettivoPrevendita>
            <xsl:value-of select="corrispettivo_prevendita"/>
        </CorrispettivoPrevendita>
		<IvaPrevendita>
            <xsl:value-of select="iva_prevendita"/>
        </IvaPrevendita>
    </Biglietto>
</xsl:stylesheet>