﻿
using libBordero;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;
using static System.Net.Mime.MediaTypeNames;

namespace libCedas
{
    public class SenderCedasConfig
    {
        public string UrlAuth { get; set; } = "https://megadwh.winticstellar.com/webapi/ExpJdk/";
        public string UrlCedas { get; set; } = "https://www.cineadmin.com";

        public string emailDests = "";
        public bool useEmailCodiceSistema = true;
        public string emailFrom = "";
        public string emailSmtp = "";
        public int emailPort = 0;
        public string emailUser = "";
        public string emailPassword = "";
        public bool UseDefaultCredentials = true;
        public bool EnableSsl = false;
    }
    public class SenderCedas : libBordero.ISenderData
    {
        public string Description()
        {
            return "Esportazione Cedas";
        }

        public bool TestMode { get; set; } = false;
        private string User = "creaweb";
        private string Password = "AawULHf1R2a7PCjuB4hR738qfevIRqil2coA1DmiTEXjBunVPLh1hxYo2AZZfPKI";

        public string Impianto { get; set; }

        private libBordero.clsBorderoServiceConfig serviceConfig { get; set; }

        public static SenderCedasConfig SenderCedasConfig { get; set; }


        public clsItemGruppo GetGruppo(IConnection conn)
        {
            clsItemGruppo gruppo = new clsItemGruppo()
            {
                NomeDll = "libCedas.dll",
                NomeConnessione = "CINEMA",
                Descrizione = Description(),
                split_GIORNI = true,
                split_ORGANIZZATORE = true,
                split_SALA = true,
                split_TIPO_EVENTO = true,
                split_TITOLO = true,
                split_NOLEGGIATORE = false,
                split_PRODUTTORE = false,
                value_NOLEGGIATORE = "",
                value_ORGANIZZATORE = "",
                value_PRODUTTORE = "",
                value_SALA = "",
                value_TIPO_EVENTO = "01",
                value_TITOLO = "",
                generazionePdf = true,
                generazioneCsv = false,
                modalitaGiorno = "IERI"
            };
            return gruppo;
        }

        public string NomeDll()
        {
            return GetGruppo(null).NomeDll;
        }

        public void Send(IConnection conn, libBordero.clsBorderoServiceConfig _serviceConfig, clsItemGruppo gruppo, bool userInterface, Func<string, bool> delegateMessages = null)
        {
            List<string> emailBody = new List<string>();
            
            emailBody.Add($"Invio CEDAS {DateTime.Now.ToString()}");

            if (conn != null)
                CheckLocalTables(conn);
            else
                serviceConfig = _serviceConfig;

            SenderCedasConfig = null;
            try
            {
                if (File.Exists("cedas.json"))
                    SenderCedasConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<SenderCedasConfig>(File.ReadAllText("cedas.json"));
            }
            catch (Exception ex)
            {
                SenderCedasConfig = null;
            }
            if (conn != null)
            {
                this.Impianto = GetImpianto(conn);
                if (string.IsNullOrEmpty(this.Impianto))
                {
                    if (userInterface)
                        MessageBox.Show("Impianto non identificato", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (delegateMessages != null)
                        delegateMessages.Invoke("Impianto non identificato");
                    emailBody.Add("Impianto non identificato");
                }
            }
            else
            {
                if (gruppo.datasetBordero != null
                    && gruppo.datasetBordero.Tables.Contains("BORDERO_CINEMA")
                    && gruppo.datasetBordero.Tables["BORDERO_CINEMA"].Rows.Count > 0
                    && gruppo.datasetBordero.Tables["BORDERO_CINEMA"].Rows[0]["DESCR"] != null
                    && gruppo.datasetBordero.Tables["BORDERO_CINEMA"].Rows[0]["DESCR"] != DBNull.Value
                    && !string.IsNullOrEmpty(gruppo.datasetBordero.Tables["BORDERO_CINEMA"].Rows[0]["DESCR"].ToString()))
                    this.Impianto = gruppo.datasetBordero.Tables["BORDERO_CINEMA"].Rows[0]["DESCR"].ToString();
                else
                {
                    if (userInterface)
                        MessageBox.Show("Impianto non identificato", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (delegateMessages != null)
                        delegateMessages.Invoke("Impianto non identificato");
                    emailBody.Add("Impianto non identificato");
                }
            }

            List<DistintaIncasso> distinteIncasso = new List<DistintaIncasso>();
            if (!string.IsNullOrEmpty(this.Impianto))
                distinteIncasso = Cedas.GetDistinteIncasso(gruppo, this.Impianto);

            if (distinteIncasso == null || distinteIncasso.Count == 0)
            {
                if (userInterface && (distinteIncasso == null || distinteIncasso.Count == 0))
                    MessageBox.Show("Nessuna distinta da inviare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (delegateMessages != null)
                    delegateMessages.Invoke("Nessuna distinta da inviare");
                emailBody.Add("Nessuna distinta da inviare");
            }
            
            Exception error = null;

            if (distinteIncasso != null && distinteIncasso.Count > 0)
            {
                emailBody.Add($"Distinte da inviare:{distinteIncasso.Count.ToString()}");
                distinteIncasso = distinteIncasso.OrderBy(distintaIncasso => distintaIncasso.Locale.CodiceSIAE + distintaIncasso.Evento.Data + distintaIncasso.Evento.OraInizio).ToList();
                distinteIncasso.ForEach(distintaIncasso =>
                {
                    if (error == null)
                    {
                        string descrizioneDistinta = "";
                        try
                        {
                            descrizioneDistinta = $"organizzatore: {distintaIncasso.Organizzatore.CodiceFiscale} {distintaIncasso.Organizzatore.Nome}" +
                                                  (distintaIncasso.Evento != null ? $"\r\nevento: {distintaIncasso.Evento.Data} {distintaIncasso.Evento.OraInizio} n.eventi: {distintaIncasso.Evento.NumeroSpettacoli}" : "");
                            if (distintaIncasso.Evento != null 
                                && distintaIncasso.Evento.Opere != null
                                && distintaIncasso.Evento.Opere.Opera != null 
                                && distintaIncasso.Evento.Opere.Opera.Count > 0)
                            {
                                distintaIncasso.Evento.Opere.Opera.ForEach(op =>
                                {
                                    descrizioneDistinta += " " + op.Titolo;
                                    if (distintaIncasso.Evento.Opere.Opera.Count > 1)
                                        descrizioneDistinta += "\r\n";
                                });
                            }
                        }
                        catch (Exception)
                        {
                        }
                        distintaIncasso.TestMode = this.TestMode;
                        List<DistintaIncasso> distintePrecedenti = distinteIncasso.Where(d => d.Locale.CodiceSIAE.Equals(distintaIncasso.Locale.CodiceSIAE) && d.Evento.Data.Equals(distintaIncasso.Evento.Data) && d.Evento.DataOraPrimoEvento < distintaIncasso.Evento.DataOraPrimoEvento).ToList();
                        distintaIncasso.Progressivo = (distintePrecedenti == null || distintePrecedenti.Count == 0 ? 1 : distintePrecedenti.Max(d => d.Progressivo) + 1);
                        distintaIncasso.Xml = Cedas.SerializeDistintaIncasso(distintaIncasso, out error);
                        distintaIncasso.XmlGzip = Compress(System.Text.Encoding.UTF8.GetBytes(distintaIncasso.Xml));
                        distintaIncasso.PdfGzip = Compress(distintaIncasso.PdfBytes);
                        distintaIncasso.Auth = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this.User}:{this.Password}"));
                        if (error != null)
                            emailBody.Add($"Errore sulla distinta di incasso: {descrizioneDistinta} {error.Message}");
                    }
                });

                if (error == null)
                {
                    distinteIncasso.ForEach(distintaIncasso =>
                    {
                        long cedasId = 0;
                        if (error == null)
                        {
                            string descrizioneDistinta = "";
                            try
                            {
                                try
                                {
                                    descrizioneDistinta = $"organizzatore: {distintaIncasso.Organizzatore.CodiceFiscale} {distintaIncasso.Organizzatore.Nome}" +
                                                          (distintaIncasso.Evento != null ? $"\r\nevento: {distintaIncasso.Evento.Data} {distintaIncasso.Evento.OraInizio} n.eventi: {distintaIncasso.Evento.NumeroSpettacoli}" : "");
                                    if (distintaIncasso.Evento != null
                                        && distintaIncasso.Evento.Opere != null
                                        && distintaIncasso.Evento.Opere.Opera != null
                                        && distintaIncasso.Evento.Opere.Opera.Count > 0)
                                    {
                                        distintaIncasso.Evento.Opere.Opera.ForEach(op =>
                                        {
                                            descrizioneDistinta += " " + op.Titolo;
                                            if (distintaIncasso.Evento.Opere.Opera.Count > 1)
                                                descrizioneDistinta += "\r\n";
                                        });
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                            catch (Exception )
                            {

                            }


                            try
                            {
                                Exception errorCREA = null;
                                cedasId = CedasLogCreate(conn, serviceConfig, distintaIncasso, out errorCREA);
                                SendToCrea(distintaIncasso, out errorCREA);
                            }
                            catch (Exception)
                            {

                            }

                            

                            bool sentCedasXml = SendXml(distintaIncasso, out error);

                            if (error != null || !sentCedasXml)
                                emailBody.Add($"Errore invio distinta: {descrizioneDistinta} {(error != null ? error.Message : "")}");
                            else
                                emailBody.Add($"Invio distinta OK: {descrizioneDistinta}");

                            CedasLogUpdate(conn, serviceConfig, "xml", cedasId, error);

                            if (error == null)
                            {
                                bool sentCedasPdf = SendPdf(distintaIncasso, out error);

                                if (error != null || !sentCedasXml)
                                    emailBody.Add($"Errore invio distinta PDF: {descrizioneDistinta} {(error != null ? error.Message : "")}");
                                else
                                    emailBody.Add($"Invio distinta PDF OK: {descrizioneDistinta}");

                                CedasLogUpdate(conn, serviceConfig, "pdf", cedasId, error);
                            }
                        }
                    });
                }

                if (userInterface)
                    MessageBox.Show($"Invio incassi terminato {(error == null ? "con SUCCESSO" : "con ERRORI\r\n" + error.Message )}", (error == null ? "Informazione" : "Errore"), MessageBoxButtons.OK, (error == null ? MessageBoxIcon.Information : MessageBoxIcon.Error));
                else if (delegateMessages != null)
                    delegateMessages.Invoke($"Invio incassi terminato {(error == null ? "con SUCCESSO" : "con ERRORI\r\n" + error.Message)}");

                emailBody.Add($"Invio incassi terminato {(error == null ? "con SUCCESSO" : "con ERRORI\r\n" + error.Message)}");
            }

            try
            {
                if (!string.IsNullOrEmpty(SenderCedasConfig.emailDests) && emailBody.Count > 0)
                {
                    if (SenderCedasConfig.useEmailCodiceSistema)
                    {
                        clsCredenzialiCodiceSistema credenzialiCodiceSistema = GetCredenzialiCodiceSistema(conn, out error);
                        SenderCedasConfig.emailFrom = credenzialiCodiceSistema.emailFrom;
                        SenderCedasConfig.emailSmtp = credenzialiCodiceSistema.emailSmtp;
                        SenderCedasConfig.emailPort = credenzialiCodiceSistema.emailPort;
                        SenderCedasConfig.UseDefaultCredentials = credenzialiCodiceSistema.UseDefaultCredentials;
                        SenderCedasConfig.EnableSsl = credenzialiCodiceSistema.EnableSsl;
                        SenderCedasConfig.emailUser = credenzialiCodiceSistema.emailUser;
                        SenderCedasConfig.emailPassword = credenzialiCodiceSistema.emailPassword;
                    }
                    string body = "";
                    emailBody.ForEach(x => body += (x.StartsWith("\r") ? "" : "\r\n") + x);
                    InvioEmail(SenderCedasConfig.emailFrom, SenderCedasConfig.emailSmtp, SenderCedasConfig.emailPort, SenderCedasConfig.UseDefaultCredentials, SenderCedasConfig.EnableSsl, SenderCedasConfig.emailUser, SenderCedasConfig.emailPassword, SenderCedasConfig.emailDests, body, null, null);
                }
            }
            catch (Exception)
            {
            }
        }

        public static bool InvioEmail(string emailMitt, string smtp, int port, bool useDefaultCredential, bool enableSsl, string user, string password, string emailDests, string body, Dictionary<string, byte[]> lista_pdf, Dictionary<string, DataTable> lista_csv)
        {
            bool result = false;
            System.Net.Mail.SmtpClient oClient = null;
            if (port == 0)
            {
                oClient = new System.Net.Mail.SmtpClient(smtp);
            }
            else
            {
                oClient = new System.Net.Mail.SmtpClient(smtp, port);
            }

            oClient.UseDefaultCredentials = useDefaultCredential;
            oClient.EnableSsl = enableSsl;

            if (user.Trim() != "" && password.Trim() != "")
            {
                oClient.Credentials = new System.Net.NetworkCredential(user, password);
            }
            oClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            oMessage.From = new System.Net.Mail.MailAddress(emailMitt);

            if (!emailDests.Contains(";"))
                oMessage.To.Add(emailDests);
            else
            {
                foreach (string dest in emailDests.Split(';'))
                    oMessage.To.Add(dest);
            }
            oMessage.IsBodyHtml = false;
            oMessage.Subject = "Invio automatico Bordero";
            if (body.Trim() == "" && lista_pdf != null && lista_pdf.Count > 0)
            {
                foreach (KeyValuePair<string, byte[]> item in lista_pdf)
                {
                    body += (body == "" ? "" : "\r\n") + item.Key;
                }
            }
            oMessage.Body = body;

            if ((lista_pdf != null && lista_pdf.Count > 0) || (lista_csv != null && lista_csv.Count > 0))
            {
                if (lista_pdf != null)
                {
                    foreach (KeyValuePair<string, byte[]> item in lista_pdf)
                    {
                        System.Net.Mail.Attachment oAttachment = new System.Net.Mail.Attachment(new MemoryStream(item.Value), item.Key, MediaTypeNames.Application.Pdf);
                        oMessage.Attachments.Add(oAttachment);
                    }
                }

                if (lista_csv != null)
                {
                    foreach (KeyValuePair<string, DataTable> item in lista_csv)
                    {
                        clsExportCsv exportCsv = new clsExportCsv();
                        Exception error = null;
                        exportCsv.WriteDataTable(item.Value, out error);
                        System.Net.Mail.Attachment oAttachment = new System.Net.Mail.Attachment(new MemoryStream(exportCsv.buffer), item.Key + ".csv", MediaTypeNames.Application.Octet);
                        oMessage.Attachments.Add(oAttachment);
                    }
                }
            }

            try
            {
                oClient.Send(oMessage);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            foreach (System.Net.Mail.Attachment attachment in oMessage.Attachments)
            {
                attachment.ContentStream.Dispose();
                attachment.Dispose();
            }

            oMessage.Dispose();
            oClient.Dispose();

            return result;
        }


        public static clsCredenzialiCodiceSistema GetCredenzialiCodiceSistema(IConnection conn, out Exception error)
        {
            error = null;
            clsCredenzialiCodiceSistema result = new clsCredenzialiCodiceSistema();
            try
            {
                StringBuilder oSB = new StringBuilder();

                oSB.Append("SELECT ");
                oSB.Append(" SMART_CARD.EMAIL_TITOLARE");
                oSB.Append(" FROM SMART_CS, SMART_CARD, SMART_CODE ");
                oSB.Append(" WHERE ");
                oSB.Append(" SMART_CS.ENABLED = 1 AND ");
                oSB.Append(" SMART_CARD.ENABLED = 1 AND ");
                oSB.Append(" SMART_CARD.CODICE_SISTEMA = SMART_CS.CODICE_SISTEMA AND ");
                oSB.Append(" SMART_CARD.IDCARD = SMART_CODE.IDCARD(+)");
                IRecordSet oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());

                if (oRS.EOF || oRS.Fields("EMAIL_TITOLARE").IsNull)
                {
                    error = new Exception("Nessuna email titolare valida");
                    return null;
                }
                else
                    result.emailFrom = oRS.Fields("EMAIL_TITOLARE").Value.ToString();

                oRS.Close();

                oSB = new StringBuilder();
                oSB.Append("SELECT * FROM WTIC.PROPRIETA_RIEPILOGHI");
                oSB.Append(" WHERE PROPRIETA IN (");
                oSB.Append(" 'EMAIL_SMTP',");
                oSB.Append(" 'EMAIL_PORT_OUT',");
                oSB.Append(" 'EMAIL_USE_DEFAULT_CREDENTIALS',");
                oSB.Append(" 'EMAIL_USE_SSL',");
                oSB.Append(" 'EMAIL_USE_USER',");
                oSB.Append(" 'EMAIL_USE_PASSWORD')");
                oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    string proprieta = oRS.Fields("PROPRIETA").Value.ToString();
                    string valore = (oRS.Fields("VALORE").IsNull ? "" : oRS.Fields("VALORE").Value.ToString());
                    switch (proprieta)
                    {
                        case "EMAIL_SMTP":
                            {
                                result.emailSmtp = valore;
                                break;
                            }
                        case "EMAIL_PORT_OUT":
                            {
                                int nPort = 0;
                                result.emailPort = nPort;
                                if (int.TryParse(valore, out nPort))
                                    result.emailPort = nPort;
                                break;
                            }
                        case "EMAIL_USE_DEFAULT_CREDENTIALS":
                            {
                                result.UseDefaultCredentials = (valore.Trim().ToUpper() == "ABILITATO");
                                break;
                            }
                        case "EMAIL_USE_SSL":
                            {
                                result.EnableSsl = (valore.Trim().ToUpper() == "ABILITATO");
                                break;
                            }
                        case "EMAIL_USE_USER":
                            {
                                result.emailUser = valore;
                                break;
                            }
                        case "EMAIL_USE_PASSWORD":
                            {
                                result.emailPassword = valore;
                                break;
                            }
                    }
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch (Exception ex)
            {
                error = ex;
                result = null;
            }
            return result;
        }

        private string GetImpianto(IConnection conn)
        {
            string result = "";
            try
            {
                DataTable tb = conn.ExecuteQuery("SELECT RAG_SOC FROM CINEMA.DATIFISSI");
                result = (tb != null && tb.Rows != null && tb.Rows.Count > 0 ? tb.Rows[0]["RAG_SOC"].ToString() : "");
                tb.Dispose();
            }
            catch (Exception)
            {
            }
            return result;
        }



        private bool SendXml(DistintaIncasso distintaIncasso, out Exception Error)
        {
            Error = null;
            try
            {
                byte[] buffer = distintaIncasso.XmlGzip;
                string url = (SenderCedasConfig != null && !string.IsNullOrEmpty(SenderCedasConfig.UrlCedas) ? SenderCedasConfig.UrlCedas : "https://www.cineadmin.com");
                var options = new RestClientOptions(url)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest(distintaIncasso.UrlDistinta, Method.Post);
                request.AddHeader("Content-Encoding", "gzip");
                //request.AddHeader("Authorization", "Basic Y3JlYXdlYjpBYXdVTEhmMVIyYTdQQ2p1QjRoUjczOHFmZXZJUnFpbDJjb0ExRG1pVEVYakJ1blZQTGgxaHhZbzJBWlpmUEtJ");
                request.AddHeader("Authorization", $"Basic {distintaIncasso.Auth}");
                request.AddHeader("Content-Type", "application/xml");
                request.AddParameter("application/xml", buffer, ParameterType.RequestBody);
                var rresponse = client.ExecuteAsync(request);
                rresponse.Wait();
                RestResponse response = rresponse.Result;
                Error = GetExceptionFromCedas(response.Content, distintaIncasso, null);
            }
            catch (Exception exWeb)
            {
                Error = GetExceptionFromCedas(exWeb.Message, distintaIncasso, exWeb);
            }
            return Error == null;
        }

        private bool SendPdf(DistintaIncasso distintaIncasso, out Exception Error)
        {
            Error = null;
            try
            {
                byte[] buffer = distintaIncasso.PdfGzip;
                string url = (SenderCedasConfig != null && !string.IsNullOrEmpty(SenderCedasConfig.UrlCedas) ? SenderCedasConfig.UrlCedas : "https://www.cineadmin.com");
                var options = new RestClientOptions(url)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest(distintaIncasso.UrlDistinta, Method.Post);
                request.AddHeader("Content-Encoding", "gzip");
                //request.AddHeader("Authorization", "Basic Y3JlYXdlYjpBYXdVTEhmMVIyYTdQQ2p1QjRoUjczOHFmZXZJUnFpbDJjb0ExRG1pVEVYakJ1blZQTGgxaHhZbzJBWlpmUEtJ");
                request.AddHeader("Authorization", $"Basic {distintaIncasso.Auth}");
                request.AddHeader("Content-Type", "application/pdf");
                request.AddParameter("application/pdf", buffer, ParameterType.RequestBody);
                var rresponse = client.ExecuteAsync(request);
                rresponse.Wait();
                RestResponse response = rresponse.Result;
                Error = GetExceptionFromCedas(response.Content, distintaIncasso, null);
            }
            catch (Exception exWeb)
            {
                Error = GetExceptionFromCedas(exWeb.Message, distintaIncasso, exWeb);
            }
            return Error == null;
        }

        private static Exception GetExceptionFromCedas(string responseContent, DistintaIncasso distintaIncasso, Exception ex)
        {
            Exception Error = null;
            if (ex != null)
                Error = ex;
            else if (responseContent == null || string.IsNullOrEmpty(responseContent) || string.IsNullOrWhiteSpace(responseContent))
            {
                Error = null;
            }
            else
            {

                string code = "";
                string text = "";
                try
                {
                    jSonWebError jsonError = JsonConvert.DeserializeObject<jSonWebError>(responseContent);
                    code = jsonError.code;
                    text = jsonError.description;
                    if (responseContent.Contains("200"))
                        Error = null;
                    else if (responseContent.Contains("201"))
                        Error = null;
                    else if (responseContent.Contains("400"))
                        Error = new Exception($"Richiesta a Cedas non valida codice:{code} messaggio:{text}");
                    else if (responseContent.Contains("401"))
                        Error = new Exception($"Utente e password non validi per invio a Cedas  codice:{code} messaggio:{text}");
                    else if (responseContent.Contains("403"))
                        Error = null;
                    else if (responseContent.Contains("404"))
                        Error = new Exception($"Url Cedas non valido  codice:{code} messaggio:{text}\r\n{distintaIncasso.UrlDistinta}");
                    else if (responseContent.Contains("405"))
                        Error = new Exception($"Metodo Cedas non valido  codice:{code} messaggio:{text}\r\n{distintaIncasso.UrlDistinta}");
                    else if (responseContent.Contains("409"))
                        Error = new Exception($"Errore di conflitto il file non può essere aggiornato o creato codice:{code} messaggio:{text}");
                    else if (responseContent.Contains("422"))
                        Error = new Exception($"Errore: Pdf non valido  codice:{code} messaggio:{text}");
                    else if (responseContent.Contains("500"))
                        Error = new Exception($"Errore: generale sul server Cedas  codice:{code} messaggio:{text}");
                    else
                        Error = ex;
                }
                catch (Exception EX)
                {
                    MessageBox.Show(EX.Message + "\r\n" + responseContent + (ex == null ? "" : "\r\n" + ex.Message + "\r\n" + ex.ToString()), "ERRORE LETTURA RISPOSTA CEDAS");
                    Error = EX;
                }
            }
            return Error;
        }

        /*
          200 - OK, file aggiornato (Metodo PUT)
          201 - OK, file salvato (Metodo POST)
          400 - Bad Request, url invalido, header mancanti o invalidi
          401 – Unauthorized, metodo di autenticazione mancante o non valido; nome utente e/o password errati
          403 - Forbidden, utente riconosciuto ma non autorizzato per il locale ****************** DA CONSIDERARE COME INVIATO OK (come fosse 201) PERCHE' AVVIENE SEMPRE LA PRIMA VOLTA x UTENTE/PASSWORD e CODICE LOCALE
          404 - Not Found, Risorsa non trovata (Metodo PUT o url sbagliato)
          405 - Method not allowed, metodo non permesso o non implementato.
          409 - Conflict, il file non puo' essere aggiornato (PUT) oppure non puo' essere creato perche' gia' esistente (POST)
          422 - Unprocessable entity, file pdf o xml non validi
          500 - Internal Server Error / api system down
          In caso di codici 4xx e 5xx viene aggiunto un payload in json (Content-Type="application/json; charset=UTF-8”)
          esempio 1:
          { "code": 400, "description": "Bad Content-Type: text/html" } esempio 2:
          { "code": 422, "description": "Error parsing xml:64:3: fatal: The element type "DistintaIncasso" must be terminated by the matching end-tag" }

        */

        public static byte[] Compress(byte[] bytStreamToCompress)
        {
            try
            {
                MemoryStream msOutputMemoryStream = new MemoryStream();
                System.Text.UTF8Encoding aeEncoding = new System.Text.UTF8Encoding();
                ICSharpCode.SharpZipLib.GZip.GZipOutputStream zos = new ICSharpCode.SharpZipLib.GZip.GZipOutputStream(msOutputMemoryStream);

                //int compLevel= zos.GetLevel();

                zos.Write(bytStreamToCompress, 0, bytStreamToCompress.Length);

                //zos.SetLevel(level);

                zos.Finish();
                zos.Close();

                return msOutputMemoryStream.ToArray();

            }
            catch (Exception e)
            {
                return null;
            }

        }
        private static string GetWebRequest(string parUrl, object parRequest, out Exception Error)
        {
            Error = null;
            string result = string.Empty;

            try
            {
                HttpWebResponse resp = null;

                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // comparable to modern browsers


                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(parUrl);
                req.CookieContainer = new CookieContainer();
                req.Credentials = CredentialCache.DefaultNetworkCredentials;
                req.UserAgent = ": Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
                req.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Anonymous;
                req.Method = "POST";
                req.Accept = "application/json";
                req.ContentType = "application/json";
                req.Timeout = 20000;

                string json = JsonConvert.SerializeObject(parRequest);

                using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    resp = (HttpWebResponse)req.GetResponse();
                    Encoding enc = Encoding.ASCII;
                    using (System.IO.StreamReader responseStream = new System.IO.StreamReader(resp.GetResponseStream(), enc))
                    {
                        result = responseStream.ReadToEnd();
                        responseStream.Close();
                    }
                    resp.Close();
                }

            }
            catch (Exception ex)
            {
                Error = ex;
            }
            return result;
        }
        private static Response getResponse(string url, Request request, out Exception error, string description = "")
        {
            error = null;
            Stopwatch sw = Stopwatch.StartNew();
            Response response = new Response();
            try
            {
                string value = GetWebRequest(url, request, out error);
                if (value != null && !string.IsNullOrEmpty(value))
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(value);
                else
                {
                    response.Success = (error == null);
                    if (error != null)
                        response.Message = error.Message;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Errore di connessione Servizio\r\n" + ex.Message;
            }
            sw.Stop();
            return response;
        }
        private static DistintaIncasso SendToCrea(DistintaIncasso distintaIncasso, out Exception error)
        {
            DistintaIncasso distintaToCrea = null;
            distintaToCrea = JsonConvert.DeserializeObject<DistintaIncasso>(JsonConvert.SerializeObject(distintaIncasso));
            if (distintaToCrea.PdfBytes != null && distintaToCrea.PdfGzip != null)
                distintaToCrea.PdfBytes = null;

            string url = (SenderCedasConfig != null && !string.IsNullOrEmpty(SenderCedasConfig.UrlCedas) ? SenderCedasConfig.UrlAuth : @"https://megadwh.winticstellar.com/webapi/ExpJdk/ImportCedas");
            //string url = @"https://megadwh.winticstellar.com/webapi/ExpJdk/ImportCedas";
            //////url = @"http://192.168.0.158:62040/ExpJdk/ImportCedas";
            var payload = new Request() { DistintaIncasso = distintaToCrea };
            DistintaIncasso distintaResult = null;
            error = null;
            Response response = getResponse(url, payload, out error);
            if (response != null && response.Data != null && response.Success)
                distintaResult = Newtonsoft.Json.JsonConvert.DeserializeObject<DistintaIncasso>(Newtonsoft.Json.JsonConvert.SerializeObject(response.Data));
            else
            {
                if (error == null)
                {
                    if (response == null) error = new Exception("Nessuna risposta.");
                    else if (response != null && response.Data == null) error = new Exception("Nessuna risposta.");
                }
            }
            return distintaResult;
        }

        private static void CheckLocalTables(IConnection conn)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER = 'CINEMA' AND TABLE_NAME = 'CEDAS_BORDERO'");
            DataTable dt = conn.ExecuteQuery(sb.ToString());

            bool create = dt == null || dt.Rows == null || dt.Rows.Count == 0;

            if (create)
            {
                sb = new StringBuilder();
                sb.Append("CREATE TABLE CINEMA.CEDAS_BORDERO");
                sb.Append(" (");
                sb.Append("   CEDAS_ID NUMBER NOT NULL");
                sb.Append(" , CODICE_LOCALE VARCHAR2(13) NOT NULL");
                sb.Append(" , DATA VARCHAR2(8) NOT NULL");
                sb.Append(" , PROGRESSIVO NUMBER NOT NULL");
                sb.Append(" , ORA_INIZIO VARCHAR2(4) NOT NULL");
                sb.Append(" , TITOLO VARCHAR2(255) NOT NULL");
                sb.Append(" , TIMESTAMP_INS TIMESTAMP DEFAULT SYSTIMESTAMP");
                sb.Append(" , XML CLOB");
                sb.Append(" , SENT_XML_ERROR VARCHAR2(255)");
                sb.Append(" , SENT_PDF_ERROR VARCHAR2(255)");
                sb.Append(" , CONSTRAINT PK_CEDAS_BORDERO PRIMARY KEY (CEDAS_ID) ");
                sb.Append(" )");
                conn.ExecuteNonQuery(sb.ToString());

                sb = new StringBuilder();
                sb.Append("CREATE SEQUENCE CINEMA.SEQ_CEDAS_ID START WITH 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCYCLE CACHE 20 NOORDER");
                conn.ExecuteNonQuery(sb.ToString());
            }
        }
        public static long CedasLogCreate(IConnection conn, libBordero.clsBorderoServiceConfig serviceConfig, DistintaIncasso distintaIncasso, out Exception exception)
        {
            long cedasId = 0;
            exception = null;
            try
            {

                StringBuilder sb = null;
                clsParameters parameters = null;
                DataTable dt = null;
                bool append = false;

                if (conn != null)
                {

                    try
                    {
                        parameters = new clsParameters();
                        parameters.Add(":pCODICE_LOCALE", distintaIncasso.Locale.CodiceSIAE);
                        parameters.Add(":pDATA", distintaIncasso.Evento.Data);
                        parameters.Add(":pPROGRESSIVO", distintaIncasso.Progressivo);
                        parameters.Add(":pORA_INIZIO", distintaIncasso.Evento.OraInizio);
                        parameters.Add(":pTITOLO", distintaIncasso.Evento.Opere.Opera[0].Titolo);
                        parameters.Add(":pXML", distintaIncasso.Xml);

                        sb = new StringBuilder();
                        sb.Append("INSERT INTO CINEMA.CEDAS_BORDERO");
                        sb.Append(" (");
                        sb.Append("    CEDAS_ID");
                        sb.Append("  , CODICE_LOCALE");
                        sb.Append("  , DATA");
                        sb.Append("  , PROGRESSIVO");
                        sb.Append("  , ORA_INIZIO");
                        sb.Append("  , TITOLO");
                        sb.Append("  , XML");
                        sb.Append(" )");
                        sb.Append(" VALUES");
                        sb.Append(" (");
                        sb.Append("    CINEMA.SEQ_CEDAS_ID.NEXTVAL");
                        sb.Append("  , :pCODICE_LOCALE");
                        sb.Append("  , :pDATA");
                        sb.Append("  , :pPROGRESSIVO");
                        sb.Append("  , :pORA_INIZIO");
                        sb.Append("  , :pTITOLO");
                        sb.Append("  , :pXML");
                        sb.Append(" )");
                        cedasId = long.Parse(conn.ExecuteNonQuery(sb.ToString(), "CEDAS_ID", parameters).ToString());
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }
                }
                else if (serviceConfig != null)
                {
                    PayLoadRiepiloghi oRequest = null;
                    ResponseRiepiloghi oResponse = null;

                    oRequest = new PayLoadRiepiloghi()
                    {
                        Token = serviceConfig.Token,
                        BorderoCedas = new libBordero.Bordero.clsBorderoCedas()
                        {
                            CODICE_LOCALE = distintaIncasso.Locale.CodiceSIAE,
                            DATA = distintaIncasso.Evento.Data,
                            PROGRESSIVO = distintaIncasso.Progressivo,
                            ORA_INIZIO = distintaIncasso.Evento.OraInizio,
                            TITOLO = distintaIncasso.Evento.Opere.Opera[0].Titolo,
                            XML = distintaIncasso.Xml
                        }
                    };
                    oResponse = libBordero.clsBorderoServiceConfigUtility.RequestToRiepiloghi(oRequest, serviceConfig.Url, "BorderoInsertCedas");
                    cedasId = oResponse != null && oResponse.Data != null && oResponse.Data.BorderoCedas != null ? oResponse.Data.BorderoCedas.CEDAS_ID : 0;
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {

            }
            return cedasId;
        }
        public static void CedasLogUpdate(IConnection conn, libBordero.clsBorderoServiceConfig serviceConfig, string tipo, long cedasId, Exception error)
        {
            try
            {

                StringBuilder sb = null;
                clsParameters parameters = null;

                if (conn != null)
                {
                    try
                    {
                        parameters = new clsParameters();
                        parameters.Add(":pCEDAS_ID", cedasId);
                        parameters.Add(":pVALUE", error == null ? "" : error.Message);

                        //SENT_XML_ERROR
                        sb = new StringBuilder();
                        sb.Append("UPDATE CINEMA.CEDAS_BORDERO ");
                        if (tipo == "xml")
                            sb.Append("SET SENT_XML_ERROR = :pVALUE ");
                        else
                            sb.Append("SET SENT_PDF_ERROR = :pVALUE ");
                        sb.Append(" WHERE CEDAS_ID = :pCEDAS_ID");
                        conn.ExecuteNonQuery(sb.ToString(), parameters).ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else if (serviceConfig != null)
                {
                    PayLoadRiepiloghi oRequest = null;
                    ResponseRiepiloghi oResponse = null;

                    oRequest = new PayLoadRiepiloghi()
                    {
                        Token = serviceConfig.Token,
                        BorderoCedas = new libBordero.Bordero.clsBorderoCedas()
                        {
                            CEDAS_ID = cedasId,
                            TIPO = tipo
                        }
                    };

                    if (tipo == "xml" && error != null)
                        oRequest.BorderoCedas.SENT_XML_ERROR = error.Message;
                    else if (tipo != "xml" && error != null)
                        oRequest.BorderoCedas.SENT_PDF_ERROR = error.Message;
                    oResponse = libBordero.clsBorderoServiceConfigUtility.RequestToRiepiloghi(oRequest, serviceConfig.Url, "BorderoUpdateCedas");
                }
            }
            catch (Exception ex)
            {
                string errGen = "";
            }
            finally
            {
            }
        }
    }

    public static class Cedas
    {
        #region Dati

        public static string GetNumValue(decimal value)
        {
            string result = value.ToString("#0.00").Replace(",",".");
            return result;
        }

        public static List<DistintaIncasso> GetDistinteIncasso(clsItemGruppo gruppo, string impianto)
        {
            List<DistintaIncasso> distinteIncasso = new List<DistintaIncasso>();
            gruppo.ItemsPdfBordero.ForEach(itemEvento =>
            {
                if (itemEvento.Table != null && itemEvento.Table.Rows.Count > 0)
                {
                    DistintaIncasso distintaIncasso = GetDistintaIncasso(itemEvento, impianto);
                    
                    if (distintaIncasso != null && distintaIncasso.QuadroB.Biglietti.Biglietto.Count > 0)
                        distinteIncasso.Add(distintaIncasso);
                }
            });
            return distinteIncasso;
        }


        private static DistintaIncasso GetDistintaIncasso(clsItemPdfBordero itemEvento, string impianto)
        {
            DataRow firstRow = itemEvento.Table.Rows[0];
            if (firstRow["TIPO_EVENTO"].ToString() != "01")
                return null;
            long B_totQta = (firstRow["B_TOT_QTA"] != null && firstRow["B_TOT_QTA"] != System.DBNull.Value ? long.Parse(firstRow["B_TOT_QTA"].ToString()) : 0);
            decimal B_corrispettivoTitoli = (firstRow["B_TOT_CORRISPETTIVO_TITOLO"] != null && firstRow["B_TOT_CORRISPETTIVO_TITOLO"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_CORRISPETTIVO_TITOLO"].ToString()) : 0) +
                                            (firstRow["B_TOT_CORRISPETTIVO_FIGURATIVO"] != null && firstRow["B_TOT_CORRISPETTIVO_FIGURATIVO"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_CORRISPETTIVO_FIGURATIVO"].ToString()) : 0);

            decimal B_ivaTitoli = (firstRow["B_TOT_IVA_TITOLO"] != null && firstRow["B_TOT_IVA_TITOLO"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_IVA_TITOLO"].ToString()) : 0) +
                                  (firstRow["B_TOT_IVA_FIGURATIVA"] != null && firstRow["B_TOT_IVA_FIGURATIVA"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_IVA_FIGURATIVA"].ToString()) : 0);
                                  //+ (firstRow["B_IVA_OMAGGI_ECCEDENTI"] != null && firstRow["B_IVA_OMAGGI_ECCEDENTI"] != System.DBNull.Value ? decimal.Parse(firstRow["B_IVA_OMAGGI_ECCEDENTI"].ToString()) : 0);

            decimal B_corrispettivoPrevendita = (firstRow["B_TOT_CORRISPETTIVO_PREVENDITA"] != null && firstRow["B_TOT_CORRISPETTIVO_PREVENDITA"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_CORRISPETTIVO_PREVENDITA"].ToString()) : 0);
            decimal B_ivaPrevendita = (firstRow["B_TOT_IVA_PREVENDITA"] != null && firstRow["B_TOT_IVA_PREVENDITA"] != System.DBNull.Value ? decimal.Parse(firstRow["B_TOT_IVA_PREVENDITA"].ToString()) : 0);
            decimal B_nettoTitoli = System.Math.Max(0, B_corrispettivoTitoli - B_ivaTitoli);
            DateTime? dataOraPrimoEvento = null;

            if (firstRow["INIZIO_PRIMO_SPETTACOLO"] != null && firstRow["INIZIO_PRIMO_SPETTACOLO"] != System.DBNull.Value)
                dataOraPrimoEvento = (DateTime)firstRow["INIZIO_PRIMO_SPETTACOLO"];

            DistintaIncasso distintaIncasso = new DistintaIncasso()
            {
                Titolare = new Titolare()
                {
                    CodiceSistema = firstRow["CODICE_SISTEMA"].ToString()
                },
                Organizzatore = new Organizzatore()
                {
                    Nome = firstRow["DENOMINAZIONE_ORGANIZZATORE"].ToString(),
                    CodiceFiscale = firstRow["CODICE_FISCALE_ORGANIZZATORE"].ToString(),
                    Tipo = "G"
                },
                Locale = new Locale()
                {
                    Nome = (firstRow["DENOMINAZIONE_LOCALE"] != null && firstRow["DENOMINAZIONE_LOCALE"] != System.DBNull.Value ? firstRow["DENOMINAZIONE_LOCALE"].ToString() : ""),
                    CodiceSIAE = (firstRow["CODICE_LOCALE"] != null && firstRow["CODICE_LOCALE"] != System.DBNull.Value ? firstRow["CODICE_LOCALE"].ToString() : ""),
                    Impianto = impianto,
                    Comune = (firstRow["COMUNE"] != null && firstRow["COMUNE"] != System.DBNull.Value ? firstRow["COMUNE"].ToString() : ""),
                    Provincia = (firstRow["PROVINCIA"] != null && firstRow["PROVINCIA"] != System.DBNull.Value ? firstRow["PROVINCIA"].ToString() : ""),
                    Nazione = "IT"
                },
                Evento = new Evento()
                {
                    DataOraPrimoEvento = dataOraPrimoEvento,
                    NumeroSpettacoli = (firstRow["QTA_EVENTI"] != null && firstRow["QTA_EVENTI"] != System.DBNull.Value ? int.Parse(firstRow["QTA_EVENTI"].ToString()) : 0),
                    Opere = new Opere()
                    { 
                        Opera = new List<Opera>() {
                            new Opera()
                            {
                                Titolo = (firstRow["TITOLO_EVENTO"] != null && firstRow["TITOLO_EVENTO"] != System.DBNull.Value ? firstRow["TITOLO_EVENTO"].ToString() : ""),
                                Tipo = (firstRow["TIPO_EVENTO"] != null && firstRow["TIPO_EVENTO"] != System.DBNull.Value ? firstRow["TIPO_EVENTO"].ToString() : ""),
                                Supporto = "CL",
                                Nazionalita = (firstRow["NAZIONE"] != null && firstRow["NAZIONE"] != System.DBNull.Value ? firstRow["NAZIONE"].ToString() : ""),
                                Noleggiatore = (firstRow["NOLEGGIO"] != null && firstRow["NOLEGGIO"] != System.DBNull.Value ? firstRow["NOLEGGIO"].ToString() : ""),
                                ProduttoreCinema = (firstRow["PRODUTTORE"] != null && firstRow["PRODUTTORE"] != System.DBNull.Value ? firstRow["PRODUTTORE"].ToString() : "")
                            }
                        }
                    }
                },
                QuadroB = new QuadroB()
                {
                    Biglietti = new Biglietti() { Biglietto = new List<Biglietto>() },
                    Riepilogo = new Riepilogo
                    {
                        TotaleEmessi = B_totQta,
                        ValCorrispettivoTitoli = B_corrispettivoTitoli,
                        ValIvaTitoli = B_ivaTitoli,
                        ValNettoTitoli = B_nettoTitoli,
                        ValCorrispettivoPrevendita = B_corrispettivoPrevendita,
                        ValIvaPrevendita = B_ivaPrevendita,
                        NumeroOmaggiEccedenti = (firstRow["B_QTA_OMAGGI_DEM"] != null && firstRow["B_QTA_OMAGGI_DEM"] != System.DBNull.Value ? long.Parse(firstRow["B_QTA_OMAGGI_DEM"].ToString()) : 0)
                    }
                },
                CalcoloNettissimo = new CalcoloNettissimo()
                {
                    ValNettoOmaggiEccedenti = (firstRow["B_OMAGGI_DEM"] != null && firstRow["B_OMAGGI_DEM"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(firstRow["B_OMAGGI_DEM"].ToString()), 2) : 0),
                    ValNettoPrestazioniComplementari = (firstRow["B_SUPPL_DEM"] != null && firstRow["B_SUPPL_DEM"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(firstRow["B_SUPPL_DEM"].ToString()), 2) : 0),
                    ValNettoTitoli = (firstRow["BASE_DEM_BORDERO"] != null && firstRow["BASE_DEM_BORDERO"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(firstRow["BASE_DEM_BORDERO"].ToString()), 2) : 0),
                    ValDirittiAutore = (firstRow["DEM_BORDERO"] != null && firstRow["DEM_BORDERO"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(firstRow["DEM_BORDERO"].ToString()), 2) : 0)
                },
                PdfBytes = itemEvento.Buffer
            };

            foreach (DataRow row in itemEvento.Table.Rows)
            {
                long qtaEmessi = (row["TOT_QTA"] != null && row["TOT_QTA"] != System.DBNull.Value ? long.Parse(row["TOT_QTA"].ToString()) : 0);
                string ivaPreassolta = (row["TITOLO_IVA_PREASSOLTA"] != null && row["TITOLO_IVA_PREASSOLTA"] != System.DBNull.Value ? row["TITOLO_IVA_PREASSOLTA"].ToString() : "");

                if (qtaEmessi > 0 && (ivaPreassolta == "N" || ivaPreassolta == "F" || ivaPreassolta == "B"))
                {
                    decimal corrispettivoTitoli = 0; 
                    decimal ivaTitoli = 0;
                    decimal nettoTitoli = 0;
                    decimal corrispettivoPrevendita = 0;
                    decimal ivaPrevendita = 0;

                    if (ivaPreassolta == "N")
                    {
                        corrispettivoTitoli = (row["TOT_CORRISPETTIVO_TITOLO"] != null && row["TOT_CORRISPETTIVO_TITOLO"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_CORRISPETTIVO_TITOLO"].ToString()), 2) : 0);
                        ivaTitoli = (row["TOT_IVA_TITOLO"] != null && row["TOT_IVA_TITOLO"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_IVA_TITOLO"].ToString()), 2) : 0);
                        nettoTitoli = System.Math.Max(0,  corrispettivoTitoli - ivaTitoli);
                        corrispettivoPrevendita = (row["TOT_CORRISPETTIVO_PREVENDITA"] != null && row["TOT_CORRISPETTIVO_PREVENDITA"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_CORRISPETTIVO_PREVENDITA"].ToString()), 2) : 0);
                        ivaPrevendita = (row["TOT_IVA_PREVENDITA"] != null && row["TOT_IVA_PREVENDITA"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_IVA_PREVENDITA"].ToString()), 2) : 0);
                    }
                    else
                    {
                        corrispettivoTitoli = (row["TOT_CORRISPETTIVO_FIGURATIVO"] != null && row["TOT_CORRISPETTIVO_FIGURATIVO"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_CORRISPETTIVO_FIGURATIVO"].ToString()), 2) : 0);
                        ivaTitoli = (row["TOT_IVA_FIGURATIVA"] != null && row["TOT_IVA_FIGURATIVA"] != System.DBNull.Value ? System.Math.Round(decimal.Parse(row["TOT_IVA_FIGURATIVA"].ToString()), 2) : 0);
                        nettoTitoli = System.Math.Max(0, corrispettivoTitoli - ivaTitoli);
                    }

                    Biglietto biglietto = new Biglietto()
                    {
                        IvaPreassolta = ivaPreassolta,
                        TipoTitolo = (row["TIPO_TITOLO"] != null && row["TIPO_TITOLO"] != System.DBNull.Value ? row["TIPO_TITOLO"].ToString() : ""),
                        Descrizione = (row["TIPO_BIGLIETTO"] != null && row["TIPO_BIGLIETTO"] != System.DBNull.Value ? row["TIPO_BIGLIETTO"].ToString() : ""),
                        TotaleEmessi = qtaEmessi,
                        ValCorrispettivoTitoli = corrispettivoTitoli,
                        ValIvaTitoli = ivaTitoli,
                        ValNettoTitoli = nettoTitoli,
                        ValCorrispettivoPrevendita = corrispettivoPrevendita,
                        ValIvaPrevendita = ivaPrevendita
                    };
                    distintaIncasso.QuadroB.AddBiglietto(biglietto);
                }
            }


            return distintaIncasso;
        }
        #endregion

        #region Xml

        public static string SerializeDistintaIncasso(DistintaIncasso distintaIncasso, out Exception oError)
        {
            oError = null;
            string value = "";
            try
            {
                using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
                {
                    System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
                    xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
                    xmlWriterSettings.OmitXmlDeclaration = false;
                    xmlWriterSettings.Indent = true;
                    xmlWriterSettings.CloseOutput = false;

                    System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    xmlNameSpaces.Add("", "");

                    using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(DistintaIncasso));
                        xmlSerializer.Serialize(xmlWriter, distintaIncasso, xmlNameSpaces);
                        xmlWriter.Dispose();
                    }
                    value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
                    xmlStream.Dispose();
                    value = value.Replace(@"<?xml version=""1.0"" encoding=""utf-8""?>", @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>").Replace('"', "'"[0]);
                    //<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
                }
            }
            catch (Exception ex)
            {
                oError = ex; // clsRiepiloghi.GetNewException("Serializzazione transazioni", "Errore durante la serialiazzione", ex);
                value = "";
            }
            return value;
        }

        #endregion
    }

    #region Models



    public class Titolare : IDisposable
    {
        [System.Xml.Serialization.XmlElement("CodiceSistema")]
        public string CodiceSistema { get; set; }

        public void Dispose()
        {

        }
    }

    public class Organizzatore : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Nome")]
        public string Nome { get; set; }

        [System.Xml.Serialization.XmlElement("CodiceFiscale")]
        public string CodiceFiscale { get; set; }

        [System.Xml.Serialization.XmlElement("PartitaIva")]
        public string PartitaIva { get; set; } = "";

        [System.Xml.Serialization.XmlElement("Tipo")]
        public string Tipo { get; set; }
        public void Dispose()
        {

        }
    }

    public class Locale : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Nome")]
        public string Nome { get; set; }

        [System.Xml.Serialization.XmlElement("CodiceSIAE")]
        public string CodiceSIAE { get; set; }

        [System.Xml.Serialization.XmlElement("Impianto")]
        public string Impianto { get; set; }

        [System.Xml.Serialization.XmlElement("Comune")]
        public string Comune { get; set; }

        [System.Xml.Serialization.XmlElement("Provincia")]
        public string Provincia { get; set; }

        [System.Xml.Serialization.XmlElement("Nazione")]
        public string Nazione { get; set; }
        public void Dispose()
        {

        }
    }

    public class Opera : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Titolo")]
        public string Titolo { get; set; }

        [System.Xml.Serialization.XmlElement("Tipo")]
        public string Tipo { get; set; }

        [System.Xml.Serialization.XmlElement("Noleggiatore")]
        public string Noleggiatore { get; set; }

        [System.Xml.Serialization.XmlElement("ProduttoreCinema")]
        public string ProduttoreCinema { get; set; }

        [System.Xml.Serialization.XmlElement("Nazionalita")]
        public string Nazionalita { get; set; }

        [System.Xml.Serialization.XmlElement("Supporto")]
        public string Supporto { get; set; }


        public void Dispose()
        {

        }
    }

    public class Opere : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Opera")]
        public List<Opera> Opera { get; set; }
        public void Dispose()
        {

        }
    }

    public class Evento : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Data")]
        public string Data { get; set; }

        [System.Xml.Serialization.XmlElement("OraInizio")]
        public string OraInizio { get; set; }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private DateTime? dataOraPrimoEvento { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public DateTime? DataOraPrimoEvento 
        { 
            get { return  dataOraPrimoEvento; } 
            set 
            { 
                dataOraPrimoEvento = value;
                Data = "";
                OraInizio = "";
                if (dataOraPrimoEvento != null)
                {
                    Data = dataOraPrimoEvento.Value.Year.ToString("0000") + dataOraPrimoEvento.Value.Month.ToString("00") + dataOraPrimoEvento.Value.Day.ToString("00");
                    OraInizio = dataOraPrimoEvento.Value.Hour.ToString("00") + dataOraPrimoEvento.Value.Minute.ToString("00");
                }
            } 
        }

        [System.Xml.Serialization.XmlElement("NumeroSpettacoli")]
        public int NumeroSpettacoli { get; set; }

        [System.Xml.Serialization.XmlElement("Opere")]
        public Opere Opere { get; set; }
        public void Dispose()
        {

        }
    }

    public class Biglietto : IDisposable
    {


        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valCorrispettivoTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValCorrispettivoTitoli { get { return valCorrispettivoTitoli; } set { valCorrispettivoTitoli = value; CorrispettivoTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valIvaTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValIvaTitoli { get { return valIvaTitoli; } set { valIvaTitoli = value; IvaTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valNettoTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValNettoTitoli { get { return valNettoTitoli; } set { valNettoTitoli = value; NettoTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valCorrispettivoPrevendita { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValCorrispettivoPrevendita { get { return valCorrispettivoPrevendita; } set { valCorrispettivoPrevendita = value; CorrispettivoPrevendita = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valIvaPrevendita { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValIvaPrevendita { get { return valIvaPrevendita; } set { valIvaPrevendita = value; IvaPrevendita = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlElement("IvaPreassolta")]
        public string IvaPreassolta { get; set; }

        [System.Xml.Serialization.XmlElement("TipoTitolo")]
        public string TipoTitolo { get; set; }

        [System.Xml.Serialization.XmlElement("Descrizione")]
        public string Descrizione { get; set; }

        [System.Xml.Serialization.XmlElement("TotaleEmessi")]
        public long TotaleEmessi { get; set; }

        [System.Xml.Serialization.XmlElement("CorrispettivoTitoli")]
        public string CorrispettivoTitoli { get; set; }
        
        [System.Xml.Serialization.XmlElement("IvaTitoli")]
        public string IvaTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("NettoTitoli")]
        public string NettoTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("CorrispettivoPrevendita")]
        public string CorrispettivoPrevendita { get; set; }

        [System.Xml.Serialization.XmlElement("IvaPrevendita")]
        public string IvaPrevendita { get; set; }

        public void Dispose()
        {

        }
    }
    
    public class Biglietti : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Biglietto")]
        public List<Biglietto> Biglietto { get; set; }
        public void Dispose()
        {

        }
    }

    public class Riepilogo : IDisposable
    {

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valCorrispettivoTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValCorrispettivoTitoli { get { return valCorrispettivoTitoli; } set { valCorrispettivoTitoli = value; CorrispettivoTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valIvaTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValIvaTitoli { get { return valIvaTitoli; } set { valIvaTitoli = value; IvaTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valNettoTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValNettoTitoli { get { return valNettoTitoli; } set { valNettoTitoli = value; NettoTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valCorrispettivoPrevendita { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValCorrispettivoPrevendita { get { return valCorrispettivoPrevendita; } set { valCorrispettivoPrevendita = value; CorrispettivoPrevendita = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valIvaPrevendita { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValIvaPrevendita { get { return valIvaPrevendita; } set { valIvaPrevendita = value; IvaPrevendita = Cedas.GetNumValue(value); } }


        [System.Xml.Serialization.XmlElement("TotaleEmessi")]
        public long TotaleEmessi { get; set; }

        [System.Xml.Serialization.XmlElement("CorrispettivoTitoli")]
        public string CorrispettivoTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("IvaTitoli")]
        public string IvaTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("NettoTitoli")]
        public string NettoTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("CorrispettivoPrevendita")]
        public string CorrispettivoPrevendita { get; set; }

        [System.Xml.Serialization.XmlElement("IvaPrevendita")]
        public string IvaPrevendita { get; set; }

        [System.Xml.Serialization.XmlElement("NumeroOmaggiEccedenti")]
        public long NumeroOmaggiEccedenti { get; set; }


        public void Dispose()
        {

        }
    }

    public class QuadroB : IDisposable
    {
        [System.Xml.Serialization.XmlElement("Biglietti")]
        public Biglietti Biglietti { get; set; }

        [System.Xml.Serialization.XmlElement("Riepilogo")]
        public Riepilogo Riepilogo { get; set; }

        public void Dispose()
        {

        }

        public void AddBiglietto(Biglietto biglietto)
        {
            this.Biglietti.Biglietto.Add(biglietto);
        }
    }

    public class CalcoloNettissimo : IDisposable
    {


        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valNettoOmaggiEccedenti { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValNettoOmaggiEccedenti { get { return valNettoOmaggiEccedenti; } set { valNettoOmaggiEccedenti = value; NettoOmaggiEccedenti = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valNettoPrestazioniComplementari { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValNettoPrestazioniComplementari { get { return valNettoPrestazioniComplementari; } set { valNettoPrestazioniComplementari = value; NettoPrestazioniComplementari = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valNettoTitoli { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValNettoTitoli { get { return valNettoTitoli; } set { valNettoTitoli = value; NettoTitoli = Cedas.GetNumValue(value); } }

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        private decimal valDirittiAutore { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public decimal ValDirittiAutore { get { return valDirittiAutore; } set { valDirittiAutore = value; DirittiAutore = Cedas.GetNumValue(value); } }


        [System.Xml.Serialization.XmlElement("NettoOmaggiEccedenti")]
        public string NettoOmaggiEccedenti { get; set; }

        [System.Xml.Serialization.XmlElement("NettoPrestazioniComplementari")]
        public string NettoPrestazioniComplementari { get; set; }

        [System.Xml.Serialization.XmlElement("NettoTitoli")]
        public string NettoTitoli { get; set; }

        [System.Xml.Serialization.XmlElement("DirittiAutore")]
        public string DirittiAutore { get; set; }


        public void Dispose()
        {

        }
    }

    public class DistintaIncasso : IDisposable
    {
        [System.Xml.Serialization.XmlAttribute("versione"), JsonIgnore]
        public string versione { get; set; } = "1.2";

        [System.Xml.Serialization.XmlElement("Titolare")]
        public Titolare Titolare { get; set; }

        [System.Xml.Serialization.XmlElement("Organizzatore")]
        public Organizzatore Organizzatore { get; set; }

        [System.Xml.Serialization.XmlElement("Locale")]
        public Locale Locale { get; set; }

        [System.Xml.Serialization.XmlElement("Evento")]
        public Evento Evento { get; set; }

        [System.Xml.Serialization.XmlElement("QuadroB")]
        public QuadroB QuadroB { get; set; }

        [System.Xml.Serialization.XmlElement("CalcoloNettissimo")]
        public CalcoloNettissimo CalcoloNettissimo { get; set; }

        public void Dispose()
        {

        }

        [System.Xml.Serialization.XmlIgnore]
        public int Progressivo { get; set; } = 0;

        [System.Xml.Serialization.XmlIgnore]
        public bool TestMode { get; set; } = false;

        [System.Xml.Serialization.XmlIgnore, JsonIgnore]
        public string UrlDistinta => $"https://www.cineadmin.com/BoxOffice/api/v1{(TestMode ? "/test" : "")}/incassi/{(Locale != null ? Locale.CodiceSIAE : "")}/{(Evento != null ? Evento.Data : "")}/{Progressivo.ToString()}";

        [System.Xml.Serialization.XmlIgnore]
        public string Xml { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public byte[] XmlGzip { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public byte[] PdfBytes { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public byte[] PdfGzip { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public string Auth { get; set; }
    }

    public class jSonWebError
    {
        public string code { get; set; }
        public string description { get; set; }
    }


    public class ResponseBase
    {
        public bool Success = true;
        public double Elapsed = 0;
        public string ErrorCode = null;
        public string Message = null;
        public string StackTrace = null;
        public List<string> Messages = new List<string>();
        public DateTime Time
        {
            get { return DateTime.Now; }
        }
    }
    public class Response : ResponseBase
    {
        public object Data { get; set; }
    }

    public class Request
    {
        public DistintaIncasso DistintaIncasso { get; set; }
    }

    #endregion
}
