﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testLibBordero
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Shown += Form1_Shown;
            this.btnCALC.Click += BtnCALC_Click;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.btnCALC.Focus();
        }

        private void BtnCALC_Click(object sender, EventArgs e)
        {
            Wintic.Data.IConnection Connection = libBordero.Bordero.GetConnection(this.txtConn.Text);
            libBordero.Bordero.DeleteDatiRichiestaBorderoFull(Connection, false);
            Connection.BeginTransaction();
            Int64 IdRichiestaBordero = 0;
            Exception oError = null;
            System.Data.DataSet oDataSetBordero = libBordero.Bordero.GetBordero(Connection, 1, this.dtINIZIO.Value, this.dtFINE.Value, ref IdRichiestaBordero, out oError);
            List<System.IO.FileInfo> oFilesBordero = new List<System.IO.FileInfo>();
            if (oError == null)
            {
                //List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();
                //libBordero.Bordero.clsFilterBordero oFiltro = new libBordero.Bordero.clsFilterBordero("BORDERO_FILT_TIPI_EVENTO");
                //oFiltro.RigheFiltro.Add(new object[] { IdRichiestaBordero, "S", "SPETTACOLO", "01", "CINEMA" });
                //oFiltri.Add(oFiltro);
                //libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);

                string cTipoBorderoMultiplo = "BORDERO_FILT_ORGANIZZATORI,BORDERO_FILT_TITOLI";
                cTipoBorderoMultiplo = "";
                List<libBordero.Bordero.clsInfoDataSet> listaDati = libBordero.Bordero.MultiBorderoStream(cTipoBorderoMultiplo, this.dtINIZIO.Value, this.dtFINE.Value, oDataSetBordero, "");

                foreach (libBordero.Bordero.clsInfoDataSet oInfoDataSet in listaDati)
                {
                    string fileName = libBordero.clsPdfDocument.GetTemptPdfFileName("Bordero", "");
                    byte[] data = oInfoDataSet.StreamPdf.ToArray();
                    oInfoDataSet.StreamPdf.Dispose();
                    System.IO.File.WriteAllBytes(fileName, data);
                    System.IO.FileInfo oFileInfo = new System.IO.FileInfo(fileName);
                    oFilesBordero.Add(oFileInfo);
                }

                //oFilesBordero = libBordero.Bordero.MultiBorderoFiles(cTipoBorderoMultiplo, oDataSetBordero, "", "");
            }
            else
            {
                MessageBox.Show(oError.ToString());
            }

            Connection.RollBack();
            Connection.Close();

            if (oError == null && oFilesBordero != null && oFilesBordero.Count > 0)
            {
                foreach (System.IO.FileInfo oFileInfo in oFilesBordero)
                {
                    System.Diagnostics.Process oRunPdfView = new System.Diagnostics.Process();
                    oRunPdfView.StartInfo = new System.Diagnostics.ProcessStartInfo(oFileInfo.FullName);
                    oRunPdfView.Start();
                    oRunPdfView.WaitForExit();
                }
            }
        }
    }
}
