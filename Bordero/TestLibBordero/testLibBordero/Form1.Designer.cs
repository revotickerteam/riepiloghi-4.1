﻿namespace testLibBordero
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtINIZIO = new System.Windows.Forms.DateTimePicker();
            this.dtFINE = new System.Windows.Forms.DateTimePicker();
            this.dgvBordero = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtConn = new System.Windows.Forms.TextBox();
            this.btnCALC = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBordero)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtINIZIO
            // 
            this.dtINIZIO.Location = new System.Drawing.Point(6, 3);
            this.dtINIZIO.Name = "dtINIZIO";
            this.dtINIZIO.Size = new System.Drawing.Size(200, 23);
            this.dtINIZIO.TabIndex = 0;
            this.dtINIZIO.Value = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
            // 
            // dtFINE
            // 
            this.dtFINE.Location = new System.Drawing.Point(212, 3);
            this.dtFINE.Name = "dtFINE";
            this.dtFINE.Size = new System.Drawing.Size(200, 23);
            this.dtFINE.TabIndex = 1;
            this.dtFINE.Value = new System.DateTime(2021, 12, 31, 0, 0, 0, 0);
            // 
            // dgvBordero
            // 
            this.dgvBordero.AllowUserToAddRows = false;
            this.dgvBordero.AllowUserToDeleteRows = false;
            this.dgvBordero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBordero.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBordero.Location = new System.Drawing.Point(0, 50);
            this.dgvBordero.Name = "dgvBordero";
            this.dgvBordero.ReadOnly = true;
            this.dgvBordero.Size = new System.Drawing.Size(710, 211);
            this.dgvBordero.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtConn);
            this.panel1.Controls.Add(this.btnCALC);
            this.panel1.Controls.Add(this.dtFINE);
            this.panel1.Controls.Add(this.dtINIZIO);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 50);
            this.panel1.TabIndex = 0;
            // 
            // txtConn
            // 
            this.txtConn.Location = new System.Drawing.Point(418, 3);
            this.txtConn.Name = "txtConn";
            this.txtConn.Size = new System.Drawing.Size(149, 23);
            this.txtConn.TabIndex = 2;
            this.txtConn.Text = "TEST-BORDERO";
            // 
            // btnCALC
            // 
            this.btnCALC.Location = new System.Drawing.Point(573, 3);
            this.btnCALC.Name = "btnCALC";
            this.btnCALC.Size = new System.Drawing.Size(75, 23);
            this.btnCALC.TabIndex = 3;
            this.btnCALC.Text = "Calcola";
            this.btnCALC.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnCALC;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(710, 261);
            this.Controls.Add(this.dgvBordero);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBordero)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtINIZIO;
        private System.Windows.Forms.DateTimePicker dtFINE;
        private System.Windows.Forms.DataGridView dgvBordero;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCALC;
        private System.Windows.Forms.TextBox txtConn;
    }
}

