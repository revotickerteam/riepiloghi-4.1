﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAppBordero.Controllers
{
    public class WebAppBorderoController : ApiController
    {

        //swagger/ui/index
        //Test.html

        [HttpPost]
        public WebAppBordero.Models.ResultBordero GetTest()
        {
            WebAppBordero.Models.RequestBordero Request = new Models.RequestBordero("WIN10X64SERVERORACLE");
            Request.CompleteRequest = true;
            Request.Inizio = new DateTime(2016, 02, 19);
            Request.Termine = new DateTime(2016, 02, 19);
            Request.Filtri = new List<libBordero.Bordero.clsFilterBordero>();
            Request.IdRichiestaBordero = 0;
            Request.MultiBorderoCriteria = "";
            WebAppBordero.Models.ResultBordero Result = GetDati(Request);
            return Result;
        }

        [HttpPost]
        public WebAppBordero.Models.ResultBordero GetDati(WebAppBordero.Models.RequestBordero Request)
        {
            WebAppBordero.Models.ResultBordero Result = new Models.ResultBordero();
            try
            {
                Wintic.Data.IConnection Connection = libBordero.Bordero.GetConnection(Request.DataSource);
                if (Connection != null && Connection.State == System.Data.ConnectionState.Open)
                {
                    Connection.BeginTransaction();
                    try
                    {
                        Int64 IdRichiestaBordero = Request.IdRichiestaBordero;
                        
                        System.Data.DataSet oDataSetBordero = libBordero.Bordero.GetBordero(Connection, Request.Inizio, Request.Termine, ref IdRichiestaBordero);

                        if (Request.CompleteRequest)
                        {
                            if (IdRichiestaBordero > 0)
                            {
                                libBordero.Bordero.DeleteDatiRichiestaBordero(Connection, Request.IdRichiestaBordero, true);
                            }
                        }

                        if (Request.Filtri != null && Request.Filtri.Count > 0)
                        {
                            libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, Request.Filtri);
                        }
                        string FontPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data");
                        string TempPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/Temp");

                        if (Request.CompleteRequest)
                        {
                            Result = new Models.ResultBordero(IdRichiestaBordero, oDataSetBordero, libBordero.Bordero.MultiBorderFiles(Request.MultiBorderoCriteria, oDataSetBordero, FontPath, TempPath));
                        }
                        else
                        {
                            Result = new Models.ResultBordero(IdRichiestaBordero, oDataSetBordero);
                        }
                        Connection.EndTransaction();
                    }
                    catch (Exception ex)
                    {
                        Connection.RollBack();
                        Result.StatusOK = false;
                        Result.StatusMessage = ex.Message;
                    }
                    Connection.Close();
                }
            }
            catch (Exception ex)
            {
                Result.StatusOK = false;
                Result.StatusMessage = ex.Message;
            }
            return Result;
        }
    }
}
