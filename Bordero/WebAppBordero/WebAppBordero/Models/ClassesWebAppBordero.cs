﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppBordero.Models
{
    public class RequestBordero
    {
        public bool CompleteRequest = false;
        public string DataSource = "";
        public DateTime Inizio = DateTime.MinValue;
        public DateTime Termine = DateTime.MinValue;
        public Int64 IdRichiestaBordero = 0;
        public List<libBordero.Bordero.clsFilterBordero> Filtri = new List<libBordero.Bordero.clsFilterBordero>();
        public string MultiBorderoCriteria = "";

        public RequestBordero()
        {
            this.DataSource = "";
            this.Inizio = DateTime.MinValue;
            this.Termine = DateTime.MinValue;
            this.IdRichiestaBordero = 0;
            this.Filtri = new List<libBordero.Bordero.clsFilterBordero>();
            this.MultiBorderoCriteria = "";
        }

        public RequestBordero(string cDataSource)
            :this()
        {
            this.DataSource = cDataSource;
        }
    }
    public class ResultBordero
    {
        public bool StatusOK = true;
        public string StatusMessage = "";
        public Int64 IdRichiestaBordero = 0;
        public List<System.Data.DataTable> FilterTables = new List<System.Data.DataTable>();
        //public List<System.Net.Http.HttpResponseMessage> Pdfs = new List<System.Net.Http.HttpResponseMessage>();
        public List<byte[]> Pdfs = new List<byte[]>();

        public ResultBordero()
        {
            this.StatusOK = true;
            this.StatusMessage = "";
            this.IdRichiestaBordero = 0;
            this.FilterTables = new List<System.Data.DataTable>();
            this.Pdfs = new List<byte[]>();
        }

        public ResultBordero(Int64 nIdRichiestaBordero)
            : this()
        {
            this.IdRichiestaBordero = nIdRichiestaBordero;
        }

        public ResultBordero(Int64 nIdRichiestaBordero, System.Data.DataSet oDataSet)
            : this(nIdRichiestaBordero)
        {
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_NOLEGGIATORI"].Copy());
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_ORGANIZZATORI"].Copy());
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_PRODUTTORI"].Copy());
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_SALE"].Copy());
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_TIPI_EVENTO"].Copy());
            this.FilterTables.Add(oDataSet.Tables["BORDERO_FILT_TITOLI"].Copy());
        }

        public ResultBordero(Int64 nIdRichiestaBordero, System.Data.DataSet oDataSet, List<System.IO.FileInfo> oFiles)
            : this(nIdRichiestaBordero, oDataSet)
        {
            foreach (System.IO.FileInfo oFileInfo in oFiles)
            {
                //System.Net.Http.HttpResponseMessage oResponse = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);
                //oResponse.Content = new System.Net.Http.StreamContent(new System.IO.FileStream(oFileInfo.FullName, System.IO.FileMode.Open, System.IO.FileAccess.Read));
                //oResponse.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                //oResponse.Content.Headers.ContentDisposition.FileName = oFileInfo.FullName;
                //oResponse.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //this.Pdfs.Add(oResponse);
                this.Pdfs.Add(System.IO.File.ReadAllBytes(oFileInfo.FullName));
            }
        }
    }
}