﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace WinAppTnsWebtic
{
    public partial class frmWinAppTnsWebtic : Form
    {

        private class BorderoMenuConnessione
        {
            public string Codice { get; set; }
            public string Descrizione { get; set; }

            public List<BorderoMenuConnessione> Connessioni { get; set; }
        }

        public frmWinAppTnsWebtic()
        {
            InitializeComponent();
            
        }

        

        private void btnFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;
            dialog.Description = "Cartella di destinazione";
            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.txtDest.Text = dialog.SelectedPath;
            }
            dialog.Dispose();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            IConnection con = new Wintic.Data.oracle.CConnectionOracle();
            try
            {
                BorderoMenuConnessione root = new BorderoMenuConnessione();
                root.Codice = "root";
                root.Descrizione = "Connessioni";
                root.Connessioni = new List<BorderoMenuConnessione>();
                BorderoMenuConnessione singoli = new BorderoMenuConnessione();
                singoli.Codice = "SINGOLI";
                singoli.Descrizione = "Singoli";
                singoli.Connessioni = new List<BorderoMenuConnessione>();
                root.Connessioni.Add(singoli);

                BorderoMenuConnessione gruppi = new BorderoMenuConnessione();
                gruppi.Codice = "GRUPPI";
                gruppi.Descrizione = "Gruppi";
                gruppi.Connessioni = new List<BorderoMenuConnessione>();
                root.Connessioni.Add(gruppi);

                BorderoMenuConnessione parent = null;

                con.ConnectionString = string.Format("user id={0};password={1};Min Pool Size=1;Max Pool Size=50;Connection Lifetime=120;Pooling=true;Enlist=true;data source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST={2})(PORT={3})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={4})))", txtUser.Text, txtPassword.Text, txtIP.Text, txtPort.Text, txtService.Text);
                con.Open();
                StringBuilder oSB = new StringBuilder();
                oSB.Append("SELECT XS_VIEW_CINEMA.IDGROUP");
                oSB.Append("     , XS_VIEW_CINEMA.DESCR_GRUPPO");
                oSB.Append("     , XS_VIEW_CINEMA.IDCINEMA");
                oSB.Append("     , XS_VIEW_CINEMA.DESCR");
                oSB.Append("     , XS_VIEW_CINEMA.TNS_NAME");
                oSB.Append("     , COUNT(DISTINCT XS_VIEW_CINEMA.IDCINEMA) OVER (PARTITION BY XS_VIEW_CINEMA.IDGROUP) AS CNT_GRUPPO");
                oSB.Append(" FROM WEBTIC_WS.XS_VIEW_CINEMA");
                oSB.Append(" ORDER BY XS_VIEW_CINEMA.DESCR_GRUPPO");
                oSB.Append("        , XS_VIEW_CINEMA.DESCR");
                DataTable table = con.ExecuteQuery(oSB.ToString());
                foreach (DataRow row in table.Rows)
                {
                    int cnt = 0;
                    int.TryParse(row["CNT_GRUPPO"].ToString(), out cnt);
                    if (cnt == 1)
                        parent = singoli;
                    else
                    {
                        if (parent == null || parent.Codice != row["IDGROUP"].ToString())
                        {
                            parent = new BorderoMenuConnessione() { Codice = row["IDGROUP"].ToString(), Descrizione = row["DESCR_GRUPPO"].ToString(), Connessioni = new List<BorderoMenuConnessione>() };
                            gruppi.Connessioni.Add(parent);
                        }
                    }
                    BorderoMenuConnessione item = new BorderoMenuConnessione() { Codice = row["TNS_NAME"].ToString(), Descrizione = row["DESCR"].ToString() };
                    parent.Connessioni.Add(item);
                }

                Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings();
                settings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                settings.Formatting = Newtonsoft.Json.Formatting.Indented;
                System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(this.txtDest.Text);
                System.IO.File.WriteAllText(dirInfo.FullName + @"\BConnessioni.json", Newtonsoft.Json.JsonConvert.SerializeObject(root, settings));
                MessageBox.Show("Fine", "Elaborazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore\r\n" + ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                    con = null;
                }
            }
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
            this.Dispose();
        }
    }
}
