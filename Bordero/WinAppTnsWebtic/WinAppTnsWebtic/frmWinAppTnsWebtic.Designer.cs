﻿
namespace WinAppTnsWebtic
{
    partial class frmWinAppTnsWebtic
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlUsr = new System.Windows.Forms.Panel();
            this.lblUser = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.pnlPsw = new System.Windows.Forms.Panel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPsw = new System.Windows.Forms.Label();
            this.pnlDest = new System.Windows.Forms.Panel();
            this.txtDest = new System.Windows.Forms.TextBox();
            this.lblDest = new System.Windows.Forms.Label();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnFolder = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.lblIP = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.lblPorta = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtService = new System.Windows.Forms.TextBox();
            this.lblService = new System.Windows.Forms.Label();
            this.pnlUsr.SuspendLayout();
            this.pnlPsw.SuspendLayout();
            this.pnlDest.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlUsr
            // 
            this.pnlUsr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUsr.Controls.Add(this.txtUser);
            this.pnlUsr.Controls.Add(this.lblUser);
            this.pnlUsr.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUsr.Location = new System.Drawing.Point(0, 0);
            this.pnlUsr.Name = "pnlUsr";
            this.pnlUsr.Padding = new System.Windows.Forms.Padding(3);
            this.pnlUsr.Size = new System.Drawing.Size(654, 35);
            this.pnlUsr.TabIndex = 0;
            // 
            // lblUser
            // 
            this.lblUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblUser.Location = new System.Drawing.Point(3, 3);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(144, 27);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "Utente:";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUser
            // 
            this.txtUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUser.Location = new System.Drawing.Point(147, 3);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(502, 27);
            this.txtUser.TabIndex = 1;
            // 
            // pnlPsw
            // 
            this.pnlPsw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPsw.Controls.Add(this.txtPassword);
            this.pnlPsw.Controls.Add(this.lblPsw);
            this.pnlPsw.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPsw.Location = new System.Drawing.Point(0, 35);
            this.pnlPsw.Name = "pnlPsw";
            this.pnlPsw.Padding = new System.Windows.Forms.Padding(3);
            this.pnlPsw.Size = new System.Drawing.Size(654, 35);
            this.pnlPsw.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPassword.Location = new System.Drawing.Point(147, 3);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(502, 27);
            this.txtPassword.TabIndex = 1;
            // 
            // lblPsw
            // 
            this.lblPsw.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPsw.Location = new System.Drawing.Point(3, 3);
            this.lblPsw.Name = "lblPsw";
            this.lblPsw.Size = new System.Drawing.Size(144, 27);
            this.lblPsw.TabIndex = 0;
            this.lblPsw.Text = "Password:";
            this.lblPsw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlDest
            // 
            this.pnlDest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDest.Controls.Add(this.txtDest);
            this.pnlDest.Controls.Add(this.btnFolder);
            this.pnlDest.Controls.Add(this.lblDest);
            this.pnlDest.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDest.Location = new System.Drawing.Point(0, 175);
            this.pnlDest.Name = "pnlDest";
            this.pnlDest.Padding = new System.Windows.Forms.Padding(3);
            this.pnlDest.Size = new System.Drawing.Size(654, 35);
            this.pnlDest.TabIndex = 5;
            // 
            // txtDest
            // 
            this.txtDest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDest.Location = new System.Drawing.Point(147, 3);
            this.txtDest.Name = "txtDest";
            this.txtDest.Size = new System.Drawing.Size(469, 27);
            this.txtDest.TabIndex = 1;
            // 
            // lblDest
            // 
            this.lblDest.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDest.Location = new System.Drawing.Point(3, 3);
            this.lblDest.Name = "lblDest";
            this.lblDest.Size = new System.Drawing.Size(144, 27);
            this.lblDest.TabIndex = 0;
            this.lblDest.Text = "Cartella di destinazione:";
            this.lblDest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlButtons
            // 
            this.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlButtons.Controls.Add(this.btnAbort);
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlButtons.Location = new System.Drawing.Point(0, 210);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(654, 61);
            this.pnlButtons.TabIndex = 6;
            // 
            // btnOK
            // 
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOK.Location = new System.Drawing.Point(565, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(84, 53);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAbort
            // 
            this.btnAbort.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(3, 3);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(84, 53);
            this.btnAbort.TabIndex = 1;
            this.btnAbort.Text = "Abbandona";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnFolder
            // 
            this.btnFolder.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFolder.Location = new System.Drawing.Point(616, 3);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(33, 27);
            this.btnFolder.TabIndex = 2;
            this.btnFolder.Text = "...";
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtIP);
            this.panel1.Controls.Add(this.lblIP);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 70);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(654, 35);
            this.panel1.TabIndex = 2;
            // 
            // txtIP
            // 
            this.txtIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIP.Location = new System.Drawing.Point(147, 3);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(502, 27);
            this.txtIP.TabIndex = 1;
            // 
            // lblIP
            // 
            this.lblIP.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblIP.Location = new System.Drawing.Point(3, 3);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(144, 27);
            this.lblIP.TabIndex = 1;
            this.lblIP.Text = "IP:";
            this.lblIP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Controls.Add(this.lblPorta);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 105);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(3);
            this.panel2.Size = new System.Drawing.Size(654, 35);
            this.panel2.TabIndex = 3;
            // 
            // txtPort
            // 
            this.txtPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPort.Location = new System.Drawing.Point(147, 3);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(502, 27);
            this.txtPort.TabIndex = 1;
            this.txtPort.Text = "1521";
            // 
            // lblPorta
            // 
            this.lblPorta.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPorta.Location = new System.Drawing.Point(3, 3);
            this.lblPorta.Name = "lblPorta";
            this.lblPorta.Size = new System.Drawing.Size(144, 27);
            this.lblPorta.TabIndex = 0;
            this.lblPorta.Text = "Porta:";
            this.lblPorta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtService);
            this.panel3.Controls.Add(this.lblService);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 140);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(654, 35);
            this.panel3.TabIndex = 4;
            // 
            // txtService
            // 
            this.txtService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtService.Location = new System.Drawing.Point(147, 3);
            this.txtService.Name = "txtService";
            this.txtService.Size = new System.Drawing.Size(502, 27);
            this.txtService.TabIndex = 1;
            // 
            // lblService
            // 
            this.lblService.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblService.Location = new System.Drawing.Point(3, 3);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(144, 27);
            this.lblService.TabIndex = 0;
            this.lblService.Text = "Service name:";
            this.lblService.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmWinAppTnsWebtic
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnAbort;
            this.ClientSize = new System.Drawing.Size(654, 271);
            this.ControlBox = false;
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.pnlDest);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlPsw);
            this.Controls.Add(this.pnlUsr);
            this.Font = new System.Drawing.Font("Bahnschrift Light Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmWinAppTnsWebtic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Costruzione connessioni da Webtic";
            this.pnlUsr.ResumeLayout(false);
            this.pnlUsr.PerformLayout();
            this.pnlPsw.ResumeLayout(false);
            this.pnlPsw.PerformLayout();
            this.pnlDest.ResumeLayout(false);
            this.pnlDest.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlUsr;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Panel pnlPsw;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPsw;
        private System.Windows.Forms.Panel pnlDest;
        private System.Windows.Forms.TextBox txtDest;
        private System.Windows.Forms.Label lblDest;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label lblPorta;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtService;
        private System.Windows.Forms.Label lblService;
    }
}

