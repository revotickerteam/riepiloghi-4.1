﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Wintic.Data;

namespace makeUpdaterTestConnection32x64
{
    class Program
    {
        static void Main(string[] args)
        {
            IConnection conn = null;
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string result = fileInfo.Directory.FullName + @"\makeUpdaterTestConnection32x64.log";
            try
            {
                conn = new Wintic.Data.oracle.CConnectionOracle();
                //conn.ConnectionString = args[0];
                conn.ConnectionString = Decrypt(args[0], kToken, false);
                conn.Open();
                System.IO.File.WriteAllText(result, "OK");
                conn.Close();
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(result, (ex.Message.Contains("BadImage") ? "ERR3264" : ex.Message));
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            
        }

        private const string kToken = "F5EE450FE2119D0B7EDF48FA";


        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}
