﻿namespace toolsUpdater
{
    partial class frmTools
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTools));
            this.btnLIB = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnCLOSE = new System.Windows.Forms.Button();
            this.btnSiteToSite = new System.Windows.Forms.Button();
            this.btnSEND = new System.Windows.Forms.Button();
            this.btnDistribution = new System.Windows.Forms.Button();
            this.pnlMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLIB
            // 
            this.btnLIB.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLIB.Location = new System.Drawing.Point(0, 0);
            this.btnLIB.Name = "btnLIB";
            this.btnLIB.Size = new System.Drawing.Size(507, 67);
            this.btnLIB.TabIndex = 0;
            this.btnLIB.Text = "Libreria";
            this.btnLIB.UseVisualStyleBackColor = true;
            this.btnLIB.Click += new System.EventHandler(this.btnLIB_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.btnCLOSE);
            this.pnlMenu.Controls.Add(this.btnSiteToSite);
            this.pnlMenu.Controls.Add(this.btnSEND);
            this.pnlMenu.Controls.Add(this.btnDistribution);
            this.pnlMenu.Controls.Add(this.btnLIB);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(507, 335);
            this.pnlMenu.TabIndex = 1;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCLOSE.Location = new System.Drawing.Point(0, 268);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(507, 67);
            this.btnCLOSE.TabIndex = 4;
            this.btnCLOSE.Text = "CHIUDI";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnSiteToSite
            // 
            this.btnSiteToSite.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSiteToSite.Location = new System.Drawing.Point(0, 201);
            this.btnSiteToSite.Name = "btnSiteToSite";
            this.btnSiteToSite.Size = new System.Drawing.Size(507, 67);
            this.btnSiteToSite.TabIndex = 3;
            this.btnSiteToSite.Text = "Invia aggiornamenti da sito a sito";
            this.btnSiteToSite.UseVisualStyleBackColor = true;
            this.btnSiteToSite.Click += new System.EventHandler(this.btnSiteToSite_Click);
            // 
            // btnSEND
            // 
            this.btnSEND.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSEND.Location = new System.Drawing.Point(0, 134);
            this.btnSEND.Name = "btnSEND";
            this.btnSEND.Size = new System.Drawing.Size(507, 67);
            this.btnSEND.TabIndex = 2;
            this.btnSEND.Text = "Invia aggiornamenti";
            this.btnSEND.UseVisualStyleBackColor = true;
            this.btnSEND.Click += new System.EventHandler(this.btnSEND_Click);
            // 
            // btnDistribution
            // 
            this.btnDistribution.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDistribution.Location = new System.Drawing.Point(0, 67);
            this.btnDistribution.Name = "btnDistribution";
            this.btnDistribution.Size = new System.Drawing.Size(507, 67);
            this.btnDistribution.TabIndex = 1;
            this.btnDistribution.Text = "Distribuzioni";
            this.btnDistribution.UseVisualStyleBackColor = true;
            this.btnDistribution.Click += new System.EventHandler(this.btnDistribution_Click);
            // 
            // frmTools
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(507, 335);
            this.Controls.Add(this.pnlMenu);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTools";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestione Aggiornamenti";
            this.pnlMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLIB;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnSiteToSite;
        private System.Windows.Forms.Button btnSEND;
        private System.Windows.Forms.Button btnDistribution;
        private System.Windows.Forms.Button btnCLOSE;
    }
}

