﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace toolsUpdater
{
    public partial class frmTools : Form
    {
        public frmTools()
        {
            InitializeComponent();
        }

        private void btnLIB_Click(object sender, EventArgs e)
        {
            Exception error = null;
            libUpdater.clsUpdaterApplication app = null;
            libUpdater.Updater.InterfaceMakePackegToUpload(out error, out app, true);
        }

        private void btnSEND_Click(object sender, EventArgs e)
        {
            
            OpenSendForm();




            //bool lret = false;
            //string tns = "";
            //string user = "";
            //string password = "";
            //string connectionString = "";
            //DialogResult rDialog = DialogResult.Abort;
            //rDialog = ShowInputDialog("Inserire l'utente Oracle di destinazione", ref user);
            //if (rDialog == DialogResult.OK)
            //{
            //    password = user;
            //    rDialog = ShowInputDialog("Inserire la password dell'utente Oracle di destinazione", ref password, "*");
                
            //    if (rDialog == DialogResult.OK)
            //    {
            //        rDialog = ShowInputDialog("Inserire il tnsName di destinazione", ref tns);
            //        lret = true;
            //        connectionString = string.Format("USER={0};PASSWORD={1};DATA SOURCE={2}", user, password, tns);
            //    }
            //}

            //string pathLib = "";
            //Wintic.Data.IConnection con = null;
            //if (lret)
            //{
            //    List<string> pathLibItems = GetLibsToSend();

            //    lret = (pathLibItems.Count > 0);

            //    if (lret && !string.IsNullOrEmpty(connectionString))
            //    {
            //        try
            //        {
            //            con = new Wintic.Data.oracle.CConnectionOracle();
            //            con.ConnectionString = connectionString;
            //            con.Open();
            //        }
            //        catch (Exception)
            //        {
            //            con = null;
            //            lret = false;
            //        }
            //    }

            //    if (lret && con != null && con.State == ConnectionState.Open)
            //    {
            //        Exception error = null;
            //        libUpdater.Updater.InterfaceUploadUpdate(con, out error, pathLibItems);
            //    }

            //    if (con != null)
            //    {
            //        con.Close();
            //        con.Dispose();
            //    }

            //}
        }

        private List<string> GetLibsToSend()
        {
            List<string> directories = new List<string>();
            while (true)
            {
                System.Windows.Forms.OpenFileDialog oF = new System.Windows.Forms.OpenFileDialog();
                oF.Multiselect = true;
                oF.CheckFileExists = true;
                oF.DefaultExt = ".json";
                oF.Filter = "json files (*.json)|*.json";
                oF.FilterIndex = 1;
                if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oF.FileName) && System.IO.File.Exists(oF.FileName))
                {
                    System.IO.FileInfo oFInfo = new System.IO.FileInfo(oF.FileName);
                    string cFullPah = oFInfo.Directory.FullName;
                    if (!directories.Contains(cFullPah))
                        directories.Add(cFullPah);
                }
                oF.Dispose();
                if (MessageBox.Show("Altre librerie ?", "Invio librerie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    break;
            }
            return directories;
        }

        private void btnSiteToSite_Click(object sender, EventArgs e)
        {
            bool lret = false;
            string user = "";
            string password = "";
            string tnsSOURCE = "";
            string tnsTARGET = "";
            string TableNameAPPS_VERSION = "";
            DialogResult rDialog = DialogResult.Abort;

            rDialog = ShowInputDialog("Inserire l'utente Oracle di destinazione", ref user);
            if (rDialog == DialogResult.OK)
            {
                password = user;
                rDialog = ShowInputDialog("Inserire la password dell'utente Oracle di destinazione", ref password);
                if (rDialog == DialogResult.OK)
                {
                    rDialog = ShowInputDialog("Inserire il tnsName da cui prendere", ref tnsSOURCE);
                    if (rDialog == DialogResult.OK)
                    {
                        rDialog = ShowInputDialog("Inserire il tnsName di destinazione", ref tnsTARGET);
                        if (rDialog == DialogResult.OK)
                        {
                            rDialog = ShowInputDialog("Inserire tabella versioni", ref TableNameAPPS_VERSION);
                            lret = rDialog == DialogResult.OK;
                        }
                        
                        
                    }
                }
            }

            Wintic.Data.IConnection conSource = null;
            Wintic.Data.IConnection conTarget = null;

            if (lret)
            {
                try
                {
                    conSource = new Wintic.Data.oracle.CConnectionOracle();
                    conSource.ConnectionString = string.Format("USER={0};PASSWORD={1};DATA SOURCE={2}", user, password, tnsSOURCE);
                    conSource.Open();
                }
                catch (Exception)
                {
                    lret = false;
                }
            }

            if (lret)
            {
                try
                {
                    conTarget = new Wintic.Data.oracle.CConnectionOracle();
                    conTarget.ConnectionString = string.Format("USER={0};PASSWORD={1};DATA SOURCE={2}", user, password, tnsTARGET);
                    conTarget.Open();
                }
                catch (Exception)
                {
                    lret = false;
                }
            }

            if (lret)
            {
                Exception error = null;
                //libUpdater.Updater.UploadUpdatesSiteToSite(conSource, conTarget, TableNameAPPS_VERSION, out error, "", true);
            }

            if (conSource != null)
            {
                conSource.Close();
                conSource.Dispose();
            }
            if (conTarget != null)
            {
                conTarget.Close();
                conTarget.Dispose();
            }
        }
        public static DialogResult ShowInputDialog(string title, ref string input)
        {
            return ShowInputDialog(title, ref input, "");
        }
        public static DialogResult ShowInputDialog(string title, ref string input, string passwordChar)
        {
            System.Drawing.Size size = new System.Drawing.Size(600, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = title;
            inputBox.StartPosition = FormStartPosition.CenterScreen;

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            if (!string.IsNullOrEmpty(passwordChar))
            {
                textBox.PasswordChar = System.Text.Encoding.ASCII.GetChars(System.Text.Encoding.ASCII.GetBytes(passwordChar))[0];
            }
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private void btnDistribution_Click(object sender, EventArgs e)
        {
            libUpdater.Updater.EditDistribution();
        }

        private List<string> GetDistributionToSend()
        {
            List<string> files = new List<string>();
            System.Windows.Forms.OpenFileDialog oF = new System.Windows.Forms.OpenFileDialog();
            oF.Multiselect = true;
            oF.CheckFileExists = true;
            oF.DefaultExt = ".json";
            oF.Filter = "json files (*.json)|*.json";
            oF.FilterIndex = 1;
            oF.Title = "Destinatari";
            oF.Multiselect = true;
            string initPathDistribuzione = libUpdater.Updater.GetDirApplicationDefaults("DISTRIBUZIONE");
            if (initPathDistribuzione.Trim() != "" && System.IO.Directory.Exists(initPathDistribuzione)) oF.InitialDirectory = initPathDistribuzione;
            if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oF.FileName) && System.IO.File.Exists(oF.FileName))
            {
                foreach (String fileName in oF.FileNames)
                {
                    System.IO.FileInfo oFInfo = new System.IO.FileInfo(fileName);
                    string cFullPah = oFInfo.FullName;
                    if (!files.Contains(cFullPah))
                        files.Add(cFullPah);
                }
            }
            oF.Dispose();
            return files;
        }

        private void OpenSendForm()
        {

            List<string> pathLibItems = GetDistributionToSend();

            if (pathLibItems != null && pathLibItems.Count > 0)
            {

                Form frmSend = new Form();
                frmSend.Icon = libUpdater.Updater.GetIcon();
                frmSend.AutoScroll = true;
                frmSend.AutoScaleMode = AutoScaleMode.None;
                frmSend.WindowState = FormWindowState.Maximized;
                frmSend.StartPosition = FormStartPosition.CenterScreen;
                frmSend.Text = "Invio";
                frmSend.Font = new Font("Calibri", 12, FontStyle.Regular);
                List<libUpdater.DistributionExecution> items = new List<libUpdater.DistributionExecution>();
                int maxThreads = 2;
                int started = 0;
                int executing = 0;
                int terminated = 0;
                bool forceToStop = false;
                System.Threading.Thread mainThread = null;
                frmSend.Shown += (o, e) =>
                {

                    Panel pnlButtons = new Panel();
                    pnlButtons.Dock = DockStyle.Bottom;
                    frmSend.Controls.Add(pnlButtons);
                    pnlButtons.BringToFront();



                    int index = 0;
                    foreach (string file in pathLibItems)
                    {
                        index += 1;
                        TextBox txt = new TextBox();
                        txt.Multiline = true;
                        txt.ScrollBars = ScrollBars.Both;
                        txt.Dock = (index == pathLibItems.Count ? DockStyle.Fill : DockStyle.Left);
                        txt.Dock = DockStyle.Left;
                        txt.Width = 500;
                        frmSend.Controls.Add(txt);
                        txt.BringToFront();
                        Splitter sp = new Splitter();
                        sp.Dock = DockStyle.Left;
                        frmSend.Controls.Add(sp);
                        sp.BringToFront();
                        string JsonAppFile = file;
                        string cJsonApplication = System.IO.File.ReadAllText(JsonAppFile);
                        libUpdater.Distribution distrubution = Newtonsoft.Json.JsonConvert.DeserializeObject<libUpdater.Distribution>(cJsonApplication);
                        libUpdater.DistributionExecution distExecution = new libUpdater.DistributionExecution(distrubution, txt);
                        distExecution.SetTextBoxValue(distrubution.OracleTnsName);
                        foreach (libUpdater.clsUpdaterApplication app in distrubution.Applications)
                        {
                            distExecution.SetTextBoxValue(app.ApplicationName);
                        }
                        distExecution.SetTextBoxValue("_".PadRight(50, '_'));
                        items.Add(distExecution);
                    }


                    clsButtonExecution btn;

                    btn = new clsButtonExecution();
                    btn.Dock = DockStyle.Left;
                    btn.Text = "Avvia";
                    btn.Click += (btnObj, btnEvent) =>
                    {
                        clsButtonExecution btnExec = (clsButtonExecution)btnObj;
                        if (btnExec.Text == "Avvia")
                        {
                            btnExec.Text = "Arresta";
                            mainThread.Start();
                        }
                        else if (btnExec.Text == "Arresta")
                        {
                            btnExec.Enabled = false;
                            forceToStop = true;
                            btnExec.Text = "Arresto in corso";
                        }
                    };
                    pnlButtons.Controls.Add(btn);
                    btn.BringToFront();

                    btn = new clsButtonExecution();
                    btn.Dock = DockStyle.Fill;
                    btn.Text = "Chiudi";
                    btn.Enabled = false;
                    btn.DialogResult = DialogResult.OK;
                    pnlButtons.Controls.Add(btn);
                    btn.BringToFront();

                    pnlButtons.ClientSizeChanged += (oP, eP) => { pnlButtons.Controls[1].Width = (pnlButtons.ClientSize.Width - pnlButtons.Padding.Horizontal) / 2; };
                    pnlButtons.Height = 80;

                    frmSend.Refresh();
                    Application.DoEvents();


                    mainThread = new System.Threading.Thread(() =>
                    {
                        while (terminated < items.Count && !forceToStop)
                        {
                            started = items.Count(i => i.Started);
                            executing = items.Count(i => i.Executing);
                            terminated = items.Count(i => i.ExecutionEnded);

                            while ((terminated < items.Count) && (started - terminated < maxThreads) && !forceToStop)
                            {
                                libUpdater.DistributionExecution item = items.Find(i => !i.Started);
                                if (item == null)
                                    break;
                                else
                                {
                                    started += 1;
                                    item.Start();
                                }
                            }
                        }

                        if (forceToStop)
                        {
                            items.ForEach(i => i.Stop());
                            terminated = items.Count(i => i.ExecutionEnded);
                            if (terminated < items.Count)
                            {
                                System.Threading.Thread.Sleep(1000);
                                terminated = items.Count(i => i.ExecutionEnded);
                            }
                        }
                        foreach (clsButtonExecution btnItems in pnlButtons.Controls)
                        {
                            if (btnItems.Text == "Chiudi")
                                btnItems.Enabled = true;
                            else
                                btnItems.Enabled = false;
                        }
                    });
                };
                frmSend.ShowDialog();
                frmSend.Close();
                frmSend.Dispose();
            }
        }

        private class clsButtonExecution : Button
        {

            private bool setting = false;
            public string Text
            {
                get { return base.Text; }
                set { this.SetText(value); } 
            }

            public bool Enabled
            {
                get { return base.Enabled; }
                set { this.SetEnabled(value); } 
            }

            private delegate void delegateSetText(string value);
            private void SetText(string value)
            {
                if (this.InvokeRequired)
                    this.Invoke(new delegateSetText(SetText), value);
                else
                {
                    base.Text = value;
                }
            }

            private delegate void delegateSetEnabled(bool value);
            private void SetEnabled(bool value)
            {
                if (this.InvokeRequired)
                    this.Invoke(new delegateSetEnabled(SetEnabled), value);
                else
                {
                    base.Enabled = value;
                }
            }

            
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }
    }
}
