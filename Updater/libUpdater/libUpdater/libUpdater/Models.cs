﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libUpdater
{
    public class clsUpdaterPackeg
    {
        public long PackegOrder { get; set; }
        public string PackegName { get; set; }
        public long PackegState { get; set; }
        public string VersionRelease { get; set; }
        public DateTime StartUpdate { get; set; }
        public DateTime Inserted { get; set; }
        public byte[] Packeg { get; set; }
        public long Compressed { get; set; }
        public string FilesList { get; set; }
    }

    public class clsUpdaterApplication
    {
        public string ApplicationName { get; set; }
        public string TableApplicationName { get; set; }
        public List<clsUpdaterPackeg> Items { get; set; }

        public string FullApplicationPackegPath { get; set; }
    }

    public class Distribution
    {
        public string OracleUser { get; set; }
        public string OraclePassword { get; set; }
        public string OracleTnsName { get; set; }
        public List<clsUpdaterApplication> Applications { get; set; }

        public Distribution()
        {
            OracleUser = "";
            OraclePassword = "";
            OracleTnsName = "";
            Applications = new List<clsUpdaterApplication>();
        }
    }

    public class DistributionExecution
    {
        public Distribution Distribution { get; set; }
        public System.Threading.Thread Thread { get; set; }
        public System.Windows.Forms.TextBox TextBox { get; set; }

        public bool Started = false;
        public bool Executing = false;
        public bool ExecutionEnded = false;
        private bool requestToStop = false;

        public DistributionExecution(Distribution distribution, System.Windows.Forms.TextBox textBox)
        {
            this.Distribution = distribution;
            this.TextBox = textBox;
            this.Thread = new System.Threading.Thread(new System.Threading.ThreadStart(Execute));
        }

        public void Start()
        {
            this.Started = true;
            this.Thread.Start();
        }

        public void Stop()
        {
            this.requestToStop = true;
        }


        public void Execute()
        {
            DateTime dNow = DateTime.Now;
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
            Exception error = null;
            Executing = true;
            Wintic.Data.IConnection conn = null;
            try
            {
                SetTextBoxValue(string.Format(" {0} {1} : {2}", dNow.ToLongTimeString(), stopWatch.Elapsed.ToString(), "Connessione..."));
                conn = new Wintic.Data.oracle.CConnectionOracle();
                conn.ConnectionString = string.Format("user={0};password={1};data source={2};", Distribution.OracleUser, Distribution.OraclePassword, Distribution.OracleTnsName);
                conn.Open();
                SetTextBoxValue(string.Format(" {0} {1} : {2}", dNow.ToLongTimeString(), stopWatch.Elapsed.ToString(), "connesso."));

                if (!this.requestToStop)
                {
                    int index = 0;
                    foreach (clsUpdaterApplication app in this.Distribution.Applications)
                    {
                        SetTextBoxValue(string.Format("{0} ({1} di {2})", app.ApplicationName, index.ToString(), this.Distribution.Applications.Count.ToString()));
                        
                        libUpdater.Updater updater = new Updater();
                        updater.OutMessageUpload += (message) => 
                        {
                            dNow = DateTime.Now;
                            SetTextBoxValue(string.Format(" {0} {1} : {2}", dNow.ToLongTimeString(), stopWatch.Elapsed.ToString(), message)); 
                        };
                        updater.InterfaceUploadUpdate(conn, out error, app.FullApplicationPackegPath, false);
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex;
                SetTextBoxValue("Errore", ex);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                dNow = DateTime.Now;
                SetTextBoxValue(string.Format("FINE {0} {1}", dNow.ToLongTimeString(), stopWatch.Elapsed.ToString()));
                SetTextBoxValue("_".PadRight(50, '_'));
            }
            stopWatch.Stop();
            this.ExecutionEnded = true;
        }

        public delegate void delegateSetTextBoxValue(string message, Exception exception = null);
        public void SetTextBoxValue(string message, Exception exception = null)
        {
            if (this.TextBox.InvokeRequired)
            {
                delegateSetTextBoxValue d = new delegateSetTextBoxValue(SetTextBoxValue);
                this.TextBox.Invoke(d, message, exception);
            }
            else
            {
                this.TextBox.Text += message + "\r\n";
                if (exception != null)
                    this.TextBox.Text += exception.Message + "\r\n";
                this.TextBox.SelectionLength = 0;
                this.TextBox.SelectionStart = this.TextBox.Text.Length;
                this.TextBox.ScrollToCaret();
                System.Windows.Forms.Application.DoEvents();
            }
        }
    }


}
