﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace libUpdater
{
    public class ItemUpdater
    {
        //public string 
    }
    public class Updater
    {
        //public static string TableNameAPPS_VERSION = "ACT_APP_VERSION";
        public static string CheckOracleConnectionString32x64 = "";

        public delegate void delegateOutMessage(string message);
        public static event delegateOutMessage OutMessage;

        public event delegateOutMessage OutMessageUpload;

        private static void RaiseOutMessage(string message)
        {
            if (OutMessage != null)
            {
                OutMessage(message);
            }
        }

        private static void RaiseOutMessage(Exception error)
        {
            RaiseOutMessage(error.Message);
        }

        private void RaiseOutMessageUpload(string message)
        {
            if (OutMessageUpload != null) OutMessageUpload(message);
        }

        private void RaiseOutMessageUpload(Exception error)
        {
            RaiseOutMessageUpload(error.Message);
        }

        public static bool CheckTableUpdates(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, Updater updater = null)
        {
            bool lRet = false;
            Error = null;
            string operation = "";

            operation = "Verifica delle tabelle degli aggiornamenti";
            RaiseOutMessage(operation);
            if (updater != null) updater.RaiseOutMessageUpload(operation);
            try
            {
                StringBuilder oSB = null;
                Wintic.Data.clsParameters oPars = null;
                Wintic.Data.IRecordSet oRS = null;
                bool lExecute = false;
                oSB = new StringBuilder("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = :pTABLE_NAME");
                oPars = new Wintic.Data.clsParameters(":pTABLE_NAME", TableNameAPPS_VERSION);
                oRS = (Wintic.Data.IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                lExecute = (oRS.EOF);
                if (lExecute)
                {
                    operation = "creazione delle tabelle degli aggiornamenti";
                    RaiseOutMessage(operation);
                    if (updater != null) updater.RaiseOutMessageUpload(operation);
                    oSB = new StringBuilder();
                    oSB.Append(string.Format("CREATE TABLE {0}", TableNameAPPS_VERSION));
                    oSB.Append(" (");
                    oSB.Append(" APPLICATION_NAME VARCHAR2(255) NOT NULL,");
                    oSB.Append(" PACKEG_ORDER NUMBER DEFAULT 0 NOT NULL,");
                    oSB.Append(" PACKEG_NAME VARCHAR2(255) NOT NULL,");
                    oSB.Append(" PACKEG_STATE NUMBER DEFAULT 0 NOT NULL,");
                    oSB.Append(" VERSION_RELEASE VARCHAR2(255) NOT NULL,");
                    oSB.Append(" START_UPDATE DATE DEFAULT SYSDATE NOT NULL,");
                    oSB.Append(" INSERTED DATE DEFAULT SYSDATE NOT NULL,");
                    oSB.Append(" COMPRESSED NUMBER DEFAULT 0 NOT NULL,");
                    oSB.Append(" FILES_LIST CLOB,");
                    oSB.Append(" PACKEG BLOB");
                    oSB.Append(" )");
                    Connection.ExecuteNonQuery(oSB.ToString());
                }

                oRS.Close();


                oSB = new StringBuilder("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = :pTABLE_NAME");
                oPars = new Wintic.Data.clsParameters(":pTABLE_NAME", TableNameAPPS_VERSION + "_PC");
                oRS = (Wintic.Data.IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                lExecute = (oRS.EOF);
                if (lExecute)
                {
                    operation = "creazione delle tabelle degli aggiornamenti";
                    RaiseOutMessage(operation);
                    if (updater != null) updater.RaiseOutMessageUpload(operation);
                    oSB = new StringBuilder();
                    oSB.Append(string.Format("CREATE TABLE {0}", TableNameAPPS_VERSION + "_PC"));
                    oSB.Append(" (");
                    oSB.Append(" APPLICATION_NAME VARCHAR2(255) NOT NULL,");
                    oSB.Append(" VERSION_RELEASE VARCHAR2(255) NOT NULL,");
                    oSB.Append(" PC_NAME VARCHAR2(255) NOT NULL,");
                    oSB.Append(" INSERTED DATE DEFAULT SYSDATE NOT NULL");
                    oSB.Append(" )");
                    Connection.ExecuteNonQuery(oSB.ToString());
                }

                oRS.Close();

                lRet = true;
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore" + "\r\n" + operation + "\r\n" + "{0}", ex.Message));
                RaiseOutMessage(Error);
                if (updater != null) updater.RaiseOutMessageUpload(Error);
                lRet = false;
            }
            return lRet;
        }

        public static clsUpdaterApplication LoadApplicationFromDatabase(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, string applicationName)
        {
            Error = null;
            return LoadApplicationFromDatabase(Connection, TableNameAPPS_VERSION, out Error, applicationName, true);
        }

        public static clsUpdaterApplication LoadApplicationFromDatabase(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, string applicationName, bool loadPackegData, Updater updater = null)
        {
            clsUpdaterApplication result = null;
            Error = null;

            string operation = "";

            operation = string.Format("Lettura aggiornamenti per l'applicazione {0}", applicationName);
            RaiseOutMessage(operation);
            if (updater != null) updater.RaiseOutMessageUpload(operation);

            try
            {
                StringBuilder oSB = null;
                Wintic.Data.clsParameters oPars = null;
                Wintic.Data.IRecordSet oRS = null;
                if (loadPackegData)
                    oSB = new StringBuilder(string.Format("SELECT * FROM {0} WHERE APPLICATION_NAME = :pAPPLICATION_NAME ORDER BY PACKEG_ORDER", TableNameAPPS_VERSION));
                else
                {
                    oSB = new StringBuilder(string.Format("SELECT APPLICATION_NAME, PACKEG_ORDER, PACKEG_NAME, PACKEG_STATE, VERSION_RELEASE, START_UPDATE, INSERTED, COMPRESSED, FILES_LIST FROM {0} WHERE APPLICATION_NAME = :pAPPLICATION_NAME ORDER BY PACKEG_ORDER", TableNameAPPS_VERSION));
                }
                oPars = new Wintic.Data.clsParameters(":pAPPLICATION_NAME", applicationName);
                oRS = (Wintic.Data.IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                if (!oRS.EOF)
                {
                    result = new clsUpdaterApplication();
                    result.ApplicationName = applicationName;
                    result.TableApplicationName = TableNameAPPS_VERSION;
                    result.Items = new List<clsUpdaterPackeg>();
                    while (!oRS.EOF && Error == null)
                    {
                        clsUpdaterPackeg oPackeg = new clsUpdaterPackeg();
                        oPackeg.PackegOrder = long.Parse(oRS.Fields("PACKEG_ORDER").Value.ToString());
                        oPackeg.PackegName = oRS.Fields("PACKEG_NAME").Value.ToString();
                        oPackeg.PackegState = long.Parse(oRS.Fields("PACKEG_STATE").Value.ToString());
                        oPackeg.VersionRelease = oRS.Fields("VERSION_RELEASE").Value.ToString();
                        oPackeg.StartUpdate = (DateTime)oRS.Fields("START_UPDATE").Value;
                        oPackeg.Inserted = (DateTime)oRS.Fields("INSERTED").Value;
                        oPackeg.Compressed = long.Parse(oRS.Fields("COMPRESSED").Value.ToString());
                        oPackeg.FilesList = oRS.Fields("FILES_LIST").ToString();
                        oPackeg.Packeg = new byte[] { };
                        if (loadPackegData)
                        {
                            try
                            {
                                oPackeg.Packeg = (byte[])oRS.Fields("PACKEG").Value;
                                result.Items.Add(oPackeg);
                            }
                            catch (Exception ex)
                            {
                                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                                oPackeg.Packeg = new byte[] { };
                                RaiseOutMessage(Error);
                                if (updater != null) updater.RaiseOutMessageUpload(Error);
                            }
                        }
                        else
                            result.Items.Add(oPackeg);
                        oRS.MoveNext();
                    }
                }
                oRS.Close();
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore" + "\r\n" + operation + "\r\n" + "{0}", ex.Message));
                RaiseOutMessage(Error);
                if (updater != null) updater.RaiseOutMessageUpload(Error);
            }
            return result;
        }

        public static bool ApplicationsIsEquals(clsUpdaterApplication a, clsUpdaterApplication b)
        {
            bool result = false;
            if (a == null && b == null)
            {
                result = true;
            }
            else if ((a != null && b == null) || (a == null && b != null))
            {
                result = false;
            }
            else
            {
                result = a.ApplicationName == b.ApplicationName;
                if (result)
                {
                    int nCountItemsA = (a.Items == null ? 0 : a.Items.Count);
                    int nCountItemsB = (b.Items == null ? 0 : b.Items.Count);
                    if (nCountItemsA != nCountItemsB)
                    {
                        result = false;
                    }
                    else if (nCountItemsA > 0)
                    {
                        foreach (clsUpdaterPackeg pA in a.Items)
                        {
                            clsUpdaterPackeg pBequals = null;
                            foreach (clsUpdaterPackeg pB in b.Items)
                            {
                                if (PackegesIsEquals(pA, pB))
                                {
                                    pBequals = pB;
                                    break;
                                }
                            }
                            if (pBequals == null)
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static bool PackegesIsEquals(clsUpdaterPackeg a, clsUpdaterPackeg b)
        {
            bool result = false;
            if (a == null && b == null)
            {
                result = true;
            }
            else if ((a != null && b == null) || (a == null && b != null))
            {
                result = false;
            }
            else
            {
                result = a.VersionRelease == b.VersionRelease;
            }
            return result;
        }

        public static clsUpdaterPackeg GetPackegFromFile(out Exception Error, string file)
        {
            clsUpdaterPackeg result = null;
            Error = null;
            string operation = "";

            operation = string.Format("Applicazione in esecuzione da file" + "\r\n" + "{0}", file);
            RaiseOutMessage(operation);
            try
            {
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(file);
                if (oFileInfo.Exists)
                {
                    result = new clsUpdaterPackeg();
                    result.PackegName = oFileInfo.Name;
                    System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFile(file);
                    result.VersionRelease = assembly.GetName().Version.ToString();
                    result.Packeg = System.IO.File.ReadAllBytes(file);
                    result.Inserted = DateTime.Now;
                    result.StartUpdate = DateTime.Now;
                    result.PackegOrder = 0;
                    result.PackegState = 1;
                    result.Compressed = 0;
                    result.FilesList = "";
                }
                else
                {
                    Error = new Exception(string.Format("Errore" + "\r\n" + operation + "\r\n" + "{0}", "file non trovato"));
                }
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore" + "\r\n" + operation + "\r\n" + "{0}", ex.Message));
                result = null;
                RaiseOutMessage(Error);
            }
            return result;
        }

        public static clsUpdaterPackeg GetMainPackegFromRunningApplication(out Exception Error)
        {
            clsUpdaterPackeg result = null;
            Error = null;
            string operation = "";

            operation = "Applicazione in esecuzione...";
            RaiseOutMessage(operation);
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                result = new clsUpdaterPackeg();
                result.PackegName = oFileInfo.Name;
                result.VersionRelease = assembly.GetName().Version.ToString();
                result.Inserted = DateTime.Now;
                result.PackegOrder = 0;
                result.PackegState = 1;
                operation = string.Format("Applicazione in esecuzione {0} {1}", result.PackegName, result.VersionRelease);
                RaiseOutMessage(operation);
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                result = null;
                RaiseOutMessage(Error);
            }
            return result;
        }

        public static bool CheckForUpdatesRunninApplication(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, out string applicationName)
        {
            bool result = false;
            Error = null;
            applicationName = "";
            string operation = "";
            operation = "Verifica aggiornamenti...";
            RaiseOutMessage(operation);

            clsUpdaterApplication appMakeUpdater = LoadApplicationFromDatabase(Connection, TableNameAPPS_VERSION, out Error, "makeUpdater.exe", false);
            if (appMakeUpdater != null)
            {
                clsUpdaterPackeg currentPackeg = GetMainPackegFromRunningApplication(out Error);
                applicationName = (currentPackeg != null ? currentPackeg.PackegName : "");
                if (Error == null && currentPackeg != null)
                {
                    clsUpdaterApplication application = LoadApplicationFromDatabase(Connection, TableNameAPPS_VERSION, out Error, currentPackeg.PackegName, false);
                    if (Error == null && application != null && application.Items != null)
                    {
                        foreach (clsUpdaterPackeg packeg in application.Items)
                        {
                            if (packeg.Compressed > 0)
                            {
                                if (packeg.PackegName == currentPackeg.PackegName + ".zip")
                                {
                                    if (PackegesIsEquals(packeg, currentPackeg))
                                    {
                                        result = false;
                                    }
                                    else
                                    {
                                        result = (String.Compare(currentPackeg.VersionRelease, packeg.VersionRelease, comparisonType: StringComparison.OrdinalIgnoreCase) < 0) && (packeg.StartUpdate < DateTime.Now);
                                    }
                                    if (result)
                                        break;
                                }
                            }
                            else
                            {
                                if (packeg.PackegName == currentPackeg.PackegName)
                                {
                                    if (PackegesIsEquals(packeg, currentPackeg))
                                    {
                                        result = false;
                                    }
                                    else
                                    {
                                        result = (String.Compare(currentPackeg.VersionRelease, packeg.VersionRelease, comparisonType: StringComparison.OrdinalIgnoreCase) < 0) && (packeg.StartUpdate < DateTime.Now);
                                    }
                                    if (result)
                                        break;
                                }
                            }
                        }

                        if (Error == null && !result && currentPackeg != null)
                        {
                            //currentPackeg
                            string namePc = System.Environment.MachineName.Trim().ToUpper();
                            StringBuilder oSBPcVersion = null;
                            Wintic.Data.clsParameters oParsPcVersion = null;
                            Wintic.Data.IRecordSet oRSPcVersion = null;
                            bool appendPcVersion = false;

                            oSBPcVersion = new StringBuilder();
                            oSBPcVersion.Append(string.Format("SELECT VERSION_RELEASE FROM {0} WHERE APPLICATION_NAME = :pAPPLICATION_NAME AND PC_NAME = :pPC_NAME", TableNameAPPS_VERSION + "_PC"));
                            oParsPcVersion = new Wintic.Data.clsParameters();
                            oParsPcVersion.Add(":pAPPLICATION_NAME", currentPackeg.PackegName);
                            oParsPcVersion.Add(":pPC_NAME", namePc);
                            oRSPcVersion = (Wintic.Data.IRecordSet)Connection.ExecuteQuery(oSBPcVersion.ToString(), oParsPcVersion);

                            if (!oRSPcVersion.EOF)
                            {
                                if (oRSPcVersion.Fields("VERSION_RELEASE").IsNull || oRSPcVersion.Fields("VERSION_RELEASE").Value.ToString() != currentPackeg.VersionRelease)
                                {
                                    oSBPcVersion = new StringBuilder();
                                    oSBPcVersion.Append(string.Format("DELETE FROM {0} WHERE APPLICATION_NAME = :pAPPLICATION_NAME AND PC_NAME = :pPC_NAME", TableNameAPPS_VERSION + "_PC"));
                                    oParsPcVersion = new Wintic.Data.clsParameters();
                                    oParsPcVersion.Add(":pAPPLICATION_NAME", currentPackeg.PackegName);
                                    oParsPcVersion.Add(":pPC_NAME", namePc);
                                    Connection.ExecuteNonQuery(oSBPcVersion.ToString(), oParsPcVersion);
                                    appendPcVersion = true;
                                }
                            }
                            else
                                appendPcVersion = true;

                            oRSPcVersion.Close();


                            if (appendPcVersion)
                            {
                                oSBPcVersion = new StringBuilder();
                                oParsPcVersion = new Wintic.Data.clsParameters();
                                oParsPcVersion.Add(":pAPPLICATION_NAME", currentPackeg.PackegName);
                                oParsPcVersion.Add(":pVERSION_RELEASE", currentPackeg.VersionRelease);
                                oParsPcVersion.Add(":pPC_NAME", namePc);
                                oSBPcVersion.Append(string.Format("INSERT INTO {0} (APPLICATION_NAME, VERSION_RELEASE, PC_NAME, INSERTED) VALUES (:pAPPLICATION_NAME, :pVERSION_RELEASE, :pPC_NAME, SYSDATE)", TableNameAPPS_VERSION + "_PC"));
                                Connection.ExecuteNonQuery(oSBPcVersion.ToString(), oParsPcVersion);
                            }
                        }
                    }
                    else
                    {
                        if (Error != null) result = false;
                    }
                }
                else
                {
                    if (Error != null) result = false;
                }
                if (Error != null)
                {
                    RaiseOutMessage(Error);
                }
            }
            
            return result;
        }

        public static bool DownloadUpdates(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, string downloadFolder)
        {
            bool result = false;
            Error = null;
            string operation = "";

            operation = "Download aggiornamenti";
            RaiseOutMessage(operation);
            RaiseOutMessage("in " + downloadFolder);

            clsUpdaterPackeg currentPackeg = GetMainPackegFromRunningApplication(out Error);
            if (Error == null && currentPackeg != null)
            {
                result = DownloadUpdates(Connection, TableNameAPPS_VERSION, out Error, currentPackeg.PackegName, downloadFolder);
            }
            return result;
        }

        public static bool DownloadUpdates(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, out Exception Error, string applicationName, string downloadFolder)
        {
            bool result = false;
            Error = null;
            string operation = "";

            operation = "Download aggiornamenti";
            RaiseOutMessage(operation);

            clsUpdaterApplication application = LoadApplicationFromDatabase(Connection, TableNameAPPS_VERSION, out Error, applicationName);
            System.IO.DirectoryInfo downloadFolderInfo = new System.IO.DirectoryInfo(downloadFolder);
            if (Error == null && application != null && application.Items != null && application.Items.Count > 0)
            {
                foreach (clsUpdaterPackeg packeg in application.Items)
                {
                    operation = string.Format("Download aggiornamenti {0}", packeg.PackegName);
                    if (packeg.VersionRelease != null)
                    {
                        operation = string.Format("Download versione {0}", packeg.VersionRelease);
                    }
                    
                    RaiseOutMessage(operation);
                    string cFile = downloadFolderInfo.FullName + @"\" + packeg.PackegName;
                    System.IO.File.WriteAllBytes(cFile, packeg.Packeg);
                    if (packeg.PackegName.Trim().ToUpper().EndsWith(".ZIP"))
                    {
                        // unzip
                        operation = string.Format("Download aggiornamenti unzip {0}", packeg.PackegName);
                        RaiseOutMessage(operation);

                        ICSharpCode.SharpZipLib.Zip.ZipInputStream oStreamInput = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(System.IO.File.OpenRead(cFile));
                        ICSharpCode.SharpZipLib.Zip.ZipEntry oZipEntry = oStreamInput.GetNextEntry();
                        while (oZipEntry != null)
                        {
                            string path = downloadFolder + @"\" + System.IO.Path.GetDirectoryName(oZipEntry.Name);
                            string file = downloadFolder + @"\" + System.IO.Path.GetFileName(oZipEntry.Name);
                            file = downloadFolder + @"\" + oZipEntry.Name;
                            if (oZipEntry.IsDirectory)
                            {
                                if (!System.IO.Directory.Exists(path))
                                {
                                    System.IO.Directory.CreateDirectory(path);
                                }
                            }
                            else if (oZipEntry.IsFile && !string.IsNullOrEmpty(file) && file != string.Empty)
                            {
                                if (System.IO.File.Exists(file))
                                {
                                    System.IO.File.Delete(file);
                                }
                                System.IO.FileStream oStreamWriter = System.IO.File.Create(file);
                                int size = 2048;
                                byte[] buffer = new byte[size];
                                while (true)
                                {
                                    size = oStreamInput.Read(buffer, 0, buffer.Length);
                                    if (size > 0)
                                    {
                                        oStreamWriter.Write(buffer, 0, size);
                                    }
                                    else
                                    {
                                        oStreamWriter.Flush();
                                        oStreamWriter.Close();
                                        break;
                                    }
                                }


                            }
                            oZipEntry = oStreamInput.GetNextEntry();
                        }
                        oStreamInput.Close();
                        oStreamInput.Dispose();

                        // delete
                        operation = string.Format("Download aggiornamenti eliminazione {0}", packeg.PackegName);
                        RaiseOutMessage(operation);
                        System.IO.File.Delete(cFile);
                    }

                }
                result = true;
            }
            else
            {
                if (Error != null) result = false;
                else
                {
                    if (application == null || application.Items == null || application.Items.Count == 0)
                    {
                        Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", "Impossibile leggere gli aggiornamenti."));
                    }
                }
            }
            return result;
        }

        private static bool CopyDownloadFolder(out Exception Error, string downloadFolder, string destinationFolder, string fileParamsRestart, out string fullAppExternArgs)
        {
            bool result = false;
            Error = null;
            string operation = "";
            fullAppExternArgs = "";



            operation = string.Format("Copia files di installazione...");
            RaiseOutMessage(operation);
            RaiseOutMessage(downloadFolder);
            RaiseOutMessage(destinationFolder);

            string installerFolder = downloadFolder + @"\makeUpdater";

            if (!string.IsNullOrEmpty(fileParamsRestart) && System.IO.File.Exists(fileParamsRestart))
            {
                RaiseOutMessage("Lettura file di parametri per riavvio applicazione...");
                fullAppExternArgs = System.IO.File.ReadAllText(fileParamsRestart);
            }
            try
            {
                System.IO.DirectoryInfo downloadFolderInfo = new System.IO.DirectoryInfo(downloadFolder);
                System.IO.DirectoryInfo destinationFolderInfo = new System.IO.DirectoryInfo(destinationFolder);

                foreach (System.IO.FileInfo oInfoCopy in downloadFolderInfo.GetFiles())
                {
                    string destinationFile = destinationFolderInfo.FullName + @"\" + oInfoCopy.Name;
                    
                    if (System.IO.File.Exists(destinationFile))
                    {
                        operation = string.Format("Eliminazione precedente versione {0}", oInfoCopy.Name);
                        RaiseOutMessage(operation);
                        System.IO.File.Delete(destinationFile);
                    }
                    operation = string.Format("Copia {0}", destinationFile);
                    RaiseOutMessage(operation);
                    System.IO.File.Copy(oInfoCopy.FullName, destinationFile);

                    
                    try
                    {
                        operation = string.Format("Eliminazione file di installazione {0}", oInfoCopy.Name);
                        RaiseOutMessage(operation);
                        System.IO.File.Delete(oInfoCopy.FullName);
                    }
                    catch (Exception)
                    {
                    }
                }
                foreach (System.IO.DirectoryInfo oInfoCopy in downloadFolderInfo.GetDirectories())
                {
                    if (oInfoCopy.FullName != installerFolder)
                    {
                        if (!System.IO.Directory.Exists(destinationFolder + @"\" + oInfoCopy.Name))
                        {
                            operation = string.Format("Creazione cartella {0}", oInfoCopy.Name);
                            System.IO.Directory.CreateDirectory(destinationFolder + @"\" + oInfoCopy.Name);
                        }
                        string tempPars = "";
                        result = CopyDownloadFolder(out Error, downloadFolder + @"\" + oInfoCopy.Name, destinationFolder + @"\" + oInfoCopy.Name, "", out tempPars);
                        if (result)
                        {
                            System.IO.DirectoryInfo oDirInfoToDelete = new System.IO.DirectoryInfo(downloadFolder + @"\" + oInfoCopy.Name);
                            operation = string.Format("Eliminazione cartella di installazione {0}", oInfoCopy.Name);
                            RaiseOutMessage(operation);
                            oDirInfoToDelete.Delete(true);
                        }
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                RaiseOutMessage(Error);
            }
            return result;
        }

        public static void CheckDownloadFolderToRemove()
        {
            string applicationFolder = "";
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                applicationFolder = oFileInfo.Directory.FullName;
                System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(applicationFolder + @"\localApplicationDownloadFolder");
                if (oDirInfo.Exists)
                {
                    frmWait.OpenWait("Eliminazione files di installazione...");
                    CleanInstallationFilesInternal(oDirInfo.FullName);
                    frmWait.CloseWait();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool GetLocalApplicationDownloadFolder(out Exception Error, out string applicationFolder, out string downloadFolder, bool create)
        {
            bool result = false;
            applicationFolder = "";
            downloadFolder = "";
            Error = null;
            string operation = "";

            operation = "Verifica" + (create ? "/Creazione" : "") + " cartella di installazione...";
            RaiseOutMessage(operation);

            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                System.IO.FileInfo oFileInfo = new System.IO.FileInfo(assembly.Location);
                applicationFolder = oFileInfo.Directory.FullName;

                operation = string.Format("Verifica" + (create ? "/Creazione" : "") + " cartella di installazione" + "\r\n" + "cartella dell'applicazione:" + "\r\n" + "{0}", applicationFolder);
                RaiseOutMessage(operation);

                System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(applicationFolder + @"\localApplicationDownloadFolder");
                if (create)
                {
                    if (!oDirInfo.Exists)
                    {
                        operation = string.Format("Verifica" + (create ? "/Creazione" : "") + " cartella di installazione" + "\r\n" + "creazione cartella di installazione:" + "\r\n" + "{0}", oDirInfo.Name);
                        RaiseOutMessage(operation);
                        oDirInfo.Create();
                        oDirInfo = new System.IO.DirectoryInfo(applicationFolder + @"\localApplicationDownloadFolder");
                    }
                    if (oDirInfo.Exists)
                    {
                        downloadFolder = oDirInfo.FullName;
                        result = true;
                    }
                }
                else
                {
                    result = oDirInfo.Exists;
                    if (result)
                    {
                        downloadFolder = oDirInfo.FullName;
                    }
                }
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                downloadFolder = null;
                RaiseOutMessage(Error);
            }
            return result;
        }

        private static void CleanInstallationFiles()
        {
            Exception Error = null;
            try
            {
                string applicationFolder = "";
                string downloadFolder = "";
                if (GetLocalApplicationDownloadFolder(out Error, out applicationFolder, out downloadFolder, false) && !string.IsNullOrEmpty(downloadFolder))
                {
                    string makerExe = applicationFolder + @"\makeUpdater.exe";
                    if (System.IO.File.Exists(makerExe))
                    {
                        System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcesses();
                        System.Diagnostics.Process makerProcess = null;
                        foreach (System.Diagnostics.Process p in processes)
                        {
                            if (!string.IsNullOrEmpty(p.MainWindowTitle))
                            {
                                if (p.MainModule.FileName == makerExe)
                                {
                                    makerProcess = p;
                                    break;
                                }
                            }
                        }
                        if (makerProcess != null)
                        {
                            makerProcess.WaitForExit();
                        }
                        try
                        {
                            System.IO.File.Delete(makerExe);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    CleanInstallationFilesInternal(downloadFolder);
                }
            }
            catch (Exception)
            {
            }
        }

        public static void CleanInstallationFilesInternal(string path, bool first = true)
        {
            try
            {
                foreach (System.IO.FileInfo oFInfo in (new System.IO.DirectoryInfo(path)).GetFiles())
                {
                    System.IO.File.Delete(oFInfo.FullName);
                }
                foreach (System.IO.DirectoryInfo oDInfo in (new System.IO.DirectoryInfo(path)).GetDirectories())
                {
                    CleanInstallationFilesInternal(oDInfo.FullName, false);
                    oDInfo.Delete(true);
                }
                if (first)
                {
                    System.IO.DirectoryInfo oRInfo = new System.IO.DirectoryInfo(path);
                    if (oRInfo.Exists) oRInfo.Delete(true);
                }
            }
            catch (Exception exClean)
            {
                string err = exClean.ToString();
            }
        }

        public static bool DoCheckOracleConnectionString32x64(string applicationName, string installerFolder, out string testerMode)
        {
            bool result = false;
            Exception error = null;
            string operation = "Inizio verifica connessione Oracle...";
            RaiseOutMessage(operation);
            testerMode = "00";
            try
            {
                System.IO.DirectoryInfo dirTester = new System.IO.DirectoryInfo(installerFolder + @"\Tester");

                if (!dirTester.Exists)
                {
                    operation = "Verifica connessione Oracle interrotta: non esiste la cartella di verifica.";
                    RaiseOutMessage(operation);
                }

                if (dirTester.Exists)
                {
                    System.IO.FileInfo corFlags = new System.IO.FileInfo(installerFolder + @"\CorFlags.exe");
                    System.IO.FileInfo testerConn = new System.IO.FileInfo(dirTester.FullName + @"\makeUpdaterTestConnection32x64.exe");

                    if (!corFlags.Exists)
                    {
                        operation = "Verifica connessione Oracle interrotta: non esiste CorFlags.exe.";
                        RaiseOutMessage(operation);
                    }

                    if (!testerConn.Exists)
                    {
                        operation = string.Format("Verifica connessione Oracle interrotta: non esiste {0}.", testerConn.Name);
                        RaiseOutMessage(operation);
                    }


                    if (corFlags.Exists && testerConn.Exists)
                    {
                        string lastMode = "";
                        List<string> modes = new List<string>() { "", "/32BIT+", "/32BIT-" };
                        System.Diagnostics.Process oProcessCorFlags = null;

                        foreach (string mode in modes)
                        {
                            lastMode = mode;

                            operation = string.Format("Verifica connessione Oracle in modalità {0}.", (mode == "" ? "Neutra" : (mode == "/32BIT+" ? "32bit" : "64bit")));
                            RaiseOutMessage(operation);

                            oProcessCorFlags = new System.Diagnostics.Process();
                            oProcessCorFlags.StartInfo = new System.Diagnostics.ProcessStartInfo();
                            oProcessCorFlags.StartInfo.FileName = corFlags.FullName;
                            oProcessCorFlags.StartInfo.Arguments = testerConn.Name + " " + mode;
                            oProcessCorFlags.StartInfo.WorkingDirectory = dirTester.FullName;
                            oProcessCorFlags.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                            try
                            {
                                if (mode != "")
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    oProcessCorFlags.Start();
                                    oProcessCorFlags.WaitForExit();
                                    oProcessCorFlags.Close();
                                    oProcessCorFlags.Dispose();
                                    oProcessCorFlags = null;
                                }

                                System.Diagnostics.Process oProcessTester = new System.Diagnostics.Process();
                                oProcessTester.StartInfo = new System.Diagnostics.ProcessStartInfo();
                                oProcessTester.StartInfo.FileName = testerConn.FullName;
                                oProcessTester.StartInfo.Arguments = Encrypt(CheckOracleConnectionString32x64, kToken, false);
                                oProcessTester.StartInfo.WorkingDirectory = dirTester.FullName;
                                oProcessTester.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                                try
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    oProcessTester.Start();
                                    oProcessTester.WaitForExit();
                                    oProcessTester.Close();
                                    oProcessTester.Dispose();
                                    oProcessTester = null;
                                    
                                    System.IO.FileInfo resultFile = new System.IO.FileInfo(dirTester.FullName + @"\makeUpdaterTestConnection32x64.log");
                                    if (resultFile.Exists)
                                    {
                                        string resultContent = System.IO.File.ReadAllText(resultFile.FullName);
                                        if (resultContent == "OK")
                                        {
                                            testerMode = (mode == "" ? "00" : (mode == "/32BIT+" ? "32" : "64"));
                                            operation = string.Format("Verifica connessione Oracle OK in modalità {0}.", (mode == "" ? "Neutra" : (mode == "/32BIT+" ? "32bit" : "64bit")));
                                            RaiseOutMessage(operation);
                                            result = true;
                                            break;
                                        }
                                        else if (resultContent == "ERR3264")
                                        {
                                            operation = string.Format("Verifica connessione Oracle FALLITA in modalità {0}.", (mode == "" ? "Neutra" : (mode == "/32BIT+" ? "32bit" : "64bit")));
                                            RaiseOutMessage(operation);
                                        }
                                        else
                                        {
                                            operation = string.Format("Verifica connessione Oracle FALLITA per un errore diverso {0}.", resultContent);
                                            RaiseOutMessage(operation);
                                            error = new Exception(string.Format("Errore nella verifica della connessione 32/64 bit:\r\n{0}",resultContent));
                                            break;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    error = new Exception(string.Format("Errore Imprevisto (2) nella verifica della connessione 32/64 bit:\r\n{0}", ex.Message));
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                error = new Exception(string.Format("Errore Imprevisto (1) nella verifica della connessione 32/64 bit:\r\n{0}", ex.Message));
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = new Exception(string.Format("Errore Imprevisto (0) nella verifica della connessione 32/64 bit:\r\n{0}", ex.Message));
            }
            if (error != null)
            {
                System.Windows.Forms.MessageBox.Show(error.ToString());
                operation = string.Format("Verifica connessione Oracle FALLITA.");
                RaiseOutMessage(operation);
            }
            operation = string.Format("Verifica connessione Oracle TERMINATA.");
            return result;
        }

        public static bool CheckForUpdatesAndUpdateRunningApplication(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, string[] ExternalArgs)
        {
            bool result = false;
            Exception Error = null;
            string operation = "";
            
            // CHIUDO OGNI ALTRA FINESTRA DI WAIT
            frmWait.CloseWait();

            frmCheckUpdates oFrmCheckUpdates = new frmCheckUpdates();
            oFrmCheckUpdates.Show();

            operation = "Verifica aggiornamenti";
            RaiseOutMessage(operation);

            if (CheckTableUpdates(Connection, TableNameAPPS_VERSION, out Error) && Error == null)
            {
                try
                {
                    string applicationName = "";
                    bool updateRequested = CheckForUpdatesRunninApplication(Connection, TableNameAPPS_VERSION, out Error, out applicationName);

                    if (Error == null && !updateRequested)
                    {
                        CheckDownloadFolderToRemove();
                    }


                    oFrmCheckUpdates.SetApplicationName(applicationName.Replace(".exe", "").Replace(".EXE", ""));
                    if (Error == null && updateRequested)
                    {
                        oFrmCheckUpdates.UpdateRequired = true;
                        System.Threading.Thread.Sleep(1000);
                        string downloadFolder = "";
                        string applicationFolder = "";
                        string installerFolder = "";
                        if (GetLocalApplicationDownloadFolder(out Error, out applicationFolder, out downloadFolder, true))
                        {
                            if (Error == null)
                            {
                                if (DownloadUpdates(Connection, TableNameAPPS_VERSION, out Error, downloadFolder))
                                {
                                    if (Error == null)
                                    {
                                        installerFolder = downloadFolder + @"\makeUpdater";
                                        if (!System.IO.Directory.Exists(installerFolder))
                                            System.IO.Directory.CreateDirectory(installerFolder);
                                        if (DownloadUpdates(Connection, TableNameAPPS_VERSION, out Error, "makeUpdater.exe", installerFolder))
                                        {
                                            if (Error == null)
                                            {
                                                string testerMode = "";
                                                // se da verificare la connessione al database
                                                if (CheckOracleConnectionString32x64 != "")
                                                {
                                                    DoCheckOracleConnectionString32x64(applicationName, installerFolder, out testerMode);
                                                }
                                                // avvio applicazione temporizzato che segue quanto segue
                                                operation = "avvio processo di aggiornamento...";
                                                RaiseOutMessage(operation);
                                                System.Threading.Thread.Sleep(1000);
                                                string makeUpdaterFileExe = installerFolder + @"\makeUpdater.exe";
                                                if (System.IO.File.Exists(makeUpdaterFileExe))
                                                {
                                                    string fileParamsRestart = installerFolder + @"\makeUpdater.exe.restart.config";
                                                    if (System.IO.File.Exists(fileParamsRestart))
                                                    {
                                                        System.IO.File.Delete(fileParamsRestart);
                                                    }
                                                    System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                                                    bool lExternalArgs = false;
                                                    if (ExternalArgs != null && ExternalArgs.Length > 0)
                                                    {
                                                        string fullAppExternArgs = "";
                                                        foreach (string cExternArg in ExternalArgs)
                                                        {
                                                            fullAppExternArgs += " " + cExternArg;
                                                        }
                                                        System.IO.File.AppendAllText(fileParamsRestart, fullAppExternArgs);
                                                        lExternalArgs = true;
                                                    }
                                                    string args = string.Format("/APP_NAME={0} /DOWNLOAD_FOLDER={1} /APPLICATION_FOLDER={2} /SECONDS_WAIT={3} /TABLE_APP_VERSION={4}", applicationName, downloadFolder, applicationFolder, "10", TableNameAPPS_VERSION);
                                                    if (lExternalArgs)
                                                    {
                                                        args += " /APP_ARGS_FILE=makeUpdater.exe.restart.config";
                                                    }
                                                    if (testerMode != "")
                                                    {
                                                        args += " /APP_CONN_LOG=" + testerMode;
                                                    }
                                                    oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo();
                                                    oProcess.StartInfo.FileName = makeUpdaterFileExe;
                                                    oProcess.StartInfo.Arguments = args;
                                                    oProcess.StartInfo.WorkingDirectory = downloadFolder;
                                                    oProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;

                                                    try
                                                    {
                                                        System.GC.Collect();
                                                        oProcess.Start();
                                                        result = true;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        System.Windows.Forms.MessageBox.Show(ex.ToString());
                                                    }
                                                }
                                                else
                                                {

                                                }
                                                
                                                // FARE stop applicazione corrente dall'applicazione stessa
                                                operation = "Arresto applicazione in esecuzione";
                                                RaiseOutMessage(operation);

                                                try
                                                {
                                                    oFrmCheckUpdates.Close();
                                                    oFrmCheckUpdates.Dispose();
                                                }
                                                catch (Exception)
                                                {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (Error == null && !updateRequested)
                    {
                        // EVENTUALE AGGIORNAMENTO VERSIONE TABELLA PER POSTAZIONE

                    }
                }
                catch (Exception ex)
                {
                    Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                    RaiseOutMessage(Error);
                }
                finally
                {
                    try
                    {
                        if (oFrmCheckUpdates != null)
                        {
                            oFrmCheckUpdates.Close();
                            oFrmCheckUpdates.Dispose();
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            if (Error != null)
                MessageBox.Show("Errore durante la verifica di aggiornamenti:\r\n" + Error.ToString());

            return result;
        }


        public static bool CheckForUpdatesAndUpdateRunningWinService(Wintic.Data.IConnection Connection, string TableNameAPPS_VERSION, string stopService, string unistallService, string installService, string restartService)
        {
            bool result = false;
            Exception Error = null;
            string operation = "";

            operation = "Verifica aggiornamenti...";
            RaiseOutMessage(operation);

            if (CheckTableUpdates(Connection, TableNameAPPS_VERSION, out Error) && Error == null)
            {
                try
                {
                    string applicationName = "";
                    bool updateRequested = CheckForUpdatesRunninApplication(Connection, TableNameAPPS_VERSION, out Error, out applicationName);
                    if (Error == null && updateRequested)
                    {
                        string downloadFolder = "";
                        string applicationFolder = "";
                        if (GetLocalApplicationDownloadFolder(out Error, out applicationFolder, out downloadFolder, true))
                        {
                            if (Error == null)
                            {
                                if (DownloadUpdates(Connection, TableNameAPPS_VERSION, out Error, downloadFolder))
                                {
                                    if (Error == null)
                                    {
                                        if (DownloadUpdates(Connection, TableNameAPPS_VERSION, out Error, "makeUpdater.exe", downloadFolder))
                                        {
                                            if (Error == null)
                                            {
                                                // avvio applicazione temporizzato che segue quanto segue
                                                operation = "Inizializzazione processo di aggiornamento...";
                                                RaiseOutMessage(operation);
                                                System.Threading.Thread.Sleep(1000);
                                                string makeUpdaterFileExe = downloadFolder + @"\makeUpdater.exe";
                                                if (System.IO.File.Exists(makeUpdaterFileExe))
                                                {
                                                    System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                                                    string args = string.Format("/APP_NAME={0} /APP_TYPE=WIN_SERVICE /DOWNLOAD_FOLDER={1} /APPLICATION_FOLDER={2} /SECONDS_WAIT={3} /TABLE_APP_VERSION={4} /APPLICATION_STOP={5} /APPLICATION_UNINSTALL={6}  /APPLICATION_INSTALL={7} /APPLICATION_RESTART={8}", applicationName, downloadFolder, applicationFolder, "10", TableNameAPPS_VERSION, stopService, unistallService, installService, restartService);

                                                    operation = string.Format("Argomenti processo di aggiornamento:" + "\r\n" + "{0}", args);
                                                    RaiseOutMessage(operation);

                                                    oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo();
                                                    oProcess.StartInfo.FileName = makeUpdaterFileExe;
                                                    oProcess.StartInfo.Arguments = args;
                                                    oProcess.StartInfo.WorkingDirectory = downloadFolder;
                                                    oProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                                                    operation = "AGGIORNAMENTO...";
                                                    RaiseOutMessage(operation);

                                                    try
                                                    {
                                                        System.GC.Collect();
                                                        oProcess.Start();
                                                        result = true;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        RaiseOutMessage("Errore avvio aggiornamento " + ex.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    operation = string.Format("Applicazione di aggiornamento non trovata:" + "\r\n" + "{0}", makeUpdaterFileExe);
                                                    RaiseOutMessage(operation);
                                                }

                                                // FARE stop applicazione corrente dall'applicazione stessa
                                                operation = "Arresto applicazione in esecuzione.";
                                                RaiseOutMessage(operation);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    operation = "Nessun aggiornamento trovato nel database.";
                                    RaiseOutMessage(operation);
                                }
                            }
                        }
                    }
                    else if (Error == null && !updateRequested)
                    {
                        operation = "Nessun aggiornamento richiesto.";
                        RaiseOutMessage(operation);
                    }
                    CleanInstallationFiles();
                }
                catch (Exception ex)
                {
                    Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                    RaiseOutMessage(Error);
                }
                finally
                {
                    
                }
            }

            return result;
        }

        private const string kToken = "F5EE450FE2119D0B7EDF48FA";

        private static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static bool MakeUpdates(out Exception Error, string applicationFolder, string downloadFolder, string applicationName, string fileParamsRestart, string applicationType, string applicationConnectionLog, string TableNameAPPS_VERSION)
        {
            Error = null;
            string operation = "";

            operation = "Aggiornamento";
            RaiseOutMessage(operation);
            try
            {
                string fullAppExternArgs = "";
                if (CopyDownloadFolder(out Error, downloadFolder, applicationFolder, fileParamsRestart, out fullAppExternArgs))
                {
                    if (Error == null)
                    {
                        if (!string.IsNullOrEmpty(applicationName))
                        {
                            if (applicationConnectionLog != "")
                            {
                                RaiseOutMessage("Impostazione avvio 32/64bit per versione Oracle client...");
                                RaiseOutMessage(applicationConnectionLog);
                                if (applicationConnectionLog == "32" || applicationConnectionLog == "64")
                                {
                                    operation = string.Format("Conversione applicazione modalità " + (applicationConnectionLog == "32" ? "32bit" : "64bit"), applicationName);
                                    RaiseOutMessage(operation);
                                    try
                                    {
                                        RaiseOutMessage("Conversione...");
                                        string installerFolder = downloadFolder + @"\makeUpdater";
                                        System.IO.FileInfo corFlags = new System.IO.FileInfo(installerFolder + @"\CorFlags.exe");
                                        System.Diagnostics.Process oProcessCorFlags = new System.Diagnostics.Process();
                                        oProcessCorFlags.StartInfo = new System.Diagnostics.ProcessStartInfo();
                                        oProcessCorFlags.StartInfo.FileName = corFlags.FullName;
                                        oProcessCorFlags.StartInfo.Arguments = applicationName + " " + (applicationConnectionLog == "32" ? "/32BIT+" : "/32BIT-");
                                        oProcessCorFlags.StartInfo.WorkingDirectory = applicationFolder;
                                        oProcessCorFlags.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                        RaiseOutMessage(" " + oProcessCorFlags.StartInfo.FileName);
                                        RaiseOutMessage(" " + oProcessCorFlags.StartInfo.Arguments);
                                        RaiseOutMessage(" " + oProcessCorFlags.StartInfo.WorkingDirectory);
                                        oProcessCorFlags.Start();
                                        oProcessCorFlags.WaitForExit();
                                        RaiseOutMessage("Fine conversione");
                                        oProcessCorFlags.Close();
                                        oProcessCorFlags.Dispose();
                                        oProcessCorFlags = null;
                                    }
                                    catch (Exception exCorFlags)
                                    {
                                        Error = exCorFlags;
                                        RaiseOutMessage(Error);
                                    }
                                }
                            }


                            // avvio applicazione
                            operation = string.Format("Avvio applicazione {0}", applicationName);
                            RaiseOutMessage(operation);
                            System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(applicationFolder);
                            System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                            oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(oDirInfo.FullName + @"\" + applicationName);
                            oProcess.StartInfo.WorkingDirectory = oDirInfo.FullName;
                            if (!string.IsNullOrEmpty(fullAppExternArgs))
                            {
                                oProcess.StartInfo.Arguments = fullAppExternArgs;
                            }
                            oProcess.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                RaiseOutMessage(Error);
            }
            return Error == null;
        }

        public static bool MakeUpdatesWinService(out Exception Error, string applicationFolder, string downloadFolder, string applicationName, string fileParamsRestart, string applicationType, string applicationInstall, string applicationRestart)
        {
            bool result = false;
            Error = null;
            string operation = "";

            operation = "Aggiornamento";
            RaiseOutMessage(operation);
            try
            {
                string fullAppExternArgs = "";
                if (CopyDownloadFolder(out Error, downloadFolder, applicationFolder, fileParamsRestart, out fullAppExternArgs))
                {
                    result = (Error == null);
                    if (result)
                    {
                        if (!string.IsNullOrEmpty(applicationName))
                        {
                            System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(applicationFolder);

                            if (System.IO.File.Exists(applicationFolder + @"\" + applicationInstall))
                            {
                                try
                                {
                                    System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                                    oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(applicationFolder + @"\" + applicationInstall);
                                    oProcess.StartInfo.RedirectStandardOutput = true;
                                    oProcess.StartInfo.UseShellExecute = false;
                                    oProcess.StartInfo.WorkingDirectory = oDirInfo.FullName;
                                    oProcess.Start();
                                    while (!oProcess.HasExited)
                                    {
                                        RaiseOutMessage(oProcess.StandardOutput.ReadToEnd());
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }

                            if (string.IsNullOrEmpty(applicationRestart) || !System.IO.File.Exists(applicationFolder + @"\" + applicationRestart))
                            {
                                // avvio applicazione
                                operation = string.Format("Avvio applicazione {0}", applicationName);
                                RaiseOutMessage(operation);
                                System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                                oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(oDirInfo.FullName + @"\" + applicationName);
                                oProcess.StartInfo.WorkingDirectory = oDirInfo.FullName;
                                if (!string.IsNullOrEmpty(fullAppExternArgs))
                                {
                                    oProcess.StartInfo.Arguments = fullAppExternArgs;
                                }
                                oProcess.Start();
                            }
                            else
                            {
                                try
                                {
                                    System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                                    oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(applicationFolder + @"\" + applicationRestart);
                                    oProcess.StartInfo.RedirectStandardOutput = true;
                                    oProcess.StartInfo.UseShellExecute = false;
                                    oProcess.StartInfo.WorkingDirectory = oDirInfo.FullName;
                                    oProcess.Start();
                                    while (!oProcess.HasExited)
                                    {
                                        RaiseOutMessage(oProcess.StandardOutput.ReadToEnd());
                                    }
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                result = false;
                RaiseOutMessage(Error);
            }
            return result;
        }

        #region Upload

        public  bool UploadUpdatesSiteToSite(Wintic.Data.IConnection ConnectionSource, Wintic.Data.IConnection ConnectionTarget, string TableNameAPPS_VERSION, out Exception Error, string applicationName, bool forceUpdate)
        {
            bool result = true;
            Error = null;
            string operation = "";

            try
            {
                List<string> Applications = new List<string>();
                if (!string.IsNullOrEmpty(applicationName))
                {
                    Applications.Add(applicationName);
                }
                else
                {
                    Wintic.Data.IRecordSet oRS = (Wintic.Data.IRecordSet)ConnectionSource.ExecuteQuery(string.Format("SELECT DISTINCT APPLICATION_NAME FROM {0}", TableNameAPPS_VERSION));
                    while (!oRS.EOF)
                    {
                        Applications.Add(oRS.Fields("APPLICATION_NAME").Value.ToString());
                        oRS.MoveNext();
                    }
                    oRS.Close();
                }

                foreach (String cItemAppName in Applications)
                {
                    bool lContinue = false;
                    clsUpdaterApplication applicationNew = LoadApplicationFromDatabase(ConnectionSource, TableNameAPPS_VERSION, out Error, cItemAppName, forceUpdate);
                    if (applicationNew != null)
                    {
                        if (!forceUpdate)
                        {
                            clsUpdaterApplication applicationInstalled = LoadApplicationFromDatabase(ConnectionTarget, TableNameAPPS_VERSION, out Error, cItemAppName, false);
                            lContinue = !Updater.ApplicationsIsEquals(applicationNew, applicationInstalled);
                        }
                        else
                        {
                            lContinue = true;
                        }
                    }

                    if (lContinue)
                    {
                        if (!forceUpdate)
                            applicationNew = LoadApplicationFromDatabase(ConnectionSource, TableNameAPPS_VERSION, out Error, cItemAppName, true);
                        if (!UploadUpdates(ConnectionTarget, out Error, applicationNew, forceUpdate))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                RaiseOutMessage(Error);
            }
            operation = string.Format("Upload aggiornamenti {0}", applicationName);
            RaiseOutMessage(operation);
            return result;
        }

        public static  Icon GetIcon()
        {
            return libUpdater.Properties.Resources.CreaLogo;
        }

        public bool UploadUpdates(Wintic.Data.IConnection Connection, out Exception Error, clsUpdaterApplication applicationNew, bool forceUpdate)
        {
            
            bool result = false;
            Error = null;
            string operation = "";

            operation = string.Format("Upload aggiornamenti {0}", applicationNew.ApplicationName);
            RaiseOutMessage(operation);
            RaiseOutMessageUpload(operation);

            string tabUpdateDefault = applicationNew.TableApplicationName;

            if (CheckTableUpdates(Connection, tabUpdateDefault, out Error, this) && Error == null)
            {
                try
                {
                    StringBuilder oSB = null;
                    Wintic.Data.clsParameters oPars = null;

                    clsUpdaterApplication applicationInstalled = LoadApplicationFromDatabase(Connection, tabUpdateDefault, out Error, applicationNew.ApplicationName, false, this);

                    operation = string.Format("Upload aggiornamenti, verifica differenze {0}", applicationNew.ApplicationName);
                    RaiseOutMessage(operation);
                    RaiseOutMessageUpload(operation);

                    if (forceUpdate || !ApplicationsIsEquals(applicationInstalled, applicationNew))
                    {
                        Connection.BeginTransaction();
                        try
                        {
                            operation = string.Format("Upload aggiornamenti, cancellazione precedente versione {0}", applicationNew.ApplicationName);
                            RaiseOutMessage(operation);
                            RaiseOutMessageUpload(operation);
                            oSB = new StringBuilder();
                            oSB.Append(string.Format("DELETE FROM {0} WHERE APPLICATION_NAME = :pAPPLICATION_NAME", tabUpdateDefault));
                            oPars = new Wintic.Data.clsParameters(":pAPPLICATION_NAME", applicationNew.ApplicationName);
                            Connection.ExecuteNonQuery(oSB.ToString(), oPars);

                            operation = string.Format("Upload aggiornamenti, inserimento nuova versione {0}", applicationNew.ApplicationName);
                            RaiseOutMessage(operation);
                            RaiseOutMessageUpload(operation);
                            oSB = new StringBuilder();
                            oSB.Append(string.Format("INSERT INTO {0}", tabUpdateDefault));
                            oSB.Append(" (");
                            oSB.Append(" APPLICATION_NAME,");
                            oSB.Append(" PACKEG_ORDER,");
                            oSB.Append(" PACKEG_NAME,");
                            oSB.Append(" PACKEG_STATE,");
                            oSB.Append(" VERSION_RELEASE,");
                            oSB.Append(" START_UPDATE,");
                            oSB.Append(" INSERTED,");
                            oSB.Append(" COMPRESSED,");
                            oSB.Append(" FILES_LIST");
                            oSB.Append(" )");
                            oSB.Append(" VALUES");
                            oSB.Append(" (");
                            oSB.Append(" :pAPPLICATION_NAME,");
                            oSB.Append(" :pPACKEG_ORDER,");
                            oSB.Append(" :pPACKEG_NAME,");
                            oSB.Append(" 0,");
                            oSB.Append(" :pVERSION_RELEASE,");
                            oSB.Append(" :pSTART_UPDATE,");
                            oSB.Append(" SYSDATE,");
                            oSB.Append(" :pCOMPRESSED,");
                            oSB.Append(" :pFILES_LIST");
                            oSB.Append(" )");

                            foreach (clsUpdaterPackeg packeg in applicationNew.Items)
                            {
                                operation = string.Format("Upload aggiornamenti {0}" + "\r\n" + "inserimento dati pacchetto" + "\r\n" + "{1}", applicationNew.ApplicationName, packeg.PackegName);
                                RaiseOutMessage(operation);
                                RaiseOutMessageUpload(operation);
                                oPars = new Wintic.Data.clsParameters(":pAPPLICATION_NAME", applicationNew.ApplicationName);
                                oPars.Add(":pPACKEG_ORDER", packeg.PackegOrder);
                                oPars.Add(":pPACKEG_NAME", packeg.PackegName);
                                oPars.Add(":pVERSION_RELEASE", packeg.VersionRelease);
                                oPars.Add(":pSTART_UPDATE", packeg.StartUpdate);
                                oPars.Add(":pCOMPRESSED", packeg.Compressed);
                                oPars.Add(":pFILES_LIST", packeg.FilesList);
                                Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                            }

                            Connection.EndTransaction();

                            try
                            {
                                foreach (clsUpdaterPackeg packeg in applicationNew.Items)
                                {
                                    operation = string.Format("Upload aggiornamenti {0}" + "\r\n" + "inserimento pacchetto" + "\r\n" + "{1}" + "\r\n" + "{2} bytes", applicationNew.ApplicationName, packeg.PackegName, packeg.Packeg.Length.ToString());
                                    RaiseOutMessage(operation);
                                    RaiseOutMessageUpload(operation);
                                    oSB = new StringBuilder();
                                    oSB.Append(string.Format(" APPLICATION_NAME = '{0}' ", applicationNew.ApplicationName));
                                    oSB.Append(" AND ");
                                    oSB.Append(string.Format(" PACKEG_NAME = '{0}' ", packeg.PackegName));
                                    System.IO.MemoryStream oStream = new System.IO.MemoryStream();
                                    oStream.Write(packeg.Packeg, 0, packeg.Packeg.Length);
                                    
                                    Connection.SaveStream(tabUpdateDefault, "PACKEG", oSB.ToString(), oStream);
                                    oStream.Close();
                                    oStream.Dispose();

                                    operation = string.Format("Upload aggiornamenti {0}" + "\r\n" + "modifica stato pacchetto" + "\r\n" + "{1}", applicationNew.ApplicationName, packeg.PackegName);
                                    RaiseOutMessage(operation);
                                    RaiseOutMessageUpload(operation);

                                    oSB = new StringBuilder();
                                    oSB.Append(string.Format("UPDATE {0} SET PACKEG_STATE = 1 WHERE APPLICATION_NAME = :pAPPLICATION_NAME AND PACKEG_NAME = :pPACKEG_NAME", tabUpdateDefault));
                                    oPars = new Wintic.Data.clsParameters(":pAPPLICATION_NAME", applicationNew.ApplicationName);
                                    oPars.Add(":pPACKEG_NAME", packeg.PackegName);
                                    Connection.ExecuteNonQuery(oSB.ToString(), oPars);
                                }
                                result = true;
                            }
                            catch (Exception exUpdate)
                            {
                                Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", exUpdate.Message));
                                RaiseOutMessage(Error);
                                RaiseOutMessageUpload(Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                            RaiseOutMessage(Error);
                            RaiseOutMessageUpload(Error);
                            Connection.RollBack();
                        }
                    }
                    else
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    Error = new Exception(string.Format("Errore " + operation + "\r\n" + "{0}", ex.Message));
                    RaiseOutMessage(Error);
                    RaiseOutMessageUpload(Error);
                }
            }
            return result;
        }

        private static System.Windows.Forms.Form GetWaitForm(string Caption, string Message)
        {
            System.Windows.Forms.Form frm = new System.Windows.Forms.Form();
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            frm.Size = new System.Drawing.Size(400, 200);
            frm.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frm.ShowInTaskbar = false;
            frm.ShowIcon = false;
            frm.Text = Caption;
            System.Windows.Forms.Label lbl = new System.Windows.Forms.Label();
            lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            lbl.Text = Message;
            lbl.AutoSize = false;
            lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            frm.Controls.Add(lbl);
            lbl.BringToFront();
            frm.Show();
            System.Windows.Forms.Application.DoEvents();
            return frm;
        }

        private delegate void delegateSetMessageWaitForm(System.Windows.Forms.Form frm, string message);
        private static void SetMessageWaitForm(System.Windows.Forms.Form frm, string message)
        {
            if (frm.InvokeRequired)
            {
                delegateSetMessageWaitForm d = new delegateSetMessageWaitForm(SetMessageWaitForm);
                frm.Invoke(d, frm, message);
            }
            else
            {
                try
                {

                    foreach (Control c in frm.Controls)
                    {
                        c.Text = message;
                        Application.DoEvents();
                        break;
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        

        public static bool InterfaceMakePackegToUpload(out Exception Error, out libUpdater.clsUpdaterApplication app, bool saveDirectInLibrary)
        {
            bool result = false;
            Error = null;
            app = null;
            try
            {
                string entryFileName = "";
                List<string> files = null;
                string tableAppVersion = "";
                bool zipped = false;
                string destPathLibreria = "";
                if (GetFiles(out entryFileName, out files, ref tableAppVersion, out destPathLibreria))
                {
                    libUpdater.clsUpdaterPackeg entryPackeg = libUpdater.Updater.GetPackegFromFile(out Error, entryFileName);

                    if (System.Windows.Forms.MessageBox.Show("Comprimere il pacchetto ?", "Compressione", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        System.Windows.Forms.Form oFrmZip = GetWaitForm("Zip", "Compressione in corso...");
                        try
                        {
                            System.IO.FileInfo oFileInfoApp = new System.IO.FileInfo(entryFileName);
                            string zipFile = oFileInfoApp.FullName + ".zip";

                            ICSharpCode.SharpZipLib.Zip.ZipFile oZipFile = ICSharpCode.SharpZipLib.Zip.ZipFile.Create(zipFile);
                            oZipFile.NameTransform = new ICSharpCode.SharpZipLib.Zip.ZipNameTransform(oFileInfoApp.Directory.FullName);
                            oZipFile.BeginUpdate();
                            oZipFile.Add(entryFileName);
                            if (files != null && files.Count > 0)
                            {
                                foreach (string fName in files)
                                {
                                    SetMessageWaitForm(oFrmZip, fName);
                                    AddFilesOrSubDirectory(oZipFile, fName);
                                }
                            }
                            SetMessageWaitForm(oFrmZip, "Finalizzazione compressione...");
                            oZipFile.CommitUpdate();
                            oZipFile.Close();
                            entryPackeg = libUpdater.Updater.GetPackegFromFile(out Error, entryFileName);
                            entryPackeg.PackegName = entryPackeg.PackegName + ".zip";
                            entryPackeg.Packeg = System.IO.File.ReadAllBytes(zipFile);
                            entryPackeg.Compressed = 1;
                            System.IO.File.Delete(zipFile);
                            files = null;
                        }
                        catch (Exception exZip)
                        {
                            Error = exZip;
                        }
                        oFrmZip.Close();
                        oFrmZip.Dispose();
                    }
                    else
                    {
                        entryPackeg = libUpdater.Updater.GetPackegFromFile(out Error, entryFileName);
                    }

                    if (entryPackeg != null && Error == null)
                    {
                        app = new libUpdater.clsUpdaterApplication();
                        System.IO.FileInfo oFileInfoApp = new System.IO.FileInfo(entryFileName);
                        app.ApplicationName = oFileInfoApp.Name;
                        app.TableApplicationName = tableAppVersion;
                        app.Items = new List<libUpdater.clsUpdaterPackeg>();
                        app.Items.Add(entryPackeg);
                        if (files != null && files.Count > 0)
                        {
                            foreach (string fName in files)
                            {
                                libUpdater.clsUpdaterPackeg itemPackeg = libUpdater.Updater.GetPackegFromFile(out Error, fName);
                                itemPackeg.PackegOrder = app.Items.Count;
                                app.Items.Add(itemPackeg);
                            }
                        }

                        string root = "";
                        bool lSavePackeg = false;
                        if (saveDirectInLibrary || System.Windows.Forms.MessageBox.Show("Salvare il pacchetto di installazione ?", "Pacchetto di installazione", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            
                            System.Windows.Forms.FolderBrowserDialog oDialog = new System.Windows.Forms.FolderBrowserDialog();
                            oDialog.Description = "Salvataggio pacchetto";

                            if (!string.IsNullOrEmpty(destPathLibreria) && System.IO.Directory.Exists(destPathLibreria))
                            {
                                oDialog.SelectedPath = destPathLibreria;
                            }

                            if (oDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oDialog.SelectedPath) && System.IO.Directory.Exists(oDialog.SelectedPath))
                            {
                                root = oDialog.SelectedPath;
                                lSavePackeg = true;
                                result = true;
                                string cDirApp = (new System.IO.FileInfo(Application.ExecutablePath).Directory.FullName);
                                string cRecentFileCfg = cDirApp + @"\recentapp.txt";

                                if (System.IO.File.Exists(cRecentFileCfg))
                                {
                                    System.IO.FileInfo oEntryFileInfo = new System.IO.FileInfo(entryFileName);
                                    List<string> itemsRecent = new List<string>();
                                    foreach (string cLine in System.IO.File.ReadAllLines(cRecentFileCfg))
                                    {
                                        if (cLine.Contains("|"))
                                        {
                                            System.IO.FileInfo oFileInfo = new System.IO.FileInfo(cLine.Split('|')[0]);
                                            if (oEntryFileInfo.Name == oFileInfo.Name)
                                            {
                                                itemsRecent.Add(oEntryFileInfo.FullName + "|" + tableAppVersion + "|" + root);
                                            }
                                            else
                                            {
                                                itemsRecent.Add(cLine);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.FileInfo oFileInfo = new System.IO.FileInfo(cLine);
                                            if (oEntryFileInfo.Name == oFileInfo.Name)
                                            {
                                                itemsRecent.Add(oEntryFileInfo.FullName + "|" + tableAppVersion + "|" + root);
                                            }
                                            else
                                            {
                                                itemsRecent.Add(cLine);
                                            }
                                        }
                                    }
                                    System.IO.File.WriteAllLines(cRecentFileCfg, itemsRecent.ToArray());
                                }
                            }
                            else
                            {
                                result = false;
                            }
                            oDialog.Dispose();
                        }

                        if (lSavePackeg && result)
                        {
                            if (!string.IsNullOrEmpty(root))
                            {
                                try
                                {
                                    System.IO.DirectoryInfo oRootInfo = new System.IO.DirectoryInfo(root);
                                    List<System.IO.FileInfo> oFilesToDelete = new List<System.IO.FileInfo>();
                                    List<System.IO.DirectoryInfo> oDirectoriesToDelete = new List<System.IO.DirectoryInfo>();
                                    bool lAskFile = true;
                                    

                                    foreach (System.IO.FileInfo oFInfoToDelete in oRootInfo.GetFiles())
                                    {
                                        if (lAskFile)
                                        {
                                            System.Windows.Forms.DialogResult rDialog = System.Windows.Forms.MessageBox.Show("Eliminare:" + oFInfoToDelete.FullName + "\r\n" + "?", "", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                                            if (rDialog == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                oFilesToDelete.Add(oFInfoToDelete);
                                                rDialog = System.Windows.Forms.MessageBox.Show("Continuare a chiedere se eliminare o meno i files ?", "", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                                                if (rDialog == System.Windows.Forms.DialogResult.No)
                                                {
                                                    lAskFile = false;
                                                }
                                                else if (rDialog == System.Windows.Forms.DialogResult.Cancel)
                                                {
                                                    result = false;
                                                    break;
                                                }
                                            }
                                            else if (rDialog == System.Windows.Forms.DialogResult.Cancel)
                                            {
                                                result = false;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            oFilesToDelete.Add(oFInfoToDelete);
                                        }
                                    }

                                    if (result)
                                    {
                                        lAskFile = true;
                                        foreach (System.IO.DirectoryInfo oDInfoToDelete in oRootInfo.GetDirectories())
                                        {
                                            if (lAskFile)
                                            {
                                                System.Windows.Forms.DialogResult rDialog = System.Windows.Forms.MessageBox.Show("Eliminare:" + oDInfoToDelete.FullName + "\r\n" + "?", "", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                                                if (rDialog == System.Windows.Forms.DialogResult.Yes)
                                                {
                                                    oDirectoriesToDelete.Add(oDInfoToDelete);
                                                    rDialog = System.Windows.Forms.MessageBox.Show("Continuare a chiedere se eliminare o meno le cartelle ?", "", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question);
                                                    if (rDialog == System.Windows.Forms.DialogResult.No)
                                                    {
                                                        lAskFile = false;
                                                    }
                                                    else if (rDialog == System.Windows.Forms.DialogResult.Cancel)
                                                    {
                                                        result = false;
                                                        break;
                                                    }
                                                }
                                                else if (rDialog == System.Windows.Forms.DialogResult.Cancel)
                                                {
                                                    result = false;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                oDirectoriesToDelete.Add(oDInfoToDelete);
                                            }
                                        }
                                    }

                                    if (result)
                                    {
                                        foreach (System.IO.FileInfo oItem in oFilesToDelete)
                                        {
                                            System.IO.File.Delete(oItem.FullName);
                                        }
                                        foreach (System.IO.DirectoryInfo oItem in oDirectoriesToDelete)
                                        {
                                            System.IO.DirectoryInfo oDInfoToDelete = new System.IO.DirectoryInfo(oItem.FullName);
                                            oDInfoToDelete.Delete(true);
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                }

                                if (result)
                                {
                                    clsUpdaterApplication appSave = new clsUpdaterApplication();
                                    appSave.ApplicationName = app.ApplicationName;
                                    appSave.TableApplicationName = app.TableApplicationName;
                                    appSave.Items = new List<clsUpdaterPackeg>();
                                    foreach (clsUpdaterPackeg oItem in app.Items)
                                    {
                                        clsUpdaterPackeg pkgSave = new clsUpdaterPackeg();
                                        pkgSave.Compressed = oItem.Compressed;
                                        pkgSave.FilesList = oItem.FilesList;
                                        pkgSave.Inserted = oItem.Inserted;
                                        pkgSave.PackegName = oItem.PackegName;
                                        pkgSave.PackegOrder = oItem.PackegOrder;
                                        pkgSave.PackegState = oItem.PackegState;
                                        pkgSave.StartUpdate = oItem.StartUpdate;
                                        pkgSave.VersionRelease = oItem.VersionRelease;
                                        appSave.Items.Add(pkgSave);
                                    }
                                    string cJsonApplication = Newtonsoft.Json.JsonConvert.SerializeObject(appSave, Newtonsoft.Json.Formatting.Indented);
                                    string cFileNameApp = root + @"\_app_name.json";
                                    System.IO.File.WriteAllText(cFileNameApp, cJsonApplication);
                                    foreach (clsUpdaterPackeg oItem in app.Items)
                                    {
                                        System.IO.File.WriteAllBytes(root + @"\" + oItem.PackegName, oItem.Packeg);
                                    }
                                }
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else if (result)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Errore" + (Error == null ? "" : "\r\n" + Error.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
                System.Windows.Forms.MessageBox.Show("Errore" + (Error == null ? "" : "\r\n" + Error.Message));
            }
            return result;

        }

        private static int Occurs(string a, string b)
        {
            int countOccurs = 0;
            while (a.Contains(b))
            {
                countOccurs++;
                a = a.Substring(a.IndexOf(b) + 1);
            }
            return countOccurs;
        }

        public bool InterfaceUploadUpdate(Wintic.Data.IConnection Connection, out Exception Error, List<string> listPathPreconfiguredApp, bool openForm)
        {
            bool result = false;
            Error = null;
            foreach (string cPathPreconfiguredApp in listPathPreconfiguredApp)
            {
                result = InterfaceUploadUpdate(Connection, out Error, cPathPreconfiguredApp, openForm);
                if (!result || Error != null) break;
            }
            System.Windows.Forms.MessageBox.Show("FINE");
            return result;
        }

        public bool InterfaceUploadUpdate(Wintic.Data.IConnection Connection, out Exception Error, string cPathPreconfiguredApp, bool openForm)
        {
            bool result = false;
            Error = null;
            libUpdater.clsUpdaterApplication app = null;


            frmCheckUpdates oFrmCheck = null;
            if (openForm)
            {
                oFrmCheck = new frmCheckUpdates();
                oFrmCheck.Show();
            }
            try
            {
                if (!string.IsNullOrEmpty(cPathPreconfiguredApp))
                {
                    string JsonAppFile = cPathPreconfiguredApp + @"\_app_name.json";
                    string cJsonApplication = System.IO.File.ReadAllText(JsonAppFile);
                    app = Newtonsoft.Json.JsonConvert.DeserializeObject<clsUpdaterApplication>(cJsonApplication);
                    foreach (clsUpdaterPackeg oItem in app.Items)
                    {
                        oItem.Packeg = System.IO.File.ReadAllBytes(cPathPreconfiguredApp + @"\" + oItem.PackegName);
                    }
                    result = true;
                }
                else
                {
                    result = InterfaceMakePackegToUpload(out Error, out app, false);
                }
                if (result)
                {
                    if (UploadUpdates(Connection, out Error, app, true))
                    {
                        result = true;
                    }
                    else
                    {
                        if (openForm)
                            System.Windows.Forms.MessageBox.Show("Errore" + (Error == null ? "" : "\r\n" + Error.Message));
                        else
                            RaiseOutMessageUpload(Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
                if (openForm)
                    System.Windows.Forms.MessageBox.Show("Errore" + (Error == null ? "" : "\r\n" + Error.Message));
                else
                    RaiseOutMessageUpload(Error);
            }
            if (openForm)
            {
                oFrmCheck.Close();
                oFrmCheck.Dispose();
            }
            return result;
        }

        public static string GetDirApplication()
        {
            string cDirApp = (new System.IO.FileInfo(Application.ExecutablePath).Directory.FullName);
            return cDirApp;
        }

        public static string GetDirApplicationParent()
        {
            string cDirApp = GetDirApplication();
            string cDirAppParent = "";
            System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(cDirApp);
            if (oDirInfo.Exists && oDirInfo.Parent.Exists)
                cDirAppParent = oDirInfo.Parent.FullName;
            return cDirApp;
        }

        public static string GetFilePathsDefaults()
        {
            return GetDirApplication() + @"\pathsDefaults.txt";
        }

        public static string GetDirApplicationDefaults(string tipo)
        {
            string cResult = "";
            if (System.IO.File.Exists(GetFilePathsDefaults()))
            {
                foreach (string cLine in System.IO.File.ReadAllLines(GetFilePathsDefaults()))
                {
                    if (cLine.StartsWith("#" + tipo + "#"))
                    {
                        cResult = cLine.Replace("#" + tipo + "#", "");
                        break;
                    }
                }
            }
            return cResult;
        }

        public static void SaveDirApplicationDefaults(string tipo, string value)
        {
            List<string> linee = new List<string>();
            List<string> linee_save = new List<string>();
            if (System.IO.File.Exists(GetFilePathsDefaults()))
            {
                linee = new List<string>();
                foreach (string cLine in System.IO.File.ReadAllLines(GetFilePathsDefaults()))
                    linee.Add(cLine);
            }
            
            foreach (string cLine in linee)
            {
                if (!cLine.StartsWith("#" + tipo + "#"))
                    linee_save.Add(cLine);
            }
            linee_save.Add("#" + tipo + "#" + value);
            System.IO.File.WriteAllLines(GetFilePathsDefaults(), linee_save.ToArray());
        }

        private static bool AddFilesOrSubDirectory(ICSharpCode.SharpZipLib.Zip.ZipFile oZipFile, string fName)
        {
            System.IO.FileAttributes attr = System.IO.File.GetAttributes(fName);
            if ((attr & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory)
            {
                oZipFile.AddDirectory(fName);
                System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(fName);
                foreach (System.IO.FileInfo oFInfo in oDirInfo.GetFiles())
                {
                    oZipFile.Add(oFInfo.FullName);
                }
                foreach (System.IO.DirectoryInfo oDInfo in oDirInfo.GetDirectories())
                {
                    AddFilesOrSubDirectory(oZipFile, oDInfo.FullName);
                }
            }
            else
            {
                oZipFile.Add(fName);
            }
            return true;
        }

        private static bool GetFiles(out string entryFileName, out List<string> files, ref string tableAppVersion, out string destPathLibreria)
        {
            bool result = false;
            destPathLibreria = "";
            entryFileName = "";
            string preTabName = tableAppVersion;
            files = new List<string>();
            System.Windows.Forms.OpenFileDialog oF;

            string cDirApp = (new System.IO.FileInfo(Application.ExecutablePath).Directory.FullName);
            string cRecentFileCfg = cDirApp + @"\recentapp.txt";
            if (System.IO.File.Exists(cRecentFileCfg))
            {
                //ShowMenuDialog
                Dictionary<string, object> menuItems = new Dictionary<string, object>();
                foreach (string cLine in System.IO.File.ReadAllLines(cRecentFileCfg))
                {
                    if (cLine.Contains("|"))
                    {
                        menuItems.Add(cLine.Split('|')[0], cLine);
                    }
                    else
                    {
                        menuItems.Add(cLine, cLine);
                    }
                }
                object value = null;
                if (ShowMenuDialog("Preselezioni", out value, menuItems) == DialogResult.OK)
                {
                    string selectedValue = (string)value;
                    if (!selectedValue.Contains("|"))
                    {
                        entryFileName = selectedValue;
                    }
                    else
                    {
                        entryFileName = selectedValue.Split('|')[0];
                        preTabName = selectedValue.Split('|')[1];
                        if (Occurs(selectedValue, "|") > 1)
                        {
                            destPathLibreria = selectedValue.Split('|')[2];
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(entryFileName))
            {
                oF = new System.Windows.Forms.OpenFileDialog();
                oF.Multiselect = false;
                oF.CheckFileExists = true;
                oF.DefaultExt = ".exe";
                oF.Filter = "exe files (*.exe)|*.exe";
                oF.FilterIndex = 1;
                string initialApth = GetDirApplicationDefaults("SOURCE");
                if (initialApth.Trim() != "" && System.IO.Directory.Exists(initialApth)) oF.InitialDirectory = initialApth;
                if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oF.FileName) && System.IO.File.Exists(oF.FileName))
                {
                    entryFileName = oF.FileName;
                    System.IO.FileInfo oFInfoPath = new System.IO.FileInfo(oF.FileName);
                    if (oFInfoPath.Directory != null && oFInfoPath.Directory.Exists && oFInfoPath.Directory.Parent != null && oFInfoPath.Directory.Parent.Exists)
                        SaveDirApplicationDefaults("SOURCE", oFInfoPath.Directory.Parent.FullName);
                }
                oF.Dispose();
            }


            if (!string.IsNullOrEmpty(entryFileName))
            {
                if (!System.IO.File.Exists(cRecentFileCfg))
                {
                    System.IO.File.AppendAllText(cRecentFileCfg, entryFileName);
                }
                else
                {
                    System.IO.FileInfo oEntryFileNameInfo = new System.IO.FileInfo(entryFileName);
                    List<string> updates = new List<string>();
                    bool lFind = false;
                    foreach (string cLine in System.IO.File.ReadAllLines(cRecentFileCfg))
                    {
                        string fileName = (cLine.Contains("|") ? cLine.Split('|')[0] : cLine);
                        System.IO.FileInfo oItemInfo = new System.IO.FileInfo(fileName);
                        if (oItemInfo.Name == oEntryFileNameInfo.Name)
                        {
                            updates.Add(entryFileName + "|" + preTabName + "|" + destPathLibreria);
                            lFind = true;
                        }
                        else
                        {
                            updates.Add(cLine);
                        }
                    }
                    if (!lFind) updates.Add(entryFileName);
                    System.IO.File.WriteAllLines(cRecentFileCfg, updates.ToArray());
                }
                if (System.Windows.Forms.MessageBox.Show("Altri files ?", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    
                    if (System.Windows.Forms.MessageBox.Show("Tutti i files o cartelle della stessa cartella ?", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        result = true;
                        System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo((new System.IO.FileInfo(entryFileName)).Directory.FullName);
                        if (System.Windows.Forms.MessageBox.Show("Tutti i files ?", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            foreach (System.IO.FileInfo oFInfo in oDirInfo.GetFiles())
                            {
                                if (oFInfo.FullName != entryFileName)
                                {
                                    files.Add(oFInfo.FullName);
                                }
                            }
                        }
                        else if (System.Windows.Forms.MessageBox.Show("Alcuni files ?", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            result = false;
                            oF = new System.Windows.Forms.OpenFileDialog();
                            oF.Multiselect = true;
                            oF.CheckPathExists = true;
                            oF.CheckFileExists = true;
                            oF.InitialDirectory = (new System.IO.FileInfo(entryFileName)).Directory.FullName;
                            if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && oF.FileNames != null && oF.FileNames.Length > 0)
                            {
                                files = new List<string>();
                                foreach (string fName in oF.FileNames)
                                {
                                    if (fName != entryFileName)
                                        files.Add(fName);
                                }
                                result = true;
                            }
                            oF.Dispose();
                        }
                        if (result)
                        {
                            if (System.Windows.Forms.MessageBox.Show("Tutte le cartelle ?", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                foreach (System.IO.DirectoryInfo oDInfo in oDirInfo.GetDirectories())
                                {
                                    files.Add(oDInfo.FullName);
                                }
                            }
                        }
                    }
                    else
                    {
                        oF = new System.Windows.Forms.OpenFileDialog();
                        oF.Multiselect = true;
                        oF.CheckPathExists = true;
                        oF.CheckFileExists = true;
                        oF.InitialDirectory = (new System.IO.FileInfo(entryFileName)).Directory.FullName;
                        if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && oF.FileNames != null && oF.FileNames.Length > 0)
                        {
                            files = new List<string>();
                            foreach (string fName in oF.FileNames)
                            {
                                if (fName != entryFileName)
                                    files.Add(fName);
                            }
                            result = true;
                        }
                        oF.Dispose();
                    }

                }
                else
                {
                    result = true;
                }
            }
            if (result)
            {
                string tabName = preTabName;
                if (ShowInputDialog("Inserire il nome della tabella che conserva l'applicazione", ref tabName) != DialogResult.OK)
                {
                    result = false;
                }
                else
                {
                    tableAppVersion = tabName;
                }
                
            }
            return result;
        }

        public static DialogResult ShowInputDialog(string title, ref string input)
        {
            return ShowInputDialog(title, ref input, "");
        }
        public static DialogResult ShowInputDialog(string title, ref string input, string passwordChar)
        {
            System.Drawing.Size size = new System.Drawing.Size(600, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = title;
            inputBox.StartPosition = FormStartPosition.CenterScreen;

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            if (!string.IsNullOrEmpty(passwordChar))
            {
                textBox.PasswordChar = System.Text.Encoding.ASCII.GetChars(System.Text.Encoding.ASCII.GetBytes(passwordChar))[0];
            }
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        public static DialogResult ShowMenuDialog(string title, out object selectedObject, Dictionary<string, object> items)
        {
            selectedObject = null;
            System.Drawing.Size size = new System.Drawing.Size(600, 600);
            Form menuBox = new Form();

            menuBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            menuBox.ClientSize = size;
            menuBox.Text = title;
            menuBox.StartPosition = FormStartPosition.CenterScreen;

            Panel oPanel = new Panel();
            oPanel.Dock = DockStyle.Fill;
            oPanel.Padding = new Padding(3);
            oPanel.AutoScroll = true;
            menuBox.Controls.Add(oPanel);
            oPanel.BringToFront();

            System.Drawing.Graphics oG = menuBox.CreateGraphics();
            System.Drawing.StringFormat oST = new System.Drawing.StringFormat();
            oST.Alignment = System.Drawing.StringAlignment.Center;
            oST.LineAlignment = System.Drawing.StringAlignment.Center;
            System.Drawing.Size oSizeText;

            foreach (KeyValuePair<string, object> menuItem in items)
            {
                Button oBtn = new Button();
                oBtn.Text = menuItem.Key;
                oSizeText = oG.MeasureString(oBtn.Text, menuBox.Font, menuBox.ClientSize.Width, oST).ToSize();
                if (oBtn.Height < oSizeText.Height + 20)
                {
                    oBtn.Height = oSizeText.Height + 20;
                }
                oBtn.Tag = menuItem.Value;
                oBtn.Dock = DockStyle.Top;
                oBtn.DialogResult = DialogResult.OK;
                oBtn.Click += delegate (object sender, EventArgs e)
                {
                    try
                    {
                        Button oBtnClicked = ((Button)sender);
                        oBtnClicked.FindForm().Tag = oBtnClicked.Tag;
                    }
                    catch (Exception)
                    {
                    }
                };
                oPanel.Controls.Add(oBtn);
                oBtn.BringToFront();
            }

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Text = "&Cancel";
            oSizeText = oG.MeasureString(cancelButton.Text, menuBox.Font, menuBox.ClientSize.Width, oST).ToSize();
            if (cancelButton.Height < oSizeText.Height + 20)
            {
                cancelButton.Height = oSizeText.Height + 20;
            }

            cancelButton.Dock = DockStyle.Top;
            oPanel.Controls.Add(cancelButton);

            menuBox.CancelButton = cancelButton;
            menuBox.Tag = null;

            DialogResult result = menuBox.ShowDialog();
            if (result == DialogResult.OK)
                selectedObject = menuBox.Tag;
            
            return result;

        }

        #endregion

        #region Distribution

        public static void EditDistribution()
        {
            string file = "";
            bool lcontinue = true;
            string value = "";

            System.Windows.Forms.OpenFileDialog oF = new System.Windows.Forms.OpenFileDialog();
            oF.Title = "Nome del file di distribuzione";
            oF.Multiselect = false;
            oF.CheckFileExists = false;
            oF.DefaultExt = ".json";
            oF.Filter = "json files (*.json)|*.json";
            oF.FilterIndex = 1;
            string initPathDistribuzione = GetDirApplicationDefaults("DISTRIBUZIONE");
            if (initPathDistribuzione.Trim() != "" && System.IO.Directory.Exists(initPathDistribuzione)) oF.InitialDirectory = initPathDistribuzione;

            lcontinue = (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oF.FileName));
            if (lcontinue) file = oF.FileName;
            oF.Dispose();

            Distribution result = (file == "" || !System.IO.File.Exists(file) ? new Distribution() : Newtonsoft.Json.JsonConvert.DeserializeObject<Distribution>(System.IO.File.ReadAllText(file)));


            if (lcontinue && file.Trim() != "")
            {
                System.IO.FileInfo oFInfo = new System.IO.FileInfo(file);
                if (oFInfo.Directory != null && oFInfo.Directory.Exists && oFInfo.Directory.Parent != null && oFInfo.Directory.Parent.Exists)
                    SaveDirApplicationDefaults("DISTRIBUZIONE", oFInfo.Directory.Parent.FullName);
            }

            if (lcontinue)
            {
                value = result.OracleUser;
                lcontinue = (ShowInputDialog("Utente Oracle", ref value) == DialogResult.OK);
                if (lcontinue)
                {
                    result.OracleUser = value;
                    if (result.OraclePassword == "")
                    {
                        if (result.OracleUser == "WTIC")
                            result.OraclePassword = "OBELIX";
                        else
                            result.OraclePassword = result.OracleUser;
                    }
                }
            }

            if (lcontinue)
            {
                value = result.OraclePassword;
                lcontinue = (ShowInputDialog("Password Oracle", ref value, "*") == DialogResult.OK);
                if (lcontinue) result.OraclePassword = value;
            }

            if (lcontinue)
            {
                value = result.OracleTnsName;
                lcontinue = (ShowInputDialog("TnsName", ref value) == DialogResult.OK);
                if (lcontinue) result.OracleTnsName = value;
            }

            if (lcontinue)
            {
                if (lcontinue)
                    result.Applications = new List<clsUpdaterApplication>();

                while (true)
                {
                    oF = new System.Windows.Forms.OpenFileDialog();
                    oF.Title = "Applicazioni da includere";
                    oF.Multiselect = true;
                    oF.CheckFileExists = true;
                    oF.DefaultExt = ".json";
                    oF.Filter = "json files (*.json)|*.json";
                    oF.FilterIndex = 1;
                    string initialPathPackes = GetDirApplicationDefaults("PACCHETTI");
                    if (initialPathPackes.Trim() != "" && System.IO.Directory.Exists(initialPathPackes)) oF.InitialDirectory = initialPathPackes;

                    if (oF.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(oF.FileName) && System.IO.File.Exists(oF.FileName))
                    {
                        System.IO.FileInfo oFInfo = new System.IO.FileInfo(oF.FileName);
                        clsUpdaterApplication app = Newtonsoft.Json.JsonConvert.DeserializeObject<clsUpdaterApplication>(System.IO.File.ReadAllText(oFInfo.FullName));
                        app.FullApplicationPackegPath = oFInfo.Directory.FullName;
                        result.Applications.Add(app);
                        
                        if (oFInfo.Directory != null && oFInfo.Directory.Exists && oFInfo.Directory.Parent != null && oFInfo.Directory.Parent.Exists)
                            SaveDirApplicationDefaults("PACCHETTI", oFInfo.Directory.Parent.FullName);
                    }
                    oF.Dispose();
                    if (MessageBox.Show("Altre librerie ?", "Invio librerie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        break;
                }
                lcontinue = (result.Applications.Count > 0);
            }

            if (lcontinue)
            {
                System.IO.File.WriteAllText(file, Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented));
            }
        }

        #endregion
    }


}
