﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace libUpdater
{
    public partial class frmWait : Form
    {
        public frmWait()
        {
            InitializeComponent();
        }

        public string Message
        {
            get { return this.lblMessage.Text; }
            set 
            {
                this.SetMessage(value);
            }
        }

        private delegate void delegateSetMessage(string value);
        private void SetMessage(string value)
        {
            if (this.InvokeRequired)
            {
                delegateSetMessage d = new delegateSetMessage(this.SetMessage);
                this.Invoke(d);
            }
            else
            {
                this.lblMessage.Text = value;
                Application.DoEvents();
            }
        }

        public static frmWait WaitWindow = null;
        public static void OpenWait(string message = "")
        {
            if (WaitWindow != null)
            {
                CloseWait();
            }
            WaitWindow = new frmWait();
            WaitWindow.Show();
            if (message != "")
                WaitWindow.Message = message;
        }

        public static void CloseWait()
        {
            try
            {
                if (WaitWindow != null)
                {
                    if (WaitWindow.InvokeRequired)
                    {
                        MethodInvoker d = new MethodInvoker(CloseWait);
                        WaitWindow.Invoke(d);
                    }
                    else
                    {
                        WaitWindow.Close();
                        WaitWindow.Dispose();
                        WaitWindow = null;
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
