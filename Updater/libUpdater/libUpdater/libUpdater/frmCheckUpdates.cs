﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace libUpdater
{
    public partial class frmCheckUpdates : Form
    {
        private bool lUpdateRequired = false;
        private Queue<string> QueueMessages = new Queue<string>();

        public frmCheckUpdates()
        {
            InitializeComponent();
            this.Opacity = 0;
            Updater.OutMessage += Updater_OutMessage;
            this.Shown += FrmMainMakeUpdater_Shown;
            this.FormClosing += FrmCheckUpdates_FormClosing;
            
        }

        public void SetApplicationName(string applicationName)
        {
            this.lblAppName.Text = applicationName;
        }

        private void FrmMainMakeUpdater_Shown(object sender, EventArgs e)
        {
            
        }

        private void FrmCheckUpdates_FormClosing(object sender, FormClosingEventArgs e)
        {
            Updater.OutMessage -= Updater_OutMessage;
        }

        private void Updater_OutMessage(string message)
        {
            if (!this.lUpdateRequired)
            {
                this.QueueMessages.Enqueue(message);
            }
            else
            {
                this.AppendMessage(message);
            }
        }

        private delegate void delegateAppendMessage(string message);

        private void AppendMessage(string message)
        {
            if (this.InvokeRequired)
            {
                delegateAppendMessage oD = new delegateAppendMessage(this.AppendMessage);
                this.Invoke(oD, message);
            }
            else
            {
                this.txtMessages.Text += "\r\n" + message;
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void SetVisibleTrue()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker oD = new MethodInvoker(this.SetVisibleTrue);
                this.Invoke(oD);
            }
            else
            {
                this.Opacity = 1;
                Application.DoEvents();
                while (this.QueueMessages.Count > 0)
                {
                    this.AppendMessage(this.QueueMessages.Dequeue());
                    System.Threading.Thread.Sleep(100);
                }

            }
        }

        public bool UpdateRequired
        {
            get => lUpdateRequired;
            set
            {
                lUpdateRequired = value;
                if (lUpdateRequired) this.SetVisibleTrue();
            }
        }

    }
}
