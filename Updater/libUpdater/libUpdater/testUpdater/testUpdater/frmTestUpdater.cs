﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testUpdater
{
    public partial class frmTestUpdater : Form
    {

        private Wintic.Data.IConnection Connection;
        public frmTestUpdater()
        {
            InitializeComponent();
            this.btnPush.Click += BtnPush_Click;
            this.btnCheck.Click += BtnCheck_Click; ;
        }

        private void BtnCheck_Click(object sender, EventArgs e)
        {
            this.Check();
        }

        private void BtnPush_Click(object sender, EventArgs e)
        {
            this.Push();
        }

        private void Push()
        {
            Exception Error = null;
            Connection = new Wintic.Data.oracle.CConnectionOracle();
            Connection.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", "SERVER-2019");
            Connection.Open();
            libUpdater.Updater.InterfaceUploadUpdate(Connection, out Error, "");
            Connection.Close();
            Connection.Dispose();
        }

        private void Check()
        {
            Connection = new Wintic.Data.oracle.CConnectionOracle();
            Connection.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", "SERVER-2019");
            Connection.Open();
            bool lUpdates = libUpdater.Updater.CheckForUpdatesAndUpdateRunningApplication(Connection, null);
            if (lUpdates)
            {
                this.Close();
                this.Dispose();
            }
        }
    }
}
