﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace makeUpdater
{
    public class clsMaker
    {
        public string applicationFolder = "";
        public string downloadFolder = "";
        public string applicationName = "";
        public string applicationType = "";
        public string applicationArgsFile = "";
        public string applicationStopBatch = "";
        public string applicationUninstallBatch = "";
        public string applicationInstallBatch = "";
        public string applicationRestartBatch = "";
        public string applicationConnectionLog = "";
        public string outputType = "";
        public long secondsWait = 0;
        public string TableNameAPPS_VERSION = "";

        public delegate void delegateSendOutMessage(string message);
        public event delegateSendOutMessage SendOutMessage;

        public bool logEnabled = true;
        public string fileLog = "";
        public int waitOutSendMills = 0;

        private void OutMessage(string message)
        {
            if (waitOutSendMills > 0)
                System.Threading.Thread.Sleep(waitOutSendMills);
            try
            {
                if (this.SendOutMessage != null) this.SendOutMessage(message);
            }
            catch (Exception)
            {
            }
            if (logEnabled)
            {
                if (fileLog == "")
                {
                    System.IO.FileInfo oAppInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    fileLog = oAppInfo.FullName + ".log";
                    System.IO.File.WriteAllText(fileLog, message);
                }
                else
                    System.IO.File.AppendAllText(fileLog, "\r\n" + message);
            }
        }

        public void InitArgs(string[] args)
        {
            OutMessage("Avvio analisi parametri...");
            try
            {

                if (args != null && args.Length > 0)
                {
                    string fullCommandLineArgs = "";
                    foreach (string argument in args)
                    {
                        fullCommandLineArgs += argument;
                    }

                    if (fullCommandLineArgs.Contains("/"))
                    {
                        foreach (string argument in fullCommandLineArgs.Split('/'))
                        {
                            if (!string.IsNullOrEmpty(argument) && argument.Contains("="))
                            {
                                string argName = argument.Split('=')[0].Trim().ToUpper();
                                string argValue = argument.Split('=')[1].Trim();
                                switch (argName)
                                {
                                    case "TABLE_APP_VERSION":
                                        {
                                            this.OutMessage("Tab.App.Version:");
                                            this.OutMessage(argValue);
                                            this.TableNameAPPS_VERSION = argValue;
                                            break;
                                        }
                                    case "APP_NAME":
                                        {
                                            this.OutMessage("Applicazione:");
                                            this.OutMessage(argValue);
                                            this.applicationName = argValue;
                                            break;
                                        }
                                    case "APP_TYPE":
                                        {
                                            this.OutMessage("Tipo Applicazione:");
                                            this.OutMessage(argValue);
                                            this.applicationType = argValue;
                                            break;
                                        }
                                    case "DOWNLOAD_FOLDER":
                                        {
                                            this.OutMessage("Copia da:");
                                            this.OutMessage(argValue);
                                            this.downloadFolder = argValue;
                                            break;
                                        }
                                    case "APPLICATION_FOLDER":
                                        {
                                            this.OutMessage("Cartella di destinazione:");
                                            this.OutMessage(argValue);
                                            this.applicationFolder = argValue;
                                            break;
                                        }
                                    case "APPLICATION_STOP":
                                        {
                                            this.OutMessage("Processo di stop:");
                                            this.OutMessage(argValue);
                                            this.applicationStopBatch = argValue;
                                            break;
                                        }
                                    case "APPLICATION_UNINSTALL":
                                        {
                                            this.OutMessage("Processo di disinstallazione:");
                                            this.OutMessage(argValue);
                                            this.applicationUninstallBatch = argValue;
                                            break;
                                        }
                                    case "APPLICATION_INSTALL":
                                        {
                                            this.OutMessage("Processo di installazione:");
                                            this.OutMessage(argValue);
                                            this.applicationInstallBatch = argValue;
                                            break;
                                        }
                                    case "APPLICATION_RESTART":
                                        {
                                            this.OutMessage("Processo di riavvio:");
                                            this.OutMessage(argValue);
                                            this.applicationRestartBatch = argValue;
                                            break;
                                        }
                                    case "SECONDS_WAIT":
                                        {
                                            this.OutMessage("Attesa:");
                                            this.OutMessage(argValue);
                                            secondsWait = 0;
                                            long.TryParse(argValue, out secondsWait);
                                            break;
                                        }
                                    case "APP_ARGS_FILE":
                                        {
                                            this.OutMessage("Tab.App.Version di parametri:");
                                            this.OutMessage(argValue);
                                            this.applicationArgsFile = argValue;
                                            break;
                                        }
                                    case "OUTPUT_TYPE":
                                        {
                                            this.OutMessage("Tipo di output:");
                                            this.OutMessage(argValue);
                                            this.outputType = argValue;
                                            break;
                                        }
                                    case "APP_CONN_LOG":
                                        {
                                            this.OutMessage("App.Connection log:");
                                            this.OutMessage(argValue);
                                            this.applicationConnectionLog = argValue;
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OutMessage("Errore analisi parametri" + "\r\n" + ex.ToString());
            }

        }

        private const string kToken = "F5EE450FE2119D0B7EDF48FA";


        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public bool MakeUpdate(out Exception Error)
        {
            bool lRet = false;
            Error = null;
            if (!string.IsNullOrEmpty(applicationName) && !string.IsNullOrEmpty(downloadFolder) && !string.IsNullOrEmpty(applicationFolder))
            {
                string cProcessToUpdate = applicationFolder + @"\" + applicationName;
                System.Diagnostics.Process oProcessToUpdate = null;
                this.OutMessage(string.Format("Veririfca se processo ancora in esecuzione {0}", applicationName));
                try
                {
                    System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcesses();
                    foreach (System.Diagnostics.Process p in processes)
                    {
                        if (!string.IsNullOrEmpty(p.MainWindowTitle))
                        {
                            if (p.MainModule.FileName == cProcessToUpdate)
                            {
                                oProcessToUpdate = p;
                                this.OutMessage("Trovato processo in esecuzione");
                                break;
                            }
                        }
                    }
                }
                catch (Exception exCheckProcess)
                {
                    Error = exCheckProcess;
                    this.OutMessage(string.Format("Errore nella veririfca se processo ancora in esecuzione {0}\r\n{1}", applicationName, Error.ToString()));
                }
                
                if (oProcessToUpdate != null)
                {
                    this.OutMessage("In attesa dell'interruzione...");
                    oProcessToUpdate.WaitForExit();
                }
                else
                    this.OutMessage("Avvio copia...");
                libUpdater.Updater.OutMessage += Updater_OutMessage;
                if (libUpdater.Updater.MakeUpdates(out Error, this.applicationFolder, this.downloadFolder, this.applicationName, this.applicationArgsFile, this.applicationType, this.applicationConnectionLog, this.TableNameAPPS_VERSION))
                {
                    lRet = (Error == null);
                }
                libUpdater.Updater.OutMessage -= Updater_OutMessage;
            }
            return lRet;
        }

        public void RemoveDownloadFolder()
        {
            try
            {
                libUpdater.Updater.CleanInstallationFilesInternal(this.downloadFolder);
            }
            catch (Exception)
            {
            }
        }

        public bool MakeUpdateWinService(out Exception Error)
        {
            bool lRet = false;
            Error = null;
            if (!string.IsNullOrEmpty(applicationName) && !string.IsNullOrEmpty(downloadFolder) && !string.IsNullOrEmpty(applicationFolder))
            {
                this.OutMessage(string.Format("Veririfca se processo ancora in esecuzione {0}", applicationName));

                string cProcessToUpdate = applicationFolder + @"\" + applicationName;

                this.OutMessage(string.Format("Veririfca esistenza processo di arresto {0}", applicationStopBatch));

                if (System.IO.File.Exists(applicationFolder + @"\" + applicationStopBatch))
                {
                    this.OutMessage(string.Format("Avvio processo di arresto {0}", applicationStopBatch));
                    try
                    {
                        System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                        oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(applicationFolder + @"\" + applicationStopBatch);
                        oProcess.StartInfo.RedirectStandardOutput = true;
                        oProcess.StartInfo.UseShellExecute = false;
                        oProcess.Start();
                        while (!oProcess.HasExited)
                        {
                            this.OutMessage(oProcess.StandardOutput.ReadToEnd());
                        }
                        //oProcess.WaitForExit();
                    }
                    catch (Exception ex)
                    {
                        this.OutMessage(string.Format("Errore processo di arresto {0}", ex.ToString()));
                    }
                }

                this.OutMessage(string.Format("Veririfca se processo ancora in esecuzione {0}", applicationName));

                try
                {
                    System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcesses();
                    System.Diagnostics.Process oProcessToUpdate = null;
                    foreach (System.Diagnostics.Process p in processes)
                    {
                        if (!string.IsNullOrEmpty(p.MainWindowTitle))
                        {
                            if (p.MainModule.FileName == cProcessToUpdate)
                            {
                                oProcessToUpdate = p;
                                this.OutMessage("Trovato processo in esecuzione");
                                break;
                            }
                        }
                    }
                    if (oProcessToUpdate != null)
                    {
                        this.OutMessage("In attesa dell'interruzione...");
                        oProcessToUpdate.WaitForExit();
                    }
                }
                catch (Exception ex )
                {
                    this.OutMessage(string.Format("Errore durante Veririfca se processo ancora in esecuzione {0}", ex.ToString()));
                }

                this.OutMessage(string.Format("Veririfca esistenza processo di disinstallazione {0}", applicationUninstallBatch));

                if (System.IO.File.Exists(applicationFolder + @"\" + applicationUninstallBatch))
                {
                    this.OutMessage(string.Format("Avvio processo di disinstallazione {0}", applicationUninstallBatch));
                    try
                    {
                        System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                        oProcess.StartInfo = new System.Diagnostics.ProcessStartInfo(applicationFolder + @"\" + applicationUninstallBatch);
                        oProcess.StartInfo.RedirectStandardOutput = true;
                        oProcess.StartInfo.UseShellExecute = false;
                        oProcess.Start();
                        while (!oProcess.HasExited)
                        {
                            this.OutMessage(oProcess.StandardOutput.ReadToEnd());
                        }
                        //oProcess.WaitForExit();
                    }
                    catch (Exception ex)
                    {
                        this.OutMessage(string.Format("Errore processo di disinstallazione {0}", ex.ToString()));
                    }
                }

                this.OutMessage("Avvio installazione nuovi files...");

                libUpdater.Updater.OutMessage += Updater_OutMessage;
                try
                {
                    if (libUpdater.Updater.MakeUpdatesWinService(out Error, this.applicationFolder, this.downloadFolder, this.applicationName, this.applicationArgsFile, this.applicationType, this.applicationInstallBatch, this.applicationRestartBatch))
                    {
                        lRet = (Error == null);
                    }
                    this.OutMessage(string.Format("Processo di installazione terminato {0} {1}", (lRet ? "con SUCCESSO" : "senza SUCCESSO"), (Error == null ? "senza errori" : "con Errori " + Error.Message)));
                }
                catch (Exception ex)
                {
                    this.OutMessage(string.Format("Errore processo installazione {0}", ex.ToString()));
                }
                libUpdater.Updater.OutMessage -= Updater_OutMessage;
            }
            return lRet;
        }

        private void Updater_OutMessage(string message)
        {
            this.OutMessage(message);
        }
    }
}
