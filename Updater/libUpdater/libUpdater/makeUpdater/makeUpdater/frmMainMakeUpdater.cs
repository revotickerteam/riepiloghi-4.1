﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace makeUpdater
{
    public partial class frmMainMakeUpdater : Form
    {
        private clsMaker Maker = null;

        public frmMainMakeUpdater()
        {
            InitializeComponent();
            this.Shown += FrmMainMakeUpdater_Shown;
            this.FormClosing += FrmMainMakeUpdater_FormClosing;
            this.btnClose.Click += BtnClose_Click;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            if (this.btnClose.Enabled && this.btnClose.Visible)
            {
                this.Close();
                this.Dispose();
            }
        }

        private void FrmMainMakeUpdater_Shown(object sender, EventArgs e)
        {
            bool closeApp = true;
            this.Maker = new clsMaker();
            this.Maker.SendOutMessage += this.Updater_OutMessage;
            this.lblAppName.Text = "Analisi parametri...";
            try
            {
                string[] args = Environment.GetCommandLineArgs();
                this.Maker.InitArgs(args);
                Exception Error = null;
                if (this.Maker.applicationName != "")
                    this.lblAppName.Text = this.Maker.applicationName;
                if (!this.Maker.MakeUpdate(out Error))
                {
                    closeApp = false;
                    this.EnableButtonClose();
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                this.AppendMessage("Errore" + "\r\n" + ex.Message);
                this.EnableButtonClose();
            }
            this.Maker.SendOutMessage -= this.Updater_OutMessage;

            if (closeApp)
            {
                this.Close();
                this.Dispose();
            }

        }
        private void FrmMainMakeUpdater_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Maker != null)
            {
                this.Maker.RemoveDownloadFolder();
            }
        }


        private void Updater_OutMessage(string message)
        {
            this.AppendMessage(message);
        }

        private delegate void delegateAppendMessage(string message);

        private void AppendMessage(string message)
        {
            if (this.InvokeRequired)
            {
                delegateAppendMessage oD = new delegateAppendMessage(this.AppendMessage);
                this.Invoke(oD, message);
            }
            else
            {

                this.txtMessages.Text += message + "\r\n";
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void EnableButtonClose()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker oD = new MethodInvoker(this.EnableButtonClose);
                this.Invoke(oD);
            }
            else
            {
                this.btnClose.Visible = true;
                this.btnClose.Enabled = true;
                this.btnClose.BringToFront();
                this.txtMessages.BringToFront();
                Application.DoEvents();
            }
        }
    }
}
