﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace makeUpdater
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            clsMaker oMake = new clsMaker();
            oMake.InitArgs(args);
            if (oMake.applicationType == "WIN_SERVICE")
            {
                if (!string.IsNullOrEmpty(oMake.outputType))
                {
                    if (oMake.outputType.StartsWith("LOG_FILE"))
                    {
                        if (oMake.outputType == "LOG_FILE")
                        {
                            logFile = oMake.applicationFolder + @"\_log_make_Updater.log";
                        }
                        else if(oMake.outputType.StartsWith("LOG_FILE:"))
                        {
                            logFile = oMake.applicationFolder + @"\" + oMake.outputType.Replace("LOG_FILE:", "");
                        }
                    }
                    else if (oMake.outputType == "CONSOLE")
                    {
                        lConsoleOutput = true;
                    }
                }
                
                Exception Error = null;
                oMake.SendOutMessage += OMake_SendOutMessage;
                oMake.MakeUpdateWinService(out Error);

            }
            else
            {
                Application.Run(new frmMainMakeUpdater());
            }
        }
        private static string logFile = "";
        private static bool lConsoleOutput = false;
        private static void OMake_SendOutMessage(string message)
        {
            if (!string.IsNullOrEmpty(logFile))
            {
                System.IO.File.AppendAllText(logFile, message + "\r\n");
            }
            else if (lConsoleOutput)
            {
                Console.WriteLine(message);
            }
        }

        private static void CleanLog()
        {
            try
            {
                if (!string.IsNullOrEmpty(logFile) && System.IO.File.Exists(logFile))
                {
                    System.IO.File.Delete(logFile);
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
