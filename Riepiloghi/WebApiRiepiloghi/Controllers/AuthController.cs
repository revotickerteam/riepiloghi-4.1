﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ApiRiepiloghi;
using Newtonsoft.Json;

namespace WebApiRiepiloghi.Controllers
{
    public class AuthController : ApiController
    {
        [HttpPost]
        public ResponseRiepiloghi Login(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetToken(parPayload.Login, parPayload.Password, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetAccount(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetAccount(parPayload.Token, parPayload.AccountId, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetTokenCodiceSistema(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetTokenCodiceSistema(parPayload.Token, parPayload.CodiceSistema, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);


            return result;
        }

        public ResponseRiepiloghi CheckToken(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = ApiRiepiloghi.ServiceRiepiloghi.CheckToken(parPayload.Token, Utility.GetEnvironment());

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi RiepiloghiAutoAuth(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.RiepiloghiAutoAuth(parPayLoad, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi GetProfileList(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            Exception oError = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetProfileListProfiles(parPayLoad.Token, Utility.GetEnvironment(), out oError);
            


            result = Utility.GetResponse(rb);

            result.Status = new ApiRiepiloghi.ResultBase();
            result.Status.StatusOK = (oError == null);
            result.Status.StatusMessage = (oError != null ? oError.Message : "OK");
            result.Status.StatusCode = (oError != null ? 1 : 0);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetAccountList(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            Exception oError = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetAccountListProfile(parPayLoad.Token, Utility.GetEnvironment(), out oError);

            result = Utility.GetResponse(rb);
            
            result.Status = new ApiRiepiloghi.ResultBase();
            result.Status.StatusOK = (oError == null);
            result.Status.StatusMessage = (oError != null ? oError.Message : "OK");
            result.Status.StatusCode = (oError != null ? 1 : 0);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi SetProfile(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            Exception oError = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SetProfile(parPayLoad.Token, parPayLoad.Profile, Utility.GetEnvironment(), out oError);

            result = Utility.GetResponse(rb);

            result.Status = new ApiRiepiloghi.ResultBase();
            result.Status.StatusOK = (oError == null);
            result.Status.StatusMessage = (oError != null ? oError.Message : "OK");
            result.Status.StatusCode = (oError != null ? 1 : 0);
            return result;
        }
        
        [HttpPost]
        public ResponseRiepiloghi CreateAccount(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            Exception oError = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CreateAccount(parPayLoad.Token, "", parPayLoad.Account.Email, parPayLoad.Account.Nome, parPayLoad.Account.Cognome, parPayLoad.Account.ProfileId, Utility.GetEnvironment(), out oError);

            result = Utility.GetResponse(rb);

            result.Status = new ApiRiepiloghi.ResultBase();
            result.Status.StatusOK = (oError == null);
            result.Status.StatusMessage = (oError != null ? oError.Message : "OK");
            result.Status.StatusCode = (oError != null ? 1 : 0);
            return result;
        }
    }
}
