﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ApiRiepiloghi;
using Newtonsoft.Json;

namespace WebApiRiepiloghi.Controllers
{
    public class RiepiloghiController : ApiController
    {
        [HttpPost]
        public ResponseRiepiloghi GetCodiceSistema(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetCodiceSistema(parPayload.Token, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpGet]
        public ResponseRiepiloghi CheckWrite()
        {
            ResponseRiepiloghi result = Utility.CheckWrite();
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetListaCampiLogTransazioniCS(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetListaCampiLogTransazioniCS(parPayload.Token, Utility.GetEnvironment(), false);
            
            result = Utility.GetResponse(rb);

            if (rb.StatusOK)
            {
                ApiRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLogTransazioni listaCampiLog = ApiRiepiloghi.ServiceRiepiloghi.GetListaCampiLogTransazioniCS(parPayload.Token, Utility.GetEnvironment(), true);
                List<clsResultBaseCampoLogTransazioni> campiLogNew = null;

                if (listaCampiLog != null && listaCampiLog.Campi != null && listaCampiLog.Campi.Length > 0)
                {
                    campiLogNew = new List<clsResultBaseCampoLogTransazioni>();
                    foreach (ApiRiepiloghi.ServiceRiepiloghi.clsResultBaseCampoLogTransazioni campo in listaCampiLog.Campi)
                    {
                        campiLogNew.Add(new clsResultBaseCampoLogTransazioni(campo.Campo, campo.Tipo, campo.Inizio, campo.Dimensione, campo.DescCampo, campo.Descrizione, campo.NomeFiscale));
                    }
                    result.Data.CampiSmart = campiLogNew.ToArray();
                }


                ApiRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLta listaCampiLta = null;
                List<clsResultBaseCampoLta> campiNew = null;

                listaCampiLta = ApiRiepiloghi.ServiceRiepiloghi.GetListaCampiLtaCS(parPayload.Token, Utility.GetEnvironment(), false);

                if (listaCampiLta != null && listaCampiLta.CampiLta != null && listaCampiLta.CampiLta.Length > 0)
                {
                    campiNew = new List<clsResultBaseCampoLta>();
                    foreach (ApiRiepiloghi.ServiceRiepiloghi.clsResultBaseCampoLta campo in listaCampiLta.CampiLta)
                    {
                        campiNew.Add(new clsResultBaseCampoLta(campo.Campo, campo.Tipo, campo.Inizio, campo.Dimensione, campo.DescCampo, campo.Descrizione, campo.NomeFiscale));
                    }
                    result.Data.CampiLta = campiNew.ToArray();

                }

                listaCampiLta = ApiRiepiloghi.ServiceRiepiloghi.GetListaCampiLtaCS(parPayload.Token, Utility.GetEnvironment(), true);

                if (listaCampiLta != null && listaCampiLta.CampiLta != null && listaCampiLta.CampiLta.Length > 0)
                {
                    campiNew = new List<clsResultBaseCampoLta>();
                    foreach (ApiRiepiloghi.ServiceRiepiloghi.clsResultBaseCampoLta campo in listaCampiLta.CampiLta)
                    {
                        campiNew.Add(new clsResultBaseCampoLta(campo.Campo, campo.Tipo, campo.Inizio, campo.Dimensione, campo.DescCampo, campo.Descrizione, campo.NomeFiscale));
                    }
                    result.Data.CampiLtaSmart = campiNew.ToArray();
                }

                result.Data.CodiciLocali = null;
                ApiRiepiloghi.ServiceRiepiloghi.ResultBaseCodiciLocali resultCodiciLocali = ApiRiepiloghi.ServiceRiepiloghi.GetListaCodiciLocale(parPayload.Token, Utility.GetEnvironment());

                if (resultCodiciLocali != null && resultCodiciLocali.CodiciLocali != null)
                {
                    result.Data.CodiciLocali = resultCodiciLocali.CodiciLocali;
                }
            }

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetListaCampiLtaCS(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetListaCampiLtaCS(parPayload.Token, Utility.GetEnvironment(), false);
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetTransazioni(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            bool lDataEvento = (!string.IsNullOrEmpty(parPayload.Mode) && !string.IsNullOrWhiteSpace(parPayload.Mode) && parPayload.Mode.Trim().ToUpper() == "DATA_EVENTO");
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioni(parPayload.Token, parPayload.DataEmiInizio, parPayload.DataEmiFine, lDataEvento, "TRUE", loadLta, "FALSE", 0, parPayload.Mode, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        
        [HttpPost]
        public ResponseRiepiloghi GetLta(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            bool lDataEmissione = (parPayload.Mode != null && !string.IsNullOrEmpty(parPayload.Mode) && !string.IsNullOrWhiteSpace(parPayload.Mode) && parPayload.Mode.Trim().ToUpper() == "DATA_EMISSIONE");
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetLta(parPayload.Token, parPayload.DataEmiInizio, parPayload.DataEmiFine, lDataEmissione, null, null, loadLog, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        

        [HttpPost]
        public ResponseRiepiloghi GetTransazioniAnnullati(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioniAnnullati(parPayload.Token, parPayload.DataEmiInizio, parPayload.DataEmiFine, "TRUE", loadLta, "FALSE", 0, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi GetTransazioneCarta(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            ServiceRiepiloghi.ResultTransazioneCartaSigilloProgressivo rb = null;

            switch (parPayload.Mode)
            {
                case "CARTA_PROGRESSIVO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(parPayload.Token, parPayload.Carta, parPayload.Progressivo, loadLta, "FALSE", Utility.GetEnvironment());
                    break;
                case "CARTA_SIGILLO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioneCARTA_SIGILLO(parPayload.Token, parPayload.Carta, parPayload.Sigillo, loadLta, "FALSE", Utility.GetEnvironment());
                    break;
                case "CARTA_PROGRESSIVI":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioneCARTA_PROGRESSIVI(parPayload.Token, parPayload.Carta, parPayload.PrimoProgressivo, parPayload.UltimoProgressivo, loadLta, "FALSE", Utility.GetEnvironment());
                    break;
                case "EVENTO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioniEVENTO(parPayload.Token, parPayload.DataEvento.Value, parPayload.CodiceLocale, parPayload.OraEvento, parPayload.TitoloEvento, loadLta, "FALSE", Utility.GetEnvironment());
                    break;

            }

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetEventi(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            ServiceRiepiloghi.ResultBaseGetEventi rb = null;

            rb = ApiRiepiloghi.ServiceRiepiloghi.GetEventi(parPayload.Token, parPayload.DataEvento.Value, parPayload.CodiceLocale, parPayload.OraEvento, parPayload.TitoloEvento, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetTransazioneCARTA_PROGRESSIVO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(parPayload.Token, parPayload.Carta, parPayload.Progressivo, loadLta, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetTransazioneCARTA_SIGILLO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetTransazioneCARTA_SIGILLO(parPayload.Token, parPayload.Carta, parPayload.Sigillo, loadLta, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }



        [HttpPost]
        public ResponseRiepiloghi GetLtaCarta(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            ServiceRiepiloghi.ResultLtaCartaSigilloProgressivo rb = null;

            switch (parPayload.Mode)
            {
                case "CARTA_PROGRESSIVO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetLtaCARTA_PROGRESSIVO(parPayload.Token, parPayload.Carta, parPayload.Progressivo, loadLog, "FALSE", Utility.GetEnvironment());
                    break;
                case "CARTA_SIGILLO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.GetLtaCARTA_SIGILLO(parPayload.Token, parPayload.Carta, parPayload.Sigillo, loadLog, "FALSE", Utility.GetEnvironment());
                    break;

            }

            result = Utility.GetResponse(rb);

            return result;
        }

        

        [HttpPost]
        public ResponseRiepiloghi GetLtaCARTA_PROGRESSIVO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetLtaCARTA_PROGRESSIVO(parPayload.Token, parPayload.Carta, parPayload.Progressivo, loadLog, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetLtaCARTA_SIGILLO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetLtaCARTA_SIGILLO(parPayload.Token, parPayload.Carta, parPayload.Sigillo, loadLog, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi SetStatoLta(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            ServiceRiepiloghi.ResultLtaCartaSigilloProgressivo rb = null;

            switch (parPayload.Mode)
            {
                case "CARTA_PROGRESSIVO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.SetStatoLtaCARTA_PROGRESSIVO(parPayload.Token, parPayload.CambioStatoLta, parPayload.Carta, parPayload.Progressivo, loadLog, "FALSE", Utility.GetEnvironment());
                    break;
                case "CARTA_SIGILLO":
                    rb = ApiRiepiloghi.ServiceRiepiloghi.SetStatoLtaCARTA_SIGILLO(parPayload.Token, parPayload.CambioStatoLta, parPayload.Carta, parPayload.Sigillo, loadLog, "FALSE", Utility.GetEnvironment());
                    break;

            }

            result = Utility.GetResponse(rb);

            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi SetStatoLtaCARTA_PROGRESSIVO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SetStatoLtaCARTA_PROGRESSIVO(parPayload.Token, parPayload.CambioStatoLta, parPayload.Carta, parPayload.Progressivo, loadLog, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi SeStatotLtaCARTA_SIGILLO(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLog = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SetStatoLtaCARTA_SIGILLO(parPayload.Token, parPayload.CambioStatoLta, parPayload.Carta, parPayload.Sigillo, loadLog, "FALSE", Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi CheckAnnulloTransazioni(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            bool loadLta = true;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CheckAnnulloTransazioni(parPayload.Token, parPayload.Carta, parPayload.Sigillo, parPayload.PrimoProgressivo, parPayload.UltimoProgressivo, parPayload.IdCalend, 
                parPayload.CodiceFiscaleOrganizzatore, parPayload.TitoloAbbonamento, parPayload.DataEmissione, parPayload.DataEvento, loadLta, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        //AnnulloTransazione(string Token, string Carta, string Sigillo, Int64? Progressivo, string CausaleAnnullamento, clsEnvironment oEnvironment)


        [HttpPost]
        public ResponseRiepiloghi AnnulloTransazione(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;

            var rb = ApiRiepiloghi.ServiceRiepiloghi.AnnulloTransazione(parPayload.Token, parPayload.Carta, parPayload.Sigillo, parPayload.Progressivo, parPayload.CausaleAnnullamento, true, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ListaRiepiloghi(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ListaRiepiloghi(parPayload.Token, parPayload.Mode, parPayload.DataInizio, parPayload.DataFine, parPayload.TipoRiepilogo, parPayload.ProgressivoRiepilogo, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GeneraRiepilogo(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CreaRiepilogo(parPayload.Token, parPayload.DataRiepilogo, parPayload.TipoRiepilogo, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi StampaRiepilogo(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.PrintRiepilogoToPDF(parPayload.Token, parPayload.DataRiepilogo, parPayload.TipoRiepilogo, parPayload.Progressivo, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi StampaTransazioniAnnullati(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            string cDataEmissioneAnnullamento = (parPayload.Filtri != null ? parPayload.Filtri.DataEmissioneAnnullamento : "");
            string cDataEvento = (parPayload.Filtri != null ? parPayload.Filtri.DataEvento : "");
            string cCodiceLocale = (parPayload.Filtri != null ? parPayload.Filtri.CodiceLocale : "");
            string cOraEvento = (parPayload.Filtri != null ? parPayload.Filtri.OraEvento : "");
            string cTitoloEvento = (parPayload.Filtri != null ? parPayload.Filtri.TitoloEvento : "");

            var rb = ApiRiepiloghi.ServiceRiepiloghi.StampaTransazioniAnnullati(parPayload.Token, parPayload.DataEmiInizio, parPayload.DataEmiFine, cDataEmissioneAnnullamento, cDataEvento, cCodiceLocale, cOraEvento, cTitoloEvento, "TRUE", 0, Utility.GetEnvironment());

            result = Utility.GetResponse(rb);

            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi SendReceiveRiepiloghi(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SendReceiveRiepiloghi(parPayload.Token, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi SpostaRiepilogoSpeditoInDaSpedire(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            Riepiloghi.clsRiepilogoGenerato oRiepilogo = new Riepiloghi.clsRiepilogoGenerato();
            oRiepilogo.Giorno = parPayload.DataRiepilogo;
            oRiepilogo.Progressivo = parPayload.ProgressivoRiepilogo;
            oRiepilogo.Tipo = parPayload.TipoRiepilogo;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SpostaRiepilogoSpeditoInDaSpedire(parPayload.Token, Utility.GetEnvironment(), oRiepilogo, false);
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi RispedisciRiepilogo(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            Riepiloghi.clsRiepilogoGenerato oRiepilogo = new Riepiloghi.clsRiepilogoGenerato();
            oRiepilogo.Giorno = parPayload.DataRiepilogo;
            oRiepilogo.Progressivo = parPayload.ProgressivoRiepilogo;
            oRiepilogo.Tipo = parPayload.TipoRiepilogo;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SpostaRiepilogoSpeditoInDaSpedire(parPayload.Token, Utility.GetEnvironment(), oRiepilogo, true);
            result = Utility.GetResponse(rb);
            return result;
        }



        [HttpPost]
        public ResponseRiepiloghi TornelloLogin(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            string urlTornello = System.Configuration.ConfigurationManager.AppSettings["url_tornello"];
            string tornello_pcName_debug = (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains<string>("tornello_pcName_debug") ? System.Configuration.ConfigurationManager.AppSettings["tornello_pcName_debug"] : "");
            var rb = ApiRiepiloghi.ServiceRiepiloghi.LoginTornelloLta(parPayload.Token, urlTornello, tornello_pcName_debug, parPayload.Tornello_Login, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi TornelloCheckBarCode(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            string urlTornello = System.Configuration.ConfigurationManager.AppSettings["url_tornello"];
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CheckVoidTornelloLta(parPayload.Token, urlTornello, false, parPayload.Tornello_Login, parPayload.Tornello_Barcode, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi TornelloVoidBarCode(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            string urlTornello = System.Configuration.ConfigurationManager.AppSettings["url_tornello"];
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CheckVoidTornelloLta(parPayload.Token, urlTornello, true, parPayload.Tornello_Login, parPayload.Tornello_Barcode, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetProprietaRiepiloghi(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetProprietaRiepiloghi(parPayLoad.Token, parPayLoad.Proprieta, "", Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi GetEmailCodiceSistema(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetEmailCodiceSistema(parPayLoad.Token, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi GetConfigBordero(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.GetConfigBordero(parPayLoad.Token, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi SetConfigBordero(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.SetConfigBordero(parPayLoad.Token, parPayLoad.ConfigurazioneBordero, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi CalcBordero(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.CalcBordero(parPayLoad.Token, parPayLoad.DataInizio, parPayLoad.DataFine, parPayLoad.QuoteManualiEscluse, parPayLoad.QuoteManualiTe, parPayLoad.QuoteManualiSi, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi BorderoSearchEvento(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.BorderoSearchEvento(parPayLoad.Token, parPayLoad.BorderoSearchEvento, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi BorderoInsertCedas(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.BorderoInsertCedas(parPayLoad.Token, parPayLoad.BorderoCedas, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi BorderoUpdateCedas(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.BorderoUpdateCedas(parPayLoad.Token, parPayLoad.BorderoCedas, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ScsGetListaSale(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsGetListaSale(parPayLoad.Token, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ScsGetNewScs(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsGetNewScs(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ScsAbortScs(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsAbortScs(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }


        [HttpPost]
        public ResponseRiepiloghi ScsConfirmScs(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsConfirmScs(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ScsDefinitivoScs(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsDefinitivoScs(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        [HttpPost]
        public ResponseRiepiloghi ScsGetScsStorici(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsGetScsStorici(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }

        //
        [HttpPost]
        public ResponseRiepiloghi ScsArchiviaScsProvvisorio(PayLoadRiepiloghi parPayLoad)
        {
            ResponseRiepiloghi result = null;
            var rb = ApiRiepiloghi.ServiceRiepiloghi.ScsArchiviaScsProvvisorio(parPayLoad.Token, parPayLoad.Scs, Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }
    }
}
