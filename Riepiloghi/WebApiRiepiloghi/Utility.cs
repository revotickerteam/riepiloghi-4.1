﻿using ApiRiepiloghi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRiepiloghi
{
    public static class Utility
    {
        [System.Diagnostics.DebuggerStepThrough]
        public static clsEnvironment GetEnvironment()
        {
            var result = new clsEnvironment()
            {
                InternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["internalConnectionString"].ConnectionString,
                ExternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["externalConnectionString"].ConnectionString,
                PathFont = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["pathFont"]),
                PathRisorseWeb = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["pathRisorseWeb"]),
                PathTemp = HttpContext.Current.Server.MapPath("/App_Data"),
                PathWriteRiepiloghi = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["pathWrite"])
            };
            return result;
        }

        public static ResponseRiepiloghi CheckWrite()
        {
            ResponseRiepiloghi result = new ResponseRiepiloghi();
            result.Status = new ResultBase();
            result.Status.StatusCode = 0;
            string message = "";
            try
            {
                System.IO.File.WriteAllText(GetEnvironment().PathTemp + @"\provaTemp.txt", "prova");
                System.IO.File.Delete(GetEnvironment().PathTemp + @"\provaTemp.txt");
            }
            catch (Exception ex)
            {
                message = (message.Trim() == "" ? "" : "\r\n") + "PathTemp ERROR:" + ex.Message;
            }

            try
            {
                System.IO.File.WriteAllText(GetEnvironment().PathWriteRiepiloghi + @"\provaWrite.txt", "prova");
                System.IO.File.Delete(GetEnvironment().PathWriteRiepiloghi + @"\provaWrite.txt");
            }
            catch (Exception ex)
            {
                message = (message.Trim() == "" ? "" : "\r\n") + "PathWriteRiepiloghi ERROR:" + ex.Message;
            }

            result.Status.StatusMessage = message;
            result.Status.StatusOK = result.Status.StatusMessage == "";
            return result;
        }

        public static ResponseRiepiloghi GetResponse(Object parResponse)
        {
            if(parResponse == null)
            {
                return new ResponseRiepiloghi()
                {
                    Status = {
                        StatusOK = false,
                        StatusMessage = "parResponse = null"
                    }
                }; 
            }

            var result = new ResponseRiepiloghi()
            {
                Data = JsonConvert.DeserializeObject<ResponseDataRiepiloghi>(JsonConvert.SerializeObject(parResponse)),
                Status = JsonConvert.DeserializeObject<ResultBase>(JsonConvert.SerializeObject(parResponse))
            };

            return result;
        }
    }
}