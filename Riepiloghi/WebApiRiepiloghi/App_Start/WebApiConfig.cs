﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;


namespace WebApiRiepiloghi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Servizi e configurazione dell'API Web

            // Route dell'API Web
            var origins = System.Configuration.ConfigurationManager.AppSettings["origins"];

            var cors = new EnableCorsAttribute(
                     origins: origins,
                     headers: "*",
                     methods: "*");

            config.EnableCors(cors);

            // Route dell'API Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var jss = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            jss.Converters.Add(new IsoDateTimeConverter());

            config.Formatters.JsonFormatter.SerializerSettings = jss;
        }
    }
}