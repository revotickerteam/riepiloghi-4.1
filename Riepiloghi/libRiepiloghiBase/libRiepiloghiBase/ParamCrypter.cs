﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Net;

namespace libRiepiloghiBase
{
    public class clsParameterValues
    {
        public string ACTION { get; set; }  // GET_SN, FIRMA
        public string SLOT { get; set; }  // 0, 1...
        public string CODICE_SISTEMA { get; set; }
        public string SERIAL_NUMBER { get; set; }
        public string PIN { get; set; }
        public string FILE_LOG { get; set; }
        public string FILE_RPGM { get; set; }
        public string FILE_EML { get; set; }
        public string MAIL_FROM { get; set; }
        public string MAIL_TO { get; set; }
        public string ERROR { get; set; }
        public string FILE_LOG_FIRMATO { get; set; }
        public string FILE_RPGM_FIRMATO { get; set; }
        public string FILE_EML_FIRMATO { get; set; }
        public string[] MESSAGES { get; set; }
    }
    public class clsParameterCryptData
    {
        public static string key = "F5AZ450FE3118D0B7EDF48AF";
        public static string Encrypt(string toEncrypt)
        {
            string parCryptKey = key;
            return Encrypt(toEncrypt, parCryptKey, false);
        }

        private static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider hashmd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            System.Security.Cryptography.TripleDESCryptoServiceProvider tdes = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = System.Security.Cryptography.CipherMode.ECB;
            tdes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            System.Security.Cryptography.ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt)
        {
            string parCryptKey = key;
            return Decrypt(toDecrypt, parCryptKey, false);
        }

        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider hashmd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            System.Security.Cryptography.TripleDESCryptoServiceProvider tdes = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = System.Security.Cryptography.CipherMode.ECB;
            tdes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            System.Security.Cryptography.ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

    }
    
}
