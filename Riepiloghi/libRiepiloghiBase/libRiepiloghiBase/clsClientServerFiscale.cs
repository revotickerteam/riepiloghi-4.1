﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Sockets;
//using Wintic.Data;

namespace libRiepiloghiBase
{

    public class clsClientServerFiscale : IDisposable
    {

        #region "Dichiarazioni"

        private Int64 nIdSocket = 0;
        private string cServerIP = "";
        private int nPort = 0;
        private TcpClient oClient = null;
        private NetworkStream oNetworkStream = null;
        //private static StreamReader oStreamReader = null;
        private StreamWriter oStreamWriter = null;

        private Exception oExceptionClient = null;

        private string cPassword = "";
        private string cClient_ID = "";

        private const string SEND_CHECK_SERVER_FISCALE = "666";
        private const string SEND_ANNULLO_BIGLIETTO_IVA_DA_ASSOLVERE = "002";
        private const string SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA = "004";
        private const string SEND_ANNULLO_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA = "010";
        private const string SEND_ANNULLO_ABBONAMENTO = "006";
        private const string SEND_ANNULLO_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA = "008";
        private const string SEND_UTILIZZO_SMART_CARD = "999";
        private const string SEND_RILASCIO_SMART_CARD = "998";

        #endregion
        
        #region "Proprieta"

        // Eccezione
        public Exception ExceptionClient
        {
            get { return oExceptionClient; }
            set { oExceptionClient = value; }
        }

        // Ritorna TRUE se il CLIENT_ID e la PASSWORD sono stati inizializzati
        public bool ClientConfigValido
        {
            get
            {
                return ((cClient_ID.Trim().Length == 3) && (cPassword.Trim().Length == 8));
            }
        }

        // Ritorna true se l'IP e la PORTA di comunicazione con il server fiscale sono stati inizializzati
        public bool SocketConfigValido
        {
            get
            {
                return ((cServerIP.Trim() != "") && (nPort > 0));
            }
        }

        // Verifica se il client è valido
        public bool ClientValido
        {
            get { return ClientConfigValido && SocketConfigValido; }
        }

        #endregion

        #region "Metodi"

        // Connessione con il server fiscale
        private bool Connect()
        {
            bool lRet = false;
            try
            {
                oClient = new TcpClient();
                oClient.ReceiveTimeout = 20;
                
                oClient.Connect(cServerIP, nPort);
                lRet = oClient.Connected;
                oNetworkStream = oClient.GetStream();
                //oStreamReader = new StreamReader(oNetworkStream);
                oStreamWriter = new StreamWriter(oNetworkStream);
                lRet = true;
            }
            catch(Exception exConnect)
            {
                lRet = false;
                ExceptionClient = exConnect;
            }
            return lRet;
        }

        // Chiusura connessione con il server fiscale
        private void Close()
        {
            try
            {
                if (oClient != null)
                {
                    oClient.Close();
                }
            }
            catch
            {
            }
        }


        // Spedizione
        private bool Send(string cMsg)
        {
            bool lRet = false;
            try
            {
                oStreamWriter.WriteLine(cMsg);
                oStreamWriter.Flush();
                lRet = true;
            }
            catch(Exception exSend)
            {
                ExceptionClient = exSend;
                ExceptionClient = new Exception("Send error " + exSend.Message);
                lRet = false;
            }
            return lRet;
        }

        // Ricezione
        private bool GetDataReceive(ref String message)
        {
            String clientData = "";

            try
            {
                // Data buffer for incoming data.
                byte[] bytes;

                bytes = new byte[oClient.ReceiveBufferSize];

                if (oNetworkStream.DataAvailable)
                {

                    int BytesRead = oNetworkStream.Read(bytes, 0, (int)oClient.ReceiveBufferSize - 1);
                    if (BytesRead > 0)
                    {
                        clientData = Encoding.ASCII.GetString(bytes, 0, BytesRead);

                        message = clientData.Replace("\r", string.Empty).Replace("\n", string.Empty);

                    }
                }
            }
            catch (Exception e)
            {
                ExceptionClient = new Exception("GetDataReceived error " + e.Message);
                return false;
            }
            return true;
        }

        private string Receive()
        {
            string cReceive = "";
            System.Diagnostics.Stopwatch timeout = new System.Diagnostics.Stopwatch();
            timeout.Start();

            long maxTimeOut = 20000;

            while (true)
            {
                try
                {
                    if (!oClient.Connected)
                    {
                        timeout.Stop();
                        ExceptionClient = new Exception("Disconnesso dal Server Fiscale");
                        break;
                    }

                    if (timeout.ElapsedMilliseconds > maxTimeOut)
                    {
                        timeout.Stop();
                        ExceptionClient = new Exception("Timeout comunicazione Server Fiscale");
                        break;
                    }

                    if (oNetworkStream.DataAvailable)
                    {
                        timeout.Stop();
                        string buffer = "";
                        if (GetDataReceive(ref buffer))
                        {
                            cReceive = buffer;
                        }
                        else
                        {
                            ExceptionClient = new Exception("Timeout comunicazione Server Fiscale");
                        }
                        break;
                    }
                }
                catch (Exception exSendReceive)
                {
                    ExceptionClient = new Exception("Receive error " +  exSendReceive.Message);
                    break;
                }
            }

            return cReceive;
        }


        // Verifica server fiscale
        public bool CheckServerFiscale(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);
            ExceptionClient = null;
            if (lRet)
            {
                try
                {
                    lRet = Connect();
                    if (lRet)
                    {
                        lRet = Send(cClient_ID + cPassword + clsClientServerFiscale.SEND_CHECK_SERVER_FISCALE);
                        
                        if (lRet)
                        {
                            string cReceive = this.Receive(); ;
                            lRet = ExceptionClient == null && cReceive.EndsWith("000");
                        }
                        Close();
                    }
                }
                catch (Exception exCheckServerFiscale)
                {
                    ExceptionClient = exCheckServerFiscale;
                    lRet = false;
                }
            }
            return lRet;
        }

        #endregion

        #region "Inizializzazione da database"

        //oParametriServerFiscale
        private bool GetParametriConnessioneServerFiscale(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale)
        {
            bool lRet = false;
            try
            {
                nIdSocket = oParametriServerFiscale.IdSocket;
                cServerIP = oParametriServerFiscale.ServerIP;
                nPort = oParametriServerFiscale.Port;
                cClient_ID = oParametriServerFiscale.Client_ID;
                cPassword = oParametriServerFiscale.Password;
                lRet = ClientValido;
            }
            catch
            {
            }
            return lRet;
        }

        #endregion

        #region "Metodi di richiesta del sigillo di annullo al server fiscale"

        // Annullo biglietto IVA da assolvere
        public bool RichiediAnnulloBigliettoIvaDaAssolvere(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_DA_ASSOLVERE, "", 0);
        }

        // Annullo biglietto IVA preassolta (rateo)
        public bool RichiediAnnulloBigliettoIvaPreassolta(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA,  CodiceBigliettoAbbonamento,  NumProgBigliettoAbbonamento);
        }

        // Annullo biglietto IVA certificata tramite fattura
        public bool RichiediAnnulloBigliettoIvaCertificataTramiteFattura(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        // Annullo abbonamento IVA da assolvere
        public bool RichiediAnnulloAbbonamentoIvaDaAssolvere(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_ABBONAMENTO, "", 0);
        }

        // Annullo abbonamento IVA certificata tramite fattura
        public bool RichiediAnnulloAbbonamentoIvaCertificataTramiteFattura(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        // Annullo sigillo
        private bool RichiediAnnullo(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string TipoDiAnnullo, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);

            if (lRet)
            {
                try
                {
                    if (Connect())
                    {
                        string cSend = cClient_ID + cPassword + TipoDiAnnullo + nIdVend.ToString("00000000");

                        if (TipoDiAnnullo == clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA)
                            cSend += NumProgBigliettoAbbonamento.ToString().Trim().PadLeft(6, '0').Trim() + CodiceBigliettoAbbonamento;

                        if (Send(cSend))
                        {
                            string cReceive = Receive();
                            if (!string.IsNullOrEmpty(cReceive))
                            {
                                lRet = (cReceive.Length >= 25) &&
                                       cReceive.Substring(0, 3) == cClient_ID &&
                                       cReceive.Substring(3, 8) == cPassword &&
                                       cReceive.Substring(11, 3) == TipoDiAnnullo &&
                                       cReceive.Substring(14, 3) == "000" &&
                                       cReceive.Substring(17, 8) == nIdVend.ToString("00000000");
                                if (lRet)
                                {
                                    cSend = cClient_ID + cPassword + TipoDiAnnullo + "000";
                                }
                                else
                                {
                                    cSend = cClient_ID + cPassword + TipoDiAnnullo + "001";
                                }

                                if (oClient.Connected)
                                    Send(cSend);
                            }
                            else
                                lRet = false;
                        }
                        Close();
                    }
                }
                catch (Exception exRichiediAnnullo)
                {
                    ExceptionClient = exRichiediAnnullo;
                    lRet = false;
                }
            }
            return lRet;
        }

        // Richiesta utilizzo smartcard
        public bool RichiestaUtilizzoSmartCard(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale, out int nSlot, out bool requestSent)
        {
            requestSent = false;
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);
            nSlot = -1;
            
            if (lRet)
            {
                try
                {
                    lRet = Connect();
                    if (lRet)
                    {
                        string cSend = cClient_ID + cPassword + clsClientServerFiscale.SEND_UTILIZZO_SMART_CARD;
                        string cReceive = "";
                        lRet = Send(cSend);
                        if (lRet)
                        {
                            requestSent = true;
                            cReceive = Receive();

                            lRet = !string.IsNullOrEmpty(cReceive) && !string.IsNullOrWhiteSpace(cReceive) && cReceive.Length >= 18;

                            if (!lRet)
                                ExceptionClient = new Exception("errore nella risposta " + (cReceive == null ? "null" : cReceive));

                            if (lRet)
                            {
                                lRet = cReceive.Substring(0, 3) == cClient_ID &&
                                       cReceive.Substring(3, 8) == cPassword &&
                                       cReceive.Substring(11, 3) == clsClientServerFiscale.SEND_UTILIZZO_SMART_CARD &&
                                       cReceive.Substring(14, 3) == "000";
                            }
                            if (lRet)
                            {
                                lRet = int.TryParse(cReceive.Substring(17), out nSlot);
                                if (!lRet)
                                {
                                    nSlot = -1;
                                    if (ExceptionClient == null)
                                    {
                                        ExceptionClient = new Exception("Errore invio richiesta al server fiscale per utilizzo smartcard, slot non valido.");
                                    }
                                }
                            }
                            else
                            {
                                ExceptionClient = new Exception("Errore invio richiesta al server fiscale per utilizzo smartcard, risposta non conforme.");
                            }
                        }
                        else
                        {
                            if (ExceptionClient == null)
                            {
                                ExceptionClient = new Exception("Errore invio richiesta al server fiscale per utilizzo smartcard.");
                            }
                        }
                        Close();
                    }
                    else
                    {
                        if (ExceptionClient == null)
                        {
                            ExceptionClient = new Exception("Errore connessione al server fiscale per utilizzo smartcard.");
                        }
                    }
                }
                catch (Exception exCheckServerFiscale)
                {
                    ExceptionClient = exCheckServerFiscale;
                    lRet = false;
                }
            }
            return lRet;
        }

        // Richiesta rilascio smartcard
        public bool RichiestaRilascioSmartCard(libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);
            if (lRet)
            {
                try
                {
                    lRet = Connect();
                    if (lRet)
                    {
                        string cSend = cClient_ID + cPassword + clsClientServerFiscale.SEND_RILASCIO_SMART_CARD;
                        lRet = Send(cSend);
                        if (!lRet)
                        {
                            if (ExceptionClient == null)
                            {
                                ExceptionClient = new Exception("Errore invio richiesta al server fiscale per rilascio smartcard.");
                            }
                        }
                        Close();
                    }
                    else
                    {
                        if (ExceptionClient == null)
                        {
                            ExceptionClient = new Exception("Errore connessione al server fiscale per rilascio smartcard.");
                        }
                    }
                    Close();
                }
                catch (Exception exCheckServerFiscale)
                {
                    ExceptionClient = exCheckServerFiscale;
                    lRet = false;
                }
            }
            return lRet;
        }

        public void Dispose()
        {
            if (oStreamWriter != null)
            {
                oStreamWriter.Dispose();
                oStreamWriter = null;
            }

            if (oNetworkStream != null)
            {
                oNetworkStream.Dispose();
                oNetworkStream = null;
            }
            if (oClient != null)
            {
                oClient.Dispose();
                oClient = null;
            }


            

            oExceptionClient = null;
        }

    #endregion
}

       
}
