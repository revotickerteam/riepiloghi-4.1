﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime;
using System.Runtime.InteropServices;
using System.IO;


namespace libRiepiloghiBase
{


    public interface iLib_SIAE_Provider
    {
        string GetSerialNumber(int Slot, out Exception Error);
        bool InitializeCARD(int Slot, out Exception Error);
        bool FinalizeCARD(int Slot, out Exception Error);

        bool FirmaFile(string Pin, int Slot, string FileDaFirmare, out Exception Error);
        bool GeneraEmail(string Pin, int Slot, string FileEml, string FileAllegato, string Mittente, string Destinatario, string Corpo, string Soggetto, out Exception Error);

        bool InviaEmail(string FileEml, string host, int port, string user, string password, List<string> cc, List<string> bcc, out Exception Error);

        List<object> RiceviEmail(string accountEmail, string user, string password, string pop3Server, int port, bool userSsl, object LocalInboxProvider, string codiceSistema, out Exception error);

        List<string> ProviderOperations();

        string DllPath {get;set;}
    }

    public class clsLibSigillo
    {
        //private static System.Reflection.Assembly assemblyGetSerialNumber = null;
        //private static string assemblyGetSerialNumberType = "";

        //private static System.Reflection.Assembly assemblyFirmatEmail = null;
        //private static string assemblyFirmatEmailType = "";

        public static string GetDirApp()
        {
            System.IO.FileInfo oFileInfoApp = new System.IO.FileInfo(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            string path = oFileInfoApp.Directory.FullName;
            return path;
        }



        public static iLib_SIAE_Provider GetInstanceLibSIAE(out Exception Error, out List<string> Operations, out System.Reflection.Assembly oAssembly)
        {
            Error = null;
            Operations = new List<string>();
            string implementedSearchType = "libRiepiloghiBase.iLib_SIAE_Provider";
            iLib_SIAE_Provider result = null;
            oAssembly = null;
            Operations.Add("GetInstanceLibSIAE START");

            //if (assemblyGetSerialNumber == null)
            //{
            try
            {

                string path = GetDirApp();
                Operations.Add(string.Format("GetInstanceLibSIAE path {0}", path));
                List<string> listDll = new List<string>() { @"libRiepiloghiSiae.dll" };
                bool findFile = false;
                Exception errorLoadDll = null;
                foreach (string dllFileName in listDll)
                {
                    errorLoadDll = null;
                    Operations.Add(string.Format("GetInstanceLibSIAE dllFileName {0}", dllFileName));
                    try
                    {
                        string dllFileNameFull = path + @"\" + dllFileName;
                        Operations.Add(string.Format("GetInstanceLibSIAE dllFileNameFull {0}", dllFileNameFull));
                        if (System.IO.File.Exists(dllFileNameFull))
                        {
                            Operations.Add(string.Format("GetInstanceLibSIAE LoadFile {0}", dllFileNameFull));
                            findFile = true;
                            oAssembly = System.Reflection.Assembly.LoadFile(dllFileNameFull);
                            Type[] FileTypes = oAssembly.GetTypes();
                            Operations.Add(string.Format("GetInstanceLibSIAE FileTypes.Length {0}", (FileTypes != null ? FileTypes.Length.ToString() : "")));
                            System.Diagnostics.FileVersionInfo AssemblyVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(dllFileNameFull);
                            string AssemblyName = AssemblyVersion.OriginalFilename;
                            foreach (Type singleFileType in FileTypes)
                            {
                                Operations.Add(string.Format("GetInstanceLibSIAE singleFileType {0}", singleFileType.ToString()));
                                Type[] interfaceTypes = singleFileType.GetInterfaces();
                                Operations.Add(string.Format("GetInstanceLibSIAE interfaceTypes.Length {0}", (interfaceTypes != null ? interfaceTypes.Length.ToString() : "")));
                                for (int indexInterface = 0; indexInterface < interfaceTypes.Length; indexInterface++)
                                {
                                    Type implementedType = interfaceTypes[indexInterface];
                                    Operations.Add(string.Format("GetInstanceLibSIAE implementedType {0}", implementedType.ToString()));
                                    Operations.Add(string.Format("GetInstanceLibSIAE implementedType.FullName {0}", implementedType.FullName.ToString()));
                                    if (implementedType.FullName == implementedSearchType)
                                    {
                                        string currentPath = System.Environment.CurrentDirectory;
                                        Operations.Add(string.Format("GetInstanceLibSIAE currentPath {0}", currentPath));
                                        FileInfo oFileInfo = new FileInfo(dllFileNameFull);
                                        string dllPath = oFileInfo.Directory.FullName;
                                        Operations.Add(string.Format("GetInstanceLibSIAE dllPath {0}", dllPath));

                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                Operations.Add(string.Format("GetInstanceLibSIAE cambio path {0} {1}", currentPath, dllPath));
                                                System.Environment.CurrentDirectory = dllPath;
                                            }
                                            catch (Exception exChangePath)
                                            {
                                                Operations.Add(string.Format("GetInstanceLibSIAE cambio path errore {0}", exChangePath.ToString()));
                                            }
                                        }

                                        try
                                        {
                                            Operations.Add(string.Format("GetInstanceLibSIAE CreateInstance {0}", singleFileType.FullName.ToString()));
                                            result = (iLib_SIAE_Provider)oAssembly.CreateInstance(singleFileType.FullName);
                                            result.DllPath = dllFileNameFull;
                                            //assemblyGetSerialNumber = oAssembly;
                                            //assemblyGetSerialNumberType = singleFileType.FullName;
                                        }
                                        catch (Exception exCreateInstance)
                                        {
                                            Operations.Add(string.Format("GetInstanceLibSIAE exCreateInstance {0}", exCreateInstance.ToString()));
                                        }

                                        if (currentPath != dllPath)
                                        {
                                            try
                                            {
                                                Operations.Add(string.Format("GetInstanceLibSIAE ritorno path {0} {1}", currentPath, dllPath));
                                                System.Environment.CurrentDirectory = currentPath;
                                            }
                                            catch (Exception exReturnPath)
                                            {
                                                Operations.Add(string.Format("GetInstanceLibSIAE cambio path errore {0}", exReturnPath.ToString()));
                                            }
                                        }
                                    }
                                    if (result != null) break;
                                }
                                if (result != null) break;
                            }
                            if (result != null) break;
                        }
                    }
                    catch (System.Reflection.ReflectionTypeLoadException exLoad)
                    {
                        errorLoadDll = exLoad;
                        Operations.Add(string.Format("GetInstanceLibSIAE exLoad {0}", errorLoadDll.ToString()));
                    }
                    catch (Exception ex)
                    {
                        errorLoadDll = ex;
                        Operations.Add(string.Format("GetInstanceLibSIAE ex {0}", errorLoadDll.ToString()));
                    }
                }

                if (result == null || !findFile)
                {
                    string cDescListFiles = "";
                    foreach (string dllFileName in listDll)
                    {
                        string dllFileNameFull = path + @"\" + dllFileName;
                        cDescListFiles += "\r\n" + dllFileName;
                    }



                    if (!findFile)
                    {
                        if (Error != null)
                        {
                            Error = new Exception(Error.Message + "\r\n" + "Impossibile trovare le dll per GetInstanceLibSIAE" + cDescListFiles);
                        }
                        else
                        {
                            Error = new Exception("Impossibile trovare le dll per GetInstanceLibSIAE" + cDescListFiles);
                        }
                    }
                    else if (result == null)
                    {
                        if (Error != null)
                        {
                            Error = new Exception(Error.Message + "\r\n" + "dll non valide per GetInstanceLibSIAE" + cDescListFiles);
                        }
                        else
                        {
                            Error = new Exception("dll non valide per GetInstanceLibSIAE" + cDescListFiles);
                        }
                    }

                    Operations.Add(string.Format("GetInstanceLibSIAE Errore finale {0}", Error.ToString()));

                    if (errorLoadDll != null)
                    {
                        errorLoadDll = new Exception(Error.Message + errorLoadDll.Message);
                        Operations.Add(string.Format("GetInstanceLibSIAE errorLoadDll {0}", errorLoadDll.ToString()));
                    }
                }
            }
            catch (Exception exGenericGetInstance)
            {
                Error = exGenericGetInstance;
                Operations.Add(string.Format("GetInstanceLibSIAE exGenericGetInstance {0}", exGenericGetInstance.ToString()));
            }

            Operations.Add("GetInstanceLibSIAE END");
            return result;
        }

        public static string GetSerialNumber(int Slot, out Exception Error)
        {
            Error = null;
            string result = "";
            string cHeaderError = "Lettura Numero di serie Smartcard";
            System.Reflection.Assembly oAssembly = null;
            try
            {
                Exception errGetInstance = null;
                List<string> operations = new List<string>();
                libRiepiloghiBase.iLib_SIAE_Provider oGetSerialNumber = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out errGetInstance, out operations, out oAssembly);
                if (oGetSerialNumber != null && errGetInstance == null)
                {
                    Exception errorGetSn = null;
                    result = oGetSerialNumber.GetSerialNumber(Slot, out errorGetSn);
                    if (errorGetSn != null)
                    {
                        Error = GetNewException(cHeaderError, errorGetSn.Message, errorGetSn);
                    }
                }
                else
                {
                    if (errGetInstance != null)
                    {
                        Error = GetNewException(cHeaderError, "Errore caricamento modulo libRiepiloghiSiae32.dll o libRiepiloghiSiae64.dll " + errGetInstance.ToString());
                    }
                    else
                    {
                        Error = GetNewException(cHeaderError, "Errore caricamento modulo libRiepiloghiSiae32.dll o libRiepiloghiSiae64.dll", errGetInstance);
                    }
                    
                }
            }
            catch (Exception exInit)
            {
                Error = GetNewException(cHeaderError, exInit.Message, exInit);
            }

            if (oAssembly != null)
            {
                // dovrei scaricarlo
            }

            return result;
        }

        // ritorna un eccezione con descrizione
        public static Exception GetNewException(string cHeaderMessage, string cMessage)
        {
            return GetNewException(cHeaderMessage, cMessage, null);
        }

        // ritorna un eccezione con descrizione ed InnerException
        public static Exception GetNewException(string cHeaderMessage, string cMessage, Exception oException)
        {
            if (oException != null)
            {
                return new Exception("Errore imprevisto " + cHeaderMessage + " " + cMessage, oException);
            }
            else
            {
                return new Exception("Errore " + cHeaderMessage + " " + cMessage);
            }
        }

        // ritorna il nome della cartella dell'apprlicazione

        // Ritorna il nome del file del log delle transazioni di un giorno senza percorso e senza estensione
        public static string GetNomeLogGiornoBase(DateTime Giorno)
        {
            return "LOG_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00");
        }

        // Ritorna il nome del file del log delle transazioni di un giorno senza percorso con estensione
        public static string GetNomeLogGiorno(DateTime Giorno)
        {
            return GetNomeLogGiornoBase(Giorno) + ".txt";
        }

        // Ritorna una stringa di ricerca per i files di log incrementali
        public static string GetNomeLogSearchIncrementale(DateTime Giorno)
        {
            return GetNomeLogGiornoBase(Giorno) + "_*.txt";
        }

        // Ritorna il nome del file per salvare la mail del Riepilogo senza percorso
        public static string GetNomeEmailGiornoBase(DateTime Giorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            if (GiornalieroMensile == "G")
            {
                return "RPG_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00") + "_" + nProgressivo.ToString("000") + ".xsi.eml";
            }
            else if (GiornalieroMensile == "M")
            {
                return "RPM_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_00_" + nProgressivo.ToString("000") + ".xsi.eml";
            }
            else if (GiornalieroMensile == "R")
            {
                return "RCA_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00") + "_" + nProgressivo.ToString("000") + ".xsi.eml";
            }
            else
            {
                return "";
            }
        }

        public static string GetDescrizioneTipoRiepilogo(string GiornalieroMensile)
        {
            string result = "";
            if (GiornalieroMensile == "G")
            {
                result = "Giornaliero";
            }
            else if (GiornalieroMensile == "M")
            {
                result = "Mensile";
            }
            else if (GiornalieroMensile == "D")
            {
                result = "Simulazione data evento";
            }
            else if (GiornalieroMensile == "R")
            {
                result = "Rca";
            }
            return result;
        }

        // Ritorna il nome del file per salvare il riepilogo senza percorso
        public static string GetNomeRiepilogoBase(DateTime Giorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            if (GiornalieroMensile == "G")
            {
                return "RPG_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00") + "_" + nProgressivo.ToString("000") + ".xsi";
            }
            else if (GiornalieroMensile == "M")
            {
                return "RPM_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_00_" + nProgressivo.ToString("000") + ".xsi";
            }
            else if (GiornalieroMensile == "D")
            {
                return "Simulazione_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00") + "_" + nProgressivo.ToString("000") + ".xsi";
            }
            else if (GiornalieroMensile == "R")
            {
                return "RCA_" + Giorno.Year.ToString("0000") + "_" + Giorno.Month.ToString("00") + "_" + Giorno.Day.ToString("00") + "_" + nProgressivo.ToString("000") + ".xsi";
            }
            else
            {
                return "";
            }
        }

        // Descrizione di una data: DD-MM-YYYY se tipo G, MM-YYYY se tipo M
        public static string GetDescGiorno(DateTime Giorno, string GiornalieroMensile)
        {
            string result = "";
            if (GiornalieroMensile == "G")
            {
                result = Giorno.Day.ToString("00") + "-" + Giorno.Month.ToString("00") + "-" + Giorno.Year.ToString("0000");
            }
            else if (GiornalieroMensile == "M")
            {
                result = Giorno.Month.ToString("00") + "-" + Giorno.Year.ToString("0000");
            }
            else if (GiornalieroMensile == "D")
            {
                result = Giorno.Day.ToString("00") + "-" + Giorno.Month.ToString("00") + "-" + Giorno.Year.ToString("0000");
            }
            else if (GiornalieroMensile == "R")
            {
                result = Giorno.Day.ToString("00") + "-" + Giorno.Month.ToString("00") + "-" + Giorno.Year.ToString("0000");
            }
            return result;
        }


    }

    public class clsParametriServerFiscale
    {
        public Int64 IdSocket = 0;
        public string ServerIP = "";
        public int Port = 0;
        public string Client_ID = "";
        public string Password = "";

        public clsParametriServerFiscale()
        {
        }

        public clsParametriServerFiscale(Int64 nIdSocket, string cServerIP, int nPort, string cClient_ID, string cPassword)
        {
            this.IdSocket = nIdSocket;
            this.ServerIP = cServerIP;
            this.Port = nPort;
            this.Client_ID = cClient_ID;
            this.Password = cPassword;
        }
    }

    public class clsItemRiepilogoMasterizzazione
    {
        public DateTime Giorno { get; set; }
        public string Tipo { get; set; }
        public string Nome { get; set; }
        public long Prog { get; set; }
        public DateTime DataIns { get; set; }
        public long MasterStato { get; set; }
        public DateTime MasterDate { get; set; }
        public string ErrorMaster { get; set; }

        public string Path { get; set; }
        public System.IO.FileInfo File { get; set; }
        public byte[] ContentFile { get; set; }
    }
}
