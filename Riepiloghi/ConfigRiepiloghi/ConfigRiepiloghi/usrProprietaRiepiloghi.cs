﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigRiepiloghi
{
    public partial class usrProprietaRiepiloghi : UserControl
    {

        private Riepiloghi.clsProprietaRiepiloghi proprieta { get; set; }
        public delegate string DelegateButton(int mode, usrProprietaRiepiloghi usrItem);
        private DelegateButton DelegateFunction;
        private string RegularExpression = "";
        private bool initializingControls = false;
        private bool readOnly = false;
        public class clsItemProprietaCombo
        {
            public string Value { get; set; }
            public override string ToString()
            {
                return this.Value;
            }
        }

        public usrProprietaRiepiloghi()
        {
            InitializeComponent();
            this.SizeChanged += UsrProprietaRiepiloghi_SizeChanged;
        }

        private void UsrProprietaRiepiloghi_SizeChanged(object sender, EventArgs e)
        {
            this.AdjustSize();   
        }

        public void AdjustSize()
        {
            foreach (Control ctl in this.panelEdit.Controls)
            {
                if (ctl.Visible && ctl.Dock == DockStyle.None)
                {
                    ctl.Location = new Point(this.panelEdit.Padding.Left, (this.panelEdit.Height - ctl.Height) / 2);
                    ctl.Width = this.panelEdit.ClientSize.Width - this.panelEdit.Padding.Horizontal - (this.btnProprieta.Visible ? this.btnProprieta.Width - this.panelEdit.Padding.Right * 2 : 0);
                }
            }
        }

        public void SetReguralExpression(string value)
        {
            this.RegularExpression = value;
            if (this.RegularExpression != "")
                this.txtProprieta.TextChanged += TxtProprieta_TextChanged_REGEX;
            else 
                this.txtProprieta.TextChanged -= TxtProprieta_TextChanged_REGEX;
        }

        public void SetDelegateButton(DelegateButton delegateFunction)
        {
            this.DelegateFunction = delegateFunction;
            if (this.DelegateFunction != null)
            {
                this.btnProprieta.Text = this.DelegateFunction(0, null);
                this.btnProprieta.Visible = true;
                this.btnProprieta.BringToFront();
                this.btnProprieta.Click += BtnProprieta_Click;
                this.AdjustSize();
            }
        }

        public void SetPasswordChar(char passwordChar = '*')
        {
            this.txtProprieta.PasswordChar = passwordChar;
            if (this.txtProprieta.PasswordChar == '*')
                this.txtProprieta.MouseDoubleClick += TxtProprieta_MouseDoubleClick;
            else
                this.txtProprieta.MouseDoubleClick -= TxtProprieta_MouseDoubleClick;
        }

        private void TxtProprieta_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(this.txtProprieta.Text);
        }

        private void BtnProprieta_Click(object sender, EventArgs e)
        {
            string value = this.DelegateFunction(1, this);
            if (!string.IsNullOrEmpty(value) && !this.readOnly)
            {
                this.proprieta.Valore = value;
                this.SetValueControl();
            }
        }

        public Riepiloghi.clsProprietaRiepiloghi Proprieta
        {
            get { return this.proprieta; }
            set
            {
                this.proprieta = value;
                this.InitControls();
            }
        }

        public bool ReadOnly
        {
            get { return this.readOnly; }
            set 
            { 
                this.readOnly = value;
                this.txtProprieta.ReadOnly = this.readOnly;
                this.txtProprieta.ForeColor = (this.readOnly ? Color.DarkSlateGray : Color.Black);
                this.txtProprieta.BackColor = (this.readOnly ? Color.LightGray : Color.White);
                this.cmbProprieta.Enabled = (!this.readOnly);
                this.chkProprieta.Enabled = (!this.readOnly);
            }
        }

        

        private void ResetControls()
        {
            this.lblProprieta.Text = "Proprieta:";
            this.lblProprieta.ForeColor = Color.Black;
            this.txtProprieta.Visible = false;
            this.cmbProprieta.Visible = false;
            this.chkProprieta.Visible = false;
            this.btnProprieta.Visible = false;
            this.chkProprieta.CheckedChanged -= ChkProprieta_CheckedChanged;
            this.cmbProprieta.SelectedValueChanged -= CmbProprieta_SelectedValueChanged;
            this.txtProprieta.TextChanged -= TxtProprieta_TextChanged;
        }

        private void InitControls()
        {
            this.ResetControls();
            this.initializingControls = true;
            if (this.proprieta != null)
            {
                this.lblProprieta.Text = this.proprieta.Descrizione + ":";
                if (this.proprieta.Descrizione.Trim() == "")
                {
                    string f = "";
                }
                this.toolTip.SetToolTip(this.lblProprieta, this.proprieta.Proprieta);
                System.Drawing.Graphics g = this.lblProprieta.CreateGraphics();
                Size sizeDescr = g.MeasureString(this.lblProprieta.Text, this.Font).ToSize();
                if (sizeDescr.Width > 250)
                    sizeDescr.Width = 250;
                this.lblProprieta.Width = sizeDescr.Width + 5;
                if (this.proprieta.RegularExpression != null && !string.IsNullOrEmpty(this.proprieta.RegularExpression))
                {
                    if (this.proprieta.RegularExpression == @"^(?i)(ABILITATO|DISABILITATO)$")
                    {
                        this.chkProprieta.Visible = true;
                        this.chkProprieta.Text = "Abilitato";
                        this.chkProprieta.Enabled = (!this.readOnly);
                        this.chkProprieta.CheckedChanged += this.ChkProprieta_CheckedChanged;
                    }
                    else if (this.proprieta.RegularExpression.StartsWith(@"^(?i)(") && this.proprieta.RegularExpression.EndsWith(@")$") && this.proprieta.RegularExpression.Contains(@"|"))
                    {
                        bool lAppendValue = !string.IsNullOrEmpty(this.proprieta.Valore);
                        this.cmbProprieta.Visible = true;
                        clsItemProprietaCombo item;
                        foreach (string itemValue in this.proprieta.RegularExpression.Replace(@"^(?i)(", "").Replace(@")$", "").Split('|'))
                        {
                            item = new clsItemProprietaCombo();
                            item.Value = itemValue;
                            this.cmbProprieta.Items.Add(item);
                            if (lAppendValue)
                                lAppendValue = (itemValue != this.proprieta.Valore);
                        }
                        if (lAppendValue)
                        {
                            item = new clsItemProprietaCombo();
                            item.Value = this.proprieta.Valore;
                            this.cmbProprieta.Items.Add(item);
                        }
                        this.cmbProprieta.SelectedValueChanged += CmbProprieta_SelectedValueChanged;
                        this.cmbProprieta.Enabled = (!this.readOnly);
                    }
                    else if (this.proprieta.RegularExpression == @"^[0-9]+$")
                    {
                        this.txtProprieta.Visible = true;
                        this.txtProprieta.TextChanged += TxtProprieta_TextChanged;
                        this.txtProprieta.ReadOnly = this.readOnly;
                    }
                    else
                    {
                        this.txtProprieta.Visible = true;
                        this.txtProprieta.TextChanged += TxtProprieta_TextChanged;
                        this.txtProprieta.ReadOnly = this.readOnly;
                    }
                }
                else
                {
                    this.txtProprieta.Visible = true;
                    this.txtProprieta.TextChanged += TxtProprieta_TextChanged;
                    this.txtProprieta.ReadOnly = this.readOnly;
                }
                this.SetValueControl();
                this.AdjustSize();
            }
            this.initializingControls = false;
        }

        private void TxtProprieta_TextChanged(object sender, EventArgs e)
        {
            if (!this.readOnly && !this.initializingControls) this.lblProprieta.ForeColor = Color.DarkOrange;
            if (!this.readOnly)
                this.proprieta.Valore = this.txtProprieta.Text;
        }

        private void CmbProprieta_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!this.initializingControls) this.lblProprieta.ForeColor = Color.DarkOrange;
            if (!this.readOnly)
                this.proprieta.Valore = (this.cmbProprieta.SelectedItem == null ? "" : this.cmbProprieta.Text);
        }

        private void ChkProprieta_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.initializingControls) this.lblProprieta.ForeColor = Color.DarkOrange;
            if (!this.readOnly)
                this.proprieta.Valore = (this.chkProprieta.Checked ? "ABILITATO" : "DISABILITATO");
        }

        public int LabelWidth
        {
            get
            {
                return this.lblProprieta.Width;
            }
            set
            {
                this.lblProprieta.Width = value;
            }
        }

        public void ChangeControlValue()
        {
            this.SetValueControl();
        }

        private void SetValueControl()
        {
            if (!this.readOnly)
            {
                if (this.proprieta.RegularExpression != null && !string.IsNullOrEmpty(this.proprieta.RegularExpression))
                {
                    if (this.proprieta.RegularExpression == @"^(?i)(ABILITATO|DISABILITATO)$")
                        this.chkProprieta.Checked = (this.proprieta.Valore == "ABILITATO");
                    else if (this.proprieta.RegularExpression.StartsWith(@"^(?i)(") && this.proprieta.RegularExpression.EndsWith(@")$") && this.proprieta.RegularExpression.Contains(@"|"))
                    {
                        foreach (clsItemProprietaCombo item in this.cmbProprieta.Items)
                        {
                            if (item.Value == this.proprieta.Valore)
                                this.cmbProprieta.SelectedItem = item;
                        }
                    }
                    else if (this.proprieta.RegularExpression == @"^[0-9]+$")
                        this.txtProprieta.Text = this.proprieta.Valore;
                    else
                        this.txtProprieta.Text = this.proprieta.Valore;
                }
                else
                    this.txtProprieta.Text = this.proprieta.Valore;
            }

        }

        private void TxtProprieta_TextChanged_REGEX(object sender, EventArgs e)
        {
            this.lblProprieta.ForeColor = (clsUtilityConfig.IsMatch(this.txtProprieta.Text, this.RegularExpression) ? Color.Black : Color.Red);
            this.lblError.Visible = !clsUtilityConfig.IsMatch(this.txtProprieta.Text, this.RegularExpression);
        }
    }
}
