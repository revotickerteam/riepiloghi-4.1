﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigRiepiloghi
{
    public partial class frmLogin : Form
    {
        private string login { get; set; }
        private string password { get; set; }
        public frmLogin()
        {
            InitializeComponent();
            this.login = "";
            this.password = "";
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.login = "";
            this.password = "";
            this.DialogResult = DialogResult.Abort;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.login = this.txtLogin.Text;
            this.password = this.txtPassword.Text;
            this.DialogResult = DialogResult.OK;
        }

        public string Login => this.login;
        public string Password => this.password;
    }
}
