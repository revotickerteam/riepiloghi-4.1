﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;
using Wintic.Data.oracle;

namespace ConfigRiepiloghi
{
    public partial class frmConfigRiepiloghi : Form
    {
        private Wintic.Data.oracle.CConnectionOracle conn { get; set; }
        private List<Riepiloghi.clsProprietaRiepiloghi> ProprietaRiepiloghi { get; set; }
        private List<usrProprietaRiepiloghi> listaUserControls = new List<usrProprietaRiepiloghi>();

        private Exception Error { get; set; }
        private TextBox txtConnectionStringDirect = new TextBox();
        RadioButton radioConnTNS = new RadioButton();
        TextBox txtConnectionStringTns = new TextBox();
        RadioButton radioConnSID = new RadioButton();
        

        public frmConfigRiepiloghi()
        {
            InitializeComponent();
            this.Shown += ConfigRiepiloghi_Shown;
            this.FormClosing += ConfigRiepiloghi_FormClosing;
        }

       

        #region Connessione

        private bool OpenConnection()
        {
            bool result = false;
            try
            {
                this.conn = new CConnectionOracle();
                //if (this.chkSistema.Checked)
                //    this.conn.ConnectionString = string.Format("USER=riepiloghi;PASSWORD=riepiloghi;DATA SOURCE={0}", this.txtConn.Text);
                //else
                    this.conn.ConnectionString = string.Format("USER=wtic;PASSWORD=obelix;DATA SOURCE={0}", this.txtConn.Text);
                this.conn.Open();
                result = this.conn.State == ConnectionState.Open;
                if (result)
                {
                    this.txtConn.Enabled = false;
                    this.btnConn.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex, "Connessione");
            }
            return result;
        }

        private void CloseConnection()
        {
            if (this.conn != null)
            {
                this.conn.Close();
                this.conn.Dispose();
                this.conn = null;
            }
        }

        #endregion

        #region Lettura

        
        public bool ReadData(bool hideReadOnly = true)
        {
            listaUserControls = new List<usrProprietaRiepiloghi>();
            this.UseWaitCursor = true;
            Application.DoEvents();
            frmWait frmWait = new frmWait(false);
            bool result = false;
            this.btnDataMigrazione.Enabled = false;
            try
            {

                Exception oError = null;
                InitDataMigrazione(false);
                string emailCodiceSistema = Riepiloghi.clsRiepiloghi.GetEmailCodiceSistema(this.conn, out oError);

                try
                {
                    //SCS_GIORNI_LAVORATIVI_ERRORE
                    StringBuilder oSB_SCS = new StringBuilder();
                    oSB_SCS.Append("INSERT INTO PROPRIETA_RIEPILOGHI (PROPRIETA,VALORE,DESCR,REGULAR_EXPRESSION,WEB_REGULAR_EXPRESSION,ORDINE)");
                    oSB_SCS.Append(" SELECT 'SCS_GIORNI_LAVORATIVI_ERRORE' AS PROPRIETA, '90' AS VALORE, 'Giorni Check Riepiloghi Scambio sale' AS DESCR, '^[0-9]+$' AS REGULAR_EXPRESSION, '[0-9]+' AS WEB_REGULAR_EXPRESSION, 1000 AS ORDINE FROM DUAL");
                    oSB_SCS.Append(" WHERE NOT EXISTS (SELECT NULL FROM WTIC.PROPRIETA_RIEPILOGHI WHERE PROPRIETA = 'SCS_GIORNI_LAVORATIVI_ERRORE')");
                    this.conn.ExecuteNonQuery(oSB_SCS.ToString());
                }
                catch (Exception)
                {
                }

                List<string> Capitoli = new List<string>();
                Dictionary<string, Dictionary<string, bool>> Paragrafi = new Dictionary<string, Dictionary<string, bool>>();

                Capitoli.Add("parServer");
                Capitoli.Add("parSaveFolders");
                Capitoli.Add("parRemoteFolders");
                Capitoli.Add("parLta");
                Capitoli.Add("parSendEmail");
                Capitoli.Add("parReceiveEmail");
                Capitoli.Add("parPrintDetails");
                Capitoli.Add("parRiepiloghiAuto");
                Capitoli.Add("parOthers");


                List<string> proprietaButtonFolders = new List<string>() { "PATH_RPG", "PATH_RPM", "PATH_EMAIL", "PATH_RCA" };
                List<string> proprietaButtonRemoteFolders = new List<string>() { "FILES_RIEPILOGHI_WRITE_MAP" };
                List<string> proprietaButtonCaptureMachineName = new List<string>() { "WINSERVICE_MACHINE_NAME" };
                List<string> proprietaButtonImportRiepiloghiIni = new List<string>() { "EMAIL_SMTP" };
                List<string> proprietaButtonRiepiloghiAuto = new List<string>() { "RIEPILOGHI_AUTO_FASCIA_CREATE" };

                string descCapitolo = "";
                foreach (string capitolo in Capitoli)
                {
                    switch (capitolo)
                    {
                        case "parServer":
                            {
                                descCapitolo = "Server";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("WINSERVICE_MACHINE_NAME", true);
                                Paragrafi[descCapitolo].Add("CHECK_SERVER_FISCALE", true);
                                Paragrafi[descCapitolo].Add("CARTA_LOCALE", false);
                                Paragrafi[descCapitolo].Add("SLOT_CARTA", false);
                                break;
                            }
                        case "parSaveFolders":
                            {
                                descCapitolo = "Cartelle di salvataggio riepiloghi";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("PATH_RPG", false);
                                Paragrafi[descCapitolo].Add("PATH_RPM", false);
                                Paragrafi[descCapitolo].Add("PATH_EMAIL", false);
                                Paragrafi[descCapitolo].Add("PATH_RCA", false);
                                break;
                            }
                        case "parLta":
                            {
                                descCapitolo = "LTA Lista titoli di accesso";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("RCA_DA_GENERARE", true);
                                break;
                            }
                        case "parRemoteFolders":
                            {
                                descCapitolo = "Parametri se salvataggio files riepiloghi remoto rispetto al server";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("FILES_RIEPILOGHI_WRITE_MAP", false);
                                Paragrafi[descCapitolo].Add("FILES_RIEPILOGHI_WRITE_PASSWORD", false);
                                Paragrafi[descCapitolo].Add("FILES_RIEPILOGHI_WRITE_USER", false);
                                break;
                            }
                        case "parSendEmail":
                            {
                                descCapitolo = string.Format("Parametri SMTP Spedizione email (l'indirizzo email utilizzato è {0})", emailCodiceSistema);
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("EMAIL_SMTP", true);
                                Paragrafi[descCapitolo].Add("EMAIL_PORT_OUT", true);
                                Paragrafi[descCapitolo].Add("EMAIL_USE_USER", true);
                                Paragrafi[descCapitolo].Add("EMAIL_USE_PASSWORD", true);
                                Paragrafi[descCapitolo].Add("EMAIL_USE_DEFAULT_CREDENTIALS", true);
                                Paragrafi[descCapitolo].Add("EMAIL_USE_SSL", true);
                                break;
                            }
                        case "parReceiveEmail":
                            {
                                descCapitolo = string.Format("Parametri Ricezione email (l'indirizzo email utilizzato è {0})", emailCodiceSistema);
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("EMAIL_IN", true);
                                Paragrafi[descCapitolo].Add("EMAIL_PORT_IN", true);
                                Paragrafi[descCapitolo].Add("EMAIL_IN_PASSWORD", true);
                                Paragrafi[descCapitolo].Add("EMAIL_DELETE_FROM_SERVER", false);
                                break;
                            }
                        case "parPrintDetails":
                            {
                                descCapitolo = "Dettagli stampa C1/C2 Giornaliero/Mensile";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("C1_PRINT_NOME_FILE_RPG", true);
                                Paragrafi[descCapitolo].Add("C1_PRINT_NOME_FILE_RPM", true);
                                Paragrafi[descCapitolo].Add("C1_PRINT_RPG_ALTRI_PROVENTI", true);
                                Paragrafi[descCapitolo].Add("C1_PRINT_RPG_MAX_LOGID", false);
                                Paragrafi[descCapitolo].Add("C1_PRINT_RPM_MAX_LOGID", false);
                                Paragrafi[descCapitolo].Add("DESC_SALA_RAG_SOC_DATIFISSI", true);
                                break;
                            }
                        case "parRiepiloghiAuto":
                            {
                                descCapitolo = "Parametri per Riepiloghi automatici";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());
                                Paragrafi[descCapitolo].Add("RIEPILOGHI_AUTO_FASCIA_CREATE", true);
                                Paragrafi[descCapitolo].Add("RIEPILOGHI_AUTO_FASCIA_EMAIL", true);
                                Paragrafi[descCapitolo].Add("RIEPILOGHI_AUTO_NUM_GG_PREC_GEN", true);
                                Paragrafi[descCapitolo].Add("RIEPILOGHI_AUTO_USER_PASSWORD", true);
                                break;
                            }
                        case "parOthers":
                            {
                                descCapitolo = "Altro";
                                Paragrafi.Add(descCapitolo, new Dictionary<string, bool>());

                                Paragrafi[descCapitolo].Add("ANNULLO_RATEI", false);
                                Paragrafi[descCapitolo].Add("RPG_FUTURO", false);
                                Paragrafi[descCapitolo].Add("RPG_SEND_FUTURO", false);
                                Paragrafi[descCapitolo].Add("RCA_FUTURO", false);
                                Paragrafi[descCapitolo].Add("RCA_SEND_FUTURO", false);
                                Paragrafi[descCapitolo].Add("CHECK_XML_DTD", false);

                                Paragrafi[descCapitolo].Add("WINSERVICE_APPLICATION_TYPE", false);
                                Paragrafi[descCapitolo].Add("WINSERVICE_COMUNICATION_MODE", false);
                                Paragrafi[descCapitolo].Add("WINSERVICE_LOG", false);
                                Paragrafi[descCapitolo].Add("WINSERVICE_POLLING_TIME", false);
                                Paragrafi[descCapitolo].Add("WINSERVICE_SOCKET_LISTENER_IP", false);
                                Paragrafi[descCapitolo].Add("WINSERVICE_SOCKET_LISTENER_PORT", false);
                                Paragrafi[descCapitolo].Add("RIEPILOGHI_CALC_IMPOSTA_ABB_RATEI", false);
                                Paragrafi[descCapitolo].Add("SAVE_RIEPILOGHI_FILES", false);
                                Paragrafi[descCapitolo].Add("GG_BACK_EMISSIONI_ANNULLATI", true);
                                Paragrafi[descCapitolo].Add("SCS_GIORNI_LAVORATIVI_ERRORE", true);
                                break;
                            }
                    }
                }

                this.ProprietaRiepiloghi = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(this.conn);

                Dictionary<string, string> regularExpressions = new Dictionary<string, string>();

                regularExpressions.Add("RIEPILOGHI_AUTO_FASCIA_CREATE", @"DALLEORE=\d{2}.\d{2};ALLEORE=\d{2}.\d{2}");
                regularExpressions.Add("RIEPILOGHI_AUTO_FASCIA_EMAIL", @"DALLEORE=\d{2}.\d{2};ALLEORE=\d{2}.\d{2}");
                regularExpressions.Add("RIEPILOGHI_AUTO_NUM_GG_PREC_GEN", @"\d+");
                regularExpressions.Add("RIEPILOGHI_AUTO_USER_PASSWORD", @"[a-zA-z0-9]+\/[a-zA-z0-9]+");

                foreach (KeyValuePair<string, Dictionary<string, bool>> capitolo in Paragrafi)
                {
                    Panel pnlCapitolo = new Panel();
                    pnlCapitolo.Dock = DockStyle.Top;
                    pnlCapitolo.AutoSize = true;
                    pnlCapitolo.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    this.pnlData.Controls.Add(pnlCapitolo);
                    pnlCapitolo.BringToFront();

                    clsLabel lblCapitolo = new clsLabel();
                    lblCapitolo.Text = capitolo.Key;
                    lblCapitolo.Dock = DockStyle.Top;
                    lblCapitolo.BorderStyle = BorderStyle.FixedSingle;
                    lblCapitolo.TextAlign = ContentAlignment.MiddleCenter;
                    pnlCapitolo.Controls.Add(lblCapitolo);
                    lblCapitolo.BringToFront();
                    lblCapitolo.Font = new Font(lblCapitolo.Font.FontFamily, lblCapitolo.Font.Size + 1, FontStyle.Bold);

                    List<usrProprietaRiepiloghi> paragrafiReadOnly = new List<usrProprietaRiepiloghi>();
                    foreach (KeyValuePair<string, bool> paragrafo in capitolo.Value)
                    {
                        foreach (Riepiloghi.clsProprietaRiepiloghi item in this.ProprietaRiepiloghi)
                        {
                            if (item.Proprieta == paragrafo.Key)
                            {
                                
                                bool lPasswordChar = item.Proprieta.Contains("PASSWORD");
                                usrProprietaRiepiloghi usrItem = new usrProprietaRiepiloghi();
                                listaUserControls.Add(usrItem);
                                usrItem.Proprieta = item;
                                usrItem.Dock = DockStyle.Top;
                                pnlCapitolo.Controls.Add(usrItem);
                                usrItem.BringToFront();
                                usrItem.Enabled = paragrafo.Value;
                                if (!usrItem.Enabled)
                                    paragrafiReadOnly.Add(usrItem);

                                if (proprietaButtonFolders.Contains(item.Proprieta))
                                    usrItem.SetDelegateButton(new usrProprietaRiepiloghi.DelegateButton(delegateDirectoryFolder));
                                else if (proprietaButtonRemoteFolders.Contains(item.Proprieta))
                                    usrItem.SetDelegateButton(new usrProprietaRiepiloghi.DelegateButton(delegateDirectoryFolder));
                                else if (proprietaButtonCaptureMachineName.Contains(item.Proprieta))
                                    usrItem.SetDelegateButton(new usrProprietaRiepiloghi.DelegateButton(delegateMachineName));
                                else if (proprietaButtonImportRiepiloghiIni.Contains(item.Proprieta))
                                    usrItem.SetDelegateButton(new usrProprietaRiepiloghi.DelegateButton(delegateImportRiepiloghiIni));
                                else if (proprietaButtonRiepiloghiAuto.Contains(item.Proprieta))
                                    usrItem.SetDelegateButton(new usrProprietaRiepiloghi.DelegateButton(delegateTaskRiepiloghiAuto));

                                if (lPasswordChar)
                                    usrItem.SetPasswordChar('*');

                                if (regularExpressions.ContainsKey(item.Proprieta))
                                    usrItem.SetReguralExpression(regularExpressions[item.Proprieta]);
                                break;
                            }
                        }
                    }

                    if (hideReadOnly && paragrafiReadOnly.Count == capitolo.Value.Count)
                    {
                        //lblCapitolo.Visible = false;
                        //foreach (usrProprietaRiepiloghi usrProprietaRiepiloghi in paragrafiReadOnly)
                        //{
                        //    usrProprietaRiepiloghi usrItem = new usrProprietaRiepiloghi();
                        //    usrItem.Proprieta = new Riepiloghi.clsProprietaRiepiloghi();
                        //    usrItem.Dock = DockStyle.Top;
                        //    pnlCapitolo.Controls.Add(usrItem);
                        //    usrItem.BringToFront();
                        //    usrItem.Enabled = true;
                        //    if (!usrItem.Enabled)
                        //        paragrafiReadOnly.Add(usrItem);
                        //}
                    }

                }

                #region connessioni interne esterne
                

                StringBuilder oSB = new StringBuilder();
                oSB.Append("SELECT RP_CODICI_SISTEMA.CODICE_SISTEMA, RP_CODICI_SISTEMA.CONNECTION_STRING ");
                oSB.Append(" FROM ");
                oSB.Append(" RIEPILOGHI.RP_CODICI_SISTEMA");
                oSB.Append(" INNER JOIN WTIC.SMART_CS ON SMART_CS.CODICE_SISTEMA = RP_CODICI_SISTEMA.CODICE_SISTEMA AND SMART_CS.ENABLED = 1");
                oSB.Append(" INNER JOIN (SELECT NVL(MAX(VALUE),'DISABILITATO') AS RP_LOGIN_MODE FROM RIEPILOGHI.RP_PROPERTIES WHERE PROPERTY = 'CODICE_SISTEMA_LOGIN_OPERATORI_LOCALI') SISTEMA_UNICO ON SISTEMA_UNICO.RP_LOGIN_MODE = 'ABILITATO'");
                oSB.Append(" WHERE RP_CODICI_SISTEMA.ENABLED = 1");

                IRecordSet ors = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                if (!ors.EOF)
                {
                    Panel pnlCapitolo = new Panel();
                    pnlCapitolo.Padding = new Padding(3);
                    pnlCapitolo.BorderStyle = BorderStyle.Fixed3D;
                    pnlCapitolo.BackColor = Color.LightBlue;
                    pnlCapitolo.Dock = DockStyle.Top;
                    pnlCapitolo.AutoSize = true;
                    pnlCapitolo.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    Regex regStringConn = new Regex(@"\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=127.0.0.1\)\(PORT=\d{4}\)\)\)\(CONNECT_DATA=\(SERVER=DEDICATED\)\(SERVICE_NAME=[A-Za-z0-9.]+\)\)\)");
                    string connectionStringExternal = ors.Fields("CONNECTION_STRING").Value.ToString().ToUpper();
                    string connectionStringBase = string.Format(@"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT={0})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={1})))", "1521", "FISCALE.WINTIC.IT");
                    bool regStringConnMatch = regStringConn.IsMatch(connectionStringExternal);
                    this.pnlData.Controls.Add(pnlCapitolo);
                    pnlCapitolo.BringToFront();

                    Panel pnlMethodConn;
                    clsLabel labelConn;

                    Label labelConforme = new Label();
                    labelConforme.AutoSize = false;
                    
                    txtConnectionStringDirect.AutoSize = false;
                    txtConnectionStringDirect.ReadOnly = true;
                    txtConnectionStringDirect.Enabled = true;
                    txtConnectionStringDirect.Multiline = true;
                    txtConnectionStringDirect.WordWrap = true;
                    txtConnectionStringDirect.MinimumSize = new Size(100, 50);
                    TextBox txtBoxConnPORT = new TextBox();
                    TextBox txtBoxConnSID = new TextBox();

                    pnlMethodConn = new Panel();
                    pnlMethodConn.Dock = DockStyle.Top;
                    pnlMethodConn.AutoSize = true;
                    pnlMethodConn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    pnlCapitolo.Controls.Add(pnlMethodConn);
                    pnlMethodConn.BringToFront();

                    radioConnTNS.Text = "Connessione tramite TNSNAMES.ORA";
                    radioConnTNS.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(radioConnTNS);
                    radioConnTNS.BringToFront();

                    radioConnSID.Text = "Connessione tramite stringa di connessione (più veloce)";
                    radioConnSID.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(radioConnSID);
                    radioConnSID.BringToFront();

                    labelConn = new clsLabel();
                    labelConn.Dock = DockStyle.Top;
                    labelConn.AutoSize = false;
                    labelConn.Font = new Font(this.Font.FontFamily, this.Font.Size + 1, FontStyle.Bold);
                    labelConn.Text = "Tramite TNSNAMES.ORA:";
                    pnlMethodConn.Controls.Add(labelConn);
                    labelConn.BringToFront();

                    txtConnectionStringTns.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(txtConnectionStringTns);
                    txtConnectionStringTns.BringToFront();
                    txtConnectionStringTns.Text = (regStringConnMatch ? "SERVERX" : connectionStringExternal);

                    pnlMethodConn = new Panel();
                    pnlMethodConn.Dock = DockStyle.Top;
                    pnlMethodConn.AutoSize = true;
                    pnlMethodConn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    pnlCapitolo.Controls.Add(pnlMethodConn);
                    pnlMethodConn.BringToFront();



                    //txtBoxConn.Text = @"(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 127.0.0.1)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = FISCALE.WINTIC.IT)))";
                    labelConn = new clsLabel();
                    labelConn.Dock = DockStyle.Top;
                    labelConn.AutoSize = false;
                    labelConn.Font = new Font(this.Font.FontFamily, this.Font.Size + 1, FontStyle.Bold);
                    labelConn.Text = "Tramite stringa di connessione:";
                    pnlMethodConn.Controls.Add(labelConn);
                    labelConn.BringToFront();

                    labelConn = new clsLabel();
                    labelConn.Dock = DockStyle.Top;
                    labelConn.AutoSize = false;
                    labelConn.Text = "Porta:";
                    pnlMethodConn.Controls.Add(labelConn);
                    labelConn.BringToFront();

                    txtBoxConnPORT.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(txtBoxConnPORT);
                    txtBoxConnPORT.BringToFront();

                    labelConn = new clsLabel();
                    labelConn.Dock = DockStyle.Top;
                    labelConn.AutoSize = false;
                    labelConn.Text = "Service name:";
                    pnlMethodConn.Controls.Add(labelConn);
                    labelConn.BringToFront();

                    txtBoxConnSID.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(txtBoxConnSID);
                    txtBoxConnSID.BringToFront();

                    txtConnectionStringDirect.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(txtConnectionStringDirect);
                    txtConnectionStringDirect.BringToFront();
                    txtConnectionStringDirect.Text = (regStringConnMatch ? connectionStringExternal : connectionStringBase);

                    labelConforme.Dock = DockStyle.Top;
                    pnlMethodConn.Controls.Add(labelConforme);
                    labelConforme.BringToFront();

                    if (regStringConnMatch)
                    {
                        txtBoxConnPORT.Text = connectionStringExternal.Replace("(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=", "").Substring(0, 4);
                        txtBoxConnSID.Text = connectionStringExternal.Replace(string.Format(@"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT={0})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=", txtBoxConnPORT.Text), "").Replace(")))","");
                    }

                    radioConnTNS.CheckedChanged += (o, e) =>
                    {
                        if (radioConnTNS.Enabled)
                        {
                            string port = txtBoxConnPORT.Text;
                            string sid = txtBoxConnSID.Text;
                            bool isConforme = false;
                            string conforme = "";
                            string connectionString = "";
                            RicalcConnectionString(radioConnSID.Checked, port, sid, out isConforme, out conforme, out connectionString);
                            labelConforme.Text = conforme;
                            txtConnectionStringDirect.Text = connectionString;
                            radioConnSID.Enabled = false;
                            radioConnSID.Checked = !radioConnTNS.Checked;
                            radioConnSID.Enabled = true;
                            labelConforme.BackColor = (radioConnSID.Checked ? (isConforme ? Color.Green : Color.Red) : Color.Transparent);
                            txtBoxConnPORT.Enabled = radioConnSID.Checked;
                            txtBoxConnSID.Enabled = radioConnSID.Checked;
                            txtConnectionStringTns.Enabled = radioConnTNS.Checked;
                        }
                    };
                    radioConnSID.CheckedChanged += (o, e) =>
                    {
                        if (radioConnSID.Enabled)
                        {
                            string port = txtBoxConnPORT.Text;
                            string sid = txtBoxConnSID.Text;
                            bool isConforme = false;
                            string conforme = "";
                            string connectionString = "";
                            RicalcConnectionString(radioConnSID.Checked, port, sid, out isConforme, out conforme, out connectionString);
                            labelConforme.Text = conforme;
                            txtConnectionStringDirect.Text = connectionString;
                            radioConnTNS.Enabled = false;
                            radioConnTNS.Checked = !radioConnSID.Checked;
                            radioConnTNS.Enabled = true;
                            labelConforme.BackColor = (radioConnSID.Checked ? (isConforme ? Color.Green : Color.Red) : Color.Transparent);
                            txtBoxConnPORT.Enabled = radioConnSID.Checked;
                            txtBoxConnSID.Enabled = radioConnSID.Checked;
                            txtConnectionStringTns.Enabled = radioConnTNS.Checked;
                        }
                    };

                    txtBoxConnPORT.TextChanged += (o, e) =>
                    {
                        string port = txtBoxConnPORT.Text;
                        string sid = txtBoxConnSID.Text;
                        bool isConforme = false;
                        string conforme = "";
                        string connectionString = "";
                        RicalcConnectionString(radioConnSID.Checked, port, sid, out isConforme, out conforme, out connectionString);
                        labelConforme.Text = conforme;
                        txtConnectionStringDirect.Text = connectionString;
                        labelConforme.BackColor = (radioConnSID.Checked ? (isConforme ? Color.Green : Color.Red) : Color.Transparent);
                    };

                    txtBoxConnSID.TextChanged += (o, e) =>
                    {
                        string port = txtBoxConnPORT.Text;
                        string sid = txtBoxConnSID.Text;
                        bool isConforme = false;
                        string conforme = "";
                        string connectionString = "";
                        RicalcConnectionString(radioConnSID.Checked, port, sid, out isConforme, out conforme, out connectionString);
                        labelConforme.Text = conforme;
                        txtConnectionStringDirect.Text = connectionString;
                        labelConforme.BackColor = (radioConnSID.Checked ? (isConforme ? Color.Green : Color.Red) : Color.Transparent);
                    };

                    if (!regStringConnMatch)
                    {
                        radioConnTNS.Checked = true;
                    }
                    else
                    {
                        radioConnSID.Checked = true;
                    }
                }
                ors.Close();

                #endregion

                this.pnlBottom.Visible = true;
                this.pnlData.Visible = true;
                this.pnlData.BringToFront();
                this.Refresh();

                int nMaxWidth = 0;
                foreach (Panel pnlCapitolo in this.pnlData.Controls)
                {
                    foreach (Control ctl in pnlCapitolo.Controls)
                    {
                        try
                        {
                            usrProprietaRiepiloghi item = (usrProprietaRiepiloghi)ctl;
                            if (nMaxWidth < item.LabelWidth)
                                nMaxWidth = item.LabelWidth;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }

                foreach (Panel pnlCapitolo in this.pnlData.Controls)
                {
                    foreach (Control ctl in pnlCapitolo.Controls)
                    {
                        try
                        {
                            usrProprietaRiepiloghi item = (usrProprietaRiepiloghi)ctl;
                            item.LabelWidth = nMaxWidth;
                            item.AdjustSize();
                        }
                        catch (Exception)
                        {
                        }
                    }
                }


                this.btnChiudi.Click += BtnChiudi_Click;
                this.btnSave.Click += BtnSave_Click;

            }
            catch (Exception error)
            {
                this.ShowError(error, "Caricamento Dati");
            }
            this.btnDataMigrazione.Enabled = true;
            
            this.UseWaitCursor = false;
            Application.DoEvents();
            frmWait.Close();
            frmWait.Dispose();

            return result;
        }

        private void RicalcConnectionString(bool conSID, string port, string sid, out bool isConforme, out string conforme, out string connectionString)
        {
            conforme = "";
            connectionString = "";
            isConforme = false;
            if (conSID)
            {
                Regex regStringConn = new Regex(@"\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=127.0.0.1\)\(PORT=\d{4}\)\)\)\(CONNECT_DATA=\(SERVER=DEDICATED\)\(SERVICE_NAME=[A-Za-z0-9.]+\)\)\)");
                connectionString = string.Format(@"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT={0})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={1})))", port, sid);
                isConforme = regStringConn.IsMatch(connectionString);
                conforme = (isConforme ? "Conforme" : "Non conforme");
            }
            else
            {
                conforme = "";
                connectionString = string.Format(@"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT={0})))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME={1})))", "1521", "FISCALE.WINTIC.IT");
            }
        }

        public string delegateDirectoryFolder(int mode, usrProprietaRiepiloghi usrItem)
        {
            string title = "Cartella di destinazione";
            string result = "";
            if (mode == 0)
            {
                result = title;
            }
            else
            {
                System.Windows.Forms.FolderBrowserDialog foldD = new FolderBrowserDialog();
                foldD.Description = title;
                foldD.ShowNewFolderButton = true;
                if (foldD.ShowDialog() == DialogResult.OK)
                {
                    result = foldD.SelectedPath;
                }
            }
            return result;
        }

        public string delegateImportRiepiloghiIni(int mode, usrProprietaRiepiloghi usrItem)
        {
            string file = "";
            string title = "Importa da riepiloghi.ini";
            string result = "";
            if (mode == 0)
            {
                result = title;
            }
            else
            {
                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Title = title;
                fileDialog.Filter = "ini files (*.ini)|*.ini";
                fileDialog.CheckFileExists = true;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    file = fileDialog.FileName;
                }

                Dictionary<string, string> chiavi = new Dictionary<string, string>();
                chiavi.Add("EMAIL_SMTP", "<MAIL_SMPT_SERVER>:");
                chiavi.Add("EMAIL_PORT_OUT", "<PORTA_SERVER_SMTP>:");
                chiavi.Add("EMAIL_USE_USER", "<AUTENTICAZIONE_MAIL_LOGIN>:");
                chiavi.Add("EMAIL_USE_PASSWORD", "<AUTENTICAZIONE_MAIL_PASSWORD>:");
                chiavi.Add("EMAIL_USE_SSL", "<AUTENTICAZIONE_MAIL>:");


                fileDialog.Dispose();
                Panel pnlItems = (Panel)usrItem.Parent;
                if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(file))
                {
                    foreach (string line in System.IO.File.ReadAllLines(file))
                    {
                        string par = "";
                        string value = "";
                        foreach (KeyValuePair<string, string> itemKey in chiavi)
                        {
                            if (line.StartsWith(itemKey.Value))
                            {
                                par = itemKey.Value.Trim();
                                value = line.Substring(par.Length).Trim();

                                foreach (Control ctl in pnlItems.Controls)
                                {
                                    usrProprietaRiepiloghi usrItemInPanel = null;
                                    try
                                    {
                                        usrItemInPanel = (usrProprietaRiepiloghi)ctl;
                                    }
                                    catch (Exception)
                                    {
                                        usrItemInPanel = null;
                                    }

                                    if (usrItemInPanel != null && usrItemInPanel.Proprieta.Proprieta == itemKey.Key)
                                    {
                                        if (line.StartsWith("<AUTENTICAZIONE_MAIL>"))
                                        {
                                            usrItemInPanel.Proprieta.Valore = value.ToUpper();
                                            usrItemInPanel.ChangeControlValue();
                                        }
                                        else
                                        {
                                            usrItemInPanel.Proprieta.Valore = value;
                                            usrItemInPanel.ChangeControlValue();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                            

                        

                    }
                }
            }
            return result;

        }

        public string delegateMachineName(int mode, usrProprietaRiepiloghi usrItem)
        {
            string title = "Nome server";
            string result = "";
            if (mode == 0)
            {
                result = title;
            }
            else
            {
                string value  = System.Environment.MachineName;
                if (MessageBox.Show(string.Format("Impostare il nome del server che genera i riepiloghi con il valore:\r\n{0}", value), "Nome server", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    result = value;
            }
            return result;
        }

        public string delegateTaskRiepiloghiAuto(int mode, usrProprietaRiepiloghi usrItem)
        {
            string title = "Attività pianificata";
            string result = "";
            if (mode == 0)
            {
                result = title;
            }
            else
            {
                Dictionary<string, string> items = new Dictionary<string, string>();
                items.Add("RIEPILOGHI_AUTO_FASCIA_CREATE", "");
                items.Add("RIEPILOGHI_AUTO_FASCIA_EMAIL", "");
                items.Add("RIEPILOGHI_AUTO_NUM_GG_PREC_GEN", "");
                items.Add("RIEPILOGHI_AUTO_USER_PASSWORD", "");


                Panel pnlItems = (Panel)usrItem.Parent;
                foreach (Control ctl in pnlItems.Controls)
                {
                    usrProprietaRiepiloghi usrItemInPanel = null;
                    try
                    {
                        usrItemInPanel = (usrProprietaRiepiloghi)ctl;
                    }
                    catch (Exception)
                    {
                        usrItemInPanel = null;
                    }

                    if (usrItemInPanel != null && items.ContainsKey(usrItemInPanel.Proprieta.Proprieta))
                    {
                        items[usrItemInPanel.Proprieta.Proprieta] = usrItemInPanel.Proprieta.Valore;
                    }
                }


                result = "";
                string path = "";
                FolderBrowserDialog folderD = new FolderBrowserDialog();
                folderD.Description = "Selezionare la cartella dei riepiloghi automatici";
                folderD.ShowNewFolderButton = false;
                if (folderD.ShowDialog() == DialogResult.OK)
                    path = folderD.SelectedPath;
                folderD.Dispose();

                //MessageBox.Show("Sistema Operativo Windows 7 ?", "Tipo di sistema operativo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes;

                bool lContinue = !string.IsNullOrEmpty(path) && System.IO.Directory.Exists(path);
                bool lWindows7 = false;

                if (lContinue)
                {
                    lWindows7 = MessageBox.Show("Sistema Operativo Windows 7 ?", "Tipo di sistema operativo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes; 
                }

                if (lContinue)
                {

                    string file01 = path + @"\0100_riepiloghiAutoCreate.ps1";
                    string file02 = path + @"\0200_riepiloghiAutoEmail.ps1";
                    lContinue = System.IO.File.Exists(path + @"\RiepiloghiAutoJDK4.1.exe");

                    if (lWindows7)
                    {
                        file01 = path + @"\0100_riepiloghiAutoCreate.xml";
                        file02 = path + @"\0200_riepiloghiAutoEmail.xml";
                    }

                    if (!lContinue)
                    {
                        MessageBox.Show("Cartella non valida. cercare la cartella con l'applicazione RiepiloghiAutoJDK4.1.exe", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        lContinue = false;
                        string pc_user = "";
                        string pc_password = "";
                        List<clsItemGenericData> user_password = new List<clsItemGenericData>();

                        user_password.Add(new clsItemGenericData("_TITLE2_", "Eventualmente creare un nuovo utente/password tra gli utenti del server", clsItemGenericData.EnumGenericDataType.TypeOnlyLabel, "", true, true));
                        user_password.Add(new clsItemGenericData("_TITLE3_", "con diritti di amministratore per questa attività", clsItemGenericData.EnumGenericDataType.TypeOnlyLabel, "", true, true));
                        user_password.Add(new clsItemGenericData("USR", "Nome utente server", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                        user_password.Add(new clsItemGenericData("PSW", "Password server", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                        frmGetGenericData fGen = new frmGetGenericData("Indicare il nome utente e password del server per eseguire l'attività pianificata.", user_password);
                        if (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            user_password = fGen.Data;
                            foreach (clsItemGenericData item_user_password in user_password)
                            {
                                if (item_user_password.Code == "USR")
                                    pc_user = item_user_password.Value.ToString();
                                else if (item_user_password.Code == "PSW")
                                    pc_password = item_user_password.Value.ToString();
                            }
                            lContinue = true;
                        }
                        fGen.Dispose();

                        if (lContinue)
                        {
                            System.IO.FileInfo appInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                            List<string> linee = new List<string>(System.IO.File.ReadAllLines(appInfo.Directory.FullName + @"\ModelloScheduledTask.txt"));
                            if (lWindows7)
                            {
                                linee = new List<string>(System.IO.File.ReadAllLines(appInfo.Directory.FullName + @"\ModelloScheduledTaskWin7.xml"));
                            }
                            Dictionary<string, Dictionary<string, string>> parametriFiles = new Dictionary<string, Dictionary<string, string>>();
                            parametriFiles.Add(file01, new Dictionary<string, string>());
                            parametriFiles.Add(file02, new Dictionary<string, string>());

                            int nH = 0;
                            int nM = 0;

                            nH = int.Parse(items["RIEPILOGHI_AUTO_FASCIA_CREATE"].Substring("DALLEORE=".Length, 5).Split('.')[0]);
                            nM = int.Parse(items["RIEPILOGHI_AUTO_FASCIA_CREATE"].Substring("DALLEORE=".Length, 5).Split('.')[1]) + 1;
                            parametriFiles[file01].Add("{NOME_TASK}", "RiepiloghiAutoCreate");
                            parametriFiles[file01].Add("{PATH_TASK}", path);
                            string cOre01 = string.Format("{0}:{1}", nH.ToString("00"), nM.ToString("00"));

                            // NON PER WIN7
                            if (!lWindows7)
                            {
                                if (nH < 13)
                                    cOre01 += "am";
                                else
                                    cOre01 += "pm";
                            }
                            else cOre01 += ":00";

                            // UTILE SOLO PER WIN7 ---------------------------------------------------------------------------------------------------------------------------------------------------------
                            parametriFiles[file01].Add("{DATA_OGGI}", string.Format("{0}-{1}-{2}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00")));
                            parametriFiles[file01].Add("{ORA_ADESSO}", string.Format("{0}:{1}:{2}.{3}", DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), DateTime.Now.Millisecond.ToString()));
                            parametriFiles[file01].Add("{NOME_PC}", Environment.MachineName);
                            // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                            parametriFiles[file01].Add("{ORA_TASK}", cOre01);
                            parametriFiles[file01].Add("{USER_TASK}", pc_user);
                            parametriFiles[file01].Add("{PASSWORD_TASK}", pc_password);

                            nH = int.Parse(items["RIEPILOGHI_AUTO_FASCIA_EMAIL"].Substring("DALLEORE=".Length, 5).Split('.')[0]);
                            nM = int.Parse(items["RIEPILOGHI_AUTO_FASCIA_EMAIL"].Substring("DALLEORE=".Length, 5).Split('.')[1]) + 5;
                            parametriFiles[file02].Add("{NOME_TASK}", "RiepiloghiAutoEmail");
                            parametriFiles[file02].Add("{PATH_TASK}", path);
                            string cOre02 = string.Format("{0}:{1}", nH.ToString("00"), nM.ToString("00"));

                            if (!lWindows7)
                            {
                                if (nH < 13)
                                    cOre02 += "am";
                                else
                                    cOre02 += "pm";
                            }
                            else cOre02 += ":00";

                            // UTILE SOLO PER WIN7 ---------------------------------------------------------------------------------------------------------------------------------------------------------
                            parametriFiles[file02].Add("{DATA_OGGI}", string.Format("{0}-{1}-{2}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00")));
                            parametriFiles[file02].Add("{ORA_ADESSO}", string.Format("{0}:{1}:{2}.{3}", DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), DateTime.Now.Millisecond.ToString()));
                            parametriFiles[file02].Add("{NOME_PC}", Environment.MachineName);
                            // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                            parametriFiles[file02].Add("{ORA_TASK}", cOre02);
                            parametriFiles[file02].Add("{USER_TASK}", pc_user);
                            parametriFiles[file02].Add("{PASSWORD_TASK}", pc_password);

                            int indexPerWin7 = 0;
                            foreach (KeyValuePair<string, Dictionary<string, string>> singlefile in parametriFiles)
                            {
                                List<string> linee_to_save = new List<string>();
                                foreach (string linea in linee)
                                {
                                    string linea_to_save = linea;
                                    foreach (KeyValuePair<string, string> parametro in singlefile.Value)
                                    {
                                        if (linea_to_save.Contains(parametro.Key))
                                            linea_to_save = linea_to_save.Replace(parametro.Key, parametro.Value);
                                    }
                                    linee_to_save.Add(linea_to_save);
                                }
                                System.IO.File.WriteAllLines(singlefile.Key, linee_to_save.ToArray());
                                if (lWindows7)
                                {
                                    indexPerWin7 += 1;
                                    if (indexPerWin7 == 1)
                                        System.IO.File.WriteAllText(file01.Replace(".xml", ".bat"), string.Format(@"schtasks /Create /XML ""{0}"" /TN ""RiepiloghiAutoCreate"" /RU ""{1}\{2}"" /RP ""{3}""", file01, Environment.MachineName, pc_user, pc_password));
                                    else if (indexPerWin7 == 2)
                                        System.IO.File.WriteAllText(file02.Replace(".xml", ".bat"), string.Format(@"schtasks /Create /XML ""{0}"" /TN ""RiepiloghiAutoEmail"" /RU ""{1}\{2}"" /RP ""{3}""", file02, Environment.MachineName, pc_user, pc_password));
                                }
                            }

                            //schtasks /Create /XML "c:\wintic\RiepiloghiAutoCreate.xml" /TN "RIepiloghiAUtoCreate" /RU "srv01\simone" /RP "lamiapassword"

                            string command = "set-executionpolicy remotesigned";
                            System.Windows.Forms.Clipboard.SetText(command);
                            if (!lWindows7)
                            {
                                MessageBox.Show(string.Format(@"Eseguire PowerShell (come amministratore)" +
                                                              "\r\n" + "Nella cartella" +
                                                              "\r\n" + "{0}" +
                                                              "\r\n" + "digitare:" +
                                                              "\r\n" + command +
                                                              "\r\n" + " (il comando precedente è stato inserito negli appunti potete incollarlo)" +
                                                              "\r\n" + "eseguire:" +
                                                              "\r\n" + "0100_riepiloghiAutoCreate.ps1" +
                                                              "\r\n" + " che imposta la creazione dei riepiloghi alle ore: {1}" +
                                                              "\r\n" + "0200_riepiloghiAutoEmail.ps1" +
                                                              "\r\n" + " che imposta la lettura delle email alle ore: {2}" +
                                                              "", path, cOre01, cOre02));
                            }
                            else
                            {

                                MessageBox.Show(string.Format(@"Eseguire PowerShell (come amministratore)" +
                                                              "\r\n" + "Nella cartella" +
                                                              "\r\n" + "{0}" +
                                                              "\r\n" + "digitare:" +
                                                              "\r\n" + command +
                                                              "\r\n" + " (il comando precedente è stato inserito negli appunti potete incollarlo)" +
                                                              "\r\n" + "eseguire:" +
                                                              "\r\n" + "0100_riepiloghiAutoCreate.bat" +
                                                              "\r\n" + " che imposta la creazione dei riepiloghi alle ore: {1}" +
                                                              "\r\n" + "0200_riepiloghiAutoEmail.bat" +
                                                              "\r\n" + " che imposta la lettura delle email alle ore: {2}" +
                                                              "", path, cOre01, cOre02));
                            }
                            System.Diagnostics.Process.Start("explorer.exe", path);
                        }
                    }
                }
            }
            return result;
        }

        #endregion

        #region Data Migrazione

        



        private void BtnDataMigrazione_Click(object sender, EventArgs e)
        {
            if (clsDataMigrazione.ThreadInsert != null)
            {
                string message = "";
                if (clsDataMigrazione.IsInsertStarted)
                {
                    if (clsDataMigrazione.IsInsertEnded)
                        message = "Inserimento log terminato, riavviare l'applicazione per fare un nuovo inserimento";
                    else
                        message = "Inserimento log in corso.";
                }
                else
                {
                    message = "Inserimento log già impostato precedentemente, riavviare l'applicazione per fare un nuovo inserimento";
                }
                MessageBox.Show(message);
            }
            else
            {
                frmDataMigrazione frmMigrazione = new frmDataMigrazione(this.conn, this.txtConn.Text);
                bool dialogResultMigrazione = frmMigrazione.ShowDialog() == DialogResult.OK;
                if (dialogResultMigrazione && clsDataMigrazione.ThreadInsert != null)
                {
                    SetLblMigrazioneValue("Attendere...", false);
                    this.pnlData.Enabled = false;
                    this.pnlConn.Enabled = false;
                    this.pnlData.Visible = false;
                    this.btnChiudi.Text = "Chiudi";
                    this.txtInsertLogs.Visible = true;
                    this.txtInsertLogs.BringToFront();
                    this.btnSave.Enabled = false;
                    this.btnAltro.Enabled = false;
                    if (clsDataMigrazione.IsInsertStarted && !clsDataMigrazione.IsInsertEnded)
                    {
                        clsDataMigrazione.InsertEnded += ClsDataMigrazione_InsertEnded;
                        clsDataMigrazione.SetOutMessageEvent += ClsDataMigrazione_SetOutMessageEvent;
                        this.timerRefreshInsert.Start();
                    }
                    else
                    {
                        this.timerRefreshInsert.Stop();
                    }
                }
                frmMigrazione.Close();
                frmMigrazione.Dispose();
                this.InitDataMigrazione(false);
            }
        }

        private delegate void delegateSetLblMigrazioneValue(string value, bool lastValue);

        private void ClsDataMigrazione_SetOutMessageEvent(string value)
        {
            this.SetLblMigrazioneValue(value, false);
        }

        private void ClsDataMigrazione_InsertEnded()
        {
            this.timerRefreshInsert.Stop();
            TimeSpan timeSpan = clsDataMigrazione.TimeSpanInsert;
            string message = "Durata complessiva";
            if (timeSpan.Days > 0)
                message += (message.Trim() == "" ? "" : " ") + string.Format("{0} giorni", timeSpan.Days);
            if (timeSpan.Hours > 0)
                message += (message.Trim() == "" ? "" : " ") + string.Format("{0} ore", timeSpan.Hours);
            message += (message.Trim() == "" ? "" : " ") + string.Format("{0} minuti", timeSpan.Minutes);
            message += (message.Trim() == "" ? "" : " ") + string.Format("{0} secondi", timeSpan.Seconds);
            this.SetLblMigrazioneValue(message, false);
            this.SetLblMigrazioneValue("Inserimento terminato", false);
        }

        private string lastLineaInsert = "";
        private void SetLblMigrazioneValue(string value, bool lastValue)
        {
            if (this.InvokeRequired)
            {
                delegateSetLblMigrazioneValue d = new delegateSetLblMigrazioneValue(SetLblMigrazioneValue);
                this.Invoke(d, value, lastValue);
            }
            else
            {
                if (!lastValue || this.txtInsertLogs.Text.Trim() == "")
                {
                    this.txtInsertLogs.Text += (this.txtInsertLogs.Text.Trim() == "" ? "" : "\r\n") + value;
                }
                else
                {
                    if (lastLineaInsert != value)
                    {
                        this.txtInsertLogs.Text += (this.txtInsertLogs.Text.Trim() == "" ? "" : "\r\n") + value;
                        lastLineaInsert = value;
                    }
                }

                this.txtInsertLogs.SelectionLength = 0;
                this.txtInsertLogs.SelectionStart = this.txtInsertLogs.Text.Length;
                this.txtInsertLogs.ScrollToCaret();
                this.txtInsertLogs.Refresh();
                this.Refresh();
                Application.DoEvents();
            }
        }

        private void TimerRefreshInsert_Tick(object sender, EventArgs e)
        {
            if (clsDataMigrazione.ThreadInsert != null)
            {
                if (clsDataMigrazione.IsInsertStarted)
                    this.ReadInsertingData();
                if (clsDataMigrazione.IsInsertEnded)
                    this.timerRefreshInsert.Stop();
            }
            else
            {
                this.timerRefreshInsert.Stop();
                this.SetLblMigrazioneValue("Nessun inserimento vecchi log in corso", true);
            }
        }

        private void ReadInsertingData()
        {
            try
            {
                IRecordSet oRS = (IRecordSet)this.conn.ExecuteQuery("SELECT LOG_DA_INSERIRE, LTA_DA_INSERIRE, LOG_INSERITI, LTA_INSERITI FROM TZ_OLDS_LOGS_DATA_MIGRAZIONE");
                if (!oRS.EOF)
                {
                    string message = string.Format("Log totali {0}, Lta totali {1}",
                                                   (!oRS.Fields("LOG_INSERITI").IsNull ? oRS.Fields("LOG_INSERITI").Value.ToString() : ""),
                                                   (!oRS.Fields("LTA_INSERITI").IsNull ? oRS.Fields("LTA_INSERITI").Value.ToString() : ""));
                    this.SetLblMigrazioneValue(message, true);
                }
                oRS.Close();
            }
            catch (Exception)
            {
            }
        }

        private void InitDataMigrazione(bool ricalc)
        {
            this.btnDataMigrazione.Text = "";
            this.btnDataMigrazione.Connesso = true;
            this.btnDataMigrazione.VerificheInCorso = true;
            this.btnDataMigrazione.Invalidate();
            Application.DoEvents();
            clsDataMigrazione.CalcOldTransazioni = ricalc;
            clsDataMigrazione.InitData(conn, this.btnDataMigrazione.data);
            this.btnDataMigrazione.VerificheInCorso = false;
            this.btnDataMigrazione.Invalidate();
            Application.DoEvents();
        }

        public class clsButtonDataMigrazione : ConfigRiepiloghi.clsButton
        {
            private bool connesso = false;
            public bool VerificheInCorso = false;
            
            [System.ComponentModel.Browsable(false)]
            public clsDataMigrazione data { get; set; }

            public clsButtonDataMigrazione()
            {
                this.data = new clsDataMigrazione();
            }

            public bool Connesso
            {
                get { return this.connesso; }
                set { this.connesso = value; }
            }


            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                SolidBrush oBrush = new SolidBrush(this.ForeColor);
                StringFormat oST = new StringFormat();
                oST.Alignment = StringAlignment.Near;
                oST.LineAlignment = StringAlignment.Near;
                string cTesto = "";
                if (Connesso)
                {
                    if (!this.data.ValidTables)
                    {
                        oST.Alignment = StringAlignment.Center;
                        oST.LineAlignment = StringAlignment.Center;
                        oBrush = new SolidBrush(Color.White);
                        e.Graphics.FillRectangle(Brushes.Red, this.ClientRectangle);
                        cTesto = "Data Migrazione:Tabelle di verifica non trovate";
                        e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);
                    }
                    else
                    {
                        if (VerificheInCorso)
                        {
                            oBrush = new SolidBrush(Color.White);
                            e.Graphics.FillRectangle(Brushes.Orange, this.ClientRectangle);
                            oST.Alignment = StringAlignment.Center;
                            oST.LineAlignment = StringAlignment.Center;
                            cTesto = "Data Migrazione: verifiche in corso...";
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);
                        }
                        else
                        {
                            oST.Alignment = StringAlignment.Near;
                            oST.LineAlignment = StringAlignment.Near;
                            if (this.data.DataMigrazione == null)
                            {
                                oBrush = new SolidBrush(Color.White);
                                e.Graphics.FillRectangle(Brushes.Red, this.ClientRectangle);
                                cTesto = "Data Migrazione NON impostata";
                            }
                            else
                                cTesto = "Data Migrazione: " + this.data.DataMigrazione.Value.ToLongDateString();
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);

                            oST.Alignment = StringAlignment.Far;
                            oST.LineAlignment = StringAlignment.Near;
                            if (this.data.DataUltimaVerifica == null)
                                cTesto = "Eseguire la verifica";
                            else
                                cTesto = string.Format("ultima verifica {0}", this.data.DataUltimaVerifica.Value.ToShortDateString());
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);

                            oST.Alignment = StringAlignment.Near;
                            oST.LineAlignment = StringAlignment.Far;
                            cTesto = string.Format("ultimo log vecchio {0}", (this.data.UltimoLogVecchio != null ? this.data.UltimoLogVecchio.Value.ToShortDateString() : "non trovato"));
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);

                            oST.Alignment = StringAlignment.Far;
                            oST.LineAlignment = StringAlignment.Far;
                            cTesto = string.Format("primo log nuovo {0}", (this.data.PrimoLogNuovo != null ? this.data.PrimoLogNuovo.Value.ToShortDateString() : "non trovato"));
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);


                            oST.Alignment = StringAlignment.Center;
                            oST.LineAlignment = StringAlignment.Center;
                            cTesto = string.Format("LOG ins.{0}, Lta ins.{1}, LOG da ins.{2}, LTA da ins.{3}",
                                                   (this.data.LogInseriti == null ? "?" : this.data.LogInseriti.Value.ToString()),
                                                   (this.data.LtaInseriti == null ? "?" : this.data.LtaInseriti.Value.ToString()),
                                                   (this.data.LogDaInserire == null ? "?" : this.data.LogDaInserire.Value.ToString()),
                                                   (this.data.LtaDaInserire == null ? "?" : this.data.LtaDaInserire.Value.ToString()));
                            e.Graphics.DrawString(cTesto, this.Font, oBrush, this.ClientRectangle, oST);
                        }
                    }
                }
                else
                {

                }
                
                //e.Graphics.DrawRectangle(Pens.Black, new Rectangle(0, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1));
            }
        }

        #endregion

        #region Save

        public bool SaveDataSistema()
        {
            return true;
        }

        private bool SaveData()
        {
            bool result = true;
            Exception error = null;

            try
            {
                bool lRCA_DA_GENERARE = true;
                List<Riepiloghi.clsProprietaRiepiloghi> datiAttuali = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(this.conn);
                foreach (Riepiloghi.clsProprietaRiepiloghi proprieta in this.ProprietaRiepiloghi)
                {
                    if (proprieta.Proprieta == "RCA_DA_GENERARE")
                    {
                        lRCA_DA_GENERARE = proprieta.Valore != "DISABILITATO";
                    }

                    bool lChanged = false;
                    bool lFind = false;
                    foreach (Riepiloghi.clsProprietaRiepiloghi proprietaCorrente in datiAttuali)
                    {
                        if (proprietaCorrente.Proprieta == proprieta.Proprieta)
                        {
                            lFind = true;
                            lChanged = proprietaCorrente.Valore != proprieta.Valore;
                            break;
                        }
                    }
                    if (!lFind || lChanged)
                    {
                        if (!Riepiloghi.clsRiepiloghi.SetProprietaRiepiloghi(this.conn, proprieta.Proprieta, proprieta.Valore, out error) || error != null)
                        {
                            result = false;
                            break;
                        }
                    }
                }

                /*
                 * RIEPILOGHI_LTA
                 * RIEPILOGHI_RICERCA_LTA
                 * RIEPILOGHI_TORNELLO
                 * */
                Dictionary<string, string> RP_FUNZIONI_enable_disable = new Dictionary<string, string>()
                { 
                  { "RIEPILOGHI_LTA", "Visualizza/Ricerca LTA" },
                  { "RIEPILOGHI_RICERCA_LTA", "Ricerca LTA per Carta e Progressivo/Sigillo" },
                  { "RIEPILOGHI_TORNELLO", "Tornello Emettitore" }
                };

                foreach (KeyValuePair<string, string> RP_FUNZIONE_enable_disable in RP_FUNZIONI_enable_disable)
                {
                    long ordine = 0;
                    string descCategoria = "";
                    StringBuilder oSB = new StringBuilder();
                    oSB.Append("UPDATE RIEPILOGHI.RP_FUNZIONI SET ENABLED = :pENABLED WHERE IDOPERAZIONE = :pIDOPERAZIONE");
                    clsParameters oPars = new clsParameters();
                    oPars.Add(":pIDOPERAZIONE", RP_FUNZIONE_enable_disable.Key);
                    oPars.Add(":pENABLED", (lRCA_DA_GENERARE ? 1 : 0));
                    long countUpdates = long.Parse(this.conn.ExecuteNonQuery(oSB.ToString(), oPars).ToString());
                    if (countUpdates == 0)
                    {
                        oSB = new StringBuilder();
                        oSB.Append("INSERT INTO RIEPILOGHI.RP_FUNZIONI (IDCATEGORIA, ORDINE, IDOPERAZIONE, DESCRIZIONE, ENABLED)");
                        oSB.Append("SELECT IDCATEGORIA, :pORDINE, :pIDOPERAZIONE, :pDESCRIZIONE, :pENABLED FROM RIEPILOGHI.RP_CATEGORIA_FUNZIONI WHERE DESCRIZIONE = :pCATEGORIA");
                        
                        switch (RP_FUNZIONE_enable_disable.Key)
                        {
                            case "RIEPILOGHI_LTA": { ordine = 31; descCategoria = "Lista Titoli Accesso"; break; }
                            case "RIEPILOGHI_RICERCA_LTA": { ordine = 32; descCategoria = "Lista Titoli Accesso"; break; }
                            case "RIEPILOGHI_TORNELLO": { ordine = 51; descCategoria = "Tornello Emettitore"; break; }
                        }
                        oPars = new clsParameters();
                        oPars.Add(":pORDINE", ordine);
                        oPars.Add(":pIDOPERAZIONE", RP_FUNZIONE_enable_disable.Key);
                        oPars.Add(":pDESCRIZIONE", RP_FUNZIONE_enable_disable.Value);
                        oPars.Add(":pENABLED", (lRCA_DA_GENERARE ? 1 : 0));
                        oPars.Add(":pCATEGORIA", descCategoria);
                        long countInsert = long.Parse(this.conn.ExecuteNonQuery(oSB.ToString(), oPars).ToString());
                    }
                }

                string connectionString = "";
                bool lSaveConnString = false;
                if (this.radioConnTNS.Checked)
                {
                    connectionString = this.txtConnectionStringTns.Text;
                    lSaveConnString = !string.IsNullOrEmpty(connectionString) && !string.IsNullOrWhiteSpace(connectionString);
                }
                else
                {
                    connectionString = this.txtConnectionStringDirect.Text;
                    Regex regStringConn = new Regex(@"\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=127.0.0.1\)\(PORT=\d{4}\)\)\)\(CONNECT_DATA=\(SERVER=DEDICATED\)\(SERVICE_NAME=[A-Za-z0-9.]+\)\)\)");
                    lSaveConnString = regStringConn.IsMatch(connectionString);
                }

                if (!lSaveConnString)
                {
                    MessageBox.Show("Stringa di connessione non valida, questa impostazione non è stata salvata.");
                }
                else
                {
                    StringBuilder oSBStringConn = new StringBuilder();
                    oSBStringConn.Append("UPDATE RIEPILOGHI.RP_CODICI_SISTEMA ");
                    oSBStringConn.Append(" SET ");
                    oSBStringConn.Append(" RP_CODICI_SISTEMA.CONNECTION_STRING = :pCONNECTION_STRING");
                    oSBStringConn.Append(" WHERE RP_CODICI_SISTEMA.CODICE_SISTEMA IN (");
                    oSBStringConn.Append(" SELECT RP_CODICI_SISTEMA.CODICE_SISTEMA ");
                    oSBStringConn.Append(" FROM ");
                    oSBStringConn.Append(" RIEPILOGHI.RP_CODICI_SISTEMA");
                    oSBStringConn.Append(" INNER JOIN WTIC.SMART_CS ON SMART_CS.CODICE_SISTEMA = RP_CODICI_SISTEMA.CODICE_SISTEMA AND SMART_CS.ENABLED = 1");
                    oSBStringConn.Append(" INNER JOIN (SELECT NVL(MAX(VALUE),'DISABILITATO') AS RP_LOGIN_MODE FROM RIEPILOGHI.RP_PROPERTIES WHERE PROPERTY = 'CODICE_SISTEMA_LOGIN_OPERATORI_LOCALI') SISTEMA_UNICO ON SISTEMA_UNICO.RP_LOGIN_MODE = 'ABILITATO'");
                    oSBStringConn.Append(" WHERE RP_CODICI_SISTEMA.ENABLED = 1");
                    oSBStringConn.Append(" )");

                    this.conn.ExecuteNonQuery(oSBStringConn.ToString(), new clsParameters(":pCONNECTION_STRING", connectionString));
                }

                
            }
            catch (Exception ex)
            {
                result = false;
                error = ex;
                this.ShowError(error, "Salvataggio");
            }


            return result;
        }

        #endregion

        private void BtnSave_Click(object sender, EventArgs e)
        {
            bool lCanClose = false;
            bool lExecution = false;
            bool lExecuted = false;
            if (clsDataMigrazione.ThreadInsert != null)
            {
                this.btnSave.Enabled = false;
                if (clsDataMigrazione.IsInsertStarted && !clsDataMigrazione.IsInsertEnded)
                    lExecution = true;
                else if (clsDataMigrazione.IsInsertStarted && clsDataMigrazione.IsInsertEnded)
                    lExecuted = true;
            }
            else
                lCanClose = true;

            if (lCanClose)
            {
                if (this.SaveData())
                {
                    this.DialogResult = DialogResult.Abort;
                    this.Close();
                }
            }
            else
            {
                if (lExecuted)
                {
                    MessageBox.Show("Chiudere e riavviare l'applicazione per modificare i parametri.");
                }
                else if (lExecution)
                    MessageBox.Show("Operazione di inserimento in corso, attendere (eventualmente iconizzare)...");
            }
        }

        private void BtnChiudi_Click(object sender, EventArgs e)
        {
            bool lCanClose = false;
            if (clsDataMigrazione.ThreadInsert != null)
            {
                if (!clsDataMigrazione.IsInsertStarted || clsDataMigrazione.IsInsertEnded)
                    lCanClose = true;
            }
            else
                lCanClose = true;

            if (lCanClose)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
            else
            {
                MessageBox.Show("Operazione di inserimento in corso, attendere (eventualmente iconizzare)...");
            }
        }


        private void ShowError(Exception error, string Info = "", string caption = "")
        {
            MessageBox.Show((Info.Trim() != "" ? Info + "\r\n" : "") + error.Message, (caption.Trim() != "" ? caption : "Errore"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ConfigRiepiloghi_Shown(object sender, EventArgs e)
        {
            this.btnConn.Click += BtnConn_Click;
            this.btnDataMigrazione.Click += BtnDataMigrazione_Click;
            this.btnIconizza.Click += BtnIconizza_Click;
            this.timerRefreshInsert.Tick += TimerRefreshInsert_Tick;
        }

        private void BtnIconizza_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ConfigRiepiloghi_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.CloseConnection();
        }

        private void BtnConn_Click(object sender, EventArgs e)
        {
            this.btnConn.Enabled = false;
            if (this.OpenConnection())
            {
                //if (this.chkSistema.Checked)
                //    this.ReadDataSistema();
                //else
                    this.ReadData();
            }
            else
                this.btnConn.Enabled = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.btnConn.Enabled = false;
            if (this.OpenConnection())
            {
                this.ReadData(false);
            }
            else
                this.btnConn.Enabled = true;
        }

        private void btnAltro_Click(object sender, EventArgs e)
        {
            Dictionary<object, string> options = new Dictionary<object, string>();
            options.Add("STATO_RIEPILOGHI", "Stato riepiloghi/Email");
            options.Add("SAVE_TO_FILE", "Salva su File");
            options.Add("IMPORT_FROM_FILE", "Importa da File");
            object result = frmGetGenericData.MenuGenerico("Altre operazioni", options);
            if (result != null)
            {
                string cResult = result.ToString();
                switch (cResult)
                {
                    case "STATO_RIEPILOGHI":
                        {
                            frmStatoRiepiloghi form = new frmStatoRiepiloghi(this.conn);
                            form.ShowDialog();
                            form.Close();
                            form.Dispose();
                            break;
                        }
                    case "SAVE_TO_FILE":
                        {
                            this.SaveToFile();
                            break;
                        }
                    case "IMPORT_FROM_FILE":
                        {
                            this.ImportFile();
                            break;
                        }
                }
            }
        }

        private void SaveToFile()
        {
            OpenFileDialog fileDialog = new OpenFileDialog()
            {
                FileName = "PROPRIETA_RIEPILOGHI.json",
                DefaultExt = "*.json|File .json",
                CheckFileExists = false,
                Multiselect = false
            };
            string targetFile = "";
            if (fileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(fileDialog.FileName))
            {
                targetFile = fileDialog.FileName;
            }
            if (!string.IsNullOrEmpty(targetFile))
            {
                if (File.Exists(targetFile))
                    File.Delete(targetFile);
                 string jsonValue = Newtonsoft.Json.JsonConvert.SerializeObject(this.ProprietaRiepiloghi, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(targetFile, jsonValue);
            }
        }

        private void ImportFile()
        {
            OpenFileDialog fileDialog = new OpenFileDialog()
            {
                FileName = "PROPRIETA_RIEPILOGHI.json",
                DefaultExt = "*.json|File .json",
                CheckFileExists = true,
                Multiselect = false
            };
            string sourceFile = "";
            if (fileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(fileDialog.FileName))
            {
                sourceFile = fileDialog.FileName;
            }
            if (!string.IsNullOrEmpty(sourceFile))
            {
                if (File.Exists(sourceFile))
                {
                    try
                    {
                        string jsonValue = File.ReadAllText(sourceFile);
                        List<Riepiloghi.clsProprietaRiepiloghi> data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Riepiloghi.clsProprietaRiepiloghi>>(jsonValue);
                        
                        if (data != null)
                        {
                            Func<Control, string, usrProprietaRiepiloghi> GetUsrCtl = null;
                            GetUsrCtl = delegate (Control parent, string propName)
                            {
                                usrProprietaRiepiloghi result = null;
                                try
                                {
                                    result = (usrProprietaRiepiloghi)parent;
                                    if (propName != result.Proprieta.Proprieta)
                                        result = null;
                                }
                                catch (Exception)
                                {
                                    result = null;
                                }
                                if (result == null)
                                {
                                    try
                                    {
                                        if (parent.Controls != null && parent.Controls.Count > 0)
                                        {
                                            foreach (Control subControl in parent.Controls)
                                            {
                                                usrProprietaRiepiloghi tempResult = GetUsrCtl(subControl, propName);
                                                if (tempResult != null && tempResult.Proprieta.Proprieta == propName)
                                                {
                                                    result = tempResult;
                                                    break;
                                                }
                                            }
                                        }    
                                    }
                                    catch (Exception)
                                    {
                                        result = null;
                                        throw;
                                    }
                                }
                                return result;
                            };
                            frmWait wait = new frmWait(false);
                            int index = 0;
                            wait.TopMost = true;
                            wait.Message = string.Format("{0} di {1}", index, data.Count);
                            wait.Show();
                            foreach (Riepiloghi.clsProprietaRiepiloghi prop in data)
                            {
                                index += 1;
                                wait.Message = string.Format("{0} di {1}", index, data.Count);
                                usrProprietaRiepiloghi usrItemInPanel = listaUserControls.FirstOrDefault(u => u.Proprieta != null && u.Proprieta.Proprieta == prop.Proprieta);
                                if (usrItemInPanel != null)
                                {
                                    if (usrItemInPanel.Proprieta.Valore == null && prop.Valore == null)
                                    {

                                    }
                                    else if (usrItemInPanel.Proprieta.Valore != null && prop.Valore == null)
                                    {
                                        usrItemInPanel.Proprieta.Valore = prop.Valore;
                                        usrItemInPanel.ChangeControlValue();
                                    }
                                    else if (usrItemInPanel.Proprieta.Valore == null && prop.Valore != null)
                                    {
                                        usrItemInPanel.Proprieta.Valore = prop.Valore;
                                        usrItemInPanel.ChangeControlValue();
                                    }
                                    else if (usrItemInPanel.Proprieta.Valore != null && prop.Valore != null && !usrItemInPanel.Proprieta.Valore.Equals(prop.Valore))
                                    {
                                        usrItemInPanel.Proprieta.Valore = prop.Valore;
                                        usrItemInPanel.ChangeControlValue();
                                    }
                                }
                            }
                            wait.Close();
                            wait.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("File non valido.");
                    }
                }
            }
        }

    }
}
