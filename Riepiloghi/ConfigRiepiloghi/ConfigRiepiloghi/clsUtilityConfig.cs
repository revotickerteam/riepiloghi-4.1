﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;
using System.Drawing;
using System.Diagnostics;

namespace ConfigRiepiloghi
{
    [DebuggerStepThrough]
    public class frmWait : Form
    {
        private string message = "";
        private bool timerIsOn = false;
        private Stopwatch stopW;

        public frmWait(bool activateTimer)
        {
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.Padding = new Padding(5);
            this.Font = new Font("Calibri", 12);
            this.timerIsOn = activateTimer;
            this.Message = "Attendere...";
            this.ControlBox = false;
            this.ShowInTaskbar = false;
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.BackColor = Color.LightGray;
            //this.Text = "Attendere...";
            this.TopMost = true;
            this.Shown += FrmWait_Shown;
            this.FormClosing += FrmWait_FormClosing;
            this.Show();
        }

        

        private void FrmWait_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.timerIsOn)
            {
                this.timerIsOn = false;
            }
        }

        private void FrmWait_Shown(object sender, EventArgs e)
        {
            if (this.timerIsOn)
            {
                this.stopW = new Stopwatch();
                this.stopW.Start();
            }
        }

        private void AggiornaTimer()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker d = new MethodInvoker(AggiornaTimer);
                this.Invoke(d);
            }
            else
            {
                this.Invalidate();
                this.Refresh();
                Application.DoEvents();
            }
        }

        public string Message
        {
            get { return this.message; }
            set { this.message = value; this.AggiornaTimer(); this.ResizeToMessage(); }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Rectangle rect = new Rectangle(0, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1);
            e.Graphics.SetClip(rect);
            string timerValue = "";
            if (this.timerIsOn && this.stopW != null && this.stopW.Elapsed.TotalSeconds > 0)
                timerValue = string.Format("{0}.{1}.{2}", this.stopW.Elapsed.Hours.ToString("00"), this.stopW.Elapsed.Minutes.ToString("00"), this.stopW.Elapsed.Seconds.ToString("00"));
            StringFormat oST = new StringFormat();
            oST.Alignment = StringAlignment.Center;
            oST.LineAlignment = StringAlignment.Center;
            //e.Graphics.DrawRectangle(Pens.Black, rect);
            e.Graphics.DrawString((timerValue == "" ? "" : timerValue + " ") + this.message, this.Font, new SolidBrush(this.ForeColor), rect, oST);
        }

        private void ResizeToMessage()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker d = new MethodInvoker(this.ResizeToMessage);
                this.Invoke(d);
            }
            else
            {
                Graphics g = this.CreateGraphics();
                string timerValue = "";
                if (this.timerIsOn)
                {
                    timerValue = "  00.00.00  ";
                }
                SizeF size = g.MeasureString((timerValue == "" ? "" : timerValue + " ") + this.message, this.Font);
                this.ClientSize = new Size(this.Padding.Horizontal + (int)size.Width, this.Padding.Vertical + (int)size.Height);
                this.Invalidate();
                this.CenterToScreen();
                this.Refresh();
                Application.DoEvents();
            }
        }
    }
    
    public class clsDataMigrazione
    {

        public static bool CalcOldTransazioni = true;
        public DateTime? DataMigrazione { get; set; }
        public DateTime? DataUltimaVerifica { get; set; }
        public DateTime? UltimoLogVecchio { get; set; }
        public DateTime? PrimoLogNuovo { get; set; }

        public bool ValidTables = false;

        public long? LogInseriti { get; set; }
        public long? LtaInseriti { get; set; }

        public long? LogDaInserire = 0;
        public long? LtaDaInserire = 0;

        public delegate void delegateSetOutMessage(string value);
        public static event delegateSetOutMessage SetOutMessageEvent;
        public delegate void delegateInsertEnded();
        public static event delegateInsertEnded InsertEnded;

        public bool LavoroEseguito => LogDaInserire + LtaDaInserire == 0;

        private static void SetOutMessage(string value)
        {
            if (SetOutMessageEvent != null)
                SetOutMessageEvent(value);
        }

        public static bool InitData(IConnection conn, clsDataMigrazione data)
        {
            //SELECT SEQ_ID_OLDS_INSERT.NEXTVAL INTO nID_OLDS_INSERT FROM DUAL;


            bool result = false;
            StringBuilder oSB = null;
            IRecordSet oRS = null;
            clsParameters oPars = null;
            data.DataMigrazione = null;
            data.DataUltimaVerifica = null;
            data.LogDaInserire = null;
            data.LtaDaInserire = null;
            data.LogInseriti = null;
            data.LtaInseriti = null;
            data.PrimoLogNuovo = null;
            data.UltimoLogVecchio = null;

            try
            {
                List<string> tables = new List<string>() { "TZ_OLDS_LOGS_DATA_MIGRAZIONE",
                                                           "TZ_INSERT_OLDS_LOGS",
                                                           "TZ_INSERT_OLDS_LTAS" };

                List<string> views = new List<string>() { "VZ_OLDS_LOGS",
                                                          "VZ_OLDS_LOGS_BIGL_VD",
                                                          "VZ_OLDS_LOGS_BIGL_AN",
                                                          "VZ_OLDS_LOGS_BIGL_OK",
                                                          "VZ_OLDS_LOGS_BIGLIETTI",
                                                          "VZ_OLDS_LOGS_RATEI_VD",
                                                          "VZ_OLDS_LOGS_RATEI_AN",
                                                          "VZ_OLDS_LOGS_RATEI_OK",
                                                          "VZ_OLDS_LOGS_RATEI_LIBERO",
                                                          "VZ_OLDS_LOGS_FISSI_VD",
                                                          "VZ_OLDS_LOGS_ABBF_VD",
                                                          "VZ_OLDS_LOGS_ABBF_AN",
                                                          "VZ_OLDS_LOGS_ABBF_OK",
                                                          "VZ_OLDS_LOGS_ABB_FISSO",
                                                          "VZ_OLDS_LOGS_ABBF_SIG",
                                                          "VZ_OLDS_LOGS_ABB_FISSO_SIG",
                                                          "VZ_OLDS_LOGS_RATEI_FISSO",
                                                          "VZ_OLDS_LOGS_INSLOG",
                                                          "VZ_OLDS_LOGS_INSLTA_BIGL",
                                                          "VZ_OLDS_LOGS_INSLTA_RATEIL",
                                                          "VZ_OLDS_LOGS_INSLTA_RATEIF",
                                                          "VZ_OLDS_LOGS_TIMES" };

                List<string> procedures = new List<string>() { "PZ_OLDS_LOGS_INSERT_RANGE",
                                                               "PZ_OLDS_LTAS_INSERT_RANGE",
                                                               "PZ_OLDS_LOGS_INSERT_MAIN"};

                foreach (string tableName in tables)
                {
                    oSB = new StringBuilder();
                    oSB.Append("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = :pTABLE_NAME");
                    //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pTABLE_NAME", "TZ_OLDS_LOGS_DATA_MIGRAZIONE"));
                    oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pTABLE_NAME", tableName));
                    data.ValidTables = (!oRS.EOF);
                    oRS.Close();
                    if (!data.ValidTables)
                        break;
                }

                //List<string> views = new List<string>() { "VZ_OLDS_LOGS", "VZ_OLDS_LOGS_BIGLIETTI", "VZ_OLDS_LOGS_ABB_LIBERO", "VZ_OLDS_LOGS_ABB_FISSO", "VZ_OLDS_LOGS_ABB_FISSO_SIG", "VZ_OLDS_LOGS_RATEI_LIBERO", "VZ_OLDS_LOGS_RATEI_FISSO", "VZ_OLDS_LOGS_INSLOG", "VZ_OLDS_LOGS_INSLTA" };

                if (data.ValidTables)
                {
                    foreach (string viewName in views)
                    {
                        oSB = new StringBuilder();
                        oSB.Append("SELECT VIEW_NAME FROM USER_VIEWS WHERE VIEW_NAME = :pVIEW_NAME");
                        oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pVIEW_NAME", viewName));
                        data.ValidTables = (!oRS.EOF);
                        oRS.Close();
                        if (!data.ValidTables)
                            break;
                    }
                }

                if (data.ValidTables)
                {
                    foreach (string procedureName in procedures)
                    {
                        oSB = new StringBuilder();
                        oSB.Append("SELECT OBJECT_NAME FROM USER_PROCEDURES WHERE OBJECT_NAME = :pOBJECT_NAME");
                        oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pOBJECT_NAME", procedureName));
                        data.ValidTables = (!oRS.EOF);
                        oRS.Close();
                        if (!data.ValidTables)
                            break;
                    }
                }

                if (data.ValidTables)
                {
                    oSB = new StringBuilder();
                    oSB.Append("SELECT * FROM WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE");
                    oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                    if (!oRS.EOF)
                    {
                        if (!oRS.Fields("DATA_MIGRAZIONE").IsNull)
                            data.DataMigrazione = (DateTime)oRS.Fields("DATA_MIGRAZIONE").Value;
                        if (!oRS.Fields("DATA_ULTIMA_VERIFICA").IsNull)
                            data.DataUltimaVerifica = (DateTime)oRS.Fields("DATA_ULTIMA_VERIFICA").Value;
                        if (!oRS.Fields("LOG_DA_INSERIRE").IsNull)
                            data.LogDaInserire = long.Parse(oRS.Fields("LOG_DA_INSERIRE").Value.ToString());
                        if (!oRS.Fields("LTA_DA_INSERIRE").IsNull)
                            data.LtaDaInserire = long.Parse(oRS.Fields("LTA_DA_INSERIRE").Value.ToString());
                        if (!oRS.Fields("LOG_INSERITI").IsNull)
                            data.LogInseriti = long.Parse(oRS.Fields("LOG_INSERITI").Value.ToString());
                        if (!oRS.Fields("LTA_INSERITI").IsNull)
                            data.LtaInseriti = long.Parse(oRS.Fields("LTA_INSERITI").Value.ToString());
                    }
                    oRS.Close();
                }
            }
            catch (Exception)
            {
            }

            if (clsDataMigrazione.CalcOldTransazioni)
            {
                try
                {
                    oSB = new StringBuilder();
                    oSB.Append("SELECT MAX(DATA_EMISSIONE) AS DATA_EMISSIONE FROM ");
                    oSB.Append("(");
                    oSB.Append(" SELECT MAX(SUBSTR(LOG_DATA,65,8)) AS DATA_EMISSIONE FROM WTIC.LOGTRANS_KEEP");
                    oSB.Append(" UNION ALL");
                    oSB.Append(" SELECT MAX(SUBSTR(LOG_DATA,65,8)) AS DATA_EMISSIONE FROM WTIC.LOGTRANS_ARCHIVE_KEEP");
                    oSB.Append(")");
                    oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                    if (!oRS.EOF && !oRS.Fields("DATA_EMISSIONE").IsNull)
                        data.UltimoLogVecchio = new DateTime(int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(0, 4)), int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(4, 2)), int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(6, 2)));
                    oRS.Close();
                }
                catch (Exception ex)
                {
                }
            }
            
            try
            {
                oSB = new StringBuilder();
                oSB.Append(" SELECT MIN(DATA_EMISSIONE_ANNULLAMENTO) AS DATA_EMISSIONE FROM WTIC.LOG_TRANSAZIONI");
                oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
                if (!oRS.EOF && !oRS.Fields("DATA_EMISSIONE").IsNull)
                    data.PrimoLogNuovo = new DateTime(int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(0, 4)), int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(4, 2)), int.Parse(oRS.Fields("DATA_EMISSIONE").Value.ToString().Substring(6, 2)));
                oRS.Close();
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        //public static void CountData(IConnection conn, clsDataMigrazione data)
        //{
        //    StringBuilder oSB = null;
        //    IRecordSet oRS = null;
        //    clsParameters oPars = null;
        //    try
        //    {
        //        long count = 0;
        //        data.LogDaInserire = 0;
        //        data.LtaDaInserire = 0;

        //        SetOutMessage("conteggio Biglietti...");

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LOGS FROM WTIC.VZ_OLDS_LOGS_BIGLIETTI X WHERE NOT EXISTS (SELECT NULL FROM WTIC.LOG_TRANSAZIONI L WHERE L.CODICE_SISTEMA = X.CODICE_SISTEMA AND L.CODICE_CARTA = X.CODICE_CARTA AND L.PROGRESSIVO_TITOLO = X.PROGRESSIVO_TITOLO AND L.SIGILLO = X.SIGILLO)");

        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LOGS").IsNull)
        //        //    count += long.Parse(oRS.Fields("CNT_LOGS").Value.ToString());
        //        //oRS.Close();

        //        SetOutMessage(string.Format("Biglietti {0}, Ratei...", count.ToString()));

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LOGS  FROM WTIC.VZ_OLDS_LOGS_RATEI_LIBERO X WHERE NOT EXISTS (SELECT NULL FROM WTIC.LOG_TRANSAZIONI L WHERE L.CODICE_SISTEMA = X.CODICE_SISTEMA AND L.CODICE_CARTA = X.CODICE_CARTA AND L.PROGRESSIVO_TITOLO = X.PROGRESSIVO_TITOLO AND L.SIGILLO = X.SIGILLO)");

        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LOGS").IsNull)
        //        //    count += long.Parse(oRS.Fields("CNT_LOGS").Value.ToString());
        //        //oRS.Close();

        //        SetOutMessage(string.Format("Biglietti + Ratei {0}, Abb. Turno Libero...", count.ToString()));

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LOGS  FROM WTIC.VZ_OLDS_LOGS_ABB_LIBERO X WHERE NOT EXISTS (SELECT NULL FROM WTIC.LOG_TRANSAZIONI L WHERE L.CODICE_SISTEMA = X.CODICE_SISTEMA AND L.CODICE_CARTA = X.CODICE_CARTA AND L.PROGRESSIVO_TITOLO = X.PROGRESSIVO_TITOLO AND L.SIGILLO = X.SIGILLO)");

        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LOGS").IsNull)
        //        //    count += long.Parse(oRS.Fields("CNT_LOGS").Value.ToString());
        //        //oRS.Close();

        //        SetOutMessage(string.Format("Biglietti + Ratei + Abb. Turno Libero {0}, Abb. Turno Fisso...", count.ToString()));

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LOGS  FROM WTIC.VZ_OLDS_LOGS_ABB_FISSO X WHERE NOT EXISTS (SELECT NULL FROM WTIC.LOG_TRANSAZIONI L WHERE L.CODICE_SISTEMA = X.CODICE_SISTEMA AND L.CODICE_CARTA = X.CODICE_CARTA AND L.PROGRESSIVO_TITOLO = X.PROGRESSIVO_TITOLO AND L.SIGILLO = X.SIGILLO)");
        //        //bool existsTurniFissi = false;
        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LOGS").IsNull)
        //        //{
        //        //    count += long.Parse(oRS.Fields("CNT_LOGS").Value.ToString());
        //        //    existsTurniFissi = long.Parse(oRS.Fields("CNT_LOGS").Value.ToString()) > 0;
        //        //}
        //        //oRS.Close();


        //        //data.LogDaInserire = count;

        //        SetOutMessage(string.Format("Biglietti + Ratei + Abb. Turno Libero e Fisso {0}, Lta...", count.ToString()));

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LTA FROM WTIC.VZ_OLDS_LOGS_INSLTA_BIGL");
        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LTA").IsNull)
        //        //    data.LtaDaInserire = long.Parse(oRS.Fields("CNT_LTA").Value.ToString());
        //        //oRS.Close();

        //        //oSB = new StringBuilder();
        //        //oSB.Append("SELECT COUNT(*) AS CNT_LTA FROM WTIC.VZ_OLDS_LOGS_INSLTA_RATEIL");
        //        //oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //if (!oRS.EOF && !oRS.Fields("CNT_LTA").IsNull)
        //        //    data.LtaDaInserire += long.Parse(oRS.Fields("CNT_LTA").Value.ToString());
        //        //oRS.Close();

        //        //if (existsTurniFissi)
        //        //{
        //        //    oSB = new StringBuilder();
        //        //    oSB.Append("SELECT COUNT(*) AS CNT_LTA FROM WTIC.VZ_OLDS_LOGS_INSLTA_RATEIF");
        //        //    oRS = (IRecordSet)conn.ExecuteQuery(oSB.ToString());
        //        //    if (!oRS.EOF && !oRS.Fields("CNT_LTA").IsNull)
        //        //        data.LtaDaInserire += long.Parse(oRS.Fields("CNT_LTA").Value.ToString());
        //        //    oRS.Close();
        //        //}

        //        SetOutMessage(string.Format("Biglietti + Ratei + Abb. Turno Libero e Fisso {0}, Lta {1}", data.LogDaInserire.ToString(), data.LtaDaInserire.ToString()));
        //        clsDataMigrazione.AssignData(conn, data, true);
        //    }
        //    catch (Exception EX)
        //    {
        //        MessageBox.Show(EX.Message);
        //    }
        //}

        public static bool AssignData(IConnection conn, clsDataMigrazione data, bool assignUltimaVerificaSysDate = false)
        {
            bool result = false;
            try
            {
                IRecordSet oRS = (IRecordSet)conn.ExecuteQuery("SELECT * FROM WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE");
                bool append = oRS.EOF;
                oRS.Close();

                if (append)
                {
                    conn.ExecuteNonQuery("INSERT INTO WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE (DATA_MIGRAZIONE) VALUES (NULL)");
                }

                StringBuilder oSB = new StringBuilder();
                clsParameters oPars = new clsParameters();

                oSB.Append("UPDATE WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE ");
                oSB.Append(" SET ");
                if (data.DataMigrazione == null)
                    oSB.Append(" DATA_MIGRAZIONE = NULL");
                else
                {
                    oSB.Append(" DATA_MIGRAZIONE = :pDATA_MIGRAZIONE");
                    oPars.Add(":pDATA_MIGRAZIONE", data.DataMigrazione);
                }

                if (data.DataUltimaVerifica == null && !assignUltimaVerificaSysDate)
                    oSB.Append(" ,DATA_ULTIMA_VERIFICA = NULL");
                else
                {
                    if (assignUltimaVerificaSysDate)
                        oSB.Append(" ,DATA_ULTIMA_VERIFICA = SYSDATE");
                    else
                    {
                        oSB.Append(" ,DATA_ULTIMA_VERIFICA = :pDATA_ULTIMA_VERIFICA");
                        oPars.Add(":pDATA_ULTIMA_VERIFICA", data.DataUltimaVerifica);
                    }
                }

                //DATA_MIGRAZIONE DATE, DATA_ULTIMA_VERIFICA DATE, LOG_DA_INSERIRE NUMBER, LTA_DA_INSERIRE NUMBER, LOG_INSERITI NUMBER DEFAULT 0, LTA_INSERITI NUMBER DEFAULT 0

                if (data.LogDaInserire == null)
                    oSB.Append(" ,LOG_DA_INSERIRE = NULL");
                else
                {
                    oSB.Append(" ,LOG_DA_INSERIRE = :pLOG_DA_INSERIRE");
                    oPars.Add(":pLOG_DA_INSERIRE", data.LogDaInserire);
                }

                if (data.LtaDaInserire == null)
                    oSB.Append(" ,LTA_DA_INSERIRE = NULL");
                else
                {
                    oSB.Append(" ,LTA_DA_INSERIRE = :pLTA_DA_INSERIRE");
                    oPars.Add(":pLTA_DA_INSERIRE", data.LtaDaInserire);
                }


                if (data.LogInseriti == null)
                    oSB.Append(" ,LOG_INSERITI = NULL");
                else
                {
                    oSB.Append(" ,LOG_INSERITI = :pLOG_INSERITI");
                    oPars.Add(":pLOG_INSERITI", data.LogInseriti);
                }

                if (data.LtaInseriti == null)
                    oSB.Append(" ,LTA_INSERITI = NULL");
                else
                {
                    oSB.Append(" ,LTA_INSERITI = :pLTA_INSERITI");
                    oPars.Add(":pLTA_INSERITI", data.LtaInseriti);
                }

                conn.ExecuteNonQuery(oSB.ToString(), oPars);

                result = true;

                clsDataMigrazione.InitData(conn, data);
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public static bool InsertData(IConnection conn, out Exception error)
        {
            error = null;
            bool result = false;
            IRecordSet oRS = (IRecordSet)conn.ExecuteQuery("SELECT WTIC.SEQ_ID_OLDS_INSERT.NEXTVAL AS ID_LOGS_INSERT FROM DUAL");
            if (!oRS.EOF && !oRS.Fields("ID_LOGS_INSERT").IsNull)
                IdLogsInsert = long.Parse(oRS.Fields("ID_LOGS_INSERT").Value.ToString());
            oRS.Close();

            if (IdLogsInsert > 0)
            {
                IsInsertStarted = true;
                IsInsertEnded = false;
                ThreadInsert = new System.Threading.Thread(new System.Threading.ThreadStart(InsertDataThread));
                ThreadInsert.Start();
                result = true;
            }
            return result;
        }

        public static string DataSourceInsertOlds { get; set; }
        public static long IdLogsInsert { get; set; }
        public static System.Threading.Thread ThreadInsert { get; set; }
        public static DateTime DataMigrazioneReale { get; set; }
        public static DateTime DataMigrazioneTEMPORANEA { get; set; }
        
        public static bool IsInsertStarted = false;
        public static bool IsInsertEnded = false;
        private static Stopwatch stopwatchInsert { get; set; }

        public static void InsertDataThread()
        {
            System.Threading.Thread.Sleep(5000);
            SetOutMessage("Avvio...");
            stopwatchInsert = new Stopwatch();
            stopwatchInsert.Start();
            Wintic.Data.IConnection connectionInsertOldsLogs = new Wintic.Data.oracle.CConnectionOracle();
            try
            {
                SetOutMessage("Connessione...");
                connectionInsertOldsLogs.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", DataSourceInsertOlds);
                connectionInsertOldsLogs.Open();

                SetOutMessage("Esecuzione...");

                StringBuilder oSB = new StringBuilder();
                oSB.Append("DECLARE");
                oSB.Append(" BEGIN");
                oSB.Append("  WTIC.PZ_OLDS_LOGS_INSERT_MAIN(:pID_LOGS_INSERT);");
                oSB.Append(" END;");

                connectionInsertOldsLogs.ExecuteNonQuery(oSB.ToString(), new clsParameters(":pID_LOGS_INSERT", IdLogsInsert));
            }
            catch (Exception ex)
            {
                SetOutMessage("Errore in inserimento vecchi logs\r\n" + ex.Message);
            }
            finally
            {
                if (connectionInsertOldsLogs != null)
                {
                    try
                    {
                        if (DataMigrazioneTEMPORANEA.Date != DataMigrazioneReale.Date)
                        {
                            SetOutMessage("Ripristino data migrazione originale...");
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("UPDATE WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE SET DATA_MIGRAZIONE = :pDATA_MIGRAZIONE");
                            connectionInsertOldsLogs.ExecuteNonQuery(oSB.ToString(), new clsParameters(":pDATA_MIGRAZIONE", DataMigrazioneReale.Date));
                        }
                    }
                    catch (Exception exRettificadataMigrazione)
                    {
                        SetOutMessage("Errore GRAVE impostazione data migrazione reale, intervenire manualmente sulla tabella WTIC.TZ_OLDS_LOGS_DATA_MIGRAZIONE" + 
                                      "\r\n" + string.Format("impostando DATA_MIGRAZIONE = {0}", DataMigrazioneReale.ToShortDateString()) + 
                                      "\r\n" + exRettificadataMigrazione.Message);
                    }
                    SetOutMessage("Chiusura connessione...");
                    connectionInsertOldsLogs.Close();
                    connectionInsertOldsLogs.Dispose();
                }
                connectionInsertOldsLogs = null;
                IsInsertEnded = true;
            }

            stopwatchInsert.Stop();
            if (InsertEnded != null) InsertEnded();
        }

        public static TimeSpan TimeSpanInsert => (stopwatchInsert != null ? stopwatchInsert.Elapsed : new TimeSpan(0));


    }
    public static class clsUtilityConfig
    {
        private static string stringRegExConnessione = @"(\<add name=""internalConnectionString"" connectionString=""DATA SOURCE=\(DESCRIPTION=\(ADDRESS=\(PROTOCOL=TCP\) \()(HOST)=([a-zA-Z0-9.]+)\) \((PORT)=(\d{4})\) \) \(CONNECT_DATA= \((SERVICE_NAME)=([a-zA-z0-9.]+)(\)\)\);USER ID=RIEPILOGHI;Password=RIEPILOGHI;Pooling=true; Min Pool Size=\d+;Max Pool Size=\d+;Connection Lifetime=\d+;Enlist=true;"" \/>)";
        private static List<string> groupNames = new List<string>() { "HOST", "PORT", "SERVICE_NAME" };

        private static string stringRegExUrlTornello = @"\<add key=""url_tornello"" value=\""http:\/\/[a-zA-z0-9]+\/api\/lta\/serviceRequest\""\/\>";
        
        private static string stringRegExCallRiepiloghiAuto = @"\<add key=""usrWebServicesRiepiloghi"" value=""[a-zA-Z0-9.]+:\/\/[a-zA-Z.]+\/api\/""\/\>";
        
        private static string stringRegExEmailAuto = @"\<add key=""{0}"" value=""[a-zA-z0-9@.]+""\/\>";

        private static string string_sendEmailReportTO = @"sendEmailReportTO";
        private static string string_sendEmailReportCC = @"sendEmailReportCC";
        private static string string_sendEmailReportCCN = @"sendEmailReportCCN";

        public static string StringRegExConnessione => stringRegExConnessione;
        public static string StringRegExUrlTornello => stringRegExUrlTornello;
        public static string StringRegExCallRiepiloghiAuto => stringRegExCallRiepiloghiAuto;

        public static string String_sendEmailReportTO => string.Format(stringRegExEmailAuto, string_sendEmailReportTO);
        public static string String_sendEmailReportCC => string.Format(stringRegExEmailAuto, string_sendEmailReportCC);
        public static string String_sendEmailReportCCN => string.Format(stringRegExEmailAuto, string_sendEmailReportCCN);

        public static bool IsMatch(string input, string pattern)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
        }

        public static List<clsItemRexEx> GetGroups(string pattern, string input)
        {
            List<clsItemRexEx> items = new List<clsItemRexEx>();
            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Match match = regEx.Match(input);
            if (match.Success)
            {
                clsItemRexEx item = null;
                bool nextIsValue = false;
                Queue<string> names = new Queue<string>(groupNames);
                foreach (System.Text.RegularExpressions.Group group in match.Groups)
                {
                    if (group.Value == input)
                        continue;

                    if (!nextIsValue && (names.Count == 0 || names.Peek() != group.Value))
                    {
                        item = new clsItemRexEx();
                        item.name = "";
                        item.value = group.Value;
                        items.Add(item);
                    }
                    else if (!nextIsValue && names.Count > 0 && names.Peek() == group.Value)
                    {
                        nextIsValue = true;
                        item = new clsItemRexEx();
                        item.name = names.Dequeue();
                        item.value = group.Value;
                        items.Add(item);
                    }
                    else if (nextIsValue)
                    {
                        nextIsValue = false;
                        item.itemValue = new clsItemRexEx();
                        item.itemValue.value = group.Value;
                        item.itemValue.index = group.Index;
                        item.itemValue.length = group.Length;
                    }
                }
            }
            return items;
        }

        public static bool CheckTryParseDecimal(string text, out decimal value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else if (text.Contains(".") && text.Contains(","))
            {
                result = decimal.TryParse(text, out value);
            }
            else if (text.Contains(".") && !text.Contains(","))
            {
                result = decimal.TryParse(text.Replace(".", ","), out value);
            }
            else
                result = decimal.TryParse(text, out value);
            return result;
        }

        public static bool CheckTryParseInt(string text, out int value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else
                result = int.TryParse(text.Replace(".", "").Replace(".", ""), out value);
            return result;
        }
    }

    public class clsItemRexEx
    {
        public string name = "";
        public string value = "";
        public int index = 0;
        public int length = 0;
        public clsItemRexEx itemValue = null;

    }

    [DebuggerStepThrough]
    public class clsStringConnectionParser
    {
        private string stringConnValue = "";
        public delegate void delegateStringMatchChanged(bool isMatch);
        public event delegateStringMatchChanged StringMatchChanged;

        public string StringConnValue
        {
            get { return this.stringConnValue; }
            set
            {
                this.stringConnValue = value;
                if (this.StringMatchChanged != null) this.StringMatchChanged(this.CheckStringValue());
            }
        }

        public bool CheckStringValue()
        {
            return clsUtilityConfig.IsMatch(this.stringConnValue, clsUtilityConfig.StringRegExConnessione);
        }

        
    }

    [DebuggerStepThrough]
    public class clsLabel : Label
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            base.OnPaint(e);
        }
    }

    [DebuggerStepThrough]
    public class clsButton : Button
    {

        public clsButton()
            :base()
        {

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            base.OnPaint(e);
        }

    }

    [DebuggerStepThrough]
    public class RadioButton : CheckBox
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            base.OnPaint(e);
        }
    }

    [DebuggerStepThrough]
    public class clsItemGenericData
    {
        public enum EnumGenericDataType : int
        {
            TypeNumInt = 1,
            TypeNumDecimal = 2,
            TypeString = 3,
            TypeDate = 4,
            TypeDateTime = 5,
            TypeOnlyLabel = 6
        }

        private object value { get; set; }

        public object Value
        {
            get
            {
                switch (this.DataType)
                {
                    case EnumGenericDataType.TypeNumInt:
                        {
                            int val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    int.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeNumDecimal:
                        {
                            decimal val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    decimal.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDate:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDateTime:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeString:
                        {
                            string val = "";
                            if (this.value != null)
                            {
                                try
                                {
                                    val = this.value.ToString();
                                }
                                catch (Exception)
                                {
                                    val = "";
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                }
                return null;
            }
            set
            {
                this.value = value;
            }
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public bool Readonly { get; set; }
        public bool Visible { get; set; }

        public string Important { get; set; }

        public EnumGenericDataType DataType { get; set; }

        public List<clsItemGenericData> ComboValues { get; set; }
        public System.Windows.Forms.ComboBoxStyle ComboStyle = System.Windows.Forms.ComboBoxStyle.DropDown;

        public bool IsComboMode => ComboValues != null && ComboValues.Count > 0;

        public clsItemGenericData(string code, string description, EnumGenericDataType dataType, object value, bool readOnly, bool visible = true, string important = "", List<clsItemGenericData> comboValues = null, System.Windows.Forms.ComboBoxStyle comboStyle = System.Windows.Forms.ComboBoxStyle.DropDown)
        {
            this.Code = code;
            this.Description = description;
            this.DataType = dataType;
            this.Value = value;
            this.Readonly = readOnly;
            this.Visible = visible;
            this.Important = important;
            this.ComboValues = comboValues;
            this.ComboStyle = comboStyle;
        }



        public static clsItemGenericData GetItemFromListByDescr(string description, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Description == description)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public static clsItemGenericData GetItemFromListBycODE(string code, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Code == code)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }
        public override string ToString()
        {
            return this.Description;
        }
    }

}
