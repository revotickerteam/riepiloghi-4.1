﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace ConfigRiepiloghi
{
    public partial class frmDataMigrazione : Form
    {

       

        public clsDataMigrazione data { get; set; }
        
        private Wintic.Data.IConnection conn { get; set; }
        public bool dataMigrazioneChanged = false;

        public frmDataMigrazione(Wintic.Data.IConnection _conn, string _dataSourceInsertOlds)
        {
            InitializeComponent();
            this.conn = _conn;
            this.data = new clsDataMigrazione();
            clsDataMigrazione.DataSourceInsertOlds = _dataSourceInsertOlds;
            this.Shown += FrmDataMigrazione_Shown;
            this.btnABORT.Click += BtnABORT_Click;
            this.btnRICALC.Click += BtnRICALC_Click;
            this.btnSAVE.Click += BtnSAVE_Click;
        }

        private void BtnSAVE_Click(object sender, EventArgs e)
        {
            if (this.data != null && this.data.DataMigrazione != null)
            {
                bool lContinue = false;
                bool lDataMigrazioneChangeTemp = false;
                DateTime GiornoInserimentoLog = this.data.DataMigrazione.Value;
                clsDataMigrazione.DataMigrazioneReale = GiornoInserimentoLog;
                clsDataMigrazione.DataMigrazioneTEMPORANEA = GiornoInserimentoLog;

                List<clsItemGenericData> dataInsert = new List<clsItemGenericData>();

                dataInsert = new List<clsItemGenericData>();
                dataInsert.Add(new clsItemGenericData("_TITLE1_", "E' possibile inserire anche i vecchi log precedenti al mese della migrazione", clsItemGenericData.EnumGenericDataType.TypeOnlyLabel, "", true, true));
                dataInsert.Add(new clsItemGenericData("_TITLE2_", "spostando temporaneamente indietro la data di migrazione", clsItemGenericData.EnumGenericDataType.TypeOnlyLabel, "", true, true));
                
                frmGetGenericData fGen = new frmGetGenericData("Parametri di inserimento", dataInsert);
                lContinue = (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK);
                fGen.Dispose();

                if (lContinue)
                {

                    dataInsert = new List<clsItemGenericData>();
                    dataInsert.Add(new clsItemGenericData("DATA_INSERT", "Data da cui partire", clsItemGenericData.EnumGenericDataType.TypeDate, GiornoInserimentoLog, false, true));
                    fGen = new frmGetGenericData("Parametri di inserimento", dataInsert);
                    lContinue = (fGen.ShowDialog() == System.Windows.Forms.DialogResult.OK);

                    if (lContinue)
                    {
                        dataInsert = fGen.Data;
                        foreach (clsItemGenericData item in dataInsert)
                        {
                            if (item.Code == "DATA_INSERT")
                            {
                                GiornoInserimentoLog = (DateTime)item.Value;
                                lContinue = (GiornoInserimentoLog.Date <= this.data.DataMigrazione.Value.Date);
                                if (!lContinue)
                                    MessageBox.Show("Data inserita superiore alla data di migrazione precedentemente impostata", "Inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    fGen.Dispose();

                    if (lContinue)
                    {
                        if (GiornoInserimentoLog.Date == this.data.DataMigrazione.Value.Date)
                        {
                            lContinue = MessageBox.Show(string.Format("Si è scelto di NON inserire log più vecchi, procedere ?\r\n{0}", this.data.DataMigrazione.Value.ToLongDateString()), "Inserimento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                        }
                        else if (GiornoInserimentoLog.Date < this.data.DataMigrazione.Value.Date)
                        {
                            lContinue = MessageBox.Show(string.Format("Si è scelto di modificare temporaneamente la data di migrazione, procedere ?\r\n{0}", GiornoInserimentoLog.ToLongDateString()), "Inserimento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                            if (lContinue)
                            {
                                lDataMigrazioneChangeTemp = true;
                            }
                        }
                    }

                    if (lContinue && lDataMigrazioneChangeTemp)
                    {
                        this.UseWaitCursor = true;
                        frmWaitRicalc = new frmWait(true);
                        frmWaitRicalc.Message = "Modifica temporanea data di migrazione...";
                        Application.DoEvents();

                        this.data.DataMigrazione = GiornoInserimentoLog;
                        clsDataMigrazione.DataMigrazioneTEMPORANEA = GiornoInserimentoLog;
                        lContinue = clsDataMigrazione.AssignData(this.conn, this.data);

                        //if (lContinue)
                        //{
                        //    frmWaitRicalc.Message = "Ricalcolo dei totali in corso, Attendere qualche minuto...";
                        //    clsDataMigrazione.SetOutMessageEvent += ClsDataMigrazione_SetOutMessageEvent;
                        //    clsDataMigrazione.CountData(this.conn, this.data);
                        //    clsDataMigrazione.SetOutMessageEvent -= ClsDataMigrazione_SetOutMessageEvent;

                        //    frmWaitRicalc.Message = "Inizializzazione...";
                        //    this.InitData();
                        //}

                        this.UseWaitCursor = false;
                        frmWaitRicalc.Close();
                        frmWaitRicalc.Dispose();
                        Application.DoEvents();

                        if (!lContinue)
                            MessageBox.Show("Si è verificato un errore.", "Inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        clsDataMigrazione.DataMigrazioneTEMPORANEA = clsDataMigrazione.DataMigrazioneReale;

                    bool result = false;
                    if (lContinue)
                    {
                        this.Enabled = false;
                        this.UseWaitCursor = true;

                        Application.DoEvents();
                        frmWaitRicalc = new frmWait(true);
                        frmWaitRicalc.Message = "Inserimento...";

                        Exception error = null;

                        //clsDataMigrazione.SetOutMessageEvent += ClsDataMigrazione_SetOutMessageEventINSERT;

                        result = clsDataMigrazione.InsertData(this.conn, out error);

                        //clsDataMigrazione.SetOutMessageEvent -= ClsDataMigrazione_SetOutMessageEventINSERT;

                        this.UseWaitCursor = false;
                        this.Enabled = true;
                        frmWaitRicalc.Close();
                        frmWaitRicalc.Dispose();
                        Application.DoEvents();

                        //if (result)
                        //{
                        //    this.InitData();
                        //}
                        //else
                        //    MessageBox.Show((error == null ? "Errore sconosciuto" : error.Message));
                    }

                    //if (lDataMigrazioneChangeTemp)
                    //{
                    //    this.data.DataMigrazione = backupDataMigrazione;
                    //    if (clsDataMigrazione.AssignData(this.conn, this.data))
                    //        this.InitData(); 
                    //}

                    if (result)
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
            }


        }

        //private void ClsDataMigrazione_SetOutMessageEventINSERT(string value)
        //{
        //    if (frmWaitRicalc != null)
        //        frmWaitRicalc.Message = value;
        //}

        private frmWait frmWaitRicalc;

        private void BtnRICALC_Click(object sender, EventArgs e)
        {
            if (this.AssegnaDataMigrazione())
            {
                bool lRicalc = (MessageBox.Show("Ricalcolo totali da inserire", "Ricalco", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
                this.UseWaitCursor = true;
                Application.DoEvents();
                frmWaitRicalc = new frmWait(true);
                frmWaitRicalc.Message = "Inizializzazione...";
                if (lRicalc)
                {
                    frmWaitRicalc.Message = "Ricalcolo dei totali in corso, Attendere qualche minuto...";
                    clsDataMigrazione.SetOutMessageEvent += ClsDataMigrazione_SetOutMessageEvent;
                    clsDataMigrazione.CalcOldTransazioni = true;
                    clsDataMigrazione.InitData(this.conn, this.data);
                    clsDataMigrazione.CalcOldTransazioni = false;
                    clsDataMigrazione.SetOutMessageEvent -= ClsDataMigrazione_SetOutMessageEvent;

                }
                frmWaitRicalc.Message = "Inizializzazione...";
                this.InitData();
                this.UseWaitCursor = false;
                Application.DoEvents();
                frmWaitRicalc.Close();
                frmWaitRicalc.Dispose();
            }
        }

        private void ClsDataMigrazione_SetOutMessageEvent(string value)
        {
            if (frmWaitRicalc != null)
            {
                frmWaitRicalc.Message = value;
            }
        }

        private bool AssegnaDataMigrazione()
        {
            this.data.DataMigrazione = this.dateTimePickerMigrazione.Value.Date;
            return clsDataMigrazione.AssignData(this.conn, this.data); 
        }

        private void BtnABORT_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void FrmDataMigrazione_Shown(object sender, EventArgs e)
        {
            this.InitData();
        }

        private void InitData()
        {
            this.UseWaitCursor = true;
            Application.DoEvents();
            this.btnABORT.Enabled = false;
            this.btnRICALC.Enabled = false;
            this.btnSAVE.Enabled = false;

            try
            {
                this.pnlCounters.Controls.Clear();
                clsDataMigrazione.InitData(this.conn, this.data);
                this.AddCounters("Data Migrazione", (this.data.DataMigrazione == null ? "non impostata" : this.data.DataMigrazione.Value.ToLongDateString()));
                this.AddCounters("Data ultima transazione vecchi log", (this.data.UltimoLogVecchio == null ? "non trovata" : this.data.UltimoLogVecchio.Value.ToLongDateString()));
                this.AddCounters("Data prima transazione nuovi log", (this.data.PrimoLogNuovo == null ? "non trovata" : this.data.PrimoLogNuovo.Value.ToLongDateString()));
                this.AddCounters("Data Ultima verifica", (this.data.DataUltimaVerifica == null ? "non eseguita" : this.data.DataUltimaVerifica.Value.ToLongDateString()));
                this.AddCounters("LOG inseriti", (this.data.LogInseriti == null ? "?" : this.data.LogInseriti.Value.ToString()));
                this.AddCounters("LTA inseriti", (this.data.LtaInseriti == null ? "?" : this.data.LtaInseriti.Value.ToString()));
                this.AddCounters("LOG da inserire", (this.data.LogDaInserire == null ? "?" : this.data.LogDaInserire.Value.ToString()));
                this.AddCounters("LTA da inserire", (this.data.LtaDaInserire == null ? "?" : this.data.LtaDaInserire.Value.ToString()));
                this.AdjustCounters();
                Application.DoEvents();

                if (this.data.DataMigrazione != null)
                    this.dateTimePickerMigrazione.Value = this.data.DataMigrazione.Value.Date;
                else
                {
                    if (this.data.PrimoLogNuovo != null)
                        this.dateTimePickerMigrazione.Value = this.data.PrimoLogNuovo.Value.Date;
                    else
                        this.dateTimePickerMigrazione.Value = DateTime.Now.Date;
                }
            }
            catch (Exception)
            {
            }

            this.btnABORT.Enabled = true;
            this.btnRICALC.Enabled = true;
            this.btnSAVE.Enabled = (this.data.DataMigrazione != null);

            this.UseWaitCursor = false;
            Application.DoEvents();
        }

        private void AdjustCounters()
        {
            int width = 0;
            foreach (Panel pnl in this.pnlCounters.Controls)
            {
                foreach (clsLabel lbl in pnl.Controls)
                {
                    if (lbl.Tag.ToString() == "DES")
                        width = (lbl.Width > width ? lbl.Width : width);
                }
            }

            foreach (Panel pnl in this.pnlCounters.Controls)
            {
                foreach (clsLabel lbl in pnl.Controls)
                {
                    if (lbl.Tag.ToString() == "DES")
                    {
                        lbl.AutoSize = false;
                        lbl.Width = width;
                    }
                }
            }
        }

        private void AddCounters(string descr, string value)
        {
            clsLabel lbl = null;

            Panel pnl = new Panel();
            pnl.Tag = this.pnlCounters.Controls.Count + 1;
            pnl.Dock = DockStyle.Top;
            pnl.BorderStyle = BorderStyle.FixedSingle;
            pnl.Height = 50;
            this.pnlCounters.Controls.Add(pnl);
            pnl.BringToFront();

            lbl = new clsLabel();
            lbl.Tag = "DES";
            lbl.Text = descr + ":";
            lbl.AutoSize = true;
            lbl.TextAlign = ContentAlignment.MiddleRight;
            lbl.Dock = DockStyle.Left;
            pnl.Controls.Add(lbl);
            lbl.BringToFront();

            lbl = new clsLabel();
            lbl.Tag = "VAL";
            lbl.Text = value;
            lbl.AutoSize = false;
            lbl.TextAlign = ContentAlignment.MiddleLeft;
            lbl.Dock = DockStyle.Fill;
            pnl.Controls.Add(lbl);
            lbl.BringToFront();
        }

        private void UpdateLastCounter(string value, bool append = false)
        {
            int lastIndex = -1;
            foreach (Panel pnl in this.pnlCounters.Controls)
            {
                if (lastIndex == -1 || ((int)pnl.Tag > lastIndex))
                    lastIndex = (int)pnl.Tag;
            }

            foreach (Panel pnl in this.pnlCounters.Controls)
            {
                if (lastIndex == ((int)pnl.Tag))
                {
                    foreach (clsLabel lbl in pnl.Controls)
                    {
                        if (lbl.Tag.ToString() == "VAL")
                        {
                            lbl.Text = value;
                        }
                    }
                    break;
                }
            }
        }
    }
}
