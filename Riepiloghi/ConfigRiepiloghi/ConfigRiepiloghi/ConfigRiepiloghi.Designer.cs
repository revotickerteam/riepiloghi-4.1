﻿
namespace ConfigRiepiloghi
{
    partial class frmConfigRiepiloghi
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            ConfigRiepiloghi.clsDataMigrazione clsDataMigrazione1 = new ConfigRiepiloghi.clsDataMigrazione();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfigRiepiloghi));
            this.pnlTnsName = new System.Windows.Forms.Panel();
            this.txtConn = new System.Windows.Forms.TextBox();
            this.lblTnsName = new ConfigRiepiloghi.clsLabel();
            this.label1 = new ConfigRiepiloghi.clsLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDataMigrazione = new ConfigRiepiloghi.frmConfigRiepiloghi.clsButtonDataMigrazione();
            this.btnConn = new ConfigRiepiloghi.clsButton();
            this.pnlConn = new System.Windows.Forms.Panel();
            this.pnlData = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnAltro = new ConfigRiepiloghi.clsButton();
            this.btnIconizza = new ConfigRiepiloghi.clsButton();
            this.btnChiudi = new ConfigRiepiloghi.clsButton();
            this.btnSave = new ConfigRiepiloghi.clsButton();
            this.timerRefreshInsert = new System.Windows.Forms.Timer(this.components);
            this.txtInsertLogs = new System.Windows.Forms.TextBox();
            this.pnlTnsName.SuspendLayout();
            this.pnlConn.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTnsName
            // 
            this.pnlTnsName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTnsName.Controls.Add(this.txtConn);
            this.pnlTnsName.Controls.Add(this.lblTnsName);
            this.pnlTnsName.Controls.Add(this.label1);
            this.pnlTnsName.Controls.Add(this.panel1);
            this.pnlTnsName.Controls.Add(this.btnDataMigrazione);
            this.pnlTnsName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTnsName.Location = new System.Drawing.Point(3, 3);
            this.pnlTnsName.Name = "pnlTnsName";
            this.pnlTnsName.Padding = new System.Windows.Forms.Padding(3);
            this.pnlTnsName.Size = new System.Drawing.Size(643, 140);
            this.pnlTnsName.TabIndex = 0;
            // 
            // txtConn
            // 
            this.txtConn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtConn.Location = new System.Drawing.Point(108, 45);
            this.txtConn.Name = "txtConn";
            this.txtConn.Size = new System.Drawing.Size(530, 27);
            this.txtConn.TabIndex = 1;
            this.txtConn.Text = "SERVERX";
            // 
            // lblTnsName
            // 
            this.lblTnsName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTnsName.Location = new System.Drawing.Point(3, 45);
            this.lblTnsName.Name = "lblTnsName";
            this.lblTnsName.Size = new System.Drawing.Size(105, 22);
            this.lblTnsName.TabIndex = 0;
            this.lblTnsName.Text = "Connessione:";
            this.lblTnsName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(635, 42);
            this.label1.TabIndex = 2;
            this.label1.Text = "Configurazione paramerti Riepiloghi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(635, 11);
            this.panel1.TabIndex = 4;
            // 
            // btnDataMigrazione
            // 
            this.btnDataMigrazione.Connesso = false;
            clsDataMigrazione1.DataMigrazione = null;
            clsDataMigrazione1.DataUltimaVerifica = null;
            clsDataMigrazione1.LogInseriti = null;
            clsDataMigrazione1.LtaInseriti = null;
            clsDataMigrazione1.PrimoLogNuovo = null;
            clsDataMigrazione1.UltimoLogVecchio = null;
            this.btnDataMigrazione.data = clsDataMigrazione1;
            this.btnDataMigrazione.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDataMigrazione.Enabled = false;
            this.btnDataMigrazione.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDataMigrazione.Location = new System.Drawing.Point(3, 78);
            this.btnDataMigrazione.Name = "btnDataMigrazione";
            this.btnDataMigrazione.Size = new System.Drawing.Size(635, 57);
            this.btnDataMigrazione.TabIndex = 3;
            this.btnDataMigrazione.Text = "Data Migrazione/Inizio";
            this.btnDataMigrazione.UseVisualStyleBackColor = true;
            // 
            // btnConn
            // 
            this.btnConn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConn.Location = new System.Drawing.Point(646, 3);
            this.btnConn.Name = "btnConn";
            this.btnConn.Size = new System.Drawing.Size(133, 140);
            this.btnConn.TabIndex = 2;
            this.btnConn.Text = "Connetti";
            this.btnConn.UseVisualStyleBackColor = true;
            // 
            // pnlConn
            // 
            this.pnlConn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConn.Controls.Add(this.pnlTnsName);
            this.pnlConn.Controls.Add(this.btnConn);
            this.pnlConn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlConn.Location = new System.Drawing.Point(0, 0);
            this.pnlConn.Name = "pnlConn";
            this.pnlConn.Padding = new System.Windows.Forms.Padding(3);
            this.pnlConn.Size = new System.Drawing.Size(784, 148);
            this.pnlConn.TabIndex = 3;
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 148);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(784, 357);
            this.pnlData.TabIndex = 4;
            this.pnlData.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnAltro);
            this.pnlBottom.Controls.Add(this.btnIconizza);
            this.pnlBottom.Controls.Add(this.btnChiudi);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 505);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Padding = new System.Windows.Forms.Padding(3);
            this.pnlBottom.Size = new System.Drawing.Size(784, 56);
            this.pnlBottom.TabIndex = 5;
            this.pnlBottom.Visible = false;
            // 
            // btnAltro
            // 
            this.btnAltro.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAltro.Location = new System.Drawing.Point(275, 3);
            this.btnAltro.Name = "btnAltro";
            this.btnAltro.Size = new System.Drawing.Size(136, 48);
            this.btnAltro.TabIndex = 6;
            this.btnAltro.Text = "Altro";
            this.btnAltro.UseVisualStyleBackColor = true;
            this.btnAltro.Click += new System.EventHandler(this.btnAltro_Click);
            // 
            // btnIconizza
            // 
            this.btnIconizza.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnIconizza.Location = new System.Drawing.Point(139, 3);
            this.btnIconizza.Name = "btnIconizza";
            this.btnIconizza.Size = new System.Drawing.Size(136, 48);
            this.btnIconizza.TabIndex = 5;
            this.btnIconizza.Text = "Iconizza";
            this.btnIconizza.UseVisualStyleBackColor = true;
            // 
            // btnChiudi
            // 
            this.btnChiudi.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnChiudi.Location = new System.Drawing.Point(3, 3);
            this.btnChiudi.Name = "btnChiudi";
            this.btnChiudi.Size = new System.Drawing.Size(136, 48);
            this.btnChiudi.TabIndex = 4;
            this.btnChiudi.Text = "Abbandona";
            this.btnChiudi.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Location = new System.Drawing.Point(643, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(136, 48);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Conferma";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // timerRefreshInsert
            // 
            this.timerRefreshInsert.Interval = 10000;
            // 
            // txtInsertLogs
            // 
            this.txtInsertLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInsertLogs.Location = new System.Drawing.Point(0, 148);
            this.txtInsertLogs.Multiline = true;
            this.txtInsertLogs.Name = "txtInsertLogs";
            this.txtInsertLogs.ReadOnly = true;
            this.txtInsertLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInsertLogs.Size = new System.Drawing.Size(784, 357);
            this.txtInsertLogs.TabIndex = 7;
            this.txtInsertLogs.Visible = false;
            // 
            // frmConfigRiepiloghi
            // 
            this.AcceptButton = this.btnConn;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.ControlBox = false;
            this.Controls.Add(this.txtInsertLogs);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlConn);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConfigRiepiloghi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurazione Riepiloghi";
            this.pnlTnsName.ResumeLayout(false);
            this.pnlTnsName.PerformLayout();
            this.pnlConn.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTnsName;
        private System.Windows.Forms.TextBox txtConn;
        private clsLabel lblTnsName;
        private clsButton btnConn;
        private System.Windows.Forms.Panel pnlConn;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.Panel pnlBottom;
        private clsButton btnChiudi;
        private clsButton btnSave;
        private clsLabel label1;
        private System.Windows.Forms.Panel panel1;
        private ConfigRiepiloghi.frmConfigRiepiloghi.clsButtonDataMigrazione btnDataMigrazione;
        private clsButton btnIconizza;
        private System.Windows.Forms.Timer timerRefreshInsert;
        private System.Windows.Forms.TextBox txtInsertLogs;
        private clsButton btnAltro;
    }
}

