﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigRiepiloghi
{
    public partial class frmStatoRiepiloghi : Form
    {
        Wintic.Data.IConnection conn = null;
        public class clsDataGridRiepiloghi : DataGridView
        {
            public clsComparerRiepiloghi comparer { get; set; }
            protected override void OnCellPainting(DataGridViewCellPaintingEventArgs e)
            {
                base.OnCellPainting(e);
                if (e.RowIndex == -1 && e.ColumnIndex >= 0)
                {
                    if (this.comparer != null && this.comparer.propertyName == this.Columns[e.ColumnIndex].DataPropertyName)
                    {
                        GraphicsPath p = new GraphicsPath();
                        float dim = 10;
                        RectangleF r = new RectangleF(e.CellBounds.Left + e.CellBounds.Width - dim - 3, e.CellBounds.Top + (e.CellBounds.Height - dim) / 2, dim, dim);
                        if (this.comparer.descend)
                        {
                            p.AddLine(new PointF(r.Left, r.Top), new PointF(r.Right, r.Top));
                            p.AddLine(p.GetLastPoint(), new PointF(p.GetLastPoint().X - (r.Width / 2), r.Bottom));
                        }
                        else
                        {
                            p.AddLine(new PointF(r.Left, r.Bottom), new PointF(r.Right, r.Bottom));
                            p.AddLine(p.GetLastPoint(), new PointF(p.GetLastPoint().X - (r.Width / 2), r.Top));
                        }
                        p.CloseFigure();
                        e.Graphics.FillPath(Brushes.Black, p);
                        e.Handled = true;
                    }
                    else
                        e.Handled = false;
                }
            }

            public class clsComparerRiepiloghi : System.Collections.Generic.IComparer<clsRiepilogo>
            {
                public string propertyName = "";
                public bool descend = false;
                
                public clsComparerRiepiloghi()
                {
                }
                public clsComparerRiepiloghi(string PropertyName, bool Descend = false)
                    : base()
                {
                    this.propertyName = PropertyName;
                    this.descend = Descend;
                }
                

                public int Compare(clsRiepilogo x, clsRiepilogo y)
                {
                    string s1 = "";
                    string s2 = "";
                    long? n1 = null;
                    long? n2 = null;
                    DateTime? d1 = null;
                    DateTime? d2 = null;
                    switch (this.propertyName)
                    {
                        case "GIORNO":
                            {
                                d1 = x.GIORNO; d2 = y.GIORNO;
                                break;
                            }
                        case "DATA_INS":
                            {
                                d1 = x.DATA_INS; d2 = y.DATA_INS;
                                break;
                            }
                        case "DATA_GENERAZIONE":
                            {
                                d1 = x.DATA_GENERAZIONE; d2 = y.DATA_GENERAZIONE;
                                break;
                            }
                        case "EMAIL_SEND_LAST":
                            {
                                d1 = x.EMAIL_SEND_LAST; d2 = y.EMAIL_SEND_LAST;
                                break;
                            }
                        case "EMAIL_SENT_DATE":
                            {
                                d1 = x.EMAIL_SENT_DATE; d2 = y.EMAIL_SENT_DATE;
                                break;
                            }
                        case "EMAIL_READ_LAST":
                            {
                                d1 = x.EMAIL_READ_LAST; d2 = y.EMAIL_READ_LAST;
                                break;
                            }
                        case "EMAIL_RETURN_DATE":
                            {
                                d1 = x.EMAIL_RETURN_DATE; d2 = y.EMAIL_RETURN_DATE;
                                break;
                            }
                        case "TIPO":
                            {
                                s1 = x.TIPO; s2 = y.TIPO;
                                break;
                            }
                        case "NOME":
                            {
                                s1 = x.NOME; s2 = y.NOME;
                                break;
                            }
                        case "EMAIL_SEND_ERROR":
                            {
                                s1 = x.EMAIL_SEND_ERROR; s2 = y.EMAIL_SEND_ERROR;
                                break;
                            }
                        case "EMAIL_READ_ERROR":
                            {
                                s1 = x.EMAIL_READ_ERROR; s2 = y.EMAIL_READ_ERROR;
                                break;
                            }
                        case "EMAIL_RETURN_CODE":
                            {
                                s1 = x.EMAIL_RETURN_CODE; s2 = y.EMAIL_RETURN_CODE;
                                break;
                            }
                        case "EMAIL_RETURN_BODY":
                            {
                                s1 = x.EMAIL_RETURN_BODY; s2 = y.EMAIL_RETURN_BODY;
                                break;
                            }
                        case "PROG":
                            {
                                n1 = x.PROG; n2 = y.PROG;
                                break;
                            }
                        case "MAX_LOG_ID":
                            {
                                n1 = x.MAX_LOG_ID; n2 = y.MAX_LOG_ID;
                                break;
                            }
                    }
                    int result = 0;
                    if (d1 != null && d2 != null)
                    {
                        if (d1 > d2) result = 1;
                        else if (d1 < d2) result = -1;
                        else result = 0;
                    }
                    else if (n1 != null && n2 != null)
                    {
                        if (n1 > n2) result = 1;
                        else if (n1 < n2) result = -1;
                        else result = 0;
                    }
                    else if (s1 != null && s2 != null)
                    {
                        result = s1.CompareTo(s2);
                    }
                    else result = 0;
                    if (descend && result != 0)
                    {
                        if (result == 1) result = -1;
                        else result = 1;
                    }
                    return result;
                }
            }

        }
        public class clsRiepilogo
        {
            [DisplayName("Data Competenza Riepilogo")]
            public DateTime GIORNO { get; set; }
            [DisplayName("Tipo Riepilogo")]
            public string TIPO { get; set; }
            [DisplayName("Nome del File")]
            public string NOME { get; set; }
            [DisplayName("Progressivo Generazione")]
            public long? PROG { get; set; }
            public long? MAX_LOG_ID { get; set; }
            [DisplayName("Data/Ora Inserimento nel DB")]
            public DateTime? DATA_INS { get; set; }
            [DisplayName("Data/Ora Generazione")]
            public DateTime? DATA_GENERAZIONE { get; set; }
            [DisplayName("Data/Ora ultimo tentativo di Invio")]
            public DateTime? EMAIL_SEND_LAST { get; set; }
            [DisplayName("Errore durante l'Invio")]
            public string EMAIL_SEND_ERROR { get; set; }
            [DisplayName("Data/Ora Inviato")]
            public DateTime? EMAIL_SENT_DATE { get; set; }
            [DisplayName("Data/Ora ultimo tentativo di lettura della risposta")]
            public DateTime? EMAIL_READ_LAST { get; set; }
            [DisplayName("Errore nella lettura della risposta")]
            public string EMAIL_READ_ERROR { get; set; }
            [DisplayName("Data/Ora Ricezione risposta")]
            public DateTime? EMAIL_RETURN_DATE { get; set; }
            [DisplayName("Codice Risposta")]
            public string EMAIL_RETURN_CODE { get; set; }
            [DisplayName("Descrizione Risposta")]
            public string EMAIL_RETURN_BODY { get; set; }

            public clsRiepilogo()
            { }

            public clsRiepilogo(Wintic.Data.IRecordSet oRS)
                :this()
            {
                this.GIORNO = (DateTime)oRS.Fields("GIORNO").Value;
                this.TIPO = (!oRS.Fields("TIPO").IsNull ? oRS.Fields("TIPO").Value.ToString() : "");
                this.NOME = (!oRS.Fields("NOME").IsNull ? oRS.Fields("NOME").Value.ToString() : "");
                this.PROG = (!oRS.Fields("PROG").IsNull ? long.Parse(oRS.Fields("PROG").Value.ToString()) : 0);
                this.MAX_LOG_ID = (!oRS.Fields("MAX_LOG_ID").IsNull ? long.Parse(oRS.Fields("MAX_LOG_ID").Value.ToString()) : 0);
                this.DATA_INS = (DateTime)oRS.Fields("DATA_INS").Value;
                if (!oRS.Fields("DATA_GENERAZIONE").IsNull)
                    this.DATA_GENERAZIONE = (DateTime)oRS.Fields("DATA_GENERAZIONE").Value;
                if (!oRS.Fields("EMAIL_SEND_LAST").IsNull)
                    this.EMAIL_SEND_LAST = (DateTime)oRS.Fields("EMAIL_SEND_LAST").Value;
                this.EMAIL_SEND_ERROR = (!oRS.Fields("EMAIL_SEND_ERROR").IsNull ? oRS.Fields("EMAIL_SEND_ERROR").Value.ToString() : "");
                if (!oRS.Fields("EMAIL_SENT_DATE").IsNull)
                    this.EMAIL_SENT_DATE = (DateTime)oRS.Fields("EMAIL_SENT_DATE").Value;
                if (!oRS.Fields("EMAIL_READ_LAST").IsNull)
                this.EMAIL_READ_LAST = (DateTime)oRS.Fields("EMAIL_READ_LAST").Value;
                this.EMAIL_READ_ERROR = (!oRS.Fields("EMAIL_READ_ERROR").IsNull ? oRS.Fields("EMAIL_READ_ERROR").Value.ToString() : "");
                if (!oRS.Fields("EMAIL_RETURN_DATE").IsNull)
                    this.EMAIL_RETURN_DATE = (DateTime)oRS.Fields("EMAIL_RETURN_DATE").Value;
                this.EMAIL_RETURN_CODE = (!oRS.Fields("EMAIL_RETURN_CODE").IsNull ? oRS.Fields("EMAIL_RETURN_CODE").Value.ToString() : "");
                this.EMAIL_RETURN_BODY = (!oRS.Fields("EMAIL_RETURN_BODY").IsNull ? oRS.Fields("EMAIL_RETURN_BODY").Value.ToString() : "");

            }
        }

        public List<clsRiepilogo> riepiloghi { get; set; }
        public BindingSource Data;
        private clsRiepilogo riepilogoSelected { get; set; }

        public frmStatoRiepiloghi(Wintic.Data.IConnection _conn)
        {
            InitializeComponent();
            this.conn = _conn;
            this.Shown += FrmStatoRiepiloghi_Shown;
        }

        private void FrmStatoRiepiloghi_Shown(object sender, EventArgs e)
        {
            this.dtDAL.Value = DateTime.Now.Date.AddDays(-1);
            this.dtAL.Value = DateTime.Now.Date;
            this.btnEstrai.Click += BtnEstrai_Click;
            this.dgv.SelectionChanged += Dgv_SelectionChanged;
            this.dgv.DataBindingComplete += Dgv_DataBindingComplete;
            this.dgv.ColumnHeaderMouseClick += Dgv_ColumnHeaderMouseClick;
            this.btnFORCE_RECEIVED.Click += BtnFORCE_RECEIVED_Click;
            this.chkSEND_NULL.CheckedChanged += ChkFilters_CheckedChanged;
            this.chkRETURN_NULL.CheckedChanged += ChkFilters_CheckedChanged;
            this.chkRETURN_ERROR.CheckedChanged += ChkFilters_CheckedChanged;
        }

        
        private void Dgv_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (this.dgv.DataSource != null)
            {
                
                string property = this.dgv.Columns[e.ColumnIndex].DataPropertyName;
                if (this.dgv.comparer == null || this.dgv.comparer.propertyName != property)
                {
                    if (this.dgv.comparer != null)
                    {
                        this.dgv.comparer.propertyName = "";
                        this.dgv.Invalidate();
                        this.dgv.Refresh();
                        Application.DoEvents();
                    }
                    this.dgv.comparer = new clsDataGridRiepiloghi.clsComparerRiepiloghi(property);
                    this.riepiloghi.Sort(this.dgv.comparer);
                }
                else
                {
                    if (this.dgv.comparer != null)
                    {
                        this.dgv.comparer.propertyName = "";
                        this.dgv.Invalidate();
                        this.dgv.Refresh();
                        Application.DoEvents();
                    }
                    this.dgv.comparer = new clsDataGridRiepiloghi.clsComparerRiepiloghi(property, true);
                    this.riepiloghi.Sort(this.dgv.comparer);
                }
                this.dgv.DataSource = this.riepiloghi;
                this.dgv.Invalidate();
                this.dgv.Refresh();
                Application.DoEvents();
            }
        }


        private void ChkFilters_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.Enabled && checkBox.Checked)
            {
                List<CheckBox> lista = new List<CheckBox>() { this.chkSEND_NULL, this.chkRETURN_NULL, this.chkRETURN_ERROR };
                lista.ForEach(c => c.Enabled = false);
                lista.ForEach(c =>
                {
                    if (!checkBox.Equals(c))
                        c.Checked = false;
                });
                lista.ForEach(c => c.Enabled = true);
            }
        }

        private void Dgv_SelectionChanged(object sender, EventArgs e)
        {
            object VALUE = null;
            this.RiepilogoSelected = null;
            try
            {
                if (this.dgv.SelectedCells.Count > 0)
                {
                    VALUE = this.dgv.SelectedCells[0].Value;
                    try
                    {
                        this.RiepilogoSelected = (clsRiepilogo)this.dgv.SelectedCells[0].OwningRow.DataBoundItem;
                    }
                    catch (Exception)
                    {
                    }
                    
                }
            }
            catch (Exception)
            {
                VALUE = null;
            }
            this.lblCELL.Text = (VALUE == null ? "" : VALUE.ToString());
        }

        private void BtnEstrai_Click(object sender, EventArgs e)
        {
            InitData();
        }

        private void setWait(bool visible)
        {
            if (this.lblWait.Width < this.panelDATA.ClientSize.Width)
            {
                this.lblWait.Size = this.panelDATA.ClientSize;
                this.lblWait.Location = new Point(0, 0);
            }
            if (visible)
            {
                lblWait.Visible = true;
                this.lblWait.BringToFront();
            }
            else
                lblWait.Visible = false;
            Application.DoEvents();
        }
        private void InitData()
        {
            this.setWait(true);
            this.RiepilogoSelected = null;
            this.dgv.comparer = null;
            this.riepiloghi = new List<clsRiepilogo>();
            this.Data = new BindingSource();
            try
            {
                StringBuilder oSB = new StringBuilder();
                Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();
                oSB.Append("SELECT ");
                oSB.Append(" GIORNO,");
                oSB.Append(" TIPO,");
                oSB.Append(" NOME,");
                oSB.Append(" PROG,");
                oSB.Append(" MAX_LOG_ID,");
                oSB.Append(" DATA_INS,");
                oSB.Append(" DATA_GENERAZIONE,");
                oSB.Append(" EMAIL_SEND_LAST,");
                oSB.Append(" EMAIL_SEND_ERROR,");
                oSB.Append(" EMAIL_SENT_DATE,");
                oSB.Append(" EMAIL_READ_LAST,");
                oSB.Append(" EMAIL_READ_ERROR,");
                oSB.Append(" EMAIL_RETURN_DATE,");
                oSB.Append(" EMAIL_RETURN_CODE,");
                oSB.Append(" EMAIL_RETURN_BODY ");
                oSB.Append(" FROM ");
                oSB.Append(" RIEPILOGHI_PROGS");
                oSB.Append(" WHERE GIORNO BETWEEN :pINIZIO AND :pFINE");

                oPars.Add(":pINIZIO", this.dtDAL.Value.Date);
                oPars.Add(":pFINE", this.dtAL.Value.Date);

                List<string> tipi = new List<string>();
                if (this.chkRPG.Checked) tipi.Add("G");
                if (this.chkRPM.Checked) tipi.Add("M");
                if (this.chkRCA.Checked) tipi.Add("R");

                if (tipi.Count == 1)
                    oSB.Append(string.Format(" AND TIPO = '{0}'", tipi[0]));
                else
                {
                    string tipiString = "";
                    tipi.ForEach(t => tipiString += (string.IsNullOrEmpty(tipiString) ? "" : ",") + "'" + t + "'");
                    oSB.Append(string.Format(" AND TIPO IN ({0})", tipiString));
                }
                if (this.chkSEND_NULL.Checked)
                    oSB.Append(" AND EMAIL_SENT_DATE IS NULL");
                if (this.chkRETURN_NULL.Checked)
                    oSB.Append(" AND EMAIL_RETURN_DATE IS NULL");
                if (this.chkRETURN_ERROR.Checked)
                    oSB.Append(" AND EMAIL_RETURN_CODE IS NOT NULL AND EMAIL_RETURN_CODE <> '0000'");

                if (this.chkOnlyExistsResponse.Checked)
                {
                    oSB.Append(" AND EXISTS (");
                    oSB.Append(" SELECT NULL FROM RIEPILOGHI_PROGS X WHERE RIEPILOGHI_PROGS.TIPO = X.TIPO AND RIEPILOGHI_PROGS.GIORNO = X.GIORNO AND RIEPILOGHI_PROGS.PROG < X.PROG");
                    if (this.chkSEND_NULL.Checked)
                        oSB.Append(" AND X.EMAIL_SENT_DATE IS NOT NULL");
                    if (this.chkRETURN_NULL.Checked)
                        oSB.Append(" AND X.EMAIL_RETURN_DATE IS NOT NULL");
                    if (this.chkRETURN_ERROR.Checked)
                        oSB.Append(" AND X.EMAIL_RETURN_CODE IS NOT NULL AND X.EMAIL_RETURN_CODE = '0000'");
                    oSB.Append(" )");
                }

                oSB.Append(" ORDER BY");
                oSB.Append(" GIORNO DESC,");
                oSB.Append(" TIPO,");
                oSB.Append(" PROG");

                Wintic.Data.IRecordSet oRS = (Wintic.Data.IRecordSet)this.conn.ExecuteQuery(oSB.ToString(), oPars);
                while (!oRS.EOF)
                {
                    clsRiepilogo riepilogo = new clsRiepilogo(oRS);
                    this.riepiloghi.Add(riepilogo);
                    oRS.MoveNext();
                }
                oRS.Close();

                this.Data.DataSource = riepiloghi;
                this.dgv.DataSource = this.Data;
                this.dgv.Invalidate();
                this.dgv.Refresh();
                Application.DoEvents();
            }
            catch (Exception)
            {
            }
            this.setWait(false);
        }


        private void Dgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            try
            {

                if (this.Data != null && this.Data.DataSource != null && this.dgv.DataSource != null)
                {
                    List<string> fieldsWrap = new List<string>() { "EMAIL_SEND_ERROR", "EMAIL_READ_ERROR", "EMAIL_RETURN_BODY" };
                    foreach (string field in fieldsWrap)
                    {
                        this.dgv.Columns[field].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    }

                    List<string> fieldsWarning = new List<string>() { "EMAIL_SEND_ERROR", "EMAIL_READ_ERROR", "EMAIL_RETURN_BODY" };
                    //List<string> fieldsError = new List<string>() { "EMAIL_SEND_ERROR", "EMAIL_READ_ERROR", "EMAIL_RETURN_CODE" };

                    foreach (DataGridViewRow row in this.dgv.Rows)
                    {
                        clsRiepilogo riepilogo = (clsRiepilogo)row.DataBoundItem;
                        Color rowColor = Color.Transparent;
                        long errorCode = 0;
                        if (!string.IsNullOrEmpty(riepilogo.EMAIL_READ_ERROR) || !string.IsNullOrEmpty(riepilogo.EMAIL_READ_ERROR) || (riepilogo.EMAIL_RETURN_CODE != "" && long.TryParse(riepilogo.EMAIL_RETURN_CODE, out errorCode) && errorCode > 0))
                            rowColor = Color.Red;
                        else if (riepilogo.EMAIL_SENT_DATE == null)
                            rowColor = Color.Yellow;
                        else if (riepilogo.EMAIL_SENT_DATE == null || riepilogo.EMAIL_RETURN_DATE == null)
                            rowColor = Color.Orange;

                        if (rowColor != Color.Transparent)
                        {
                            row.DefaultCellStyle.BackColor = rowColor;
                            //foreach (DataGridViewCell celll in row.Cells)
                            //    celll.Style.BackColor = rowColor;
                        }
                    }
                }
            }
            catch (Exception)
            {

                
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private clsRiepilogo RiepilogoSelected
        {
            get { return this.riepilogoSelected; }
            set
            {
                this.btnFORCE_RECEIVED.Enabled = false;
                this.riepilogoSelected = value;
                if (this.riepilogoSelected != null)
                {
                    this.btnFORCE_RECEIVED.Enabled = (this.riepilogoSelected.EMAIL_SENT_DATE != null &&
                                                      string.IsNullOrEmpty(this.riepilogoSelected.EMAIL_SEND_ERROR) &&
                                                      this.riepilogoSelected.EMAIL_RETURN_DATE == null);
                }
            }
        }

        private void BtnFORCE_RECEIVED_Click(object sender, EventArgs e)
        {
            if (this.riepilogoSelected != null &&
                this.riepilogoSelected.EMAIL_SENT_DATE != null &&
                string.IsNullOrEmpty(this.riepilogoSelected.EMAIL_SEND_ERROR) &&
                this.riepilogoSelected.EMAIL_RETURN_DATE == null)
            {
                if (MessageBox.Show("Impostare il riepilogo come se fosse stata ricevuta l'email ?","Email non ricevuta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DateTime GIORNO = this.riepilogoSelected.GIORNO;
                    string TIPO = this.riepilogoSelected.TIPO;
                    long PROG = this.riepilogoSelected.PROG.Value;
                    List<clsItemGenericData> items = new List<clsItemGenericData>();
                    items.Add(new clsItemGenericData("DATA_ORA", "Data/Ora di ricezione", clsItemGenericData.EnumGenericDataType.TypeDateTime, DateTime.Now, false, true));
                    items.Add(new clsItemGenericData("RISPOSTA", "Risposta", clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                    frmGetGenericData fGen = new frmGetGenericData("Impostare il riepilogo come se fosse stata ricevuta l'email", items);
                    DateTime? dataOra = null;
                    string messaggio = "";
                    if (fGen.ShowDialog() == DialogResult.OK)
                    {
                        foreach (clsItemGenericData item in fGen.Data)
                        {
                            if (item.Code == "DATA_ORA")
                                dataOra = (DateTime)item.Value;
                            else if (item.Code == "RISPOSTA")
                                messaggio = item.Value.ToString();
                        }
                    }

                    fGen.Close();
                    fGen.Dispose();

                    if (dataOra != null && !string.IsNullOrEmpty(messaggio) && !string.IsNullOrWhiteSpace(messaggio))
                    {
                        try
                        {
                            this.conn.BeginTransaction();
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("UPDATE WTIC.RIEPILOGHI_PROGS");
                            oSB.Append(" SET ");
                            oSB.Append(" EMAIL_RETURN_DATE = :pEMAIL_RETURN_DATE,");
                            oSB.Append(" EMAIL_RETURN_BODY = :pEMAIL_RETURN_BODY,");
                            oSB.Append(" EMAIL_RETURN_CODE = 0");
                            oSB.Append(" WHERE GIORNO = :pGIORNO");
                            oSB.Append("   AND TIPO = :pTIPO");
                            oSB.Append("   AND PROG = :pPROG");
                            oSB.Append("   AND EMAIL_RETURN_DATE IS NULL");
                            Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();
                            oPars.Add(":pEMAIL_RETURN_DATE", dataOra);
                            oPars.Add(":pEMAIL_RETURN_BODY", messaggio);
                            oPars.Add(":pGIORNO", GIORNO);
                            oPars.Add(":pTIPO", TIPO);
                            oPars.Add(":pPROG", PROG);
                            decimal updated = this.conn.ExecuteNonQuery(oSB.ToString(), oPars);
                            if (updated == 1)
                                this.conn.EndTransaction();
                            else
                                throw new Exception(string.Format("Aggiornate {0} righe operazione abbandonata", updated.ToString()));
                        }
                        catch (Exception ex)
                        {
                            this.conn.RollBack();
                            MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

    }
}
