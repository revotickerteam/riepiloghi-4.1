﻿
namespace ConfigRiepiloghi
{
    partial class frmStatoRiepiloghi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStatoRiepiloghi));
            this.panelDATA = new System.Windows.Forms.Panel();
            this.lblWait = new ConfigRiepiloghi.clsLabel();
            this.dgv = new ConfigRiepiloghi.frmStatoRiepiloghi.clsDataGridRiepiloghi();
            this.panelBOT = new System.Windows.Forms.Panel();
            this.panelBUTTONS = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFORCE_RECEIVED = new ConfigRiepiloghi.clsButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.lblCELL = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelFILTERS = new System.Windows.Forms.FlowLayoutPanel();
            this.chkRPG = new System.Windows.Forms.CheckBox();
            this.chkRPM = new System.Windows.Forms.CheckBox();
            this.chkRCA = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtDAL = new System.Windows.Forms.DateTimePicker();
            this.lblDAL = new ConfigRiepiloghi.clsLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtAL = new System.Windows.Forms.DateTimePicker();
            this.lblAL = new ConfigRiepiloghi.clsLabel();
            this.chkSEND_NULL = new System.Windows.Forms.CheckBox();
            this.chkRETURN_NULL = new System.Windows.Forms.CheckBox();
            this.chkRETURN_ERROR = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnEstrai = new ConfigRiepiloghi.clsButton();
            this.chkOnlyExistsResponse = new System.Windows.Forms.CheckBox();
            this.panelDATA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelBOT.SuspendLayout();
            this.panelBUTTONS.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelFILTERS.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelDATA
            // 
            this.panelDATA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDATA.Controls.Add(this.lblWait);
            this.panelDATA.Controls.Add(this.dgv);
            this.panelDATA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDATA.Location = new System.Drawing.Point(0, 103);
            this.panelDATA.Name = "panelDATA";
            this.panelDATA.Size = new System.Drawing.Size(1007, 375);
            this.panelDATA.TabIndex = 1;
            // 
            // lblWait
            // 
            this.lblWait.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWait.Location = new System.Drawing.Point(0, 0);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(52, 13);
            this.lblWait.TabIndex = 1;
            this.lblWait.Text = "Attendere...";
            this.lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWait.Visible = false;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.comparer = null;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowHeadersWidth = 62;
            this.dgv.Size = new System.Drawing.Size(1005, 373);
            this.dgv.TabIndex = 0;
            // 
            // panelBOT
            // 
            this.panelBOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBOT.Controls.Add(this.panelBUTTONS);
            this.panelBOT.Controls.Add(this.splitter1);
            this.panelBOT.Controls.Add(this.lblCELL);
            this.panelBOT.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBOT.Location = new System.Drawing.Point(0, 491);
            this.panelBOT.Name = "panelBOT";
            this.panelBOT.Size = new System.Drawing.Size(1007, 121);
            this.panelBOT.TabIndex = 2;
            // 
            // panelBUTTONS
            // 
            this.panelBUTTONS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBUTTONS.Controls.Add(this.btnFORCE_RECEIVED);
            this.panelBUTTONS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBUTTONS.Location = new System.Drawing.Point(277, 0);
            this.panelBUTTONS.Name = "panelBUTTONS";
            this.panelBUTTONS.Size = new System.Drawing.Size(728, 119);
            this.panelBUTTONS.TabIndex = 2;
            // 
            // btnFORCE_RECEIVED
            // 
            this.btnFORCE_RECEIVED.Enabled = false;
            this.btnFORCE_RECEIVED.Location = new System.Drawing.Point(3, 3);
            this.btnFORCE_RECEIVED.Name = "btnFORCE_RECEIVED";
            this.btnFORCE_RECEIVED.Size = new System.Drawing.Size(134, 65);
            this.btnFORCE_RECEIVED.TabIndex = 0;
            this.btnFORCE_RECEIVED.Text = "Imposta come ricevuto con risposta";
            this.btnFORCE_RECEIVED.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Location = new System.Drawing.Point(264, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(13, 119);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // lblCELL
            // 
            this.lblCELL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCELL.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCELL.Location = new System.Drawing.Point(0, 0);
            this.lblCELL.Name = "lblCELL";
            this.lblCELL.Size = new System.Drawing.Size(264, 119);
            this.lblCELL.TabIndex = 0;
            this.lblCELL.Text = "...";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 478);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1007, 13);
            this.splitter2.TabIndex = 5;
            this.splitter2.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 90);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1007, 13);
            this.splitter3.TabIndex = 6;
            this.splitter3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panelFILTERS);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1007, 90);
            this.panel3.TabIndex = 7;
            // 
            // panelFILTERS
            // 
            this.panelFILTERS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFILTERS.Controls.Add(this.chkRPG);
            this.panelFILTERS.Controls.Add(this.chkRPM);
            this.panelFILTERS.Controls.Add(this.chkRCA);
            this.panelFILTERS.Controls.Add(this.panel2);
            this.panelFILTERS.Controls.Add(this.panel1);
            this.panelFILTERS.Controls.Add(this.chkSEND_NULL);
            this.panelFILTERS.Controls.Add(this.chkRETURN_NULL);
            this.panelFILTERS.Controls.Add(this.chkRETURN_ERROR);
            this.panelFILTERS.Controls.Add(this.chkOnlyExistsResponse);
            this.panelFILTERS.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFILTERS.Location = new System.Drawing.Point(0, 0);
            this.panelFILTERS.Margin = new System.Windows.Forms.Padding(2);
            this.panelFILTERS.Name = "panelFILTERS";
            this.panelFILTERS.Padding = new System.Windows.Forms.Padding(5);
            this.panelFILTERS.Size = new System.Drawing.Size(946, 89);
            this.panelFILTERS.TabIndex = 1;
            // 
            // chkRPG
            // 
            this.chkRPG.BackColor = System.Drawing.Color.Gainsboro;
            this.chkRPG.Checked = true;
            this.chkRPG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRPG.Location = new System.Drawing.Point(8, 8);
            this.chkRPG.Name = "chkRPG";
            this.chkRPG.Size = new System.Drawing.Size(55, 29);
            this.chkRPG.TabIndex = 0;
            this.chkRPG.Text = "RPG";
            this.chkRPG.UseVisualStyleBackColor = false;
            // 
            // chkRPM
            // 
            this.chkRPM.BackColor = System.Drawing.Color.Gainsboro;
            this.chkRPM.Checked = true;
            this.chkRPM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRPM.Location = new System.Drawing.Point(69, 8);
            this.chkRPM.Name = "chkRPM";
            this.chkRPM.Size = new System.Drawing.Size(64, 29);
            this.chkRPM.TabIndex = 1;
            this.chkRPM.Text = "RPM";
            this.chkRPM.UseVisualStyleBackColor = false;
            // 
            // chkRCA
            // 
            this.chkRCA.BackColor = System.Drawing.Color.Gainsboro;
            this.chkRCA.Checked = true;
            this.chkRCA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRCA.Location = new System.Drawing.Point(139, 8);
            this.chkRCA.Name = "chkRCA";
            this.chkRCA.Size = new System.Drawing.Size(55, 29);
            this.chkRCA.TabIndex = 2;
            this.chkRCA.Text = "RCA";
            this.chkRCA.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dtDAL);
            this.panel2.Controls.Add(this.lblDAL);
            this.panel2.Location = new System.Drawing.Point(200, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(297, 28);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // dtDAL
            // 
            this.dtDAL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDAL.Location = new System.Drawing.Point(39, 0);
            this.dtDAL.Name = "dtDAL";
            this.dtDAL.Size = new System.Drawing.Size(258, 27);
            this.dtDAL.TabIndex = 8;
            this.dtDAL.Value = new System.DateTime(2022, 11, 30, 0, 0, 0, 0);
            // 
            // lblDAL
            // 
            this.lblDAL.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDAL.Location = new System.Drawing.Point(0, 0);
            this.lblDAL.Name = "lblDAL";
            this.lblDAL.Size = new System.Drawing.Size(39, 28);
            this.lblDAL.TabIndex = 5;
            this.lblDAL.Text = "Dal:";
            this.lblDAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.dtAL);
            this.panel1.Controls.Add(this.lblAL);
            this.panel1.Location = new System.Drawing.Point(503, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 28);
            this.panel1.TabIndex = 1;
            // 
            // dtAL
            // 
            this.dtAL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtAL.Location = new System.Drawing.Point(39, 0);
            this.dtAL.Name = "dtAL";
            this.dtAL.Size = new System.Drawing.Size(258, 27);
            this.dtAL.TabIndex = 9;
            this.dtAL.Value = new System.DateTime(2022, 11, 30, 0, 0, 0, 0);
            // 
            // lblAL
            // 
            this.lblAL.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblAL.Location = new System.Drawing.Point(0, 0);
            this.lblAL.Name = "lblAL";
            this.lblAL.Size = new System.Drawing.Size(39, 28);
            this.lblAL.TabIndex = 8;
            this.lblAL.Text = "Al:";
            this.lblAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkSEND_NULL
            // 
            this.chkSEND_NULL.BackColor = System.Drawing.Color.Gainsboro;
            this.chkSEND_NULL.Location = new System.Drawing.Point(8, 43);
            this.chkSEND_NULL.Name = "chkSEND_NULL";
            this.chkSEND_NULL.Size = new System.Drawing.Size(137, 29);
            this.chkSEND_NULL.TabIndex = 8;
            this.chkSEND_NULL.Text = "Email da Inviare";
            this.chkSEND_NULL.UseVisualStyleBackColor = false;
            // 
            // chkRETURN_NULL
            // 
            this.chkRETURN_NULL.BackColor = System.Drawing.Color.Gainsboro;
            this.chkRETURN_NULL.Location = new System.Drawing.Point(151, 43);
            this.chkRETURN_NULL.Name = "chkRETURN_NULL";
            this.chkRETURN_NULL.Size = new System.Drawing.Size(147, 29);
            this.chkRETURN_NULL.TabIndex = 9;
            this.chkRETURN_NULL.Text = "Email da ricevere";
            this.chkRETURN_NULL.UseVisualStyleBackColor = false;
            // 
            // chkRETURN_ERROR
            // 
            this.chkRETURN_ERROR.BackColor = System.Drawing.Color.Gainsboro;
            this.chkRETURN_ERROR.Location = new System.Drawing.Point(304, 43);
            this.chkRETURN_ERROR.Name = "chkRETURN_ERROR";
            this.chkRETURN_ERROR.Size = new System.Drawing.Size(163, 29);
            this.chkRETURN_ERROR.TabIndex = 10;
            this.chkRETURN_ERROR.Text = "Risposta con errore";
            this.chkRETURN_ERROR.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnEstrai);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(946, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(61, 90);
            this.panel4.TabIndex = 0;
            // 
            // btnEstrai
            // 
            this.btnEstrai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEstrai.Location = new System.Drawing.Point(3, 3);
            this.btnEstrai.Name = "btnEstrai";
            this.btnEstrai.Size = new System.Drawing.Size(55, 84);
            this.btnEstrai.TabIndex = 8;
            this.btnEstrai.Text = "Estrai";
            this.btnEstrai.UseVisualStyleBackColor = true;
            // 
            // chkOnlyExistsResponse
            // 
            this.chkOnlyExistsResponse.BackColor = System.Drawing.Color.Gainsboro;
            this.chkOnlyExistsResponse.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOnlyExistsResponse.Location = new System.Drawing.Point(473, 43);
            this.chkOnlyExistsResponse.Name = "chkOnlyExistsResponse";
            this.chkOnlyExistsResponse.Size = new System.Drawing.Size(224, 29);
            this.chkOnlyExistsResponse.TabIndex = 11;
            this.chkOnlyExistsResponse.Text = "solo quelli con risposta successiva";
            this.chkOnlyExistsResponse.UseVisualStyleBackColor = false;
            // 
            // frmStatoRiepiloghi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1007, 612);
            this.Controls.Add(this.panelDATA);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelBOT);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStatoRiepiloghi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stato Riepiloghi";
            this.panelDATA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelBOT.ResumeLayout(false);
            this.panelBUTTONS.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelFILTERS.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelDATA;
        private System.Windows.Forms.Panel panelBOT;
        private clsDataGridRiepiloghi dgv;
        private System.Windows.Forms.FlowLayoutPanel panelBUTTONS;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label lblCELL;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.FlowLayoutPanel panelFILTERS;
        private System.Windows.Forms.CheckBox chkRPG;
        private System.Windows.Forms.CheckBox chkRPM;
        private System.Windows.Forms.CheckBox chkRCA;
        private System.Windows.Forms.Panel panel2;
        private clsLabel lblDAL;
        private System.Windows.Forms.Panel panel1;
        private clsLabel lblAL;
        private System.Windows.Forms.CheckBox chkSEND_NULL;
        private System.Windows.Forms.CheckBox chkRETURN_NULL;
        private System.Windows.Forms.CheckBox chkRETURN_ERROR;
        private System.Windows.Forms.Panel panel4;
        private clsButton btnEstrai;
        private System.Windows.Forms.DateTimePicker dtDAL;
        private System.Windows.Forms.DateTimePicker dtAL;
        private clsButton btnFORCE_RECEIVED;
        private clsLabel lblWait;
        private System.Windows.Forms.CheckBox chkOnlyExistsResponse;
    }
}