﻿
namespace ConfigRiepiloghi
{
    partial class frmDataMigrazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataMigrazione));
            this.pnlData = new System.Windows.Forms.Panel();
            this.pnlCounters = new System.Windows.Forms.Panel();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.dateTimePickerMigrazione = new System.Windows.Forms.DateTimePicker();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnRICALC = new ConfigRiepiloghi.clsButton();
            this.lblDataMigrazioneValue = new ConfigRiepiloghi.clsLabel();
            this.btnABORT = new ConfigRiepiloghi.clsButton();
            this.lblTitle = new ConfigRiepiloghi.clsLabel();
            this.btnSAVE = new ConfigRiepiloghi.clsButton();
            this.pnlData.SuspendLayout();
            this.pnlEdit.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.Controls.Add(this.pnlCounters);
            this.pnlData.Controls.Add(this.pnlEdit);
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 41);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(984, 519);
            this.pnlData.TabIndex = 1;
            // 
            // pnlCounters
            // 
            this.pnlCounters.AutoScroll = true;
            this.pnlCounters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCounters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCounters.Location = new System.Drawing.Point(0, 0);
            this.pnlCounters.Name = "pnlCounters";
            this.pnlCounters.Size = new System.Drawing.Size(683, 519);
            this.pnlCounters.TabIndex = 3;
            // 
            // pnlEdit
            // 
            this.pnlEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEdit.Controls.Add(this.btnSAVE);
            this.pnlEdit.Controls.Add(this.btnRICALC);
            this.pnlEdit.Controls.Add(this.dateTimePickerMigrazione);
            this.pnlEdit.Controls.Add(this.lblDataMigrazioneValue);
            this.pnlEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlEdit.Location = new System.Drawing.Point(683, 0);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Padding = new System.Windows.Forms.Padding(3);
            this.pnlEdit.Size = new System.Drawing.Size(301, 519);
            this.pnlEdit.TabIndex = 6;
            // 
            // dateTimePickerMigrazione
            // 
            this.dateTimePickerMigrazione.Dock = System.Windows.Forms.DockStyle.Top;
            this.dateTimePickerMigrazione.Location = new System.Drawing.Point(3, 45);
            this.dateTimePickerMigrazione.Name = "dateTimePickerMigrazione";
            this.dateTimePickerMigrazione.Size = new System.Drawing.Size(293, 37);
            this.dateTimePickerMigrazione.TabIndex = 4;
            // 
            // pnlBottom
            // 
            this.pnlBottom.AutoScroll = true;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnABORT);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 560);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Padding = new System.Windows.Forms.Padding(3);
            this.pnlBottom.Size = new System.Drawing.Size(984, 77);
            this.pnlBottom.TabIndex = 2;
            // 
            // btnRICALC
            // 
            this.btnRICALC.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRICALC.Location = new System.Drawing.Point(3, 82);
            this.btnRICALC.Name = "btnRICALC";
            this.btnRICALC.Size = new System.Drawing.Size(293, 126);
            this.btnRICALC.TabIndex = 9;
            this.btnRICALC.Text = "Assegna e Ricalcola";
            this.btnRICALC.UseVisualStyleBackColor = true;
            // 
            // lblDataMigrazioneValue
            // 
            this.lblDataMigrazioneValue.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDataMigrazioneValue.Location = new System.Drawing.Point(3, 3);
            this.lblDataMigrazioneValue.Name = "lblDataMigrazioneValue";
            this.lblDataMigrazioneValue.Size = new System.Drawing.Size(293, 42);
            this.lblDataMigrazioneValue.TabIndex = 5;
            this.lblDataMigrazioneValue.Text = "Imposta Data Migrazione:";
            this.lblDataMigrazioneValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnABORT
            // 
            this.btnABORT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnABORT.Location = new System.Drawing.Point(3, 3);
            this.btnABORT.Name = "btnABORT";
            this.btnABORT.Size = new System.Drawing.Size(976, 69);
            this.btnABORT.TabIndex = 9;
            this.btnABORT.Text = "Chiudi";
            this.btnABORT.UseVisualStyleBackColor = true;
            // 
            // lblTitle
            // 
            this.lblTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(984, 41);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Inserimento LOG/LTA precedenti alla migrazione";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSAVE
            // 
            this.btnSAVE.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSAVE.Location = new System.Drawing.Point(3, 375);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(293, 139);
            this.btnSAVE.TabIndex = 10;
            this.btnSAVE.Text = "INSERISCI";
            this.btnSAVE.UseVisualStyleBackColor = true;
            // 
            // frmDataMigrazione
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(984, 637);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.lblTitle);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDataMigrazione";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Migrazione";
            this.pnlData.ResumeLayout(false);
            this.pnlEdit.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private clsLabel lblTitle;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.Panel pnlCounters;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.DateTimePicker dateTimePickerMigrazione;
        private clsLabel lblDataMigrazioneValue;
        private clsButton btnABORT;
        private clsButton btnRICALC;
        private clsButton btnSAVE;
    }
}