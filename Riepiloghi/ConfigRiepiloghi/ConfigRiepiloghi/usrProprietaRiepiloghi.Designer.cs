﻿
namespace ConfigRiepiloghi
{
    partial class usrProprietaRiepiloghi
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblProprieta = new clsLabel();
            this.txtProprieta = new System.Windows.Forms.TextBox();
            this.btnProprieta = new clsButton();
            this.chkProprieta = new System.Windows.Forms.CheckBox();
            this.cmbProprieta = new System.Windows.Forms.ComboBox();
            this.panelEdit = new System.Windows.Forms.Panel();
            this.panelSpace = new System.Windows.Forms.Panel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblError = new clsLabel();
            this.panelEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblProprieta
            // 
            this.lblProprieta.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblProprieta.Location = new System.Drawing.Point(3, 3);
            this.lblProprieta.Name = "lblProprieta";
            this.lblProprieta.Size = new System.Drawing.Size(119, 69);
            this.lblProprieta.TabIndex = 0;
            this.lblProprieta.Text = "label1";
            this.lblProprieta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtProprieta
            // 
            this.txtProprieta.Location = new System.Drawing.Point(6, 3);
            this.txtProprieta.Name = "txtProprieta";
            this.txtProprieta.Size = new System.Drawing.Size(100, 27);
            this.txtProprieta.TabIndex = 1;
            // 
            // btnProprieta
            // 
            this.btnProprieta.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProprieta.Location = new System.Drawing.Point(342, 0);
            this.btnProprieta.Name = "btnProprieta";
            this.btnProprieta.Size = new System.Drawing.Size(109, 69);
            this.btnProprieta.TabIndex = 2;
            this.btnProprieta.Text = "button1";
            this.btnProprieta.UseVisualStyleBackColor = true;
            // 
            // chkProprieta
            // 
            this.chkProprieta.AutoSize = true;
            this.chkProprieta.Location = new System.Drawing.Point(239, 5);
            this.chkProprieta.Name = "chkProprieta";
            this.chkProprieta.Size = new System.Drawing.Size(97, 23);
            this.chkProprieta.TabIndex = 3;
            this.chkProprieta.Text = "checkBox1";
            this.chkProprieta.UseVisualStyleBackColor = true;
            // 
            // cmbProprieta
            // 
            this.cmbProprieta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProprieta.FormattingEnabled = true;
            this.cmbProprieta.Location = new System.Drawing.Point(112, 3);
            this.cmbProprieta.Name = "cmbProprieta";
            this.cmbProprieta.Size = new System.Drawing.Size(121, 27);
            this.cmbProprieta.TabIndex = 4;
            // 
            // panelEdit
            // 
            this.panelEdit.Controls.Add(this.lblError);
            this.panelEdit.Controls.Add(this.txtProprieta);
            this.panelEdit.Controls.Add(this.cmbProprieta);
            this.panelEdit.Controls.Add(this.btnProprieta);
            this.panelEdit.Controls.Add(this.chkProprieta);
            this.panelEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEdit.Location = new System.Drawing.Point(147, 3);
            this.panelEdit.Name = "panelEdit";
            this.panelEdit.Size = new System.Drawing.Size(451, 69);
            this.panelEdit.TabIndex = 5;
            // 
            // panelSpace
            // 
            this.panelSpace.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSpace.Location = new System.Drawing.Point(122, 3);
            this.panelSpace.Name = "panelSpace";
            this.panelSpace.Size = new System.Drawing.Size(25, 69);
            this.panelSpace.TabIndex = 6;
            // 
            // toolTip
            // 
            this.toolTip.IsBalloon = true;
            // 
            // lblError
            // 
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 50);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(342, 19);
            this.lblError.TabIndex = 5;
            this.lblError.Text = "Dato non valido";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblError.Visible = false;
            // 
            // usrProprietaRiepiloghi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panelEdit);
            this.Controls.Add(this.panelSpace);
            this.Controls.Add(this.lblProprieta);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "usrProprietaRiepiloghi";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(601, 75);
            this.panelEdit.ResumeLayout(false);
            this.panelEdit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private clsLabel lblProprieta;
        private System.Windows.Forms.TextBox txtProprieta;
        private clsButton btnProprieta;
        private System.Windows.Forms.CheckBox chkProprieta;
        private System.Windows.Forms.ComboBox cmbProprieta;
        private System.Windows.Forms.Panel panelEdit;
        private System.Windows.Forms.Panel panelSpace;
        private System.Windows.Forms.ToolTip toolTip;
        private clsLabel lblError;
    }
}
