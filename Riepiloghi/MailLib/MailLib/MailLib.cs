﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using MimeKit;
using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using System.IO;
//using System.Net.Mail;

namespace MailLib
{

    public partial class Mail
    {
        // SETTINGS

        public struct ServerSmtp
        {
            public bool enabled;

            public string Host;

            public string User;
            public string Password;

            public int Port;

            public string EmailFrom;

            public string Protocol;
        }

        private struct ServerSMTP
        {
            public const string SMTP_HOST = "mobility.creaweb.it";

            public const Int32 SMTP_PORT = 2500;

            public const string SMTP_USER = "importante@creaweb.it";

            public const string SMTP_PASSWORD = "nonsidaingiro";

            public const string SMTP_MAIL_FROM = "no-reply@evolutiontic.it";


        }


        public static ServerSmtp smtpServer = new ServerSmtp();

        public static void setSmtpServer()
        {
            smtpServer.enabled = true;

            smtpServer.Host = ServerSMTP.SMTP_HOST;

            smtpServer.Port = ServerSMTP.SMTP_PORT;

            smtpServer.User = ServerSMTP.SMTP_USER;
            smtpServer.Password = ServerSMTP.SMTP_PASSWORD;



            //smtpServer.EmailFrom = ServerSMTP.SMTP_MAIL_FROM;
        }

        public static void setSmtpServer(string host, int port, string user, string password)
        {
            smtpServer.enabled = true;

            smtpServer.Host = host;

            smtpServer.Port = port;

            smtpServer.User = user;

            smtpServer.Password = password;

        }

        public static void setSmtpServer(string host, int port, string user, string password, string protocol)
        {
            smtpServer.enabled = true;

            smtpServer.Host = host;

            smtpServer.Port = port;

            smtpServer.User = user;

            smtpServer.Password = password;

            smtpServer.Protocol = protocol;
        }




        public static bool sendEmailFromEmlFile(string fileName, ref string err_message, ref int err_code)
        {
            try
            {
                InternetAddressList _ccAddresses = new InternetAddressList();
                InternetAddressList _bccAddresses = new InternetAddressList();

                return sendEmailFromEmlFile(fileName, _ccAddresses, _bccAddresses, ref err_message, ref err_code);
            }
            catch (Exception e)
            {
                err_code = -125;
                err_message = e.Message;
                return false;
            }

            //System.Net.Mail.MailAddress mailAddress = new System.Net.Mail.MailAddress("pipppo@pippo.it");
            //MailboxAddress mailboxAddress = new MailboxAddress(mailAddress.DisplayName, mailAddress.Address);
            //MailboxAddress mailboxAddress = (MailboxAddress)mailAddress;
        }


        public static bool sendEmailFromEmlFile(string fileName, System.Net.Mail.MailAddressCollection ccAddresses, System.Net.Mail.MailAddressCollection bccAddresses, ref string err_message, ref int err_code)
        {                
            try
            {
                InternetAddressList _ccAddresses = new InternetAddressList((InternetAddressList)ccAddresses);
                InternetAddressList _bccAddresses = new InternetAddressList((InternetAddressList)bccAddresses);

                return sendEmailFromEmlFile(fileName, _ccAddresses, _bccAddresses, ref err_message, ref err_code);
            }
            catch(Exception e)
            {
                err_code = -125;
                err_message = e.Message;
                return false;
            }
           
        }
        
        private static bool sendEmailFromEmlFile(string fileName, InternetAddressList ccAddresses, InternetAddressList bccAddresses, ref string err_message, ref int err_code)
        {   
            try
            {
                var message = MimeMessage.Load(fileName);
                var body = message.TextBody;


                message.Date = DateTime.Now;

                if (ccAddresses != null) message.Cc.AddRange(ccAddresses);
                if(bccAddresses != null) message.Bcc.AddRange(bccAddresses);
                
                using (var client = new SmtpClient())
                {
                    // Note: don't set a timeout unless you REALLY know what you are doing.
                    //client.Timeout = 1000 * 20;

                    //client.DeliveryStatusNotificationType

                    //client.ServerCertificateValidationCallback = (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true;
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    

                    //client.SslProtocols. = true;

                    client.Connect(smtpServer.Host, smtpServer.Port, SecureSocketOptions.Auto);

                    
                    // The AuthenticationMechanisms do not get populated until the client connects to the server.                    
                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(smtpServer.User, smtpServer.Password);

                    client.Send(message);
                    client.Disconnect(true);
                }

                //ProcessDeliveryStatusNotification(message);

                //GetDeliveryStatusNotifications()

                //DSNSmtpClient xx = new DSNSmtpClient();
                //xx..GetDeliveryStatusNotifications()

            }
            catch (Exception e)
            {
                err_code = -1000;
                err_message = e.Message;

                return false;
            }

            

            return true;
        }

        /*
        protected override DeliveryStatusNotification? GetDeliveryStatusNotifications(MimeMessage message, MailboxAddress mailbox)
        {
            // In this example, we only want to be notified of failures to deliver to a mailbox.
            // If you also want to be notified of delays or successful deliveries, simply bitwise-or
            // whatever combination of flags you want to be notified about.
            return DeliveryStatusNotification.Failure;
        }
        */

        public static void ProcessDeliveryStatusNotification(MimeMessage message)
        {
            var report = message.Body as MultipartReport;

            if (report == null || report.ReportType == null || !report.ReportType.Equals("delivery-status", StringComparison.OrdinalIgnoreCase))
            {
                // this is not a delivery status notification message...
                return;
            }

            // process the report
            foreach (var mds in report.OfType<MessageDeliveryStatus>())
            {
                // process the status groups - each status group represents a different recipient

                // The first status group contains information about the message
                var envelopeId = mds.StatusGroups[0]["Original-Envelope-Id"];

                // all of the other status groups contain per-recipient information
                for (int i = 1; i < mds.StatusGroups.Count; i++)     // mds.StatusGroups.Length
                {
                    var recipient = mds.StatusGroups[i]["Original-Recipient"];
                    var action = mds.StatusGroups[i]["Action"];

                    if (recipient == null)
                        recipient = mds.StatusGroups[i]["Final-Recipient"];

                    // the recipient string should be in the form: "rfc822;user@domain.com"
                    var index = recipient.IndexOf(';');
                    var address = recipient.Substring(index + 1);

                    switch (action)
                    {
                        case "failed":
                            Console.WriteLine("Delivery of message {0} failed for {1}", envelopeId, address);
                            break;
                        case "delayed":
                            Console.WriteLine("Delivery of message {0} has been delayed for {1}", envelopeId, address);
                            break;
                        case "delivered":
                            Console.WriteLine("Delivery of message {0} has been delivered to {1}", envelopeId, address);
                            break;
                        case "relayed":
                            Console.WriteLine("Delivery of message {0} has been relayed for {1}", envelopeId, address);
                            break;
                        case "expanded":
                            Console.WriteLine("Delivery of message {0} has been delivered to {1} and relayed to the the expanded recipients", envelopeId, address);
                            break;
                    }
                }
            }
        }

        
    }

    public class DSNSmtpClient : SmtpClient
    {
        public DSNSmtpClient()
        {
        }

        /// <summary>
        /// Get the envelope identifier to be used with delivery status notifications.
        /// </summary>
        /// <remarks>
        /// <para>The envelope identifier, if non-empty, is useful in determining which message
        /// a delivery status notification was issued for.</para>
        /// <para>The envelope identifier should be unique and may be up to 100 characters in
        /// length, but must consist only of printable ASCII characters and no white space.</para>
        /// <para>For more information, see rfc3461, section 4.4.</para>
        /// </remarks>
        /// <returns>The envelope identifier.</returns>
        /// <param name="message">The message.</param>
        protected override string GetEnvelopeId(MimeMessage message)
        {
            // Since you will want to be able to map whatever identifier you return here to the
            // message, the obvious identifier to use is probably the Message-Id value.
            return message.MessageId;
        }

        /// <summary>
        /// Get the types of delivery status notification desired for the specified recipient mailbox.
        /// </summary>
        /// <remarks>
        /// Gets the types of delivery status notification desired for the specified recipient mailbox.
        /// </remarks>
        /// <returns>The desired delivery status notification type.</returns>
        /// <param name="message">The message being sent.</param>
        /// <param name="mailbox">The mailbox.</param>
        protected override DeliveryStatusNotification? GetDeliveryStatusNotifications(MimeMessage message, MailboxAddress mailbox)
        {
            // In this example, we only want to be notified of failures to deliver to a mailbox.
            // If you also want to be notified of delays or successful deliveries, simply bitwise-or
            // whatever combination of flags you want to be notified about.
            return DeliveryStatusNotification.Failure;
        }
    }

}




/*


        private static bool sendMailOrder(CConnectionOracle conn, string DomainId, Int32 IdDoc, string _emailMittente, string _emailDestinatario, string mittente, bool mittenteIsAlsoDestinatario, bool mittenteInCopia, ref String errorCode, ref String errorMessage)
        {
            String TipoDocumento = "";
            String Destinatario = "";
            String Mittente = "";
            String Barcode = "";

            String oggettoEmail = "";

            String EmailMittente = "";
            String EmailDestinatario = "";


            bool isAdminDocument = false;

            byte[] bufferAttachment = null;

            try
            {
                // TIPO DI EMAIL E DI DOCUMENTO
                //if (MAIL_TYPE != "") ;

                try
                {
                    if (DomainId == "1") isAdminDocument = true;

                    libWebPdfDocumenti.cDocumento Documento = libWebPdfDocumentiDataRecover.clsDocReader.GetDocumento(conn, IdDoc);
                    libWebPdfDocumenti.cLayoutDocumento Layout = libWebPdfDocumentiDataRecover.clsDocReader.Get_cLayoutDocumento(conn, Documento.IdTipDoc, isAdminDocument);

                    // GENERAZIONE PDF
                    string _fontsPath = System.Web.HttpContext.Current.Server.MapPath(@"~/printer-lib/fonts");


                    // USER FOR EMAIL
                    TipoDocumento = Documento.DescrTipDoc;
                    Destinatario = Documento.Destinatario.Descr;
                    Mittente = Documento.Mittente.Descr;
                    Barcode = Documento.BarCodeRiferimento;


                    byte[] aPdfByteArray = libWebPdfDocumenti.clsPrintDocBase.GetPdfBytes(Documento, Layout, false, _fontsPath, "", "");

                    bufferAttachment = aPdfByteArray;

                }
                catch (Exception e) { }



                // RECUPERO DEI DATI EMAIL DEL MITTENTE E DEL FORNITORE              

                if (!getEmailMittenteFromDocumento(conn, IdDoc, ref EmailMittente))
                {
                    errorCode = "015";
                    errorMessage = "Email Mittente inesistente";
                    return false;
                }

                if (!getEmailDestinatarioFromDocumento(conn, IdDoc, ref EmailDestinatario))
                {
                    errorCode = "016";
                    errorMessage = "Email Destinatario inesistente";
                    return false;
                }

                if (_emailMittente != "") EmailMittente = _emailMittente;
                if (_emailDestinatario != "") EmailDestinatario = _emailDestinatario;


                // REGOLE PER MITTENTE E DESTINATARIO E BCC

                if (mittenteIsAlsoDestinatario) EmailDestinatario = EmailMittente;

                
                // RECUPERO DELLE CCLIST
                List<string> ccList = new List<string>();
                if(mittenteInCopia) ccList.Add(EmailMittente);

                //ccList.Add("francesco.g@creaweb.it");

                //ccList.Add("danielealgranati@dalgra.it");
                //ccList.Add("francesco.g@yako.it");
                //ccList.Add("andrea@creaweb.it");
                //ccList.Add("alberto@creaweb.it");

                if (mittente != "") EmailMittente = mittente;


                if (!getEmailListFromDocumento(conn, IdDoc, ref ccList))
                {

                }


                // IMPOSTAZIONE MAIL SERVER
                commonLib.Functions.MailLib.setSmtpServer();

                // TEMPLATE HTML
                String htmlEmail = MailMapper.getMailTemplate(MailMapper.ContextTypeMailMap.GET_MAIL_ORDINE_TO_FORNITORE);

                // IMMAGINI HTML
                List<String> imgListEmail = MailMapper.getMailImageList(MailMapper.ContextTypeMailMap.GET_MAIL_ORDINE_TO_FORNITORE);


                String attachmentName = TipoDocumento + "_" + IdDoc.ToString();

                Attachment A = null;
                if (bufferAttachment != null) A = MailLib.GetAttachment(bufferAttachment, attachmentName, "application/pdf");


                List<System.Net.Mail.Attachment> attList = new List<Attachment>();
                if (A != null) attList.Add(A);

                              


                Int32 idstato = 0;
                getIdStatoDocumento(conn, IdDoc, ref idstato);

                if (idstato == 1)
                {
                    htmlEmail = htmlEmail.Replace("_DESTINATARIO_", Destinatario).Replace("_TIPO_DOCUMENTO_", "ANNULLAMENTO " + TipoDocumento.ToLower()).Replace("_NUMERO_DOCUMENTO_", IdDoc.ToString()).Replace("_MITTENTE_", Mittente).Replace("_BC_", Barcode);
                    oggettoEmail = "ANNULLAMENTO " + TipoDocumento + " " + Mittente;
                }
                else
                {
                    htmlEmail = htmlEmail.Replace("_DESTINATARIO_", Destinatario).Replace("_TIPO_DOCUMENTO_", TipoDocumento.ToLower()).Replace("_NUMERO_DOCUMENTO_", IdDoc.ToString()).Replace("_MITTENTE_", Mittente).Replace("_BC_", Barcode);
                    oggettoEmail = TipoDocumento + " " + Mittente;
                }


                htmlEmail = htmlEmail.Replace("_EML_MITT_", EmailMittente).Replace("_EML_DEST_", EmailDestinatario);
                if (mittenteInCopia) htmlEmail = htmlEmail.Replace("_EML_COPY_", EmailMittente);


                // WHITE req.IdDoc
                if(!MailLib.buildAndSendEmail(htmlEmail, imgListEmail, oggettoEmail, EmailDestinatario, EmailMittente, attList, ccList, MailLib.smtpServer, true, ref errorCode, ref errorMessage))
                {
                    return false;
                }

                


            }
            catch (Exception e)
            {
                Logger.Error("sendMailOrder " + e.Message);
                //res.setError(enumErrorCode.EVP_API_EXC_UNKNOWN, "Exception in sendMailOrder", e.Message);
                errorMessage = e.Message;
                return false;
            }

            return true;
        }


        private static bool sendMailDocument(CConnectionOracle conn, string DomainId, RequestSendMail req, ResultSendMail res)
        {
            String TipoDocumento = "";
            String Destinatario = "";
            String Mittente = "";
            String Barcode = "";

            String oggettoEmail = "";

            String EmailMittente = "";
            String EmailDestinatario = "";

            String errorCode = "";
            String errorMessage = "";


            bool isAdminDocument = false;

            byte[] bufferAttachment = null;

            try
            {

                try
                {
                    //if (DomainId == "1") isAdminDocument = true;

                    libWebPdfDocumenti.cDocumento Documento = libWebPdfDocumentiDataRecover.clsDocReader.GetDocumento(conn, req.IdDoc);
                    Documento.TextOver = "COPIA CONFORME";
                    libWebPdfDocumenti.cLayoutDocumento Layout = libWebPdfDocumentiDataRecover.clsDocReader.Get_cLayoutDocumento(conn, Documento.IdTipDoc, isAdminDocument);

                    // GENERAZIONE PDF
                    string _fontsPath = System.Web.HttpContext.Current.Server.MapPath(@"~/printer-lib/fonts");


                    // USER FOR EMAIL
                    TipoDocumento = Documento.DescrTipDoc;
                    Destinatario = Documento.Destinatario.Descr;
                    Mittente = Documento.Mittente.Descr;
                    Barcode = Documento.BarCodeRiferimento;


                    byte[] aPdfByteArray = libWebPdfDocumenti.clsPrintDocBase.GetPdfBytes(Documento, Layout, false, _fontsPath, "", "");

                    bufferAttachment = aPdfByteArray;

                }
                catch (Exception e) { }



                // RECUPERO DEI DATI EMAIL DEL MITTENTE E DEL FORNITORE              

                if (!getEmailMittenteFromDocumento(conn, req.IdDoc, ref EmailMittente))
                {
                    res.setError(enumErrorCode.EVP_API_ERR_INVIO_EMAIL, "Email Mittente inesistente");
                    return false;
                }

                if (!getEmailDestinatarioFromDocumento(conn, req.IdDoc, ref EmailDestinatario))
                {
                    res.setError(enumErrorCode.EVP_API_ERR_INVIO_EMAIL, "Email Destinatario inesistente");
                    return false;
                }


                if (!String.IsNullOrEmpty(req._emailMittente)) EmailMittente = req._emailMittente;
                if (!String.IsNullOrEmpty(req._emailDestinatario)) EmailDestinatario = req._emailDestinatario;


                // REGOLE PER MITTENTE E DESTINATARIO E BCC
                
                Int32 iddest = 0;

                if (!getIdDestFromIdDoc(conn, req.IdDoc, ref iddest))
                {
                }

                req.mittenteInCopia = true;

                if (isProviderEnabledForWebApiOrder(conn, iddest))
                {
                    req.mittenteIsAlsoDestinatario = true;
                    req.mittenteInCopia = false;
                }



                if (req.mittenteIsAlsoDestinatario) EmailDestinatario = EmailMittente;


                // RECUPERO DELLE CCLIST
                List<string> ccList = new List<string>();
                if (req.mittenteInCopia) ccList.Add(EmailMittente);

                //ccList.Add("francesco.g@creaweb.it");


                if (req.mittente != "") EmailMittente = req.mittente;


                if (!getEmailListFromDocumento(conn, req.IdDoc, ref ccList))
                {

                }


                // IMPOSTAZIONE MAIL SERVER
                commonLib.Functions.MailLib.setSmtpServer();

                // TEMPLATE HTML
                String htmlEmail = MailMapper.getMailTemplate(MailMapper.ContextTypeMailMap.GET_MAIL_ORDINE_TO_FORNITORE);

                // IMMAGINI HTML
                List<String> imgListEmail = MailMapper.getMailImageList(MailMapper.ContextTypeMailMap.GET_MAIL_ORDINE_TO_FORNITORE);


                String attachmentName = TipoDocumento + "_" + req.IdDoc.ToString();

                Attachment A = null;
                if (bufferAttachment != null) A = MailLib.GetAttachment(bufferAttachment, attachmentName, "application/pdf");


                List<System.Net.Mail.Attachment> attList = new List<Attachment>();
                if (A != null) attList.Add(A);
                

                Int32 idstato = 0;
                getIdStatoDocumento(conn, req.IdDoc, ref idstato);

                if (idstato == 1)
                {
                    htmlEmail = htmlEmail.Replace("_DESTINATARIO_", Destinatario).Replace("_TIPO_DOCUMENTO_", "ANNULLAMENTO " + TipoDocumento.ToLower()).Replace("_NUMERO_DOCUMENTO_", req.IdDoc.ToString()).Replace("_MITTENTE_", Mittente).Replace("_BC_", Barcode);
                    oggettoEmail = "ANNULLAMENTO " + TipoDocumento + " " + Mittente;
                }
                else
                {
                    htmlEmail = htmlEmail.Replace("_DESTINATARIO_", Destinatario).Replace("_TIPO_DOCUMENTO_", TipoDocumento.ToLower()).Replace("_NUMERO_DOCUMENTO_", req.IdDoc.ToString()).Replace("_MITTENTE_", Mittente).Replace("_BC_", Barcode);
                    oggettoEmail = TipoDocumento + " " + Mittente;
                }


                htmlEmail = htmlEmail.Replace("_EML_MITT_", EmailMittente).Replace("_EML_DEST_", EmailDestinatario);
                if (req.mittenteInCopia) htmlEmail = htmlEmail.Replace("_EML_COPY_", EmailMittente);



                // WHITE req.IdDoc
                if (!MailLib.buildAndSendEmail(htmlEmail, imgListEmail, oggettoEmail, EmailDestinatario, EmailMittente, attList, ccList, MailLib.smtpServer, true, ref errorCode, ref errorMessage))
                {
                    res.setError(enumErrorCode.EVP_API_ERR_INVIO_EMAIL, errorMessage);
                    return false;
                }
                
                deleteMailError(conn, req.IdDoc);

                res.rispost = "Invio Email con Ordine eseguito correttamente.";
                
            }
            catch (Exception e)
            {
                Logger.Error("sendMailDocument " + e.Message);
                res.setError(enumErrorCode.EVP_API_EXC_UNKNOWN, "Exception in sendMailDocument", e.Message);
                return false;
            }
                        
            return true;

        }






*/



