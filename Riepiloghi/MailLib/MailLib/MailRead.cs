﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib
{
    public partial class Mail
    {
        public static List<object> readMessagesPop3(string accountEmail, string user, string password, string pop3Server, int port, bool userSsl, IUidsLocalInboxProvider LocalInboxProvider, string codiceSistema, out Exception error)
        {
            error = null;
            bool lCheckUdis = false;
            List<object> result = new List<object>();

            //string patternCodiceSistema = "[P][0][0][0][0][3][9][9]";
            string patternCodiceSistema = "";

            foreach (char cCs in codiceSistema)
            {
                patternCodiceSistema += string.Format("[{0}]", cCs);
            }

            string patternStandardSubject = @"\d\d\d\d[:][R][e][:][R][PC][GMA][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d";

            Dictionary<string, string> patternsTypeSubject = new Dictionary<string, string>() {
            { "RPG", @"\d\d\d\d[:][R][e][:][R][P][G][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d" },
            { "RPM", @"\d\d\d\d[:][R][e][:][R][P][M][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d" },
            { "RCA", @"\d\d\d\d[:][R][e][:][R][C][A][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d" } };

            string patternStandardFile = @"[R][PC][GMA][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d[_]\d\d\d\d[_]\d\d[_]\d\d[_]\d{1,}[_]\d{1,}[.][Tt][Xx][Tt]";

            Dictionary<string, string> patternsTypeFile = new Dictionary<string, string>() {
            { "RPG", @"[R][P][G][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d[_]\d\d\d\d[_]\d\d[_]\d\d[_]\d{1,}[_]\d{1,}[.][Tt][Xx][Tt]" },
            { "RPM", @"[R][P][M][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d[_]\d\d\d\d[_]\d\d[_]\d\d[_]\d{1,}[_]\d{1,}[.][Tt][Xx][Tt]" },
            { "RCA", @"[R][C][A][_]\d\d\d\d[_]\d\d[_]\d\d[_]" + patternCodiceSistema + @"[_]\d\d\d[_][X][S][I][_][V][.]\d\d[.]\d\d[_]\d\d\d\d[_]\d\d[_]\d\d[_]\d{1,}[_]\d{1,}[.][Tt][Xx][Tt]" }
            };

            try
            {
                using (var client = new MailKit.Net.Pop3.Pop3Client())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    try
                    {
                        client.Connect(pop3Server, port, userSsl);
                        if (!client.IsConnected)
                        {
                            error = new Exception("Errore di connessione.");
                        }
                        else
                        {
                            try
                            {
                                client.Authenticate(user, password);
                                if (!client.IsAuthenticated)
                                {
                                    error = new Exception("Errore di autenticazione.");
                                }
                                else
                                {
                                    IList<string> uids_list = null;
                                    int nStartIndex = 0;

                                    // preventivamente controllo se il server contente Pop3Capabilities.UIDL
                                    //                           se è stato passato un oggetto IUidsLocalInboxProvider che implementa la ricerca per uid in locale
                                    try
                                    {
                                        if (LocalInboxProvider != null && LocalInboxProvider.CheckUidImplemented() && client.Capabilities.HasFlag(MailKit.Net.Pop3.Pop3Capabilities.UIDL))
                                        {
                                            uids_list = client.GetMessageUids();
                                            lCheckUdis = uids_list != null && uids_list.Count > 0;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    // preventivamente controllo se è stato passato un oggetto IUidsLocalInboxProvider che implementa il metodo LastMessageIndex per partire da li in poi
                                    try
                                    {
                                        if (LocalInboxProvider != null && LocalInboxProvider.LastMessageIndexImplemented())
                                        {
                                            nStartIndex = LocalInboxProvider.LastMessageIndex() + 1;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        nStartIndex = 0;
                                    }


                                    // ciclo dei messaggio da nStartIndex (default 0)
                                    for (int index = nStartIndex; index < client.Count; index++)
                                    {
                                        bool lProcessMessage = true;

                                        bool lRPG = false;
                                        bool lRPM = false;
                                        bool lRCA = false;
                                        DateTime? dGIORNO = null;
                                        long? nPROG = null;

                                        // eventuale verifica 
                                        if (lCheckUdis)
                                        {
                                            // procedo se il messaggio non è già stato elaborato localmente
                                            lProcessMessage = LocalInboxProvider.CheckUid(uids_list[index]);
                                            if (!lProcessMessage) continue;
                                        }
                                        
                                        // prendo il messaggio
                                        var message = client.GetMessage(index);

                                        // verifiche di base sul messaggio: non nullo con un destinatario e con allegati
                                        lProcessMessage = message != null && message.To != null && message.Attachments != null && message.Subject != null;
                                        if (!lProcessMessage) continue;

                                        // verifico se il messaggio è per il destinatario accountEmail
                                        lProcessMessage = message.To.ToString().Trim().ToUpper() == accountEmail.Trim().ToUpper();
                                        if (!lProcessMessage) continue;

                                        System.Text.RegularExpressions.Regex regExSubject = null;

                                        regExSubject = new System.Text.RegularExpressions.Regex(patternStandardSubject);
                                        lProcessMessage = (regExSubject.IsMatch(message.Subject));
                                        if (!lProcessMessage) continue;


                                        foreach (KeyValuePair<string, string> itemType in patternsTypeSubject)
                                        {
                                            regExSubject = new System.Text.RegularExpressions.Regex(itemType.Value);
                                            if (regExSubject.IsMatch(message.Subject))
                                            {
                                                lRPG = itemType.Key == "RPG";
                                                lRPM = itemType.Key == "RPM";
                                                lRCA = itemType.Key == "RCA";
                                                break;
                                            }
                                        }
                                        lProcessMessage = lRPG || lRPM || lRCA;
                                        if (!lProcessMessage) continue;

                                        // analisi degli allegati
                                        try
                                        {
                                            IEnumerable<MimeKit.MimeEntity> attachments_list = message.Attachments;

                                            foreach (MimeKit.MimeEntity attachment in attachments_list)
                                            {
                                                System.IO.MemoryStream oStreamTarget = new System.IO.MemoryStream();
                                                try
                                                {
                                                    var fileName = "";
                                                    if (attachment is MimeKit.MessagePart)
                                                    {
                                                        fileName = attachment.ContentDisposition?.FileName;
                                                        var rfc822 = (MimeKit.MessagePart)attachment;

                                                        if (!string.IsNullOrEmpty(fileName))
                                                        {
                                                            rfc822.Message.WriteTo(oStreamTarget);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var part = (MimeKit.MimePart)attachment;
                                                        fileName = part.FileName;
                                                        part.Content.DecodeTo(oStreamTarget);
                                                    }

                                                    if (fileName != null && !string.IsNullOrEmpty(fileName) && !string.IsNullOrWhiteSpace(fileName))
                                                    {
                                                        //patternStandardFile
                                                        regExSubject = new System.Text.RegularExpressions.Regex(patternStandardFile);
                                                        lProcessMessage = (regExSubject.IsMatch(fileName));
                                                        if (!lProcessMessage) continue;

                                                        // determino di nuovo se RPG,RPM o RCA
                                                        lRPG = false;
                                                        lRPM = false;
                                                        lRCA = false;
                                                        foreach (KeyValuePair<string, string> itemType in patternsTypeFile)
                                                        {
                                                            regExSubject = new System.Text.RegularExpressions.Regex(itemType.Value);
                                                            if (regExSubject.IsMatch(fileName))
                                                            {
                                                                lRPG = itemType.Key == "RPG";
                                                                lRPM = itemType.Key == "RPM";
                                                                lRCA = itemType.Key == "RCA";
                                                                break;
                                                            }
                                                        }
                                                        lProcessMessage = lRPG || lRPM || lRCA;
                                                        if (!lProcessMessage) continue;

                                                        try
                                                        {
                                                            //1234567890123456789012345678901234567890123456789012345678901234567890123456789
                                                            //123456789012345678 (PRIMI 18 INDICANO IL NOME)
                                                            //01234567890123456789012345678901234567890123456789012345678901234567890123456789
                                                            //RPG_2020_02_17_P0000399_001_XSI_V.01.00_2020_02_19_103953529_0000.txt
                                                            //                                                             12345678 ULTIMI 8, DI CUI I PRIMI 4 DANNO IL CODICE DI ERRORE
                                                            emailRiepiloghiResponse emailDaConsiderare = new emailRiepiloghiResponse();

                                                            emailDaConsiderare.FileNameRiepilogo = fileName.Substring(0, 15) + fileName.Substring(24, 3);
                                                            emailDaConsiderare.Tipo = (lRPG ? "G" : (lRPM ? "M" : "R"));

                                                            int anno = int.Parse(emailDaConsiderare.FileNameRiepilogo.Substring(4, 4));
                                                            int mese = int.Parse(emailDaConsiderare.FileNameRiepilogo.Substring(9, 2));
                                                            int giorno = (lRPM ? 1 : int.Parse(emailDaConsiderare.FileNameRiepilogo.Substring(12, 2)));
                                                            emailDaConsiderare.Giorno = new DateTime(anno, mese, giorno);
                                                            emailDaConsiderare.Progressivo = int.Parse(emailDaConsiderare.FileNameRiepilogo.Substring(15, 3));

                                                            int startPosErrCode = fileName.Length - 1;
                                                            int posPointExt = 0;
                                                            while (startPosErrCode > 0 && fileName.Substring(startPosErrCode,1) != "_")
                                                            {
                                                                if (fileName.Substring(startPosErrCode, 1) == ".")
                                                                    posPointExt = startPosErrCode;
                                                                startPosErrCode -= 1;
                                                            }
                                                            lProcessMessage = (startPosErrCode > 0 && posPointExt > 0);
                                                            if (!lProcessMessage) continue;
                                                                
                                                            emailDaConsiderare.ErrorCode = int.Parse(fileName.Substring(startPosErrCode + 1, posPointExt - startPosErrCode - 1));
                                                            emailDaConsiderare.Body = System.Text.Encoding.UTF8.GetString(oStreamTarget.GetBuffer());
                                                            result.Add(emailDaConsiderare);
                                                        }
                                                        catch (Exception exCreateItemResult)
                                                        {
                                                            error = exCreateItemResult;
                                                            error = new Exception(string.Format("Errore durante creazione item messaggio di risposta {0}", exCreateItemResult.Message));
                                                            break;
                                                        }


                                                    }
                                                }
                                                catch (Exception exAttachment)
                                                {
                                                    // devo considerare gli errori ?
                                                    error = new Exception(string.Format("Errore durante la lettura di un allegato {0}", exAttachment.Message));
                                                    lProcessMessage = false;
                                                    break;
                                                }
                                            }

                                        }
                                        catch (Exception exAttachmentsList)
                                        {
                                            // devo considerare gli errori ?
                                            error = new Exception(string.Format("Errore durante la lettura degli allegati {0}", exAttachmentsList.Message));
                                            lProcessMessage = false;
                                        }

                                        if (!lProcessMessage) continue;
                                    }
                                }
                            }
                            catch (Exception exAuth)
                            {
                                error = new Exception(string.Format("Errore durante l'autenticazione {0}", exAuth.Message));
                            }

                            client.Disconnect(true);
                        }

                    }
                    catch (Exception exConnect)
                    {
                        error = new Exception(string.Format("Errore durante la connessione {0}", exConnect.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                error = new Exception(string.Format("Errore generico {0}", ex.Message));
            }

            return result;
        }
    }

    public interface IUidsLocalInboxProvider
    {
        bool Initialize(object[] parameters);
        bool Finalize(object[] parameters);

        bool CheckUid(object uid);
        bool CheckUidImplemented();

        int LastMessageIndex();
        bool LastMessageIndexImplemented();
        
    }

    public class emailRiepiloghiResponse
    {
        public int MessageIndex { get; set; }
        public object MessageUid { get; set; }
        public DateTime MessageDate { get; set; }
        public string FileNameRiepilogo { get; set; }
        public string Tipo { get; set; }
        public DateTime Giorno { get; set; }
        public int Progressivo { get; set; }
        public string Body { get; set; }
        public int ErrorCode { get; set; }
    }

}
