﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailLib.Tools
{
    public partial class Utility
    {



        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }



        /*
                public static bool buildAndSendEmail(string htmlBody, List<String> imgListEmail, string oggetto, string destinatario, string mittente, List<Attachment> sAttachments, List<string> ccList, ServerSmtp serverSmtp, bool isHtmlBody, ref string errorCode, ref string errorMessage)
                {
                    SmtpClient client = new SmtpClient();

                    string fromEmail = mittente;

                    if (serverSmtp.enabled)
                    {
                        client.Host = serverSmtp.Host;
                        client.Credentials = new System.Net.NetworkCredential(serverSmtp.UserId, serverSmtp.Password);
                        fromEmail = String.IsNullOrEmpty(serverSmtp.EmailFrom) ? fromEmail : serverSmtp.EmailFrom;
                    }

                    MailAddress author = new MailAddress(fromEmail);

                    MailAddress destAddress = new MailAddress(destinatario);

                    List<LinkedResource> linkedResource = new List<LinkedResource>();

                    foreach (String imgEmail in imgListEmail)
                    {
                        // HTML IMAGES
                        var imageData = Convert.FromBase64String(imgEmail);

                        var contentId = Guid.NewGuid().ToString();

                        LinkedResource lnkResource = new LinkedResource(new MemoryStream(imageData), MediaTypeNames.Image.Jpeg);

                        lnkResource.ContentId = contentId;
                        lnkResource.TransferEncoding = TransferEncoding.Base64;

                        linkedResource.Add(lnkResource);

                    }

                    string xHtmlBody = "";

                    try
                    {
                        string[] ss = linkedResource.Select(s => s.ContentId).ToArray();

                        xHtmlBody = String.Format(htmlBody, ss);

                        xHtmlBody = xHtmlBody.Replace("{{", "{").Replace("}}", "}");
                    }
                    catch (Exception e)
                    {
                        //xHtmlBody = String.Format(htmlBody, linkedResource.ContentId);
                    }


                    AlternateView avHtml = AlternateView.CreateAlternateViewFromString(xHtmlBody, null, MediaTypeNames.Text.Html);

                    foreach (LinkedResource lnkResource in linkedResource) avHtml.LinkedResources.Add(lnkResource);



                    ////string htmlBody = "<html><body><h1>Picture</h1><br><img src=\"cid:Pic1\"></body></html>";
                    ////AlternateView avHtml = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

                    ////// Create a LinkedResource object for each embedded image
                    ////LinkedResource pic1 = new LinkedResource("pic.jpg", MediaTypeNames.Image.Jpeg);
                    ////pic1.ContentId = "Pic1";
                    ////avHtml.LinkedResources.Add(pic1);


                    ////// Add the alternate views instead of using MailMessage.Body
                    ////MailMessage m = new MailMessage();
                    ////m.AlternateViews.Add(avHtml);



                    MailMessage msg = new MailMessage();
                    msg.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                    msg.Subject = oggetto;
                    msg.Sender = author;

                    //msg.Body = htmlBody;

                    msg.AlternateViews.Add(avHtml);


                    msg.From = author;
                    //msg.ReplyTo = author;
                    msg.ReplyToList.Add(author);
                    msg.To.Add(destAddress);

                    if (ccList != null)
                    {
                        foreach (String emailCcn in ccList)
                        {
                            try
                            {
                                MailAddress ccnAddress = new MailAddress(emailCcn);
                                //msg.Bcc.Add(ccnAddress);
                                msg.CC.Add(ccnAddress);
                            }
                            catch { }
                        }
                    }

                    msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    msg.IsBodyHtml = true;


                    foreach (Attachment attach in sAttachments)
                    {
                        ContentDisposition disposition = attach.ContentDisposition;
                        disposition.FileName = attach.Name;
                        msg.Attachments.Add(attach);
                    }

                    try
                    {
                        client.Send(msg);
                    }
                    catch (Exception e)
                    {
                        errorCode = "MAIL_SEND_ERROR";
                        errorMessage = e.Message;
                        return false;
                    }

                    return true;
                }
        */



        /*
                public static Attachment GetAttachment(byte[] parBuffer, string parName, string parContentType)
                {
                    Attachment result = null;



                    MemoryStream ms = new MemoryStream(parBuffer);
                    ContentType ct = new ContentType(parContentType);
                    ct.CharSet = "iso-8859-1";
                    result = new Attachment(ms, ct);
                    result.Name = parName;
                    return result;
                }

        */

        /*
                public static bool buildEmailOriginal(string htmlBody, string oggetto, string destinatario, string mittente, List<Attachment> sAttachments, List<string> ccList, ServerSmtp serverSmtp, bool isHtmlBody)
                {
                    bool retVal = true;

                    SmtpClient client = new SmtpClient();

                    string fromEmail = mittente;

                    if (serverSmtp.enabled)
                    {
                        client.Host = serverSmtp.Host;
                        client.Credentials = new System.Net.NetworkCredential(serverSmtp.UserId, serverSmtp.Password);
                        fromEmail = String.IsNullOrEmpty(serverSmtp.EmailFrom) ? fromEmail : serverSmtp.EmailFrom;
                    }

                    MailAddress author = new MailAddress(fromEmail);

                    MailAddress destAddress = new MailAddress(destinatario);



                    // HTML IMAGES
                    //var imageData = Convert.FromBase64String(EvolutionStoreLib.ServerSMTP.IMG_PAPEETE_WHITE);
                    var imageData = new byte[32];

                    var contentId = Guid.NewGuid().ToString();

                    LinkedResource linkedResource = new LinkedResource(new MemoryStream(imageData), MediaTypeNames.Image.Jpeg);

                    linkedResource.ContentId = contentId;
                    linkedResource.TransferEncoding = TransferEncoding.Base64;

                    string xHtmlBody = "";

                    try
                    {
                        xHtmlBody = String.Format(htmlBody, linkedResource.ContentId);
                    }
                    catch (Exception e)
                    {
                        //xHtmlBody = String.Format(htmlBody, linkedResource.ContentId);
                    }

                    AlternateView avHtml = AlternateView.CreateAlternateViewFromString(xHtmlBody, null, MediaTypeNames.Text.Html);

                    avHtml.LinkedResources.Add(linkedResource);



                    MailMessage msg = new MailMessage();
                    msg.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                    msg.Subject = oggetto;
                    msg.Sender = author;

                    //msg.Body = htmlBody;

                    msg.AlternateViews.Add(avHtml);


                    msg.From = author;
                    msg.ReplyTo = author;
                    //msg.ReplyToList = author;
                    msg.To.Add(destAddress);

                    if (ccList != null)
                    {
                        foreach (String emailCcn in ccList)
                        {
                            try
                            {
                                MailAddress ccnAddress = new MailAddress(emailCcn);
                                msg.Bcc.Add(ccnAddress);
                            }
                            catch { }
                        }
                    }

                    msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    msg.IsBodyHtml = true;


                    foreach (Attachment attach in sAttachments)
                    {
                        ContentDisposition disposition = attach.ContentDisposition;
                        disposition.FileName = attach.Name;
                        msg.Attachments.Add(attach);
                    }


                    try { client.Send(msg); }
                    catch (Exception) { retVal = false; }

                    return retVal;
                }

        */




    }

}
