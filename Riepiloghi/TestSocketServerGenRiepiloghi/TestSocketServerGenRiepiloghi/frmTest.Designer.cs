﻿namespace TestSocketServerGenRiepiloghi
{
    partial class frmTest
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtGiorno = new System.Windows.Forms.DateTimePicker();
            this.grpTipo = new System.Windows.Forms.GroupBox();
            this.btnStartServer = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.radioButtonMensile = new System.Windows.Forms.RadioButton();
            this.radioButtonGiornaliero = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnGenera = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnUpdates = new System.Windows.Forms.Button();
            this.btnDebug = new System.Windows.Forms.Button();
            this.btnSendEmail = new System.Windows.Forms.Button();
            this.lblGiorno = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMessages = new System.Windows.Forms.TextBox();
            this.pnlParametri = new System.Windows.Forms.Panel();
            this.txtServerIP = new System.Windows.Forms.TextBox();
            this.txtServerPORT = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.btnLog = new System.Windows.Forms.Button();
            this.grpTipo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlParametri.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtGiorno
            // 
            this.dtGiorno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGiorno.Location = new System.Drawing.Point(56, 4);
            this.dtGiorno.Name = "dtGiorno";
            this.dtGiorno.Size = new System.Drawing.Size(843, 27);
            this.dtGiorno.TabIndex = 1;
            this.dtGiorno.Value = new System.DateTime(2019, 3, 11, 0, 0, 0, 0);
            // 
            // grpTipo
            // 
            this.grpTipo.Controls.Add(this.btnStartServer);
            this.grpTipo.Controls.Add(this.btnConnect);
            this.grpTipo.Controls.Add(this.btnLog);
            this.grpTipo.Controls.Add(this.radioButtonMensile);
            this.grpTipo.Controls.Add(this.radioButtonGiornaliero);
            this.grpTipo.Controls.Add(this.panel2);
            this.grpTipo.Controls.Add(this.btnGenera);
            this.grpTipo.Controls.Add(this.btnExit);
            this.grpTipo.Controls.Add(this.btnUpdates);
            this.grpTipo.Controls.Add(this.btnDebug);
            this.grpTipo.Controls.Add(this.btnSendEmail);
            this.grpTipo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTipo.Location = new System.Drawing.Point(0, 105);
            this.grpTipo.Name = "grpTipo";
            this.grpTipo.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.grpTipo.Size = new System.Drawing.Size(903, 76);
            this.grpTipo.TabIndex = 3;
            this.grpTipo.TabStop = false;
            this.grpTipo.Text = "Tipo Riepilogo";
            // 
            // btnStartServer
            // 
            this.btnStartServer.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnStartServer.Location = new System.Drawing.Point(342, 23);
            this.btnStartServer.Name = "btnStartServer";
            this.btnStartServer.Size = new System.Drawing.Size(113, 50);
            this.btnStartServer.TabIndex = 7;
            this.btnStartServer.Text = "AVVIA SERVER";
            this.btnStartServer.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConnect.Location = new System.Drawing.Point(455, 23);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(79, 50);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "Connetti";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // radioButtonMensile
            // 
            this.radioButtonMensile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioButtonMensile.Location = new System.Drawing.Point(254, 23);
            this.radioButtonMensile.Name = "radioButtonMensile";
            this.radioButtonMensile.Size = new System.Drawing.Size(359, 50);
            this.radioButtonMensile.TabIndex = 1;
            this.radioButtonMensile.Text = "Mensile";
            this.radioButtonMensile.UseVisualStyleBackColor = true;
            // 
            // radioButtonGiornaliero
            // 
            this.radioButtonGiornaliero.Checked = true;
            this.radioButtonGiornaliero.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonGiornaliero.Location = new System.Drawing.Point(149, 23);
            this.radioButtonGiornaliero.Name = "radioButtonGiornaliero";
            this.radioButtonGiornaliero.Size = new System.Drawing.Size(105, 50);
            this.radioButtonGiornaliero.TabIndex = 0;
            this.radioButtonGiornaliero.TabStop = true;
            this.radioButtonGiornaliero.Text = "Giornaliero";
            this.radioButtonGiornaliero.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(90, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 50);
            this.panel2.TabIndex = 3;
            // 
            // btnGenera
            // 
            this.btnGenera.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnGenera.Location = new System.Drawing.Point(613, 23);
            this.btnGenera.Name = "btnGenera";
            this.btnGenera.Size = new System.Drawing.Size(79, 50);
            this.btnGenera.TabIndex = 2;
            this.btnGenera.Text = "Genera";
            this.btnGenera.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.Location = new System.Drawing.Point(10, 23);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(80, 50);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Chiudi";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // btnUpdates
            // 
            this.btnUpdates.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnUpdates.Location = new System.Drawing.Point(692, 23);
            this.btnUpdates.Name = "btnUpdates";
            this.btnUpdates.Size = new System.Drawing.Size(79, 50);
            this.btnUpdates.TabIndex = 9;
            this.btnUpdates.Text = "Updates";
            this.btnUpdates.UseVisualStyleBackColor = true;
            this.btnUpdates.Click += new System.EventHandler(this.btnUpdates_Click);
            // 
            // btnDebug
            // 
            this.btnDebug.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDebug.Location = new System.Drawing.Point(771, 23);
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.Size = new System.Drawing.Size(79, 50);
            this.btnDebug.TabIndex = 10;
            this.btnDebug.Text = "Debug";
            this.btnDebug.UseVisualStyleBackColor = true;
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSendEmail.Location = new System.Drawing.Point(850, 23);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(50, 50);
            this.btnSendEmail.TabIndex = 8;
            this.btnSendEmail.Text = "MAIL";
            this.btnSendEmail.UseVisualStyleBackColor = true;
            // 
            // lblGiorno
            // 
            this.lblGiorno.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGiorno.Location = new System.Drawing.Point(4, 4);
            this.lblGiorno.Name = "lblGiorno";
            this.lblGiorno.Size = new System.Drawing.Size(52, 27);
            this.lblGiorno.TabIndex = 0;
            this.lblGiorno.Text = "Data:";
            this.lblGiorno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtGiorno);
            this.panel1.Controls.Add(this.lblGiorno);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 70);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(903, 35);
            this.panel1.TabIndex = 2;
            // 
            // txtMessages
            // 
            this.txtMessages.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessages.Location = new System.Drawing.Point(0, 181);
            this.txtMessages.Multiline = true;
            this.txtMessages.Name = "txtMessages";
            this.txtMessages.ReadOnly = true;
            this.txtMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessages.Size = new System.Drawing.Size(903, 336);
            this.txtMessages.TabIndex = 4;
            // 
            // pnlParametri
            // 
            this.pnlParametri.Controls.Add(this.txtServerIP);
            this.pnlParametri.Controls.Add(this.txtServerPORT);
            this.pnlParametri.Controls.Add(this.lblServer);
            this.pnlParametri.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlParametri.Location = new System.Drawing.Point(0, 0);
            this.pnlParametri.Name = "pnlParametri";
            this.pnlParametri.Padding = new System.Windows.Forms.Padding(4);
            this.pnlParametri.Size = new System.Drawing.Size(903, 35);
            this.pnlParametri.TabIndex = 0;
            // 
            // txtServerIP
            // 
            this.txtServerIP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtServerIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtServerIP.Location = new System.Drawing.Point(65, 4);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.ReadOnly = true;
            this.txtServerIP.Size = new System.Drawing.Size(678, 27);
            this.txtServerIP.TabIndex = 1;
            // 
            // txtServerPORT
            // 
            this.txtServerPORT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtServerPORT.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtServerPORT.Location = new System.Drawing.Point(743, 4);
            this.txtServerPORT.Name = "txtServerPORT";
            this.txtServerPORT.ReadOnly = true;
            this.txtServerPORT.Size = new System.Drawing.Size(156, 27);
            this.txtServerPORT.TabIndex = 2;
            // 
            // lblServer
            // 
            this.lblServer.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblServer.Location = new System.Drawing.Point(4, 4);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(61, 27);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "Server:";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtPassword);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtUser);
            this.panel3.Controls.Add(this.lblUser);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 35);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(4);
            this.panel3.Size = new System.Drawing.Size(903, 35);
            this.panel3.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPassword.Location = new System.Drawing.Point(343, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(556, 27);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.Text = "crea";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(192, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Password:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUser
            // 
            this.txtUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtUser.Location = new System.Drawing.Point(65, 4);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(127, 27);
            this.txtUser.TabIndex = 1;
            this.txtUser.Text = "crea";
            // 
            // lblUser
            // 
            this.lblUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblUser.Location = new System.Drawing.Point(4, 4);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(61, 27);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "User:";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLog
            // 
            this.btnLog.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLog.Location = new System.Drawing.Point(534, 23);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(79, 50);
            this.btnLog.TabIndex = 12;
            this.btnLog.Text = "Abilita/Disabilita log";
            this.btnLog.UseVisualStyleBackColor = true;
            // 
            // frmTest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(903, 517);
            this.Controls.Add(this.txtMessages);
            this.Controls.Add(this.grpTipo);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlParametri);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Socket Server Generazione Riepiloghi";
            this.grpTipo.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlParametri.ResumeLayout(false);
            this.pnlParametri.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtGiorno;
        private System.Windows.Forms.GroupBox grpTipo;
        private System.Windows.Forms.RadioButton radioButtonMensile;
        private System.Windows.Forms.RadioButton radioButtonGiornaliero;
        private System.Windows.Forms.Label lblGiorno;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGenera;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtMessages;
        private System.Windows.Forms.Panel pnlParametri;
        private System.Windows.Forms.TextBox txtServerIP;
        private System.Windows.Forms.TextBox txtServerPORT;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Button btnStartServer;
        private System.Windows.Forms.Button btnSendEmail;
        private System.Windows.Forms.Button btnUpdates;
        private System.Windows.Forms.Button btnDebug;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnLog;
    }
}

