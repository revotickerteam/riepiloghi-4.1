﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;


namespace TestSocketServerGenRiepiloghi
{
    public partial class frmTest : Form
    {

        //private Socket m_sock;
        
        Riepiloghi.SocketClientRiepiloghi oClient = null;
        WinServiceRiepiloghi.WindowsServiceRiepiloghi oWindowsServiceServer = null;

        private string cIP = "";
        private int nPort = 0;

        public frmTest()
        {
            InitializeComponent();
            this.InitForm();
        }

        private void InitForm()
        {
            this.cIP = "127.0.0.1";
            this.nPort = 12001;
            this.txtServerIP.Text = this.cIP;
            this.txtServerPORT.Text = this.nPort.ToString();

            this.btnStartServer.Click += this.btnStartServer_Click;
            this.btnGenera.Click += btnGenera_Click;
            this.btnConnect.Click += BtnConnect_Click;
            this.btnExit.Click += btnExit_Click;
            this.btnSendEmail.Click += this.btnSendEmail_Click;
            this.btnDebug.Click += BtnDebug_Click;
            this.btnLog.Click += BtnLog_Click;
        }

        
        private delegate void delegateSetMessage(Riepiloghi.SocketClientRiepiloghi socket, string data);

        private void OClient_OnReceived(Riepiloghi.SocketClientRiepiloghi socket, string data)
        {
            if (this.InvokeRequired)
            {
                delegateSetMessage oD = new delegateSetMessage(OClient_OnReceived);
                this.Invoke(oD, socket, data);
            }
            else
            {
                Riepiloghi.clsWinServiceModel_response_Operation response = Newtonsoft.Json.JsonConvert.DeserializeObject<Riepiloghi.clsWinServiceModel_response_Operation>(data);
                this.txtMessages.Text += string.Format("\r\n" + "Code: {0}",response.Code);
                this.txtMessages.Text += string.Format("\r\n" + "Message: {0}", response.Message);
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();    
            }
            
        }

        private delegate void delegateSetMessageError(Riepiloghi.SocketClientRiepiloghi socket, Exception e);

        private void OClient_OnError(Riepiloghi.SocketClientRiepiloghi socket, Exception e)
        {
            if (this.InvokeRequired)
            {
                delegateSetMessageError oD = new delegateSetMessageError(OClient_OnError);
                this.Invoke(oD, socket, e);
            }
            else
            {
                this.txtMessages.Text += string.Format("\r\n" + "Error: {0}", e.Message);
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private delegate void delegateSetMessageGeneric(Riepiloghi.SocketClientRiepiloghi socket);

        
        private void OClient_OnOpened(Riepiloghi.SocketClientRiepiloghi socket)
        {
            if (this.InvokeRequired)
            {
                delegateSetMessageGeneric oD = new delegateSetMessageGeneric(OClient_OnOpened);
                this.Invoke(oD, socket);
            }
            else
            {
                this.txtMessages.Text += "\r\n" + "Opened";
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void OClient_OnClosed(Riepiloghi.SocketClientRiepiloghi socket)
        {
            if (this.InvokeRequired)
            {
                delegateSetMessageGeneric oD = new delegateSetMessageGeneric(OClient_OnClosed);
                this.Invoke(oD, socket);
            }
            else
            {
                this.txtMessages.Text += "\r\n" + "Closed";
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
                this.ChiudiClient();
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.ChiudiClient();
            this.ChiudiServer();
            this.Close();
            this.Dispose();
            Application.Exit();
        }

        
        private void btnStartServer_Click(object sender, EventArgs e)
        {
            this.btnStartServer.Enabled = false;
            System.Threading.Thread oThread = new System.Threading.Thread(new System.Threading.ThreadStart(AvvioServer));
            oThread.Start();
        }

        private void AvvioServer()
        {
            this.oWindowsServiceServer = new WinServiceRiepiloghi.WindowsServiceRiepiloghi();
            this.oWindowsServiceServer.ExecuteStart();
        }

        private void ChiudiServer()
        {
            if (this.oWindowsServiceServer != null)
            {
                oWindowsServiceServer.Stop();
                oWindowsServiceServer.Dispose();
            }
        }

        private bool InitClient()
        {
            if (this.oWindowsServiceServer != null)
            {
                if (this.oWindowsServiceServer.Configuration.ComunicationMode == Riepiloghi.clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_SOCKET)
                {
                    if (oClient == null)
                    {
                        oClient = new Riepiloghi.SocketClientRiepiloghi(this.txtServerIP.Text, int.Parse(this.txtServerPORT.Text));
                        oClient.OnOpened += OClient_OnOpened;
                        oClient.OnClosed += OClient_OnClosed;
                        oClient.OnError += OClient_OnError;
                        oClient.OnReceived += OClient_OnReceived;
                        return oClient.Open();
                    }
                    else
                    {
                        if (!oClient.IsOpened())
                        {
                            return oClient.Open();
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            else
                return false;
        }

        private void ChiudiClient()
        {
            if (this.oClient != null)
            {
                if (this.oClient.IsOpened())
                    this.oClient.Close();
                this.oClient.Dispose();
                this.oClient = null;
            }
        }

        bool lLogEnabled = true;
        private void BtnLog_Click(object sender, EventArgs e)
        {
            //bool lRet = (this.oClient != null && this.oClient.IsOpened());
            //Exception Error = null;
            //string token = "";
            //string cOperation = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_ENABLED_LOGS;
            //lLogEnabled = !lLogEnabled;
            //if (lLogEnabled)
            //    cOperation = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_ENABLED_LOGS;
            //else
            //    cOperation = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DISABLED_LOGS;
                
            //if (lRet)
            //{
            //    Wintic.Data.IConnection con = Riepiloghi.clsWinServiceOperations.GetConnection();

            //    token = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(con, "P0000399", "BBBBBB", cOperation, out Error);
            //    lRet = (!string.IsNullOrEmpty(token) && Error == null);
            //    con.Close();
            //    con.Dispose();
            //}
            //if (lRet)
            //{
            //    Riepiloghi.clsWinServiceModel_request_Operation request = new Riepiloghi.clsWinServiceModel_request_Operation();
            //    request.Token = token;
            //    request.Code = cOperation;
            //    if (oClient.Send(Newtonsoft.Json.JsonConvert.SerializeObject(request)))
            //    {
            //    }
            //}
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            this.InitClient();
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            //bool lRet = (this.oClient != null && this.oClient.IsOpened());
            //Exception Error = null;
            //string token = "";
            //if (lRet)
            //{
            //    Wintic.Data.IConnection con = Riepiloghi.clsWinServiceOperations.GetConnection();
            //    token = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(con, "P0000399", "BBBBBB", Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_SEND_EMAIL, out Error);
            //    lRet = (!string.IsNullOrEmpty(token) && Error == null);
            //    con.Close();
            //    con.Dispose();
            //}
            //if (lRet)
            //{
            //    Riepiloghi.clsWinServiceModel_request_Operation request = new Riepiloghi.clsWinServiceModel_request_Operation();
            //    request.Code = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_SEND_EMAIL;
            //    if (oClient.Send(Newtonsoft.Json.JsonConvert.SerializeObject(request)))
            //    {
            //    }
            //}

        }
        private delegate void delegateSetResponse(Riepiloghi.clsWinServiceModel_response_Operation response);

        private void ClsWinServiceOperations_EventServerSocketRiepiloghi(Riepiloghi.clsWinServiceModel_response_Operation response)
        {
            if (this.InvokeRequired)
            {
                delegateSetResponse oD = new delegateSetResponse(ClsWinServiceOperations_EventServerSocketRiepiloghi);
                this.Invoke(oD, response);
            }
            else
            {
                this.txtMessages.Text += string.Format("\r\n" + "Code: {0}", response.Code);
                this.txtMessages.Text += string.Format("\r\n" + "Message: {0}", response.Message);
                this.txtMessages.SelectionLength = 0;
                this.txtMessages.SelectionStart = this.txtMessages.Text.Length;
                this.txtMessages.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void btnUpdates_Click(object sender, EventArgs e)
        {
            //bool lRet = (this.oClient != null && this.oClient.IsOpened());
            //Exception Error = null;
            //string token = "";
            //if (lRet)
            //{
            //    Wintic.Data.IConnection con = Riepiloghi.clsWinServiceOperations.GetConnection();
            //    token = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(con, "P0000399", "BBBBBB", Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES, out Error);
            //    lRet = (!string.IsNullOrEmpty(token) && Error == null);
            //    con.Close();
            //    con.Dispose();
            //}
            //if (lRet)
            //{
            //    Riepiloghi.clsWinServiceModel_request_Operation request = new Riepiloghi.clsWinServiceModel_request_Operation();
            //    request.Code = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES;
            //    if (oClient.Send(Newtonsoft.Json.JsonConvert.SerializeObject(request)))
            //    {
            //    }
            //}
        }

        private void btnGenera_Click(object sender, EventArgs e)
        {
            bool lRet = (this.oWindowsServiceServer != null && (this.oWindowsServiceServer.Configuration.ComunicationMode == Riepiloghi.clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_VIA_DB || (this.oClient != null && this.oClient.IsOpened())));
            Exception oErr = null;
            string token = "";
            if (lRet)
            {
                Wintic.Data.IConnection oCon = Riepiloghi.clsWinServiceOperations.GetConnection("RIEPILOGHI-CLIENT");
                Riepiloghi.clsWinServiceClientOperation oClientOperation = new Riepiloghi.clsWinServiceClientOperation(Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WIN_SERVICE);
                long IdOperazione = 0;

                Riepiloghi.clsWinServiceConfig config = Riepiloghi.clsWinServiceConfig.GetWinServiceConfig(oCon, out oErr, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WIN_SERVICE);
                lRet = oErr == null;

                if (lRet)
                    //lRet = oClientOperation.PushOperazione(oCon, "P0000399", "C3E82E0CC8BC4E64AEE656C5B7E62A5C", Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, "G", this.dtGiorno.Value.Date, 0, out IdOperazione, out oErr, "");
                oCon.Close();
                oCon.Dispose();
            }

            if (oErr != null)
                MessageBox.Show(oErr.ToString());
        }

        private void BtnDebug_Click(object sender, EventArgs e)
        {
            //bool lRet = (this.oClient != null && this.oClient.IsOpened());
            //Exception Error = null;
            //string token = "";
            //if (lRet)
            //{
            //    Wintic.Data.IConnection con = Riepiloghi.clsWinServiceOperations.GetConnection();
            //    token = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(con, "P0000399", "BBBBBB", Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, out Error);
            //    lRet = (!string.IsNullOrEmpty(token) && Error == null);
            //    con.Close();
            //    con.Dispose();
            //}
            //if (lRet)
            //{
            //    Riepiloghi.clsWinServiceModel_request_Operation request = new Riepiloghi.clsWinServiceModel_request_Operation();
            //    request.Code = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DEBUG;
            //    request.Token = token;
            //    if (oClient.Send(Newtonsoft.Json.JsonConvert.SerializeObject(request)))
            //    {
            //    }
            //}
        }



    }
}
