﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSocketServerGenRiepiloghi
{
    public partial class FormServersocket : Form
    {
        private Riepiloghi.SocketServerRiepiloghi SocketServerRiepiloghi;
        public FormServersocket()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.SocketServerRiepiloghi = new Riepiloghi.SocketServerRiepiloghi("127.0.0.1", 12001,"RIEPILOGHI-CLIENT","");
            this.SocketServerRiepiloghi.StartListen();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.SocketServerRiepiloghi.StopListener();
        }
    }
}
