﻿namespace TestRichiestaCardServerFiscale
{
    partial class frmTestRichiestaCardServerFiscale
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRUN = new System.Windows.Forms.Button();
            this.chkStepByStep = new System.Windows.Forms.CheckBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.panelTest = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkSmartCard = new System.Windows.Forms.CheckBox();
            this.panelTest.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRUN
            // 
            this.btnRUN.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRUN.Location = new System.Drawing.Point(5, 51);
            this.btnRUN.Name = "btnRUN";
            this.btnRUN.Size = new System.Drawing.Size(788, 53);
            this.btnRUN.TabIndex = 0;
            this.btnRUN.Text = "(1) Connessione DB";
            this.btnRUN.UseVisualStyleBackColor = true;
            this.btnRUN.Click += new System.EventHandler(this.btnRUN_Click);
            // 
            // chkStepByStep
            // 
            this.chkStepByStep.AutoSize = true;
            this.chkStepByStep.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkStepByStep.Location = new System.Drawing.Point(5, 28);
            this.chkStepByStep.Name = "chkStepByStep";
            this.chkStepByStep.Size = new System.Drawing.Size(788, 23);
            this.chkStepByStep.TabIndex = 1;
            this.chkStepByStep.Text = "Step By Step";
            this.chkStepByStep.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(5, 145);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(788, 298);
            this.txtLog.TabIndex = 2;
            // 
            // panelTest
            // 
            this.panelTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTest.Controls.Add(this.txtLog);
            this.panelTest.Controls.Add(this.btnClear);
            this.panelTest.Controls.Add(this.btnRUN);
            this.panelTest.Controls.Add(this.chkStepByStep);
            this.panelTest.Controls.Add(this.chkSmartCard);
            this.panelTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTest.Location = new System.Drawing.Point(0, 0);
            this.panelTest.Name = "panelTest";
            this.panelTest.Padding = new System.Windows.Forms.Padding(5);
            this.panelTest.Size = new System.Drawing.Size(800, 450);
            this.panelTest.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnClear.Location = new System.Drawing.Point(5, 104);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(788, 41);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Pulisci log e ricomincia da capo";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkSmartCard
            // 
            this.chkSmartCard.AutoSize = true;
            this.chkSmartCard.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkSmartCard.Location = new System.Drawing.Point(5, 5);
            this.chkSmartCard.Name = "chkSmartCard";
            this.chkSmartCard.Size = new System.Drawing.Size(788, 23);
            this.chkSmartCard.TabIndex = 4;
            this.chkSmartCard.Text = "Leggi SN Card";
            this.chkSmartCard.UseVisualStyleBackColor = true;
            // 
            // frmTestRichiestaCardServerFiscale
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panelTest);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmTestRichiestaCardServerFiscale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Richiesta Card Server Fiscale";
            this.panelTest.ResumeLayout(false);
            this.panelTest.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRUN;
        private System.Windows.Forms.CheckBox chkStepByStep;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Panel panelTest;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox chkSmartCard;
    }
}

