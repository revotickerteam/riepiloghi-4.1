﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;
using System.Diagnostics;
using Riepiloghi;
using libRiepiloghiBase;
using System.IO;

namespace TestRichiestaCardServerFiscale
{
    public partial class frmTestRichiestaCardServerFiscale : Form
    {

        private IConnection connection;
        private clsParametriServerFiscale oParametriServerFiscale = null;
        private int passo = 0;
        private DateTime lastOp = DateTime.MinValue;
        private int nSlotCarta = 0;
        private bool lCartaRichiestaAlServerFiscale = false;
        private libRiepiloghiBase.iLib_SIAE_Provider oSiaeProvider;

        private string file = "TextFileTEST.txt";
        private string email = "EmailFileTest.eml";
        private string serial = "";
        private string pin = "";
        private string mailFrom = "";
        private string mailTo = "";

        public frmTestRichiestaCardServerFiscale()
        {
            InitializeComponent();
            this.Shown += FrmTestRichiestaCardServerFiscale_Shown;
        }


        private void FrmTestRichiestaCardServerFiscale_Shown(object sender, EventArgs e)
        {
            this.InitForm();
        }

        private void InitForm()
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                connection = null;
            }
            oSiaeProvider = null;
            nSlotCarta = 0;
            lCartaRichiestaAlServerFiscale = false;
            lastOp = DateTime.MinValue;
            pin = "";
            mailFrom = "";
            mailTo = "";
            passo = 1;
            this.setDescrizioneRUN();
            this.btnRUN.Enabled = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtLog.Text = "";
            InitForm();
        }

        private void btnRUN_Click(object sender, EventArgs e)
        {
            if (this.chkSmartCard.Checked)
            {
                testSmartCard();
            }
            else
                EseguiPasso();
        }

        private int Passo
        {
            get { return passo; }
            set { passo = value; setDescrizioneRUN(); }
        }

        private void setDescrizioneRUN()
        {
            string desc = "";
            switch (Passo)
            {
                case 1: { desc = "connessione DB"; break; }
                case 2: { desc = "lettura parametri server fiscale"; break; }
                case 3: { desc = "verifica server fiscale..."; break; }
                case 4: { desc = "chiedi carta"; break; }
                case 5: { desc = "firma un file"; break; }
                case 6: { desc = "genera email"; break; }
                case 7: { desc = "restituisci carta e chiudi conn."; break; }
            }
            this.btnRUN.Text = string.Format("({0}) {1}", Passo.ToString(), desc);
            Application.DoEvents();
        }

        private void AddText(string msg)
        {
            TimeSpan ts = DateTime.Now - lastOp;
            string elapsed = ts.TotalMilliseconds.ToString();
            this.txtLog.Text += string.Format("{0} {1} {2}ms", (this.txtLog.Text.Length > 0 ? "\r\n" : ""), msg, elapsed);
            this.txtLog.SelectionLength = 0;
            this.txtLog.SelectionStart = this.txtLog.Text.Length;
            this.txtLog.ScrollToCaret();
            Application.DoEvents();
            lastOp = DateTime.Now;
        }

        private void EseguiPasso()
        {
            switch (Passo)
            {
                case 1: { ApriConnessione(); break; }
                case 2: { InitParametriServerFiscale(); break; }
                case 3: { VerificaServerFiscale(); break; }
                case 4: { ChiediCarta(); break; }
                case 5: { FirmaFile(); break; }
                case 6: { FaiEmail(); break; }
                case 7: { RilasciaCarta(); break; }
            }
        }

        public string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        private void ApriConnessione()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            this.txtLog.Text = "";
            lastOp = DateTime.Now;
            AddText(GetCurrentMethod() + "...");
            try
            {

                connection = UtilityTest.GetConnection();


                AddText(string.Format("OK {0}", GetCurrentMethod()));
                this.btnRUN.Enabled = ret;
                Passo += 1;
                if (!this.chkStepByStep.Checked) EseguiPasso();
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }
        }

        private void InitParametriServerFiscale()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;
                oParametriServerFiscale = clsRiepiloghi.GetParametriServerFiscale(connection, out oError);
                AddText(string.Format("OK {0} {1} {2}", GetCurrentMethod(), (oParametriServerFiscale == null ? "" : oParametriServerFiscale.ServerIP), (oParametriServerFiscale == null ? "" : oParametriServerFiscale.Port.ToString())));
                this.btnRUN.Enabled = ret;
                Passo += 1;
                if (!this.chkStepByStep.Checked) EseguiPasso();
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }
            Application.DoEvents();
        }



        private void VerificaServerFiscale()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;
                ret = clsRiepiloghi.CheckServerFiscale(connection, out oError);
                AddText(string.Format(string.Format("{0} {1} {2}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message))));
                this.btnRUN.Enabled = ret;
                if (ret)
                {
                    Passo += 1;
                    if (!this.chkStepByStep.Checked) EseguiPasso();
                }
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }

        }

        private void testSmartCard()
        {
            Exception oError = null;
            if (oSiaeProvider == null)
            {
                AddText("Prendo dll SIAE...");
                //oSiaeProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError);
            }
            string serial = clsLibSigillo.GetSerialNumber(nSlotCarta, out oError);

        }

        private void ChiediCarta()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;
                AddText("Chiedo carta...");
                using (clsClientServerFiscale oClientServerFiscale = new clsClientServerFiscale())
                {
                    bool requestSentToServerFiscale = false;
                    ret = oClientServerFiscale.RichiestaUtilizzoSmartCard(oParametriServerFiscale, out nSlotCarta, out requestSentToServerFiscale);
                    lCartaRichiestaAlServerFiscale = requestSentToServerFiscale;
                    AddText(string.Format(string.Format("{0} {1} {2}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message))));
                    AddText(string.Format(string.Format("{0} {1}  carta ottenuta {2} Slot {3}", (ret ? "OK" : "ERR"), GetCurrentMethod(), lCartaRichiestaAlServerFiscale.ToString().ToUpper(), nSlotCarta.ToString())));
                }

                if (ret)
                {
                    AddText("Prendo serial number...");
                    serial = clsLibSigillo.GetSerialNumber(nSlotCarta, out oError);
                    ret = oError == null;
                    AddText(string.Format(string.Format("{0} {1} {2} S.N. {3}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message), serial)));
                }

                if (ret)
                {
                    AddText("Prendo pin, email from ed email to da db...");
                    StringBuilder oSB;
                    clsParameters oPars;
                    IRecordSet oRS;

                    try
                    {
                        oSB = new StringBuilder();
                        oPars = new clsParameters();

                        oSB.Append("SELECT ");
                        oSB.Append(" SMART_CARD.CODICE_SISTEMA,");
                        oSB.Append(" SMART_CARD.EMAIL_TITOLARE,");
                        oSB.Append(" SMART_CARD.EMAIL_SIAE,");
                        oSB.Append(" SMART_CODE.PIN ");
                        oSB.Append(" FROM SMART_CS, SMART_CARD, SMART_CODE ");
                        oSB.Append(" WHERE ");
                        oSB.Append(" SMART_CS.ENABLED = 1 AND ");
                        oSB.Append(" SMART_CARD.ENABLED = 1 AND ");
                        oSB.Append(" SMART_CARD.CODICE_SISTEMA = SMART_CS.CODICE_SISTEMA ");
                        oSB.Append(" AND SMART_CARD.SN_CARD = :pSN_CARD AND ");
                        oSB.Append(" SMART_CARD.IDCARD = SMART_CODE.IDCARD(+)");


                        oPars.Add(":pSN_CARD", serial);
                        oRS = (IRecordSet)connection.ExecuteQuery(oSB.ToString(), oPars);
                        if (oRS.EOF)
                        {
                            ret = false;
                            oError = new Exception("smartcard non codificata nel sistema " + serial);
                        }
                        else
                        {
                            pin = oRS.Fields("pin").Value.ToString();
                            mailFrom = oRS.Fields("email_titolare").Value.ToString();
                            mailTo = oRS.Fields("email_siae").Value.ToString();
                        }
                        oRS.Close();
                        AddText(string.Format(string.Format("{0} {1} {2} S.N. {3} PIN {4} from {5} to {6}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message), serial, pin, mailFrom, mailTo)));

                    }
                    catch (Exception exInitCodiceSistemaPin)
                    {
                        ret = false;
                        oError = exInitCodiceSistemaPin;
                    }

                }

                this.btnRUN.Enabled = ret;
                if (ret)
                {
                    Passo += 1;
                    if (!this.chkStepByStep.Checked) EseguiPasso();
                }
                else
                    RilasciaCarta();
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }

        }

        private void FirmaFile()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;

                if (File.Exists(file + ".p7m"))
                {
                    AddText("Elimino " + file + ".p7m");
                    File.Delete(file + ".p7m");
                }

                if (oSiaeProvider == null)
                {
                    AddText("Prendo dll SIAE...");
                    System.Reflection.Assembly oAssembly = null;

                    oSiaeProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError, out oAssembly);
                    ret = oError == null;
                }

                if (ret)
                {
                    AddText(string.Format("firma file {0}", file));
                    oSiaeProvider.FirmaFile(pin, nSlotCarta, file, out oError);
                    ret = oError == null;
                }

                AddText(string.Format(string.Format("{0} {1} {2}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message))));

                this.btnRUN.Enabled = ret;
                if (ret)
                {
                    Passo += 1;
                    if (!this.chkStepByStep.Checked) EseguiPasso();
                }
                else
                    RilasciaCarta();
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }

        }

        private void FaiEmail()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;

                if (File.Exists(email))
                {
                    AddText("Elimino " + email);
                    File.Delete(email);
                }

                if (oSiaeProvider == null)
                {
                    AddText("Prendo dll SIAE...");
                    System.Reflection.Assembly oAssembly = null;

                    oSiaeProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError, out oAssembly);
                    ret = oError == null;
                }


                if (ret)
                {
                    AddText("genero email");
                    ret = oSiaeProvider.GeneraEmail(pin, nSlotCarta, email, file, mailFrom, mailTo, "", "Soggetto", out oError) && oError == null;
                }

                AddText(string.Format(string.Format("{0} {1} {2}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message))));

                this.btnRUN.Enabled = ret;
                if (ret)
                {
                    Passo += 1;
                    if (!this.chkStepByStep.Checked) EseguiPasso();
                }
                else
                    RilasciaCarta();
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }

        }

        private void RilasciaCarta()
        {
            bool ret = true;
            this.btnRUN.Enabled = false;
            AddText(GetCurrentMethod() + "...");
            try
            {
                Exception oError = null;

                if (lCartaRichiestaAlServerFiscale)
                {
                    using (clsClientServerFiscale oClientServerFiscale = new clsClientServerFiscale())
                    {
                        oClientServerFiscale.RichiestaRilascioSmartCard(oParametriServerFiscale);
                        oError = oClientServerFiscale.ExceptionClient;
                    }
                }

                AddText(string.Format(string.Format("{0} {1} {2}", (ret ? "OK" : "ERR"), GetCurrentMethod(), (oError == null ? "" : oError.Message))));

                this.btnRUN.Enabled = ret;
                if (ret)
                {
                    InitForm();
                }
            }
            catch (Exception ex)
            {
                ret = false;
                AddText(string.Format("Err. {0} {1}", GetCurrentMethod(), ex.Message));
            }

        }
    }
}
