﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Wintic.Data;
using Wintic.Data.oracle;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Text;

namespace WebServiceRiepiloghi
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServiceRiepiloghi : System.Web.Services.WebService
    {
        // Classe di risposta base
        public class ResultBase
        {
            [XmlElement("statusok")]
            public bool StatusOK = true;

            [XmlElement("statusmessage")]
            public string StatusMessage = "";

            [XmlElement("statuscode")]
            public Int64 StatusCode = 0;

            [XmlElement("ExecutionTime")]
            public string execution_time = "";

            public ResultBase()
            {
            }

            public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }
        }

        #region "funzioni di base"

        // funzione che Recupera la connessione
        public static Wintic.Data.oracle.CConnectionOracle GetConnection(string ServiceName, out Exception oError)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ServiceName);
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch(Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione.", exGetConnection);
            }
            return oCon;
        }

        // Converte una stringa nella modalità storico: TRUE, FALSE, CHECK
        private Riepiloghi.clsRiepiloghi.DefModalitaStorico GetModalitaStoricoFromString(string cValue)
        {
            Riepiloghi.clsRiepiloghi.DefModalitaStorico oModalitaStorico = Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check;
            foreach (Riepiloghi.clsRiepiloghi.DefModalitaStorico ItemEnum in Enum.GetValues(typeof(Riepiloghi.clsRiepiloghi.DefModalitaStorico)))
            {
                if (ItemEnum.ToString().Trim().ToUpper() == cValue.Trim().ToUpper())
                {
                    oModalitaStorico = ItemEnum;
                    break;
                }
            }
            return oModalitaStorico;
        }

        public static bool CheckUserAmministratore(IConnection oConnection, string User, string Password, out Exception oError)
        {
            bool lRet = false;
            oError = null;
            lRet = Riepiloghi.clsRiepiloghi.CheckUserAmministratore(oConnection, User, Password, out oError);
            return lRet;
        }

        // Verifica se necessaria login e password per le funzioni e verifica le credenziali passate
        public static bool CheckUserOperazione(IConnection oConnection, string Operazione, string User, string Password, out Exception oError)
        {
            bool lRet = false;
            oError = null;
            lRet = Riepiloghi.clsRiepiloghi.CheckUserOperazione(oConnection, Operazione, User, Password, out oError);
            return lRet;
        }

        public static bool CheckUserPolicyEnabled(IConnection oConnection)
        {
            return Riepiloghi.clsRiepiloghi.CheckUserPolicyEnabled(oConnection);
        }

        #endregion

        #region "funzioni di Consultazione"

        #region "Codice sistema"

        // Calsse di risposta con anche codice sistema
        public class ResultBaseCodiceSistema : ResultBase
        {
            public string CodiceSistema = "";

            public ResultBaseCodiceSistema()
            {
            }

            public ResultBaseCodiceSistema(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceSistema)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.CodiceSistema = cCodiceSistema;
            }
        }

        // Richiesrta codice sistema
        [WebMethod]
        public ResultBaseCodiceSistema GetCodiceSistema(string ServiceName, string User, string Password)
        {
            ResultBaseCodiceSistema oRet = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cCodiceSistema = "";
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI", User, Password, out oError))
                    {
                        cCodiceSistema = Riepiloghi.clsRiepiloghi.GetCodiceSistema(oConnection, out oError);
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseCodiceSistema(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "");
            }
            else
            {
                oRet = new ResultBaseCodiceSistema(true, "CodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cCodiceSistema); ;
            }
            return oRet;
        }

        #endregion

        #region "Controllo server fiscale"

        // Controllo serve fiscale acceso
        [WebMethod]
        public ResultBase CheckServerFiscale(string ServiceName)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            bool lRet = false;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    lRet = Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError);
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione server fiscale", "Errore imprevisto nella connessione al server fiscale.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                return new ResultBase(lRet, lRet.ToString(), 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
        }

        #endregion

        #region "GetTransazioni, GetTransazioniAnnullati, GetTransazioneCARTA_PROGRESSIVO, GetTransazioneCARTA_SIGILLO"

        // Classe per filtri transazioni
        [XmlRoot("dictionary")]
        public class clsDizionarioFiltri<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }
        }

        // Classe parametro per estrarre transazioni filtrate
        public class clsFiltroTransazioni
        {
            public clsDizionarioFiltri<string, object> Filtri;
            
            public clsFiltroTransazioni()
            {
            }
            
            public clsFiltroTransazioni(IConnection Connection, string cFiltro, out Exception oError)
            {
                oError = null;
                this.Filtri = this.DecodeFiltri(cFiltro, Connection, out oError);
            }

            public clsFiltroTransazioni(clsDizionarioFiltri<string, object> IFiltri)
            {
                this.Filtri = IFiltri;
            }

            // Compone una lista di filtri per la lista delle transazioni
            private clsDizionarioFiltri<string, object> DecodeFiltri(string cFiltri, IConnection oConnection, out Exception oError)
            {
                clsDizionarioFiltri<string, object> oRet = null;
                oError = null;
                if (cFiltri.Trim() != "")
                {
                    System.Collections.SortedList oListaCampi = Riepiloghi.clsRiepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError);
                    if (oError == null && oListaCampi != null && oListaCampi.Count > 0)
                    {
                        oRet = new clsDizionarioFiltri<string, object>();
                        if (!cFiltri.Contains(";"))
                        {
                            cFiltri += ";";
                        }
                        foreach (string cValoreFiltro in cFiltri.Split(';'))
                        {
                            if (cValoreFiltro.Contains("="))
                            {
                                string cCampo = cValoreFiltro.Split('=')[0];
                                string cValue = cValoreFiltro.Split('=')[1];
                                Riepiloghi.clsRiepiloghi.clsCampoTransazione oCampo = null;
                                foreach (System.Collections.DictionaryEntry ItemCampoDic in oListaCampi)
                                {
                                    Riepiloghi.clsRiepiloghi.clsCampoTransazione oItemCampo = (Riepiloghi.clsRiepiloghi.clsCampoTransazione)ItemCampoDic.Value;
                                    if (oItemCampo.Campo == cCampo)
                                    {
                                        oCampo = oItemCampo;
                                        break;
                                    }
                                }

                                if (oCampo == null)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "campo filtro [" + cCampo + "] non valido.");
                                }
                                else
                                {
                                    if (oCampo.Tipo == "S")
                                    {
                                        if (cValue.Length > oCampo.Dimensione)
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore TESTO per il campo " + cCampo + " troppo lungo " + cValue);
                                        }
                                        else
                                        {
                                            oRet.Add(oCampo.Campo, cValue.PadRight(oCampo.Dimensione));
                                        }

                                    }
                                    else if (oCampo.Tipo == "N")
                                    {
                                        Int64 nValue = 0;
                                        if (Int64.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore NUMERICO per il campo " + cCampo + " non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "C")
                                    {
                                        decimal nValue = 0;
                                        if (decimal.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore IMPORTO per il campo " + cCampo + " [0,00] non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "D")
                                    {
                                        DateTime dValue;
                                        if (cValue.Length == 8)
                                        {
                                            try
                                            {
                                                dValue = new DateTime(int.Parse(cValue.Substring(0, 4)), int.Parse(cValue.Substring(4, 2)), int.Parse(cValue.Substring(6, 2)));
                                                oRet.Add(oCampo.Campo, dValue);
                                            }
                                            catch
                                            {
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore DATA per il campo " + cCampo + " [YYYYMMDD] non valido " + cValue);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (oError == null)
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Errore durante la lettura dei campi di filtro.");
                        }
                    }
                }
                return oRet;
            }
        }

        // Classe con i campi del log delle transazioni
        public class clsResultTransazione
        {
            public string CodiceFiscaleOrganizzatore = "";
            public string CodiceFiscaleTitolare = "";
            public string TitoloAbbonamento = "";
            public string TitoloIvaPreassolta = "";
            public string SpettacoloIntrattenimento = "";
            public string Valuta = "";
            public decimal ImponibileIntrattenimenti = 0;
            public string OrdineDiPosto = "";
            public string Posto = "";
            public string TipoTitolo = "";
            public string Annullato = "";
            public decimal ProgressivoAnnullati = 0;
            public DateTime DataEmissioneAnnullamento = DateTime.MinValue;
            public string OraEmissioneAnnullamento = "";
            public Int64 ProgressivoTitolo = 0;
            public string CodicePuntoVendita = "";
            public string Sigillo = "";
            public string CodiceSistema = "";
            public string CodiceCarta = "";
            public string Prestampa = "";
            public string CodiceLocale = "";
            public DateTime DataEvento = DateTime.MinValue;
            public string TipoEvento = "";
            public string TitoloEvento = "";
            public string OraEvento = "";
            public string CausaleOmaggioRiduzione = "";
            public string TipoTurno = "";
            public Int64 NumeroEventiAbilitati = 0;
            public DateTime DataLimiteValidita = DateTime.MinValue;
            public string CodiceAbbonamento = "";
            public decimal NumProgAbbonamento = 0;
            public decimal RateoEvento = 0;
            public decimal IvaRateo = 0;
            public decimal RateoImponibileIntra = 0;
            public string CausaleOmaggioRiduzioneOpen = "";
            public decimal CorrispettivoTitolo = 0;
            public decimal CorrispettivoPrevendita = 0;
            public decimal IvaTitolo = 0;
            public decimal IvaPrevendita = 0;
            public decimal CorrispettivoFigurativo = 0;
            public decimal IvaFigurativa = 0;
            public string CodiceFiscaleAbbonamento = "";
            public string CodiceBigliettoAbbonamento = "";
            public Int64 NumProgBigliettoAbbonamento = 0;
            public string CodicePrestazione1 = "";
            public decimal ImportoPrestazione1 = 0;
            public string CodicePrestazione2 = "";
            public decimal ImportoPrestazione2 = 0;
            public string CodicePrestazione3 = "";
            public decimal ImportoPrestazione3 = 0;
            public string CartaOriginaleAnnullato = "";
            public string CausaleAnnullamento = "";

            public clsResultTransazione()
            {
            }

            public clsResultTransazione(IRecordSet oRS)
            {
                if (oRS != null && !oRS.EOF)
                {
                    this.CodiceFiscaleOrganizzatore = (oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").Value.ToString());
                    this.CodiceFiscaleTitolare = (oRS.Fields("CODICE_FISCALE_TITOLARE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_TITOLARE").Value.ToString());
                    this.TitoloAbbonamento = (oRS.Fields("TITOLO_ABBONAMENTO").IsNull ? "" : oRS.Fields("TITOLO_ABBONAMENTO").Value.ToString());
                    this.TitoloIvaPreassolta = (oRS.Fields("TITOLO_IVA_PREASSOLTA").IsNull ? "" : oRS.Fields("TITOLO_IVA_PREASSOLTA").Value.ToString());
                    this.SpettacoloIntrattenimento = (oRS.Fields("SPETTACOLO_INTRATTENIMENTO").IsNull ? "" : oRS.Fields("SPETTACOLO_INTRATTENIMENTO").Value.ToString());
                    this.Valuta = (oRS.Fields("VALUTA").IsNull ? "" : oRS.Fields("VALUTA").Value.ToString());
                    this.ImponibileIntrattenimenti = (oRS.Fields("IMPONIBILE_INTRATTENIMENTI").IsNull ? 0 : decimal.Parse(oRS.Fields("IMPONIBILE_INTRATTENIMENTI").Value.ToString()));
                    this.OrdineDiPosto = (oRS.Fields("ORDINE_DI_POSTO").IsNull ? "" : oRS.Fields("ORDINE_DI_POSTO").Value.ToString());
                    this.Posto = (oRS.Fields("POSTO").IsNull ? "" : oRS.Fields("POSTO").Value.ToString());
                    this.TipoTitolo = (oRS.Fields("TIPO_TITOLO").IsNull ? "" : oRS.Fields("TIPO_TITOLO").Value.ToString());
                    this.Annullato = (oRS.Fields("ANNULLATO").IsNull ? "" : oRS.Fields("ANNULLATO").Value.ToString());
                    this.ProgressivoAnnullati = (oRS.Fields("PROGRESSIVO_ANNULLATI").IsNull ? 0 : Int64.Parse(oRS.Fields("PROGRESSIVO_ANNULLATI").Value.ToString()));
                    this.DataEmissioneAnnullamento = (oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").Value);
                    this.OraEmissioneAnnullamento = (oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").Value.ToString());
                    this.ProgressivoTitolo = (oRS.Fields("PROGRESSIVO_TITOLO").IsNull ? 0 : Int64.Parse(oRS.Fields("PROGRESSIVO_TITOLO").Value.ToString()));
                    this.CodicePuntoVendita = (oRS.Fields("CODICE_PUNTO_VENDITA").IsNull ? "" : oRS.Fields("CODICE_PUNTO_VENDITA").Value.ToString());
                    this.Sigillo = (oRS.Fields("SIGILLO").IsNull ? "" : oRS.Fields("SIGILLO").Value.ToString());
                    this.CodiceSistema = (oRS.Fields("CODICE_SISTEMA").IsNull ? "" : oRS.Fields("CODICE_SISTEMA").Value.ToString());
                    this.CodiceCarta = (oRS.Fields("CODICE_CARTA").IsNull ? "" : oRS.Fields("CODICE_CARTA").Value.ToString());
                    this.Prestampa = (oRS.Fields("PRESTAMPA").IsNull ? "" : oRS.Fields("PRESTAMPA").Value.ToString());
                    this.CodiceLocale = (oRS.Fields("CODICE_LOCALE").IsNull ? "" : oRS.Fields("CODICE_LOCALE").Value.ToString());
                    this.DataEvento = (oRS.Fields("DATA_EVENTO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_EVENTO").Value);
                    this.TipoEvento = (oRS.Fields("TIPO_EVENTO").IsNull ? "" : oRS.Fields("TIPO_EVENTO").Value.ToString());
                    this.TitoloEvento = (oRS.Fields("TITOLO_EVENTO").IsNull ? "" : oRS.Fields("TITOLO_EVENTO").Value.ToString());
                    this.OraEvento = (oRS.Fields("ORA_EVENTO").IsNull ? "" : oRS.Fields("ORA_EVENTO").Value.ToString());
                    this.CausaleOmaggioRiduzione = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").Value.ToString());
                    this.TipoTurno = (oRS.Fields("TIPO_TURNO").IsNull ? "" : oRS.Fields("TIPO_TURNO").Value.ToString());
                    this.NumeroEventiAbilitati = (oRS.Fields("NUMERO_EVENTI_ABILITATI").IsNull ? 0 : Int64.Parse(oRS.Fields("NUMERO_EVENTI_ABILITATI").Value.ToString()));
                    this.DataLimiteValidita = (oRS.Fields("DATA_LIMITE_VALIDITA").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_LIMITE_VALIDITA").Value);
                    this.CodiceAbbonamento = (oRS.Fields("CODICE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_ABBONAMENTO").Value.ToString());
                    this.NumProgAbbonamento = (oRS.Fields("NUM_PROG_ABBONAMENTO").IsNull ? 0 : Int64.Parse(oRS.Fields("NUM_PROG_ABBONAMENTO").Value.ToString()));
                    this.RateoEvento = (oRS.Fields("RATEO_EVENTO").IsNull ? 0 : decimal.Parse(oRS.Fields("RATEO_EVENTO").Value.ToString()));
                    this.IvaRateo = (oRS.Fields("IVA_RATEO").IsNull ? 0 : decimal.Parse(oRS.Fields("IVA_RATEO").Value.ToString()));
                    this.RateoImponibileIntra = (oRS.Fields("RATEO_IMPONIBILE_INTRA").IsNull ? 0 : decimal.Parse(oRS.Fields("RATEO_IMPONIBILE_INTRA").Value.ToString()));
                    this.CausaleOmaggioRiduzioneOpen = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").Value.ToString());
                    this.CorrispettivoTitolo = (oRS.Fields("CORRISPETTIVO_TITOLO").IsNull ? 0 : decimal.Parse(oRS.Fields("CORRISPETTIVO_TITOLO").Value.ToString()));
                    this.CorrispettivoPrevendita = (oRS.Fields("CORRISPETTIVO_PREVENDITA").IsNull ? 0 : decimal.Parse(oRS.Fields("CORRISPETTIVO_PREVENDITA").Value.ToString()));
                    this.IvaTitolo = (oRS.Fields("IVA_TITOLO").IsNull ? 0 : decimal.Parse(oRS.Fields("IVA_TITOLO").Value.ToString()));
                    this.IvaPrevendita = (oRS.Fields("IVA_PREVENDITA").IsNull ? 0 : decimal.Parse(oRS.Fields("IVA_PREVENDITA").Value.ToString()));
                    this.CorrispettivoFigurativo = (oRS.Fields("CORRISPETTIVO_FIGURATIVO").IsNull ? 0 : decimal.Parse(oRS.Fields("CORRISPETTIVO_FIGURATIVO").Value.ToString()));
                    this.IvaFigurativa = (oRS.Fields("IVA_FIGURATIVA").IsNull ? 0 : decimal.Parse(oRS.Fields("IVA_FIGURATIVA").Value.ToString()));
                    this.CodiceFiscaleAbbonamento = (oRS.Fields("CODICE_FISCALE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ABBONAMENTO").Value.ToString());
                    this.CodiceBigliettoAbbonamento = (oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").Value.ToString());
                    //this.NumProgBigliettoAbbonamento = (oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").IsNull || oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").Value.ToString() == "" ? 0 : Int64.Parse(oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").Value.ToString()));
                    this.CodicePrestazione1 = (oRS.Fields("CODICE_PRESTAZIONE1").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE1").Value.ToString());
                    this.ImportoPrestazione1 = (oRS.Fields("IMPORTO_PRESTAZIONE1").IsNull || oRS.Fields("IMPORTO_PRESTAZIONE1").Value.ToString() == "" ? 0 : decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE1").Value.ToString()));
                    this.CodicePrestazione2 = (oRS.Fields("CODICE_PRESTAZIONE2").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE2").Value.ToString());
                    this.ImportoPrestazione2 = (oRS.Fields("IMPORTO_PRESTAZIONE2").IsNull || oRS.Fields("IMPORTO_PRESTAZIONE2").Value.ToString() == "" ? 0 : decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE2").Value.ToString()));
                    this.CodicePrestazione3 = (oRS.Fields("CODICE_PRESTAZIONE3").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE3").Value.ToString());
                    this.ImportoPrestazione3 = (oRS.Fields("IMPORTO_PRESTAZIONE3").IsNull || oRS.Fields("IMPORTO_PRESTAZIONE3").Value.ToString() == "" ? 0 : decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE3").Value.ToString()));
                    this.CartaOriginaleAnnullato = (oRS.Fields("CARTA_ORIGINALE_ANNULLATO").IsNull ? "" : oRS.Fields("CARTA_ORIGINALE_ANNULLATO").Value.ToString());
                    this.CausaleAnnullamento = (oRS.Fields("CAUSALE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("CAUSALE_ANNULLAMENTO").Value.ToString());
                }
            }
        }

        // Classe singolo campo log transazioni
        public class clsResultBaseCampoLogTransazioni 
        {
            public string Campo = "";
            public string Tipo = "";
            public int Inizio = 0;
            public int Dimensione = 0;
            public string DescCampo = "";
            public string Descrizione = "";

            public clsResultBaseCampoLogTransazioni()
            {
            }
            
            public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione)
            {
                this.Campo = cCampo;
                this.Tipo = cTipo;
                this.Inizio = nInizio;
                this.Dimensione = nDimensione;
                this.DescCampo = cDescCampo;
                this.Descrizione = cDescrizione;
            }
        }

        // Risultato base che contiene un array di descrizione dei campi del log delle transazioni 
        public class ResultBaseCampiLogTransazioni : ResultBase
        {
            public clsResultBaseCampoLogTransazioni[] Campi = null;

            public ResultBaseCampiLogTransazioni()
            {
            }

            public ResultBaseCampiLogTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oListaCampi != null && oListaCampi.Count > 0)
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[oListaCampi.Count];
                    int Index = 0;
                    foreach (System.Collections.DictionaryEntry ItemCampoDict in oListaCampi)
                    {
                        Riepiloghi.clsRiepiloghi.clsCampoTransazione oCampo = (Riepiloghi.clsRiepiloghi.clsCampoTransazione)ItemCampoDict.Value;
                        this.Campi[Index] = new clsResultBaseCampoLogTransazioni(oCampo.Campo, oCampo.Tipo, oCampo.Inizio, oCampo.Dimensione, oCampo.DescCampo, oCampo.Descrizione);
                        Index += 1;
                    }
                }
                else
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[0];
                }
            }
        }

        // Risultato base che contiene un array di clsResulttransazioni (lista delle transazioni)
        public class ResultBaseGetTransazioni : ResultBase
        {
            public clsResultTransazione[] Transazioni = null;
            public System.Data.DataTable TableTransazioni = null;

            public ResultBaseGetTransazioni()
            {
                
            }

            public ResultBaseGetTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS, bool PreserveDataTable) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = (lStatusOK && oRS != null && !oRS.EOF);
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                if (oRS != null && !oRS.EOF)
                {
                    this.Transazioni = new clsResultTransazione[(int)oRS.RecordCount];
                    int Index = 0;
                    while (!oRS.EOF)
                    {
                        this.Transazioni[Index] = new clsResultTransazione(oRS);
                        Index += 1;
                        oRS.MoveNext();
                    }
                    if (PreserveDataTable)
                    {
                        this.TableTransazioni = (System.Data.DataTable)oRS;
                        this.TableTransazioni.TableName = "TRANSAZIONI";
                    }
                }
                else
                {
                    this.Transazioni = new clsResultTransazione[0];
                }

            }
        }

        // Risultato transazioni per periodo che contiene un array di clsResulttransazioni (lista delle transazioni)
        // ed il dettaglio della richiesta con data inizio, data fine e modalità storico
        public class ResultTransazionePeriodo : ResultBaseGetTransazioni
        {
            public DateTime DataEmiInizio;
            public DateTime DataEmiFine;
            public string ModalitaStorico = "";

            public ResultTransazionePeriodo()
            {
            }

            public ResultTransazionePeriodo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataEmiInizio, DateTime dDataEmiFine, string cModalitaStorico, IRecordSet oRS, bool PreserveDataTable) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, PreserveDataTable)
            {
                this.DataEmiInizio = dDataEmiInizio;
                this.DataEmiFine = dDataEmiFine;
                this.ModalitaStorico = cModalitaStorico;
            }
        }

        [WebMethod]
        public ResultBaseCampiLogTransazioni GetListaCampiLogTransazioni(string ServiceName, string User, string Password)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCampiLogTransazioni oResult = null;
            Exception oError = null;
            IConnection oConnection = null;
            System.Collections.SortedList oListaCampi = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    oListaCampi = Riepiloghi.clsRiepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError);
                    if (oListaCampi == null || oListaCampi.Count == 0)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi log transazioni", "Lista campi log transazioni non caricati.");
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Richiesta campi log transazioni", "Errore imprevisto durante la richiesta della lista dei campi log transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
            }
            else
            {
                oResult = new ResultBaseCampiLogTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            return oResult;
        }

        // Lista delle transazioni per periodo
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioni(string ServiceName, string User, string Password, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, null, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Lista delle transazioni per periodo + classe contenente i filtri
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniClassFilter(string ServiceName, string User, string Password, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, clsFiltroTransazioni Filtro, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        System.Collections.SortedList oFiltriTransazioni = null;
                        if (Filtro != null)
                        {
                            oFiltriTransazioni = new System.Collections.SortedList();
                            foreach (KeyValuePair<string, object> Item in Filtro.Filtri)
                            {
                                if (!oFiltriTransazioni.ContainsKey(Item.Key))
                                {
                                    oFiltriTransazioni.Add(Item.Key, Item.Value);
                                }
                            }
                        }
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, oFiltriTransazioni, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Lista delle transazioni per periodo + stringa contenente i filtri
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniStringFilter(string ServiceName, string User, string Password, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string FiltroString, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;

            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        System.Collections.SortedList oFiltriTransazioni = null;
                        clsFiltroTransazioni Filtro = new clsFiltroTransazioni(oConnection, FiltroString, out oError);
                        if (oError == null && Filtro != null)
                        {
                            oFiltriTransazioni = new System.Collections.SortedList();
                            foreach (KeyValuePair<string, object> Item in Filtro.Filtri)
                            {
                                if (!oFiltriTransazioni.ContainsKey(Item.Key))
                                {
                                    oFiltriTransazioni.Add(Item.Key, Item.Value);
                                }
                            }
                        }
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, oFiltriTransazioni, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Lista delle transazioni annullate per perido (annullanti ed annullati)
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniAnnullati(string ServiceName, string User, string Password, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        oRS = Riepiloghi.clsRiepiloghi.GetTransazioniAnnullati(dDataEmiInizio, dDataEmiFine, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni annullate", "Errore imprevisto durante lettura delle transazioni annullate.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Risultato transazione per carta e sigillo o progressivo che contiene un array di clsResulttransazioni (lista delle transazioni, una sola)
        // ed il dettaglio della richiesta con carta e sigillo o progressivo
        public class ResultTransazioneCartaSigilloProgressivo : ResultBaseGetTransazioni
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;

            public ResultTransazioneCartaSigilloProgressivo()
            {
            }

            public ResultTransazioneCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Carta, string Sigillo, Int64 Progressivo, IRecordSet oRS, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, PreserveDataTable)
            {
                this.Carta = Carta;
                this.Sigillo = Sigillo;
                this.Progressivo = Progressivo;
            }
        }

        // Ricerca transazione carta e progressivo
        [WebMethod]
        public ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_PROGRESSIVO(string ServiceName, string User, string Password, string Carta, Int64 Progressivo, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(Carta, Progressivo, oConnection, out oError);
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivo", "Errore imprevisto durante lettura delle transazioni per carta e progressivo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            
            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca transazione carta e sigillo
        [WebMethod]
        public ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_SIGILLO(string ServiceName, string User, string Password, string Carta, string Sigillo, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", User, Password, out oError))
                    {
                        oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_SIGILLO(Carta, Sigillo, oConnection, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e sigillo", "Errore imprevisto durante lettura delle transazioni per carta e sigillo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        #endregion

        #region "Ritorno delle tabelle di base"

        // Classe generica Dictionary per ritorno tabelle standard CODICE e DESCRIZIONE
        [XmlRoot("dictionary")]
        public class clsDizionarioTabella<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }
        }

        // Classe per risultato tabella standard
        public class ResultBaseTabella : ResultBase
        {
            public clsDizionarioTabella<string, string> Righe = new clsDizionarioTabella<string, string>();

            public ResultBaseTabella()
            {
            }

            public ResultBaseTabella(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        if (!Righe.ContainsKey(oRS.Fields("codice").Value.ToString()))
                        {
                            Righe.Add(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                        }
                        oRS.MoveNext();
                    }
                }
            }
        }

        // Funzione interna per leggere tabella con CODICE e DESCRIZIONE
        private ResultBaseTabella GetTabellaStandard(string ServiceName, string QueryTabella)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseTabella oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    oRS = (IRecordSet)oConnection.ExecuteQuery(QueryTabella);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseTabella(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseTabella(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;

        }
        
        // VR_TIPOEVENTO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoEvento(string ServiceName)
        {
            return this.GetTabellaStandard(ServiceName, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOEVENTO ORDER BY CODICE");
        }

        // VR_ORDINIDIPOSTO
        [WebMethod]
        public ResultBaseTabella GetTabellaOrdiniDiPosto(string ServiceName)
        {
            return this.GetTabellaStandard(ServiceName, "SELECT CODICE, DESCRIZIONE FROM VR_ORDINIDIPOSTO ORDER BY CODICE");
        }

        // VR_TIPOTITOLO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoTitolo(string ServiceName)
        {
            return this.GetTabellaStandard(ServiceName, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOTITOLO ORDER BY CODICE");
        }

        // VR_TIPOPROVENTO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoProvento(string ServiceName)
        {
            return this.GetTabellaStandard(ServiceName, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOPROVENTO ORDER BY CODICE");
        }

        // classe per VR_CRITERIO_OMAGGI_CONFIG
        public class ResultBaseCriteriOmaggioConfig : ResultBase
        {
            public clsDizionarioTabella<Int64, Riepiloghi.clsCriterioOmaggiConfig> Righe = new clsDizionarioTabella<long, Riepiloghi.clsCriterioOmaggiConfig>();

            public ResultBaseCriteriOmaggioConfig()
            {
            }

            public ResultBaseCriteriOmaggioConfig(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        Int64 nOrdine = 0;
                        Int64 nEnabled = 0;
                        if (!oRS.Fields("ordine").IsNull && 
                            Int64.TryParse(oRS.Fields("ordine").Value.ToString(), out nOrdine) && 
                            Int64.TryParse(oRS.Fields("enabled").Value.ToString(), out nEnabled))
                        {
                            Riepiloghi.clsCriterioOmaggiConfig oConfig = new Riepiloghi.clsCriterioOmaggiConfig(nOrdine, nEnabled, oRS.Fields("origine").Value.ToString(), oRS.Fields("ordine_di_posto").Value.ToString(), oRS.Fields("tipo_titolo").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                            if (!Righe.ContainsKey(oConfig.Ordine))
                            {
                                Righe.Add(oConfig.Ordine, oConfig);
                            }
                        }

                        oRS.MoveNext();
                    }
                }
            }
        }

        // VR_CRITERIO_OMAGGI_CONFIG
        [WebMethod]
        public ResultBaseCriteriOmaggioConfig GetTabellaCriteriOmaggiConfig(string ServiceName)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCriteriOmaggioConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT * FROM VR_CRITERIO_OMAGGI_CONFIG ORDER BY ORDINE");
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;
        }

        #endregion

        #endregion

        #region "Operazioni"

        #region "Annulli"

        // classe di Controllo per annullo transazione
        public class ResultBaseCheckAnnulloTransazione : ResultBase
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;

            public ResultBaseCheckAnnulloTransazione()
            {
            }

            public ResultBaseCheckAnnulloTransazione(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceCarta, string cSigillo, Int64 nProgressivo)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Carta = cCodiceCarta;
                this.Sigillo = cSigillo;
                this.Progressivo = nProgressivo;
            }
        }

        // classe per annullo transazione
        public class ResultBaseAnnulloTransazione : ResultBaseCheckAnnulloTransazione
        {
            public string CartaAnnullante = "";
            public string SigilloAnnullante = "";
            public Int64 ProgressivoAnnullante = 0;

            public ResultBaseAnnulloTransazione()
            {
            }

            public ResultBaseAnnulloTransazione(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceCarta, string cSigillo, Int64 nProgressivo, string cCodiceCartaAnnullante, string cSigilloAnnullante, Int64 nProgressivoAnnullante)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, cCodiceCarta, cSigillo, nProgressivo)
            {
                this.CartaAnnullante = cCodiceCartaAnnullante;
                this.SigilloAnnullante = cSigilloAnnullante;
                this.ProgressivoAnnullante = nProgressivoAnnullante;
            }
        }

        [WebMethod]
        public ResultBaseCheckAnnulloTransazione CheckAnnulloTransazione(string ServiceName, string User, string Password, string Carta, string Sigillo, Int64 Progressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cDescrizione = "";
            bool lWarning = false;
            Int64 nRet = 0;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_ANNULLI", User, Password, out oError))
                    {
                        nRet = Riepiloghi.clsRiepiloghi.CheckAnnulloTransazione(Carta, Sigillo, Progressivo, oConnection, out oError, out cDescrizione, out lWarning);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBaseCheckAnnulloTransazione(false, oError.Message, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo);
            }
            else
            {
                return new ResultBaseCheckAnnulloTransazione((nRet == 0 || lWarning), cDescrizione, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo);
            }
        }

        [WebMethod]
        public ResultBaseAnnulloTransazione AnnulloTransazione(string ServiceName, string User, string Password, string Carta, string Sigillo, Int64 Progressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cDescrizione = "";
            bool lWarning = false;
            Int64 nRet = 0;
            IConnection oConnection = null;
            string CartaAnnullante = "";
            string SigilloAnnullante = "";
            Int64 ProgressivoAnnullante = 0;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_ANNULLI", User, Password, out oError))
                    {
                        nRet = Riepiloghi.clsRiepiloghi.CheckAnnulloTransazione(Carta, Sigillo, Progressivo, oConnection, out oError, out cDescrizione, out lWarning);

                        if (nRet == 0 || lWarning)
                        {
                            if (Riepiloghi.clsRiepiloghi.AnnulloTransazione(Carta, Sigillo, Progressivo, oConnection, out oError, out CartaAnnullante, out SigilloAnnullante, out ProgressivoAnnullante))
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBaseAnnulloTransazione(false, oError.Message, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo, "", "", 0);
            }
            else
            {
                return new ResultBaseAnnulloTransazione((nRet == 0 || lWarning), cDescrizione, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo, CartaAnnullante, SigilloAnnullante, ProgressivoAnnullante);
            }
        }

        #endregion
        
        #region "Generazione Riepilogo"

        // Classe per singola riga del riepilogo da generare
        public class clsResultBaseRiepilogoDaGenerare
        {
            public string GiornalieroMensile = "";
            public DateTime GiornoMese = DateTime.MinValue;
            public DateTime UltimaEmissione = DateTime.MinValue;
            public DateTime DataOraGenerazione = DateTime.MinValue;
            public string DescGenerazione = "";

            public clsResultBaseRiepilogoDaGenerare()
            {
            }

            public clsResultBaseRiepilogoDaGenerare(string cGiornalieroMensile, DateTime dGiornoMese, DateTime dUltimaEmissione, DateTime dDataOraGenerazione, string cDescGenerazione)
            {
                this.GiornalieroMensile = cGiornalieroMensile;
                this.GiornoMese = dGiornoMese;
                this.UltimaEmissione = dUltimaEmissione;
                this.DataOraGenerazione = dDataOraGenerazione;
                this.DescGenerazione = cDescGenerazione;
            }
        }

        // Risposta del servizio
        public class ResultBaseRiepiloghiDaGenerare : ResultBase
        {
            public clsResultBaseRiepilogoDaGenerare[] RiepiloghiDaGenerare = null;
            public System.Data.DataTable TableRiepiloghiDaGenerare = null;
            public string GiornalieroMensile = "";

            public ResultBaseRiepiloghiDaGenerare()
            {
            }

            public ResultBaseRiepiloghiDaGenerare(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string GiornalieroMensile, IRecordSet oRS, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.GiornalieroMensile = GiornalieroMensile;
                if (oRS != null && !oRS.EOF)
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[(int)oRS.RecordCount];
                    int Index = 0;
                    while (!oRS.EOF)
                    {
                        this.RiepiloghiDaGenerare[Index] = new clsResultBaseRiepilogoDaGenerare(oRS.Fields("giornaliero_mensile").Value.ToString(),
                                                                                                (oRS.Fields("giorno_mese").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("giorno_mese").Value),
                                                                                                (oRS.Fields("ultima_emissione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("ultima_emissione").Value),
                                                                                                (oRS.Fields("dataora_generazione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("dataora_generazione").Value),
                                                                                                oRS.Fields("desc_generazione").Value.ToString());
                        oRS.MoveNext();
                    }
                    if (PreserveDataTable)
                    {
                        this.TableRiepiloghiDaGenerare = (System.Data.DataTable)oRS;
                        this.TableRiepiloghiDaGenerare.TableName = "RIEPILOGHI_DA_GENERARE";
                    }
                }
                else
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[0];
                }
            }
        }

        [WebMethod]
        public ResultBaseRiepiloghiDaGenerare GetRiepiloghiDaGenerare(string ServiceName, string User, string Password, string GiornalieroMensile, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseRiepiloghiDaGenerare oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_CREAZIONE", User, Password, out oError))
                    {
                        oRS = Riepiloghi.clsRiepiloghi.GetRiepiloghiDaGenerare(oConnection, GiornalieroMensile, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura riepiloghi da generare", "Errore imprevisto durante la lettura dei riepiloghi da generare.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        public class ResultBaseGenerazioneRiepilogo : ResultBase
        {
            public string FileLogFirmato = "";
            public string FileRiepilogo = "";
            public string FileEmail = "";

            public ResultBaseGenerazioneRiepilogo()
            {
            }

            public ResultBaseGenerazioneRiepilogo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cFileLogFirmato, string cFileRiepilogo, string cFileEmail) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.FileLogFirmato = cFileLogFirmato;
                this.FileRiepilogo = cFileRiepilogo;
                this.FileEmail = cFileEmail;
            }
        }

        // Generazione del riepilogo
        [WebMethod]
        public ResultBaseGenerazioneRiepilogo CreaRiepilogo(string ServiceName, string User, string Password, DateTime Giorno, string GiornalieroMensile)
        {
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            string cFileLogFirmato = "";
            string cFileRiepilogoFirmato = "";
            string cFileEmailFirmato = "";
            ResultBaseGenerazioneRiepilogo oResult = null;
            IConnection oConnection = null;
            try
            {
                oConnection = ServiceRiepiloghi.GetConnection(ServiceName, out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_CREAZIONE", User, Password, out oError))
                    {
                        lRet = Riepiloghi.clsRiepiloghi.CreaRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError, out cFileLogFirmato, out cFileRiepilogoFirmato, out cFileEmailFirmato);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
            }
            if (oError == null)
            {
                string cDesc = "Riepilogo " + (GiornalieroMensile == "G" ? "Giornaliero" : "Mensile") + " generato correttamente:" +
                               (cFileLogFirmato.Trim() == "" ? "" : "\r\n" + "File di log " + cFileLogFirmato) +
                               (cFileRiepilogoFirmato.Trim() == "" ? "" : "\r\n" + "Riepilogo " + cFileRiepilogoFirmato) +
                               (cFileEmailFirmato.Trim() == "" ? "" : "\r\n" + "Mail " + cFileEmailFirmato);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, cDesc, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato);
            }
            else
            {
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato);
            }
            return oResult;
        }

        #endregion

        #endregion

        //Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo
        //Riepiloghi.clsRiepiloghi.GetNomeEmailGiorno
        //Riepiloghi.clsRiepiloghi.GetNomeEmailGiornoBase
        //Riepiloghi.clsRiepiloghi.GetNomeLogGiorno
        //Riepiloghi.clsRiepiloghi.GetNomeLogGiornoBase
        //Riepiloghi.clsRiepiloghi.GetNomeLogSearchIncrementale
        //Riepiloghi.clsRiepiloghi.GetNomeRiepilogo
        //Riepiloghi.clsRiepiloghi.GetNomeRiepilogoBase
        //Riepiloghi.clsRiepiloghi.CreaRiepilogo
        //Riepiloghi.clsRiepiloghi.Master_EseguiMasterizzazione
        //Riepiloghi.clsRiepiloghi.SpedizioneMail

        //Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi

        //Riepiloghi.clsRiepiloghi.PrintRiepilogoToPDF
        //Riepiloghi.clsRiepiloghi.PrintToPDF_SimulazioneMensile
        //Riepiloghi.clsRiepiloghi.PrintTransazioneCARTA_SIGILLO_PROGRESSIVO
        //Riepiloghi.clsRiepiloghi.PrintTransazioni
    }
}