﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Wintic.Data;

namespace WebServiceRiepiloghi
{
    /// <summary>
    /// Summary description for RiepiloghiPdfHandler
    /// </summary>
    public class RiepiloghiPdfHandler : IHttpHandler
    {
        private string ServiceName = "serverx";
        private string User = "";
        private string Password = "";

        private bool lRiepiloghi = false;
        private string GiornalieroMensile = "";
        private DateTime Giorno = DateTime.MinValue;
        private Int64 ProgRiepilogo = 0;
        private bool lDettaglioOmaggiEccedenti = false;

        private bool lTransazioni = false;
        private bool lPeriodoValido = false;
        private bool lAnnulli = false;
        private bool lUnaTransazionePerPagina = false;
        private DateTime DataEmiInizio = DateTime.MinValue;
        private DateTime DataEmiFine = DateTime.MinValue;

        private bool lRicercaTransazione = false;
        private string Carta = "";
        private string Sigillo = "";
        private Int64 Progressivo = 0;

        public void ProcessRequest(HttpContext context)
        {
            Exception oError = null;
            string cFilePdf = "";
            bool lRet = false;
            IConnection oConnection = null;
            Riepiloghi.clsRiepiloghi.PathRisorse = HttpContext.Current.Server.MapPath("//RisorseWeb");
            if (this.GetParameters(context, out oError))
            {
                oConnection = ServiceRiepiloghi.GetConnection(this.ServiceName, out oError);

                if (oError == null)
                {
                    try
                    {
                        // Richietsa stampa riepiloghi o similazione
                        if (lRiepiloghi)
                        {
                            if (GiornalieroMensile == "D")
                            {
                                String[] aCodiciLocali = null;
                                lRet = ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_STAMPA_SIMULAZIONE_SALA", this.User, this.Password, out oError);
                                if (lRet)
                                {
                                    try
                                    {
                                        StringBuilder oSB = new StringBuilder();
                                        clsParameters oPars = new clsParameters();

                                        if (ServiceRiepiloghi.CheckUserPolicyEnabled(oConnection))
                                        {
                                            oSB.Append("SELECT CODICE_LOCALE FROM VR_OPERATORI_SALE_PWD WHERE LOGIN = :pLOGIN AND PASSWORD = MD5_DIGEST(:pPASSWORD) GROUP BY CODICE_LOCALE ORDER BY CODICE_LOCALE");
                                        }
                                        else
                                        {
                                            oSB.Append("SELECT CODICE_LOCALE FROM VR_OPERATORI_SALE WHERE LOGIN = :pLOGIN AND PASSWORD = :pPASSWORD GROUP BY CODICE_LOCALE ORDER BY CODICE_LOCALE");
                                        }

                                        oPars.Add(":pLOGIN", User);
                                        oPars.Add(":pPASSWORD", Password);
                                        IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), oPars);
                                        lRet = !oRS.EOF;
                                        if (!lRet)
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Simulazione per data evento", "Nessuna sala abilitata per questo operatore.");
                                        }
                                        else
                                        {
                                            // Operatore abilitato a tutte le sale
                                            if (oRS.Fields("codice_locale").Value.ToString() == "ALL")
                                            {
                                                lRet = true;
                                                aCodiciLocali = null;
                                            }
                                            else
                                            {
                                                aCodiciLocali = new string[(int)oRS.RecordCount];
                                                int Index = 0;
                                                while (!oRS.EOF)
                                                {
                                                    aCodiciLocali[Index] = oRS.Fields("codice_locale").Value.ToString();
                                                    Index += 1;
                                                    oRS.MoveNext();
                                                }
                                            }
                                        }
                                        oRS.Close();
                                    }
                                    catch (Exception exGetSaleSimulazione)
                                    {
                                        lRet = false;
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Simulazione per data evento", "Errore durante l'autenticazione per lista sale abilitate.", exGetSaleSimulazione);
                                    }
                                }
                                if (lRet)
                                {
                                    lRet = Riepiloghi.clsRiepiloghi.PrintToPDF_SimulazioneMensile(Giorno, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, this.lDettaglioOmaggiEccedenti, out oError, "", out cFilePdf, aCodiciLocali);
                                }
                            }
                            else
                            {
                                lRet = ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_STAMPA", this.User, this.Password, out oError);

                                if (lRet)
                                {
                                    // se non viene passato il progressivo del riepilog prendo l'ultimo generato
                                    if (this.ProgRiepilogo == 0)
                                    {
                                        this.ProgRiepilogo = Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError);
                                        lRet = (this.ProgRiepilogo > 0);
                                    }
                                    else
                                    {
                                        if (this.ProgRiepilogo <= Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError))
                                        {
                                            lRet = (oError == null);
                                        }
                                    }

                                    if (this.ProgRiepilogo > 0)
                                    {
                                        lRet = Riepiloghi.clsRiepiloghi.PrintRiepilogoToPDF(Giorno, GiornalieroMensile, ProgRiepilogo, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, lDettaglioOmaggiEccedenti, out oError, "", out cFilePdf, null);
                                    }
                                    else
                                    {
                                        lRet = false;
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Riepilogo", "Riepilogo non generato.");
                                    }
                                }
                            }
                        }

                        // Log delle transazioni
                        if (lTransazioni || lAnnulli)
                        {
                            lRet = ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", this.User, this.Password, out oError);
                            if (lRet)
                            {
                                lRet = Riepiloghi.clsRiepiloghi.PrintTransazioni(DataEmiInizio, DataEmiFine, lAnnulli, lUnaTransazionePerPagina, null, null, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, out oError, "", out cFilePdf);
                            }
                        }

                        // ricerca transazione
                        if (lRicercaTransazione)
                        {
                            lRet = ServiceRiepiloghi.CheckUserOperazione(oConnection, "RIEPILOGHI_TRANSAZIONI", this.User, this.Password, out oError);
                            if (lRet)
                            {
                                lRet = Riepiloghi.clsRiepiloghi.PrintTransazioneCARTA_SIGILLO_PROGRESSIVO(Carta, Sigillo, Progressivo, oConnection, out oError, "", out cFilePdf);
                            }
                        }

                        if (lRet)
                        {
                            lRet = (cFilePdf.Trim() != "" && System.IO.File.Exists(cFilePdf));
                        }

                        if (lRet)
                        {
                            if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                            context.Response.ContentType = "application/pdf";
                            context.Response.WriteFile(cFilePdf);
                            context.Response.Flush();
                            try
                            {
                                System.IO.File.Delete(cFilePdf);
                            }
                            catch
                            {
                            }
                            context.Response.End();
                        }
                        else
                        {
                            if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                            context.Response.ContentType = "text/plain";
                            if (oError != null)
                            {
                                context.Response.Write("Errore: " + oError.Message);
                            }
                            context.Response.Flush();
                            context.Response.End();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                    }
                }
                else
                {
                    if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                    context.Response.ContentType = "text/plain";
                    if (oError != null)
                    {
                        context.Response.Write("parametri di connessione non validi: " + oError.Message);
                    }
                    else
                    {
                        context.Response.Write("parametri connessione non validi.");
                    }
                }
            }
            else
            {
                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                context.Response.ContentType = "text/plain";
                if (oError != null)
                {
                    context.Response.Write("parametri non validi: " + oError.Message);
                }
                else
                {
                    context.Response.Write("parametri non validi.");
                }
                context.Response.Flush();
                context.Response.End();
            }
        }

        private object GetSingleParameter(HttpContext context, string paramName)
        {
            object oValue = null;
            try
            {
                oValue = context.Request[paramName].ToString();
            }
            catch
            {
                oValue = "";
            }
            return oValue;
        }
        
        // Lettura parametri
        private bool GetParameters(HttpContext context, out Exception oError)
        {
            bool lRet = false;
            oError = null;

            if (GetSingleParameter(context, "login").ToString().Trim() != "")
            {
                this.User = GetSingleParameter(context, "login").ToString().Trim();
            }
            if (GetSingleParameter(context, "password").ToString().Trim() != "")
            {
                this.Password = GetSingleParameter(context, "password").ToString().Trim();
            }
            if (GetSingleParameter(context, "servicename").ToString().Trim() != "")
            {
                this.ServiceName = GetSingleParameter(context, "servicename").ToString().Trim();
            }

            if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPG") // Stampa Riepilogo giornaliero
            {
                GiornalieroMensile = "G";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPM") // Stampa Riepilogo mensile
            {
                GiornalieroMensile = "M";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPD")// Stampa Simulazione mensile per data evento
            {
                GiornalieroMensile = "D";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "LOG") // Stampa log delle transazioni
            {
                lTransazioni = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "ANN") // Stampa log transazioni annullate
            {
                lAnnulli = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "SRC") // Stampa Ricerca transazione
            {
                lRicercaTransazione = true;
            }

            if (lRiepiloghi)
            {
                if (GetSingleParameter(context, "giorno").ToString().Trim() != "" && DateTime.TryParse(GetSingleParameter(context, "giorno").ToString(), out Giorno))
                {
                    lRet = true;
                }
                else
                {
                    lRet = false;
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Riepilogo", "Giorno non valido.");
                }
            }

            if (lTransazioni || lAnnulli)
            {
                lPeriodoValido = DateTime.TryParse(GetSingleParameter(context, "dal").ToString(), out DataEmiInizio) &&
                                 DateTime.TryParse(GetSingleParameter(context, "al").ToString(), out DataEmiFine);

                if (lPeriodoValido)
                {
                    lPeriodoValido = DataEmiInizio <= DataEmiFine;
                }

                if (!lPeriodoValido)
                {
                    lRet = false;
                    if (lTransazioni)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Transazioni", "Periodo non valido.");
                    }
                    else
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Transazioni Annullate", "Periodo non valido.");
                    }
                }
                else
                {
                    lRet = true;
                }

                lUnaTransazionePerPagina = false;
                if (GetSingleParameter(context, "style").ToString().Trim().ToUpper() == "TAB")
                {
                    lUnaTransazionePerPagina = false;
                }
                else
                {
                    lUnaTransazionePerPagina = true;
                }
            }

            if (lRicercaTransazione)
            {

                if (GetSingleParameter(context, "carta").ToString().Trim().Length == 8)
                {
                    Carta = GetSingleParameter(context, "carta").ToString().Trim().ToUpper();
                    if (GetSingleParameter(context, "sigillo").ToString().Trim().Length > 0)
                    {
                        Sigillo = GetSingleParameter(context, "sigillo").ToString().Trim().ToUpper();
                        lRet = (Sigillo.Length == 16);
                        if (!lRet)
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Sigillo", "Sigillo non valido: [" + GetSingleParameter(context, "sigillo") + "]");
                        }
                    }
                    else
                    {
                        if (Int64.TryParse(GetSingleParameter(context, "progressivo").ToString().Trim(), out Progressivo))
                        {
                            lRet = true;
                        }
                        else
                        {
                            lRet = false;
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Progressivo", "Progressivo non valido: [" + GetSingleParameter(context, "progressivo") + "]");
                        }
                    }
                }
                else
                {
                    lRet = false;
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Sigillo/Progressivo", "Carta non valida: [" + GetSingleParameter(context, "carta") + "]");
                }

            }

            return lRet;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}