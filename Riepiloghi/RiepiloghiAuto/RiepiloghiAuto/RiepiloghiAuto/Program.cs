﻿//using ApiRiepiloghi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
//using Wintic.Data;
//using static ApiRiepiloghi.ServiceRiepiloghi;

namespace RiepiloghiAuto
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        private static string UrlBase = @"https://riepiloghi.webtic.it/api/"; // @"http://localhost:51816/api/";
        private static clsSequenzaOperazioni sequenzaOperazioni = new clsSequenzaOperazioni();
        private static clsLocaleApplicationDatabase ApplicationDb = null;
        private static bool manualLogin = false;
        private static string manualUsr = "";
        private static string manualPsw = "";
        private static bool lAutoMode = true;
        private static bool lVerbose = false;

        static void Main(string[] args)
        {
            bool lExcuteProcedure = true;

            #region Analisi parametri

            if (args != null && args.Length > 0)
            {
                foreach (string itemArg in args)
                {
                    string argName = ((itemArg.Contains("=")) ? itemArg.Split('=')[0] : itemArg).Trim().ToUpper();
                    string argValue = ((itemArg.Contains("=")) ? itemArg.Split('=')[1] : "");
                    switch (argName)
                    {
                        case "HELP":
                            {
                                lExcuteProcedure = false;
                                lAutoMode = false;
                                foreach (string line in GetHelp())
                                {
                                    Console.WriteLine(line);
                                }
                                break;
                            }
                        case "MANUAL":
                            {
                                lAutoMode = false;
                                break;
                            }
                        case "VERBOSE":
                            {
                                lVerbose = true;
                                break;
                            }
                    }
                    if (itemArg.Contains("="))
                    {

                    }
                }
            }

            #endregion

            if (lExcuteProcedure)
            {

                #region Avvio

                if (lAutoMode)
                {
                    if (!lVerbose)
                    {
                        try
                        {
                            var handle = GetConsoleWindow();
                            ShowWindow(handle, SW_HIDE);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                else
                {
                    manualLogin = true;
                    Console.Write("Inserire l'utente e premere Invio: ");
                    manualUsr = Console.ReadLine();
                    Console.Write("Inserire la Password e premere Invio: ");
                    ConsoleKeyInfo key;
                    manualPsw = "";
                    while (true)
                    {
                        key = Console.ReadKey(true);
                        if (key.Key != ConsoleKey.Backspace)
                        {
                            if (key.Key != ConsoleKey.Enter)
                            {
                                manualPsw += key.KeyChar;
                                Console.Write("*");
                            }
                            else
                            {
                                string f = "";
                            }
                        }
                        else
                        {
                            Console.Write("\b");
                        }
                        if (key.Key == ConsoleKey.Enter)
                            break;
                    }
                    //manualPsw = Console.ReadLine();
                }

                #endregion

                #region Operazioni preliminari

                if (!lAutoMode || lVerbose)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Avvio...");
                }

                System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
                DateTime startApplication = DateTime.Now;
                watch.Start();
                Exception error = null;
                string token = "";
                string codiceSistema = "";
                string tokenCodiceSistema = "";
                bool lRichiestaRiepiloghiDaGenerare = false;

                sequenzaOperazioni = new clsSequenzaOperazioni();
                sequenzaOperazioni.EventAdd += SequenzaOperazioni_EventAdd;
                sequenzaOperazioni.EventStop += SequenzaOperazioni_EventStop;
                sequenzaOperazioni.EventAddMessage += SequenzaOperazioni_EventAddMessage;

                sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Inizializzazione applicazione", false));

                FileInfo applicationFileInfo = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                FileInfo fileLocalApplicationDatabase = new FileInfo(applicationFileInfo.Directory.FullName + @"\RiepiloghiAuto.db.json");
                ApplicationDb = new clsLocaleApplicationDatabase();

                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" Percorso: {0}", fileLocalApplicationDatabase.Directory.FullName));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" File: {0}", fileLocalApplicationDatabase.Name));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" Esistenza: {0}", fileLocalApplicationDatabase.Exists.ToString()));

                if (fileLocalApplicationDatabase.Exists)
                {
                    try
                    {
                        ApplicationDb = Newtonsoft.Json.JsonConvert.DeserializeObject<clsLocaleApplicationDatabase>(System.IO.File.ReadAllText(fileLocalApplicationDatabase.FullName));
                    }
                    catch (Exception)
                    {
                        ApplicationDb = new clsLocaleApplicationDatabase();
                    }
                }

                ApplicationDb.watch = watch;

                Dictionary<string, DateTime> fasciaOrariaGenerazioneRiepiloghi = new Dictionary<string, DateTime>();
                Dictionary<string, DateTime> fasciaOrariaVerificaEmail = new Dictionary<string, DateTime>();

                UrlBase = ConfigurationManager.AppSettings["usrWebServicesRiepiloghi"];
                //UrlBase = @"http://localhost:51816/api/";

                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" Url: {0}", UrlBase));

                string sendTo = ConfigurationManager.AppSettings["sendEmailReportTO"];
                string sendCC = ConfigurationManager.AppSettings["sendEmailReportCC"];
                string sendCCN = ConfigurationManager.AppSettings["sendEmailReportCCN"];
                bool sendPDF_RPG = (ConfigurationManager.AppSettings["sendPDF_RPG"] != null && ConfigurationManager.AppSettings["sendPDF_RPG"].Trim().ToLower() == "true");
                bool sendPDF_RPM = (ConfigurationManager.AppSettings["sendPDF_RPM"] != null && ConfigurationManager.AppSettings["sendPDF_RPM"].Trim().ToLower() == "true");
                bool sendPDF_RCA = (ConfigurationManager.AppSettings["sendPDF_RCA"] != null && ConfigurationManager.AppSettings["sendPDF_RCA"].Trim().ToLower() == "true");
                //bool createDIRECT = (ConfigurationManager.AppSettings["createDIRECT"] != null && ConfigurationManager.AppSettings["createDIRECT"].Trim().ToLower() == "true");

                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendEmailReportTO: {0}", sendTo));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendEmailReportCC: {0}", sendCC));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendEmailReportCCN: {0}", sendCCN));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendPDF_RPG: {0}", sendPDF_RPG.ToString()));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendPDF_RPM: {0}", sendPDF_RPM.ToString()));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" sendPDF_RCA: {0}", sendPDF_RCA.ToString()));
                //sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" createDIRECT: {0}", createDIRECT.ToString()));
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();

                List<clsRiepilogoGeneratoWebApi> listaRiepiloghiDaGenerare = null;
                clsItemSequenzaAutoOperazioni oOperazioniAutomatiche = new clsItemSequenzaAutoOperazioni("Riepiloghi Automatici", false);

                DateTime dSysdateCodiceSistema = DateTime.MinValue;
                DateTime dSysdateGenerazione = DateTime.MinValue;
                int countRiepiloghiGenerati = 0;

                #endregion

                #region Elaborazione

                try
                {
                    if (Auth(out token, out codiceSistema, out error))
                    {
                        if (GetTokenCodiceSistema(token, codiceSistema, out tokenCodiceSistema, out dSysdateCodiceSistema, out error))
                        {
                            sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format("Codice Sistema: {0}", codiceSistema));
                            sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format("SYSDATE Codice Sistema: {0} {1}", dSysdateCodiceSistema.ToShortDateString(), dSysdateCodiceSistema.ToLongTimeString()));

                            GetFasceOrarieGenerazioneInvioRicezione(tokenCodiceSistema, dSysdateCodiceSistema, out fasciaOrariaGenerazioneRiepiloghi, out fasciaOrariaVerificaEmail, out error);

                            #region Generazione Riepilogi

                            lRichiestaRiepiloghiDaGenerare = (error == null) &&
                                                             (dSysdateCodiceSistema >= fasciaOrariaGenerazioneRiepiloghi["start"] &&
                                                              dSysdateCodiceSistema <= fasciaOrariaGenerazioneRiepiloghi["stop"]);

                            if (error == null && lRichiestaRiepiloghiDaGenerare)
                                lRichiestaRiepiloghiDaGenerare = GetListaRiepiloghiDaGenerare(tokenCodiceSistema, out listaRiepiloghiDaGenerare, out dSysdateGenerazione, out error);

                            if (error == null && lRichiestaRiepiloghiDaGenerare)
                            {
                                clsItemSequenzaAutoOperazioni operazioneListaCompletaDaGenerare = new clsItemSequenzaAutoOperazioni("Riepiloghi da generare", false);
                                List<clsRiepilogoGeneratoWebApi> listaCompletaDaGenerare = new List<clsRiepilogoGeneratoWebApi>();
                                bool lFirstTime = true;

                                if (listaRiepiloghiDaGenerare.Count > 0)
                                {
                                    bool lContinuaGenerazione = true;


                                    while (lContinuaGenerazione)
                                    {
                                        lContinuaGenerazione = false;
                                        clsRiepilogoGeneratoWebApi riepilogoDaGenerare = null;

                                        foreach (clsRiepilogoGeneratoWebApi itemRiepilogo in listaRiepiloghiDaGenerare)
                                        {
                                            bool lDaGenerare = false;
                                            //bool lMensileMeseInCorso = false;

                                            switch (itemRiepilogo.Tipo)
                                            {
                                                case "G":
                                                    {
                                                        lDaGenerare = (itemRiepilogo.Giorno.Date < dSysdateGenerazione.Date);
                                                        break;
                                                    }
                                                case "M":
                                                    {
                                                        lDaGenerare = true;
                                                        break;
                                                    }
                                                case "R":
                                                    {
                                                        lDaGenerare = (itemRiepilogo.Giorno.Date < dSysdateGenerazione.Date);
                                                        break;
                                                    }
                                            }

                                            if (lDaGenerare && lFirstTime)
                                                listaCompletaDaGenerare.Add(itemRiepilogo);

                                            if (error == null && lDaGenerare && riepilogoDaGenerare == null)
                                            {
                                                riepilogoDaGenerare = itemRiepilogo;
                                            }
                                        }
                                        lFirstTime = false;

                                        if (riepilogoDaGenerare != null)
                                        {
                                            //bool lEsitoGenerazione = GeneraRiepilogo(tokenCodiceSistema, riepilogoDaGenerare, createDIRECT, out error);
                                            bool lEsitoGenerazione = GeneraRiepilogo(tokenCodiceSistema, riepilogoDaGenerare, out error);

                                            clsItemSequenzaAutoOperazioni itemDiGenerazione = sequenzaOperazioni[sequenzaOperazioni.Count - 1];

                                            if (riepilogoDaGenerare.Tipo == "M")
                                            {
                                                DateTime dInizioMeseAttuale = new DateTime(dSysdateGenerazione.Year, dSysdateGenerazione.Month, 1);
                                                DateTime dInizioMeseDaGenerare = new DateTime(riepilogoDaGenerare.Giorno.Year, riepilogoDaGenerare.Giorno.Month, 1);
                                                if (dInizioMeseDaGenerare == dInizioMeseAttuale)
                                                {
                                                    if (lEsitoGenerazione)
                                                        sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Il riepilogo mensile del mese in corso e' stato generato per l'obbligo di inviarlo quotidianamente.");
                                                    else
                                                        sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Il riepilogo mensile del mese in corso deve essere generato per l'obbligo di inviarlo quotidianamente.");
                                                }
                                            }

                                            if (lEsitoGenerazione)
                                            {
                                                // tolgo dalla lista il riepilogo generato
                                                countRiepiloghiGenerati += 1;
                                                listaCompletaDaGenerare.Remove(riepilogoDaGenerare);

                                                Exception errorGetPdf = null;

                                                if ((sendPDF_RPG && riepilogoDaGenerare.Tipo == "G") ||
                                                    (sendPDF_RPM && riepilogoDaGenerare.Tipo == "M") ||
                                                    (sendPDF_RCA && riepilogoDaGenerare.Tipo == "R"))
                                                {
                                                    ResultContentFile contentFile = StampaRiepilogo(tokenCodiceSistema, riepilogoDaGenerare, out errorGetPdf);

                                                    if (contentFile == null || contentFile.Buffer == null || contentFile.Buffer.Length == 0 || errorGetPdf != null)
                                                    {
                                                        sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Impossibile recuperare il PDF del riepilogo" + (errorGetPdf == null ? "" : " Errore:" + errorGetPdf.Message));
                                                        sequenzaOperazioni[sequenzaOperazioni.Count - 1].Important = true;
                                                    }
                                                    else
                                                    {
                                                        if (itemDiGenerazione != null)
                                                        {
                                                            itemDiGenerazione.Messages.Add("Segue email con allegato riepilogo in formato PDF.");
                                                            itemDiGenerazione.contentFile = contentFile;
                                                        }

                                                    }
                                                }

                                                listaRiepiloghiDaGenerare = null;
                                                dSysdateGenerazione = DateTime.MinValue;
                                                lRichiestaRiepiloghiDaGenerare = GetListaRiepiloghiDaGenerare(tokenCodiceSistema, out listaRiepiloghiDaGenerare, out dSysdateGenerazione, out error);

                                                if (error == null && lRichiestaRiepiloghiDaGenerare && listaRiepiloghiDaGenerare != null && listaRiepiloghiDaGenerare.Count > 0)
                                                {
                                                    lContinuaGenerazione = (dSysdateGenerazione >= fasciaOrariaGenerazioneRiepiloghi["start"] &&
                                                                            dSysdateGenerazione <= fasciaOrariaGenerazioneRiepiloghi["stop"]);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (error == null)
                                    operazioneListaCompletaDaGenerare.Stop();
                                else
                                {
                                    operazioneListaCompletaDaGenerare.Stop(error);
                                    if (listaCompletaDaGenerare.Count > 0)
                                    {
                                        sequenzaOperazioni.Add(operazioneListaCompletaDaGenerare);
                                        foreach (clsRiepilogoGeneratoWebApi itemRiepilogo in listaCompletaDaGenerare)
                                        {
                                            operazioneListaCompletaDaGenerare.Messages.Add(GetDescrizioneRiepilogo(itemRiepilogo));
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region Invio e ricezione email
                            //se ho generato almeno un riepilogo o sono nella fascia di invio e ricezione
                            if (countRiepiloghiGenerati > 0 || 
                                ((fasciaOrariaVerificaEmail != null) &&
                                (dSysdateCodiceSistema >= fasciaOrariaVerificaEmail["start"] &&
                                 dSysdateCodiceSistema <= fasciaOrariaVerificaEmail["stop"])))
                            {
                                List<clsRiepilogoGeneratoWebApi> listaRiepilogiInvioRicezione = new List<clsRiepilogoGeneratoWebApi>();
                                Exception errorSendReceive = null;
                                SendReceiveRiepiloghi(tokenCodiceSistema, out listaRiepilogiInvioRicezione, out dSysdateCodiceSistema, countRiepiloghiGenerati, out errorSendReceive);
                                if (errorSendReceive != null)
                                {
                                    if (error == null) error = errorSendReceive;
                                    else error = new Exception(error.Message + "r\n" + "Invio/Ricezione: " + errorSendReceive.Message);
                                }
                            }

                            #endregion
                        }
                    }

                }
                catch (Exception ex)
                {
                    error = new Exception("Operazioni Automatiche Errore generico " + ex.Message);
                }

                if (error != null) oOperazioniAutomatiche.Stop(error);
                else oOperazioniAutomatiche.Stop();

                sequenzaOperazioni.Add(oOperazioniAutomatiche);

                #endregion

                #region report

                clsSequenzaOperazioni operazioniReport = new clsSequenzaOperazioni();

                operazioniReport.EventAdd += SequenzaOperazioni_EventAdd;
                operazioniReport.EventStop += SequenzaOperazioni_EventStop;
                operazioniReport.EventAddMessage += SequenzaOperazioni_EventAddMessage;

                if (tokenCodiceSistema.Trim() != "")
                {
                    SendEmailServizio(tokenCodiceSistema, sendTo, sendCC, sendCCN, error, sequenzaOperazioni, sendPDF_RPG, sendPDF_RPM, sendPDF_RCA, ref operazioniReport);
                }

                #endregion

                #region Salvataggio database locale

                try
                {
                    ApplicationDb.watch.Stop();
                    ApplicationDb.startExecutionUTC = startApplication;
                    ApplicationDb.endExecutionUTC = DateTime.Now;
                    ApplicationDb.operazioni = sequenzaOperazioni;
                    ApplicationDb.operazioniReport = operazioniReport;
                    JsonSerializerSettings settings = new JsonSerializerSettings();
                    settings.NullValueHandling = NullValueHandling.Ignore;
                    settings.Formatting = Formatting.Indented;
                    settings.StringEscapeHandling = StringEscapeHandling.Default;
                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(ApplicationDb, settings);
                    byte[] jsonBuffer = System.Text.Encoding.Default.GetBytes(jsonString);
                    System.IO.File.WriteAllBytes(fileLocalApplicationDatabase.FullName, jsonBuffer);
                }
                catch (Exception)
                {
                }

                #endregion

                if (ApplicationDb != null && ApplicationDb.watch != null)
                {
                    ApplicationDb.watch.Stop();
                }

                
            }

            if (!lAutoMode || lVerbose)
            {
                Console.WriteLine("");
                if (ApplicationDb != null && ApplicationDb.watch != null)
                {
                    Console.WriteLine("FINE OPERAZIONI.");
                    Console.WriteLine(" tempo di esecuzione " + ApplicationDb.TotalExecutionTime);
                }
                Console.Write("Premere qualsiasi tasto.");
                Console.ReadKey(true);
            }
        }


        #region Eventi console application

        private static void SequenzaOperazioni_EventAdd(clsItemSequenzaAutoOperazioni item)
        {
            if (!lAutoMode || lVerbose)
                Console.WriteLine(item.Operation + " " + item.StartDateDescription);
        }

        private static void SequenzaOperazioni_EventAddMessage(string message)
        {
            if (!lAutoMode || lVerbose)
                Console.WriteLine(message);
        }

        private static void SequenzaOperazioni_EventStop(clsItemSequenzaAutoOperazioni item)
        {
            if (!lAutoMode || lVerbose)
            {
                Console.WriteLine(" termine operazione " + item.EndDateDescription + " " + item.ExecutionTime);
                string descr = item.Description(false);
                if (!string.IsNullOrEmpty(descr) && !string.IsNullOrWhiteSpace(descr))
                {
                    if (descr.Split('\r').Length > 0)
                    {
                        foreach (string line in descr.Split('\r'))
                        {
                            Console.WriteLine(" " + line);
                        }
                    }
                    else Console.WriteLine(descr);
                }
                if (item.error != null)
                {
                    ConsoleColor foreColorRegular = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" " + item.error.Message);
                    Console.ForegroundColor = foreColorRegular;
                }
                Console.WriteLine("-".PadRight(80, '-'));
            }
        }

        #endregion

        #region Autenticazione

        private static bool Auth(out string token, out string codiceSistema, out Exception error)
        {
            error = null;
            codiceSistema = "";
            bool result = false;

            if (Login(out token, out error))
            {
                result = GetAccount(token, out codiceSistema, out error) && (error == null);
            }
            return result;
        }

        private static bool GetAutoAuth(out string user, out string password, out Exception error, out string WebServiceExecutionTime)
        {
            WebServiceExecutionTime = "";
            user = "";
            password = "";
            error = null;
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "GetAutoAuth";
            
            string UrlAuthLogin = UrlBase + @"Auth/RiepiloghiAutoAuth";
            //UrlAuthLogin = UrlBase + @"RiepiloghiAutoAuth";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = Encrypt("Ch3T31oD1c0AF@re");
            response = GetResponse(UrlAuthLogin, payload, caller, out error);

            if (error == null)
            {
                if (response.Data.Token == null || string.IsNullOrEmpty(response.Data.Token) || string.IsNullOrWhiteSpace(response.Data.Token))
                {
                    error = new Exception(string.Format(caller + ": {0}", "Token non valido."));
                }
                else
                {
                    string value = Decrypt(response.Data.Token);
                    if (value.Contains("/"))
                    {
                        user = value.Split('/')[0];
                        password = value.Split('/')[1];
                        result = true;
                    }
                    else error = new Exception(string.Format(caller + ": {0}", "Token non valido."));
                }
            }

            if (!result && error == null)
                error = new Exception(string.Format(caller + ": {0}", "Errore."));

            WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        private static bool Login(out string token, out Exception error)
        {
            token = "";
            error = null;
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "Login";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni(caller, false));
            string user = "";
            string password = "";
            bool lGetAutoAuit = false;
            string WebServiceExecutionTimeGetAutoAuth = "";
            
            if (manualLogin)
            {
                lGetAutoAuit = true;
                user = manualUsr;
                password = manualPsw;
            }
            else lGetAutoAuit = GetAutoAuth(out user, out password, out error, out WebServiceExecutionTimeGetAutoAuth);

            if (lGetAutoAuit)
            {
                string UrlAuthLogin = UrlBase + @"Auth/Login";
                //UrlAuthLogin = UrlBase + @"Login";

                PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
                payload.Login = user;
                payload.Password = password;
                
                response = GetResponse(UrlAuthLogin, payload, caller, out error);

                if (error == null)
                {
                    if (response.Data.Token == null || string.IsNullOrEmpty(response.Data.Token) || string.IsNullOrWhiteSpace(response.Data.Token))
                    {
                        error = new Exception(string.Format(caller + ": {0}", "Token NULLO."));
                    }
                    else
                    {
                        token = response.Data.Token;
                        result = true;
                    }
                    if (error == null) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                    else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
                }
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");
            }
            else
            {
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = WebServiceExecutionTimeGetAutoAuth;
            }


            return result;
        }

        #region Crypt user/password operatore per riepiloghi automatici

        private const string indexRiepiloghiAutoAuth = "E5AE450FE2119D0B7EDF48AF";

        public static string Encrypt(string toEncrypt)
        {
            string key = indexRiepiloghiAutoAuth;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt)
        {
            string key = indexRiepiloghiAutoAuth;
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        #endregion

        private static bool GetAccount(string token, out string codiceSistema, out Exception error)
        {
            error = null;
            codiceSistema = "";
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "GetAccount";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni(caller, false));

            string UrlAuthLogin = UrlBase + @"Auth/GetAccount";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = token;
            response = GetResponse(UrlAuthLogin, payload, caller, out error);

            if (error == null)
            {
                if (response.Data.Account == null)
                {
                    error = new Exception(string.Format(caller + ": {0}", "Account NULLO."));
                }
                else
                {
                    if (response.Data.Account.AccountId == null || string.IsNullOrEmpty(response.Data.Account.AccountId) || string.IsNullOrWhiteSpace(response.Data.Account.AccountId))
                    {
                        error = new Exception(string.Format(caller + ": {0}", "AccountId NULLO."));
                    }
                    else if (response.Data.Account.CodiciSistema == null || response.Data.Account.CodiciSistema.Count == 0)
                    {
                        error = new Exception(string.Format(caller + ": {0}", "Lista dei CodiceSistema vuota."));
                    }
                    else
                    {
                        foreach (clsCodiceSistema oCodiceSistema in response.Data.Account.CodiciSistema)
                        {
                            codiceSistema = oCodiceSistema.CodiceSistema;
                            result = true;
                            break;
                        }
                    }
                }

                if (error == null) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        #endregion

        #region Codice Sistema

        private static bool GetTokenCodiceSistema(string token, string codiceSistema, out string tokenCodiceSistema, out DateTime sysdateCodiceSistema, out Exception error)
        {
            tokenCodiceSistema = "";
            error = null;
            sysdateCodiceSistema = DateTime.MinValue;
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "GetTokenCodiceSistema";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni(caller, false));

            string UrlAuthTokenCodiceSistema = UrlBase + @"Auth/GetTokenCodiceSistema";
            //string codiceSistema = "";


            PayLoadRiepiloghi payLoad = new PayLoadRiepiloghi();
            payLoad.Token = token;
            payLoad.CodiceSistema = codiceSistema;

            response = GetResponse(UrlAuthTokenCodiceSistema, payLoad, caller, out error);

            if (error == null)
            {
                sysdateCodiceSistema = response.Status.Sysdate;
                if (response.Data.Token == null || string.IsNullOrEmpty(response.Data.Token) || string.IsNullOrWhiteSpace(response.Data.Token))
                {
                    error = new Exception(string.Format(caller + ": {0}", "Token Codice Sistema NULLO."));
                }
                else
                {
                    tokenCodiceSistema = response.Data.Token;
                    result = true;
                }
                if (error == null) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        #endregion

        #region Get Fasce orarie per Generazione/Invio e ricezione

        private static bool GetFasceOrarieGenerazioneInvioRicezione(string tokenCodiceSistema, DateTime dSysdateCodiceSistema, out Dictionary<string, DateTime> fasciaOrariaGenerazioneRiepiloghi, out Dictionary<string, DateTime> fasiaOrariaVerificaEmail, out Exception error)
        {
            bool result = false;
            error = null;
            fasciaOrariaGenerazioneRiepiloghi = null;
            fasiaOrariaVerificaEmail = null;
            // DALLEORE=00.00;ALLEORE=23.59

            string cFascia = "";
            int nDalOre = 0;
            int nDalMinuti = 0;
            int nAlOre = 0;
            int nAlMinuti = 0;

            Dictionary<string, string> fasceDaPrendere = new Dictionary<string, string>();
            fasceDaPrendere.Add("RIEPILOGHI_AUTO_FASCIA_CREATE", "Fascia Oraria per creazione automatica Riepiloghi");
            fasceDaPrendere.Add("RIEPILOGHI_AUTO_FASCIA_EMAIL", "Fascia Oraria per verifica automatica email Riepiloghi");

            foreach (KeyValuePair<string, string> itemFascia in fasceDaPrendere)
            {
                if (GetProprietaRiepiloghi(tokenCodiceSistema, itemFascia.Key, false, out cFascia, out error) && error == null)
                {
                    if (cFascia != null &&
                        !string.IsNullOrEmpty(cFascia) &&
                        !string.IsNullOrWhiteSpace(cFascia) &&
                        cFascia.Contains(";") &&
                        cFascia.Contains("DALLEORE=") &&
                        cFascia.Contains("ALLEORE=") &&
                        cFascia.Split(';')[0].Replace("DALLEORE=", "").Length == 5 &&
                        cFascia.Split(';')[0].Replace("ALLEORE=", "").Contains(".") &&
                        int.TryParse(cFascia.Split(';')[0].Replace("DALLEORE=", "").Substring(0, 2), out nDalOre) &&
                        int.TryParse(cFascia.Split(';')[0].Replace("DALLEORE=", "").Substring(3, 2), out nDalMinuti) &&
                        int.TryParse(cFascia.Split(';')[1].Replace("ALLEORE=", "").Substring(0, 2), out nAlOre) &&
                        int.TryParse(cFascia.Split(';')[1].Replace("ALLEORE=", "").Substring(3, 2), out nAlMinuti))
                    {
                        DateTime dStartFascia = new DateTime(dSysdateCodiceSistema.Year, dSysdateCodiceSistema.Month, dSysdateCodiceSistema.Day, nDalOre, nDalMinuti, 0);
                        DateTime dStopFascia = new DateTime(dSysdateCodiceSistema.Year, dSysdateCodiceSistema.Month, dSysdateCodiceSistema.Day, nAlOre, nAlOre, 0);

                        if (dStartFascia > dStopFascia)
                        {
                            error = new Exception(itemFascia.Value + " non valida:" + "\r\n" + "Formato corretto: DALLEORE=00.00;ALLEORE=23.59" + "\r\n" + cFascia + "\r\n" + "Orario di inizio maggiore dell'orario di fine.");
                        }
                        else
                        {
                            switch (itemFascia.Key)
                            {
                                case "RIEPILOGHI_AUTO_FASCIA_CREATE":
                                    {
                                        fasciaOrariaGenerazioneRiepiloghi = new Dictionary<string, DateTime>();
                                        fasciaOrariaGenerazioneRiepiloghi.Add("start", dStartFascia);
                                        fasciaOrariaGenerazioneRiepiloghi.Add("stop", dStopFascia);
                                        break;
                                    }
                                case "RIEPILOGHI_AUTO_FASCIA_EMAIL":
                                    {
                                        fasiaOrariaVerificaEmail = new Dictionary<string, DateTime>();
                                        fasiaOrariaVerificaEmail.Add("start", dStartFascia);
                                        fasiaOrariaVerificaEmail.Add("stop", dStopFascia);
                                        break;
                                    }
                            }
                        }
                    }
                    else
                        error = new Exception(itemFascia.Value + " non valida:" + "\r\n" + "Formato corretto: DALLEORE=00.00;ALLEORE=23.59" + "\r\n" + cFascia);
                }
                else
                {
                    if (error == null)
                        error = new Exception("Lettura " + itemFascia.Value + " non riuscita.");
                    else
                        error = new Exception("Lettura " + itemFascia.Value + " non riuscita." + "\r\n" + error.Message);
                }

                if (error != null) break;
            }

            result = (error == null);

            return result;
        }

        #endregion

        #region Generazione riepiloghi pending

        private static string GetDescrizioneRiepilogo(clsRiepilogoGeneratoWebApi itemRiepilogo)
        {
            return GetDescrizioneRiepilogo(itemRiepilogo, false);
        }
        private static string GetDescrizioneRiepilogo(clsRiepilogoGeneratoWebApi itemRiepilogo, bool emailDescription)
        {
            string result = "";
            switch (itemRiepilogo.Tipo)
            {
                case "G": { result = "RPG Giornaliero " + itemRiepilogo.Giorno.ToShortDateString() + " Prog. " + itemRiepilogo.Progressivo.ToString() ; break; }
                case "M": { result = "RPM Mensile " + itemRiepilogo.Giorno.ToShortDateString() + " Prog. " + itemRiepilogo.Progressivo.ToString(); break; }
                case "R": { result = "RCA Accessi " + itemRiepilogo.Giorno.ToShortDateString() + " Prog. " + itemRiepilogo.Progressivo.ToString(); break; }
            }

            if (emailDescription)
            {
                if (itemRiepilogo.EmailSend_Sent == null) // da Spedire
                {
                    result += " DA SPEDIRE";
                    
                    if (itemRiepilogo.EmailSend_Last != null)
                    {
                        result += " Ultimo tentativo di spedizione " + ((DateTime)itemRiepilogo.EmailSend_Last).ToShortDateString() + " " + ((DateTime)itemRiepilogo.EmailSend_Last).ToShortTimeString();
                    }
                    if (itemRiepilogo.EmailSend_Error != null && !string.IsNullOrEmpty(itemRiepilogo.EmailSend_Error) && !string.IsNullOrWhiteSpace(itemRiepilogo.EmailSend_Error))
                        result += " Err.Spedizione: " + itemRiepilogo.EmailSend_Error;
                }
                else if (itemRiepilogo.EmailSend_Sent != null && itemRiepilogo.EmailReturn_Date == null) // da Ricevere
                {
                    result += " IN ATTESA DI RISPOSTA";
                    if (itemRiepilogo.EmailRead_Last != null)
                    {
                        result += " Ultimo tentativo di lettura " + ((DateTime)itemRiepilogo.EmailRead_Last).ToShortDateString() + " " + ((DateTime)itemRiepilogo.EmailRead_Last).ToShortTimeString();
                    }

                    if (itemRiepilogo.EmailRead_Error != null && !string.IsNullOrEmpty(itemRiepilogo.EmailRead_Error) && !string.IsNullOrWhiteSpace(itemRiepilogo.EmailRead_Error))
                        result += " Err.Lettura: " + itemRiepilogo.EmailRead_Error;
                }
                else if (itemRiepilogo.EmailSend_Sent != null && itemRiepilogo.EmailReturn_Date != null) // inviato e ricevuto
                {
                    result += " RISPOSTA RICEVUTA" + ((DateTime)itemRiepilogo.EmailReturn_Date).ToShortDateString() + " " + ((DateTime)itemRiepilogo.EmailReturn_Date).ToShortTimeString();
                    result += "\r\n" + " Codice: " + itemRiepilogo.EmailReturn_Code.ToString();
                    if (itemRiepilogo.EmailReturn_Body != null)
                    {
                        result += "\r\n" + " Messaggio:" + "\r\n" + itemRiepilogo.EmailReturn_Body;
                    }
                }
            }
            return result;    
        }

        private static bool GetListaRiepiloghiDaGenerare(string tokenCodiceSistema, out List<clsRiepilogoGeneratoWebApi> listaRiepiloghiDaGenerare, out DateTime sysdateCodiceSistema, out Exception error)
        {
            error = null;
            bool result = false;
            listaRiepiloghiDaGenerare = null;
            sysdateCodiceSistema = DateTime.MinValue;

            ResponseRiepiloghi response = null;
            string caller = "GetListaRiepiloghiDaGenerare";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Lettura riepiloghi da generare", false));

            string UrlRiepiloghiPending = UrlBase + @"Riepiloghi/ListaRiepiloghi";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = tokenCodiceSistema;
            payload.Mode = "DA_GENERARE";
            response = GetResponse(UrlRiepiloghiPending, payload, caller, out error);

            if (error == null)
            {
                if (response.Data.ListaRiepiloghi == null)
                {
                    error = new Exception(string.Format(caller + ": {0}", "ListaRiepiloghi NULLO."));
                }
                else 
                {
                    listaRiepiloghiDaGenerare = response.Data.ListaRiepiloghi;
                    sysdateCodiceSistema = response.Status.Sysdate;
                    result = true;
                }
                if (error == null)
                {
                    sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                    if (listaRiepiloghiDaGenerare.Count == 0) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Nessun riepilogo da generare.");
                    else if (listaRiepiloghiDaGenerare.Count == 1) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Un riepilogo da generare.");
                    else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(listaRiepiloghiDaGenerare.Count.ToString() + " riepiloghi da generare.");
                }
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }
        //private static bool GeneraRiepilogo(string tokenCodiceSistema, clsRiepilogoGeneratoWebApi riepiloghiDaGenerare, bool createDIRECT, out Exception error)
        private static bool GeneraRiepilogo(string tokenCodiceSistema, clsRiepilogoGeneratoWebApi riepiloghiDaGenerare, out Exception error)
        {
            error = null;
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "GeneraRiepilogo";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Genera riepilogo" + " " + GetDescrizioneRiepilogo(riepiloghiDaGenerare), true));
            sequenzaOperazioni[sequenzaOperazioni.Count - 1].TypeItem = "RP";
            sequenzaOperazioni[sequenzaOperazioni.Count - 1].Riepilogo = riepiloghiDaGenerare;

            string UrlGeneraRiepilogo = UrlBase + @"Riepiloghi/GeneraRiepilogo";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = tokenCodiceSistema;
            payload.DataRiepilogo = riepiloghiDaGenerare.Giorno;
            payload.TipoRiepilogo = riepiloghiDaGenerare.Tipo;
            //if (createDIRECT)
            //    response = GeneraRiepilogoDirect(payload, out error);
            //else
            //    response = GetResponse(UrlGeneraRiepilogo, payload, caller, out error);
            response = GetResponse(UrlGeneraRiepilogo, payload, caller, out error);
            if (error == null)
            {
                if (response.Data.RichiestaRiepilogo == null)
                {
                    error = new Exception(string.Format(caller + ": {0}", "RichiestaRiepilogo NULLO."));
                }
                else
                {
                    if (response.Data.RichiestaRiepilogo.ErrorMessage != null && (!string.IsNullOrEmpty(response.Data.RichiestaRiepilogo.ErrorMessage) || !string.IsNullOrWhiteSpace(response.Data.RichiestaRiepilogo.ErrorMessage)))
                    {
                        error = new Exception(string.Format(caller + ": {0}", response.Data.RichiestaRiepilogo.ErrorMessage));
                    }
                    else
                    {
                        result = true;
                        sequenzaOperazioni[sequenzaOperazioni.Count - 1].Riepilogo = response.Data.RichiestaRiepilogo;
                    }
                }
                if (error == null) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        //public static clsEnvironment GetEnvironment()
        //{
        //    System.IO.FileInfo fInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().FullName);
        //    var result = new clsEnvironment()
        //    {
        //        InternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["internalConnectionString"].ConnectionString,
        //        ExternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["externalConnectionString"].ConnectionString,
        //        PathFont = System.Configuration.ConfigurationManager.AppSettings["pathFont"],
        //        PathRisorseWeb = System.Configuration.ConfigurationManager.AppSettings["pathRisorseWeb"],
        //        PathTemp = fInfo.Directory.FullName,
        //        PathWriteRiepiloghi = System.Configuration.ConfigurationManager.AppSettings["pathWrite"]
        //    };
        //    return result;
        //}

        //public static ResponseRiepiloghi GeneraRiepilogoDirect(PayLoadRiepiloghi parPayload, out Exception error)
        //{
        //    error = null;
        //    ResponseRiepiloghi result = null;
        //    var rb = CreaRiepilogoDirect(parPayload.Token, parPayload.DataRiepilogo, parPayload.TipoRiepilogo, GetEnvironment(), out error);
        //    result = GetResponse(rb);
        //    return result;
        //}

        public static ResponseRiepiloghi GetResponse(Object parResponse)
        {
            if (parResponse == null)
            {
                return new ResponseRiepiloghi()
                {
                    Status = {
                        StatusOK = false,
                        StatusMessage = "parResponse = null"
                    }
                };
            }

            var result = new ResponseRiepiloghi()
            {
                Data = JsonConvert.DeserializeObject<ResponseDataRiepiloghi>(JsonConvert.SerializeObject(parResponse)),
                Status = JsonConvert.DeserializeObject<ResultBase>(JsonConvert.SerializeObject(parResponse))
            };

            return result;
        }

        public static void AddLog(string message, Exception error = null)
        {
            if (!lAutoMode || lVerbose)
            {
                Console.WriteLine(message);
                if (error != null)
                    Console.WriteLine(error.Message);
            }
        }

        //public static ResultBaseGenerazioneRiepilogo CreaRiepilogoDirect(string Token, DateTime Giorno, string GiornalieroMensile, clsEnvironment oEnvironment, out Exception oError)
        //{
        //    AddLog("Avvio generazione riepilogo...");
        //    Riepiloghi.clsLogRequest oLogRequest = null;
        //    DateTime dStart = DateTime.Now;
        //    bool lRet = false;
        //    oError = null;
        //    string cFileLogFirmato = "";
        //    string cFileRiepilogoFirmato = "";
        //    string cFileEmailFirmato = "";
        //    ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
        //    //string PathXslt = HttpContext.Current.Server.MapPath("RisorseWeb");
        //    string pathRisorse = oEnvironment.PathRisorseWeb;
        //    Int64 Progressivo = 0;

        //    Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
        //    percorsi.Risorse = oEnvironment.PathRisorseWeb;
        //    percorsi.Temp = oEnvironment.PathTemp;
        //    percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;

        //    IConnection oConnectionInternal = null;
        //    IConnection oConnection = null;
        //    Riepiloghi.clsRiepilogoGeneratoWebApi dettaglioRiepilogo = null;
        //    DateTime dSysdate = DateTime.MinValue;

        //    try
        //    {
        //        AddLog("Connessione interna...");
        //        oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
        //        if (oError != null) AddLog("errore", oError);


        //        if (oError == null)
        //        {
        //            dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);
        //            AddLog(string.Format("{0}", dSysdate.ToLongDateString()));
        //            oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreaRiepilogo", Token, new Dictionary<string, object>() { { "Giorno", Giorno }, { "GiornalieroMensile", GiornalieroMensile } });

        //            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_CREAZIONE", "ok");
        //            AddLog("Connessione interna...", oError);
        //            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
        //            if (oError != null) AddLog("errore", oError);
        //            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

        //            if (oError == null && GiornalieroMensile == "G")
        //            {
        //                AddLog("Check RPG...", oError);
        //                bool lGeneraRPGGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RPG_FUTURO", "DISABILITATO") == "ABILITATO";
        //                if (!lGeneraRPGGiorniFuturi)
        //                {
        //                    try
        //                    {
        //                        DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
        //                        if (Giorno.Date > dSysdateCodiceSistema.Date)
        //                        {
        //                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo giornaliero", string.Format("Impossibile Generare il Riepilogo Giornaliero di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                    }
        //                }
        //            }

        //            if (oError == null && GiornalieroMensile == "R")
        //            {
        //                AddLog("Check RCA...", oError);
        //                bool lGeneraRCAGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RCA_FUTURO", "DISABILITATO") == "ABILITATO";

        //                if (!lGeneraRCAGiorniFuturi)
        //                {
        //                    try
        //                    {
        //                        DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
        //                        if (Giorno.Date > dSysdateCodiceSistema.Date)
        //                        {
        //                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo controllo accessi", string.Format("Impossibile Generare il Riepilogo Controllo Accessi di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                    }
        //                }
        //            }

        //            if (oError != null) AddLog("errore", oError);

        //            if (oError == null)
        //            {
        //                AddLog("Check Server fiscale...", oError);
        //                bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
        //                if (lCheckServerFiscale)
        //                {
        //                    if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
        //                    {
        //                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
        //                    }
        //                }
        //            }

        //            if (oError != null) AddLog("errore", oError);

        //            if (oError == null)
        //            {
        //                if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
        //                {
        //                    AddLog("Check Multicinema...", oError);
        //                    oError = new Exception("Operazione disponibile solo per il titolare del sistema");
        //                }
        //            }

        //            if (oError != null) AddLog("errore", oError);

        //            if (oError == null)
        //            {
        //                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
        //                Riepiloghi.clsWinServiceClientOperation oClientOperation = new Riepiloghi.clsWinServiceClientOperation(Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);
        //                long IdOperazione = 0;

        //                Riepiloghi.clsWinServiceConfig config = Riepiloghi.clsWinServiceConfig.GetWinServiceConfig(oConnection, out oError, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);

        //                lRet = oError == null;

        //                if (oError != null) AddLog("errore", oError);

        //                if (lRet)
        //                {
        //                    AddLog("Avvio generazione...", oError);
        //                    Riepiloghi.clsRiepilogoGenerato riepilogoGenerato = null;
        //                    oClientOperation.MessaggesWinServiceClientOperation += (m, e) =>
        //                    {
        //                        AddLog(m, e);
        //                    };
        //                    lRet = oClientOperation.PushOperazione(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.AccountId, percorsi, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, GiornalieroMensile, Giorno, 0, out IdOperazione, out riepilogoGenerato, out oError);

        //                    if (oError != null) AddLog("errore", oError);

        //                    if (lRet && oError == null)
        //                    {
        //                        dettaglioRiepilogo = new Riepiloghi.clsRiepilogoGeneratoWebApi(riepilogoGenerato);

        //                        // TOLGO IL PERCORSO DAL NOME DEL FILE DI RIEPILOGO
        //                        try
        //                        {
        //                            if (dettaglioRiepilogo.Nome != null && !string.IsNullOrEmpty(dettaglioRiepilogo.Nome) && !string.IsNullOrWhiteSpace(dettaglioRiepilogo.Nome))
        //                            {
        //                                System.IO.FileInfo oNomeInfo = new System.IO.FileInfo(dettaglioRiepilogo.Nome);
        //                                dettaglioRiepilogo.Nome = oNomeInfo.Name;
        //                            }
        //                        }
        //                        catch (Exception)
        //                        {
        //                        }
        //                        string cDesc = string.Format("Generazione Riepilogo {0} {1}", (GiornalieroMensile == "G" ? "Giornaliero" : (GiornalieroMensile == "M" ? "Mensile" : "Controllo accessi")), (config.ComunicationMode == Riepiloghi.clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_DIRECT) ? "terminata" : "avviata");
        //                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, cDesc, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);

        //                        Exception exLista = null;
        //                        List<Riepiloghi.clsRiepilogoGenerato> lista = Riepiloghi.clsRiepilogoGenerato.GetRigaRiepiloghiProgs(oConnection, out exLista, "COMPETENZA", GiornalieroMensile, Giorno, Giorno, 0);
        //                        if (exLista == null && lista != null)
        //                        {
        //                            oResult.ListaRiepiloghi = Riepiloghi.clsRiepilogoGeneratoWebApi.GetList(lista);
        //                        }

        //                        exLista = null;
        //                        oResult.EmailToSendOrReceive = Riepiloghi.clsRiepiloghi.CheckRiepiloghiEmailPending(oConnection, true, true, out exLista);
        //                        oResult.Sysdate = dSysdate;
        //                    }
        //                    else
        //                    {
        //                        lRet = false;
        //                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
        //                        oResult.Sysdate = dSysdate;
        //                    }
        //                    if (oError != null) AddLog("errore", oError);
        //                }
        //                else
        //                {
        //                    lRet = false;
        //                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
        //                    oResult.Sysdate = dSysdate;
        //                    if (oError != null) AddLog("errore", oError);
        //                }
        //            }
        //            else
        //            {
        //                lRet = false;
        //                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
        //                oResult.Sysdate = dSysdate;
        //                if (oError != null) AddLog("errore", oError);
        //            }
        //        }
        //        else
        //        {
        //            lRet = false;
        //            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
        //            oResult.Sysdate = dSysdate;
        //            if (oError != null) AddLog("errore", oError);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lRet = false;
        //        oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
        //        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
        //        oResult.Sysdate = dSysdate;
        //        if (oError != null) AddLog("errore", oError);
        //    }
        //    finally
        //    {
        //        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
        //        if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
        //    }
        //    return oResult;

        //}

        private static ResultContentFile StampaRiepilogo(string tokenCodiceSistema, clsRiepilogoGeneratoWebApi riepiloghiDaGenerare, out Exception error)
        {
            error = null;
            ResultContentFile result = null;

            ResponseRiepiloghi response = null;
            string caller = "StampaRiepilogo";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Stampa riepilogo" + " " + GetDescrizioneRiepilogo(riepiloghiDaGenerare), false));

            string UrlGeneraRiepilogo = UrlBase + @"Riepiloghi/StampaRiepilogo";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = tokenCodiceSistema;
            payload.DataRiepilogo = riepiloghiDaGenerare.Giorno;
            payload.TipoRiepilogo = riepiloghiDaGenerare.Tipo;
            response = GetResponse(UrlGeneraRiepilogo, payload, caller, out error);

            if (error == null)
            {
                if (response.Data.contentFile == null || response.Data.contentFile.Buffer == null || response.Data.contentFile.Buffer.Length == 0)
                {
                    error = new Exception(string.Format(caller + ": {0}", "StampaRiepilogo NULLO."));
                }
                else
                {
                    result = response.Data.contentFile;
                }
                if (error == null) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        #endregion

        #region Invia e Ricevi

        private static bool ItemsEquals(clsRiepilogoGeneratoWebApi itemFromWebApi, clsRiepilogoGeneratoWebApi itemDatabase)
        {
            bool result = false;
            if (itemFromWebApi == null && itemDatabase == null)
            {
                result = false;
            }
            else if (itemFromWebApi == null && itemDatabase != null)
            {
                result = false;
            }
            else if (itemFromWebApi != null && itemDatabase == null)
            {
                result = false;
            }
            else
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(itemFromWebApi) == Newtonsoft.Json.JsonConvert.SerializeObject(itemDatabase);
            }
            return result;
        }

        private static bool SendReceiveRiepiloghi(string tokenCodiceSistema, out List<clsRiepilogoGeneratoWebApi> listaRiepiloghiInviaRicevi, out DateTime sysdateCodiceSistema, int countRiepiloghiGenerati, out Exception error)
        {
            error = null;
            bool result = false;
            listaRiepiloghiInviaRicevi = null;
            sysdateCodiceSistema = DateTime.MinValue;

            ResponseRiepiloghi response = null;
            string caller = "SendReceiveRiepiloghi";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Invia e Ricevi" + (countRiepiloghiGenerati > 0 ? string.Format(" (a seguito della generazione di {0} riepiloghi)", countRiepiloghiGenerati.ToString()): ""), true));

            string UrlRiepiloghiPending = UrlBase + @"Riepiloghi/SendReceiveRiepiloghi";

            PayLoadRiepiloghi payload = new PayLoadRiepiloghi();
            payload.Token = tokenCodiceSistema;
            response = GetResponse(UrlRiepiloghiPending, payload, caller, out error);

            if (error == null)
            {
                if (response.Data.ListaRiepiloghi == null)
                {
                    error = new Exception(string.Format(caller + ": {0}", "ListaRiepiloghi NULLO."));
                }
                else
                {
                    listaRiepiloghiInviaRicevi = response.Data.ListaRiepiloghi;
                    sysdateCodiceSistema = response.Status.Sysdate;
                    result = true;
                }
                if (error == null)
                {
                    sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                    if (listaRiepiloghiInviaRicevi.Count == 0) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Nessun riepilogo in Invio e Ricezione.");
                    else if (listaRiepiloghiInviaRicevi.Count == 1) sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add("Un riepilogo in Invio e Ricezione.");
                    else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(listaRiepiloghiInviaRicevi.Count.ToString() + " riepiloghi in Invio e Ricezione.");

                    if (listaRiepiloghiInviaRicevi.Count == 0)
                    {
                        ApplicationDb.itemsEmailLastSendReceive = new List<clsRiepilogoGeneratoWebApi>();
                    }
                    else
                    {
                        foreach (clsRiepilogoGeneratoWebApi itemRiepilogo in listaRiepiloghiInviaRicevi)
                        {
                            bool lFindInApplicationDb = false;
                            if (ApplicationDb.itemsEmailLastSendReceive != null && ApplicationDb.itemsEmailLastSendReceive.Count > 0)
                            {
                                foreach (clsRiepilogoGeneratoWebApi itemDatabase in ApplicationDb.itemsEmailLastSendReceive)
                                {
                                    if (ItemsEquals(itemRiepilogo, itemDatabase))
                                    {
                                        lFindInApplicationDb = true;
                                        break;
                                    }
                                }
                            }

                            if (!lFindInApplicationDb)
                            {
                                sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni("Invio Ricezione " + GetDescrizioneRiepilogo(itemRiepilogo, false), true));
                                sequenzaOperazioni[sequenzaOperazioni.Count - 1].TypeItem = "EM";
                                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Riepilogo = itemRiepilogo;
                                if (itemRiepilogo.EmailSend_Error != null)
                                    sequenzaOperazioni[sequenzaOperazioni.Count - 1].error = new Exception(itemRiepilogo.EmailSend_Error);
                                else if (itemRiepilogo.EmailRead_Error != null)
                                    sequenzaOperazioni[sequenzaOperazioni.Count - 1].error = new Exception(itemRiepilogo.EmailRead_Error);
                            }
                        }
                    }
                }
                else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            ApplicationDb.itemsEmailLastSendReceive = listaRiepiloghiInviaRicevi;

            return result;
        }

        #endregion

        #region Get Proprieta riepiloghi

        private static bool GetProprietaRiepiloghi(string tokenCodiceSistema, string proprieta, bool secretValue, out string valore, out Exception error)
        {
            error = null;
            valore = "";
            bool result = false;
            ResponseRiepiloghi response = null;
            string caller = "GetProprietaRiepiloghi";
            sequenzaOperazioni.Add(new clsItemSequenzaAutoOperazioni(string.Format("Richiesta proprietà {0}", proprieta), false));

            string UrlGetProprieta = UrlBase + @"Riepiloghi/GetProprietaRiepiloghi";

            if (proprieta == "EmailCodiceSistema")
                UrlGetProprieta = UrlBase + @"Riepiloghi/GetEmailCodiceSistema";

            PayLoadRiepiloghi payLoad = new PayLoadRiepiloghi();
            payLoad.Token = tokenCodiceSistema;
            payLoad.Proprieta = proprieta;

            response = GetResponse(UrlGetProprieta, payLoad, caller, out error);

            if (error == null)
            {
                valore = response.Data.Valore;
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop();
                sequenzaOperazioni[sequenzaOperazioni.Count - 1].Messages.Add(string.Format(" valore ottenuto: {0}", (secretValue ? "nascosto" : valore)));

                result = true;
            }
            else sequenzaOperazioni[sequenzaOperazioni.Count - 1].Stop(error);

            sequenzaOperazioni[sequenzaOperazioni.Count - 1].WebServiceExecutionTime = (response != null && response.Status != null && response.Status.execution_time != null ? response.Status.execution_time : "");

            return result;
        }

        #endregion

        #region Invio richiesta

        private static ResponseRiepiloghi GetResponse(string Url, PayLoadRiepiloghi payload, string caller, out Exception error)
        {

            error = null;
            ResponseRiepiloghi response = null;

            string headerError = caller + ": {0}";

            string cResponseString = "";

            try
            {
                cResponseString = GetWebRequest(Url, payload);
            }
            catch (Exception exUrl)
            {
                error = new Exception(string.Format(headerError + " " + Url, exUrl.Message));
                response = null;
            }

            if (error == null)
            {
                try
                {
                    response = JsonToObject<ResponseRiepiloghi>(cResponseString);

                    if (response == null)
                        error = new Exception(string.Format(headerError, "nessuna risposta."));
                    else
                    {
                        if (response.Status == null)
                        {
                            error = new Exception(string.Format(headerError, "Status NULL."));
                        }
                        else
                        {
                            if (!response.Status.StatusOK)
                            {
                                error = new Exception(string.Format(headerError, response.Status.StatusMessage));
                            }
                            else
                            {
                                if (response.Data == null)
                                {
                                    error = new Exception(string.Format(headerError, "Data NULL."));
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = new Exception(string.Format(headerError, ex.Message));
                    response = null;
                }
            }
            return response;
        }

        private static T JsonToObject<T>(string parJson)
        {
            return (T)JsonConvert.DeserializeObject(parJson, typeof(T));
        }

        private static string GetWebRequest(string parUrl, object parRequest)
        {
            string result = string.Empty;

            HttpWebResponse resp = null;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(parUrl);
            req.CookieContainer = new CookieContainer();
            req.Credentials = CredentialCache.DefaultNetworkCredentials;
            req.UserAgent = ": Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
            req.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Anonymous;
            req.Method = "POST";
            req.ContentType = "text/json";

            string json = JsonConvert.SerializeObject(parRequest);

            using (StreamWriter streamWriter = new StreamWriter(req.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();

                resp = (HttpWebResponse)req.GetResponse();
                Encoding enc = Encoding.UTF8;
                using (StreamReader responseStream = new StreamReader(resp.GetResponseStream(), enc))
                {
                    result = responseStream.ReadToEnd();
                    responseStream.Close();
                }
                resp.Close();
            }
            return result;
        }

        #endregion

        #region email di servizio

        private static bool GetParametriEmailCodiceSistema(string tokenCodiceSistema, out string emailCodiceSistema, out string smtpHost, out int smtpPort, out string useDefaultCredentials, out string useSSL, out string smtpUser, out string smptPassword, out Exception error)
        {
            bool result = false;
            error = null;
            emailCodiceSistema = "";
            smtpHost = "";
            smtpPort = 0;
            string c_smtpPort = "";
            useDefaultCredentials = "";
            useSSL = "";
            smtpUser = "";
            smptPassword = "";
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EmailCodiceSistema", false, out emailCodiceSistema, out error);
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_SMTP", false, out smtpHost, out error);
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_PORT_OUT", false, out c_smtpPort, out error);
            if (error == null && c_smtpPort .Trim() != "" && !int.TryParse(c_smtpPort, out smtpPort))
            {
                error = new Exception("Porta spedizione email errata");
            }
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_USE_DEFAULT_CREDENTIALS", false, out useDefaultCredentials, out error);
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_USE_SSL", false, out useSSL, out error);
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_USE_USER", true, out smtpUser, out error);
            if (error == null) GetProprietaRiepiloghi(tokenCodiceSistema, "EMAIL_USE_PASSWORD", true, out smptPassword, out error);



            result = (error == null);

            return result;
        }

        private static bool SendEmailServizio(string tokenCodiceSistema, string to, string destCC, string destCCN, Exception error, List<clsItemSequenzaAutoOperazioni> operazioni, bool sendPDF_RPG, bool sendPDF_RPM, bool sendPDF_RCA, ref clsSequenzaOperazioni operazioniReport)
        {
            bool result = false;

            bool somethingToSend = false;
            bool oneErrorInSequenza = false;
            bool checkItemPDF = false;

            int countImportant = 0;
            int countImportantPdf = 0;

            #region verifiche iniziali

            operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Analisi operazioni per eventuale invio", false));

            if (error != null) somethingToSend = true;

            if (!somethingToSend)
            {
                foreach (clsItemSequenzaAutoOperazioni itemSequenza in operazioni)
                {
                    if (itemSequenza.error != null || itemSequenza.Important)
                        countImportant += 1;

                    oneErrorInSequenza = oneErrorInSequenza || (itemSequenza.error != null);
                    somethingToSend = somethingToSend || oneErrorInSequenza || itemSequenza.Important;

                    if (itemSequenza.Riepilogo != null &&
                        itemSequenza.contentFile != null &&
                        itemSequenza.contentFile.Buffer != null &&
                        itemSequenza.contentFile.Buffer.Length > 0 &&
                        ((sendPDF_RPG && itemSequenza.Riepilogo.Tipo == "G") ||
                         (sendPDF_RPM && itemSequenza.Riepilogo.Tipo == "M") ||
                         (sendPDF_RCA && itemSequenza.Riepilogo.Tipo == "R")))
                    {
                        checkItemPDF = true;
                        countImportantPdf += 1;
                    }
                }
            }
            operazioniReport[operazioniReport.Count - 1].Stop();
            operazioniReport[operazioniReport.Count - 1].Messages.Add((somethingToSend ? string.Format("invio report necessario per {0} elementi.", countImportant.ToString()) : "nessun report da generare"));

            #endregion

            #region parametri per spedizione

            string emailCodiceSistema = "";
            string smtp = "";
            int port = 0;
            string useDefaultCredential = "";
            string enableSsl = "";
            string user = "";
            string password = "";
            Exception errorGetParams = null;

            if (somethingToSend)
            {
                somethingToSend = GetParametriEmailCodiceSistema(tokenCodiceSistema, out emailCodiceSistema, out smtp, out port, out useDefaultCredential, out enableSsl, out user, out password, out errorGetParams);
            }

            #endregion

            if (somethingToSend && errorGetParams == null)
            {
                try
                {
                    #region inizializzazione del client di spedizione

                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Inizializzazione client di spedizione", false));

                    System.Net.Mail.SmtpClient oClient = null;
                    if (port == 0)
                    {
                        oClient = new System.Net.Mail.SmtpClient(smtp);
                    }
                    else
                    {
                        oClient = new System.Net.Mail.SmtpClient(smtp, port);
                    }

                    oClient.UseDefaultCredentials = (useDefaultCredential.Trim().ToUpper() == "TRUE");
                    oClient.EnableSsl = (enableSsl.Trim().ToUpper() == "TRUE");

                    if (user.Trim() != "" && password.Trim() != "")
                    {
                        oClient.Credentials = new System.Net.NetworkCredential(user, password);
                    }
                    oClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    operazioniReport[operazioniReport.Count - 1].Stop();

                    #endregion

                    #region messaggio

                    System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();

                    #region Intestazioni del messaggio

                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Inizializzazione intestazione messaggio", false));

                    oMessage.From = new System.Net.Mail.MailAddress(emailCodiceSistema);
                    foreach (string cItem in (to + ";").Split(';'))
                    {
                        if (cItem.TrimEnd() != "")
                        {
                            oMessage.To.Add(cItem);
                        }
                    }
                    foreach (string cItem in (destCC + ";").Split(';'))
                    {
                        if (cItem.TrimEnd() != "")
                        {
                            oMessage.CC.Add(cItem);
                        }
                    }
                    foreach (string cItem in (destCCN + ";").Split(';'))
                    {
                        if (cItem.TrimEnd() != "")
                        {
                            oMessage.Bcc.Add(cItem);
                        }
                    }
                    oMessage.IsBodyHtml = true;
                    oMessage.Subject = "RIEPILOGHI AUTOMATICI";
                    if (error != null || oneErrorInSequenza)
                    {
                        oMessage.Subject += " terminati con errori.";
                    }

                    operazioniReport[operazioniReport.Count - 1].Stop();

                    #endregion

                    #region corpo del messaggio

                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Report", false));

                    string html_lastType = "_";

                    Dictionary<string, clsItemTemplate> templateList = clsItemTemplate.GetTemplateModels(Properties.Resources.emailRiepiloghiAutomatici_html);

                    clsItemTemplate templateHtml = new clsItemTemplate(templateList["{HTML}"]);
                    clsItemTemplate templateTable = null; // templateList["{TABLE}"];
                    clsItemTemplate templateItem = null;
                    string keyTemplate = "";

                    int indexItem = 0;

                    foreach (clsItemSequenzaAutoOperazioni itemSequenza in operazioni)
                    {
                        if (itemSequenza.Important || itemSequenza.error != null)
                        {
                            indexItem += 1;
                            if (itemSequenza.TypeItem != html_lastType)
                            {

                                templateTable = new clsItemTemplate(templateList["{TABLE}"]);
                                templateHtml.subItems.Add(templateTable);

                                string title = "";
                                switch (itemSequenza.TypeItem)
                                {
                                    case "": { title = "Operazione Automatica"; break; }
                                    case "RP": { title = "Generazione Automatica Riepiloghi"; break; }
                                    case "EM": { title = "Invio Ricezione Automatica Email"; break; }
                                }

                                templateTable.SetTemplate(new Dictionary<string, string>() { { "{TITLE}", title } });
                            }

                            if (itemSequenza.TypeItem == "")
                            {
                                keyTemplate = "";
                                if (itemSequenza.error == null) keyTemplate = "{OPERATION_OK}";
                                else keyTemplate = "{OPERATION_ERR}";

                                templateItem = new clsItemTemplate(templateList[keyTemplate]);
                                templateTable.subItems.Add(templateItem);
                                templateItem.SetTemplate(new Dictionary<string, string>() { { "{NAME}", itemSequenza.Operation },
                                                                                            { "{DESCRIPTION}", itemSequenza.Description(true) } });
                            }
                            else if ((itemSequenza.TypeItem == "RP" || itemSequenza.TypeItem == "EM") && itemSequenza.Riepilogo != null)
                            {
                                string errorMessage = "";
                                if (itemSequenza.error != null)
                                    errorMessage += (!string.IsNullOrEmpty(errorMessage) ? "\r\n" : "") + itemSequenza.error.Message;

                                if (itemSequenza.Riepilogo.ErrorMessage != null || !string.IsNullOrEmpty(itemSequenza.Riepilogo.ErrorMessage) || !string.IsNullOrWhiteSpace(itemSequenza.Riepilogo.ErrorMessage))
                                    errorMessage += (!string.IsNullOrEmpty(errorMessage) ? "\r\n" : "") + itemSequenza.Riepilogo.ErrorMessage;

                                if (!string.IsNullOrEmpty(errorMessage))
                                {
                                    errorMessage += (!string.IsNullOrEmpty(errorMessage) ? "\r\n" : "") + itemSequenza.Description(true);
                                }

                                string messages = "";
                                if (itemSequenza.Messages != null && itemSequenza.Messages.Count > 0)
                                {
                                    foreach (string line in itemSequenza.Messages)
                                    {
                                        messages += (messages.Trim() == "" ? "" : "\r\n") + line;
                                    }
                                }

                                if (itemSequenza.Riepilogo.DataOraGenerazione == null)
                                {
                                    keyTemplate = "";

                                    if (!string.IsNullOrEmpty(errorMessage)) keyTemplate = "{CREATE_ERROR}";
                                    else keyTemplate = "{CREATE}";

                                    templateItem = new clsItemTemplate(templateList[keyTemplate]);
                                    templateTable.subItems.Add(templateItem);

                                    templateItem.SetTemplate(new Dictionary<string, string>() { { "{NAME}", itemSequenza.Operation },
                                                                                                { "{ERROR}", errorMessage },
                                                                                                { "{DESCRIPTION}", messages } });
                                }
                                else if (itemSequenza.Riepilogo.EmailSend_Sent == null)
                                {
                                    keyTemplate = "";

                                    if (!string.IsNullOrEmpty(errorMessage)) keyTemplate = "{TOSEND_ERROR}";
                                    else keyTemplate = "{TOSEND}";

                                    templateItem = new clsItemTemplate(templateList[keyTemplate]);
                                    templateTable.subItems.Add(templateItem);

                                    templateItem.SetTemplate(new Dictionary<string, string>() { { "{NAME}", itemSequenza.Operation }, 
                                                                                                { "{CREATE_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.DataOraGenerazione) },
                                                                                                { "{ERROR}", errorMessage },
                                                                                                { "{DESCRIPTION}", messages } });
                                }
                                else if (itemSequenza.Riepilogo.EmailReturn_Date == null)
                                {
                                    keyTemplate = "";

                                    if (!string.IsNullOrEmpty(errorMessage)) keyTemplate = "{TORECEIVE_ERROR}";
                                    else keyTemplate = "{TORECEIVE}";

                                    templateItem = new clsItemTemplate(templateList[keyTemplate]);
                                    templateTable.subItems.Add(templateItem);

                                    templateItem.SetTemplate(new Dictionary<string, string>() { { "{NAME}", itemSequenza.Operation }, 
                                                                                                { "{CREATE_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.DataOraGenerazione) },
                                                                                                { "{SENT_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.EmailSend_Sent) },
                                                                                                { "{ERROR}", errorMessage },
                                                                                                { "{DESCRIPTION}", messages } });
                                }
                                else
                                {
                                    keyTemplate = "";

                                    if (!string.IsNullOrEmpty(errorMessage)) keyTemplate = "{RECEIVED_ERR}";
                                    else keyTemplate = "{RECEIVED}";

                                    templateItem = new clsItemTemplate(templateList[keyTemplate]);
                                    templateTable.subItems.Add(templateItem);
                                    
                                    templateItem.SetTemplate(new Dictionary<string, string>() { { "{NAME}", itemSequenza.Operation }, 
                                                                                                { "{CREATE_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.DataOraGenerazione) },
                                                                                                { "{SENT_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.EmailSend_Sent) },
                                                                                                { "{RECEIVED_DATE}", GetDescrDateTime((DateTime)itemSequenza.Riepilogo.EmailReturn_Date) },
                                                                                                { "{RETURN_CODE}", itemSequenza.Riepilogo.EmailReturn_Code.ToString() },
                                                                                                { "{DESCRIPTION}", itemSequenza.Riepilogo.EmailReturn_Body },
                                                                                                { "{ERROR}", errorMessage } });
                                }
                            }
                        }
                    }

                    oMessage.Body = templateHtml.ReplaceSubItems();

                    operazioniReport[operazioniReport.Count - 1].Stop();

                    #endregion

                    #endregion

                    #region Spedizione del messaggio

                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Spedizione Report", false));

                    oClient.Send(oMessage);

                    operazioniReport[operazioniReport.Count - 1].Stop();

                    #endregion

                    #region eventuale messaggio con allegati pdf

                    if (checkItemPDF)
                    {
                        operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Messaggi con allegati PDF", false));
                        int indexPdf = 0;
                        foreach (clsItemSequenzaAutoOperazioni itemSequenza in operazioni)
                        {
                            if (itemSequenza.Important && itemSequenza.Riepilogo != null && itemSequenza.contentFile != null)
                            {
                                if (itemSequenza.Riepilogo != null &&
                                    itemSequenza.contentFile != null &&
                                    itemSequenza.contentFile.Buffer != null &&
                                    itemSequenza.contentFile.Buffer.Length > 0 &&
                                    ((sendPDF_RPG && itemSequenza.Riepilogo.Tipo == "G") ||
                                     (sendPDF_RPM && itemSequenza.Riepilogo.Tipo == "M") ||
                                     (sendPDF_RCA && itemSequenza.Riepilogo.Tipo == "R")))
                                {
                                    indexPdf += 1;
                                    operazioniReport[operazioniReport.Count - 1].Messages.Add(string.Format(" Allegato PDF {0} di {1}", indexPdf.ToString(), countImportantPdf.ToString()));

                                    oMessage = new System.Net.Mail.MailMessage();
                                    oMessage.From = new System.Net.Mail.MailAddress(emailCodiceSistema);
                                    foreach (string cItem in (to + ";").Split(';'))
                                    {
                                        if (cItem.TrimEnd() != "")
                                        {
                                            oMessage.To.Add(cItem);
                                        }
                                    }
                                    foreach (string cItem in (destCC + ";").Split(';'))
                                    {
                                        if (cItem.TrimEnd() != "")
                                        {
                                            oMessage.CC.Add(cItem);
                                        }
                                    }
                                    foreach (string cItem in (destCCN + ";").Split(';'))
                                    {
                                        if (cItem.TrimEnd() != "")
                                        {
                                            oMessage.Bcc.Add(cItem);
                                        }
                                    }

                                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Costruzione Allegato PDF", false));

                                    oMessage.IsBodyHtml = false;
                                    oMessage.Subject = "RIEPILOGHI AUTOMATICI PDF " + GetDescrizioneRiepilogo(itemSequenza.Riepilogo);
                                    oMessage.Body = "";
                                    oMessage.Body += "\r\n" + itemSequenza.Description(false);
                                    MemoryStream oStream = new MemoryStream(Convert.FromBase64String(itemSequenza.contentFile.Buffer));
                                    System.Net.Mail.Attachment oAttachment = new System.Net.Mail.Attachment(oStream, itemSequenza.contentFile.FileName, itemSequenza.contentFile.ContentType);
                                    oMessage.Attachments.Add(oAttachment);

                                    operazioniReport.Add(new clsItemSequenzaAutoOperazioni("Spedizione con Allegato PDF", false));

                                    oClient.Send(oMessage);
                                    oAttachment.Dispose();
                                    oStream.Dispose();
                                }
                            }
                        }

                        operazioniReport[operazioniReport.Count - 1].Stop();

                    }

                    #endregion

                    oClient.Dispose();
                }
                catch (Exception EX)
                {
                    string g = "";
                }
            }
            return result;
        }

        private static string GetDescrDateTime(DateTime value)
        {
            return value.ToShortDateString() + " " + value.ToShortTimeString();
        }



        #endregion

        #region Help

        private static List<string> GetHelp()
        {
            List<string> result = new List<string>();
            result.Add("RIEPILOGHI AUTOMATICI  RiepiloghiAuto.exe");
            result.Add("");
            result.Add(" L'APPLICAZIONE NON RICHIEDE LA CONNESSIONE AD ORACLE");
            result.Add(" INSERIRLA NELLE ATTIVITA' PIANIFICATE DI WINDOWS SENZA ALCUN PARAMETRO");
            result.Add(" (Per es. al mattino per creazione e spedizione e più tardi per ricezione risposte)");
            result.Add("");
            result.Add("Attività di richiesta ai WebServices delle Funzioni di servizio (Riepiloghi)");
            result.Add(" - Generazione dei riepiloghi da generare:");
            result.Add("    RPG Giornalieri dei giorni precedenti da generare.");
            result.Add("    RCA Accessi dei giorni precedenti da generare.");
            result.Add("    RPM Mensili dei mesi precedenti da generare.");
            result.Add("    RPM del mese in corso che viene generato una volta al giorno per tutto il mese.");
            result.Add(" - Invio e Ricezione email.");
            result.Add(" - Creazione email con resoconto delle attività svolte per i destinatari configurati");
            result.Add("   utilizzando l'account email del Codice Sistema.");
            result.Add("");
            result.Add("Parametri applicazione:");
            result.Add(" HELP   visualizza queste informazioni.");
            result.Add(" MANUAL esegue la procedura manualmente visualizzando le operazioni in corso.");
            result.Add("");
            result.Add("File di configurazione: RiepiloghiAuto.exe.config");
            result.Add(" (non modificare MAI i valori contenuti nel campo key)");
            result.Add(@" URL da specificare nel campo value per le richieste ai WebServices delle Funzioni di servizio (Riepilogi)");
            result.Add(@"  <add key=""usrWebServicesRiepiloghi"" value=""https://riepiloghi.webtic.it/api/""/>");
            result.Add(@" Destinatari da specificare nel campo value delle email di resoconto (Opzionale value="""")");
            result.Add(@"  <add key=""sendEmailReportTO"" value=""destinatario1;destinatario2;...""/>");
            result.Add(@" Destinatari da specificare nel campo value in copia delle email di resoconto (Opzionale value="""")");
            result.Add(@"  <add key=""sendEmailReportCC"" value=""destinatario3;destinatario4;...""/>");
            result.Add(@" Destinatari da specificare nel campo value in copia nascosti delle email di resoconto (Opzionale value="""")");
            result.Add(@"  <add key=""sendEmailReportCCN"" value=""destinatario5;destinatario6;...""/>");
            result.Add(@" Indicare se inviare anche il PDF del Riepilogo Giornaliero creato impostando value=""true""");
            result.Add(@"  <add key=""sendPDF_RPG"" value=""true""/>");
            result.Add(@" Indicare se inviare anche il PDF del Riepilogo Mensile creato impostando  value=""true""");
            result.Add(@"  <add key=""sendPDF_RPM"" value=""true""/>");
            result.Add(@" Indicare se inviare anche il PDF del Riepilogo Controllo Accessi creato impostando value=""true""");
            result.Add(@"  <add key=""sendPDF_RCA"" value=""true""/>");
            result.Add("");
            result.Add("File delle ultime operazioni eseguite: RiepiloghiAuto.db.json");
            result.Add(" Contiene le ultime operazioni effettuate e gli ultimi item inviati ai destinatari.");
            result.Add(" Se l'applicazione viene eseguita con il medesimo risultato della precedente esecuzione");
            result.Add(" non viene creato un nuovo resoconto.");
            result.Add(" Il file può essere cancellato, se manipolato manualmente non viene preso in considerazione");
            result.Add(" e comunque viene rigenerato ad ogni esecuzione.");
            result.Add("");
            return result;
        }

        #endregion

    }
}
