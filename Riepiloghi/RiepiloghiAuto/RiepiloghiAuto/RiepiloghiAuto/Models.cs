﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiepiloghiAuto
{
    public class PayLoadRiepiloghi
    {
        public string Mode { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        //public ApiRiepiloghi.ServiceRiepiloghi.clsTornelloLogin Tornello_Login { get; set; }
        public string Tornello_Barcode { get; set; }
        public string AccountId { get; set; }

        public string CodiceSistema { get; set; }

        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }

        public string Carta { get; set; }
        public string Sigillo { get; set; }
        public Int64 Progressivo { get; set; }

        public Int64 PrimoProgressivo { get; set; }
        public Int64 UltimoProgressivo { get; set; }

        public string CausaleAnnullamento { get; set; }

        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }

        public DateTime DataRiepilogo { get; set; }
        public string TipoRiepilogo { get; set; }
        public long ProgressivoRiepilogo { get; set; }

        public string CambioStatoLta { get; set; }
        public string Proprieta { get; set; }

    }

    public class clsCodiceLocale
    {
        public string CodiceSistema = "";
        public string CodiceLocale = "";
        public string Descrizione = "";
        public bool Enabled = false;
    }

    public class clsCFOrganizzatore
    {
        public string CodiceSistema = "";
        public string CFOrganizzatore = "";
        public string Descrizione = "";
        public bool Enabled = false;
    }

    public class clsCodiceSistema
    {
        // sdfsdf
        public string CodiceSistema = "";
        public string Descrizione = "";
        public bool Enabled = false;
        public string ConnectionString = "";
        public List<clsCodiceLocale> CodiciLocale = new List<clsCodiceLocale>();
        public List<clsCFOrganizzatore> CFOrganizzatori = new List<clsCFOrganizzatore>();
        public bool AllCodiciLocali = false;
        public bool AllCFOrganizzatori = false;
    }

    public class clsOperazioneRiepiloghi
    {
        public Int64 Ordine = 0;
        public string IdOperazione = "";
        public string Descrizione = "";
        public Int64 IdCategoria = 0;
        public Int64 OrdineCategoria = 0;
        public string DescrizioneCategoria = "";
        public bool Enabled = false;
    }

    public class clsCategoriaOperazioniRiepiloghi
    {
        public Int64 IdCategoria = 0;
        public Int64 Ordine = 0;
        public string Descrizione = "";
        public List<clsOperazioneRiepiloghi> ListaOperazioni = new List<clsOperazioneRiepiloghi>();
    }

    public class clsAccount
    {
        public string AccountId = "";
        public string Nome = "";
        public string Cognome = "";
        public string Email = "";
        public bool Enabled = false;
        public List<clsCodiceSistema> CodiciSistema = new List<clsCodiceSistema>();
        public List<clsCategoriaOperazioniRiepiloghi> Operazioni = new List<clsCategoriaOperazioniRiepiloghi>();
    }

    public class clsRiepilogoGenerato
    {
        public long IdOperazione { get; set; }
        public DateTime Giorno { get; set; }
        public string Tipo { get; set; }
        public string Nome { get; set; }
        public long Progressivo { get; set; }
        public DateTime? DataOraGenerazione { get; set; }
        public DateTime? EmailSend_Last { get; set; }
        public string EmailSend_Error { get; set; }
        public DateTime? EmailSend_Sent { get; set; }
        [JsonIgnore]
        public DateTime? EmailRead_Last { get; set; }
        public string EmailRead_Error { get; set; }
        public DateTime? EmailReturn_Date { get; set; }
        public long EmailReturn_Code { get; set; }
        public string EmailReturn_Body { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class clsRiepilogoGeneratoWebApi : clsRiepilogoGenerato
    {

    }
    public class ResultContentFile
    {
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string Buffer { get; set; }
    }

    public class ResponseRiepiloghi
    {
        public ResponseDataRiepiloghi Data { get; set; }
        public ResultBase Status { get; set; }
    }
    public class ResponseDataRiepiloghi
    {
        public string Token { get; set; }

        // GetAccount
        public clsAccount Account { get; set; }

        // GetCodiceSistema
        //public Riepiloghi.clsCodiceSistema CodiceSistema { get; set; }

        // GetListaCampiLogTransazioniCS
        //public clsResultBaseCampoLogTransazioni[] Campi { get; set; }
        //public clsResultBaseCampoLogTransazioni[] CampiSmart { get; set; }
        //public Riepiloghi.clsFiltroCampoTransazione[] Filtri { get; set; }
        //public clsResultBaseCampoLta[] CampiLta { get; set; }
        //public clsResultBaseCampoLta[] CampiLtaSmart { get; set; }

        //public Riepiloghi.clsCodiceLocale[] CodiciLocali { get; set; }

        //// GetTransazioni, GetTransazioniAnnullati
        //public DateTime DataEmiInizio { get; set; }
        //public DateTime DataEmiFine { get; set; }
        //public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione[] Transazioni { get; set; }
        //public ApiRiepiloghi.ServiceRiepiloghi.clsResultLta[] Lta { get; set; }

        //public string Carta { get; set; }
        //public string Sigillo { get; set; }
        //public Int64 Progressivo { get; set; }

        //public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione Transazione { get; set; }

        //public List<ApiRiepiloghi.ServiceRiepiloghi.clsWSCheckAnnullo> CheckTransazioni { get; set; }

        //public string CartaAnnullante { get; set; }
        //public string SigilloAnnullante { get; set; }
        //public Int64 ProgressivoAnnullante { get; set; }
        //public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione TransazioneAnnullante { get; set; }

        //public List<string> WarningsPostAnnullo { get; set; }

        public List<clsRiepilogoGeneratoWebApi> ListaRiepiloghi { get; set; }
        public clsRiepilogoGeneratoWebApi RichiestaRiepilogo { get; set; }


        //// RIEPILOGHI DA GENERARE

        //// RIEPILOGHI SU CUI RICEVERE RISPOSTA o DA INVIARE
        public bool EmailToSendOrReceive { get; set; }


        public ResultContentFile contentFile { get; set; }

        //public string Tornello_Barcode { get; set; }
        //public ApiRiepiloghi.ServiceRiepiloghi.clsTornelloLogin Tornello_Login { get; set; }


        //public ApiRiepiloghi.ServiceRiepiloghi.clsResultLta Tornello_Lta { get; set; }

        public string Proprieta { get; set; }
        public string Valore { get; set; }

    }

    public class ResultBase
    {
        public bool StatusOK = true;

        public string StatusMessage = "";

        public Int64 StatusCode = 0;

        public string execution_time = "";

        public DateTime Sysdate { get; set; }

        public ResultBase()
        {
        }

        public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
        {
            this.StatusOK = lStatusOK;
            this.StatusMessage = cStatusMessage;
            this.StatusCode = nStatusCode;
            this.execution_time = cExecutionTime;
        }
    }

    public class clsResultBaseCampoLogTransazioni
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLogTransazioni()
        {
        }

        public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

    public class clsResultBaseCampoLta
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLta()
        {
        }

        public clsResultBaseCampoLta(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

    public class clsMessages : List<string>
    {
        public delegate void delegateAdd(string message);
        public event delegateAdd EventAdd;

        public new void Add(string message)
        {
            base.Add(message);
            if (this.EventAdd != null) this.EventAdd(message);
        }
    }

    public class clsItemSequenzaAutoOperazioni
    {
        public delegate void delegateItem(clsItemSequenzaAutoOperazioni item);

        public event delegateItem EventStop;
        public event clsMessages.delegateAdd EventAddMessage;

        public string Operation { get; set; }
        public string StartDateDescription
        {
            get
            {
                return GetDateTimeDescription(startDateUTC);
            }
        }

        public string EndDateDescription
        {
            get
            {
                return GetDateTimeDescription(endDateUTC);
            }
        }

        public string ExecutionTime
        {
            get
            {
                string watchDescr = "";
                if (this.Watch.Elapsed.Hours > 0) watchDescr = this.Watch.Elapsed.Hours.ToString() + "h:";
                if (this.Watch.Elapsed.Minutes > 0) watchDescr = this.Watch.Elapsed.Minutes.ToString("00") + "':";
                if (this.Watch.Elapsed.Seconds > 0) watchDescr = this.Watch.Elapsed.Seconds.ToString("00") + "'':";
                if (this.Watch.Elapsed.Milliseconds > 0) watchDescr = this.Watch.Elapsed.Milliseconds.ToString() + "ms";
                return watchDescr;
            }
        }
        public DateTime startDateUTC { get; set; }
        public DateTime endDateUTC { get; set; }

        [JsonIgnore]
        public System.Diagnostics.Stopwatch Watch { get; set; }

        public clsMessages Messages { get; set; }

        public string WebServiceExecutionTime { get; set; }

        public Exception error { get; set; }

        public bool Important { get; set; }

        public clsRiepilogoGeneratoWebApi Riepilogo { get; set; }
        public ResultContentFile contentFile { get; set; }

        public string TypeItem { get; set; }

        public clsItemSequenzaAutoOperazioni(string operation, bool important)
        {
            this.startDateUTC = DateTime.Now;
            this.Watch = new System.Diagnostics.Stopwatch();
            this.Watch.Start();
            this.Operation = operation;
            this.Messages = new clsMessages();
            this.Messages.EventAdd += Messages_EventAdd;
            this.WebServiceExecutionTime = "";
            this.Important = important;
            this.TypeItem = "";
        }

        private void Messages_EventAdd(string message)
        {
            if (this.EventAddMessage != null) this.EventAddMessage(message);
        }

        private string GetDateTimeDescription(DateTime dateTime)
        {
            string result = dateTime.Day.ToString("00") + "-" +
                            dateTime.Month.ToString("00") + "-" +
                            dateTime.Year.ToString("0000") + " " +
                            dateTime.Hour.ToString("00") + ":" +
                            dateTime.Minute.ToString("00") + ":" +
                            dateTime.Second.ToString("00") + ":" +
                            dateTime.Millisecond.ToString();

            return result;
        }

        public void Stop()
        {
            this.Watch.Stop();
            this.endDateUTC = DateTime.Now;
            if (this.EventStop != null) this.EventStop(this);
        }

        public void Stop(Exception error)
        {
            this.Watch.Stop();
            this.endDateUTC = DateTime.Now;
            this.error = error;
            if (this.EventStop != null) this.EventStop(this);
        }

        public string Description(bool addMesssages)
        {
            string watchDescr = ExecutionTime;
            string result = "";

            if (this.Messages.Count > 0 && addMesssages)
            {
                foreach (string itemMessage in this.Messages)
                {
                    result += (result.Trim() == "" ? "" : "\r\n") + itemMessage;
                }
            }


            if (error != null)
            {
                result += (result.Trim() == "" ? "" : "\r\n") + "Operazione: " + this.startDateUTC.ToShortDateString() + " " + this.startDateUTC.ToLongTimeString() + " " + "Fine: " + this.endDateUTC.ToLongTimeString();
                if (this.WebServiceExecutionTime.Trim() != "")
                    result += (result.Trim() == "" ? "" : "\r\n") + "Risposta dal server: " + this.WebServiceExecutionTime;
                if (watchDescr.Trim() != "")
                    result += (result.Trim() == "" ? "" : "\r\n") + "Tempo operazione: " + watchDescr;
            }

            return result;
        }

    }

    public class clsSequenzaOperazioni : List<clsItemSequenzaAutoOperazioni>
    {
        public delegate void delegateEventAdd(clsItemSequenzaAutoOperazioni item);
        public event delegateEventAdd EventAdd;

        public event clsItemSequenzaAutoOperazioni.delegateItem EventStop;
        public event clsMessages.delegateAdd EventAddMessage;

        public new void Add(clsItemSequenzaAutoOperazioni itemSequenzaAutoOperazioni)
        {
            base.Add(itemSequenzaAutoOperazioni);
            itemSequenzaAutoOperazioni.EventStop += ItemSequenzaAutoOperazioni_EventStop;
            itemSequenzaAutoOperazioni.EventAddMessage += ItemSequenzaAutoOperazioni_EventAddMessage;
            if (EventAdd != null) EventAdd(itemSequenzaAutoOperazioni);
        }

        private void ItemSequenzaAutoOperazioni_EventAddMessage(string message)
        {
            if (this.EventAddMessage != null) this.EventAddMessage(message);
        }

        private void ItemSequenzaAutoOperazioni_EventStop(clsItemSequenzaAutoOperazioni item)
        {
            if (this.EventStop != null) this.EventStop(item);
        }

    }

    public class clsLocaleApplicationDatabase
    {
        public string StartExecution
        {
            get
            {
                string result = GetDateTimeDescription(this.startExecutionUTC);
                
                return result;
            }
        }
        public string EndExecution
        {
            get
            {
                string result = GetDateTimeDescription(this.endExecutionUTC);

                return result;
            }
        }
        public string TotalExecutionTime
        {
            get
            {
                string watchDescr = "";
                if (this.watch.Elapsed.Hours > 0) watchDescr += this.watch.Elapsed.Hours.ToString("00") + " h:";
                if (this.watch.Elapsed.Hours + this.watch.Elapsed.Minutes > 0) watchDescr += this.watch.Elapsed.Minutes.ToString("00") + " min:";
                if (this.watch.Elapsed.Hours + this.watch.Elapsed.Minutes + this.watch.Elapsed.Seconds > 0) watchDescr += this.watch.Elapsed.Seconds.ToString("00") + " sec:";
                if (this.watch.Elapsed.Hours + this.watch.Elapsed.Minutes + this.watch.Elapsed.Seconds + this.watch.Elapsed.Milliseconds > 0) watchDescr += this.watch.Elapsed.Milliseconds.ToString() + " ms";
                return watchDescr;
            }
        }
        public DateTime startExecutionUTC { get; set; }
        public DateTime endExecutionUTC { get; set; }
        public clsSequenzaOperazioni operazioni { get; set; }
        public clsSequenzaOperazioni operazioniReport { get; set; }
        public List<clsRiepilogoGeneratoWebApi> itemsEmailLastSendReceive { get; set; }

        [JsonIgnore]
        public System.Diagnostics.Stopwatch watch { get; set; }

        public clsLocaleApplicationDatabase()
        {
            this.watch = new System.Diagnostics.Stopwatch();
            this.endExecutionUTC = DateTime.MinValue;
            this.itemsEmailLastSendReceive = new List<clsRiepilogoGeneratoWebApi>();
            this.operazioni = new clsSequenzaOperazioni();
            this.operazioniReport = new clsSequenzaOperazioni();
        }
        private string GetDateTimeDescription(DateTime dateTime)
        {
            string result = dateTime.Day.ToString("00") + "-" +
                            dateTime.Month.ToString("00") + "-" +
                            dateTime.Year.ToString("0000") + " " +
                            dateTime.Hour.ToString("00") + ":" +
                            dateTime.Minute.ToString("00") + ":" +
                            dateTime.Second.ToString("00") + ":" +
                            dateTime.Millisecond.ToString();

            return result;
        }
    }

    public class clsItemTemplate
    {
        public string key { get; set; }
        public List<string> lines { get; set; }

        public List<clsItemTemplate> subItems { get; set; }
        public clsItemTemplate()
        {
            this.key = "";
            this.lines = new List<string>();
            this.subItems = new List<clsItemTemplate>();
        }
        public clsItemTemplate(string Key)
            : this()
        {
            this.key = Key;
        }

        public clsItemTemplate(clsItemTemplate item)
            : this()
        {
            this.key = item.key;
            this.lines = new List<string>(item.lines.ToArray());
        }

        public void SetTemplate(Dictionary<string, string> values)
        {
            List<string> newLines = new List<string>();
            foreach (string line in this.lines)
            {
                string lineTemplate = line;
                foreach (KeyValuePair<string, string> param in values)
                {
                    lineTemplate = lineTemplate.Replace(param.Key, param.Value.Replace("\r\n", "<br>"));
                }
                newLines.Add(lineTemplate);
            }
            this.lines = newLines;
        }

        public static Dictionary<string, clsItemTemplate> GetTemplateModels(string content)
        {
            Dictionary<string, clsItemTemplate> result = new Dictionary<string, clsItemTemplate>();
            clsItemTemplate currentItem = null;
            Stack<clsItemTemplate> stackItems = new Stack<clsItemTemplate>();
            foreach (string item_line in content.Split('\r'))
            {
                string line = item_line.Replace("\n", "");
                if (line.StartsWith("{"))
                {
                    if (!result.ContainsKey(line))
                    {
                        if (currentItem != null) stackItems.Push(currentItem);
                        currentItem = new clsItemTemplate(line);
                        result.Add(currentItem.key, currentItem);
                    }
                    else
                    {
                        if (stackItems.Count > 0)
                            currentItem = stackItems.Pop();
                    }
                }
                else
                {
                    currentItem.lines.Add(line);
                }
            }
            return result;
        }


        public string ReplaceSubItems(clsItemTemplate item = null)
        {
            string result = "";
            bool isRoot = (item == null);
            clsItemTemplate currentItem = (isRoot ? this : item);

            if (currentItem.subItems != null && currentItem.subItems.Count > 0)
            {
                string rowsSubItems = "";
                foreach (clsItemTemplate subItem in currentItem.subItems)
                {
                    rowsSubItems += ReplaceSubItems(subItem);
                }
                List<string> newLines = new List<string>();
                foreach (string line in currentItem.lines)
                {
                    if (line == ":{subitems}")
                        newLines.Add(line.Replace(":{subitems}", rowsSubItems));
                    else newLines.Add(line);
                }
                currentItem.lines = newLines;
            }
            foreach (string line in currentItem.lines)
            {
                result += line + "\r\n";
            }
            return result;
        }


    }



}
