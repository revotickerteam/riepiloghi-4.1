﻿
namespace libLogToolJDK41
{
    partial class frmCheckContatori
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlParameters = new System.Windows.Forms.Panel();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lblPath = new libLogToolJDK41.clsLabel();
            this.btnFolderDialog = new libLogToolJDK41.clsButtonBase();
            this.chkCheckDB = new libLogToolJDK41.clsCheckBox();
            this.btnStart = new libLogToolJDK41.clsButtonBase();
            this.pnlData = new System.Windows.Forms.Panel();
            this.txtData = new System.Windows.Forms.TextBox();
            this.pnlParameters.SuspendLayout();
            this.pnlData.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlParameters
            // 
            this.pnlParameters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlParameters.Controls.Add(this.txtPath);
            this.pnlParameters.Controls.Add(this.lblPath);
            this.pnlParameters.Controls.Add(this.btnFolderDialog);
            this.pnlParameters.Controls.Add(this.chkCheckDB);
            this.pnlParameters.Controls.Add(this.btnStart);
            this.pnlParameters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlParameters.Location = new System.Drawing.Point(0, 0);
            this.pnlParameters.Name = "pnlParameters";
            this.pnlParameters.Padding = new System.Windows.Forms.Padding(3);
            this.pnlParameters.Size = new System.Drawing.Size(899, 74);
            this.pnlParameters.TabIndex = 0;
            // 
            // txtPath
            // 
            this.txtPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPath.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPath.Location = new System.Drawing.Point(107, 3);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(613, 37);
            this.txtPath.TabIndex = 1;
            this.txtPath.Text = "C:\\RIEPILOGHI";
            // 
            // lblPath
            // 
            this.lblPath.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.lblPath.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPath.Icon = null;
            this.lblPath.ImageIcon = null;
            this.lblPath.Location = new System.Drawing.Point(3, 3);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(104, 38);
            this.lblPath.TabIndex = 0;
            this.lblPath.Text = "Percorso:";
            this.lblPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPath.VerticalText = "";
            // 
            // btnFolderDialog
            // 
            this.btnFolderDialog.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnFolderDialog.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFolderDialog.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolderDialog.Location = new System.Drawing.Point(720, 3);
            this.btnFolderDialog.Name = "btnFolderDialog";
            this.btnFolderDialog.Size = new System.Drawing.Size(51, 38);
            this.btnFolderDialog.TabIndex = 3;
            this.btnFolderDialog.Text = "...";
            this.btnFolderDialog.UseVisualStyleBackColor = true;
            // 
            // chkCheckDB
            // 
            this.chkCheckDB.AutoSize = true;
            this.chkCheckDB.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chkCheckDB.Location = new System.Drawing.Point(3, 41);
            this.chkCheckDB.Name = "chkCheckDB";
            this.chkCheckDB.Size = new System.Drawing.Size(768, 28);
            this.chkCheckDB.TabIndex = 4;
            this.chkCheckDB.Text = "controllo anche nel db";
            this.chkCheckDB.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnStart.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnStart.Location = new System.Drawing.Point(771, 3);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(123, 66);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "avvia";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // pnlData
            // 
            this.pnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlData.Controls.Add(this.txtData);
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 74);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(899, 554);
            this.pnlData.TabIndex = 1;
            // 
            // txtData
            // 
            this.txtData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtData.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(0, 0);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtData.Size = new System.Drawing.Size(897, 552);
            this.txtData.TabIndex = 0;
            // 
            // frmCheckContatori
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(899, 628);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlParameters);
            this.Name = "frmCheckContatori";
            this.Text = "Controllo contatori file di LOG";
            this.pnlParameters.ResumeLayout(false);
            this.pnlParameters.PerformLayout();
            this.pnlData.ResumeLayout(false);
            this.pnlData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlParameters;
        private System.Windows.Forms.TextBox txtPath;
        private clsButtonBase btnStart;
        private clsLabel lblPath;
        private clsButtonBase btnFolderDialog;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.TextBox txtData;
        private clsCheckBox chkCheckDB;
    }
}