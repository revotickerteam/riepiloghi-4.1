﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace libLogToolJDK41
{
    public partial class frmLogToolMenu : libLogToolJDK41.frmLogToolBase
    {
        public frmLogToolMenu()
        {
            InitializeComponent();
            this.btnChiudi.Click += BtnChiudi_Click;
            this.btnCheckContatori.Click += BtnCheckContatori_Click;
        }

        private void BtnCheckContatori_Click(object sender, EventArgs e)
        {
            frmCheckContatori formCheckContatori = new frmCheckContatori();
            formCheckContatori.ShowDialog();
            formCheckContatori.Close();
            formCheckContatori.Dispose();
        }

        private void BtnChiudi_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
