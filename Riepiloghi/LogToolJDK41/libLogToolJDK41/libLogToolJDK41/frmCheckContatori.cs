﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace libLogToolJDK41
{
    public partial class frmCheckContatori : frmLogToolBase
    {
        public frmCheckContatori()
            :base()
        {
            InitializeComponent();
            this.btnFolderDialog.Click += BtnFolderDialog_Click;
            this.btnStart.Click += BtnStart_Click;
        }

        

        private void BtnFolderDialog_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.Description = "Selezionare il percorso dei files dei riepiloghi";
            fd.ShowNewFolderButton = false;
            if (fd.ShowDialog() == DialogResult.OK && fd.SelectedPath != "" && System.IO.Directory.Exists(fd.SelectedPath))
            {
                this.txtPath.Text = fd.SelectedPath;
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            this.pnlParameters.Enabled = false;
            try
            {
                this.Elabora();
            }
            catch (Exception)
            {
            }
            this.pnlParameters.Enabled = true;
        }

        private void Elabora()
        {
            this.ResetTextData();
            List<clsFile> result = null;
            Exception error = null;
            clsFile.OutMessage += ClsFile_OutMessage;
            if (clsFile.GetFiles(this.txtPath.Text, ref result, out error))
            {
                this.AddTextData(string.Format("Trovati {0} file totali.", result.Count.ToString()));
                Dictionary<string, clsCartaCheckContatori> carte = clsFile.CheckTransazioniContatori(result);
                foreach (KeyValuePair<string, clsCartaCheckContatori> itemCarta in carte)
                {
                    clsCartaCheckContatori carta = itemCarta.Value;
                    string descCarta = string.Format("CARTA: {0} primo: {1}  ultimo: {2}  contatori validi: {3}", carta.Carta, carta.ContatoreMin.ToString(), carta.ContatoreMax, carta.NumeroContatoriNonValido.ToString());
                    this.AddTextData(descCarta);
                    if (carta.ContatoriMancanti != null && carta.ContatoriMancanti.Count > 0)
                    {
                        carta.ContatoriMancanti.ForEach(c => this.AddTextData(string.Format("mancante da: {0}  a {1}", c.Key.ToString(), c.Value.ToString())));
                    }
                    if (carta.Transazioni != null && carta.Transazioni.Count > 0)
                    {
                        carta.Transazioni.ForEach(t =>
                        {
                            if (t.Errori != null && t.Errori.Count > 0)
                            {
                                this.AddTextData("Errori:");
                                t.Errori.ForEach(e => this.AddTextData(" " + e.Message));
                            }
                        });
                    }

                }
            }
            else
                this.AddTextData("Nessun file.");

            if (this.chkCheckDB.Checked)
            {
                this.AddTextData("Connessione...", true);
                IConnection conn = new Wintic.Data.oracle.CConnectionOracle();
                try
                {
                    conn.ConnectionString = "USER=WTIC;PASSWORD=OBELIX;DATA SOURCE=SERVER-2019;";
                    conn.Open();
                    StringBuilder oSB;
                    DataTable oTable;
                    clsParameters oPars;

                    string codice_sistema = "";
                    oSB = new StringBuilder();
                    oSB.Append("SELECT CODICE_SISTEMA FROM WTIC.SMART_CS");
                    oTable = conn.ExecuteQuery(oSB.ToString());
                    if (oTable != null && oTable.Rows != null && oTable.Rows.Count > 0)
                        codice_sistema = oTable.Rows[0]["CODICE_SISTEMA"].ToString();
                    oTable.Dispose();
                    this.AddTextData("Codice sistema [" + codice_sistema + "]", true);

                    oSB = new StringBuilder();
                    oSB.Append(" SELECT");
                    oSB.Append(" DATA_EMI, CODICE_SISTEMA, CODICE_CARTA, PROGRESSIVO_TITOLO, CONTATORE_MIN, CONTATORE_MAX, CONTATORI, CONTATORE_PREC");
                    oSB.Append(" FROM");
                    oSB.Append(" (SELECT DATA_EMISSIONE_ANNULLAMENTO AS DATA_EMI, CODICE_SISTEMA, CODICE_CARTA, PROGRESSIVO_TITOLO");
                    oSB.Append(" , ROW_NUMBER() OVER (PARTITION BY CODICE_SISTEMA, CODICE_CARTA ORDER BY CODICE_SISTEMA, CODICE_CARTA, PROGRESSIVO_TITOLO) AS RIGA ");
                    oSB.Append(" , MIN(PROGRESSIVO_TITOLO) OVER (PARTITION BY CODICE_SISTEMA, CODICE_CARTA) AS CONTATORE_MIN ");
                    oSB.Append(" , MAX(PROGRESSIVO_TITOLO) OVER (PARTITION BY CODICE_SISTEMA, CODICE_CARTA) AS CONTATORE_MAX");
                    oSB.Append(" , COUNT(PROGRESSIVO_TITOLO) OVER (PARTITION BY CODICE_SISTEMA, CODICE_CARTA) AS CONTATORI");
                    oSB.Append(" , NVL(LAG(PROGRESSIVO_TITOLO) OVER (PARTITION BY CODICE_SISTEMA, CODICE_CARTA ORDER BY CODICE_SISTEMA, CODICE_CARTA, PROGRESSIVO_TITOLO), 0) AS CONTATORE_PREC");
                    oSB.Append(" FROM LOG_TRANSAZIONI");
                    oSB.Append(" WHERE");
                    oSB.Append(" CODICE_SISTEMA = :pCODICE_SISTEMA");
                    oSB.Append(" )");
                    oSB.Append(" WHERE CONTATORE_PREC <> PROGRESSIVO_TITOLO - 1");
                    oSB.Append(" OR (RIGA = 1 AND CONTATORE_MIN > 1)");
                    oSB.Append(" ORDER BY");
                    oSB.Append(" CODICE_CARTA");
                    oSB.Append(" ,PROGRESSIVO_TITOLO");

                    oPars = new clsParameters();
                    oPars.Add(":pCODICE_SISTEMA", codice_sistema);
                    oTable = conn.ExecuteQuery(oSB.ToString(), oPars);
                    List<int> sizes = null;
                    if (oTable != null && oTable.Rows != null && oTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in oTable.Rows)
                        {
                            this.AddTextData(clsUtility.ExtractRowDataToString(row, true, ref sizes));
                            break;
                        }
                        foreach (DataRow row in oTable.Rows)
                            this.AddTextData(clsUtility.ExtractRowDataToString(row, false, ref sizes));
                    }
                    oTable.Dispose();
                }
                catch (Exception ex)
                {
                    this.AddTextData(ex.Message, true);
                }
                conn.Close();
                conn.Dispose();
            }

        }

        private void ClsFile_OutMessage(string value, bool append, Exception error = null)
        {
            this.AddTextData(value, append);
            if (error != null) this.AddTextData(error.Message, true);
        }

        private delegate void delegateAddTextData(string value, bool append);
        private void AddTextData(string value, bool append = true)
        {
            if (this.InvokeRequired)
            {
                delegateAddTextData d = new delegateAddTextData(AddTextData);
                this.Invoke(d, value);
            }
            else
            {
                if (append)
                    this.txtData.Text += "\r\n" + value;
                else
                {
                    if (this.txtData.Text.Trim() == "")
                        this.txtData.Text = value;
                    else if (!this.txtData.Text.Contains("\r\n"))
                        this.txtData.Text = value;
                    else
                    {
                        List<string> lines = this.txtData.Text.Split(new char[] { '\r', '\n' }).ToList<string>();
                        lines[lines.Count - 1] = value;
                        string result = "";
                        lines.ForEach(l => result += (l.Replace('\r', ' ').Replace('\n', ' ').Trim() != "" ? "\r\n" + l.Replace('\r',' ').Replace('\n',' ') : ""));
                        this.txtData.Text = result;
                    }
                }
                this.txtData.SelectionLength = 0;
                this.txtData.SelectionStart = this.txtData.Text.Length;
                this.txtData.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void ResetTextData()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(ResetTextData));
            else
            {
                this.txtData.Text = "";
                Application.DoEvents();
            }
                
        }
    }
}
