﻿
namespace libLogToolJDK41
{
    partial class frmLogToolMenu
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheckContatori = new libLogToolJDK41.clsButtonBase();
            this.btnChiudi = new libLogToolJDK41.clsButtonBase();
            this.SuspendLayout();
            // 
            // btnCheckContatori
            // 
            this.btnCheckContatori.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnCheckContatori.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCheckContatori.Location = new System.Drawing.Point(0, 0);
            this.btnCheckContatori.Name = "btnCheckContatori";
            this.btnCheckContatori.Size = new System.Drawing.Size(454, 73);
            this.btnCheckContatori.TabIndex = 0;
            this.btnCheckContatori.Text = "Controllo contatori";
            this.btnCheckContatori.UseVisualStyleBackColor = true;
            // 
            // btnChiudi
            // 
            this.btnChiudi.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.btnChiudi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChiudi.Location = new System.Drawing.Point(0, 73);
            this.btnChiudi.Name = "btnChiudi";
            this.btnChiudi.Size = new System.Drawing.Size(454, 69);
            this.btnChiudi.TabIndex = 1;
            this.btnChiudi.Text = "Chiudi";
            this.btnChiudi.UseVisualStyleBackColor = true;
            // 
            // frmLogToolMenu
            // 
            this.ClientSize = new System.Drawing.Size(454, 142);
            this.Controls.Add(this.btnChiudi);
            this.Controls.Add(this.btnCheckContatori);
            this.Name = "frmLogToolMenu";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private clsButtonBase btnCheckContatori;
        private clsButtonBase btnChiudi;
    }
}
