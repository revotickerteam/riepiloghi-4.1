﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using Wintic.Data;

namespace libLogToolJDK41
{
    public delegate void clsFileDelegateOutMessage(string value, bool append, Exception error = null);

    public class clsFile
    {
        public static event clsFileDelegateOutMessage OutMessage;

        private static void RaiseOutMessage(string value, bool append, Exception error = null)
        {
            if (OutMessage != null) OutMessage(value, append, error);
        }
        public string FullFileName { get; set; }
        public string FileName { get; set; }
        public string FolderName { get; set; }
        public string Tipo { get; set; }
        public DateTime? Giorno { get; set; }
        public int Prog { get; set; }
        public bool IsEmail => FileName != null && FileName.Trim().ToUpper().EndsWith(".EML");
        public bool IsSent { get; set; }
        public Exception Error { get; set; }
        public bool Exists { get; set; }
        
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdate { get; set; }

        public string Content { get; set; }
        public bool ValidContent { get; set; }
        public List<Dictionary<string, string>> Data { get; set; }

        public bool ExistsProgRow { get; set; }
        public bool ExistsFileRow { get; set; }

        public clsFile(string fullFileName)
        {
            this.FullFileName = fullFileName;
            FileInfo oFileInfo = new FileInfo(this.FullFileName);
            this.FolderName = oFileInfo.Directory.Name;
            this.Exists = oFileInfo.Exists;
            if (this.Exists)
            {
                this.CreationDate = oFileInfo.CreationTime;
                this.LastUpdate = oFileInfo.LastWriteTime;
            }
            this.FileName = oFileInfo.Name;
            string tipo = "";
            DateTime? giorno;
            int prog = 0;
            Exception error = null;
            if (ExtractInfoFromFileName(FileName, out tipo, out giorno, out prog, out error))
            {
                this.Tipo = tipo;
                this.Giorno = giorno;
                this.Prog = prog;
                if (this.IsEmail)
                    this.IsSent = oFileInfo.Directory.Name == "Spedite";
            }
            else
                this.Error = error;

            if (Exists && this.Error == null)
            {
                string content = "";
                bool validContent = false;
                List<Dictionary<string, string>> data = null;
                ExtractContentFile(oFileInfo, tipo, out content, out validContent, out data, out error);
                this.Error = error;
                this.Content = content;
                this.ValidContent = validContent;
                this.Data = data;
            }
        }

        public static bool ExtractInfoFromFileName(string fileName, out string tipo, out DateTime? giorno, out int prog, out Exception error)
        {
            //RaiseOutMessage("Info file");
            tipo = "";
            giorno = null;
            prog = 0;
            error = null;

            int year = 0;
            int month = 0;
            int day = 0;

            if (fileName == null || string.IsNullOrEmpty(fileName) || string.IsNullOrWhiteSpace(fileName))
                error = new Exception("Nome file vuoto");

            if (error == null)
            {
                Regex regexLOG = new Regex(@"LOG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
                Regex regexLTA = new Regex(@"LTA_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
                Regex regexRPG = new Regex(@"RPG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
                Regex regexRCA = new Regex(@"RCA_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
                Regex regexRPM = new Regex(@"RPM_\d{4}_\d{2}_00_\d{3}.xsi.p7m");
                if (regexLOG.IsMatch(fileName))
                    tipo = "LOG";
                else if (regexLTA.IsMatch(fileName))
                    tipo = "LTA";
                else if (regexRPG.IsMatch(fileName))
                    tipo = "RPG";
                else if (regexRCA.IsMatch(fileName))
                    tipo = "RCA";
                else if (regexRPM.IsMatch(fileName))
                    tipo = "RPM";
                

                if (string.IsNullOrWhiteSpace(tipo))
                {
                    Regex regexRPG_EML = new Regex(@"RPG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.eml");
                    Regex regexRCA_EML = new Regex(@"RCA_\d{4}_\d{2}_\d{2}_\d{3}.xsi.eml");
                    Regex regexRPM_EML = new Regex(@"RPM_\d{4}_\d{2}_00_\d{3}.xsi.eml");
                    if (regexRPG_EML.IsMatch(fileName))
                        tipo = "RPG";
                    else if (regexRCA_EML.IsMatch(fileName))
                        tipo = "RCA";
                    else if (regexRPM_EML.IsMatch(fileName))
                        tipo = "RPM";
                }

                if (string.IsNullOrWhiteSpace(tipo))
                    error = new Exception("Nome file non riconosciuto");

                if (error == null)
                {
                    //RaiseOutMessage(string.Format("Tipo {0}", tipo));
                    // 0 2 4 6 8 0 2 4 6 8 0 2 4
                    //  1 3 5 7 9 1 3 5 7 9 1 3 5
                    // LOG_2021_08_03_001.xsi.p7m
                    // LTA_2021_06_29_001.xsi.p7m
                    // RPG_2021_08_03_001.xsi.p7m
                    // RCA_2021_06_29_001.xsi.p7m
                    // RPM_2021_06_00_001.xsi.p7m

                    if (!int.TryParse(fileName.Substring(4, 4), out year))
                        error = new Exception("Anno non valido");
                    else if (!int.TryParse(fileName.Substring(9, 2), out month))
                        error = new Exception("Mese non valido");
                    else if (!int.TryParse(fileName.Substring(12, 2), out day))
                        error = new Exception("Giorno non valido");
                    else
                    {
                        try
                        {
                            giorno = (tipo == "RPM" ? new DateTime(year, month, 1) : new DateTime(year, month, day));
                            //RaiseOutMessage(string.Format("giorno {0}", giorno.Value.ToLongDateString()));
                        }
                        catch (Exception ex)
                        {
                            error = ex;
                        }
                        
                    }

                    if (error == null)
                    {
                        if (!int.TryParse(fileName.Substring(15, 3), out prog))
                            error = new Exception("Progressivo non valido");
                    }

                }
            }
            if (error != null)
                RaiseOutMessage("Info File", true, error);
            return error == null;
        }
    
        
        public static bool ExtractContentFile(FileInfo fileInfo, string tipo, out string content, out bool validContent, out List<Dictionary<string, string>> data, out Exception error)
        {
            //RaiseOutMessage("Contenuto File");
            content = "";
            validContent = false;
            data = null;
            error = null;
            Dictionary<string, string> tipiInizio = new Dictionary<string, string>() { { "LOG", @"<LogTransazione" },
                                                                                       { "LTA", @"<LTA_Giornaliera" },
                                                                                       { "RPG", @"<RiepilogoGiornaliero" },
                                                                                       { "RCA", @"<RiepilogoControlloAccessi" },
                                                                                       { "RPM", @"<RiepilogoMensile" }  };

            Dictionary<string, string> tipiTermin = new Dictionary<string, string>() { { "LOG", @"</LogTransazione>" },
                                                                                       { "LTA", @"</LTA_Giornaliera>" },
                                                                                       { "RPG", @"</RiepilogoGiornaliero>" },
                                                                                       { "RCA", @"</RiepilogoControlloAccessi>" },
                                                                                       { "RPM", @"</RiepilogoMensile>" }  };

            //Dictionary<string, string> tipiChkDtd = new Dictionary<string, string>() { { "LOG", @"Log_v0040_20190627.dtd" },
            //                                                                           { "LTA", @"Lta_v0001_20081106.dtd" },
            //                                                                           { "RPG", @"RiepilogoGiornaliero_v0039_20040209.dtd" },
            //                                                                           { "RCA", @"ControlloAccessi_v0001_20080626.dtd" },
            //                                                                           { "RPM", @"RiepilogoMensile_v0039_20040209.dtd" }  };

            Dictionary<string, string> tipiRNodes = new Dictionary<string, string>() { { "LOG", @"//Transazione" },
                                                                                       { "LTA", @"//LTA_Evento" } };

            if (fileInfo.Extension.Trim().ToUpper() == ".EML")
            {
                string f = "";
            }
            else
            {
                string inner_content = File.ReadAllText(fileInfo.FullName);

                if (!inner_content.Contains(tipiInizio[tipo]) || !inner_content.Contains(tipiTermin[tipo]))
                {
                    error = new Exception("errore nel contenuto del file");
                    content = inner_content;
                    validContent = false;
                }
                else
                {
                    int inizio = inner_content.IndexOf(tipiInizio[tipo]);
                    content = inner_content.Substring(inizio);
                    int termin = content.LastIndexOf(tipiTermin[tipo]);
                    content = content.Substring(0, termin + tipiTermin[tipo].Length);
                    validContent = content.StartsWith(tipiInizio[tipo]) && content.EndsWith(tipiTermin[tipo]);
                    if (validContent && (tipo == "LOG" || tipo == "LTA"))
                    {
                        try
                        {
                            XmlDocument doc = new XmlDocument();
                            try
                            {
                                doc.LoadXml(content);
                            }
                            catch (Exception ex)
                            {
                                error = ex;
                            }
                            
                            if (tipo == "LOG" && error == null)
                            {
                                XmlNodeList Transazioni = doc.SelectNodes(tipiRNodes[tipo]);
                                data = new List<Dictionary<string, string>>();
                                foreach (XmlNode Transazione in Transazioni)
                                {
                                    Dictionary<string, string> dataTransazione = new Dictionary<string, string>();
                                    data.Add(dataTransazione);
                                    foreach (XmlAttribute attribute in Transazione.Attributes)
                                    {
                                        dataTransazione.Add(attribute.Name, attribute.Value);
                                    }

                                    if (Transazione.InnerText != null && Transazione.InnerText != "" && (Transazione.ChildNodes == null || Transazione.ChildNodes.Count == 0))
                                    {
                                        dataTransazione.Add(Transazione.Name, Transazione.InnerText);
                                    }

                                    bool lTitoloAccesso = false;
                                    foreach (XmlNode childNode in Transazione.ChildNodes)
                                    {
                                        if (childNode.Name == "TitoloAccesso")
                                        {
                                            dataTransazione.Add("TitoloAbbonamento", "T");
                                            lTitoloAccesso = true;
                                        }
                                        else if (childNode.Name == "BigliettoAbbonamento")
                                        {
                                            dataTransazione.Add("TitoloAbbonamento", "T");
                                        }
                                        else if (childNode.Name == "Abbonamento")
                                        {
                                            dataTransazione.Add("TitoloAbbonamento", "A");
                                        }
                                        foreach (XmlAttribute attribute in childNode.Attributes)
                                        {
                                            dataTransazione.Add(attribute.Name, attribute.Value);
                                        }
                                        if (childNode.InnerText != null && childNode.InnerText != "" && (childNode.ChildNodes == null || childNode.ChildNodes.Count == 0))
                                        {
                                            dataTransazione.Add(childNode.Name, childNode.InnerText);
                                        }
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            foreach (XmlAttribute attribute in subChildNode.Attributes)
                                            {
                                                if (subChildNode.Name == "Turno" && attribute.Name == "valore")
                                                    dataTransazione.Add("Turno", attribute.Value);
                                                else
                                                    dataTransazione.Add(attribute.Name, attribute.Value);
                                            }
                                            if (subChildNode.InnerText != null && subChildNode.InnerText != "" && (subChildNode.ChildNodes == null || subChildNode.ChildNodes.Count == 0))
                                            {
                                                dataTransazione.Add(subChildNode.Name, subChildNode.InnerText);
                                            }

                                            if (lTitoloAccesso && subChildNode.Name == "Complementare")
                                            {
                                                string d = "";
                                            }
                                        }
                                    }
                                }
                            }
                            else if (tipo == "LTA" && error == null)
                            {
                                data = new List<Dictionary<string, string>>();

                                XmlNode LTA_Giornaliera = doc.SelectSingleNode("//LTA_Giornaliera");

                                Dictionary<string, string> dataLTA_Giornaliera = new Dictionary<string, string>();

                                foreach (XmlAttribute attribute in LTA_Giornaliera.Attributes)
                                {
                                    dataLTA_Giornaliera.Add(attribute.Name, attribute.Value);
                                }

                                if (LTA_Giornaliera.InnerText != null && LTA_Giornaliera.InnerText != "" && (LTA_Giornaliera.ChildNodes == null || LTA_Giornaliera.ChildNodes.Count == 0))
                                {
                                    dataLTA_Giornaliera.Add(LTA_Giornaliera.Name, LTA_Giornaliera.InnerText);
                                }

                                foreach (XmlNode LTA_Evento in LTA_Giornaliera.ChildNodes)
                                {
                                    Dictionary<string, string> dataLTA_Evento = new Dictionary<string, string>();

                                    foreach (XmlAttribute attribute in LTA_Evento.Attributes)
                                    {
                                        dataLTA_Evento.Add(attribute.Name, attribute.Value);
                                    }

                                    if (LTA_Evento.InnerText != null && LTA_Evento.InnerText != "" && (LTA_Evento.ChildNodes == null || LTA_Evento.ChildNodes.Count == 0))
                                    {
                                        dataLTA_Evento.Add(LTA_Evento.Name, LTA_Evento.InnerText);
                                    }

                                    Dictionary<string, string> dataSupporto = null;
                                    Dictionary<string, string> dataTitoloAccesso = null;
                                    foreach (XmlNode node in LTA_Evento.ChildNodes)
                                    {
                                        if (node.Name == "Supporto")
                                        {
                                            //dataSupporto = new Dictionary<string, string>();
                                            //foreach (XmlAttribute attribute in node.Attributes)
                                            //{
                                            //    dataSupporto.Add(attribute.Name, attribute.Value);
                                            //}
                                        }
                                        else if (node.Name == "TitoloAccesso")
                                        {
                                            dataTitoloAccesso = new Dictionary<string, string>();
                                            foreach (XmlAttribute attribute in node.Attributes)
                                            {
                                                dataTitoloAccesso.Add(attribute.Name, attribute.Value);
                                            }
                                            // fine riga

                                            if (dataLTA_Giornaliera != null && dataLTA_Evento != null && dataTitoloAccesso != null)
                                            {
                                                Dictionary<string, string> row = new Dictionary<string, string>();
                                                data.Add(row);
                                                foreach (KeyValuePair<string, string> item in dataLTA_Giornaliera)
                                                {
                                                    if (!row.ContainsKey(item.Key))
                                                        row.Add(item.Key, item.Value);
                                                }
                                                foreach (KeyValuePair<string, string> item in dataLTA_Evento)
                                                {
                                                    if (!row.ContainsKey(item.Key))
                                                        row.Add(item.Key, item.Value);
                                                }
                                                if (dataSupporto != null)
                                                {
                                                    foreach (KeyValuePair<string, string> item in dataSupporto)
                                                    {
                                                        if (!row.ContainsKey(item.Key))
                                                            row.Add(item.Key, item.Value);
                                                    }
                                                }
                                                foreach (KeyValuePair<string, string> item in dataTitoloAccesso)
                                                {
                                                    if (!row.ContainsKey(item.Key))
                                                        row.Add(item.Key, item.Value);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            error = new Exception("Errore parsing xml" + "\r\n" + ex.Message);
                        }
                    }
                }
            }
            if (error != null)
                RaiseOutMessage("Contenuto file ERRORE", true, error);
            return error == null;
        }


        public static bool GetFiles(string path, ref List<clsFile> result, out Exception error)
        {
            bool resultFile = false;
            error = null;
            RaiseOutMessage(path, true);
            try
            {

                if (result == null)
                    result = new List<clsFile>();

                System.IO.DirectoryInfo dirInfo = new DirectoryInfo(path);
                List<string> tipi = new List<string>() { "LOG", "LTA", "RPG", "RCA", "RPM" };
                foreach (FileInfo fileInfo in dirInfo.GetFiles())
                {
                    if ((fileInfo.Extension.Trim().ToUpper() == ".P7M" && tipi.Contains(fileInfo.Name.Substring(0, 3))) ||
                        (fileInfo.Extension.Trim().ToUpper() == ".EML" && tipi.Contains(fileInfo.Name.Substring(0, 3))))
                    {
                        resultFile = true;
                        clsFile file = new clsFile(fileInfo.FullName);
                        result.Add(file);
                        //RaiseOutMessage(file.FileName);
                    }
                }
                if (result != null && result.Count > 0)
                    RaiseOutMessage(string.Format("Trovati {0} files.", result.Count.ToString()), true);

                foreach (DirectoryInfo subDirInfo in dirInfo.GetDirectories())
                {
                    resultFile = GetFiles(subDirInfo.FullName, ref result, out error);
                }

            }
            catch (Exception ex)
            {
                error = ex;
            }
            return resultFile;
        }

        public static bool CheckExistsRows(List<clsFile> lista, out Exception error)
        {
            error = null;
            bool result = false;
            IConnection conn = new Wintic.Data.oracle.CConnectionOracle();
            conn.ConnectionString = "user=wtic;password=obelix;data source=server-2019;";
            try
            {
                conn.Open();
                DataTable tabProgs = conn.ExecuteQuery("SELECT * FROM WTIC.RIEPILOGHI_PROGS");
                DataTable tabFiles = conn.ExecuteQuery("SELECT * FROM WTIC.RIEPILOGHI_FILES");
                List<string> tipi = new List<string>() { "LOG", "LTA", "RPG", "RCA", "RPM" };
                foreach (string tipo in tipi)
                { 
                    foreach (clsFile file in lista.Where(f => f.Tipo == tipo).ToList().OrderBy(f => f.Giorno).ThenBy(f => f.Prog))
                    {
                        if (file.Error == null)
                        {
                            bool existsProg = false;
                            bool existsFile = false;
                            foreach (DataRow row in tabProgs.Rows)
                            {
                                if ((DateTime)row["GIORNO"] == file.Giorno
                                     && row["TIPO"].ToString() == file.Tipo
                                     && row["NOME"].ToString() == file.FileName
                                     && int.Parse(row["PROG"].ToString()) == file.Prog)
                                {
                                    existsProg = true;
                                    break;
                                }
                            }

                            foreach (DataRow row in tabFiles.Rows)
                            {
                                if ((DateTime)row["GIORNO"] == file.Giorno
                                     && row["TIPO"].ToString() == file.Tipo
                                     && row["NOME"].ToString() == file.FileName
                                     && int.Parse(row["PROG"].ToString()) == file.Prog)
                                {
                                    existsFile = true;
                                    break;
                                }
                            }

                            if (tipo == "LOG")
                            {
                                // verifica inserimento righe
                                List<Dictionary<string, object>> logs = GetListFromTable(conn.ExecuteQuery("SELECT * FROM WTIC.LOG_TRANSAZIONI WHERE DATA_EMISSIONE_ANNULLAMENTO LIKE :pGIORNO", new clsParameters(":pGIORNO", file.Giorno)));
                                foreach (Dictionary<string, string> row in file.Data)
                                {
                                    Dictionary<string, object> dataRow = CheckDataRowExists(logs, "SIGILLO", row["SigilloFiscale"].ToString());
                                    if (dataRow == null)
                                    {

                                    }
                                }
                            }
                            else if (tipo == "LTA")
                            {
                                List<Dictionary<string, object>> lta = GetListFromTable(conn.ExecuteQuery("SELECT * FROM WTIC.LOG_TRANSAZIONI WHERE DATA_ORA_EVENTO LIKE :pGIORNO", new clsParameters(":pGIORNO", file.Giorno)));
                                foreach (Dictionary<string, string> row in file.Data)
                                {
                                    Dictionary<string, object> dataRow = CheckDataRowExists(lta, "SIGILLO", row["SigilloFiscale"].ToString());
                                    if (dataRow == null)
                                    {

                                    }
                                }
                            }

                            if (!existsProg || !existsFile)
                            {
                                string ff = "";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                error = ex;
            }
            conn.Close();
            conn.Dispose();
            return result;
        }

        private static List<Dictionary<string, object>> GetListFromTable(DataTable table)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            foreach (DataRow row in table.Rows)
            {
                Dictionary<string, object> itemRow = new Dictionary<string, object>();
                result.Add(itemRow);
                foreach (DataColumn col in table.Columns)
                {
                    itemRow.Add(col.ColumnName, row[col.ColumnName]);
                }
            }
            return result;
        }

        private static Dictionary<string, object> CheckDataRowExists(List<Dictionary<string, object>> rows, string field, string value)
        {
            return rows.Find(r => r[field].ToString() == value); ;
        }

        public static Dictionary<string, clsCartaCheckContatori> CheckTransazioniContatori(List<clsFile> files)
        {
            RaiseOutMessage("Controllo Contatori", true);
            RaiseOutMessage("Avvio...", false);
            Dictionary<string, clsCartaCheckContatori> carte = new Dictionary<string, clsCartaCheckContatori>();
            int count = 0;
            List<clsFile> listaLog = files.Where(f => f.Tipo == "LOG").ToList();
            foreach (clsFile logFile in listaLog)
            {
                count += 1;
                RaiseOutMessage(string.Format("{0} di {1}", count.ToString(), listaLog.Count.ToString()), false);

                foreach (Dictionary<string, string> row in logFile.Data)
                {
                    clsCartaCheckContatori cartaCheckContatori = null;
                    clsTransazioneCheckContatori transazione = new clsTransazioneCheckContatori();
                    transazione.Row = row;

                    if (!transazione.Row.ContainsKey("CartaAttivazione"))
                    {
                        transazione.AddErrore(new Exception("campo CartaAttivazione non trovato"));
                        transazione.Carta = "sconosciuta";
                    }
                    else
                    {
                        transazione.Carta = transazione.Row["CartaAttivazione"].ToString();
                    }

                    if (!carte.ContainsKey(transazione.Carta))
                    {
                        cartaCheckContatori = new clsCartaCheckContatori(transazione.Carta);
                        carte.Add(transazione.Carta, cartaCheckContatori);
                    }
                    else
                        cartaCheckContatori = carte[transazione.Carta];

                    Int32 progressivo = 0;
                    if (!transazione.Row.ContainsKey("NumeroProgressivo"))
                        transazione.AddErrore(new Exception("campo NumeroProgressivo non trovato"));
                    else
                    {
                        if (!Int32.TryParse(transazione.Row["NumeroProgressivo"], out progressivo))
                            transazione.AddErrore(new Exception(string.Format("campo NumeroProgressivo non valido [{0}]", transazione.Row["NumeroProgressivo"])));
                        else
                            transazione.Contatore = progressivo;
                    }

                    if (transazione.Errori == null)
                    {
                        clsTransazioneCheckContatori transazioneDupl = cartaCheckContatori.Transazioni.Find(t => t.Carta == transazione.Carta && t.Contatore == transazione.Contatore);
                        if (transazioneDupl != null)
                            transazione.AddErrore(new Exception("Transazione duplicata"));
                    }
                    if (transazione.Errori != null)
                        transazione.FileLog = logFile.FullFileName;

                    cartaCheckContatori.Transazioni.Add(transazione);
                }
            }


            foreach (KeyValuePair<string, clsCartaCheckContatori> itemCarta in carte)
            {
                itemCarta.Value.CheckContatoriMancanti();
            }
            return carte;
        }
    }

    
    public class clsTransazioneCheckContatori
    {
        public Dictionary<string, string> Row { get; set; }
        public List<Exception> Errori { get; set; }
        public string Carta { get; set; }
        public Int32 Contatore { get; set; }
        public string FileLog { get; set; }

        public clsTransazioneCheckContatori()
        {
            this.Row = null;
            this.Errori = null;
            this.Carta = "";
            this.Contatore = 0;
        }

        public clsTransazioneCheckContatori(string carta, Int32 contatore)
            :this()
        {
            this.Carta = carta;
            this.Contatore = contatore;
        }

        public void AddErrore(Exception errore)
        {
            if (this.Errori == null)
                this.Errori = new List<Exception>();
            this.Errori.Add(errore);
        }
    }

    public class clsCartaCheckContatori
    {
        public string Carta { get; set; }
        public List<clsTransazioneCheckContatori> Transazioni { get; set; }

        public List<KeyValuePair<Int32, Int32>> ContatoriMancanti { get; set; }

        public clsCartaCheckContatori()
        {
            this.Transazioni = new List<clsTransazioneCheckContatori>();
        }

        public clsCartaCheckContatori(string carta)
            :this()
        {
            this.Carta = carta;
        }

        public Int32 ContatoreMin => (this.Transazioni != null && this.Transazioni.Count > 0 ? this.Transazioni.Min(t => t.Contatore) : 0);
        public Int32 ContatoreMax => (this.Transazioni != null && this.Transazioni.Count > 0 ? this.Transazioni.Max(t => t.Contatore) : 0);
        public bool NumeroContatoriNonValido => (this.Transazioni != null && this.Transazioni.Count > 0 ? (this.ContatoreMax - this.ContatoreMin + 1) != this.Transazioni.Count() : false);

        public List<KeyValuePair<Int32, Int32>> CheckContatoriMancanti()
        {
            List<KeyValuePair<Int32, Int32>> result = new List<KeyValuePair<int, int>>();
            List<Int32> mancanti = new List<int>();

            if (this.Transazioni != null && this.Transazioni.Count > 0)
            {
                this.Transazioni.ForEach(t =>
                {
                    if (t.Contatore > 1)
                    {
                        Int32 maxPrecedente = 0;
                        List<clsTransazioneCheckContatori> listaPrecedenti = this.Transazioni.Where(tm => tm.Contatore < t.Contatore).ToList();
                        if (listaPrecedenti != null && listaPrecedenti.Count > 0)
                            maxPrecedente = listaPrecedenti.Max(tm => tm.Contatore);

                        if (maxPrecedente < t.Contatore - 1)
                        {
                            for (Int32 contatoreMancante = maxPrecedente + 1; contatoreMancante < t.Contatore; contatoreMancante++)
                                mancanti.Add(contatoreMancante);
                        }
                    }
                });
            }

            Int32 inizio = 0;
            Int32 fine = 0;
            mancanti.Sort((x, y) => x.CompareTo(y));
            mancanti.ForEach(m =>
            {
                if (inizio == 0)
                {
                    inizio = m;
                    fine = inizio;
                }
                else
                {
                    if (m == fine + 1)
                        fine = m;
                    else
                    {
                        result.Add(new System.Collections.Generic.KeyValuePair<Int32, Int32>(inizio, fine));
                        inizio = m;
                        fine = inizio;
                    }
                }
            });
            if (inizio > 0)
                result.Add(new System.Collections.Generic.KeyValuePair<Int32, Int32>(inizio, fine));
            this.ContatoriMancanti = result;
            return result;
        }
    }

    public class clsButtonBase : Button
    {

        private CheckState checkSt = CheckState.Indeterminate;
        public clsButtonBase()
        {
            this.DoubleBuffered = true;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            base.OnPaint(e);
            if (this.checkSt == CheckState.Checked || this.checkSt == CheckState.Unchecked)
            {
                Point p = new Point(3, (this.ClientSize.Height - System.Windows.Forms.CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal).Height) / 2);
                System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(e.Graphics, p, (this.checkSt == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal));
            }
        }

        public CheckState CheckState
        {
            get { return this.checkSt; }
            set { this.checkSt = value; this.Invalidate(); }
        }

    }

    public class clsCheckBox : CheckBox
    {
        protected override void OnPaint(PaintEventArgs pevent)
        {
            pevent.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            base.OnPaint(pevent);
            //pevent.Graphics.DrawRectangle(Pens.Black, new Rectangle(0, 0, this.ClientSize.Width - 1, this.ClientSize.Height - 1));
        }
    }

    public class clsLabel : Label
    {
        //public bool DrawArrow = true;
        private Icon icon = null;
        private Image imageIcon = null;
        private string verticalText = "";

        private CheckState checkSt = CheckState.Indeterminate;
        public CheckState CheckState
        {
            get { return this.checkSt; }
            set { this.checkSt = value; this.Invalidate(); }
        }

        public System.Drawing.Rectangle GetRectIcon()
        {
            if (this.icon == null)
            {
                if (this.imageIcon == null)
                    return new Rectangle(0, 0, 0, 0);
                else
                    return new Rectangle(new Point(2, this.ClientSize.Height - this.imageIcon.Height - 2), new Size(this.imageIcon.Width, this.imageIcon.Height));
            }
            else
            {
                return new Rectangle(new Point(2, this.ClientSize.Height - this.icon.Height - 2), new Size(this.icon.Width, this.icon.Height));
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            StringFormat st = clsUtility.GetStringFormatFromContentAlign(base.TextAlign);
            //if (DrawArrow)
            //{
            //    int middle = this.ClientSize.Height / 2;
            //    int sizeArrow = 8;
            //    Rectangle rectArror = new Rectangle(this.ClientSize.Width - sizeArrow - 1, (this.ClientSize.Height - sizeArrow) / 2, sizeArrow, sizeArrow);
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left, middle), new Point(rectArror.Right, middle));
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left + (sizeArrow / 2), rectArror.Top), new Point(rectArror.Right, middle));
            //    e.Graphics.DrawLine(Pens.Black, new Point(rectArror.Left + (sizeArrow / 2), rectArror.Bottom), new Point(rectArror.Right, middle));
            //    Rectangle rectText = new Rectangle(0, 0, this.ClientSize.Width - rectArror.Width - 1, this.ClientSize.Height);
            //    e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), rectText, st);
            //}
            //else
            e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), this.ClientRectangle, st);

            if (this.verticalText != "")
            {
                e.Graphics.RotateTransform(-90);
                Font vFont = new Font(this.Font.FontFamily, this.Font.Size - 2);
                Size sizeText = e.Graphics.MeasureString(this.verticalText, vFont).ToSize();
                e.Graphics.DrawString(this.verticalText, vFont, new SolidBrush(this.ForeColor), -(sizeText.Width), sizeText.Height - 1);
                e.Graphics.ResetTransform();
            }

            if (this.icon != null)
                e.Graphics.DrawIcon(this.icon, this.GetRectIcon().X, this.GetRectIcon().Y);
            else if (this.imageIcon != null)
                e.Graphics.DrawImage(this.imageIcon, this.GetRectIcon().X, this.GetRectIcon().Y);

            if (this.checkSt == CheckState.Checked || this.checkSt == CheckState.Unchecked)
            {
                Point p = new Point(3, (this.ClientSize.Height - System.Windows.Forms.CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal).Height) / 2);
                System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(e.Graphics, p, (this.checkSt == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal));
            }
        }

        public Icon Icon
        {
            get { return this.icon; }
            set { this.icon = value; this.Invalidate(); }
        }

        public Image ImageIcon
        {
            get { return this.imageIcon; }
            set { this.imageIcon = value; this.Invalidate(); }
        }

        [System.ComponentModel.Browsable(false)]
        public string VerticalText
        {
            get { return this.verticalText; }
            set { this.verticalText = value; this.Invalidate(); }
        }
    }

    public static class clsUtility
    {
        public static string ApplicationPath
        {
            get
            {
                System.IO.FileInfo ofileInfoApp = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string result = ofileInfoApp.Directory.FullName;

                return result;
            }
        }

        public static bool CheckTryParseDecimal(string text, out decimal value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else if (text.Contains(".") && text.Contains(","))
            {
                result = decimal.TryParse(text, out value);
            }
            else if (text.Contains(".") && !text.Contains(","))
            {
                result = decimal.TryParse(text.Replace(".", ","), out value);
            }
            else
                result = decimal.TryParse(text, out value);
            return result;
        }

        public static bool CheckTryParseInt(string text, out int value)
        {
            value = 0;
            bool result = false;
            if (text == "")
            {
                result = true;
                value = 0;
            }
            else
                result = int.TryParse(text.Replace(".", "").Replace(".", ""), out value);
            return result;
        }

        public static StringFormat GetStringFormatFromContentAlign(ContentAlignment align)
        {
            StringFormat result = new StringFormat();
            result.Alignment = StringAlignment.Center;
            result.LineAlignment = StringAlignment.Center;
            switch (align)
            {
                case ContentAlignment.TopLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.MiddleLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.BottomLeft: { result.Alignment = StringAlignment.Near; break; }
                case ContentAlignment.TopRight: { result.Alignment = StringAlignment.Far; break; }
                case ContentAlignment.MiddleRight: { result.Alignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomRight: { result.Alignment = StringAlignment.Far; break; }
            }
            switch (align)
            {
                case ContentAlignment.TopLeft: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.TopRight: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.TopCenter: { result.LineAlignment = StringAlignment.Near; break; }
                case ContentAlignment.BottomLeft: { result.LineAlignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomRight: { result.LineAlignment = StringAlignment.Far; break; }
                case ContentAlignment.BottomCenter: { result.LineAlignment = StringAlignment.Far; break; }
            }
            return result;
        }

        public static object MenuGenerico(string title, Dictionary<object, string> options, Point? startupPosition = null, Icon iconMenu = null, bool checkBox = false, Dictionary<object, bool> itemschecked = null)
        {
            object result = null;
            Form frmMenu = new Form();
            frmMenu.Font = new Font("Calibri", 12, FontStyle.Regular);
            if (startupPosition != null)
            {
                frmMenu.StartPosition = FormStartPosition.Manual;
                frmMenu.Location = (Point)startupPosition;
            }
            else
                frmMenu.StartPosition = FormStartPosition.CenterScreen;
            if (iconMenu != null)
                frmMenu.Icon = iconMenu;
            frmMenu.FormBorderStyle = FormBorderStyle.FixedDialog;
            frmMenu.ControlBox = true;
            frmMenu.MinimizeBox = false;
            frmMenu.MaximizeBox = false;
            frmMenu.AutoScroll = true;
            frmMenu.Size = new Size(300, 400);
            frmMenu.Text = title;
            clsButtonBase btn;
            bool findAbort = false;
            int height = frmMenu.Padding.Vertical;

            Graphics g;
            if (checkBox)
            {
                clsLabel lbl = new clsLabel();
                lbl.CheckState = CheckState.Unchecked;
                lbl.AutoSize = false;

                lbl.Text = "";
                lbl.Dock = DockStyle.Top;
                lbl.Size = new Size(100, 20);
                height += lbl.Height;
                lbl.Tag = "_aLL_";
                frmMenu.Controls.Add(lbl);
                lbl.BringToFront();

                lbl.Click += (sender, e) =>
                {
                    clsLabel lblSender = (clsLabel)sender;
                    lblSender.CheckState = (lblSender.CheckState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
                    foreach (Control c in frmMenu.Controls)
                    {
                        try
                        {
                            clsButtonBase btnOther = (clsButtonBase)c;
                            if (btnOther.Tag != null)
                                btnOther.CheckState = lblSender.CheckState;
                        }
                        catch (Exception)
                        {
                        }
                    }

                };
            }
            foreach (KeyValuePair<object, string> item in options)
            {
                btn = new clsButtonBase();
                btn.Text = item.Value;
                btn.Dock = DockStyle.Top;
                btn.Size = new Size(100, 50);
                height += btn.Height;
                findAbort = item.Key == null;
                btn.Tag = item.Key;
                g = btn.CreateGraphics();
                Size sizeText = g.MeasureString(btn.Text, frmMenu.Font, frmMenu.ClientSize.Width - frmMenu.Padding.Horizontal).ToSize();
                if (sizeText.Height > btn.Height)
                {
                    btn.Size = new Size(100, sizeText.Height + 20);
                }
                if (checkBox)
                {
                    btn.CheckState = ((itemschecked != null && itemschecked.ContainsKey(item.Key) && itemschecked[item.Key]) ? CheckState.Checked : CheckState.Unchecked);
                }
                frmMenu.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (sender, e) =>
                {
                    clsButtonBase btnSender = (clsButtonBase)sender;
                    if (!checkBox)
                    {
                        ((Form)btnSender.Parent).Tag = btnSender.Tag;
                        if (((Form)btnSender.Parent).Tag != null)
                            ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        btnSender.CheckState = (btnSender.CheckState == CheckState.Checked ? CheckState.Unchecked : CheckState.Checked);
                    }
                };
            }

            if (checkBox)
            {
                btn = new clsButtonBase();
                btn.Text = "Conferma";
                btn.Dock = DockStyle.Top;
                btn.Size = new Size(100, 50);
                height += btn.Height;
                frmMenu.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (sender, e) =>
                {
                    clsButtonBase btnSender = (clsButtonBase)sender;
                    Dictionary<object, string> selected = new Dictionary<object, string>();
                    foreach (Control c in frmMenu.Controls)
                    {
                        try
                        {
                            clsButtonBase btnCheckd = (clsButtonBase)c;
                            if (btnCheckd.CheckState == CheckState.Checked)
                            {
                                string item = options.Where(w => w.Key == btnCheckd.Tag.ToString()).FirstOrDefault().Value;
                                if (item != null)
                                    selected.Add(btnCheckd.Tag, item);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    ((Form)btnSender.Parent).Tag = selected;
                    ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                };
            }

            if (!findAbort)
            {
                btn = new clsButtonBase();
                btn.Text = "Abbandona";
                btn.Dock = DockStyle.Top;
                btn.Size = new Size(100, 50);
                height += btn.Height;
                frmMenu.Controls.Add(btn);
                btn.BringToFront();
                btn.Click += (sender, e) =>
                {
                    clsButtonBase btnSender = (clsButtonBase)sender;
                    ((Form)btnSender.Parent).Tag = null;
                    ((Form)btnSender.Parent).DialogResult = DialogResult.OK;
                };
            }

            if (frmMenu.ClientSize.Height > height)
            {
                frmMenu.ClientSize = new Size(frmMenu.ClientSize.Width, height);
            }

            if (frmMenu.ShowDialog() == DialogResult.OK)
            {
                result = frmMenu.Tag;
            }
            frmMenu.Close();
            frmMenu.Dispose();

            return result;
        }


        private static bool lFontResourceInitialized = false;
        private static System.Drawing.Text.PrivateFontCollection oFontCollection = new System.Drawing.Text.PrivateFontCollection();
        public static string FontName = "Swis721 Cn BT"; //"Univers Condensed"; // "Swis721 Cn BT"; //"Courier New";
        private static float nFontSize = 14;
        private static FontStyle oFontStyle = new FontStyle();
        public static System.Drawing.Text.PrivateFontCollection FontCollection
        {
            get
            {
                if (!lFontResourceInitialized)
                {
                    lFontResourceInitialized = true;
                    try
                    {
                        System.IO.FileInfo[] oFontFiles = (new System.IO.DirectoryInfo(ApplicationPath + "\\Fonts")).GetFiles("*.ttf");
                        if (oFontFiles != null && oFontFiles.Length > 0)
                        {
                            foreach (System.IO.FileInfo ItemFontFile in oFontFiles)
                            {
                                oFontCollection.AddFontFile(ItemFontFile.FullName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string cErrorLoadFont = ex.ToString();
                    }
                }
                return oFontCollection;
            }
        }

        public static Font Font()
        {
            return GetFont(FontName, nFontSize, oFontStyle);
        }
        public static Font GetFont(float fontSize)
        {
            return GetFont(FontName, fontSize, oFontStyle);
        }
        public static Font GetFont(float fontSize, FontStyle fontStyle)
        {
            return GetFont(FontName, fontSize, fontStyle);
        }
        public static Font GetFont(string fontName, float fontSize, FontStyle fontStyle)
        {
            bool lPrivateFont = false;
            Font oFont = null;
            if (FontCollection != null && FontCollection.Families.Length > 0)
            {
                foreach (System.Drawing.FontFamily ItemFontFamily in FontCollection.Families)
                {
                    if (ItemFontFamily.Name == fontName)
                    {
                        oFont = new Font(ItemFontFamily, fontSize, fontStyle);
                        lPrivateFont = true;
                        break;
                    }
                }
            }

            if (!lPrivateFont)
            {
                oFont = new Font(fontName, fontSize, fontStyle);
            }
            return oFont;
        }

        public static Stream ConvertToBase64(this Stream stream)
        {
            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }

            string base64 = Convert.ToBase64String(bytes);
            return new MemoryStream(Encoding.UTF8.GetBytes(base64));
        }

        public static int GetNumDayOfWeek(DateTime dDay)
        {
            int nRet = 0;
            switch (dDay.DayOfWeek)
            {
                case DayOfWeek.Monday: { nRet = 1; break; }
                case DayOfWeek.Tuesday: { nRet = 2; break; }
                case DayOfWeek.Wednesday: { nRet = 3; break; }
                case DayOfWeek.Thursday: { nRet = 4; break; }
                case DayOfWeek.Friday: { nRet = 5; break; }
                case DayOfWeek.Saturday: { nRet = 6; break; }
                case DayOfWeek.Sunday: { nRet = 7; break; }
            }
            return nRet;
        }

        public static string GetDescDayOfWeek(DateTime dDay)
        {
            return GetDescDayOfWeek(GetNumDayOfWeek(dDay));
        }

        public static string GetDescDayOfWeek(int nDay)
        {
            string cRet = "";
            switch (nDay)
            {
                case 1: { cRet = "Lu"; break; }
                case 2: { cRet = "Ma"; break; }
                case 3: { cRet = "Me"; break; }
                case 4: { cRet = "Gi"; break; }
                case 5: { cRet = "Ve"; break; }
                case 6: { cRet = "Sa"; break; }
                case 7: { cRet = "Do"; break; }
            }
            return cRet;
        }

        public static string GetDescMonth(DateTime dDate)
        {
            return GetDescMonth(dDate.Month);
        }

        public static string GetDescMonth(int nMonth)
        {
            string cRet = "";
            switch (nMonth)
            {
                case 1: { cRet = "Gennaio"; break; }
                case 2: { cRet = "Febbraio"; break; }
                case 3: { cRet = "Marzo"; break; }
                case 4: { cRet = "Aprile"; break; }
                case 5: { cRet = "Maggio"; break; }
                case 6: { cRet = "Giugno"; break; }
                case 7: { cRet = "Luglio"; break; }
                case 8: { cRet = "Agosto"; break; }
                case 9: { cRet = "Settembre"; break; }
                case 10: { cRet = "Ottobre"; break; }
                case 11: { cRet = "Novembre"; break; }
                case 12: { cRet = "Dicembre"; break; }
            }
            return cRet;
        }

        public static String ConvertFromByteToString(byte b)
        {
            StringBuilder str = new StringBuilder(8);
            int[] bl = new int[8];

            for (int i = 0; i < bl.Length; i++)
            {
                bl[bl.Length - 1 - i] = ((b & (1 << i)) != 0) ? 1 : 0;
            }

            foreach (int num in bl) str.Append(num);

            return str.ToString();
        }

        public static byte ConvertFromStringToByte(string binaryStr)
        {
            var byteArray = Enumerable.Range(0, int.MaxValue / 8)
                          .Select(i => i * 8)
                          .TakeWhile(i => i < binaryStr.Length)
                          .Select(i => binaryStr.Substring(i, 8))
                          .Select(s => Convert.ToByte(s, 2))
                          .ToArray();
            return byteArray[0];
        }

        public static Color Lighten(Color inColor, double inAmount)
        {
            return Color.FromArgb(inColor.A,
                    (int)Math.Min(255, inColor.R + 255 * inAmount),
                    (int)Math.Min(255, inColor.G + 255 * inAmount),
                    (int)Math.Min(255, inColor.B + 255 * inAmount));
        }

        public static void ColorToHSV(Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }

        public static Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }

        public static string ExtractRowDataToString(DataRow row, bool headers, ref List<int> sizes)
        {
            string result = "";
            int index = 0;
            if (sizes == null && headers)
            {
                sizes = new List<int>();
                foreach (DataColumn col in row.Table.Columns)
                {
                    sizes.Add(col.ColumnName.Length);
                }
                foreach (DataRow rowSize in row.Table.Rows)
                {
                    index = 0;
                    foreach (DataColumn col in row.Table.Columns)
                    {
                        string value = "";
                        if (rowSize[col.ColumnName] != null) value = rowSize[col.ColumnName].ToString();
                        if (sizes[index] < value.Length) sizes[index] = value.Length;
                        index += 1;
                    }
                }
            }
            if (headers)
            {
                index = 0;
                foreach (DataColumn col in row.Table.Columns)
                {
                    result += (result.Trim() == "" ? "" : "|") + (sizes == null || sizes.Count == 0 ? col.ColumnName : col.ColumnName.PadRight(sizes[index]));
                    index += 1;
                }
            }
            else
            {
                index = 0;
                foreach (DataColumn col in row.Table.Columns)
                {
                    result += (result.Trim() == "" ? "" : "|") + (sizes == null || sizes.Count == 0 ? (row[col.ColumnName] != null ? row[col.ColumnName].ToString() : "") : (row[col.ColumnName] != null ? row[col.ColumnName].ToString() : "").PadRight(sizes[index]));
                    index += 1;
                }
            }
            return result;
        }
    }

    public class clsItemGenericData
    {
        public enum EnumGenericDataType : int
        {
            TypeNumInt = 1,
            TypeNumDecimal = 2,
            TypeString = 3,
            TypeDate = 4,
            TypeDateTime = 5
        }

        private object value { get; set; }

        public object Value
        {
            get
            {
                switch (this.DataType)
                {
                    case EnumGenericDataType.TypeNumInt:
                        {
                            int val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    int.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeNumDecimal:
                        {
                            decimal val = 0;
                            if (this.value != null)
                            {
                                try
                                {
                                    decimal.TryParse(this.value.ToString(), out val);
                                }
                                catch (Exception)
                                {
                                    val = 0;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDate:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeDateTime:
                        {
                            DateTime val = DateTime.MinValue;
                            if (this.value != null)
                            {
                                try
                                {
                                    val = (DateTime)this.value;
                                }
                                catch (Exception)
                                {
                                    val = DateTime.MinValue;
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                    case EnumGenericDataType.TypeString:
                        {
                            string val = "";
                            if (this.value != null)
                            {
                                try
                                {
                                    val = this.value.ToString();
                                }
                                catch (Exception)
                                {
                                    val = "";
                                    throw;
                                }
                            }
                            return val;
                            break;
                        }
                }
                return null;
            }
            set
            {
                this.value = value;
            }
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public bool Readonly { get; set; }
        public bool Visible { get; set; }

        public string Important { get; set; }

        public EnumGenericDataType DataType { get; set; }

        public List<clsItemGenericData> ComboValues { get; set; }
        public System.Windows.Forms.ComboBoxStyle ComboStyle = System.Windows.Forms.ComboBoxStyle.DropDown;

        public bool IsComboMode => ComboValues != null && ComboValues.Count > 0;

        public clsItemGenericData(string code, string description, EnumGenericDataType dataType, object value, bool readOnly, bool visible = true, string important = "", List<clsItemGenericData> comboValues = null, System.Windows.Forms.ComboBoxStyle comboStyle = System.Windows.Forms.ComboBoxStyle.DropDown)
        {
            this.Code = code;
            this.Description = description;
            this.DataType = dataType;
            this.Value = value;
            this.Readonly = readOnly;
            this.Visible = visible;
            this.Important = important;
            this.ComboValues = comboValues;
            this.ComboStyle = comboStyle;
        }



        public static clsItemGenericData GetItemFromListByDescr(string description, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Description == description)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public static clsItemGenericData GetItemFromListBycODE(string code, List<clsItemGenericData> items)
        {
            clsItemGenericData result = null;
            foreach (clsItemGenericData item in items)
            {
                if (item.Code == code)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }
        public override string ToString()
        {
            return this.Description;
        }
    }

    public class clsData
    {


        //        LOG_ID NUMBER        NOT NULL,
        //LOG_DATE                        DATE NOT NULL,
        //	IDVEND NUMBER(10),
        //CODICE_FISCALE_ORGANIZZATORE CFOrganizzatore
        //CODICE_FISCALE_TITOLARE CFTitolare
        //TITOLO_ABBONAMENTO TitoloAbbonamento
        //TITOLO_IVA_PREASSOLTA IVAPreassolta
        //SPETTACOLO_INTRATTENIMENTO TipoTassazione
        //VALUTA Valuta
        //IMPONIBILE_INTRATTENIMENTI ImponibileIntrattenimenti
        //ORDINE_DI_POSTO CodiceOrdine
        //POSTO Posto
        //TIPO_TITOLO TipoTitolo
        //ANNULLATO Annullamento	????
        //PROGRESSIVO_ANNULLATI OriginaleAnnullato

        //    DATA_ORA_EMISSIONE DATE          NOT NULL,
        //DATA_EMISSIONE_ANNULLAMENTO     DataEmissione
        //ORA_EMISSIONE_ANNULLAMENTO      OraEmissione
        //PROGRESSIVO_TITOLO              NumeroProgressivo
        //CODICE_PUNTO_VENDITA            VARCHAR2(8 BYTE),
        //SIGILLO SigilloFiscale,
        //CODICE_SISTEMA                  SistemaEmissione
        //CODICE_CARTA                    CartaAttivazione
        //PRESTAMPA                       Prestampa
        //CODICE_LOCALE                   CodiceLocale
        //    DATA_ORA_EVENTO                 DATE,
        //DATA_EVENTO DataEvento
        //TIPO_EVENTO TipoGenere
        //TITOLO_EVENTO Titolo
        //ORA_EVENTO OraEvento
        //CAUSALE_OMAGGIO_RIDUZIONE Causale
        //TIPO_TURNO Turno	???
        //NUMERO_EVENTI_ABILITATI QuantitaEventiAbilitati

        //    DATA_SCADENZA_ABBONAMENTO DATE,
        //DATA_LIMITE_VALIDITA            Validita
        //CODICE_ABBONAMENTO              CodiceAbbonamento
        //NUM_PROG_ABBONAMENTO            ProgressivoAbbonamento
        //RATEO_EVENTO                    Rateo
        //IVA_RATEO                       RateoIVA
        //    RATEO_IMPONIBILE_INTRA          RateoIntrattenimenti
        //    CAUSALE_OMAGGIO_RIDUZIONE_OPEN  VARCHAR2(30 BYTE),
        //CORRISPETTIVO_TITOLO CorrispettivoLordo
        //CORRISPETTIVO_PREVENDITA Prevendita
        //IVA_TITOLO IVACorrispettivo
        //IVA_PREVENDITA IVAPrevendita
        //CORRISPETTIVO_FIGURATIVO ImportoFigurativo
        //IVA_FIGURATIVA IVAFigurativa
        //CODICE_FISCALE_ABBONAMENTO CodiceFiscale
        //CODICE_BIGLIETTO_ABBONAMENTO CodiceAbbonamento
        //NUM_PROG_BIGLIETTO_ABBONAMENTO ProgressivoAbbonamento

        //    CODICE_PRESTAZIONE1 VARCHAR2(3 BYTE),
        //	IMPORTO_PRESTAZIONE1 NUMBER,
        //    CODICE_PRESTAZIONE2             VARCHAR2(3 BYTE),
        //	IMPORTO_PRESTAZIONE2 NUMBER,
        //    CODICE_PRESTAZIONE3             VARCHAR2(3 BYTE),
        //	IMPORTO_PRESTAZIONE3 NUMBER,
        //CARTA_ORIGINALE_ANNULLATO       CartaOriginaleAnnullato
        //CAUSALE_ANNULLAMENTO            CausaleAnnullamento
        //    PARTECIPANTE_NOME               VARCHAR2(30 BYTE),
        //	PARTECIPANTE_COGNOME VARCHAR2(30 BYTE),
        //	ACQREG_AUTENTICAZIONE VARCHAR2(4 BYTE),
        //	ACQREG_CODICEUNIVOCO_ACQ VARCHAR2(30 BYTE),
        //	ACQREG_INDIRIZZOIP_REG VARCHAR2(30 BYTE),
        //	ACQREG_DATAORA_REG DATE,
        //    ACQTRAN_CODICEUNIVOCO_NUM_TRAN  VARCHAR2(30 BYTE),
        //	ACQTRAN_CELLULARE_ACQ VARCHAR2(30 BYTE),
        //	ACQTRAN_EMAIL_ACQ VARCHAR2(90 BYTE),
        //	ACQTRAN_INDIRIZZO_IP_TRAN VARCHAR2(30 BYTE),
        //	ACQTRAN_DATAORAINIZIOCHECKOUT DATE,
        //    ACQTRAN_DATAORAESECUZIONE_PAG   DATE,
        //	ACQTRAN_CRO VARCHAR2(30 BYTE),
        //	ACQTRAN_METODO_SPED_TITOLO VARCHAR2(30 BYTE),
        //	ACQTRAN_INDIRIZZO_SPED_TITOLO VARCHAR2(30 BYTE),
        //	DIGITALE_TRADIZIONALE VARCHAR2(1 BYTE),
        //	CAPIENZA_SETTORE NUMBER




        //  Causale
        //  CodiceRichiedenteEmissioneSigillo
    }
}
