﻿using ApiRiepiloghi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;
using static ApiRiepiloghi.ServiceRiepiloghi;

namespace WinAppRiepiloghi
{
    public partial class frmWinAppRiepiloghi : Form
    {
        private string MyToken { get; set; }
        public frmWinAppRiepiloghi()
        {
            InitializeComponent();
            this.cmbTipo.Items.Add("G");
            this.cmbTipo.Items.Add("M");
            this.cmbTipo.Items.Add("R");
            this.btnOK.Click += BtnOK_Click;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if (this.txtLogin.Text != "" && this.txtPsw.Text != "" && this.cmbTipo.SelectedItem != null)
            {
                this.btnOK.Enabled = false;
                this.txtOperations.Text = "";
                this.txtOperations.SelectionLength = 0;
                this.txtOperations.SelectionStart = this.txtOperations.Text.Length;
                this.txtOperations.ScrollToCaret();
                this.txtOperations.Refresh();
                Application.DoEvents();
                try
                {
                    this.Esegui();
                }
                catch (Exception)
                {
                }
                this.btnOK.Enabled = true;
            }
        }

        public delegate void delegateAddLog(string message, Exception errore = null);
        public void AddLog(string message, Exception errore = null)
        {
            if (this.InvokeRequired)
            {
                delegateAddLog delegateV = new delegateAddLog(this.AddLog);
                this.Invoke(delegateV, message, errore);
            }
            else
            {
                this.txtOperations.Text += "\r\n" + message;
                if (errore != null)
                {
                    this.txtOperations.Text += "\r\n" + errore.Message;
                }
                this.txtOperations.SelectionLength = 0;
                this.txtOperations.SelectionStart = this.txtOperations.Text.Length;
                this.txtOperations.ScrollToCaret();
                this.txtOperations.Refresh();
                Application.DoEvents();
            }
        }

        public void Esegui()
        {
            AddLog("Login...");
            ResultBaseToken resultToken = ApiRiepiloghi.ServiceRiepiloghi.GetToken(this.txtLogin.Text, this.txtPsw.Text, frmWinAppRiepiloghi.Utility.GetEnvironment());
            if (resultToken != null && resultToken.StatusOK)
            {
                AddLog("Login OK ! Account...");
                ResultBaseAccount resultAccount = ApiRiepiloghi.ServiceRiepiloghi.GetAccount(resultToken.Token, "", frmWinAppRiepiloghi.Utility.GetEnvironment());
                if (resultAccount != null && resultAccount.StatusOK)
                {
                    AddLog("Account OK ! Token CODICE SISTEMA...");
                    resultToken = ApiRiepiloghi.ServiceRiepiloghi.GetTokenCodiceSistema(resultToken.Token, resultAccount.Account.CodiciSistema[0].CodiceSistema,  frmWinAppRiepiloghi.Utility.GetEnvironment());
                    if (resultToken != null && resultToken.StatusOK)
                    {
                        AddLog("Token CODICE SISTEMA OK !");
                        PayLoadRiepiloghi parPayload = new PayLoadRiepiloghi()
                        {
                            Token = resultToken.Token,
                            DataRiepilogo = this.dtGiorno.Value.Date,
                            TipoRiepilogo = this.cmbTipo.SelectedItem.ToString()
                        };
                        GeneraRiepilogo(parPayload);
                    }
                    else
                        if (resultToken != null) 
                            AddLog("errore", new Exception(resultToken.StatusMessage));
                        else
                            AddLog("errore", new Exception("Errore token codice sistema"));
                }
                else
                    if (resultToken != null) 
                        AddLog("errore", new Exception(resultAccount.StatusMessage));
                    else
                        AddLog("errore", new Exception("Errore account"));
            }
            else
                if (resultToken != null) 
                    AddLog("errore", new Exception(resultToken.StatusMessage));
                else
                    AddLog("errore", new Exception("Errore login"));
        }

        public ResponseRiepiloghi GeneraRiepilogo(PayLoadRiepiloghi parPayload)
        {
            ResponseRiepiloghi result = null;
            var rb = CreaRiepilogo(parPayload.Token, parPayload.DataRiepilogo, parPayload.TipoRiepilogo, frmWinAppRiepiloghi.Utility.GetEnvironment());
            result = Utility.GetResponse(rb);
            return result;
        }


        public  ResultBaseGenerazioneRiepilogo CreaRiepilogo(string Token, DateTime Giorno, string GiornalieroMensile, clsEnvironment oEnvironment)
        {
            AddLog("Avvio generazione riepilogo...");
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            string cFileLogFirmato = "";
            string cFileRiepilogoFirmato = "";
            string cFileEmailFirmato = "";
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            //string PathXslt = HttpContext.Current.Server.MapPath("RisorseWeb");
            string pathRisorse = oEnvironment.PathRisorseWeb;
            Int64 Progressivo = 0;

            Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
            percorsi.Risorse = oEnvironment.PathRisorseWeb;
            percorsi.Temp = oEnvironment.PathTemp;
            percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.clsRiepilogoGeneratoWebApi dettaglioRiepilogo = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                AddLog("Connessione interna...");
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError != null) AddLog("errore", oError);


                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);
                    AddLog(string.Format("{0}", dSysdate.ToLongDateString()));
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreaRiepilogo", Token, new Dictionary<string, object>() { { "Giorno", Giorno }, { "GiornalieroMensile", GiornalieroMensile } });

                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_CREAZIONE", "ok");
                    AddLog("Connessione interna...", oError);
                    oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                    if (oError != null) AddLog("errore", oError);
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                    if (oError == null && GiornalieroMensile == "G")
                    {
                        AddLog("Check RPG...", oError);
                        bool lGeneraRPGGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RPG_FUTURO", "DISABILITATO") == "ABILITATO";
                        if (!lGeneraRPGGiorniFuturi)
                        {
                            try
                            {
                                DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
                                if (Giorno.Date > dSysdateCodiceSistema.Date)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo giornaliero", string.Format("Impossibile Generare il Riepilogo Giornaliero di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    if (oError == null && GiornalieroMensile == "R")
                    {
                        AddLog("Check RCA...", oError);
                        bool lGeneraRCAGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RCA_FUTURO", "DISABILITATO") == "ABILITATO";

                        if (!lGeneraRCAGiorniFuturi)
                        {
                            try
                            {
                                DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
                                if (Giorno.Date > dSysdateCodiceSistema.Date)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo controllo accessi", string.Format("Impossibile Generare il Riepilogo Controllo Accessi di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    if (oError != null) AddLog("errore", oError);

                    if (oError == null)
                    {
                        AddLog("Check Server fiscale...", oError);
                        bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                        if (lCheckServerFiscale)
                        {
                            if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                            }
                        }
                    }

                    if (oError != null) AddLog("errore", oError);

                    if (oError == null)
                    {
                        if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                        {
                            AddLog("Check Multicinema...", oError);
                            oError = new Exception("Operazione disponibile solo per il titolare del sistema");
                        }
                    }

                    if (oError != null) AddLog("errore", oError);

                    if (oError == null)
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        Riepiloghi.clsWinServiceClientOperation oClientOperation = new Riepiloghi.clsWinServiceClientOperation(Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);
                        long IdOperazione = 0;

                        Riepiloghi.clsWinServiceConfig config = Riepiloghi.clsWinServiceConfig.GetWinServiceConfig(oConnection, out oError, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);

                        lRet = oError == null;

                        if (oError != null) AddLog("errore", oError);

                        if (lRet)
                        {
                            AddLog("Avvio generazione...", oError);
                            Riepiloghi.clsRiepilogoGenerato riepilogoGenerato = null;
                            oClientOperation.MessaggesWinServiceClientOperation += (m, e) =>
                            {
                                this.AddLog(m, e);
                            };
                            lRet = oClientOperation.PushOperazione(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.AccountId, percorsi, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, GiornalieroMensile, Giorno, 0, out IdOperazione, out riepilogoGenerato, out oError);

                            if (oError != null) AddLog("errore", oError);

                            if (lRet && oError == null)
                            {
                                dettaglioRiepilogo = new Riepiloghi.clsRiepilogoGeneratoWebApi(riepilogoGenerato);

                                // TOLGO IL PERCORSO DAL NOME DEL FILE DI RIEPILOGO
                                try
                                {
                                    if (dettaglioRiepilogo.Nome != null && !string.IsNullOrEmpty(dettaglioRiepilogo.Nome) && !string.IsNullOrWhiteSpace(dettaglioRiepilogo.Nome))
                                    {
                                        System.IO.FileInfo oNomeInfo = new System.IO.FileInfo(dettaglioRiepilogo.Nome);
                                        dettaglioRiepilogo.Nome = oNomeInfo.Name;
                                    }
                                }
                                catch (Exception)
                                {
                                }
                                string cDesc = string.Format("Generazione Riepilogo {0} {1}", (GiornalieroMensile == "G" ? "Giornaliero" : (GiornalieroMensile == "M" ? "Mensile" : "Controllo accessi")), (config.ComunicationMode == Riepiloghi.clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_DIRECT) ? "terminata" : "avviata");
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, cDesc, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);

                                Exception exLista = null;
                                List<Riepiloghi.clsRiepilogoGenerato> lista = Riepiloghi.clsRiepilogoGenerato.GetRigaRiepiloghiProgs(oConnection, out exLista, "COMPETENZA", GiornalieroMensile, Giorno, Giorno, 0);
                                if (exLista == null && lista != null)
                                {
                                    oResult.ListaRiepiloghi = Riepiloghi.clsRiepilogoGeneratoWebApi.GetList(lista);
                                }

                                exLista = null;
                                oResult.EmailToSendOrReceive = Riepiloghi.clsRiepiloghi.CheckRiepiloghiEmailPending(oConnection, true, true, out exLista);
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                lRet = false;
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                                oResult.Sysdate = dSysdate;
                            }
                            if (oError != null) AddLog("errore", oError);
                        }
                        else
                        {
                            lRet = false;
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                            oResult.Sysdate = dSysdate;
                            if (oError != null) AddLog("errore", oError);
                        }
                    }
                    else
                    {
                        lRet = false;
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                        oResult.Sysdate = dSysdate;
                        if (oError != null) AddLog("errore", oError);
                    }
                }
                else
                {
                    lRet = false;
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                    oResult.Sysdate = dSysdate;
                    if (oError != null) AddLog("errore", oError);
                }
            }
            catch (Exception ex)
            {
                lRet = false;
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                oResult.Sysdate = dSysdate;
                if (oError != null) AddLog("errore", oError);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }




        public static class Utility
        {
            [System.Diagnostics.DebuggerStepThrough]
            public static clsEnvironment GetEnvironment()
            {
                System.IO.FileInfo fInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().FullName);
                var result = new clsEnvironment()
                {
                    InternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["internalConnectionString"].ConnectionString,
                    ExternalConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["externalConnectionString"].ConnectionString,
                    PathFont = System.Configuration.ConfigurationManager.AppSettings["pathFont"],
                    PathRisorseWeb = System.Configuration.ConfigurationManager.AppSettings["pathRisorseWeb"],
                    PathTemp = fInfo.Directory.FullName,
                    PathWriteRiepiloghi = System.Configuration.ConfigurationManager.AppSettings["pathWrite"]
                };
                return result;
            }

            public static ResponseRiepiloghi CheckWrite()
            {
                ResponseRiepiloghi result = new ResponseRiepiloghi();
                result.Status = new ApiRiepiloghi.ResultBase();
                result.Status.StatusCode = 0;
                string message = "";
                try
                {
                    System.IO.File.WriteAllText(GetEnvironment().PathTemp + @"\provaTemp.txt", "prova");
                    System.IO.File.Delete(GetEnvironment().PathTemp + @"\provaTemp.txt");
                }
                catch (Exception ex)
                {
                    message = (message.Trim() == "" ? "" : "\r\n") + "PathTemp ERROR:" + ex.Message;
                }

                try
                {
                    System.IO.File.WriteAllText(GetEnvironment().PathWriteRiepiloghi + @"\provaWrite.txt", "prova");
                    System.IO.File.Delete(GetEnvironment().PathWriteRiepiloghi + @"\provaWrite.txt");
                }
                catch (Exception ex)
                {
                    message = (message.Trim() == "" ? "" : "\r\n") + "PathWriteRiepiloghi ERROR:" + ex.Message;
                }

                result.Status.StatusMessage = message;
                result.Status.StatusOK = result.Status.StatusMessage == "";
                return result;
            }

            public static ResponseRiepiloghi GetResponse(Object parResponse)
            {
                if (parResponse == null)
                {
                    return new ResponseRiepiloghi()
                    {
                        Status = {
                        StatusOK = false,
                        StatusMessage = "parResponse = null"
                    }
                    };
                }

                var result = new ResponseRiepiloghi()
                {
                    Data = JsonConvert.DeserializeObject<ResponseDataRiepiloghi>(JsonConvert.SerializeObject(parResponse)),
                    Status = JsonConvert.DeserializeObject<ApiRiepiloghi.ResultBase>(JsonConvert.SerializeObject(parResponse))
                };

                return result;
            }
        }
    }

    

}
