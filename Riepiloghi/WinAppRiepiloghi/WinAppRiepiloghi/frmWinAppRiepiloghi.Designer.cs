﻿
namespace WinAppRiepiloghi
{
    partial class frmWinAppRiepiloghi
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWinAppRiepiloghi));
            this.pnlParametri = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.pnlPsw = new System.Windows.Forms.Panel();
            this.txtPsw = new System.Windows.Forms.TextBox();
            this.lblPsw = new System.Windows.Forms.Label();
            this.pnlTipo = new System.Windows.Forms.Panel();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.lblTipo = new System.Windows.Forms.Label();
            this.pnlGiorno = new System.Windows.Forms.Panel();
            this.dtGiorno = new System.Windows.Forms.DateTimePicker();
            this.lblGiorno = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtOperations = new System.Windows.Forms.TextBox();
            this.pnlParametri.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.pnlPsw.SuspendLayout();
            this.pnlTipo.SuspendLayout();
            this.pnlGiorno.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlParametri
            // 
            this.pnlParametri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlParametri.Controls.Add(this.pnlLogin);
            this.pnlParametri.Controls.Add(this.pnlPsw);
            this.pnlParametri.Controls.Add(this.pnlTipo);
            this.pnlParametri.Controls.Add(this.pnlGiorno);
            this.pnlParametri.Controls.Add(this.btnOK);
            this.pnlParametri.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlParametri.Location = new System.Drawing.Point(0, 0);
            this.pnlParametri.Name = "pnlParametri";
            this.pnlParametri.Size = new System.Drawing.Size(626, 136);
            this.pnlParametri.TabIndex = 0;
            // 
            // pnlLogin
            // 
            this.pnlLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogin.Controls.Add(this.txtLogin);
            this.pnlLogin.Controls.Add(this.lblLogin);
            this.pnlLogin.Location = new System.Drawing.Point(3, 3);
            this.pnlLogin.MaximumSize = new System.Drawing.Size(10000, 38);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Padding = new System.Windows.Forms.Padding(5);
            this.pnlLogin.Size = new System.Drawing.Size(232, 38);
            this.pnlLogin.TabIndex = 6;
            // 
            // txtLogin
            // 
            this.txtLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogin.Location = new System.Drawing.Point(55, 5);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(170, 27);
            this.txtLogin.TabIndex = 1;
            this.txtLogin.Text = "riepiloghi";
            // 
            // lblLogin
            // 
            this.lblLogin.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblLogin.Location = new System.Drawing.Point(5, 5);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(50, 26);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Login:";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPsw
            // 
            this.pnlPsw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPsw.Controls.Add(this.txtPsw);
            this.pnlPsw.Controls.Add(this.lblPsw);
            this.pnlPsw.Location = new System.Drawing.Point(241, 3);
            this.pnlPsw.MaximumSize = new System.Drawing.Size(10000, 38);
            this.pnlPsw.Name = "pnlPsw";
            this.pnlPsw.Padding = new System.Windows.Forms.Padding(5);
            this.pnlPsw.Size = new System.Drawing.Size(232, 38);
            this.pnlPsw.TabIndex = 7;
            // 
            // txtPsw
            // 
            this.txtPsw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPsw.Location = new System.Drawing.Point(76, 5);
            this.txtPsw.Name = "txtPsw";
            this.txtPsw.PasswordChar = '*';
            this.txtPsw.Size = new System.Drawing.Size(149, 27);
            this.txtPsw.TabIndex = 1;
            this.txtPsw.Text = "riepiloghi";
            // 
            // lblPsw
            // 
            this.lblPsw.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPsw.Location = new System.Drawing.Point(5, 5);
            this.lblPsw.Name = "lblPsw";
            this.lblPsw.Size = new System.Drawing.Size(71, 26);
            this.lblPsw.TabIndex = 0;
            this.lblPsw.Text = "Password:";
            this.lblPsw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlTipo
            // 
            this.pnlTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTipo.Controls.Add(this.cmbTipo);
            this.pnlTipo.Controls.Add(this.lblTipo);
            this.pnlTipo.Location = new System.Drawing.Point(3, 47);
            this.pnlTipo.MaximumSize = new System.Drawing.Size(10000, 38);
            this.pnlTipo.Name = "pnlTipo";
            this.pnlTipo.Padding = new System.Windows.Forms.Padding(5);
            this.pnlTipo.Size = new System.Drawing.Size(232, 38);
            this.pnlTipo.TabIndex = 3;
            // 
            // cmbTipo
            // 
            this.cmbTipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Location = new System.Drawing.Point(40, 5);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(185, 27);
            this.cmbTipo.TabIndex = 1;
            // 
            // lblTipo
            // 
            this.lblTipo.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTipo.Location = new System.Drawing.Point(5, 5);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(35, 26);
            this.lblTipo.TabIndex = 0;
            this.lblTipo.Text = "Tipo:";
            this.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlGiorno
            // 
            this.pnlGiorno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGiorno.Controls.Add(this.dtGiorno);
            this.pnlGiorno.Controls.Add(this.lblGiorno);
            this.pnlGiorno.Location = new System.Drawing.Point(241, 47);
            this.pnlGiorno.MaximumSize = new System.Drawing.Size(10000, 38);
            this.pnlGiorno.Name = "pnlGiorno";
            this.pnlGiorno.Padding = new System.Windows.Forms.Padding(5);
            this.pnlGiorno.Size = new System.Drawing.Size(327, 38);
            this.pnlGiorno.TabIndex = 4;
            // 
            // dtGiorno
            // 
            this.dtGiorno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGiorno.Location = new System.Drawing.Point(40, 5);
            this.dtGiorno.Name = "dtGiorno";
            this.dtGiorno.Size = new System.Drawing.Size(280, 27);
            this.dtGiorno.TabIndex = 1;
            this.dtGiorno.Value = new System.DateTime(2022, 11, 11, 0, 0, 0, 0);
            // 
            // lblGiorno
            // 
            this.lblGiorno.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGiorno.Location = new System.Drawing.Point(5, 5);
            this.lblGiorno.Name = "lblGiorno";
            this.lblGiorno.Size = new System.Drawing.Size(35, 26);
            this.lblGiorno.TabIndex = 0;
            this.lblGiorno.Text = "Tipo:";
            this.lblGiorno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(3, 91);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 38);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "Genera";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // txtOperations
            // 
            this.txtOperations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOperations.Location = new System.Drawing.Point(0, 136);
            this.txtOperations.Multiline = true;
            this.txtOperations.Name = "txtOperations";
            this.txtOperations.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOperations.Size = new System.Drawing.Size(626, 311);
            this.txtOperations.TabIndex = 1;
            // 
            // frmWinAppRiepiloghi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(626, 447);
            this.Controls.Add(this.txtOperations);
            this.Controls.Add(this.pnlParametri);
            this.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmWinAppRiepiloghi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App Riepiloghi";
            this.pnlParametri.ResumeLayout(false);
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.pnlPsw.ResumeLayout(false);
            this.pnlPsw.PerformLayout();
            this.pnlTipo.ResumeLayout(false);
            this.pnlGiorno.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel pnlParametri;
        private System.Windows.Forms.Panel pnlTipo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.Panel pnlGiorno;
        private System.Windows.Forms.DateTimePicker dtGiorno;
        private System.Windows.Forms.Label lblGiorno;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Panel pnlPsw;
        private System.Windows.Forms.TextBox txtPsw;
        private System.Windows.Forms.Label lblPsw;
        private System.Windows.Forms.TextBox txtOperations;
    }
}

