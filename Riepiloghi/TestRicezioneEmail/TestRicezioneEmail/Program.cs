﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRicezioneEmail
{
    class Program
    {
        static void Main(string[] args)
        {
            Exception error = null;
            RiceviEmail("hollywood.gela@creaweb.it", "hollywood.gela@creaweb.it", "gela", "pop.telnetwork.it", 110, false, null, "00073443", out error);
        }

        public static List<object> RiceviEmail(string accountEmail, string user, string password, string pop3Server, int port, bool userSsl, object LocalInboxProvider, string codiceSistema, out Exception error)
        {
            error = null;
            MailLib.IUidsLocalInboxProvider LocalInboxProviderObj = null;
            try
            {
                if (LocalInboxProvider != null)
                    LocalInboxProviderObj = (MailLib.IUidsLocalInboxProvider)LocalInboxProvider;
            }
            catch (Exception)
            {
            }

            List<object> result = null;
            try
            {
                result = MailLib.Mail.readMessagesPop3(accountEmail, user, password, pop3Server, port, userSsl, LocalInboxProviderObj, codiceSistema, out error);
            }
            catch (Exception ex)
            {
                error = new Exception(string.Format("Errore spedizione email {0}", ex.ToString()));
            }
            return result;
        }
    }
}
