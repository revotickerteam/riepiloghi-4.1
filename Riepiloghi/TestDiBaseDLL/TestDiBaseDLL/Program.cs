﻿using Riepiloghi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintic.Data;

namespace TestDiBaseDLL
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args != null)
            {
                foreach (string arg in args)
                {
                    if (arg == "GETSN")
                        Test01();
                    else if (arg == "EMAIL_RECEIVE")
                        TestReceiveEmail();
                }
            }

            //Exception error = null;

            //error = null;
            //string SN = libRiepiloghiBase.clsLibSigillo.GetSerialNumber(0, out error);
            //Console.WriteLine(string.Format("Serial number : {0}", SN));
            //if (error != null)
            //{
            //    Console.WriteLine(error.ToString());
            //}

            //error = null;
            //string file = @"C:\Users\Administrator\Documents\Visual Studio 2017\Projects\Riepiloghi\Riepiloghi\TestDiBaseDLL\TestDiBaseDLL\bin\Debug\file_di_prova_da_firmare.txt";

            //if (libRiepiloghiBase.clsLibSigillo.FirmaFile("802828", 0, file, out error))
            //{
            //    Console.WriteLine("firma OK");
            //}
            //if (error != null)
            //{
            //    Console.WriteLine(error.ToString());
            //}
        }

        private static void Test01()
        {
            System.Reflection.Assembly oAssembly = null;
            try
            {
                Exception errGetInstance = null;
                List<string> operations = new List<string>();
                libRiepiloghiBase.iLib_SIAE_Provider oGetSerialNumber = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out errGetInstance, out operations, out oAssembly);
                foreach (string operation in operations)
                {
                    Console.WriteLine(operation);
                }
                if (oGetSerialNumber != null && errGetInstance == null)
                {
                    Exception errorGetSn = null;
                    string sn = oGetSerialNumber.GetSerialNumber(0, out errorGetSn);
                    Console.Write("sn " + sn);
                    if (errorGetSn != null)
                    {
                        
                        Console.WriteLine("errorGetSn \r\n" + errorGetSn.ToString());
                    }
                }
                else
                {
                    if (errGetInstance != null)
                    {
                        Console.WriteLine("errGetInstance \r\n" + errGetInstance.ToString());
                    }
                }
            }
            catch (Exception exInit)
            {
                Console.WriteLine("exInit \r\n" + exInit.ToString());
            }

            if (oAssembly != null)
            {
                // dovrei scaricarlo
            }
        }

        private static void TestReceiveEmail()
        {
            DateTime dStart = DateTime.Now;

            Exception oError = null;
            DateTime dSysdate = DateTime.MinValue;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Reflection.Assembly oAssembly = null;

            try
            {
                clsEnvironment oEnvironment = new clsEnvironment();
                oConnectionInternal = GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oConnection = GetConnection(oConnectionInternal, out oError, oEnvironment);
                    if (oError != null)
                        Console.WriteLine(string.Format("{0}\r\n{1}", "errore connessione", oError.Message));
                }
                else
                    Console.WriteLine(string.Format("{0}\r\n{1}", "errore connessione interna", oError.Message));

                if (oError == null)
                {
                    
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);
                    List<string> operations = new List<string>();
                    libRiepiloghiBase.iLib_SIAE_Provider oProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError, out operations, out oAssembly);
                    if (oError == null)
                    {
                        bool lUseDefaultCredentials = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_USE_DEFAULT_CREDENTIALS", "ABILITATO") == "ABILITATO";
                        bool lUseSSL = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_USE_SSL", "DISABILITATO") == "ABILITATO";
                        string SmtpUser = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_USE_USER", "DISABILITATO");
                        string SmptPassword = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_USE_PASSWORD", "DISABILITATO");

                        string Pop3Server = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_IN", "");
                        int Pop3Port = int.Parse(clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_PORT_IN", "0"));
                        string Pop3Password = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_IN_PASSWORD", "EMAIL_IN_PASSWORD");
                        string SendCC = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_CC_RIEPILOGHI", "");
                        string SendBCC = clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "EMAIL_BCC_RIEPILOGHI", "");

                        string codiceSistema = GetCodiceSistema(oConnection, out oError);
                        string emailSistema = "";
                        if (oError == null)
                        {
                            emailSistema = GetEmailCodiceSistema(oConnection, out oError);
                            List<object> receiveMessages = oProvider.RiceviEmail(emailSistema, emailSistema, Pop3Password, Pop3Server, Pop3Port, lUseSSL, null, codiceSistema, out oError);
                            if (receiveMessages != null && oError == null)
                            {
                                foreach (object itemReceived in receiveMessages)
                                {
                                    String itemSerializeObject = Newtonsoft.Json.JsonConvert.SerializeObject(itemReceived);
                                    emailRiepiloghiResponse item = Newtonsoft.Json.JsonConvert.DeserializeObject<emailRiepiloghiResponse>(itemSerializeObject);
                                }
                            }
                            else
                                Console.WriteLine(string.Format("{0}\r\n{1}", "errore ricezione email", oError.Message));
                        }
                    }
                    else
                        Console.WriteLine(string.Format("{0}\r\n{1}", "errore istanza lib siae", oError.Message));
                }
                else
                {
                    Console.WriteLine(string.Format("{0}\r\n{1}", "errore connessione interna", oError.Message));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore spedizione/ricezione lista riepiloghi", "Errore imprevisto.", ex);
                Console.WriteLine(string.Format("{0}\r\n{1}", "errore connessione interna", oError.Message));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
                if (oAssembly != null)
                {
                    // dovrei scaricarlo
                }
            }
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnectionInternal(out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                oCon.ConnectionString = oEnvironment.InternalConnectionString;
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
            }
            return oCon;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnection(IConnection ConnectionInternal, out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA");

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione." + exGetConnection.ToString(), exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    //oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.ConnectionString = string.Format(oEnvironment.ExternalConnectionString, ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
                }
            }
            return oCon;
        }

        public static string GetCodiceSistema(IConnection oConnection, out Exception oError)
        {
            string cRet = "";
            oError = null;
            try
            {
                IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT (F_GET_CODICE_SISTEMA()) AS CODICE_SISTEMA FROM DUAL");
                if (!oRS.EOF && !oRS.Fields("codice_sistema").IsNull && oRS.Fields("codice_sistema").Value.ToString() != "")
                {
                    cRet = oRS.Fields("codice_sistema").Value.ToString();
                }
                else
                {
                    oError = clsRiepiloghi.GetNewException("Lettura codice sistema", "Nessun codice sistema valido codificato.");
                }
                oRS.Close();
            }
            catch (Exception ex)
            {
                oError = clsRiepiloghi.GetNewException("Lettura codice sistema", "Errore imprevisto durante la lettura del codice sistema", ex);
            }
            return cRet;
        }

        public static string GetEmailCodiceSistema(IConnection oConnection, out Exception oError)
        {
            string cRet = "";
            oError = null;
            try
            {
                IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT CODICE_SISTEMA, EMAIL_TITOLARE FROM VR_CODICE_SISTEMA WHERE CODICE_SISTEMA = F_GET_CODICE_SISTEMA()");
                if (!oRS.EOF && !oRS.Fields("codice_sistema").IsNull && oRS.Fields("codice_sistema").Value.ToString() != ""
                             && !oRS.Fields("email_titolare").IsNull && oRS.Fields("email_titolare").Value.ToString() != "")
                {
                    cRet = oRS.Fields("email_titolare").Value.ToString();
                }
                else
                {
                    oError = clsRiepiloghi.GetNewException("Lettura email codice sistema", "Nessuna email codice sistema valido codificata.");
                }
                oRS.Close();
            }
            catch (Exception ex)
            {
                oError = clsRiepiloghi.GetNewException("Lettura email codice sistema", "Errore imprevisto durante la lettura dell'email codice sistema", ex);
            }
            return cRet;
        }
    }
}
