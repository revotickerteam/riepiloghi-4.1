﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDiBaseDLL
{
    class classiBase
    {
    }
    public class clsEnvironment
    {
        public string InternalConnectionString { get; set; }
        public string ExternalConnectionString { get; set; }
        public string PathRisorseWeb { get; set; }
        public string PathFont { get; set; }
        public string PathTemp { get; set; }
        public string PathWriteRiepiloghi { get; set; }
    }

    public class emailRiepiloghiResponse
    {
        public int MessageIndex { get; set; }
        public object MessageUid { get; set; }
        public DateTime MessageDate { get; set; }
        public string FileNameRiepilogo { get; set; }
        public string Tipo { get; set; }
        public DateTime Giorno { get; set; }
        public int Progressivo { get; set; }
        public string Body { get; set; }
        public int ErrorCode { get; set; }
    }
}
