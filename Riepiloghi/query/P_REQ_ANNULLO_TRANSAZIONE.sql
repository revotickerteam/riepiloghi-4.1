CREATE OR REPLACE PROCEDURE WTIC.P_REQ_ANNULLO_TRANSAZIONE(cCodiceCarta VARCHAR2 := null, nProgressivo IN NUMBER := null, cSigillo IN VARCHAR2 := null, logCUR OUT SYS_REFCURSOR, errCUR OUT SYS_REFCURSOR, cauCUR OUT SYS_REFCURSOR)
IS
    nErrorCode NUMBER;
    nWarningDataOraEvento NUMBER;
    nWarningAccesso NUMBER;
    oRow WTIC.OBJ_ROW_LOG_BASE;
    v_tab WTIC.OBJ_TABLE_LOG_BASE;
    v_tab_annullo WTIC.OBJ_TABLE_LOG_BASE;
    cProprietaValore VARCHAR2(100);
    cTipoTurno VARCHAR2(1);
    nIdTex NUMBER;
    nContaRateiAttivi NUMBER;
    v_tab_ratei WTIC.OBJ_TABLE_LOG_BASE;
    nRiga NUMBER;
    dDataEvento DATE;
BEGIN
    nErrorCode := 0;
    nWarningDataOraEvento := 0;
    nWarningAccesso := 0;

    /* VALORIZZO SEMPRE IL CURSORE DI ERRORE CON NESSUNA RIGA */
    OPEN errCUR FOR SELECT * FROM (SELECT 0 AS CODICE, '' AS LIVELLO, '' AS DESCRIZIONE FROM DUAL) WHERE CODICE = -1;

    /* VALORIZZO SEMPRE IL CURSORE DELLE CAUSALI CON NESSUNA RIGA */
    OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE CODICE IS NULL;

    /* LA FUNZIONE F_TABLE_LOG_CARTA_PROG_SIGILLO ritorna sempre v_tab ANCHE SE CON NESSUNA RIGA */
    v_tab := WTIC.F_TABLE_LOG_CARTA_PROG_SIGILLO(cCodiceCarta, nProgressivo, cSigillo);
    OPEN logCUR FOR SELECT * FROM TABLE(v_tab);

    IF (v_tab IS NULL OR v_tab.LAST IS NULL OR v_tab.LAST = 0) THEN

        nErrorCode := 1;    /*Transazione non trovata. */

    END IF;

    IF (nErrorCode = 0 AND v_tab IS NOT NULL AND v_tab.LAST IS NOT NULL AND v_tab.LAST > 0) THEN

        oRow := v_tab(v_tab.LAST);

        /* VERIFICO SUBITO SE SI TRATTA DI UNA TRANSAZIONE DI ANNULLO */
        IF (nErrorCode = 0 AND oRow.ANNULLATO = 'A') THEN

            nErrorCode := 2; /* ERRORE BLOCCANTE : Transazione di annullo. */

        END IF;

        /* VERIFICO SE LA TRANSAZIONE E' GIA' STATA ANNULLATA */
        IF (nErrorCode = 0) THEN

            v_tab_annullo := F_TABLE_LOG_CARTA_PROG_ANNULLO(oRow.CODICE_CARTA, oRow.PROGRESSIVO_TITOLO);

            IF (v_tab_annullo IS NOT NULL AND v_tab_annullo.LAST IS NOT NULL AND v_tab_annullo.LAST > 0) THEN

                nErrorCode := 3;    /* ERRORE BLOCCANTE : Transazione annullata precedentemente. */

            END IF;

        END IF;

        /* SE SI TRATTA DI UN BIGLIETTO RATEO */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'T' AND oRow.TITOLO_IVA_PREASSOLTA = 'B') THEN

            IF (nErrorCode = 0 AND (oRow.CODICE_BIGLIETTO_ABBONAMENTO IS NULL OR LENGTH(TRIM(oRow.CODICE_BIGLIETTO_ABBONAMENTO)) <> 8)) THEN

                nErrorCode := 201; /* ERRORE BLOCCANTE : Codice biglietto abbonamento non valido. */

            END IF;

            IF (nErrorCode = 0 AND (oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO IS NULL OR oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO <= 0)) THEN

                nErrorCode := 202; /* ERRORE BLOCCANTE : Num Prog biglietto abbonamento non valido. */

            END IF;

            /* RICERCO IL TIPO DI TURNO SULL'ABBONAMENTO PRADRE */
            IF (nErrorCode = 0) THEN

                cTipoTurno := NULL;

                SELECT MAX(TIPO_TURNO) INTO cTipoTurno FROM VR_LOGTRANS WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABBONAMENTO = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;

                IF (cTipoTurno IS NULL) THEN

                    SELECT MAX(TIPO_TURNO) INTO cTipoTurno FROM VR_LOGTRANS_ARCHIVE WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABBONAMENTO = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;

                END IF;

                IF (cTipoTurno IS NULL) THEN

                    SELECT MAX(TIPO_TURNO) INTO cTipoTurno FROM VR_SMART_TESSERE WHERE CODICE_ABBONAMENTO = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND NUM_PROG_ABB = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;

                END IF;

                IF (cTipoTurno IS NULL) THEN

                    nErrorCode := 203; /* ERRORE BLOCCANTE : impossibile trovare abbonamento padre. */

                END IF;

                IF (nErrorCode = 0) THEN

                    IF (cTipoTurno = 'F') THEN

                        nErrorCode := 204; /* ERRORE BLOCCANTE : abbonamento padre turno fisso, annullare abbonamento. */

                    END IF;

                END IF;

                IF (nErrorCode = 0) THEN

                    SELECT MAX(IDTEX) INTO nIdTex FROM VR_TESSERE WHERE DOTAZIONEF = oRow.CODICE_BIGLIETTO_ABBONAMENTO AND IDTEX = oRow.NUM_PROG_BIGLIETTO_ABBONAMENTO;

                    IF (nIdTex IS NULL) THEN

                        nErrorCode := 205; /* ERRORE BLOCCANTE : impossibile trovare la tessera padre. */

                    END IF;

                END IF;

                IF (nErrorCode = 0) THEN

                    SELECT VALORE INTO cProprietaValore FROM WTIC.PROPRIETA_RIEPILOGHI WHERE PROPRIETA = 'ANNULLO_RATEI';

                    IF (cProprietaValore IS NULL OR cProprietaValore <> 'ABILITATO') THEN

                        nErrorCode := 206;    /* ERRORE BLOCCANTE : Annullo rateo non consentito come da configurazione. */

                    END IF;

                END IF;

            END IF;

        END IF;


        /* SE SI TRATTA DI UN BIGLIETTO DI QUALSIASI NATURA VERIFICO I WARNING E DETERMINO LE POSSIBILI CAUSALI DI ANNULLO */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'T') THEN

            IF (oRow.DIGITALE_TRADIZIONALE = 'T') THEN

                /* CONTROLLO SE SONO PASSATI PIU' DI 5 GIORNI LAVORATIVI DALLA DATA EVENTO WARNING NON BLOCCANTE */
                IF (WTIC.F_GET_GIORNI_LAVORATIVI(oRow.DATA_ORA_EVENTO) > 5) THEN

                    nWarningDataOraEvento := 101; /* WARNING: Annullo abbonamento turno fisso TRADIZIONALE, data evento 1� rateo oltre 5 giorni lavorativi. */

                END IF;

            ELSE

                IF (dDataEvento < SYSDATE) THEN

                    nWarningDataOraEvento := 102; /* WARNING: Annullo abbonamento turno fisso DIGITALE, data evento 1� rateo passata. */

                END IF;

            END IF;

        END IF;

        /* SE SI TRATTA DI UN ABBONAMENTO */
        IF (nErrorCode = 0 AND oRow.TITOLO_ABBONAMENTO = 'A') THEN

            /* VERIFICO SE SI TRATTA DI UN ABBONAMENTO CIVETTA */
            IF (nErrorCode = 0 AND oRow.CODICE_ABBONAMENTO LIKE '#%') THEN

                nErrorCode := 301;    /* ERRORE BLOCCANTE : Annullo abbonamento civetta non consentito. */

            END IF;

            /* VERIFICO CODICE_ABBONAMENTO */
            IF (nErrorCode = 0 AND (oRow.CODICE_ABBONAMENTO IS NULL OR LENGTH(TRIM(oRow.CODICE_ABBONAMENTO)) <> 8)) THEN

                nErrorCode := 302;    /* ERRORE BLOCCANTE : Codice abbonamento non valido. */

            END IF;

            /* VERIFICO NUM_PROG_ABBONAMENTO */
            IF (nErrorCode = 0 AND (oRow.NUM_PROG_ABBONAMENTO IS NULL OR oRow.NUM_PROG_ABBONAMENTO <= 0)) THEN

                nErrorCode := 303;    /* ERRORE BLOCCANTE : Codice abbonamento non valido. */

            END IF;

            /* RICERCO LA TESSERA */
            IF (nErrorCode = 0) THEN

                SELECT NVL(MAX(IDTEX), 0) INTO nIdTex FROM VR_TESSERE WHERE DOTAZIONEF = oRow.CODICE_ABBONAMENTO AND IDTEX = oRow.NUM_PROG_ABBONAMENTO;

                IF (nIdTex = 0) THEN

                    nErrorCode := 304; /* ERRORE BLOCCANTE : impossibile trovare la tessera. */

                END IF;

            END IF;

            /* VERIFICO SUL TURNO LIBERO SE TUTTI I RATEI SONO ANNULLATI */
            IF (nErrorCode = 0 AND oRow.TIPO_TURNO = 'L') THEN

                nContaRateiAttivi := WTIC.F_NUM_RATEI_ABBONAMENTO_ATTIVI(oRow.CODICE_ABBONAMENTO, oRow.NUM_PROG_ABBONAMENTO);

                IF (nContaRateiAttivi > 0) THEN

                    nErrorCode := 305;  /* ERRORE BLOCCANTE: Annullo abbonamento turno libero, non tutti i rati sono stati annullati. */

                END IF;

            END IF;

            IF (nErrorCode = 0 AND oRow.TIPO_TURNO = 'F') THEN

                dDataEvento := NULL;

                v_tab_ratei := WTIC.F_TABLE_LOG_RATEI_ABBONAMENTO(oRow.CODICE_ABBONAMENTO, oRow.NUM_PROG_ABBONAMENTO);

                IF (v_tab_ratei IS NOT NULL AND v_tab_ratei.LAST IS NOT NULL AND v_tab_ratei.LAST > 0) THEN

                    FOR nRiga IN v_tab_ratei.FIRST .. v_tab_ratei.LAST
                    LOOP

                        IF (v_tab_ratei(nRiga).ANNULLATO = ' ') THEN

                            IF (dDataEvento IS NULL) THEN

                                dDataEvento := v_tab_ratei(nRiga).DATA_ORA_EVENTO;

                            ELSE

                                IF (v_tab_ratei(nRiga).DATA_ORA_EVENTO < dDataEvento) THEN

                                    dDataEvento := v_tab_ratei(nRiga).DATA_ORA_EVENTO;

                                END IF;

                            END IF;

                        END IF;

                    END LOOP;

                    IF (dDataEvento IS NOT NULL) THEN

                        /* SE ABBONAMENTO TRADIZIONALE ANCHE IL PRIMO RATEO SEGUE LA REGOLA DEI 5 GIORNI LAVORATIVI */
                        IF (oRow.DIGITALE_TRADIZIONALE = 'T') THEN

                            /* CONTROLLO SE SONO PASSATI PIU' DI 5 GIORNI LAVORATIVI DALLA DATA EVENTO WARNING NON BLOCCANTE */
                            IF (WTIC.F_GET_GIORNI_LAVORATIVI(dDataEvento) > 5) THEN

                                nWarningDataOraEvento := 307; /* WARNING: Annullo abbonamento turno fisso TRADIZIONALE, data evento 1� rateo oltre 5 giorni lavorativi. */

                            END IF;

                        ELSE

                            IF (dDataEvento < SYSDATE) THEN

                                nWarningDataOraEvento := 308; /* WARNING: Annullo abbonamento turno fisso DIGITALE, data evento 1� rateo passata. */

                            END IF;

                        END IF;


                    ELSE

                        nErrorCode := 306;  /* ERRORE BLOCCANTE: Annullo abbonamento turno fisso, impossibile determinare la data del primo evento usufruibile. */

                    END IF;
                ELSE

                    nErrorCode := 306;  /* ERRORE BLOCCANTE: Annullo abbonamento turno fisso, impossibile determinare la data del primo evento usufruibile. */

                END IF;

            END IF;


        END IF;

    END IF;

    IF (nErrorCode > 0) THEN

        /* VALORIZZO SEMPRE IL CURSORE DI ERRORE CON NESSUNA RIGA */
        OPEN errCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nErrorCode;

    ELSE

        IF (nWarningDataOraEvento > 0 AND nWarningAccesso > 0) THEN

            OPEN errCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nWarningDataOraEvento
                            UNION ALL
                            SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nWarningAccesso;

        END IF;

        IF (nWarningDataOraEvento > 0 AND nWarningAccesso = 0) THEN

            OPEN errCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nWarningDataOraEvento;

        END IF;

        IF (nWarningDataOraEvento = 0 AND nWarningAccesso > 0) THEN

            OPEN errCUR FOR SELECT CODICE, LIVELLO, DESCRIZIONE FROM CODICI_ERRORE_ANNULLO WHERE CODICE = nWarningAccesso;

        END IF;


        /* DETERMINO LE POSSIBILI CAUSALI DI ANNULLO SE BIGLIETTO */
        IF (oRow.TITOLO_ABBONAMENTO = 'T') THEN

            IF (nWarningDataOraEvento > 0 OR nWarningAccesso > 0) THEN

                OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE ENABLED = 1 AND CODICE = '006';

            ELSE

                OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE ENABLED = 1 AND CODICE IN ('001','002','004','005','006','009') ORDER BY CODICE;

            END IF;

        END IF;



        /* DETERMINO LE POSSIBILI CAUSALI DI ANNULLO SE ABBONAMENTO */
        IF (oRow.TITOLO_ABBONAMENTO = 'A') THEN

            IF (nWarningDataOraEvento > 0 OR nWarningAccesso > 0) THEN

                OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE ENABLED = 1 AND CODICE = '006';

            ELSE

                OPEN cauCUR FOR SELECT CODICE, DESCRIZIONE FROM CAUSALI_DI_ANNULLO WHERE ENABLED = 1 AND CODICE IN ('001','002','004','005','006','009') ORDER BY CODICE;

            END IF;

        END IF;

    END IF;
END;