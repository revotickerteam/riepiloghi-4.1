GetProprietaRiepiloghi(string Token, string Proprieta, string DefaultValue)
GetPathRPG
GetPathRPM
GetPathEML
GetPathLOG_INCREMENTALE

GetNomeLogGiornoBase(string cGiorno)
GetNomeLogGiorno(string cGiorno)
GetNomeLogSearchIncrementale(string cGiorno)
GetNomeEmailGiornoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
GetNomeEmailGiorno(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
GetNomeRiepilogoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
GetNomeRiepilogo(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo)


GetParametriServerFiscale(string Token)
GetCodiceSistema(string Token)
GetInfoSmartCard(string Token, string SerialNumber)

CheckServerFiscale(string Token)

GetListaCampiLogTransazioni(string Token)

GetTabella....

F_TABLE_LOG_BASE_DATA
F_TRANSAZIONI_RIEPILOGHI
F_CALC_RIEPILOGHI_DETTAGLIO
F_CALC_RIEPILOGHI
F_CALC_RIEPILOGHI_PRINT
F_RIEPILOGHI_CALC_TITOLI
F_READ_RIEPILOGHI
F_READ_RIEPILOGHI_PRINT
F_GETTABLE_IMPOSTE
F_RIEPILOGHI_GETRIF_OMAGGIO
F_TABLE_CALC_IVAISI
F_TABLE_RIEPILOGHI_DA_GENERARE



F_TABLE_LOG_BASE_DATA(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cTipo IN VARCHAR2, cGiornalieroMensile IN VARCHAR2, cFlagLogStorico IN VARCHAR2) RETURN WTIC.OBJ_TABLE_LOG_BASE

F_RIEPILOGHI_GETRIF_OMAGGIO(cFlagRicercaNeiLog IN VARCHAR2, 
                                                            cCodiceLocale IN VARCHAR2, 
                                                            dDataOraEvento IN DATE, 
							    cTitoloEvento IN VARCHAR2, 
							    cOrdineDiPosto IN VARCHAR2, 
							    cTipoTitolo IN VARCHAR2, 
							    nIdCalend IN NUMBER) RETURN WTIC.OBJ_TABLE_RIF_OMAGGI




F_TABLE_LOG_CARTA_PROG_SIGILLO(cCodiceCarta VARCHAR2, nProgressivo IN NUMBER, cSigillo IN VARCHAR2) RETURN WTIC.OBJ_TABLE_LOG_BASE
F_TABLE_LOG_CARTA_PROG_ANNULLO(cCodiceCarta VARCHAR2, nProgressivo IN NUMBER) RETURN OBJ_TABLE_LOG_BASE


F_TRANSAZIONI_RIEPILOGHI(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cTipoBigliettiAbbonamenti IN VARCHAR2, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2) RETURN  WTIC.OBJ_TABLE_RIEPILOGHI
utilizza F_TABLE_LOG_BASE_DATA

F_CALC_RIEPILOGHI_DETTAGLIO(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2) RETURN WTIC.OBJ_TABLE_RIEPILOGHI
utilizza F_TRANSAZIONI_RIEPILOGHI

F_CALC_RIEPILOGHI(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2) RETURN WTIC.OBJ_TABLE_RIEPILOGHI
utilizza F_CALC_RIEPILOGHI_DETTAGLIO

P_RIEPILOGHI_CREATE(dGiornoMese IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2)
utilizza F_CALC_RIEPILOGHI_DETTAGLIO

F_READ_RIEPILOGHI(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2) RETURN WTIC.OBJ_TABLE_RIEPILOGHI


F_CALC_RIEPILOGHI_PRINT(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2) RETURN WTIC.OBJ_TABLE_RIEPILOGHI
utilizza F_CALC_RIEPILOGHI_DETTAGLIO

F_READ_RIEPILOGHI_PRINT(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2) RETURN WTIC.OBJ_TABLE_RIEPILOGHI
