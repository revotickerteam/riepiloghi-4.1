
CREATE OR REPLACE FUNCTION WTIC.F_PIPPO_FETCH(logCUR IN SYS_REFCURSOR) RETURN WTIC.OBJ_TABLE_LOG_BASE
IS
      v_tab WTIC.OBJ_TABLE_LOG_BASE;
      v_tab_TEMP WTIC.OBJ_TABLE_LOG_BASE;
      rowCUR OBJ_ROW_LOG_BASE;
      recordCUR TR_LOGTRANS_ROWTYPE%rowtype;

      oTableIMPOSTE  WTIC.OBJ_TABLE_IMPOSTE;

      lResetVarEven BOOLEAN;

      c_CODICE_SISTEMA VARCHAR2(8);
      c_CODICE_FISCALE_TITOLARE VARCHAR2(16);
      c_CODICE_FISCALE_ORGANIZZATORE VARCHAR2(16);
      c_DATA_EVENTO VARCHAR2(8);
      c_CODICE_LOCALE VARCHAR2(13);
      c_ORA_EVENTO VARCHAR2(4);
      c_TIPO_EVENTO VARCHAR2(2);
      c_SPETTACOLO_INTRATTENIMENTO VARCHAR2(1);
      n_IDCALEND NUMBER;

      d_DATA_ORA DATE;
      d_DATA_VALIDITA DATE;

      /* variabili temporanee per calcoli IVA, IMPOSTA INTRATTENIMENTO, IMPONIBILE INTRATTENIMENTO */
      nPerc_IVA NUMBER;
      nPerc_INTRATTENIMENTO NUMBER;
      nIncidenza_INTRATTENIMENTO NUMBER;

      /* variabili prese dal log che non vengono ricalcolate */
      n_PREZZO NUMBER;
      n_PREVENDITA NUMBER;

      /* variabili calcolate da salvare nel risultato */
      n_NETTO_TITOLO NUMBER;
      n_NETTO_PREVENDITA NUMBER;
      n_IMPONIBILE_IMPOSTA_INTRA NUMBER;
      n_IVA_TITOLO NUMBER;
      n_IVA_PREVENDITA NUMBER;
      n_ISI_TOTALE NUMBER;
      c_FLAG_IRRIPARTIBILE VARCHAR2(1);

      nTEMP_PercIvaSuIntra NUMBER;
      nTEMP_PercIsiSuIntra NUMBER;


      nTEMP_PercIvaAbbonamento NUMBER;
      nTEMP_PercIsiAbbonamento NUMBER;

BEGIN

      lResetVarEven := FALSE;

      c_CODICE_SISTEMA := NULL;
      c_CODICE_FISCALE_TITOLARE := NULL;
      c_CODICE_FISCALE_ORGANIZZATORE := NULL;
      c_DATA_EVENTO := NULL;
      c_CODICE_LOCALE := NULL;
      c_ORA_EVENTO := NULL;
      c_TIPO_EVENTO := NULL;
      c_SPETTACOLO_INTRATTENIMENTO := NULL;
      n_IDCALEND := NULL;
      d_DATA_ORA := NULL;
      d_DATA_VALIDITA := NULL;
      nPerc_IVA := NULL;
      nPerc_INTRATTENIMENTO := NULL;
      nIncidenza_INTRATTENIMENTO := NULL;
      n_PREZZO := NULL;
      n_PREVENDITA := NULL;
      n_NETTO_TITOLO := NULL;
      n_NETTO_PREVENDITA := NULL;
      n_IMPONIBILE_IMPOSTA_INTRA := NULL;
      n_IVA_TITOLO := NULL;
      n_IVA_PREVENDITA := NULL;
      n_ISI_TOTALE := NULL;
      c_FLAG_IRRIPARTIBILE := NULL;

      oTableIMPOSTE := WTIC.F_GETTABLE_IMPOSTE();

      v_tab_TEMP := WTIC.OBJ_TABLE_LOG_BASE();
      LOOP
            FETCH logCUR into recordCUR;
            EXIT WHEN logCUR%NOTFOUND;
            rowCUR := OBJ_ROW_LOG_BASE(recordCUR.LOG_ID                          ,
                                       recordCUR.LOG_DATE                        ,
                                       recordCUR.IDVEND                          ,
                                       recordCUR.CODICE_FISCALE_ORGANIZZATORE    ,
                                       recordCUR.CODICE_FISCALE_TITOLARE         ,
                                       recordCUR.TITOLO_ABBONAMENTO              ,
                                       recordCUR.TITOLO_IVA_PREASSOLTA           ,
                                       recordCUR.SPETTACOLO_INTRATTENIMENTO      ,
                                       recordCUR.VALUTA                          ,
                                       recordCUR.IMPONIBILE_INTRATTENIMENTI      ,
                                       recordCUR.ORDINE_DI_POSTO                 ,
                                       recordCUR.POSTO                           ,
                                       recordCUR.TIPO_TITOLO                     ,
                                       recordCUR.ANNULLATO                       ,
                                       recordCUR.PROGRESSIVO_ANNULLATI           ,
                                       recordCUR.DATA_EMISSIONE_ANNULLAMENTO     ,
                                       recordCUR.ORA_EMISSIONE_ANNULLAMENTO      ,
                                       recordCUR.DATAORA_EMISSIONE_ANNULLAMENTO  ,
                                       recordCUR.PROGRESSIVO_TITOLO              ,
                                       recordCUR.CODICE_PUNTO_VENDITA            ,
                                       recordCUR.SIGILLO                         ,
                                       recordCUR.CODICE_SISTEMA                  ,
                                       recordCUR.CODICE_CARTA                    ,
                                       recordCUR.PRESTAMPA                       ,
                                       recordCUR.CODICE_LOCALE                   ,
                                       recordCUR.DATA_EVENTO                     ,
                                       recordCUR.TIPO_EVENTO                     ,
                                       recordCUR.TITOLO_EVENTO                   ,
                                       recordCUR.ORA_EVENTO                      ,
                                       recordCUR.DATA_ORA_EVENTO                 ,
                                       recordCUR.CAUSALE_OMAGGIO_RIDUZIONE       ,
                                       recordCUR.TIPO_TURNO                      ,
                                       recordCUR.NUMERO_EVENTI_ABILITATI         ,
                                       recordCUR.DATA_LIMITE_VALIDITA            ,
                                       recordCUR.DATA_SCADENZA_ABBONAMENTO       ,
                                       recordCUR.CODICE_ABBONAMENTO              ,
                                       recordCUR.NUM_PROG_ABBONAMENTO            ,
                                       recordCUR.RATEO_EVENTO                    ,
                                       recordCUR.IVA_RATEO                       ,
                                       recordCUR.RATEO_IMPONIBILE_INTRA          ,
                                       recordCUR.CAUSALE_OMAGGIO_RIDUZIONE_OPEN  ,
                                       recordCUR.CORRISPETTIVO_TITOLO            ,
                                       recordCUR.CORRISPETTIVO_PREVENDITA        ,
                                       recordCUR.IVA_TITOLO                      ,
                                       recordCUR.IVA_PREVENDITA                  ,
                                       recordCUR.CORRISPETTIVO_FIGURATIVO        ,
                                       recordCUR.IVA_FIGURATIVA                  ,
                                       recordCUR.CODICE_FISCALE_ABBONAMENTO      ,
                                       recordCUR.CODICE_BIGLIETTO_ABBONAMENTO    ,
                                       recordCUR.NUM_PROG_BIGLIETTO_ABBONAMENTO  ,
                                       recordCUR.CODICE_PRESTAZIONE1             ,
                                       recordCUR.IMPORTO_PRESTAZIONE1            ,
                                       recordCUR.CODICE_PRESTAZIONE2             ,
                                       recordCUR.IMPORTO_PRESTAZIONE2            ,
                                       recordCUR.CODICE_PRESTAZIONE3             ,
                                       recordCUR.IMPORTO_PRESTAZIONE3            ,
                                       recordCUR.CARTA_ORIGINALE_ANNULLATO       ,
                                       recordCUR.CAUSALE_ANNULLAMENTO            ,
                                       recordCUR.PARTECIPANTE_NOME               ,
                                       recordCUR.PARTECIPANTE_COGNOME            ,
                                       recordCUR.ACQREG_AUTENTICAZIONE           ,
                                       recordCUR.ACQREG_CODICEUNIVOCO_ACQ        ,
                                       recordCUR.ACQREG_INDIRIZZOIP_REG          ,
                                       recordCUR.ACQREG_DATAORA_REG              ,
                                       recordCUR.ACQTRAN_CODICEUNIVOCO_NUM_TRAN  ,
                                       recordCUR.ACQTRAN_CELLULARE_ACQ           ,
                                       recordCUR.ACQTRAN_EMAIL_ACQ               ,
                                       recordCUR.ACQTRAN_INDIRIZZO_IP_TRAN       ,
                                       recordCUR.ACQTRAN_DATAORAINIZIOCHECKOUT   ,
                                       recordCUR.ACQTRAN_DATAORAESECUZIONE_PAG   ,
                                       recordCUR.ACQTRAN_CRO                     ,
                                       recordCUR.ACQTRAN_METODO_SPED_TITOLO      ,
                                       recordCUR.ACQTRAN_INDIRIZZO_SPED_TITOLO   ,
                                       recordCUR.DIGITALE_TRADIZIONALE,
                                       recordCUR.CAPIENZA_SETTORE);
            v_tab_TEMP.EXTEND;
            v_tab_TEMP (v_tab_TEMP.LAST) := rowCUR;
      END LOOP;

      v_tab := WTIC.OBJ_TABLE_LOG_BASE();
      FOR curDATA IN 
      (
       SELECT
       NVL(VR_VENDITE.IDCALEND, 0) AS IDCALEND,
       T_LOG.* 
       FROM 
       TABLE(v_tab_TEMP) T_LOG, 
       VR_VENDITE 
       WHERE
       T_LOG.IDVEND = VR_VENDITE.IDVEND(+)
       ORDER BY
       T_LOG.CODICE_SISTEMA,
       T_LOG.CODICE_FISCALE_TITOLARE,
       T_LOG.CODICE_FISCALE_ORGANIZZATORE,
       DECODE(T_LOG.TITOLO_ABBONAMENTO,'T', 1, 'A', 2),
       T_LOG.DATA_EVENTO,
       T_LOG.CODICE_LOCALE,
       T_LOG.ORA_EVENTO,
       T_LOG.TIPO_EVENTO,
       T_LOG.SPETTACOLO_INTRATTENIMENTO,
       VR_VENDITE.IDCALEND,
       T_LOG.DATAORA_EMISSIONE_ANNULLAMENTO,
       DECODE(T_LOG.TITOLO_IVA_PREASSOLTA, 'N', 1, 'F', 2, 'B', 3)
      )
      LOOP
            rowCUR := OBJ_ROW_LOG_BASE(curDATA.LOG_ID                          ,
                                       curDATA.LOG_DATE                        ,
                                       curDATA.IDVEND                          ,
                                       curDATA.CODICE_FISCALE_ORGANIZZATORE    ,
                                       curDATA.CODICE_FISCALE_TITOLARE         ,
                                       curDATA.TITOLO_ABBONAMENTO              ,
                                       curDATA.TITOLO_IVA_PREASSOLTA           ,
                                       curDATA.SPETTACOLO_INTRATTENIMENTO      ,
                                       curDATA.VALUTA                          ,
                                       curDATA.IMPONIBILE_INTRATTENIMENTI      ,
                                       curDATA.ORDINE_DI_POSTO                 ,
                                       curDATA.POSTO                           ,
                                       curDATA.TIPO_TITOLO                     ,
                                       curDATA.ANNULLATO                       ,
                                       curDATA.PROGRESSIVO_ANNULLATI           ,
                                       curDATA.DATA_EMISSIONE_ANNULLAMENTO     ,
                                       curDATA.ORA_EMISSIONE_ANNULLAMENTO      ,
                                       curDATA.DATAORA_EMISSIONE_ANNULLAMENTO  ,
                                       curDATA.PROGRESSIVO_TITOLO              ,
                                       curDATA.CODICE_PUNTO_VENDITA            ,
                                       curDATA.SIGILLO                         ,
                                       curDATA.CODICE_SISTEMA                  ,
                                       curDATA.CODICE_CARTA                    ,
                                       curDATA.PRESTAMPA                       ,
                                       curDATA.CODICE_LOCALE                   ,
                                       curDATA.DATA_EVENTO                     ,
                                       curDATA.TIPO_EVENTO                     ,
                                       curDATA.TITOLO_EVENTO                   ,
                                       curDATA.ORA_EVENTO                      ,
                                       curDATA.DATA_ORA_EVENTO                 ,
                                       curDATA.CAUSALE_OMAGGIO_RIDUZIONE       ,
                                       curDATA.TIPO_TURNO                      ,
                                       curDATA.NUMERO_EVENTI_ABILITATI         ,
                                       curDATA.DATA_LIMITE_VALIDITA            ,
                                       curDATA.DATA_SCADENZA_ABBONAMENTO       ,
                                       curDATA.CODICE_ABBONAMENTO              ,
                                       curDATA.NUM_PROG_ABBONAMENTO            ,
                                       curDATA.RATEO_EVENTO                    ,
                                       curDATA.IVA_RATEO                       ,
                                       curDATA.RATEO_IMPONIBILE_INTRA          ,
                                       curDATA.CAUSALE_OMAGGIO_RIDUZIONE_OPEN  ,
                                       curDATA.CORRISPETTIVO_TITOLO            ,
                                       curDATA.CORRISPETTIVO_PREVENDITA        ,
                                       curDATA.IVA_TITOLO                      ,
                                       curDATA.IVA_PREVENDITA                  ,
                                       curDATA.CORRISPETTIVO_FIGURATIVO        ,
                                       curDATA.IVA_FIGURATIVA                  ,
                                       curDATA.CODICE_FISCALE_ABBONAMENTO      ,
                                       curDATA.CODICE_BIGLIETTO_ABBONAMENTO    ,
                                       curDATA.NUM_PROG_BIGLIETTO_ABBONAMENTO  ,
                                       curDATA.CODICE_PRESTAZIONE1             ,
                                       curDATA.IMPORTO_PRESTAZIONE1            ,
                                       curDATA.CODICE_PRESTAZIONE2             ,
                                       curDATA.IMPORTO_PRESTAZIONE2            ,
                                       curDATA.CODICE_PRESTAZIONE3             ,
                                       curDATA.IMPORTO_PRESTAZIONE3            ,
                                       curDATA.CARTA_ORIGINALE_ANNULLATO       ,
                                       curDATA.CAUSALE_ANNULLAMENTO            ,
                                       curDATA.PARTECIPANTE_NOME               ,
                                       curDATA.PARTECIPANTE_COGNOME            ,
                                       curDATA.ACQREG_AUTENTICAZIONE           ,
                                       curDATA.ACQREG_CODICEUNIVOCO_ACQ        ,
                                       curDATA.ACQREG_INDIRIZZOIP_REG          ,
                                       curDATA.ACQREG_DATAORA_REG              ,
                                       curDATA.ACQTRAN_CODICEUNIVOCO_NUM_TRAN  ,
                                       curDATA.ACQTRAN_CELLULARE_ACQ           ,
                                       curDATA.ACQTRAN_EMAIL_ACQ               ,
                                       curDATA.ACQTRAN_INDIRIZZO_IP_TRAN       ,
                                       curDATA.ACQTRAN_DATAORAINIZIOCHECKOUT   ,
                                       curDATA.ACQTRAN_DATAORAESECUZIONE_PAG   ,
                                       curDATA.ACQTRAN_CRO                     ,
                                       curDATA.ACQTRAN_METODO_SPED_TITOLO      ,
                                       curDATA.ACQTRAN_INDIRIZZO_SPED_TITOLO   ,
                                       curDATA.DIGITALE_TRADIZIONALE,
                                       curDATA.CAPIENZA_SETTORE);

            IF (curDATA.TITOLO_IVA_PREASSOLTA = 'N') THEN

                n_PREZZO := curDATA.CORRISPETTIVO_TITOLO;
                n_PREVENDITA := curDATA.CORRISPETTIVO_PREVENDITA;

            ELSE

                n_PREZZO := curDATA.CORRISPETTIVO_FIGURATIVO;
                n_PREVENDITA := 0;

            END IF;

            IF (curDATA.TITOLO_ABBONAMENTO = 'T') THEN

                lResetVarEven := (c_CODICE_SISTEMA IS NULL) OR (c_CODICE_SISTEMA <> curDATA.CODICE_SISTEMA) OR 
                                 (c_CODICE_FISCALE_TITOLARE IS NULL) OR (c_CODICE_FISCALE_TITOLARE <> curDATA.CODICE_FISCALE_TITOLARE) OR
                                 (c_CODICE_FISCALE_ORGANIZZATORE IS NULL) OR (c_CODICE_FISCALE_ORGANIZZATORE <> curDATA.CODICE_FISCALE_ORGANIZZATORE) OR
                                 (c_DATA_EVENTO IS NULL) OR (c_DATA_EVENTO <> curDATA.DATA_EVENTO) OR
                                 (c_CODICE_LOCALE IS NULL) OR (c_CODICE_LOCALE <> curDATA.CODICE_LOCALE) OR 
                                 (c_ORA_EVENTO IS NULL) OR (c_ORA_EVENTO <> curDATA.ORA_EVENTO) OR
                                 (c_TIPO_EVENTO IS NULL) OR (c_TIPO_EVENTO <> curDATA.TIPO_EVENTO) OR 
                                 (c_SPETTACOLO_INTRATTENIMENTO IS NULL) OR (c_SPETTACOLO_INTRATTENIMENTO <> curDATA.SPETTACOLO_INTRATTENIMENTO) OR 
                                 (n_IDCALEND IS NULL) OR (n_IDCALEND <> curDATA.IDCALEND);


                IF lResetVarEven THEN

                        c_CODICE_SISTEMA := curDATA.CODICE_SISTEMA;
                        c_CODICE_FISCALE_TITOLARE := curDATA.CODICE_FISCALE_TITOLARE;
                        c_CODICE_FISCALE_ORGANIZZATORE := curDATA.CODICE_FISCALE_ORGANIZZATORE;
                        c_DATA_EVENTO := curDATA.DATA_EVENTO;
                        c_CODICE_LOCALE := curDATA.CODICE_LOCALE;
                        c_ORA_EVENTO := curDATA.ORA_EVENTO;
                        c_TIPO_EVENTO := curDATA.TIPO_EVENTO;
                        c_SPETTACOLO_INTRATTENIMENTO := curDATA.SPETTACOLO_INTRATTENIMENTO;
                        n_IDCALEND := curDATA.IDCALEND;

                        d_DATA_ORA := curDATA.DATA_ORA_EVENTO;
                        d_DATA_VALIDITA := TRUNC(curDATA.DATA_ORA_EVENTO);

                        nPerc_IVA := NULL;
                        nPerc_INTRATTENIMENTO := NULL;
                        nIncidenza_INTRATTENIMENTO := NULL;
                        n_NETTO_TITOLO := 0;
                        n_NETTO_PREVENDITA := 0;
                        n_IMPONIBILE_IMPOSTA_INTRA := 0;
                        n_IVA_TITOLO := 0;
                        n_IVA_PREVENDITA := 0;
                        n_ISI_TOTALE := 0;
                        c_FLAG_IRRIPARTIBILE := NULL;

                        IF (c_SPETTACOLO_INTRATTENIMENTO = 'S') THEN

                                nPerc_INTRATTENIMENTO := 0;
                                nIncidenza_INTRATTENIMENTO := 0;

                                SELECT MAX(PERC_IVA) INTO nPerc_IVA FROM WTIC.VR_TE_IMPOSTE WHERE TE = c_TIPO_EVENTO;

                                IF (nPerc_IVA IS NULL) THEN

                                        nPerc_IVA := 10;

                                END IF;

                        ELSE

                                IF (n_IDCALEND = 0) THEN

                                        SELECT 
                                        NVL(MAX(VR_CALENDARIO.IDCALEND), 0),
                                        VR_INCIDENZA_INTRATTENIMENTO.PERC_IVA,
                                        VR_INCIDENZA_INTRATTENIMENTO.PERC_ISI,
                                        VR_INCIDENZA_INTRATTENIMENTO.INCIDENZA_INTRATTENIMENTO
                                        INTO
                                        n_IDCALEND,
                                        nPerc_IVA,
                                        nPerc_INTRATTENIMENTO,
                                        nIncidenza_INTRATTENIMENTO 
                                        FROM 
                                        VR_CALENDARIO, VR_SALE, VR_INCIDENZA_INTRATTENIMENTO
                                        WHERE 
                                        VR_CALENDARIO.DATA_ORA = d_DATA_ORA
                                        AND VR_SALE.CODICE_LOCALE = c_CODICE_LOCALE
                                        AND VR_CALENDARIO.TE = c_TIPO_EVENTO
                                        AND VR_CALENDARIO.SI = c_SPETTACOLO_INTRATTENIMENTO
                                        AND VR_CALENDARIO.IDCALEND = VR_INCIDENZA_INTRATTENIMENTO.IDCALEND(+);

                                ELSE

                                        SELECT 
                                        VR_INCIDENZA_INTRATTENIMENTO.PERC_IVA,
                                        VR_INCIDENZA_INTRATTENIMENTO.PERC_ISI,
                                        VR_INCIDENZA_INTRATTENIMENTO.INCIDENZA_INTRATTENIMENTO
                                        INTO
                                        nPerc_IVA,
                                        nPerc_INTRATTENIMENTO,
                                        nIncidenza_INTRATTENIMENTO 
                                        FROM 
                                        VR_INCIDENZA_INTRATTENIMENTO
                                        WHERE 
                                        VR_INCIDENZA_INTRATTENIMENTO.IDCALEND = n_IDCALEND;

                                END IF;

                                IF (nPerc_IVA IS NULL OR nPerc_INTRATTENIMENTO IS NULL) THEN

                                        SELECT MAX(PERC_IVA), MAX(PERC_INTRA) INTO nTEMP_PercIvaSuIntra, nTEMP_PercIsiSuIntra FROM WTIC.VR_TE_IMPOSTE WHERE TE = c_TIPO_EVENTO;

                                        IF (nPerc_IVA IS NULL) THEN

                                                nPerc_IVA := nTEMP_PercIvaSuIntra;

                                        END IF;

                                        IF (nPerc_INTRATTENIMENTO IS NULL) THEN

                                                nPerc_INTRATTENIMENTO := nTEMP_PercIsiSuIntra;

                                        END IF;

                                END IF;

                                IF (nIncidenza_INTRATTENIMENTO IS NULL) THEN

                                        nIncidenza_INTRATTENIMENTO := 100;

                                END IF;

                        END IF;

                END IF;

                P_CALC_IVAISI_R(c_TIPO_EVENTO, c_SPETTACOLO_INTRATTENIMENTO, d_DATA_VALIDITA, nIncidenza_INTRATTENIMENTO, n_PREZZO, n_PREVENDITA,
                                nPerc_IVA, nPerc_INTRATTENIMENTO,
                                n_NETTO_TITOLO, n_NETTO_PREVENDITA, n_IMPONIBILE_IMPOSTA_INTRA, n_IVA_TITOLO, n_IVA_PREVENDITA, n_ISI_TOTALE, c_FLAG_IRRIPARTIBILE, 'F',
                                n_IDCALEND, oTableIMPOSTE);

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'N') THEN

                        rowCUR.IVA_TITOLO := n_IVA_TITOLO;
                        rowCUR.IVA_PREVENDITA := n_IVA_PREVENDITA;

                END IF;

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'B') THEN

                        rowCUR.IVA_FIGURATIVA := n_IVA_TITOLO;

                END IF;

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'F') THEN

                        rowCUR.IVA_FIGURATIVA := n_IVA_TITOLO;

                END IF;

                IF c_SPETTACOLO_INTRATTENIMENTO = 'I' THEN

                        rowCUR.IMPONIBILE_INTRATTENIMENTI := n_IMPONIBILE_IMPOSTA_INTRA;

                ELSE

                        rowCUR.IMPONIBILE_INTRATTENIMENTI := 0;

                END IF;

            ELSE


                /* VALORIZZAZIONE IVA_TITOLO, IVA_PREVENDITA, IMPONIBILE_INTRATTENIMENTI PER VENDITA ABBONAMENTO */
                c_CODICE_SISTEMA := curDATA.CODICE_SISTEMA;
                c_CODICE_FISCALE_TITOLARE := curDATA.CODICE_FISCALE_TITOLARE;
                c_CODICE_FISCALE_ORGANIZZATORE := curDATA.CODICE_FISCALE_ORGANIZZATORE;
                c_SPETTACOLO_INTRATTENIMENTO := curDATA.SPETTACOLO_INTRATTENIMENTO;
                d_DATA_VALIDITA := curDATA.DATAORA_EMISSIONE_ANNULLAMENTO;
                nPerc_IVA := 0;
                nPerc_INTRATTENIMENTO := 0;
                nIncidenza_INTRATTENIMENTO := 0;
                nTEMP_PercIvaAbbonamento := 0;
                nTEMP_PercIsiAbbonamento := 0;


                SELECT TE INTO c_TIPO_EVENTO FROM VR_SMART_TESSERE_TE WHERE IDVEND = curDATA.IDVEND;

                IF c_TIPO_EVENTO IS NULL THEN

                        SELECT MAX(TRIM(TO_CHAR(TO_NUMBER(PROPERTY_VALUE),'00'))) INTO c_TIPO_EVENTO FROM VR_PARAMETRI_CASSA WHERE PROPERTY_NAME = 'FISCALE_EVENTO_DEFAULT';

                END IF;

                IF c_SPETTACOLO_INTRATTENIMENTO = 'I' THEN

                        nIncidenza_INTRATTENIMENTO := 100;
                        SELECT PERC_IVA, PERC_INTRA INTO nTEMP_PercIvaAbbonamento, nTEMP_PercIsiAbbonamento FROM VR_TE_IMPOSTE WHERE TE = c_TIPO_EVENTO;

                ELSE

                        SELECT PERC_IVA INTO nTEMP_PercIvaAbbonamento FROM VR_TE_IMPOSTE WHERE TE = c_TIPO_EVENTO;

                END IF;

                IF nTEMP_PercIvaAbbonamento IS NULL THEN

                        SELECT MAX(TO_NUMBER(PROPERTY_VALUE)) INTO nTEMP_PercIvaAbbonamento FROM VR_PARAMETRI_CASSA WHERE PROPERTY_NAME = 'FISCALE_PERC_IVA_BIGLIETTI';

                END IF;

                IF c_SPETTACOLO_INTRATTENIMENTO = 'I' THEN

                        IF nTEMP_PercIvaAbbonamento IS NULL THEN

                                nTEMP_PercIvaAbbonamento := 22;

                        END IF;

                        IF nTEMP_PercIsiAbbonamento IS NULL THEN

                                nTEMP_PercIsiAbbonamento := 16;

                        END IF;

                ELSE

                        IF nTEMP_PercIvaAbbonamento IS NULL THEN

                                nTEMP_PercIvaAbbonamento := 10;

                        END IF;

                END IF;

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'N') THEN

                    n_PREZZO := curDATA.CORRISPETTIVO_TITOLO;
                    n_PREVENDITA := curDATA.CORRISPETTIVO_PREVENDITA;

                ELSE

                    n_PREZZO := curDATA.CORRISPETTIVO_FIGURATIVO;
                    n_PREVENDITA := 0;

                END IF;

                nPerc_IVA := nTEMP_PercIvaAbbonamento;
                nPernPerc_INTRATTENIMENTOc_IVA := nTEMP_PercIsiAbbonamento;

                P_CALC_IVAISI_R(c_TIPO_EVENTO, c_SPETTACOLO_INTRATTENIMENTO, d_DATA_VALIDITA, nIncidenza_INTRATTENIMENTO, n_PREZZO, n_PREVENDITA,
                                nPerc_IVA, nPerc_INTRATTENIMENTO,
                                n_NETTO_TITOLO, n_NETTO_PREVENDITA, n_IMPONIBILE_IMPOSTA_INTRA, n_IVA_TITOLO, n_IVA_PREVENDITA, n_ISI_TOTALE, c_FLAG_IRRIPARTIBILE, 'F',
                                0, oTableIMPOSTE);

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'N') THEN

                        rowCUR.IVA_TITOLO := n_IVA_TITOLO;
                        rowCUR.IVA_PREVENDITA := n_IVA_PREVENDITA;

                END IF;

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'B') THEN

                        rowCUR.IVA_FIGURATIVA := n_IVA_TITOLO;

                END IF;

                IF (curDATA.TITOLO_IVA_PREASSOLTA = 'F') THEN

                        rowCUR.IVA_FIGURATIVA := n_IVA_TITOLO;

                END IF;

                IF c_SPETTACOLO_INTRATTENIMENTO = 'I' THEN

                        rowCUR.IMPONIBILE_INTRATTENIMENTI := n_IMPONIBILE_IMPOSTA_INTRA;

                ELSE

                        rowCUR.IMPONIBILE_INTRATTENIMENTI := 0;

                END IF;

                /* VALORIZZAZIONE IVA_RATEO, RATEO_IMPONIBILE_INTRA PER DATI DEL RATEO SULLA VENDITA ABBONAMENTO */
                c_CODICE_SISTEMA := curDATA.CODICE_SISTEMA;
                c_CODICE_FISCALE_TITOLARE := curDATA.CODICE_FISCALE_TITOLARE;
                c_CODICE_FISCALE_ORGANIZZATORE := curDATA.CODICE_FISCALE_ORGANIZZATORE;
                c_SPETTACOLO_INTRATTENIMENTO := curDATA.SPETTACOLO_INTRATTENIMENTO;
                d_DATA_VALIDITA := curDATA.DATAORA_EMISSIONE_ANNULLAMENTO;
                nPerc_IVA := nTEMP_PercIvaAbbonamento;
                nPerc_INTRATTENIMENTO := nTEMP_PercIsiAbbonamento;

                n_PREZZO := rowCUR.IVA_RATEO := 0;
                n_PREVENDITA := 0;

                P_CALC_IVAISI_R(c_TIPO_EVENTO, c_SPETTACOLO_INTRATTENIMENTO, d_DATA_VALIDITA, nIncidenza_INTRATTENIMENTO, n_PREZZO, n_PREVENDITA,
                                nPerc_IVA, nPerc_INTRATTENIMENTO,
                                n_NETTO_TITOLO, n_NETTO_PREVENDITA, n_IMPONIBILE_IMPOSTA_INTRA, n_IVA_TITOLO, n_IVA_PREVENDITA, n_ISI_TOTALE, c_FLAG_IRRIPARTIBILE, 'F',
                                0, oTableIMPOSTE);

                rowCUR.IVA_RATEO := n_IVA_TITOLO;

                IF c_SPETTACOLO_INTRATTENIMENTO = 'I' THEN

                        rowCUR.RATEO_IMPONIBILE_INTRA := n_IMPONIBILE_IMPOSTA_INTRA;

                ELSE

                        rowCUR.RATEO_IMPONIBILE_INTRA := 0;

                END IF;

            END IF;

            v_tab.EXTEND;
            v_tab (v_tab.LAST) := rowCUR;
      END LOOP;

      v_tab_TEMP := NULL;

      RETURN v_tab;
END;











CREATE OR REPLACE FUNCTION WTIC.F_TABLE_LOG_BASE_PIPPO(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cTipo IN VARCHAR2, cGiornalieroMensile IN VARCHAR2, cFlagLogStorico IN VARCHAR2, nMIN_LOG_ID IN NUMBER, nMAX_LOG_ID IN NUMBER, nIdFilterExtIdVend IN NUMBER) RETURN WTIC.OBJ_TABLE_LOG_BASE
IS
      v_tab WTIC.OBJ_TABLE_LOG_BASE;
      rowCUR OBJ_ROW_LOG_BASE;
      
      cDataEmiInizio VARCHAR2(8);
      cDataEmiFine VARCHAR2(8);
      cFlagStorico VARCHAR2(10);
      cCodiceSistema VARCHAR2(8);
      logCUR SYS_REFCURSOR;
      recordCUR LOG_TRANSAZIONI%rowtype;
BEGIN
      cCodiceSistema := WTIC.F_GET_CODICE_SISTEMA();
      cFlagStorico := WTIC.F_CHECK_STORICO(dDataEmiInizio, cGiornalieroMensile, cFlagLogStorico);
      v_tab := WTIC.OBJ_TABLE_LOG_BASE();

      /* Se Giornaliero G*/
      IF (cGiornalieroMensile = 'G') THEN
            IF (cTipo IS NULL) THEN
                  IF (nIdFilterExtIdVend = 0) THEN
                        IF (nMIN_LOG_ID > 0) THEN
                              P_TABLE_LOGBASE_LOGMIN(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, nMIN_LOG_ID, logCUR);
                        ELSE
                              P_TABLE_LOGBASE_RPG(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, nMAX_LOG_ID, logCUR);
                        END IF;
                  ELSE
                        P_TABLE_LOGBASE_RPG_FILT(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, nIdFilterExtIdVend, nMAX_LOG_ID, logCUR);
                  END IF;
            ELSE
                  IF (nIdFilterExtIdVend = 0) THEN
                        P_TABLE_LOGBASE_RPG_TIPO(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cTipo, nMAX_LOG_ID, logCUR);
                  ELSE
                        P_TABLE_LOGBASE_RPG_TIPO_FILT(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cTipo, nIdFilterExtIdVend, nMAX_LOG_ID, logCUR);
                  END IF;
            END IF;
      ELSE
            IF (cTipo IS NULL) THEN
                  IF (nIdFilterExtIdVend = 0) THEN
                        P_TABLE_LOGBASE_RPM(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cGiornalieroMensile, logCUR);
                  ELSE
                        P_TABLE_LOGBASE_RPM_FILT(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cGiornalieroMensile, nIdFilterExtIdVend, logCUR);
                  END IF;
            ELSE
                  IF (nIdFilterExtIdVend = 0) THEN
                        P_TABLE_LOGBASE_RPM_TIPO(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cGiornalieroMensile, cTipo, logCUR);
                  ELSE
                        P_TABLE_LOGBASE_RPM_TIPO_FILT(cCodiceSistema, dDataEmiInizio, dDataEmiFine, cFlagStorico, cGiornalieroMensile, cTipo, nIdFilterExtIdVend, logCUR);
                  END IF;
            END IF;
      END IF;

      IF (logCUR IS NOT NULL) THEN
            v_tab := F_PIPPO_FETCH(logCUR);
            CLOSE logCUR;
      END IF;
      return v_tab;
END;
































SELECT 
A.IDVEND,
A.TIPO_EVENTO,
A.SPETTACOLO_INTRATTENIMENTO,
A.TITOLO_IVA_PREASSOLTA,
A.CORRISPETTIVO_TITOLO,
A.IVA_TITOLO,
B.IVA_TITOLO AS IVA_TITOLO_RICALC,
A.CORRISPETTIVO_PREVENDITA,
A.IVA_PREVENDITA,
B.IVA_PREVENDITA AS IVA_PREVENDITA_CALC,
A.CORRISPETTIVO_FIGURATIVO,
A.IVA_FIGURATIVA ,
B.IVA_PREVENDITA ASIVA_FIGURATIVA_CALC,
A.IMPONIBILE_INTRATTENIMENTI,
B.IMPONIBILE_INTRATTENIMENTI AS IMPONIBILE_INTRATTENIMENTI_C,
A.IVA_RATEO,
B.IVA_RATEO AS IVA_RATEO_CALC,
A.RATEO_IMPONIBILE_INTRA,
B.RATEO_IMPONIBILE_INTRA AS RATEO_IMPONIBILE_INTRA_CALC
FROM
(SELECT X.* FROM TABLE(F_TABLE_LOG_BASE_DATA(SYSDATE-1000, SYSDATE, NULL, 'G', 'CHECK', 0, 0, 0)) X) A,
(SELECT X.* FROM TABLE(F_TABLE_LOG_BASE_PIPPO(SYSDATE-1000, SYSDATE, NULL, 'G', 'CHECK', 0, 0, 0)) X) B
WHERE
A.IDVEND = B.IDVEND
AND
(A.IVA_TITOLO <> B.IVA_TITOLO
 OR A.IVA_PREVENDITA <> B.IVA_PREVENDITA
 OR A.IVA_FIGURATIVA <> B.IVA_FIGURATIVA
 OR A.IMPONIBILE_INTRATTENIMENTI <> B.IMPONIBILE_INTRATTENIMENTI
 OR A.IVA_RATEO <> B.IVA_RATEO
 OR A.RATEO_IMPONIBILE_INTRA <> B.RATEO_IMPONIBILE_INTRA)
ORDER BY
A.TIPO_EVENTO,
A.IDVEND