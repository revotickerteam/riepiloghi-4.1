CREATE OR REPLACE FUNCTION WTIC.F_TRANSAZIONI_RIEPILOGHI_VUOTI(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2, nMAX_LOG_ID IN NUMBER, nIdFilterExtIdVend IN NUMBER) RETURN WTIC.OBJ_TABLE_RIEPILOGHI
IS
    v_tab  WTIC.OBJ_TABLE_RIEPILOGHI;
    oRow  WTIC.OBJ_ROW_RIEPILOGHI;

    cDenominazioneTitolare VARCHAR2(40);
    cDenominazioneOrganizzatore VARCHAR2(40);
    cTipoCfPi VARCHAR2(2);

    cDenominazioneLocale VARCHAR2(50);
    cDescSalaRagSocDatifissi VARCHAR2(100);
    cRagSocDatiFissi VARCHAR2(30);
    cLuogoLocale VARCHAR2(100);

    nRigaOrganizzatore NUMBER;

    /* variabili temporanee per calcoli IVA, IMPOSTA INTRATTENIMENTO, IMPONIBILE INTRATTENIMENTO */
    nPerc_IVA NUMBER;
    nPerc_INTRATTENIMENTO NUMBER;
    nIncidenza_INTRATTENIMENTO NUMBER;

    nPercIVA_ROW NUMBER;
    nPercINTRA_ROW NUMBER;

	dDataIns DATE;
    nProgRiepilogo NUMBER;
	oTableOrganizzatori WTIC.OBJ_TABLE_ORGANIZZATORI;

	dDataInizioFiltro DATE;
	dDataTerminFiltro DATE;

BEGIN
    v_tab := WTIC.OBJ_TABLE_RIEPILOGHI();
    dDataIns := SYSDATE;
	nProgRiepilogo := 0;
	oTableOrganizzatori := WTIC.F_INIT_ORGANIZZATORI();

    IF (cGiornalieroMensile = 'D' OR cGiornalieroMensile = 'M') THEN

        IF (cGiornalieroMensile = 'D') THEN
            dDataInizioFiltro := TO_DATE(TRIM(TO_CHAR(TRUNC(dDataEmiInizio),'YYYYMMDD')) || '000000', 'YYYYMMDDHH24MISS');
            dDataTerminFiltro := TO_DATE(TRIM(TO_CHAR(TRUNC(dDataEmiFine)  ,'YYYYMMDD')) || '235959', 'YYYYMMDDHH24MISS');
        ELSE
            dDataInizioFiltro := TRUNC(F_INIZIO_MESE(dDataEmiInizio));
            dDataTerminFiltro := TRUNC(F_FINE_MESE(dDataEmiFine));
        END IF;

        SELECT NVL(MAX(VALORE),'TRUE') INTO cDescSalaRagSocDatifissi FROM WTIC.PROPRIETA_RIEPILOGHI WHERE PROPRIETA = 'DESC_SALA_RAG_SOC_DATIFISSI';
        IF (cDescSalaRagSocDatifissi IS NOT NULL AND cDescSalaRagSocDatifissi = 'ABILITATO') THEN
            SELECT RAG_SOC INTO cRagSocDatiFissi FROM WTIC.VR_DATIFISSI;
            IF (cRagSocDatiFissi IS NOT NULL) THEN
                cRagSocDatiFissi := TRIM(cRagSocDatiFissi) || ' ';
            ELSE
                cRagSocDatiFissi := '';
            END IF;
        ELSE
            cRagSocDatiFissi := '';
        END IF;

        FOR curBIGLIETTI IN 
        (SELECT 
         VR_CODICE_SISTEMA.CODICE_SISTEMA, VR_CODICE_SISTEMA.CF_TITOLARE AS CODICE_FISCALE_TITOLARE,
         NULL AS DATA_EMISSIONE_ANNULLAMENTO, NULL AS DATAORA_EMISSIONE_ANNULLAMENTO,
         VR_CALENDARIO.ORGANIZZATORE AS CODICE_FISCALE_ORGANIZZATORE, TRIM(TO_CHAR(VR_CALENDARIO.DATA_ORA,'YYYYMMDD')) AS DATA_EVENTO,
         VR_SALE.CODICE_LOCALE, TRIM(TO_CHAR(VR_CALENDARIO.DATA_ORA,'HH24MI')) AS ORA_EVENTO, VR_CALENDARIO.DATA_ORA AS DATA_ORA_EVENTO,
         'T' AS TITOLO_ABBONAMENTO, VR_CALENDARIO.SI AS SPETTACOLO_INTRATTENIMENTO,
         VR_CALENDARIO.TE AS TIPO_EVENTO, VR_PACCHETTO.DESCRIZIONE AS TITOLO_EVENTO, 
         NULL AS TITOLO_IVA_PREASSOLTA, NULL AS ORDINE_DI_POSTO, NULL AS TIPO_TITOLO, NULL AS CORRISPETTIVO_TITOLO, NULL AS IVA_TITOLO,
         NULL AS CORRISPETTIVO_PREVENDITA, NULL AS IVA_PREVENDITA, NULL AS CORRISPETTIVO_FIGURATIVO, NULL AS IVA_FIGURATIVA,
         NULL AS CODICE_PRESTAZIONE1, NULL AS IMPORTO_PRESTAZIONE1, NULL AS CODICE_PRESTAZIONE2, NULL AS IMPORTO_PRESTAZIONE2, NULL AS CODICE_PRESTAZIONE3, NULL AS IMPORTO_PRESTAZIONE3,
         NULL AS CODICE_BIGLIETTO_ABBONAMENTO, NULL AS CODICE_FISCALE_ABBONAMENTO, VR_CALENDARIO.IDCALEND,
         VR_CALENDARIO.IDSALA, VR_CALENDARIO.IDPACCHETTO, VR_CALENDARIO.IDTARIFFA, 0 AS IDBIGLIETTO,
         NULL AS MIN_DATAORA_EMI, NULL AS MAX_DATAORA_EMI, 
         0 AS QTA_EMESSI, 0 AS QTA_ANNULLATI, 0 AS TOT_QTA, 0 AS IS_OMAGGIO
         FROM 
         WTIC.VR_CODICE_SISTEMA, WTIC.VR_CALENDARIO, WTIC.VR_SALE, WTIC.VR_PACCHETTO
         WHERE 
         VR_CALENDARIO.IDSALA = VR_SALE.IDSALA
         AND
         VR_CALENDARIO.IDPACCHETTO = VR_PACCHETTO.IDPACCHETTO
         AND
         VR_PACCHETTO.PROGR = 1
         AND
         VR_CALENDARIO.DATA_ORA < SYSDATE
         AND
         TRUNC(VR_CALENDARIO.DATA_ORA) >= dDataInizioFiltro
         AND
         TRUNC(VR_CALENDARIO.DATA_ORA) <= dDataTerminFiltro
         AND
         NOT EXISTS 
         (SELECT 
          NULL 
          FROM 
          VR_LOGTRANS
          WHERE
          VR_LOGTRANS.TITOLO_ABBONAMENTO = 'T'
          AND
          VR_LOGTRANS.DATA_ORA_EVENTO = VR_CALENDARIO.DATA_ORA
          AND
          VR_LOGTRANS.CODICE_LOCALE = VR_SALE.CODICE_LOCALE
          AND
          VR_LOGTRANS.TITOLO_EVENTO = VR_PACCHETTO.DESCRIZIONE
          AND
          VR_LOGTRANS.DATA_ORA_EVENTO < SYSDATE
          AND
          TRUNC(VR_LOGTRANS.DATA_ORA_EVENTO) >= dDataInizioFiltro
          AND
          TRUNC(VR_LOGTRANS.DATA_ORA_EVENTO) <= dDataTerminFiltro
         )
         ORDER BY 
         TRUNC(VR_CALENDARIO.DATA_ORA),
         VR_SALE.CODICE_LOCALE,
         VR_CALENDARIO.DATA_ORA
        )
        LOOP

            oRow := WTIC.OBJ_ROW_RIEPILOGHI(
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                    NULL,NULL);

            SELECT NVL(MAX(TITOLARE),'') into cDenominazioneTitolare FROM WTIC.SMART_CARD WHERE CODICE_SISTEMA = curBIGLIETTI.CODICE_SISTEMA;

            /* Denominazione Organizzatore e tipo CF o PI */
            FOR nRigaOrganizzatore IN oTableOrganizzatori.FIRST .. oTableOrganizzatori.LAST
            LOOP
                IF (TRIM(oTableOrganizzatori(nRigaOrganizzatore).ORGANIZZATORE) = TRIM(curBIGLIETTI.CODICE_FISCALE_ORGANIZZATORE)) THEN
                    cDenominazioneOrganizzatore := oTableOrganizzatori(nRigaOrganizzatore).RAG_SOC;
                    cTipoCfPi := oTableOrganizzatori(nRigaOrganizzatore).CF_PI;
                    EXIT WHEN TRUE;
                END IF;
            END LOOP;

            /* cerco la denominazione della sala per IDCALEND */
            cDenominazioneLocale := NULL;
            cLuogoLocale := NULL;
            IF (curBIGLIETTI.IDSALA IS NOT NULL) THEN
                SELECT MAX(ALIAS) INTO cDenominazioneLocale FROM WTIC.VR_SALE WHERE IDSALA = curBIGLIETTI.IDSALA;
                IF (cDenominazioneLocale IS NULL) THEN
                    SELECT MAX(DESCR) INTO cDenominazioneLocale FROM WTIC.VR_SALE WHERE IDSALA = curBIGLIETTI.IDSALA;
                END IF;
                IF (cDenominazioneLocale IS NULL) THEN
                    SELECT MAX(CODICE_LOCALE) INTO cDenominazioneLocale FROM WTIC.VR_SALE WHERE IDSALA = curBIGLIETTI.IDSALA;
                END IF;
                IF (cDenominazioneLocale IS NULL) THEN
                    cDenominazioneLocale := curBIGLIETTI.CODICE_LOCALE;
                END IF;
                IF (cDenominazioneLocale IS NULL) THEN
                    cDenominazioneLocale := ' ';
                END IF;
                SELECT MAX(LUOGO) INTO cLuogoLocale FROM WTIC.VR_SALE_LUOGO WHERE IDSALA = curBIGLIETTI.IDSALA;
                IF (cLuogoLocale IS NULL) THEN
                    cLuogoLocale := ' ';
                END IF;
            END IF;

            IF (cDenominazioneLocale IS NULL) THEN
                cDenominazioneLocale := curBIGLIETTI.CODICE_LOCALE;
                cLuogoLocale := '';
            END IF;
            cDenominazioneLocale := cRagSocDatiFissi || cDenominazioneLocale;

            /* cerco IVA per IDCALEND */
            nPerc_IVA := -1;
            IF (curBIGLIETTI.IDCALEND IS NOT NULL) THEN
                SELECT NVL(MAX(IVA),-1) INTO nPerc_IVA FROM WTIC.VR_IVA_INTRATTENIMENTO WHERE IDCALEND = curBIGLIETTI.IDCALEND;
            END IF;

            /* se nPerc_IVA = -1 la cerco per tipo evento in WTIC.VR_TE_IMPOSTE */
            IF (nPerc_IVA = -1) THEN
                SELECT NVL(MAX(PERC_IVA),-1) INTO nPerc_IVA FROM WTIC.VR_TE_IMPOSTE WHERE TE = curBIGLIETTI.TIPO_EVENTO;
            END IF;

            /* Percentuale IVA e IMPOSTA INTRATTENIMENTO */
            IF (curBIGLIETTI.SPETTACOLO_INTRATTENIMENTO = 'S') THEN
                nPerc_INTRATTENIMENTO := 0;
                nIncidenza_INTRATTENIMENTO := 0;
                /* se nPerc_IVA = -1 di default per lo spettacolo e' 10 */
                IF (nPerc_IVA = -1) THEN
                    nPerc_IVA := 10;
                END IF;
            ELSE
                /* per ora al 100% poi vediamo dove metterlo nell'utente CINEMA legato al CALENDARIO */
                IF curBIGLIETTI.IDCALEND IS NOT NULL THEN
                    SELECT NVL(MAX(INCIDENZA_INTRATTENIMENTO),0) INTO nIncidenza_INTRATTENIMENTO FROM WTIC.VR_INCIDENZA_INTRATTENIMENTO WHERE IDCALEND = curBIGLIETTI.IDCALEND;
                    IF nIncidenza_INTRATTENIMENTO IS NULL THEN
                        nIncidenza_INTRATTENIMENTO := 0;
                    END IF;
                ELSE
                    nIncidenza_INTRATTENIMENTO := 100;
                END IF;
                /* se nPerc_IVA = -1 per l'intrattenimento la metto al 22 */
                IF (nPerc_IVA = -1) THEN
                    nPerc_IVA := 22;
                END IF;
                SELECT NVL(MAX(PERC_INTRA),-1) INTO nPerc_INTRATTENIMENTO FROM WTIC.VR_TE_IMPOSTE WHERE TE = curBIGLIETTI.TIPO_EVENTO;
                /* se  isi = -1 default 16 */
                IF (nPerc_INTRATTENIMENTO = -1) THEN
                    nPerc_INTRATTENIMENTO := 16;
                END IF;
            END IF;

            nProgRiepilogo := WTIC.F_CALC_PROG_RIEPILOGO(TRUNC(curBIGLIETTI.DATA_ORA_EVENTO), cGiornalieroMensile);

            nPercIVA_ROW := nPerc_IVA;
            nPercINTRA_ROW := nPerc_INTRATTENIMENTO;

            oRow.DATA_INS := dDataIns;
            oRow.PROG_RIEPILOGO := nProgRiepilogo;

            /* Sistema */
            oRow.CODICE_SISTEMA := curBIGLIETTI.CODICE_SISTEMA;
            oRow.CODICE_FISCALE_TITOLARE := curBIGLIETTI.CODICE_FISCALE_TITOLARE;
            oRow.DENOMINAZIONE_TITOLARE := cDenominazioneTitolare;

            /* Tipo di riepilogo */
            oRow.TIPO_RIEPILOGO := cGiornalieroMensile;

            IF (cGiornalieroMensile = 'M') THEN
                /* primo giorno del mese */
                oRow.GIORNO_MESE := WTIC.F_INIZIO_MESE(TRUNC(curBIGLIETTI.DATA_ORA_EVENTO));
            ELSE
                /* cGiornalieroMensile = 'D' */
                oRow.GIORNO_MESE := TRUNC(curBIGLIETTI.DATA_ORA_EVENTO);
            END IF;

            /* Organizzatore */
            oRow.CODICE_FISCALE_ORGANIZZATORE := curBIGLIETTI.CODICE_FISCALE_ORGANIZZATORE;
            oRow.DENOMINAZIONE_ORGANIZZATORE := cDenominazioneOrganizzatore;
            oRow.TIPO_CF_PI := cTipoCfPi;

            /* Biglietti */
            oRow.TITOLO_ABBONAMENTO := 'T';

            /* Evento */
            oRow.SPETTACOLO_INTRATTENIMENTO := curBIGLIETTI.SPETTACOLO_INTRATTENIMENTO;
            oRow.DENOMINAZIONE_LOCALE := cDenominazioneLocale;
            oRow.LUOGO_LOCALE := cLuogoLocale;
            oRow.CODICE_LOCALE := curBIGLIETTI.CODICE_LOCALE;
            oRow.DATA_EVENTO := curBIGLIETTI.DATA_EVENTO;
            oRow.ORA_EVENTO := curBIGLIETTI.ORA_EVENTO;
            oRow.DATA_ORA_EVENTO := curBIGLIETTI.DATA_ORA_EVENTO;
            oRow.TIPO_EVENTO := curBIGLIETTI.TIPO_EVENTO;
            oRow.TITOLO_EVENTO := curBIGLIETTI.TITOLO_EVENTO;

            /* Tipo IVA */
            oRow.TITOLO_IVA_PREASSOLTA := curBIGLIETTI.TITOLO_IVA_PREASSOLTA;

            /* Settore */
            oRow.ORDINE_DI_POSTO := curBIGLIETTI.ORDINE_DI_POSTO;

            /* Capienza */
            oRow.CAPIENZA := 0;
            oRow.NUMERO_OMAGGI_RICONOSCIUTI := 0;

            oRow.CODICE_FISCALE_ABBONAMENTO := NULL;
            oRow.CODICE_BIGLIETTO_ABBONAMENTO := NULL;

            /* Tipo di biglietto */
            oRow.TIPO_TITOLO := NULL;

            /* Valorizzazione della riga */
            oRow.CORRISPETTIVO_TITOLO := 0;
            oRow.CORRISPETTIVO_PREVENDITA := 0;
            oRow.CORRISPETTIVO_FIGURATIVO := 0;



            oRow.PERC_IVA := nPercIVA_ROW;
            oRow.PERC_ISI := nPercINTRA_ROW;
            oRow.INCIDENZA_ISI := nIncidenza_INTRATTENIMENTO;
            oRow.PERC_OMAGGI_TIPO_EVENTO := 0;

            oRow.NETTO_TITOLO := 0;
            oRow.NETTO_PREVENDITA := 0;
            oRow.NETTO_INTRATTENIMENTO := 0;

            oRow.NETTO_FIGURATIVO := 0;
            oRow.NETTO_INTRA_FIGURATIVO := 0;

            oRow.IVA_TITOLO := 0;
            oRow.IVA_PREVENDITA := 0;
            oRow.IMPOSTA_INTRATTENIMENTO := 0;

            oRow.IVA_FIGURATIVA := 0;
            oRow.IMPOSTA_INTRA_FIGURATIVA := 0;

            oRow.CODICE_PRESTAZIONE1 := NULL;
            oRow.IMPORTO_PRESTAZIONE1 := 0;
            oRow.CODICE_PRESTAZIONE2 := NULL;
            oRow.IMPORTO_PRESTAZIONE2 := 0;
            oRow.CODICE_PRESTAZIONE3 := NULL;
            oRow.IMPORTO_PRESTAZIONE3 := 0;


            /* Prima ed ultima emissione */
            oRow.MIN_DATAORA_EMI := curBIGLIETTI.MIN_DATAORA_EMI;
            oRow.MAX_DATAORA_EMI := curBIGLIETTI.MAX_DATAORA_EMI;

            /* Riferimenti all'evento dell'utente CINEMA */
            oRow.IDCALEND := NVL(curBIGLIETTI.IDCALEND, 0);
            oRow.IDPACCHETTO := NVL(curBIGLIETTI.IDPACCHETTO, 0);
            oRow.IDTARIFFA := NVL(curBIGLIETTI.IDTARIFFA, 0);

            /* Riferimento al biglietto dell'utente CINEMA */
            oRow.IDBIGLIETTO := 0;

            /* Quantita' Totali della riga TUTTI A ZERO */
            oRow.QTA_EMESSI := 0;
            oRow.QTA_ANNULLATI := 0;

            /*---------------------------------------------------------*/
            /*        QUESTO VALORE E' AL NETTO DEGLI ANNULLATI        */
            /*---------------------------------------------------------*/
            oRow.TOT_QTA := curBIGLIETTI.TOT_QTA;
            oRow.TOT_QTA_OMAGGI_PROG := 0;

            /* Importi Totali EMISSIONI della riga */

            /*---------------------------------------------------------*/
            /*     QUESTI VALORI NON SONO AL NETTO DEGLI ANNULLATI     */
            /*---------------------------------------------------------*/
            oRow.TOT_CORRISPETTIVO_TITOLO := 0;
            oRow.TOT_CORRISPETTIVO_PREVENDITA := 0;
            oRow.TOT_CORRISPETTIVO_FIGURATIVO := 0;

            oRow.TOT_NETTO_TITOLO := 0;
            oRow.TOT_NETTO_PREVENDITA := 0;
            oRow.TOT_NETTO_INTRATTENIMENTO := 0;

            oRow.TOT_NETTO_FIGURATIVO := 0;
            oRow.TOT_NETTO_INTRA_FIGURATIVO := 0;

            oRow.TOT_IVA_TITOLO := 0;
            oRow.TOT_IVA_PREVENDITA := 0;
            oRow.TOT_IMPOSTA_INTRATTENIMENTO := 0;

            oRow.TOT_IVA_FIGURATIVA := 0;
            oRow.TOT_IMPOSTA_INTRA_FIGURATIVA := 0;

            oRow.TOT_IMPORTO_PRESTAZIONE1 := 0;
            oRow.TOT_IMPORTO_PRESTAZIONE2 := 0;
            oRow.TOT_IMPORTO_PRESTAZIONE3 := 0;

            /*---------------------------------------------------------*/
            /*     QUESTI VALORI SONO RELATIVI AGLI ANNULLATI          */
            /*---------------------------------------------------------*/
            oRow.ANN_CORRISPETTIVO_TITOLO := 0;
            oRow.ANN_CORRISPETTIVO_PREVENDITA := 0;
            oRow.ANN_CORRISPETTIVO_FIGURATIVO := 0;

            oRow.ANN_NETTO_TITOLO := 0;
            oRow.ANN_NETTO_PREVENDITA := 0;
            oRow.ANN_NETTO_INTRATTENIMENTO := 0;

            oRow.ANN_NETTO_FIGURATIVO := 0;
            oRow.TOT_NETTO_INTRA_FIGURATIVO := 0;

            oRow.ANN_IVA_TITOLO := 0;
            oRow.ANN_IVA_PREVENDITA := 0;
            oRow.ANN_IMPOSTA_INTRATTENIMENTO := 0;

            oRow.ANN_IVA_FIGURATIVA := 0;
            oRow.ANN_IMPOSTA_INTRA_FIGURATIVA := 0;

            oRow.ANN_IMPORTO_PRESTAZIONE1 := 0;
            oRow.ANN_IMPORTO_PRESTAZIONE2 := 0;
            oRow.ANN_IMPORTO_PRESTAZIONE3 := 0;

            /* Riferimenti per Omaggi */
            oRow.OMAGGIO_ORDINE_DI_POSTO_RIF    := NULL;
            oRow.OMAGGIO_TIPO_TITOLO_RIF        := NULL;
            oRow.OMAGGIO_PREZZO_RIF             := NULL;
            oRow.OMAGGIO_NETTO_RIF              := NULL;
            oRow.OMAGGIO_IVA_RIF                := NULL;
            oRow.OMAGGIO_ISI_RIF                := NULL;
            oRow.OMAGGIO_TIPO_RIF               := NULL;
            oRow.QTA_OMAGGI_ECCEDENTI           := 0;
            oRow.IVA_OMAGGI_ECCEDENTI           := 0;
            oRow.IMPOSTA_INTRA_OMAGGI_ECCEDENTI := 0;


            v_tab.EXTEND;
            v_tab (v_tab.LAST) := oRow;

        END LOOP;


    END IF;

    


    RETURN v_tab;
EXCEPTION
    WHEN OTHERS THEN
    raise_application_error(-20001,'ERRORE F_TRANSAZIONI_RIEPILOGHI_VUOTI - '||SQLCODE||' -ERROR- '||SQLERRM);

END;
.
R



CREATE OR REPLACE PROCEDURE WTIC.P_TRANSAZIONI_RIEPILOGHI_VUOTI(dDataEmiInizio IN DATE, dDataEmiFine IN DATE, cGiornalieroMensile IN VARCHAR2, cFlagStorico IN VARCHAR2, nMAX_LOG_ID IN NUMBER, nIdFilterExtIdVend IN NUMBER, v_tab IN OUT WTIC.OBJ_TABLE_RIEPILOGHI)
IS
    oRow  WTIC.OBJ_ROW_RIEPILOGHI;
BEGIN
    FOR curBIGLIETTI_VUOTI IN
    (
     SELECT
     DATA_INS,PROG_RIEPILOGO,CODICE_SISTEMA,CODICE_FISCALE_TITOLARE,DENOMINAZIONE_TITOLARE,TIPO_RIEPILOGO,GIORNO_MESE,CODICE_FISCALE_ORGANIZZATORE,DENOMINAZIONE_ORGANIZZATORE,
     TIPO_CF_PI,TITOLO_ABBONAMENTO,SPETTACOLO_INTRATTENIMENTO,DENOMINAZIONE_LOCALE,LUOGO_LOCALE,CODICE_LOCALE,DATA_EVENTO,ORA_EVENTO,DATA_ORA_EVENTO,TIPO_EVENTO,TITOLO_EVENTO,
     PERC_IVA,PERC_ISI,INCIDENZA_ISI,PERC_OMAGGI_TIPO_EVENTO,TITOLO_IVA_PREASSOLTA,ORDINE_DI_POSTO,CAPIENZA,NUMERO_OMAGGI_RICONOSCIUTI,TIPO_TITOLO,CORRISPETTIVO_TITOLO,CORRISPETTIVO_PREVENDITA,
     CORRISPETTIVO_FIGURATIVO,NETTO_TITOLO,NETTO_PREVENDITA,NETTO_INTRATTENIMENTO,NETTO_FIGURATIVO,NETTO_INTRA_FIGURATIVO,IVA_TITOLO,IVA_PREVENDITA,IMPOSTA_INTRATTENIMENTO,IVA_FIGURATIVA,
     IMPOSTA_INTRA_FIGURATIVA,CODICE_BIGLIETTO_ABBONAMENTO,CODICE_PRESTAZIONE1,IMPORTO_PRESTAZIONE1,CODICE_PRESTAZIONE2,IMPORTO_PRESTAZIONE2,CODICE_PRESTAZIONE3,IMPORTO_PRESTAZIONE3,
     OMAGGIO_ORDINE_DI_POSTO_RIF,OMAGGIO_TIPO_TITOLO_RIF,OMAGGIO_PREZZO_RIF,OMAGGIO_NETTO_RIF,OMAGGIO_IVA_RIF,OMAGGIO_ISI_RIF,OMAGGIO_TIPO_RIF,IDCALEND,IDPACCHETTO,IDTARIFFA,IDBIGLIETTO,
     CODICE_FISCALE_ABBONAMENTO,TIPO_TURNO,NUMERO_EVENTI_ABILITATI,DATA_LIMITE_VALIDITA,DATA_SCADENZA_ABBONAMENTO,CODICE_ABBONAMENTO,RATEO_EVENTO,IVA_RATEO,RATEO_IMPONIBILE_INTRA,
     MIN_DATAORA_EMI,MAX_DATAORA_EMI,QTA_EMESSI,QTA_ANNULLATI,TOT_QTA,TOT_QTA_OMAGGI_PROG,TOT_CORRISPETTIVO_TITOLO,TOT_CORRISPETTIVO_PREVENDITA,TOT_CORRISPETTIVO_FIGURATIVO,TOT_NETTO_TITOLO,
     TOT_NETTO_PREVENDITA,TOT_NETTO_INTRATTENIMENTO,TOT_NETTO_FIGURATIVO,TOT_NETTO_INTRA_FIGURATIVO,TOT_IVA_TITOLO,TOT_IVA_PREVENDITA,TOT_IMPOSTA_INTRATTENIMENTO,TOT_IVA_FIGURATIVA,
     TOT_IMPOSTA_INTRA_FIGURATIVA,TOT_IMPORTO_PRESTAZIONE1,TOT_IMPORTO_PRESTAZIONE2,TOT_IMPORTO_PRESTAZIONE3,ANN_CORRISPETTIVO_TITOLO,ANN_CORRISPETTIVO_PREVENDITA,ANN_CORRISPETTIVO_FIGURATIVO,
     ANN_NETTO_TITOLO,ANN_NETTO_PREVENDITA,ANN_NETTO_INTRATTENIMENTO,ANN_NETTO_FIGURATIVO,ANN_NETTO_INTRA_FIGURATIVO,ANN_IVA_TITOLO,ANN_IVA_PREVENDITA,ANN_IMPOSTA_INTRATTENIMENTO,
     ANN_IVA_FIGURATIVA,ANN_IMPOSTA_INTRA_FIGURATIVA,ANN_IMPORTO_PRESTAZIONE1,ANN_IMPORTO_PRESTAZIONE2,ANN_IMPORTO_PRESTAZIONE3,QTA_OMAGGI_ECCEDENTI,IVA_OMAGGI_ECCEDENTI,
     IMPOSTA_INTRA_OMAGGI_ECCEDENTI,NUM_RIGA,NUM_RIGA_EVENTO_TIPO_IVA
     FROM TABLE(WTIC.F_TRANSAZIONI_RIEPILOGHI_VUOTI(dDataEmiInizio, dDataEmiFine, cGiornalieroMensile, cFlagStorico, nMAX_LOG_ID, nIdFilterExtIdVend))
    )
    LOOP

        oRow := WTIC.OBJ_ROW_RIEPILOGHI(
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                NULL,NULL);

        oRow.DATA_INS                         := curBIGLIETTI_VUOTI.DATA_INS                         ;
        oRow.PROG_RIEPILOGO                   := curBIGLIETTI_VUOTI.PROG_RIEPILOGO                   ;
        oRow.CODICE_SISTEMA                   := curBIGLIETTI_VUOTI.CODICE_SISTEMA                   ;
        oRow.CODICE_FISCALE_TITOLARE          := curBIGLIETTI_VUOTI.CODICE_FISCALE_TITOLARE          ;
        oRow.DENOMINAZIONE_TITOLARE           := curBIGLIETTI_VUOTI.DENOMINAZIONE_TITOLARE           ;
        oRow.TIPO_RIEPILOGO                   := curBIGLIETTI_VUOTI.TIPO_RIEPILOGO                   ;
        oRow.GIORNO_MESE                      := curBIGLIETTI_VUOTI.GIORNO_MESE                      ;
        oRow.CODICE_FISCALE_ORGANIZZATORE     := curBIGLIETTI_VUOTI.CODICE_FISCALE_ORGANIZZATORE     ;
        oRow.DENOMINAZIONE_ORGANIZZATORE      := curBIGLIETTI_VUOTI.DENOMINAZIONE_ORGANIZZATORE      ;
        oRow.TIPO_CF_PI                       := curBIGLIETTI_VUOTI.TIPO_CF_PI                       ;
        oRow.TITOLO_ABBONAMENTO               := curBIGLIETTI_VUOTI.TITOLO_ABBONAMENTO               ;
        oRow.SPETTACOLO_INTRATTENIMENTO       := curBIGLIETTI_VUOTI.SPETTACOLO_INTRATTENIMENTO       ;
        oRow.DENOMINAZIONE_LOCALE             := curBIGLIETTI_VUOTI.DENOMINAZIONE_LOCALE             ;
        oRow.LUOGO_LOCALE                     := curBIGLIETTI_VUOTI.LUOGO_LOCALE                     ;
        oRow.CODICE_LOCALE                    := curBIGLIETTI_VUOTI.CODICE_LOCALE                    ;
        oRow.DATA_EVENTO                      := curBIGLIETTI_VUOTI.DATA_EVENTO                      ;
        oRow.ORA_EVENTO                       := curBIGLIETTI_VUOTI.ORA_EVENTO                       ;
        oRow.DATA_ORA_EVENTO                  := curBIGLIETTI_VUOTI.DATA_ORA_EVENTO                  ;
        oRow.TIPO_EVENTO                      := curBIGLIETTI_VUOTI.TIPO_EVENTO                      ;
        oRow.TITOLO_EVENTO                    := curBIGLIETTI_VUOTI.TITOLO_EVENTO                    ;
        oRow.PERC_IVA                         := curBIGLIETTI_VUOTI.PERC_IVA                         ;
        oRow.PERC_ISI                         := curBIGLIETTI_VUOTI.PERC_ISI                         ;
        oRow.INCIDENZA_ISI                    := curBIGLIETTI_VUOTI.INCIDENZA_ISI                    ;
        oRow.PERC_OMAGGI_TIPO_EVENTO          := curBIGLIETTI_VUOTI.PERC_OMAGGI_TIPO_EVENTO          ;
        oRow.TITOLO_IVA_PREASSOLTA            := curBIGLIETTI_VUOTI.TITOLO_IVA_PREASSOLTA            ;
        oRow.ORDINE_DI_POSTO                  := curBIGLIETTI_VUOTI.ORDINE_DI_POSTO                  ;
        oRow.CAPIENZA                         := curBIGLIETTI_VUOTI.CAPIENZA                         ;
        oRow.NUMERO_OMAGGI_RICONOSCIUTI       := curBIGLIETTI_VUOTI.NUMERO_OMAGGI_RICONOSCIUTI       ;
        oRow.TIPO_TITOLO                      := curBIGLIETTI_VUOTI.TIPO_TITOLO                      ;
        oRow.CORRISPETTIVO_TITOLO             := curBIGLIETTI_VUOTI.CORRISPETTIVO_TITOLO             ;
        oRow.CORRISPETTIVO_PREVENDITA         := curBIGLIETTI_VUOTI.CORRISPETTIVO_PREVENDITA         ;
        oRow.CORRISPETTIVO_FIGURATIVO         := curBIGLIETTI_VUOTI.CORRISPETTIVO_FIGURATIVO         ;
        oRow.NETTO_TITOLO                     := curBIGLIETTI_VUOTI.NETTO_TITOLO                     ;
        oRow.NETTO_PREVENDITA                 := curBIGLIETTI_VUOTI.NETTO_PREVENDITA                 ;
        oRow.NETTO_INTRATTENIMENTO            := curBIGLIETTI_VUOTI.NETTO_INTRATTENIMENTO            ;
        oRow.NETTO_FIGURATIVO                 := curBIGLIETTI_VUOTI.NETTO_FIGURATIVO                 ;
        oRow.NETTO_INTRA_FIGURATIVO           := curBIGLIETTI_VUOTI.NETTO_INTRA_FIGURATIVO           ;
        oRow.IVA_TITOLO                       := curBIGLIETTI_VUOTI.IVA_TITOLO                       ;
        oRow.IVA_PREVENDITA                   := curBIGLIETTI_VUOTI.IVA_PREVENDITA                   ;
        oRow.IMPOSTA_INTRATTENIMENTO          := curBIGLIETTI_VUOTI.IMPOSTA_INTRATTENIMENTO          ;
        oRow.IVA_FIGURATIVA                   := curBIGLIETTI_VUOTI.IVA_FIGURATIVA                   ;
        oRow.IMPOSTA_INTRA_FIGURATIVA         := curBIGLIETTI_VUOTI.IMPOSTA_INTRA_FIGURATIVA         ;
        oRow.CODICE_BIGLIETTO_ABBONAMENTO     := curBIGLIETTI_VUOTI.CODICE_BIGLIETTO_ABBONAMENTO     ;
        oRow.CODICE_PRESTAZIONE1              := curBIGLIETTI_VUOTI.CODICE_PRESTAZIONE1              ;
        oRow.IMPORTO_PRESTAZIONE1             := curBIGLIETTI_VUOTI.IMPORTO_PRESTAZIONE1             ;
        oRow.CODICE_PRESTAZIONE2              := curBIGLIETTI_VUOTI.CODICE_PRESTAZIONE2              ;
        oRow.IMPORTO_PRESTAZIONE2             := curBIGLIETTI_VUOTI.IMPORTO_PRESTAZIONE2             ;
        oRow.CODICE_PRESTAZIONE3              := curBIGLIETTI_VUOTI.CODICE_PRESTAZIONE3              ;
        oRow.IMPORTO_PRESTAZIONE3             := curBIGLIETTI_VUOTI.IMPORTO_PRESTAZIONE3             ;
        oRow.OMAGGIO_ORDINE_DI_POSTO_RIF      := curBIGLIETTI_VUOTI.OMAGGIO_ORDINE_DI_POSTO_RIF      ;
        oRow.OMAGGIO_TIPO_TITOLO_RIF          := curBIGLIETTI_VUOTI.OMAGGIO_TIPO_TITOLO_RIF          ;
        oRow.OMAGGIO_PREZZO_RIF               := curBIGLIETTI_VUOTI.OMAGGIO_PREZZO_RIF               ;
        oRow.OMAGGIO_NETTO_RIF                := curBIGLIETTI_VUOTI.OMAGGIO_NETTO_RIF                ;
        oRow.OMAGGIO_IVA_RIF                  := curBIGLIETTI_VUOTI.OMAGGIO_IVA_RIF                  ;
        oRow.OMAGGIO_ISI_RIF                  := curBIGLIETTI_VUOTI.OMAGGIO_ISI_RIF                  ;
        oRow.OMAGGIO_TIPO_RIF                 := curBIGLIETTI_VUOTI.OMAGGIO_TIPO_RIF                 ;
        oRow.IDCALEND                         := curBIGLIETTI_VUOTI.IDCALEND                         ;
        oRow.IDPACCHETTO                      := curBIGLIETTI_VUOTI.IDPACCHETTO                      ;
        oRow.IDTARIFFA                        := curBIGLIETTI_VUOTI.IDTARIFFA                        ;
        oRow.IDBIGLIETTO                      := curBIGLIETTI_VUOTI.IDBIGLIETTO                      ;
        oRow.CODICE_FISCALE_ABBONAMENTO       := curBIGLIETTI_VUOTI.CODICE_FISCALE_ABBONAMENTO       ;
        oRow.TIPO_TURNO                       := curBIGLIETTI_VUOTI.TIPO_TURNO                       ;
        oRow.NUMERO_EVENTI_ABILITATI          := curBIGLIETTI_VUOTI.NUMERO_EVENTI_ABILITATI          ;
        oRow.DATA_LIMITE_VALIDITA             := curBIGLIETTI_VUOTI.DATA_LIMITE_VALIDITA             ;
        oRow.DATA_SCADENZA_ABBONAMENTO        := curBIGLIETTI_VUOTI.DATA_SCADENZA_ABBONAMENTO        ;
        oRow.CODICE_ABBONAMENTO               := curBIGLIETTI_VUOTI.CODICE_ABBONAMENTO               ;
        oRow.RATEO_EVENTO                     := curBIGLIETTI_VUOTI.RATEO_EVENTO                     ;
        oRow.IVA_RATEO                        := curBIGLIETTI_VUOTI.IVA_RATEO                        ;
        oRow.RATEO_IMPONIBILE_INTRA           := curBIGLIETTI_VUOTI.RATEO_IMPONIBILE_INTRA           ;
        oRow.MIN_DATAORA_EMI                  := curBIGLIETTI_VUOTI.MIN_DATAORA_EMI                  ;
        oRow.MAX_DATAORA_EMI                  := curBIGLIETTI_VUOTI.MAX_DATAORA_EMI                  ;
        oRow.QTA_EMESSI                       := curBIGLIETTI_VUOTI.QTA_EMESSI                       ;
        oRow.QTA_ANNULLATI                    := curBIGLIETTI_VUOTI.QTA_ANNULLATI                    ;
        oRow.TOT_QTA                          := curBIGLIETTI_VUOTI.TOT_QTA                          ;
        oRow.TOT_QTA_OMAGGI_PROG              := curBIGLIETTI_VUOTI.TOT_QTA_OMAGGI_PROG              ;
        oRow.TOT_CORRISPETTIVO_TITOLO         := curBIGLIETTI_VUOTI.TOT_CORRISPETTIVO_TITOLO         ;
        oRow.TOT_CORRISPETTIVO_PREVENDITA     := curBIGLIETTI_VUOTI.TOT_CORRISPETTIVO_PREVENDITA     ;
        oRow.TOT_CORRISPETTIVO_FIGURATIVO     := curBIGLIETTI_VUOTI.TOT_CORRISPETTIVO_FIGURATIVO     ;
        oRow.TOT_NETTO_TITOLO                 := curBIGLIETTI_VUOTI.TOT_NETTO_TITOLO                 ;
        oRow.TOT_NETTO_PREVENDITA             := curBIGLIETTI_VUOTI.TOT_NETTO_PREVENDITA             ;
        oRow.TOT_NETTO_INTRATTENIMENTO        := curBIGLIETTI_VUOTI.TOT_NETTO_INTRATTENIMENTO        ;
        oRow.TOT_NETTO_FIGURATIVO             := curBIGLIETTI_VUOTI.TOT_NETTO_FIGURATIVO             ;
        oRow.TOT_NETTO_INTRA_FIGURATIVO       := curBIGLIETTI_VUOTI.TOT_NETTO_INTRA_FIGURATIVO       ;
        oRow.TOT_IVA_TITOLO                   := curBIGLIETTI_VUOTI.TOT_IVA_TITOLO                   ;
        oRow.TOT_IVA_PREVENDITA               := curBIGLIETTI_VUOTI.TOT_IVA_PREVENDITA               ;
        oRow.TOT_IMPOSTA_INTRATTENIMENTO      := curBIGLIETTI_VUOTI.TOT_IMPOSTA_INTRATTENIMENTO      ;
        oRow.TOT_IVA_FIGURATIVA               := curBIGLIETTI_VUOTI.TOT_IVA_FIGURATIVA               ;
        oRow.TOT_IMPOSTA_INTRA_FIGURATIVA     := curBIGLIETTI_VUOTI.TOT_IMPOSTA_INTRA_FIGURATIVA     ;
        oRow.TOT_IMPORTO_PRESTAZIONE1         := curBIGLIETTI_VUOTI.TOT_IMPORTO_PRESTAZIONE1         ;
        oRow.TOT_IMPORTO_PRESTAZIONE2         := curBIGLIETTI_VUOTI.TOT_IMPORTO_PRESTAZIONE2         ;
        oRow.TOT_IMPORTO_PRESTAZIONE3         := curBIGLIETTI_VUOTI.TOT_IMPORTO_PRESTAZIONE3         ;
        oRow.ANN_CORRISPETTIVO_TITOLO         := curBIGLIETTI_VUOTI.ANN_CORRISPETTIVO_TITOLO         ;
        oRow.ANN_CORRISPETTIVO_PREVENDITA     := curBIGLIETTI_VUOTI.ANN_CORRISPETTIVO_PREVENDITA     ;
        oRow.ANN_CORRISPETTIVO_FIGURATIVO     := curBIGLIETTI_VUOTI.ANN_CORRISPETTIVO_FIGURATIVO     ;
        oRow.ANN_NETTO_TITOLO                 := curBIGLIETTI_VUOTI.ANN_NETTO_TITOLO                 ;
        oRow.ANN_NETTO_PREVENDITA             := curBIGLIETTI_VUOTI.ANN_NETTO_PREVENDITA             ;
        oRow.ANN_NETTO_INTRATTENIMENTO        := curBIGLIETTI_VUOTI.ANN_NETTO_INTRATTENIMENTO        ;
        oRow.ANN_NETTO_FIGURATIVO             := curBIGLIETTI_VUOTI.ANN_NETTO_FIGURATIVO             ;
        oRow.ANN_NETTO_INTRA_FIGURATIVO       := curBIGLIETTI_VUOTI.ANN_NETTO_INTRA_FIGURATIVO       ;
        oRow.ANN_IVA_TITOLO                   := curBIGLIETTI_VUOTI.ANN_IVA_TITOLO                   ;
        oRow.ANN_IVA_PREVENDITA               := curBIGLIETTI_VUOTI.ANN_IVA_PREVENDITA               ;
        oRow.ANN_IMPOSTA_INTRATTENIMENTO      := curBIGLIETTI_VUOTI.ANN_IMPOSTA_INTRATTENIMENTO      ;
        oRow.ANN_IVA_FIGURATIVA               := curBIGLIETTI_VUOTI.ANN_IVA_FIGURATIVA               ;
        oRow.ANN_IMPOSTA_INTRA_FIGURATIVA     := curBIGLIETTI_VUOTI.ANN_IMPOSTA_INTRA_FIGURATIVA     ;
        oRow.ANN_IMPORTO_PRESTAZIONE1         := curBIGLIETTI_VUOTI.ANN_IMPORTO_PRESTAZIONE1         ;
        oRow.ANN_IMPORTO_PRESTAZIONE2         := curBIGLIETTI_VUOTI.ANN_IMPORTO_PRESTAZIONE2         ;
        oRow.ANN_IMPORTO_PRESTAZIONE3         := curBIGLIETTI_VUOTI.ANN_IMPORTO_PRESTAZIONE3         ;
        oRow.QTA_OMAGGI_ECCEDENTI             := curBIGLIETTI_VUOTI.QTA_OMAGGI_ECCEDENTI             ;
        oRow.IVA_OMAGGI_ECCEDENTI             := curBIGLIETTI_VUOTI.IVA_OMAGGI_ECCEDENTI             ;
        oRow.IMPOSTA_INTRA_OMAGGI_ECCEDENTI   := curBIGLIETTI_VUOTI.IMPOSTA_INTRA_OMAGGI_ECCEDENTI   ;
        oRow.NUM_RIGA                         := curBIGLIETTI_VUOTI.NUM_RIGA                         ;
        oRow.NUM_RIGA_EVENTO_TIPO_IVA         := curBIGLIETTI_VUOTI.NUM_RIGA_EVENTO_TIPO_IVA         ;

        v_tab.EXTEND;
        v_tab (v_tab.LAST) := oRow;

    END LOOP;

END;
.
R
