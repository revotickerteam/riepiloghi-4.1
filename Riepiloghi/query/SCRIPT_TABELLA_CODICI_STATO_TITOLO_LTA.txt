CREATE TABLE WTIC.CODICI_STATO_TITOLO
(
  CODICE       VARCHAR2(2 BYTE),
  DESCRIZIONE  VARCHAR2(70 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX WTIC.CODICI_STATO_TITOLO_PK ON WTIC.CODICI_STATO_TITOLO
(CODICE)
LOGGING
NOPARALLEL;
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('VT', 'Valido (titolo tradizionale)');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('VD', 'Valido (titolo digitale)');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('ZT', 'Accesso automatizz. tit. tradizionale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('ZD', 'Accesso automatizzato digitale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('MT', 'Accesso Manuale tit. tradizionale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('MD', 'Accesso Manuale tit. digitale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('DT', 'Daspato tit. tradizionale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('DD', 'Daspato tit. digitale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('FT', 'Denuncia furto per tit. tradizionale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('FD', 'Denuncia furto per tit. digitale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('AT', 'Annullato tit. tradizionale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('AD', 'Annullato tit. digitale');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('BT', 'Black List a disposizione dell''organizzatore per tit. tradizionali');
Insert into WTIC.CODICI_STATO_TITOLO
   (CODICE, 
    DESCRIZIONE)
 Values
   ('BD', 'Black list a disposizione dell''organizzatore per tit. digitali ');
COMMIT;