DECLARE
--wOWNER VARCHAR2(255) := 'WEBTIC_WS';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_XS_CINEMA';
--wOBJECT_NAME VARCHAR2(255) := Upper('APP18N_FILES_U');

--wOWNER VARCHAR2(255) := 'TMS';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_TMS';
--wOBJECT_NAME VARCHAR2(255) := Upper('LOAD_STRUCTURE');

wOWNER VARCHAR2(255) := 'WTIC';
wPACKAGE_NAME VARCHAR2(255) := '*'; --NOME DEL PACKAGE 
wOBJECT_NAME VARCHAR2(255) := Upper('P_TEST_CURSORI');  --NOME DELLA PROCEDURE

--wOWNER VARCHAR2(255) := 'EDICOLA';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_EDICOLA';
--wOBJECT_NAME VARCHAR2(255) := Upper('U_USCITE');

--wOWNER VARCHAR2(255) := 'WTDIRECTOR';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_WTD';
--wOBJECT_NAME VARCHAR2(255) := Upper('READ_ACCOUNT');

--wOWNER VARCHAR2(255) := 'COLFMILAN';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_COLF';
--wOBJECT_NAME VARCHAR2(255) := Upper('CC_SELLA_POS_U');

--wOWNER VARCHAR2(255) := 'CREADW';
--wPACKAGE_NAME VARCHAR2(255) := 'PKG_CREADW';
--wOBJECT_NAME VARCHAR2(255) := Upper('GetRevenues');


wARGUMENT_NAME VARCHAR2(255);
wDATA_TYPE VARCHAR2(255);
wIN_OUT VARCHAR2(255);
wCHAR_LENGTH number;


ColumnName VARCHAR2(255);

Comma CHAR(2);
IsrtParameters VARCHAR2(32767);
IsrtColumns VARCHAR2(32767);
IsrtValues VARCHAR2(32767);
ReplSets VARCHAR2(32767);

PropertyStatement VARCHAR2(32767);
CostructorStatement VARCHAR2(32767);
ClassStatement VARCHAR2(32767);

crlf  VARCHAR2(2)  := chr(13)||chr(10);

CURSOR cColumns (PAR_OWNER VARCHAR2, PAR_PACKAGE_NAME VARCHAR2, PAR_OBJECT_NAME VARCHAR2) is
      SELECT
      ARGUMENT_NAME,
     CASE DATA_TYPE
           when 'VARCHAR2' then 'DbMapFieldString'
           when 'CHAR' then 'DbMapFieldString'
            when 'NVARCHAR2' then 'DbMapFieldString'
            when 'RAW' then 'DbMapFieldBinary'
            when 'BLOB' then 'DbMapFieldBlob'
            when 'CLOB' then 'DbMapFieldClob'
           when 'NUMBER' then 'DbMapFieldDecimal'
           when 'DATE' then 'DbMapFieldDateTime'
           when 'REF CURSOR' then 'DbMapFieldCursor'
      else 'DbMapField'
     END,
     CASE IN_OUT
           when 'IN' then 'ParameterDirection.Input'
           when 'OUT' then 'ParameterDirection.Output'
           when 'IN/OUT' then 'ParameterDirection.InputOutput'
      else 'ParameterDirection.ReturnValue'
     END,
      Nvl(CHAR_LENGTH,32767)

      FROM ALL_ARGUMENTS
      WHERE OWNER = PAR_OWNER AND NVL(PACKAGE_NAME,PAR_PACKAGE_NAME) = PAR_PACKAGE_NAME AND OBJECT_NAME = PAR_OBJECT_NAME ORDER BY POSITION;

BEGIN

    ClassStatement := '';
    PropertyStatement := '';
    CostructorStatement := '';
            CostructorStatement := CostructorStatement || 'PrefixParameter = String.Empty;' || crlf;
            CostructorStatement := CostructorStatement || 'Command.CommandTimeout = 30;' || crlf;
            CostructorStatement := CostructorStatement || 'Command.CommandText = "wPACKAGE_NAME.wOBJECT_NAME";' || crlf;
            CostructorStatement := CostructorStatement || 'Command.CommandType = CommandType.StoredProcedure;' || crlf;

    OPEN cColumns(wOWNER,wPACKAGE_NAME,wOBJECT_NAME);
    LOOP
        FETCH cColumns INTO wARGUMENT_NAME,wDATA_TYPE,wIN_OUT,wCHAR_LENGTH;
        EXIT WHEN cColumns%NOTFOUND;

        PropertyStatement := PropertyStatement || 'public ' || wDATA_TYPE || ' ' || wARGUMENT_NAME || ';' || crlf;


        CostructorStatement := CostructorStatement || wARGUMENT_NAME || ' =  new  ' || wDATA_TYPE || '(this,"' || wARGUMENT_NAME || '");' || crlf;
        CostructorStatement := CostructorStatement || wARGUMENT_NAME || '.Parameter.Direction = ' || wIN_OUT || ';' || crlf;
              IF wCHAR_LENGTH > 0 then
            CostructorStatement := CostructorStatement || wARGUMENT_NAME || '.Parameter.Size = ' || To_Char(wCHAR_LENGTH) || ';' || crlf;
              END IF;
          IF wDATA_TYPE = 'DbMapFieldCursor' then
            CostructorStatement := CostructorStatement || wARGUMENT_NAME || '.CursorName = "' || wARGUMENT_NAME || '";' || crlf;
            CostructorStatement := CostructorStatement || wARGUMENT_NAME || '.IsSingleRow = false;' || crlf;
            CostructorStatement := CostructorStatement || wARGUMENT_NAME || '.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;' || crlf;
          END IF;

    END LOOP;

    CLOSE cColumns;

    ClassStatement := 'public class wPACKAGE_NAME_wOBJECT_NAME: DbMapTable {crlfPropertyStatement crlfpublic wPACKAGE_NAME_wOBJECT_NAME(DatabaseManager parDatabaseManager):base(parDatabaseManager) {crlfCostructorStatementcrlf}crlf}';

    ClassStatement := REPLACE(ClassStatement,'PropertyStatement',PropertyStatement);
    ClassStatement := REPLACE(ClassStatement,'CostructorStatement',CostructorStatement);
    ClassStatement := REPLACE(ClassStatement,'crlf',crlf);
    ClassStatement := REPLACE(ClassStatement,'wPACKAGE_NAME',wPACKAGE_NAME);
    ClassStatement := REPLACE(ClassStatement,'wOBJECT_NAME',wOBJECT_NAME);


    print_out(ClassStatement,255,crlf,crlf);

    --print_out(ClassStatement,255,crlf,crlf);


END;
