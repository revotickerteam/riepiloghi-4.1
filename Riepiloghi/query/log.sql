select
substr(log_data,1,16) as codice_fiscale_organizzatore,
substr(log_data,17,16) as codice_fiscale_titolare,
substr(log_data,33,1) as titolo_abbonamento,
substr(log_data,34,1) as titolo_iva_preassolta,
substr(log_data,35,1) as spettacolo_intrattenimento,
substr(log_data,36,1) as valuta,
TO_NUMBER(substr(log_data, 37, 9)) / 100 as imponibile_intrattenimenti,
substr(log_data,46,2) as ordine_di_posto,
substr(log_data,48,6) as posto,
substr(log_data,54,2) as tipo_titolo,
substr(log_data,56,1) as annullato,
to_number(substr(log_data,57,8)) as progressivo_annullati,
to_date(substr(log_data,65,8),'YYYYMMDD') as data_emissione_annullamento,
to_char(to_date(substr(log_data,73,4),'HH24MI'),'HH24.MI') as ora_emissione_annullamento,
to_number(substr(log_data,77,8)) as progressivo_titolo,
substr(log_data,85,8) as codice_punto_vendita,
substr(log_data,93,16) as sigillo,
substr(log_data,109,8) as codice_sistema,
substr(log_data,117,8) as codice_carta,
substr(log_data,125,40) as prestampa,
decode(substr(log_data,33,1), 'T', substr(log_data, 165, 13), rpad(' ',13,' ')) as codice_locale,
decode(substr(log_data,33,1), 'T', to_date(substr(log_data, 178, 8),'YYYYMMDD'), null) as data_evento,
decode(substr(log_data,33,1), 'T', substr(log_data, 186, 2), rpad(' ',2,' ')) as tipo_evento,
decode(substr(log_data,33,1), 'T', substr(log_data, 188, 40), rpad(' ',40,' ')) as titolo_evento,
decode(substr(log_data,33,1), 'T', TO_CHAR(TO_DATE(substr(log_data, 228, 4),'HH24MI'),'HH24.MI'), null) as ora_evento,
decode(substr(log_data,33,1), 'T', substr(log_data, 232, 30), rpad(' ',30,' ')) as causale_omaggio_riduzione,
decode(substr(log_data,33,1), 'A', substr(log_data, 165, 1), ' ') as tipo_turno,
decode(substr(log_data,33,1), 'A', to_number(substr(log_data, 166, 4)), 0) as numero_eventi_abilitati,
decode(substr(log_data,33,1), 'A', to_date(substr(log_data, 170, 8),'YYYYMMDD'), null) as data_limite_validita,
decode(substr(log_data,33,1), 'A', substr(log_data, 178, 8), rpad(' ',8,' ')) as codice_abbonamento,
decode(substr(log_data,33,1), 'A', substr(log_data, 186, 8), 0) as num_prog_abbonamento,
decode(substr(log_data,33,1), 'A', to_number(substr(log_data, 194, 9)) / 100, 0) as rateo_evento,
decode(substr(log_data,33,1), 'A', TO_NUMBER(substr(log_data, 203, 9)) / 100, 0) as iva_rateo,
decode(substr(log_data,33,1), 'A', TO_NUMBER(substr(log_data, 212, 9)) / 100, 0) as rateo_imponibile_intra,
decode(substr(log_data,33,1), 'A', substr(log_data, 221, 30), rpad(' ',30,' ')) as causale_omaggio_riduzione_open,
decode(substr(log_data,34,1), 'N', to_number(substr(log_data, 262, 9)) / 100, 0) as corrispettivo_titolo,
decode(substr(log_data,34,1), 'N', to_number(substr(log_data, 271, 8)) / 100, 0) as corrispettivo_prevendita,
decode(substr(log_data,34,1), 'N', to_number(substr(log_data, 279, 9)) / 100, 0) as iva_titolo,
decode(substr(log_data,34,1), 'N', to_number(substr(log_data, 288, 8)) / 100, 0) as iva_prevendita,
decode(substr(log_data,34,1), 'F', to_number(substr(log_data, 262, 9)) / 100, 'B', to_number(substr(log_data, 262, 9)) / 100, 0) as corrispettivo_figurativo,
decode(substr(log_data,34,1), 'F', to_number(substr(log_data, 271, 9)) / 100, 'B', to_number(substr(log_data, 271, 9)) / 100, 0) as iva_figurativa,
decode(substr(log_data,34,1), 'B', substr(log_data, 280, 16), rpad(' ',16,' ')) as codice_fiscale_abbonamento,
decode(substr(log_data,34,1), 'B', substr(log_data, 296, 8), rpad(' ',8,' ')) as codice_biglietto_abbonamento,
decode(substr(log_data,34,1), 'B', substr(log_data, 304, 8), rpad(' ',8,' ')) as num_prog_biglietto_abbonamento,
substr(log_data, 312, 3) as codice_prestazione1,
to_number(substr(log_data, 315, 9)) / 100 as importo_prestazione1,
substr(log_data, 324, 3) as codice_prestazione2,
to_number(substr(log_data, 327, 9)) / 100 as importo_prestazione2,
substr(log_data, 336, 3) as codice_prestazione3,
to_number(substr(log_data, 339, 9)) / 100 as importo_prestazione3,
substr(log_data, 348, 8) as carta_originale_annullato,
substr(log_data, 356, 3) as causale_annullamento
from logtrans
order by
substr(log_data,1,16),
substr(log_data,17,16),
substr(log_data,109,8),
substr(log_data,117,8),
to_number(substr(log_data,77,8))









