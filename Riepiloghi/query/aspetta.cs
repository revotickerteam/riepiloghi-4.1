        public static System.Data.DataSet CreaDataSetRiepilogo(DateTime Giorno, string GiornalieroMensile, Int64 nProgNextRiepilogo, IConnection oConnection, out bool lRiepilogoVuoto, out Exception oError, DataTable tabRIEPILOGHI_BASE, DataTable tabRIEPILOGHI_TITOLI_EVENTI)
        {
            System.Data.DataSet oDataSet = null;
            bool lRet = false;
            StringBuilder oSB = null;
            clsParameters oPars = null;
            IRecordSet oRS_ = null;
            string cNomeDataSet = "";
            lRiepilogoVuoto = false;
            oError = null;

            try
            {

            
                if (GiornalieroMensile == "M")
                {
                    Giorno = new DateTime(Giorno.Year, Giorno.Month, 1);
                    cNomeDataSet = "riepilogo_mensile";
                }
                else
                {
                    Giorno = Giorno.Date;
                    cNomeDataSet = "riepilogo_giornaliero";
                }

                // creazione del dataset
                oDataSet = new System.Data.DataSet(cNomeDataSet);

                // RIEPILOGO
                System.Data.DataTable oTabRiepilogo = new DataTable(cNomeDataSet);
                oTabRiepilogo.Columns.Add(new System.Data.DataColumn("sostituzione", System.Type.GetType("System.String")));
                oTabRiepilogo.Columns.Add(new System.Data.DataColumn("data", System.Type.GetType("System.String")));
                oTabRiepilogo.Columns.Add(new System.Data.DataColumn("data_generazione", System.Type.GetType("System.String")));
                oTabRiepilogo.Columns.Add(new System.Data.DataColumn("ora_generazione", System.Type.GetType("System.String")));
                oTabRiepilogo.Columns.Add(new System.Data.DataColumn("progressivo_generazione", System.Type.GetType("System.String")));

                oDataSet.Tables.Add(oTabRiepilogo);


                // TITOLARE
                System.Data.DataTable oTabTitolare = new DataTable("titolare");
                oTabTitolare.Columns.Add(new System.Data.DataColumn("denominazione_titolare", System.Type.GetType("System.String")));
                oTabTitolare.Columns.Add(new System.Data.DataColumn("codice_fiscale_titolare", System.Type.GetType("System.String")));
                oTabTitolare.Columns.Add(new System.Data.DataColumn("codice_sistema", System.Type.GetType("System.String")));

                oDataSet.Tables.Add(oTabTitolare);

                // ORGANIZZATORI
                System.Data.DataTable oTabOrganizzatori = new DataTable("organizzatori");
                oTabOrganizzatori.Columns.Add(new System.Data.DataColumn("denominazione_organizzatore", System.Type.GetType("System.String")));
                oTabOrganizzatori.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                oTabOrganizzatori.Columns.Add(new System.Data.DataColumn("tipo_cf_pi", System.Type.GetType("System.String")));
                oTabOrganizzatori.Columns.Add(new System.Data.DataColumn("tipo_organizzatore", System.Type.GetType("System.String")));

                oDataSet.Tables.Add(oTabOrganizzatori);

                // EVENTI
                System.Data.DataTable oTabEventi = new DataTable("eventi");
                // Chiave ORGANIZZATORE
                //oTabEventi.Columns.Add(new System.Data.DataColumn("denominazione_organizzatore", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                //oTabEventi.Columns.Add(new System.Data.DataColumn("tipo_cf_pi", System.Type.GetType("System.String")));

                oTabEventi.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("denominazione_locale", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("tipo_evento", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("titolo_evento", System.Type.GetType("System.String")));
                oTabEventi.Columns.Add(new System.Data.DataColumn("incidenza_isi", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabEventi);

                // Impostazione relazione EVENTI <-> ORGANIZZATORI
                DataRelation oRelOrganizzatoriEventi = new DataRelation("rl_organizzatori_eventi",
                                                                        new DataColumn[] { oTabOrganizzatori.Columns["codice_fiscale_organizzatore"]},
                                                                        new DataColumn[] { oTabEventi.Columns["codice_fiscale_organizzatore"]}, true);

                oRelOrganizzatoriEventi.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriEventi);



                // TITOLI_OPERE
                System.Data.DataTable oTabTitoliOpere = new DataTable("titoli_opere");
                // Chiave ORGANIZZATORE
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));

                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("titolo", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("produttore_cinema", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("autore", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("esecutore", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("nazionalita", System.Type.GetType("System.String")));
                oTabTitoliOpere.Columns.Add(new System.Data.DataColumn("distributore", System.Type.GetType("System.String")));

                oDataSet.Tables.Add(oTabTitoliOpere);

                // Impostazione relazione TITOLI_OPERE <-> EVENTI
                DataRelation oRelEventiTitoliOpere = new DataRelation("rl_eventi_titoli_opere",
                                                                      new DataColumn[] { oTabEventi.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabEventi.Columns["codice_locale"],
                                                                                         oTabEventi.Columns["data_evento"],
                                                                                         oTabEventi.Columns["ora_evento"]},
                                                                      new DataColumn[] { oTabTitoliOpere.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabTitoliOpere.Columns["codice_locale"],
                                                                                         oTabTitoliOpere.Columns["data_evento"],
                                                                                         oTabTitoliOpere.Columns["ora_evento"]}, true);
                oRelEventiTitoliOpere.Nested = true;
                oDataSet.Relations.Add(oRelEventiTitoliOpere);



                // ORDINI_DI_POSTO
                System.Data.DataTable oTabOrdiniDiPosto = new DataTable("ordini_di_posto");
                // Chiave ORGANIZZATORE
                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));

                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("capienza", System.Type.GetType("System.Int64")));

                if (GiornalieroMensile == "M")
                {
                    oTabOrdiniDiPosto.Columns.Add(new System.Data.DataColumn("iva_eccedente_omaggi", System.Type.GetType("System.Int64")));
                }

                oDataSet.Tables.Add(oTabOrdiniDiPosto);

                // Impostazione relazione ORDINI_DI_POSTO <-> EVENTI
                DataRelation oRelEventiOrdiniDiPosto = new DataRelation("rl_eventi_ordini_di_posto",
                                                                      new DataColumn[] { oTabEventi.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabEventi.Columns["codice_locale"],
                                                                                         oTabEventi.Columns["data_evento"],
                                                                                         oTabEventi.Columns["ora_evento"]},
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"]}, true);
                oRelEventiOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelEventiOrdiniDiPosto);


                // TITOLI_ACCESSO
                System.Data.DataTable oTabTitoliAccesso = new DataTable("titoli_accesso");
                // Chiave ORGANIZZATORE
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("qta_emessi", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("corrispettivo_titolo", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("corrispettivo_prevendita", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("iva_titolo", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("iva_prevendita", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("imposta_intrattenimento", System.Type.GetType("System.Int64")));
                oTabTitoliAccesso.Columns.Add(new System.Data.DataColumn("importo_prestazione", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabTitoliAccesso);

                // Impostazione relazione TITOLI_ACCESSO <-> ORDINI_DI_POSTO
                DataRelation oRelTitoliOrdiniDiPosto = new DataRelation("rl_titoli_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabTitoliAccesso.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabTitoliAccesso.Columns["codice_locale"],
                                                                                         oTabTitoliAccesso.Columns["data_evento"],
                                                                                         oTabTitoliAccesso.Columns["ora_evento"],
                                                                                         oTabTitoliAccesso.Columns["ordine_di_posto"]}, true);
                oRelTitoliOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelTitoliOrdiniDiPosto);



                // TITOLI_ANNULLATI
                System.Data.DataTable oTabTitoliAnnullati = new DataTable("titoli_annullati");
                // Chiave ORGANIZZATORE
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("qta_annullati", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_titolo", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_prevendita", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("iva_titolo", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("iva_prevendita", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("imposta_intrattenimento", System.Type.GetType("System.Int64")));
                oTabTitoliAnnullati.Columns.Add(new System.Data.DataColumn("importo_prestazione", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabTitoliAnnullati);


                // Impostazione relazione TITOLI_ANNULLATI <-> ORDINI_DI_POSTO
                DataRelation oRelTitoliAnnullatiOrdiniDiPosto = new DataRelation("titoli_annullati_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabTitoliAnnullati.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabTitoliAnnullati.Columns["codice_locale"],
                                                                                         oTabTitoliAnnullati.Columns["data_evento"],
                                                                                         oTabTitoliAnnullati.Columns["ora_evento"],
                                                                                         oTabTitoliAnnullati.Columns["ordine_di_posto"]}, true);
                oRelTitoliAnnullatiOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelTitoliAnnullatiOrdiniDiPosto);



                // TITOLI_IVA_PREASSOLTA
                System.Data.DataTable oTabTitoliIVAPreassolta = new DataTable("titoli_iva_preassolta");
                // Chiave ORGANIZZATORE
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("qta_emessi", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassolta.Columns.Add(new System.Data.DataColumn("imposta_intra_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabTitoliIVAPreassolta);

                // Impostazione relazione TITOLI_IVA_PREASSOLTA <-> ORDINI_DI_POSTO
                DataRelation oRelTitoliIvaPreassoltaOrdiniDiPosto = new DataRelation("rl_titoli_iva_preassolta_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabTitoliIVAPreassolta.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabTitoliIVAPreassolta.Columns["codice_locale"],
                                                                                         oTabTitoliIVAPreassolta.Columns["data_evento"],
                                                                                         oTabTitoliIVAPreassolta.Columns["ora_evento"],
                                                                                         oTabTitoliIVAPreassolta.Columns["ordine_di_posto"]}, true);
                oRelTitoliIvaPreassoltaOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelTitoliIvaPreassoltaOrdiniDiPosto);



                // TITOLI_IVA_PREASSOLTA_ANNULLATI
                System.Data.DataTable oTabTitoliIVAPreassoltaAnnullati = new DataTable("titoli_iva_preassolta_annullati");
                // Chiave ORGANIZZATORE
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("qta_annullati", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));
                oTabTitoliIVAPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("imposta_intra_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabTitoliIVAPreassoltaAnnullati);


                // Impostazione relazione TITOLI_IVA_PREASSOLTA_ANNULLATI <-> ORDINI_DI_POSTO
                DataRelation oRelTitoliIvaPreassoltaAnnullatiOrdiniDiPosto = new DataRelation("rl_titoli_iva_preassolta_annullati_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabTitoliIVAPreassoltaAnnullati.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabTitoliIVAPreassoltaAnnullati.Columns["codice_locale"],
                                                                                         oTabTitoliIVAPreassoltaAnnullati.Columns["data_evento"],
                                                                                         oTabTitoliIVAPreassoltaAnnullati.Columns["ora_evento"],
                                                                                         oTabTitoliIVAPreassoltaAnnullati.Columns["ordine_di_posto"]}, true);
                oRelTitoliIvaPreassoltaAnnullatiOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelTitoliIvaPreassoltaAnnullatiOrdiniDiPosto);




                // BIGLIETTI_ABBONAMENTO
                System.Data.DataTable oTabBigliettiAbbonamento = new DataTable("biglietti_abbonamento");
                // Chiave ORGANIZZATORE
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("codice_fiscale_abbonamento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("codice_biglietto_abbonamento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("qta_emessi", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamento.Columns.Add(new System.Data.DataColumn("imposta_intra_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabBigliettiAbbonamento);

                // Impostazione relazione BIGLIETTI_ABBONAMENTO <-> ORDINI_DI_POSTO
                DataRelation oRelBigliettiAbbonamentoOrdiniDiPosto = new DataRelation("rl_biglietti_abbonamento_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabBigliettiAbbonamento.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabBigliettiAbbonamento.Columns["codice_locale"],
                                                                                         oTabBigliettiAbbonamento.Columns["data_evento"],
                                                                                         oTabBigliettiAbbonamento.Columns["ora_evento"],
                                                                                         oTabBigliettiAbbonamento.Columns["ordine_di_posto"]}, true);
                oRelBigliettiAbbonamentoOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelBigliettiAbbonamentoOrdiniDiPosto);


                // BIGLIETTI_ABBONAMENTO_ANNULLATI
                System.Data.DataTable oTabBigliettiAbbonamentoAnnullati = new DataTable("biglietti_abbonamento_annullati");
                // Chiave ORGANIZZATORE
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));
                // Chiave ORDINE_DI_POSTO
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("ordine_di_posto", System.Type.GetType("System.String")));
                // campi
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_abbonamento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("codice_biglietto_abbonamento", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("qta_annullati", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));
                oTabBigliettiAbbonamentoAnnullati.Columns.Add(new System.Data.DataColumn("imposta_intra_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabBigliettiAbbonamentoAnnullati);

                // Impostazione relazione BIGLIETTI_ABBONAMENTO_ANNULLATI <-> ORDINI_DI_POSTO
                DataRelation oRelBigliettiAbbonamentoAnnullatiOrdiniDiPosto = new DataRelation("rl_biglietti_abbonamento_annullati_ordini_di_posto",
                                                                      new DataColumn[] { oTabOrdiniDiPosto.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabOrdiniDiPosto.Columns["codice_locale"],
                                                                                         oTabOrdiniDiPosto.Columns["data_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ora_evento"],
                                                                                         oTabOrdiniDiPosto.Columns["ordine_di_posto"]},
                                                                      new DataColumn[] { oTabBigliettiAbbonamentoAnnullati.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabBigliettiAbbonamentoAnnullati.Columns["codice_locale"],
                                                                                         oTabBigliettiAbbonamentoAnnullati.Columns["data_evento"],
                                                                                         oTabBigliettiAbbonamentoAnnullati.Columns["ora_evento"],
                                                                                         oTabBigliettiAbbonamentoAnnullati.Columns["ordine_di_posto"]}, true);
                oRelBigliettiAbbonamentoAnnullatiOrdiniDiPosto.Nested = true;
                oDataSet.Relations.Add(oRelBigliettiAbbonamentoAnnullatiOrdiniDiPosto);

                // ALTRI_PROVENTO_EVENTO
                System.Data.DataTable oTabAltriProventiEvento = new DataTable("altri_proventi_evento");
                // Chiave ORGANIZZATORE
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // Chiave EVENTO
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("codice_locale", System.Type.GetType("System.String")));
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("data_evento", System.Type.GetType("System.String")));
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("ora_evento", System.Type.GetType("System.String")));

                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("tipo_provento", System.Type.GetType("System.String")));
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("corrispettivo_lordo", System.Type.GetType("System.Int64")));
                oTabAltriProventiEvento.Columns.Add(new System.Data.DataColumn("iva_corrispettivo", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAltriProventiEvento);


                // Impostazione relazione EVENTI <-> ALTRI_PROVENTO_EVENTO
                DataRelation oRelEventiAltriProventi = new DataRelation("rl_eventi_altri_proventi",
                                                                      new DataColumn[] { oTabEventi.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabEventi.Columns["codice_locale"],
                                                                                         oTabEventi.Columns["data_evento"],
                                                                                         oTabEventi.Columns["ora_evento"]},
                                                                      new DataColumn[] { oTabAltriProventiEvento.Columns["codice_fiscale_organizzatore"], 
                                                                                         oTabAltriProventiEvento.Columns["codice_locale"],
                                                                                         oTabAltriProventiEvento.Columns["data_evento"],
                                                                                         oTabAltriProventiEvento.Columns["ora_evento"]}, true);
                oRelEventiAltriProventi.Nested = true;
                oDataSet.Relations.Add(oRelEventiAltriProventi);



                // ABBONAMENTI
                System.Data.DataTable oTabAbbonamenti = new DataTable("abbonamenti");
                // Chiave ORGANIZZATORE
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                // campi
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("codice_abbonamento", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("validita", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("turno", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("codice_ordine", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabAbbonamenti.Columns.Add(new System.Data.DataColumn("numero_eventi_abilitati", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAbbonamenti);


                // Impostazione relazione ABBONAMENTI <-> ORGANIZZATORI
                DataRelation oRelOrganizzatoriAbbonamenti = new DataRelation("rl_organizzatori_abbonamenti",
                                                                        new DataColumn[] { oTabOrganizzatori.Columns["codice_fiscale_organizzatore"]},
                                                                        new DataColumn[] { oTabAbbonamenti.Columns["codice_fiscale_organizzatore"]}, true);

                oRelOrganizzatoriAbbonamenti.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriAbbonamenti);


                // ABBONAMENTI_EMESSI
                System.Data.DataTable oTabAbbonamentiEmessi = new DataTable("abbonamenti_emessi");
                // Chiave ORGANIZZATORE + ABBONAMENTO
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("codice_abbonamento", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("validita", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("turno", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("codice_ordine", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("numero_eventi_abilitati", System.Type.GetType("System.Int64")));
                // campi
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("qta_emessi", System.Type.GetType("System.Int64")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("corrispettivo_titolo", System.Type.GetType("System.Int64")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("corrispettivo_prevendita", System.Type.GetType("System.Int64")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("iva_titolo", System.Type.GetType("System.Int64")));
                oTabAbbonamentiEmessi.Columns.Add(new System.Data.DataColumn("iva_prevendita", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAbbonamentiEmessi);


                // Impostazione relazione ABBONAMENTI_EMESSI <-> ABBONAMENTI
                DataRelation oRelOrganizzatoriAbbonamentiEmessi = new DataRelation("rl_abbonamenti_emessi",
                                                                        new DataColumn[] { oTabAbbonamenti.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamenti.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamenti.Columns["validita"],
                                                                                           oTabAbbonamenti.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamenti.Columns["turno"],
                                                                                           oTabAbbonamenti.Columns["codice_ordine"],
                                                                                           oTabAbbonamenti.Columns["tipo_titolo"],
                                                                                           oTabAbbonamenti.Columns["numero_eventi_abilitati"]},
                                                                        new DataColumn[] { oTabAbbonamentiEmessi.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamentiEmessi.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamentiEmessi.Columns["validita"],
                                                                                           oTabAbbonamentiEmessi.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamentiEmessi.Columns["turno"],
                                                                                           oTabAbbonamentiEmessi.Columns["codice_ordine"],
                                                                                           oTabAbbonamentiEmessi.Columns["tipo_titolo"],
                                                                                           oTabAbbonamentiEmessi.Columns["numero_eventi_abilitati"]}, true);

                oRelOrganizzatoriAbbonamentiEmessi.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriAbbonamentiEmessi);

                // ABBONAMENTI_ANNULLATI
                System.Data.DataTable oTabAbbonamentiAnnullati = new DataTable("abbonamenti_annullati");
                // Chiave ORGANIZZATORE + ABBONAMENTO
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("codice_abbonamento", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("validita", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("turno", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("codice_ordine", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("numero_eventi_abilitati", System.Type.GetType("System.Int64")));
                // Campi
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("qta_annullati", System.Type.GetType("System.Int64")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_titolo", System.Type.GetType("System.Int64")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_prevendita", System.Type.GetType("System.Int64")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("iva_corrispettivo", System.Type.GetType("System.Int64")));
                oTabAbbonamentiAnnullati.Columns.Add(new System.Data.DataColumn("iva_prevendita", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAbbonamentiAnnullati);

                // Impostazione relazione ABBONAMENTI <-> ORGANIZZATORI
                DataRelation oRelOrganizzatoriAbbonamentiAnnullati = new DataRelation("rl_abbonamenti_annullati",
                                                                        new DataColumn[] { oTabAbbonamenti.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamenti.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamenti.Columns["validita"],
                                                                                           oTabAbbonamenti.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamenti.Columns["turno"],
                                                                                           oTabAbbonamenti.Columns["codice_ordine"],
                                                                                           oTabAbbonamenti.Columns["tipo_titolo"],
                                                                                           oTabAbbonamenti.Columns["numero_eventi_abilitati"]},
                                                                        new DataColumn[] { oTabAbbonamentiAnnullati.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamentiAnnullati.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamentiAnnullati.Columns["validita"],
                                                                                           oTabAbbonamentiAnnullati.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamentiAnnullati.Columns["turno"],
                                                                                           oTabAbbonamentiAnnullati.Columns["codice_ordine"],
                                                                                           oTabAbbonamentiAnnullati.Columns["tipo_titolo"],
                                                                                           oTabAbbonamentiAnnullati.Columns["numero_eventi_abilitati"]}, true);

                oRelOrganizzatoriAbbonamentiAnnullati.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriAbbonamentiAnnullati);

                // ABBONAMENTI IVA PREASSOLTA EMESSI
                System.Data.DataTable oTabAbbIvaPreassoltaEmessi = new DataTable("abbonamenti_ivapreassolta_emessi");
                // Chiave ORGANIZZATORE + ABBONAMENTO
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("codice_abbonamento", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("validita", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("turno", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("codice_ordine", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("numero_eventi_abilitati", System.Type.GetType("System.Int64")));
                // campi
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("qta_emessi", System.Type.GetType("System.Int64")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabAbbIvaPreassoltaEmessi.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAbbIvaPreassoltaEmessi);


                // Impostazione relazione ABBONAMENTI_EMESSI <-> ABBONAMENTI
                DataRelation oRelOrganizzatoriAbbonamentiIvaPreassoltaEmessi = new DataRelation("rl_abbonamenti_iva_preassolta_emessi",
                                                                        new DataColumn[] { oTabAbbonamenti.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamenti.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamenti.Columns["validita"],
                                                                                           oTabAbbonamenti.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamenti.Columns["turno"],
                                                                                           oTabAbbonamenti.Columns["codice_ordine"],
                                                                                           oTabAbbonamenti.Columns["tipo_titolo"],
                                                                                           oTabAbbonamenti.Columns["numero_eventi_abilitati"]},
                                                                        new DataColumn[] { oTabAbbIvaPreassoltaEmessi.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["codice_abbonamento"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["validita"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["turno"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["codice_ordine"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["tipo_titolo"],
                                                                                           oTabAbbIvaPreassoltaEmessi.Columns["numero_eventi_abilitati"]}, true);

                oRelOrganizzatoriAbbonamentiEmessi.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriAbbonamentiIvaPreassoltaEmessi);

                // ABBONAMENTI_ANNULLATI
                System.Data.DataTable oTabAbbIvaPreassoltaAnnullati = new DataTable("abbonamenti_iva_preassolta_annullati");
                // Chiave ORGANIZZATORE + ABBONAMENTO
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("codice_fiscale_organizzatore", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("codice_abbonamento", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("validita", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("spettacolo_intrattenimento", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("turno", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("codice_ordine", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("tipo_titolo", System.Type.GetType("System.String")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("numero_eventi_abilitati", System.Type.GetType("System.Int64")));
                // Campi
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("qta_annullati", System.Type.GetType("System.Int64")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("corrispettivo_figurativo", System.Type.GetType("System.Int64")));
                oTabAbbIvaPreassoltaAnnullati.Columns.Add(new System.Data.DataColumn("iva_figurativa", System.Type.GetType("System.Int64")));

                oDataSet.Tables.Add(oTabAbbIvaPreassoltaAnnullati);

                // Impostazione relazione ABBONAMENTI <-> ORGANIZZATORI
                DataRelation oRelOrganizzatoriAbbonamentiIvaPreassoltaAnnullati = new DataRelation("rl_abbonamenti_iva_preassolta_annullati",
                                                                        new DataColumn[] { oTabAbbonamenti.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbonamenti.Columns["codice_abbonamento"],
                                                                                           oTabAbbonamenti.Columns["validita"],
                                                                                           oTabAbbonamenti.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbonamenti.Columns["turno"],
                                                                                           oTabAbbonamenti.Columns["codice_ordine"],
                                                                                           oTabAbbonamenti.Columns["tipo_titolo"],
                                                                                           oTabAbbonamenti.Columns["numero_eventi_abilitati"]},
                                                                        new DataColumn[] { oTabAbbIvaPreassoltaAnnullati.Columns["codice_fiscale_organizzatore"], 
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["codice_abbonamento"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["validita"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["spettacolo_intrattenimento"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["turno"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["codice_ordine"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["tipo_titolo"],
                                                                                           oTabAbbIvaPreassoltaAnnullati.Columns["numero_eventi_abilitati"]}, true);

                oRelOrganizzatoriAbbonamentiAnnullati.Nested = true;
                oDataSet.Relations.Add(oRelOrganizzatoriAbbonamentiIvaPreassoltaAnnullati);

                System.Collections.SortedList oTempList = new System.Collections.SortedList();

                // Letture


                // lettura RIEPILOGO
                if (lRet)
                {
                    oSB = new StringBuilder();
                    oPars = new clsParameters();

                    oSB.Append("select");
                    oSB.Append(" decode(prog, 1, 'N', 'S') as sostituzione,");
                    oSB.Append(" trim(to_char(giorno,'YYYYMMDD')) as data,");
                    oSB.Append(" trim(to_char(data_generazione,'YYYYMMDD')) as data_generazione,");
                    oSB.Append(" trim(to_char(data_generazione,'HH24MISS')) as ora_generazione,");
                    oSB.Append(" prog as progressivo_generazione");
                    oSB.Append(" FROM RIEPILOGHI_PROGS where giorno = :pGIORNO and tipo = :pGIORNALIERO_MENSILE and prog = :pPROG_RIEPILOGO");
                    oPars.Add(":pGIORNO", Giorno);
                    oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
                    oPars.Add(":pPROG_RIEPILOGO", nProgNextRiepilogo);
                    IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), oPars);

                    lRet = (!oRS.EOF);

                    if (lRet)
                    {
                        System.Data.DataRow oRow = oTabRiepilogo.NewRow();
                        oRow.ItemArray = new object[]{oRS.Fields("sostituzione").Value.ToString(),
                                                  oRS.Fields("data").Value.ToString(),
                                                  oRS.Fields("data_generazione").Value.ToString(),
                                                  oRS.Fields("ora_generazione").Value.ToString(),
                                                  oRS.Fields("progressivo_generazione").Value.ToString()};
                        oTabRiepilogo.Rows.Add(oRow);
                    }

                    oRS.Close();
                }

                if (lRet)
                {
                    oSB = new StringBuilder();
                    oPars = new clsParameters();
                    //oSB.Append("SELECT * FROM TABLE(F_READ_RIEPILOGHI(:pGIORNO, :pGIORNO, :pGIORNALIERO_MENSILE)) WHERE GIORNO_MESE = :pGIORNO AND TIPO_RIEPILOGO = :pGIORNALIERO_MENSILE AND PROG_RIEPILOGO = :pPROG_RIEPILOGO");
                    //oPars.Add(":pGIORNO", Giorno);
                    //oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
                    //oPars.Add(":pPROG_RIEPILOGO", nProgNextRiepilogo);
                    //oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), oPars);
                    //lRet = (!oRS.EOF);

                    lRet = (tabRIEPILOGHI_BASE != null && tabRIEPILOGHI_BASE.Rows != null && tabRIEPILOGHI_BASE.Rows.Count > 0);


                    // lettura TITOLARE
                    if (lRet)
                    {
                        //oRS.MoveFirst();
                        //if (!oRS.EOF)
                        //{
                        //    System.Data.DataRow oRow = oTabTitolare.NewRow();
                        //    oRow.ItemArray = new object[]{oRS.Fields("denominazione_titolare").Value.ToString(), 
                        //                                  oRS.Fields("codice_fiscale_titolare").Value.ToString(), 
                        //                                  oRS.Fields("codice_sistema").Value.ToString()};
                        //    oTabTitolare.Rows.Add(oRow);
                        //}

                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            System.Data.DataRow oRow = oTabTitolare.NewRow();
                            oRow.ItemArray = new object[]{oRowData["denominazione_titolare"].ToString(),
                                                          oRowData["codice_fiscale_titolare"].ToString(),
                                                          oRowData["codice_sistema"].ToString()};
                            oTabTitolare.Rows.Add(oRow);
                            break;
                        }
                    }
                    else
                    {
                        // determino se il riepilogo � vuoto
                        lRiepilogoVuoto = true;
                        oSB = new StringBuilder();
                        oSB.Append("SELECT");
                        oSB.Append(" MAX(CODICE_SISTEMA) AS CODICE_SISTEMA,");
                        oSB.Append(" MAX(CF_TITOLARE) AS CODICE_FISCALE_TITOLARE,");
                        oSB.Append(" MAX(TITOLARE) AS DENOMINAZIONE_TITOLARE");
                        oSB.Append(" FROM");
                        oSB.Append(" SMART_CARD");
                        oSB.Append(" WHERE");
                        oSB.Append(" CODICE_SISTEMA = F_GET_CODICE_SISTEMA");
                        oSB.Append(" AND");
                        oSB.Append(" ENABLED = 1");
                        IRecordSet oRSTitolare = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString());
                        if (!oRSTitolare.EOF)
                        {
                            System.Data.DataRow oRow = oTabTitolare.NewRow();
                            oRow.ItemArray = new object[]{oRSTitolare.Fields("denominazione_titolare").Value.ToString(), 
                                                          oRSTitolare.Fields("codice_fiscale_titolare").Value.ToString(), 
                                                          oRSTitolare.Fields("codice_sistema").Value.ToString()};
                            oTabTitolare.Rows.Add(oRow);
                        }
                        oRSTitolare.Close();
                    }

                

                    // lettura ORGANIZZATORI
                    if (lRet)
                    {
                        
                        oTempList = new System.Collections.SortedList();
                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            string cKey = oRowData["denominazione_organizzatore"].ToString() +
                                          oRowData["codice_fiscale_organizzatore"].ToString() +
                                          oRowData["tipo_cf_pi"].ToString();
                            
                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabOrganizzatori.NewRow();
                                oRow.ItemArray = new object[]{oRowData["denominazione_organizzatore"].ToString(),
                                                              oRowData["codice_fiscale_organizzatore"].ToString(),
                                                              oRowData["tipo_cf_pi"].ToString()};
                                oTabOrganizzatori.Rows.Add(oRow);
                            }
                        }

                        //oRS.MoveFirst();
                        //while (!oRS.EOF)
                        //{
                        //    string cKey = oRS.Fields("denominazione_organizzatore").Value.ToString() +
                        //                  oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //                  oRS.Fields("tipo_cf_pi").Value.ToString();
                        //    if (!oTempList.ContainsKey(cKey))
                        //    {
                        //        oTempList.Add(cKey, null);
                        //        System.Data.DataRow oRow = oTabOrganizzatori.NewRow();
                        //        oRow.ItemArray = new object[]{oRS.Fields("denominazione_organizzatore").Value.ToString(), 
                        //                                      oRS.Fields("codice_fiscale_organizzatore").Value.ToString(), 
                        //                                      oRS.Fields("tipo_cf_pi").Value.ToString()};
                        //        oTabOrganizzatori.Rows.Add(oRow);
                        //    }
                        //    oRS.MoveNext();
                        //}
                    }

                    // lettura EVENTI per ORGANIZZATORE
                    if (lRet)
                    {
                        //oRS.MoveFirst();
                        oTempList = new System.Collections.SortedList();

                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            if (oRowData["titolo_abbonamento"].ToString() == "T")
                            {
                                DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                                string cKey = oRowData["denominazione_organizzatore"].ToString() +
                                              oRowData["codice_fiscale_organizzatore"].ToString() +
                                              oRowData["tipo_cf_pi"].ToString() +
                                              oRowData["spettacolo_intrattenimento"].ToString() +
                                              oRowData["codice_locale"].ToString() +
                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                              oRowData["tipo_evento"].ToString() +
                                              oRowData["titolo_evento"].ToString();
                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);
                                    System.Data.DataRow oRow = oTabEventi.NewRow();

                                    oRow.ItemArray = new object[]{oRowData["denominazione_organizzatore"].ToString(),
                                                                  oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                  oRowData["tipo_cf_pi"].ToString(),
                                                                  oRowData["spettacolo_intrattenimento"].ToString(),
                                                                  oRowData["codice_locale"].ToString(),
                                                                  dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                  dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                  oRowData["tipo_evento"].ToString(),
                                                                  oRowData["titolo_evento"].ToString(),
                                                                  Int64.Parse(oRowData["incidenza_isi"].ToString())};
                                    oTabEventi.Rows.Add(oRow);
                                }
                            }
                        }

                        //while (!oRS.EOF)
                        //{
                        //    if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T")
                        //    {
                        //        DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                        //        string cKey = oRS.Fields("denominazione_organizzatore").Value.ToString() +
                        //                      oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //                      oRS.Fields("tipo_cf_pi").Value.ToString() +
                        //                      oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                        //                      oRS.Fields("codice_locale").Value.ToString() +
                        //                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                        //                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                        //                      oRS.Fields("tipo_evento").Value.ToString() +
                        //                      oRS.Fields("titolo_evento").Value.ToString();

                        //        if (!oTempList.ContainsKey(cKey))
                        //        {
                        //            oTempList.Add(cKey, null);
                        //            System.Data.DataRow oRow = oTabEventi.NewRow();

                        //            oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                        //                                          oRS.Fields("denominazione_locale").Value.ToString(),
                        //                                          oRS.Fields("codice_locale").Value.ToString(),
                        //                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                          oRS.Fields("tipo_evento").Value.ToString(),
                        //                                          oRS.Fields("titolo_evento").Value.ToString(),
                        //                                          Int64.Parse(oRS.Fields("incidenza_isi").Value.ToString())};
                        //            oTabEventi.Rows.Add(oRow);
                        //        }
                        //    }

                        //    oRS.MoveNext();
                        //}
                    }

                    // lettura TITOLI_OPERE
                    if (lRet)
                    {
                        oTempList = new System.Collections.SortedList();
                        if (GiornalieroMensile != "D")
                        {
                            StringBuilder oSBTitoli = new StringBuilder();
                            IRecordSet oRSTitoli = null;
                            clsParameters oParsTitoli = new clsParameters();

                            //oSBTitoli.Append("SELECT * FROM RIEPILOGHI_TITOLI_EVENTI where GIORNO_MESE = :pGIORNO_MESE AND TIPO_RIEPILOGO = :pTIPO_RIEPILOGO AND PROG_RIEPILOGO = :pPROG_RIEPILOGO");
                            //oParsTitoli.Add(":pGIORNO_MESE", Giorno);
                            //oParsTitoli.Add(":pTIPO_RIEPILOGO", GiornalieroMensile);
                            //oParsTitoli.Add(":pPROG_RIEPILOGO", nProgNextRiepilogo);
                            //oRSTitoli = (IRecordSet)oConnection.ExecuteQuery(oSBTitoli.ToString(), oParsTitoli);

                            oRSTitoli = (IRecordSet)tabRIEPILOGHI_TITOLI_EVENTI;

                            while (!oRSTitoli.EOF)
                            {
                                string cKey = "";
                                string cDataEvento = oRSTitoli.Fields("data_evento").Value.ToString();
                                string cOraEvento = oRSTitoli.Fields("ora_evento").Value.ToString();
                                //DateTime dDataOraEvento = (DateTime)oRSTitoli.Fields("data_evento").Value;
                                //dDataOraEvento = dDataOraEvento.AddHours(double.Parse(oRSTitoli.Fields("ora_evento").Value.ToString().Substring(0, 2)));
                                //dDataOraEvento = dDataOraEvento.AddMinutes(double.Parse(oRSTitoli.Fields("ora_evento").Value.ToString().Substring(3, 2)));

                                //cKey = oRSTitoli.Fields("codice_fiscale_organizzatore").Value.ToString().PadRight(16) +
                                //       oRSTitoli.Fields("codice_locale").Value.ToString() +
                                //       dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                //       dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00");
                                cKey = oRSTitoli.Fields("codice_fiscale_organizzatore").Value.ToString().PadRight(16) +
                                       oRSTitoli.Fields("codice_locale").Value.ToString() +
                                       cDataEvento + cOraEvento;

                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);

                                    System.Data.DataRow oRow = oTabTitoliOpere.NewRow();
                                    string cProduttoreCinema = (!oRSTitoli.Fields("produttore").IsNull ? oRSTitoli.Fields("produttore").Value.ToString() : "");
                                    string cAutore = (!oRSTitoli.Fields("autore").IsNull ? oRSTitoli.Fields("autore").Value.ToString() : "");
                                    string cEsecutore = (!oRSTitoli.Fields("esecutore").IsNull ? oRSTitoli.Fields("esecutore").Value.ToString() : "");
                                    string cNazionalita = (!oRSTitoli.Fields("nazionalita").IsNull ? oRSTitoli.Fields("nazionalita").Value.ToString() : "");
                                    string cDistributore = (!oRSTitoli.Fields("distributore").IsNull ? oRSTitoli.Fields("distributore").Value.ToString() : "");

                                    if (!oRSTitoli.Fields("tipo_evento").IsNull)
                                    {
                                        // Se tipo evento 01 CINEMA
                                        if (oRSTitoli.Fields("tipo_evento").Value.ToString() == "01")
                                        {
                                            cAutore = "";
                                            cEsecutore = "";
                                        }
                                        else if ("45_46_47_48_49_50_51_52_53_54_55_56_57_58_59_60_61_62_63_64_65_66_67_68".Contains(oRSTitoli.Fields("tipo_evento").Value.ToString()))
                                        {
                                            // Se tipo evento Teatro o altro (dal 45 al 68) vedere tabella TIPOEVENTO
                                            cProduttoreCinema = "";
                                            cDistributore = "";
                                        }
                                        else
                                        {
                                            cAutore = "";
                                            cEsecutore = "";
                                            cProduttoreCinema = "";
                                            cDistributore = "";
                                        }
                                    }

                                    oRow.ItemArray = new object[]{oRSTitoli.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                                  oRSTitoli.Fields("codice_locale").Value.ToString(),
                                                                  cDataEvento,
                                                                  cOraEvento,
                                                                  oRSTitoli.Fields("titolo").Value.ToString(), 
                                                                  cProduttoreCinema, 
                                                                  cAutore, 
                                                                  cEsecutore, 
                                                                  cNazionalita, 
                                                                  cDistributore};

                                    oTabTitoliOpere.Rows.Add(oRow);
                                }
                                oRSTitoli.MoveNext();
                            }
                            oRSTitoli.Close();
                        }

                        // Lettura comunque delle transazioni per verificare se non tutti i titoli sono associati
                        //oRS.MoveFirst();
                        //while (!oRS.EOF)
                        //{
                        //    if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T")
                        //    {
                        //        string cKey = "";
                        //        DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;

                        //        cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //               oRS.Fields("codice_locale").Value.ToString() +
                        //               dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                        //               dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00");

                        //        if (!oTempList.ContainsKey(cKey))
                        //        {
                        //            oTempList.Add(cKey, null);
                        //            StringBuilder oSBTitoliOpere = new StringBuilder();

                        //            oSBTitoliOpere.Append("select ");
                        //            oSBTitoliOpere.Append(" vr_film.titolo, ");
                        //            oSBTitoliOpere.Append(" vr_produttori.rag_soc as produttore_cinema, ");
                        //            oSBTitoliOpere.Append(" vr_film.regista as autore, ");
                        //            oSBTitoliOpere.Append(" '' as esecutore, ");
                        //            oSBTitoliOpere.Append(" vr_film.idnazione as nazionalita, ");
                        //            oSBTitoliOpere.Append(" vr_noleggio.rag_soc as distributore ");
                        //            oSBTitoliOpere.Append(" from vr_pacchetto, vr_film, vr_produttori, vr_noleggio");
                        //            oSBTitoliOpere.Append(" where ");
                        //            oSBTitoliOpere.Append(" vr_pacchetto.descrizione = :pTITOLO_EVENTO ");
                        //            oSBTitoliOpere.Append(" and ");
                        //            oSBTitoliOpere.Append(" vr_pacchetto.idfilm = vr_film.idfilm ");
                        //            oSBTitoliOpere.Append(" and ");
                        //            oSBTitoliOpere.Append(" vr_film.idnolo = vr_noleggio.idnolo ");
                        //            oSBTitoliOpere.Append(" and ");
                        //            oSBTitoliOpere.Append(" vr_film.idprod = vr_produttori.idprod ");
                        //            oSBTitoliOpere.Append(" order by ");
                        //            oSBTitoliOpere.Append(" vr_pacchetto.progr ");
                        //            IRecordSet oRSTitoliOpere = (IRecordSet)oConnection.ExecuteQuery(oSBTitoliOpere.ToString(), new clsParameters(":pTITOLO_EVENTO", oRS.Fields("titolo_evento").Value.ToString().TrimEnd()));
                        //            if (!oRSTitoliOpere.EOF)
                        //            {
                        //                while (!oRSTitoliOpere.EOF)
                        //                {
                        //                    System.Data.DataRow oRow = oTabTitoliOpere.NewRow();
                        //                    string cProduttoreCinema = (!oRSTitoliOpere.Fields("produttore_cinema").IsNull ? oRSTitoliOpere.Fields("produttore_cinema").Value.ToString() : "");
                        //                    string cAutore = (!oRSTitoliOpere.Fields("autore").IsNull ? oRSTitoliOpere.Fields("autore").Value.ToString() : "");
                        //                    string cEsecutore = (!oRSTitoliOpere.Fields("esecutore").IsNull ? oRSTitoliOpere.Fields("esecutore").Value.ToString() : "");
                        //                    string cNazionalita = (!oRSTitoliOpere.Fields("nazionalita").IsNull ? oRSTitoliOpere.Fields("nazionalita").Value.ToString() : "");
                        //                    string cDistributore = (!oRSTitoliOpere.Fields("distributore").IsNull ? oRSTitoliOpere.Fields("distributore").Value.ToString() : "");

                        //                    if (!oRS.Fields("tipo_evento").IsNull)
                        //                    {
                        //                        // Se tipo evento 01 CINEMA
                        //                        if (oRS.Fields("tipo_evento").Value.ToString() == "01")
                        //                        {
                        //                            cAutore = "";
                        //                            cEsecutore = "";
                        //                        }
                        //                        else if ("45_46_47_48_49_50_51_52_53_54_55_56_57_58_59_60_61_62_63_64_65_66_67_68".Contains(oRS.Fields("tipo_evento").Value.ToString()))
                        //                        {
                        //                            // Se tipo evento Teatro o altro (dal 45 al 68) vedere tabella TIPOEVENTO
                        //                            cProduttoreCinema = "";
                        //                            cDistributore = "";
                        //                        }
                        //                        else
                        //                        {
                        //                            cAutore = "";
                        //                            cEsecutore = "";
                        //                            cProduttoreCinema = "";
                        //                            cDistributore = "";
                        //                        }
                        //                    }

                        //                    oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                                  oRS.Fields("codice_locale").Value.ToString(),
                        //                                                  dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                                  dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                                  oRSTitoliOpere.Fields("titolo").Value.ToString(), 
                        //                                                  cProduttoreCinema, 
                        //                                                  cAutore, 
                        //                                                  cEsecutore, 
                        //                                                  cNazionalita, 
                        //                                                  cDistributore};

                        //                    oTabTitoliOpere.Rows.Add(oRow);

                        //                    oRSTitoliOpere.MoveNext();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                System.Data.DataRow oRow = oTabTitoliOpere.NewRow();
                        //                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                              oRS.Fields("codice_locale").Value.ToString(),
                        //                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                              oRS.Fields("titolo_evento").Value.ToString(), 
                        //                                              ".", 
                        //                                              ".", 
                        //                                              ".", 
                        //                                              "IT", 
                        //                                              "."};

                        //                oTabTitoliOpere.Rows.Add(oRow);
                        //            }
                        //            oRSTitoliOpere.Close();
                        //        }
                        //    }
                        //    oRS.MoveNext();
                        //}



                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            if (oRowData["titolo_abbonamento"].ToString() == "T")
                            {
                                string cKey = "";
                                DateTime dDataOraEvento = (DateTime)oRowData["data_ora_evento"];

                                cKey = oRowData["codice_fiscale_organizzatore"].ToString() +
                                       oRowData["codice_locale"].ToString() +
                                       dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                       dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00");

                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);
                                    StringBuilder oSBTitoliOpere = new StringBuilder();

                                    oSBTitoliOpere.Append("select ");
                                    oSBTitoliOpere.Append(" vr_film.titolo, ");
                                    oSBTitoliOpere.Append(" vr_produttori.rag_soc as produttore_cinema, ");
                                    oSBTitoliOpere.Append(" vr_film.regista as autore, ");
                                    oSBTitoliOpere.Append(" '' as esecutore, ");
                                    oSBTitoliOpere.Append(" vr_film.idnazione as nazionalita, ");
                                    oSBTitoliOpere.Append(" vr_noleggio.rag_soc as distributore ");
                                    oSBTitoliOpere.Append(" from vr_pacchetto, vr_film, vr_produttori, vr_noleggio");
                                    oSBTitoliOpere.Append(" where ");
                                    oSBTitoliOpere.Append(" vr_pacchetto.descrizione = :pTITOLO_EVENTO ");
                                    oSBTitoliOpere.Append(" and ");
                                    oSBTitoliOpere.Append(" vr_pacchetto.idfilm = vr_film.idfilm ");
                                    oSBTitoliOpere.Append(" and ");
                                    oSBTitoliOpere.Append(" vr_film.idnolo = vr_noleggio.idnolo ");
                                    oSBTitoliOpere.Append(" and ");
                                    oSBTitoliOpere.Append(" vr_film.idprod = vr_produttori.idprod ");
                                    oSBTitoliOpere.Append(" order by ");
                                    oSBTitoliOpere.Append(" vr_pacchetto.progr ");
                                    IRecordSet oRSTitoliOpere = (IRecordSet)oConnection.ExecuteQuery(oSBTitoliOpere.ToString(), new clsParameters(":pTITOLO_EVENTO", oRowData["titolo_evento"].ToString().TrimEnd()));
                                    if (!oRSTitoliOpere.EOF)
                                    {
                                        while (!oRSTitoliOpere.EOF)
                                        {
                                            System.Data.DataRow oRow = oTabTitoliOpere.NewRow();
                                            string cProduttoreCinema = (!oRSTitoliOpere.Fields("produttore_cinema").IsNull ? oRSTitoliOpere.Fields("produttore_cinema").Value.ToString() : "");
                                            string cAutore = (!oRSTitoliOpere.Fields("autore").IsNull ? oRSTitoliOpere.Fields("autore").Value.ToString() : "");
                                            string cEsecutore = (!oRSTitoliOpere.Fields("esecutore").IsNull ? oRSTitoliOpere.Fields("esecutore").Value.ToString() : "");
                                            string cNazionalita = (!oRSTitoliOpere.Fields("nazionalita").IsNull ? oRSTitoliOpere.Fields("nazionalita").Value.ToString() : "");
                                            string cDistributore = (!oRSTitoliOpere.Fields("distributore").IsNull ? oRSTitoliOpere.Fields("distributore").Value.ToString() : "");

                                            // Se tipo evento 01 CINEMA
                                            if (oRowData["tipo_evento"].ToString() == "01")
                                            {
                                                cAutore = "";
                                                cEsecutore = "";
                                            }
                                            else if ("45_46_47_48_49_50_51_52_53_54_55_56_57_58_59_60_61_62_63_64_65_66_67_68".Contains(oRowData["tipo_evento"].ToString()))
                                            {
                                                // Se tipo evento Teatro o altro (dal 45 al 68) vedere tabella TIPOEVENTO
                                                cProduttoreCinema = "";
                                                cDistributore = "";
                                            }
                                            else
                                            {
                                                cAutore = "";
                                                cEsecutore = "";
                                                cProduttoreCinema = "";
                                                cDistributore = "";
                                            }

                                            oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                          oRowData["codice_locale"].ToString(),
                                                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                          oRSTitoliOpere.Fields("titolo").Value.ToString(),
                                                                          cProduttoreCinema,
                                                                          cAutore,
                                                                          cEsecutore,
                                                                          cNazionalita,
                                                                          cDistributore};

                                            oTabTitoliOpere.Rows.Add(oRow);

                                            oRSTitoliOpere.MoveNext();
                                        }
                                    }
                                    else
                                    {
                                        System.Data.DataRow oRow = oTabTitoliOpere.NewRow();
                                        oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                      oRowData["codice_locale"].ToString(),
                                                                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                      oRowData["titolo_evento"].ToString(),
                                                                      ".",
                                                                      ".",
                                                                      ".",
                                                                      "IT",
                                                                      "."};

                                        oTabTitoliOpere.Rows.Add(oRow);
                                    }
                                    oRSTitoliOpere.Close();
                                }
                            }
                        }

                    }

                    // lettura ALTRI_PROVENTI_EVENTO se MENSILE
                    if (lRet && GiornalieroMensile == "M")
                    {
                        oTempList = new System.Collections.SortedList();


                        //oRS.MoveFirst();
                        //while (!oRS.EOF)
                        //{
                        //    if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T")
                        //    {
                        //        string cKey = "";
                        //        DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                        //        Int64 nIdCalend = 0;

                        //        StringBuilder oSBAltriProventiEvento = null;
                        //        clsParameters oParsAltriProventiEvento = null;
                        //        IRecordSet oRSAltriProventiEvento = null;

                        //        if (!oRS.Fields("idcalend").IsNull)
                        //        {
                        //            nIdCalend = Int64.Parse(oRS.Fields("idcalend").Value.ToString());
                        //            if (nIdCalend > 0)
                        //            {
                        //                oSBAltriProventiEvento = new StringBuilder();
                        //                oParsAltriProventiEvento = new clsParameters();
                        //                oSBAltriProventiEvento.Append("SELECT TIPO_PROVENTO, CORRISPETTIVO_LORDO, IVA_CORRISPETTIVO FROM VR_ALTRIPROVENTI WHERE IDCALEND = :pIDCALEND");
                        //                oParsAltriProventiEvento.Add(":pIDCALEND", nIdCalend);
                        //                oRSAltriProventiEvento = (IRecordSet)oConnection.ExecuteQuery(oSBAltriProventiEvento.ToString(), oParsAltriProventiEvento);
                        //                if (oRSAltriProventiEvento.EOF)
                        //                {
                        //                    oRSAltriProventiEvento.Close();
                        //                    oRSAltriProventiEvento = null;
                        //                }
                        //            }
                        //        }

                        //        if (oRSAltriProventiEvento == null)
                        //        {
                        //            oSBAltriProventiEvento = new StringBuilder();
                        //            oParsAltriProventiEvento = new clsParameters();
                        //            oSBAltriProventiEvento.Append("SELECT TIPO_PROVENTO, CORRISPETTIVO_LORDO, IVA_CORRISPETTIVO FROM VR_ALTRIPROVENTI WHERE CODICE_LOCALE = :pCODICE_LOCALE AND TITOLO = :pTITOLO_EVENTO AND DATA_ORA = :pDATA_ORA");
                        //            oParsAltriProventiEvento.Add(":pCODICE_LOCALE", oRS.Fields("codice_locale").Value.ToString());
                        //            oParsAltriProventiEvento.Add(":pTITOLO_EVENTO", oRS.Fields("titolo_evento").Value.ToString().TrimEnd());
                        //            oParsAltriProventiEvento.Add(":pDATA_ORA", dDataOraEvento);
                        //            oRSAltriProventiEvento = (IRecordSet)oConnection.ExecuteQuery(oSBAltriProventiEvento.ToString(), oParsAltriProventiEvento);
                        //        }

                        //        if (oRSAltriProventiEvento != null && !oRSAltriProventiEvento.EOF)
                        //        {

                        //            cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //                   oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                        //                   oRS.Fields("codice_locale").Value.ToString() +
                        //                   dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                        //                   dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                        //                   oRS.Fields("tipo_evento").Value.ToString() +
                        //                   oRS.Fields("titolo_evento").Value.ToString();

                        //            if (!oTempList.ContainsKey(cKey))
                        //            {
                        //                oTempList.Add(cKey, null);

                        //                while (!oRSAltriProventiEvento.EOF)
                        //                {
                        //                    System.Data.DataRow oRow = oTabAltriProventiEvento.NewRow();
                        //                    oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                                  oRS.Fields("codice_locale").Value.ToString(),
                        //                                                  dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                                  dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                                  oRSAltriProventiEvento.Fields("tipo_provento").Value.ToString(),
                        //                                                  decimal.Parse(oRSAltriProventiEvento.Fields("corrispettivo_lordo").Value.ToString()) * 100,
                        //                                                  decimal.Parse(oRSAltriProventiEvento.Fields("iva_corrispettivo").Value.ToString()) * 100};

                        //                    oTabAltriProventiEvento.Rows.Add(oRow);
                        //                    oRSAltriProventiEvento.MoveNext();
                        //                }
                        //            }
                        //        }
                        //        oRSAltriProventiEvento.Close();
                        //    }
                        //    oRS.MoveNext();
                        //}


                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            if (oRowData["titolo_abbonamento"].ToString() == "T")
                            {
                                string cKey = "";
                                DateTime dDataOraEvento = (DateTime)oRowData["data_ora_evento"];
                                Int64 nIdCalend = 0;

                                StringBuilder oSBAltriProventiEvento = null;
                                clsParameters oParsAltriProventiEvento = null;
                                IRecordSet oRSAltriProventiEvento = null;

                                if (!string.IsNullOrEmpty(oRowData["idcalend"].ToString()))
                                {
                                    nIdCalend = Int64.Parse(oRowData["idcalend"].ToString());
                                    if (nIdCalend > 0)
                                    {
                                        oSBAltriProventiEvento = new StringBuilder();
                                        oParsAltriProventiEvento = new clsParameters();
                                        oSBAltriProventiEvento.Append("SELECT TIPO_PROVENTO, CORRISPETTIVO_LORDO, IVA_CORRISPETTIVO FROM VR_ALTRIPROVENTI WHERE IDCALEND = :pIDCALEND");
                                        oParsAltriProventiEvento.Add(":pIDCALEND", nIdCalend);
                                        oRSAltriProventiEvento = (IRecordSet)oConnection.ExecuteQuery(oSBAltriProventiEvento.ToString(), oParsAltriProventiEvento);
                                        if (oRSAltriProventiEvento.EOF)
                                        {
                                            oRSAltriProventiEvento.Close();
                                            oRSAltriProventiEvento = null;
                                        }
                                    }
                                }

                                if (oRSAltriProventiEvento == null)
                                {
                                    oSBAltriProventiEvento = new StringBuilder();
                                    oParsAltriProventiEvento = new clsParameters();
                                    oSBAltriProventiEvento.Append("SELECT TIPO_PROVENTO, CORRISPETTIVO_LORDO, IVA_CORRISPETTIVO FROM VR_ALTRIPROVENTI WHERE CODICE_LOCALE = :pCODICE_LOCALE AND TITOLO = :pTITOLO_EVENTO AND DATA_ORA = :pDATA_ORA");
                                    oParsAltriProventiEvento.Add(":pCODICE_LOCALE", oRowData["codice_locale"].ToString());
                                    oParsAltriProventiEvento.Add(":pTITOLO_EVENTO", oRowData["titolo_evento"].ToString().TrimEnd());
                                    oParsAltriProventiEvento.Add(":pDATA_ORA", dDataOraEvento);
                                    oRSAltriProventiEvento = (IRecordSet)oConnection.ExecuteQuery(oSBAltriProventiEvento.ToString(), oParsAltriProventiEvento);
                                }

                                if (oRSAltriProventiEvento != null && !oRSAltriProventiEvento.EOF)
                                {

                                    cKey = oRowData["codice_fiscale_organizzatore"].ToString() +
                                           oRowData["spettacolo_intrattenimento"].ToString() +
                                           oRowData["codice_locale").Value.ToString() +
                                           dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                           dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                           oRowData["tipo_evento"].ToString() +
                                           oRowData["titolo_evento"].ToString();

                                    if (!oTempList.ContainsKey(cKey))
                                    {
                                        oTempList.Add(cKey, null);

                                        while (!oRSAltriProventiEvento.EOF)
                                        {
                                            System.Data.DataRow oRow = oTabAltriProventiEvento.NewRow();
                                            oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                          oRowData["codice_locale"].ToString(),
                                                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                          oRSAltriProventiEvento.Fields("tipo_provento").Value.ToString(),
                                                                          decimal.Parse(oRSAltriProventiEvento.Fields("corrispettivo_lordo").Value.ToString()) * 100,
                                                                          decimal.Parse(oRSAltriProventiEvento.Fields("iva_corrispettivo").Value.ToString()) * 100};

                                            oTabAltriProventiEvento.Rows.Add(oRow);
                                            oRSAltriProventiEvento.MoveNext();
                                        }
                                    }
                                }
                                oRSAltriProventiEvento.Close();
                            }
                        }
                    }

                    // lettura ORDINI_DI_POSTO per ORGANIZZATORE + EVENTO
                    if (lRet)
                    {
                        oTempList = new System.Collections.SortedList();

                        //oRS.MoveFirst();
                        //while (!oRS.EOF)
                        //{
                        //    if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T")
                        //    {
                        //        DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                        //        string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //                      oRS.Fields("codice_locale").Value.ToString() +
                        //                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                        //                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                        //                      oRS.Fields("ordine_di_posto").Value.ToString();

                        //        if (!oTempList.ContainsKey(cKey))
                        //        {
                        //            oTempList.Add(cKey, null);
                        //            System.Data.DataRow oRow = oTabOrdiniDiPosto.NewRow();

                        //            if (GiornalieroMensile == "G")
                        //            {
                        //                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                              oRS.Fields("codice_locale").Value.ToString(),
                        //                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                        //                                              oRS.Fields("capienza").Value.ToString()};
                        //            }
                        //            else
                        //            {
                        //                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                              oRS.Fields("codice_locale").Value.ToString(),
                        //                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                        //                                              oRS.Fields("capienza").Value.ToString(),
                        //                                              0};
                        //            }


                        //            oTabOrdiniDiPosto.Rows.Add(oRow);
                        //        }
                        //    }

                        //    oRS.MoveNext();
                        //}

                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            if (oRowData["titolo_abbonamento"].ToString() == "T")
                            {
                                DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                                string cKey = oRowData["codice_fiscale_organizzatore"].ToString() +
                                              oRowData["codice_locale"].ToString() +
                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                              oRowData["ordine_di_posto"].ToString();

                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);
                                    System.Data.DataRow oRow = oTabOrdiniDiPosto.NewRow();

                                    if (GiornalieroMensile == "G")
                                    {
                                        oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                      oRowData["codice_locale"].ToString(),
                                                                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                      oRowData["ordine_di_posto"].ToString(),
                                                                      oRowData["capienza"].ToString()};
                                    }
                                    else
                                    {
                                        oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                      oRowData["codice_locale"].ToString(),
                                                                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                      oRowData["ordine_di_posto"].ToString(),
                                                                      oRowData["capienza"].ToString(),
                                                                      0};
                                    }


                                    oTabOrdiniDiPosto.Rows.Add(oRow);
                                }
                            }
                        }
                    }

                    // lettura TITOLI_ACCESSO per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    if (lRet)
                    {
                        oTempList = new System.Collections.SortedList();

                        //oRS.MoveFirst();
                        //while (!oRS.EOF)
                        //{
                        //    if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                        //        oRS.Fields("titolo_iva_preassolta").Value.ToString() == "N" &&
                        //        Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0)
                        //    {
                        //        DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                        //        string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                        //                      oRS.Fields("codice_locale").Value.ToString() +
                        //                      dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                        //                      dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                        //                      oRS.Fields("ordine_di_posto").Value.ToString() +
                        //                      oRS.Fields("tipo_titolo").Value.ToString();

                        //        if (!oTempList.ContainsKey(cKey))
                        //        {
                        //            oTempList.Add(cKey, null);
                        //            System.Data.DataRow oRow = oTabTitoliAccesso.NewRow();

                        //            oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                        //                                          oRS.Fields("codice_locale").Value.ToString(),
                        //                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                        //                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                        //                                          oRS.Fields("ordine_di_posto").Value.ToString(),
                        //                                          oRS.Fields("tipo_titolo").Value.ToString(),
                        //                                          Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()),
                        //                                          Decimal.Parse(oRS.Fields("tot_corrispettivo_titolo").Value.ToString()) * 100,
                        //                                          Decimal.Parse(oRS.Fields("tot_corrispettivo_prevendita").Value.ToString()) * 100,
                        //                                          Decimal.Parse(oRS.Fields("tot_iva_titolo").Value.ToString()) * 100,
                        //                                          Decimal.Parse(oRS.Fields("tot_iva_prevendita").Value.ToString()) * 100,
                        //                                          Decimal.Parse(oRS.Fields("tot_imposta_intrattenimento").Value.ToString()) * 100,
                        //                                          Decimal.Parse(oRS.Fields("tot_importo_prestazione1").Value.ToString()) * 100 + 
                        //                                          Decimal.Parse(oRS.Fields("tot_importo_prestazione3").Value.ToString()) * 100 + 
                        //                                          Decimal.Parse(oRS.Fields("tot_importo_prestazione2").Value.ToString()) * 100};
                        //            oTabTitoliAccesso.Rows.Add(oRow);
                        //        }
                        //    }

                        //    oRS.MoveNext();
                        //}


                        foreach (DataRow oRowData in tabRIEPILOGHI_BASE.Rows)
                        {
                            if (oRowData["titolo_abbonamento"].ToString() == "T" &&
                                oRowData["titolo_iva_preassolta"].ToString() == "N" &&
                                Int64.Parse(oRowData["qta_emessi"].ToString()) > 0)
                            {
                                DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                                string cKey = oRowData["codice_fiscale_organizzatore"].ToString() +
                                              oRowData["codice_locale"].ToString() +
                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                              oRowData["ordine_di_posto"].ToString() +
                                              oRowData["tipo_titolo"].ToString();

                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);
                                    System.Data.DataRow oRow = oTabTitoliAccesso.NewRow();

                                    oRow.ItemArray = new object[]{oRowData["codice_fiscale_organizzatore"].ToString(),
                                                                  oRowData["codice_locale"].ToString(),
                                                                  dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                  dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                  oRowData["ordine_di_posto"].ToString(),
                                                                  oRowData["tipo_titolo"].ToString(),
                                                                  Int64.Parse(oRowData["qta_emessi").Value.ToString()),
                                                                  Decimal.Parse(oRS.Fields("tot_corrispettivo_titolo").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("tot_corrispettivo_prevendita").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("tot_iva_titolo").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("tot_iva_prevendita").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("tot_imposta_intrattenimento").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("tot_importo_prestazione1").Value.ToString()) * 100 +
                                                                  Decimal.Parse(oRS.Fields("tot_importo_prestazione3").Value.ToString()) * 100 +
                                                                  Decimal.Parse(oRS.Fields("tot_importo_prestazione2").Value.ToString()) * 100};
                                    oTabTitoliAccesso.Rows.Add(oRow);
                                }
                            }

                            oRS.MoveNext();
                        }

                    }

                    // lettura TITOLI_ANNULLATI per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    if (lRet)
                    {
                        oRS.MoveFirst();
                        oTempList = new System.Collections.SortedList();

                        while (!oRS.EOF)
                        {
                            if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                                oRS.Fields("titolo_iva_preassolta").Value.ToString() == "N" &&
                                Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0)
                            {
                                DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                                string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                              oRS.Fields("codice_locale").Value.ToString() +
                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                              oRS.Fields("ordine_di_posto").Value.ToString() +
                                              oRS.Fields("tipo_titolo").Value.ToString();

                                if (!oTempList.ContainsKey(cKey))
                                {
                                    oTempList.Add(cKey, null);
                                    System.Data.DataRow oRow = oTabTitoliAnnullati.NewRow();

                                    oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                                  oRS.Fields("codice_locale").Value.ToString(),
                                                                  dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                                  dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                                  oRS.Fields("ordine_di_posto").Value.ToString(),
                                                                  oRS.Fields("tipo_titolo").Value.ToString(),
                                                                  Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()),
                                                                  Decimal.Parse(oRS.Fields("ann_corrispettivo_titolo").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("ann_corrispettivo_prevendita").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("ann_iva_titolo").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("ann_iva_prevendita").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("ann_imposta_intrattenimento").Value.ToString()) * 100,
                                                                  Decimal.Parse(oRS.Fields("ann_importo_prestazione1").Value.ToString()) * 100 + 
                                                                  Decimal.Parse(oRS.Fields("ann_importo_prestazione3").Value.ToString()) * 100 + 
                                                                  Decimal.Parse(oRS.Fields("ann_importo_prestazione2").Value.ToString()) * 100};
                                    oTabTitoliAnnullati.Rows.Add(oRow);
                                }
                            }

                            oRS.MoveNext();
                        }
                    }

                    // lettura TITOLI_IVA_PREASSOLTA per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "F" &&
                            Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0)
                        {
                            DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_locale").Value.ToString() +
                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                          oRS.Fields("ordine_di_posto").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabTitoliIVAPreassolta.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_locale").Value.ToString(),
                                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_figurativa").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_imposta_intra_figurativa").Value.ToString()) * 100};
                                oTabTitoliIVAPreassolta.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }


                    // lettura TITOLI_IVA_PREASSOLTA_ANNULLATI per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "F" &&
                            Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0)
                        {
                            DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_locale").Value.ToString() +
                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                          oRS.Fields("ordine_di_posto").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabTitoliIVAPreassoltaAnnullati.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_locale").Value.ToString(),
                                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("ann_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_iva_figurativa").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_imposta_intra_figurativa").Value.ToString()) * 100};
                                oTabTitoliIVAPreassoltaAnnullati.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }



                    // lettura BIGLIETTI_ABBONAMENTO per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "B" &&
                            Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0)
                        {
                            DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_locale").Value.ToString() +
                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                          oRS.Fields("ordine_di_posto").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() + 
                                          oRS.Fields("codice_fiscale_abbonamento").Value.ToString() +
                                          oRS.Fields("codice_biglietto_abbonamento").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabBigliettiAbbonamento.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_locale").Value.ToString(),
                                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                                                              oRS.Fields("codice_fiscale_abbonamento").Value.ToString(),
                                                              oRS.Fields("codice_biglietto_abbonamento").Value.ToString(),
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_figurativa").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_imposta_intra_figurativa").Value.ToString()) * 100};
                                oTabBigliettiAbbonamento.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }


                    // lettura BIGLIETTI_ABBONAMENTO ANNULLATI per ORGANIZZATORE + EVENTO + ORDINE_DI_POSTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "T" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "B" &&
                            Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0)
                        {
                            DateTime dDataOraEvento = (DateTime)oRS.Fields("data_ora_evento").Value;
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_locale").Value.ToString() +
                                          dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00") +
                                          dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00") +
                                          oRS.Fields("ordine_di_posto").Value.ToString() +
                                          oRS.Fields("codice_fiscale_abbonamento").Value.ToString() +
                                          oRS.Fields("codice_biglietto_abbonamento").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabBigliettiAbbonamentoAnnullati.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_locale").Value.ToString(),
                                                              dDataOraEvento.Year.ToString("0000") + dDataOraEvento.Month.ToString("00") + dDataOraEvento.Day.ToString("00"),
                                                              dDataOraEvento.Hour.ToString("00") + dDataOraEvento.Minute.ToString("00"),
                                                              oRS.Fields("ordine_di_posto").Value.ToString(),
                                                              oRS.Fields("codice_fiscale_abbonamento").Value.ToString(),
                                                              oRS.Fields("codice_biglietto_abbonamento").Value.ToString(),
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("ann_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_iva_figurativa").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_imposta_intra_figurativa").Value.ToString()) * 100};
                                oTabBigliettiAbbonamentoAnnullati.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }

                    // lettura ABBONAMENTI per ORGANIZZATORE
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "A" && 
                            (Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0 || Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0))
                        {
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_abbonamento").Value.ToString() +
                                          oRS.Fields("data_limite_validita").Value.ToString() + 
                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                                          oRS.Fields("tipo_turno").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() +
                                          oRS.Fields("numero_eventi_abilitati").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabAbbonamenti.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_abbonamento").Value.ToString(),
                                                              oRS.Fields("data_limite_validita").Value.ToString(),
                                                              oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                                                              oRS.Fields("tipo_turno").Value.ToString(),
                                                              "",
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("numero_eventi_abilitati").Value.ToString())};
                                oTabAbbonamenti.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }

                    // lettura ABBONAMENTI EMESSI per ABBONAMENTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "A" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "N" &&
                            Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0)
                        {
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_abbonamento").Value.ToString() +
                                          oRS.Fields("data_limite_validita").Value.ToString() +
                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                                          oRS.Fields("tipo_turno").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() +
                                          oRS.Fields("numero_eventi_abilitati").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabAbbonamentiEmessi.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_abbonamento").Value.ToString(),
                                                              oRS.Fields("data_limite_validita").Value.ToString(),
                                                              oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                                                              oRS.Fields("tipo_turno").Value.ToString(),
                                                              "",
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("numero_eventi_abilitati").Value.ToString()),
                                                              Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_titolo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_prevendita").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_titolo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_prevendita").Value.ToString()) * 100};

                                oTabAbbonamentiEmessi.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }

                    // lettura ABBONAMENTI ANNULLATI per ABBONAMENTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "A" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() == "N" &&
                            Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0)
                        {
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_abbonamento").Value.ToString() +
                                          oRS.Fields("data_limite_validita").Value.ToString() +
                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                                          oRS.Fields("tipo_turno").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() +
                                          oRS.Fields("numero_eventi_abilitati").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabAbbonamentiAnnullati.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_abbonamento").Value.ToString(),
                                                              oRS.Fields("data_limite_validita").Value.ToString(),
                                                              oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                                                              oRS.Fields("tipo_turno").Value.ToString(),
                                                              "",
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("numero_eventi_abilitati").Value.ToString()),
                                                              Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("ann_corrispettivo_titolo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_corrispettivo_prevendita").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_iva_titolo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("ann_iva_prevendita").Value.ToString()) * 100};
                                oTabAbbonamentiAnnullati.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }


                    // lettura ABBONAMENTI IVA PREASSOLTA EMESSI per ABBONAMENTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "A" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() != "N" &&
                            Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()) > 0)
                        {
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_abbonamento").Value.ToString() +
                                          oRS.Fields("data_limite_validita").Value.ToString() +
                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                                          oRS.Fields("tipo_turno").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() +
                                          oRS.Fields("numero_eventi_abilitati").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabAbbonamentiEmessi.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_abbonamento").Value.ToString(),
                                                              oRS.Fields("data_limite_validita").Value.ToString(),
                                                              oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                                                              oRS.Fields("tipo_turno").Value.ToString(),
                                                              "",
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("numero_eventi_abilitati").Value.ToString()),
                                                              Int64.Parse(oRS.Fields("qta_emessi").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_figurativa").Value.ToString()) * 100};

                                oTabAbbIvaPreassoltaEmessi.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }

                    // lettura ABBONAMENTI IVA PREASSOLTA ANNULLATI per ABBONAMENTO
                    oRS.MoveFirst();
                    oTempList = new System.Collections.SortedList();

                    while (!oRS.EOF)
                    {
                        if (oRS.Fields("titolo_abbonamento").Value.ToString() == "A" &&
                            oRS.Fields("titolo_iva_preassolta").Value.ToString() != "N" &&
                            Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()) > 0)
                        {
                            string cKey = oRS.Fields("codice_fiscale_organizzatore").Value.ToString() +
                                          oRS.Fields("codice_abbonamento").Value.ToString() +
                                          oRS.Fields("data_limite_validita").Value.ToString() +
                                          oRS.Fields("spettacolo_intrattenimento").Value.ToString() +
                                          oRS.Fields("tipo_turno").Value.ToString() +
                                          oRS.Fields("tipo_titolo").Value.ToString() +
                                          oRS.Fields("numero_eventi_abilitati").Value.ToString();

                            if (!oTempList.ContainsKey(cKey))
                            {
                                oTempList.Add(cKey, null);
                                System.Data.DataRow oRow = oTabAbbonamentiAnnullati.NewRow();

                                oRow.ItemArray = new object[]{oRS.Fields("codice_fiscale_organizzatore").Value.ToString(),
                                                              oRS.Fields("codice_abbonamento").Value.ToString(),
                                                              oRS.Fields("data_limite_validita").Value.ToString(),
                                                              oRS.Fields("spettacolo_intrattenimento").Value.ToString(),
                                                              oRS.Fields("tipo_turno").Value.ToString(),
                                                              "",
                                                              oRS.Fields("tipo_titolo").Value.ToString(),
                                                              Int64.Parse(oRS.Fields("numero_eventi_abilitati").Value.ToString()),
                                                              Int64.Parse(oRS.Fields("qta_annullati").Value.ToString()),
                                                              Decimal.Parse(oRS.Fields("tot_corrispettivo_figurativo").Value.ToString()) * 100,
                                                              Decimal.Parse(oRS.Fields("tot_iva_figurativa").Value.ToString()) * 100};
                            
                                oTabAbbIvaPreassoltaAnnullati.Rows.Add(oRow);
                            }
                        }

                        oRS.MoveNext();
                    }

                    oRS.Close();
                }
            }
            catch (Exception exCreaDataSetRiepilogo)
            {
                oError = exCreaDataSetRiepilogo;
            }

            return oDataSet;
        }
