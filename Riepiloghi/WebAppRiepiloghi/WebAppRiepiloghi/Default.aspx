﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs"
    Inherits="ServiceConsumer" ValidateRequest="false" %>

<!DOCTYPE html>
<html>
<head id="Head1">
    <title>consume JSON web service</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.2.0/sandstone/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
    <script src="Scripts/table.js"></script>
    <style>
        body { padding-top:50px; }
    </style>
</head>
<body>
    <form>
<div ng-app="myApp" ng-controller="mainController">
    <p>Ordinamento: {{ sortDesc }}</p>
<table class="table table-bordered table-striped">
<thead>
    <tr>
        <td ng-repeat="hd in fields">
            <a href="#" ng-click="orderFunction(hd.key)">
                <span style="white-space:nowrap">{{hd.desc}}</span>
            </a>
            <span ng-show="sortType == hd.key && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == hd.key && sortReverse" class="fa fa-caret-up"></span>
        </td>
    </tr>
    <tr>
        <td ng-repeat="hd in fields">
            <span><input type="text" class="form-control" ng-model="hd.value"></span>
        </td>
    </tr>
</thead>
<tbody>
    <tr ng-animate="'animate'"  ng-repeat="row in rows | orderBy:sortType:sortReverse | filter: filterFunction">
        <td ng-repeat="hd in fields"><span>{{row[hd.key]}}</span></td>
    </tr>
</tbody>
</table>
</div>
        </form>
    <!--
    <form id="form1" runat="server">
    <div>
        <div ng-controller="transazioniController">
            <table>
                <tr ng-repeat="row in TRANSAZIONI">
                    <td ng-repeat="col in row track by $index">{{col}}</td>
                </tr>
            </table>
        </div>
        <br />
    </div>
    <script>

        var app = angular.module('serviceConsumer', []);

        app.controller('transazioniController', function ($scope, $http) {

            var urlTransazioni = "WebServiceToRiepiloghi.asmx/GetTransazioni";

            $http.get(urlTransazioni)
                       .success(function (data) {

                           var myjson = JSON.parse(data);

                           $scope.TRANSAZIONI = JSON.parse(myjson);

                       })
        })

    </script>
    </form>
    -->
</body>
</html>
