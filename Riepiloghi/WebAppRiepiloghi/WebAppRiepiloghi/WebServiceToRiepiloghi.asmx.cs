﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

public class ItemCampo
{
    public string key = "";
    public string desc = "";
    public string value = "";
    public string format = "";

    public ItemCampo()
    {
    }
    public ItemCampo(string cKey, string cDesc, string cTipo)
    {
        this.key = cKey;
        this.desc = cDesc;
        switch (cTipo)
        {
            case "S": { this.format = "'string'"; break; }
            case "D": { this.format = "'date:short'"; break; }
            case "N": { this.format = "'number'"; break; }
            case "C": { this.format = "'number'"; break; }
        }
    }
}

/// <summary>
    /// Descrizione di riepilogo per WebServiceToRiepiloghi
    /// </summary>
    [WebService(Namespace = "AnyNameSpace")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class WebServiceToRiepiloghi : System.Web.Services.WebService
    {
        WebAppRiepiloghi.WebServiceRiepiloghi.ServiceRiepiloghi oService;
        
        public WebServiceToRiepiloghi()
        {
            this.oService = new WebAppRiepiloghi.WebServiceRiepiloghi.ServiceRiepiloghi();
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetCampiTransazioni()
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            WebAppRiepiloghi.WebServiceRiepiloghi.ResultBaseCampiLogTransazioni oResult = oService.GetListaCampiLogTransazioni("serverx", "crea", "crea");
            Response data = new Response();
            List<ItemCampo> ListaCampi = new List<ItemCampo>();
            foreach (WebAppRiepiloghi.WebServiceRiepiloghi.clsResultBaseCampoLogTransazioni Campo in oResult.Campi)
            {
                ListaCampi.Add(new ItemCampo(Campo.Campo, Campo.DescCampo, Campo.Tipo));
            }
            data.Message = js.Serialize(ListaCampi);
            Context.Response.Write(js.Serialize(data.Message));
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetTransazioni()
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            DateTime dStart = new DateTime(2015, 01, 01);
            DateTime dStop = new DateTime(2016, 01, 01);
            WebAppRiepiloghi.WebServiceRiepiloghi.ResultBaseGetTransazioni oResult = oService.GetTransazioni("serverx", "crea", "crea", dStart, dStop, "TRUE", "TRUE");
            WebAppRiepiloghi.WebServiceRiepiloghi.ResultBaseCampiLogTransazioni oResultCampi = oService.GetListaCampiLogTransazioni("serverx", "crea", "crea");
            Response data = new Response();
            data.Message = ConvertDataTableTojSonString(oResult.TableTransazioni, oResultCampi);
            Context.Response.Write(js.Serialize(data.Message));
        }

        public String ConvertDataTableTojSonString(DataTable dataTable, WebAppRiepiloghi.WebServiceRiepiloghi.ResultBaseCampiLogTransazioni oResultCampi)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
                   new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<String, Object>> tableRows = new List<Dictionary<String, Object>>();

            Dictionary<String, Object> row;

            Dictionary<string, string> dataTypes = new Dictionary<string, string>();
            foreach (WebAppRiepiloghi.WebServiceRiepiloghi.clsResultBaseCampoLogTransazioni Campo in oResultCampi.Campi)
            {
                dataTypes.Add(Campo.Campo, Campo.Tipo);
            }

            foreach (DataRow dr in dataTable.Rows)
            {
                row = new Dictionary<String, Object>();
                foreach (DataColumn col in dataTable.Columns)
                {
                    object value = dr[col];
                    if (value != null && value.ToString() != "")
                    {
                        if (dataTypes.ContainsKey(col.ColumnName) && dataTypes[col.ColumnName] == "D")
                        {
                            try
                            {
                                value = ((DateTime)dr[col]).ToShortDateString();
                            }
                            catch
                            {
                            }
                            
                        }
                    }
                    row.Add(col.ColumnName, value);
                }
                tableRows.Add(row);
            }
            return serializer.Serialize(tableRows);
        }

        public DataTable GetDataTable()
        {
            DataTable Table = new DataTable("Questions");
            Table.Columns.Add(new DataColumn("QuestionID"));
            Table.Columns.Add(new DataColumn("QuestionText"));
            DataRow oRow = Table.NewRow();
            oRow.ItemArray = new object[] { 1, "ciao" };
            Table.Rows.Add(oRow);
            return Table;
        }

        public class Response
        {
            public String Message;
        }
    }
