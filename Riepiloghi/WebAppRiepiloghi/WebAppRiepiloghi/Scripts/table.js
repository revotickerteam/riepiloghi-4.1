﻿// app.js
angular.module('myApp', [])

.controller('mainController', function ($scope, $http) {

    var urlCampi = "WebServiceToRiepiloghi.asmx/GetCampiTransazioni";

    $scope.sortType = '';
    $scope.sortDesc = '';
    $scope.sortReverse = false;
    $http.get(urlCampi)
                       .success(function (data) {
                           var myjson = JSON.parse(data);
                           $scope.fields = JSON.parse(myjson);
                           $scope.sortType = $scope.fields[0].key;
                           $scope.sortDesc = $scope.fields[0].desc;
                       });

    
    
    /*$scope.fields = [{ key: 'CODICE_SISTEMA', value: '' },
                     { key: 'CODICE_CARTA', value: '' },
                     { key: 'SIGILLO', value: '' }];
                     */
    var urlTransazioni = "WebServiceToRiepiloghi.asmx/GetTransazioni";
    $http.get(urlTransazioni)
                       .success(function (data) {
                           var myjson = JSON.parse(data);
                           $scope.rows = JSON.parse(myjson);
                       });

    $scope.orderFunction = function (key) {
        if ($scope.sortType != key) {
            $scope.sortType = key;
            $scope.sortReverse = false;
        }
        else {
            $scope.sortReverse = !$scope.sortReverse;
        }
        
        for (i = 0; i < $scope.fields.length; i++) {
            if ($scope.fields[i].key == key) {
                $scope.sortDesc = $scope.fields[i].desc;
                break;
            }
        }
    }

    $scope.filterFunction = function (element) {
        var lFilterOK = true;
        for (i = 0; i < $scope.fields.length; i++) {
            var value = new String(element[$scope.fields[i].key]).toUpperCase();
            var filter = new String($scope.fields[i].value).toUpperCase();
            if (filter != '' && !value.match(filter)){
                lFilterOK = false;
                break;
            }
        }
        return lFilterOK;
    };
});
