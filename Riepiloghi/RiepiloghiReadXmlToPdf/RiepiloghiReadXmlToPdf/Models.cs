﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace RiepiloghiReadXmlToPdf
{
    
    public class FileRiepilogo
    {
        public string FileName { get; set; }

        public string TipoRiepilogo
        {
            get
            {
                string result = "";
                if (!string.IsNullOrEmpty(this.FileName))
                {
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(this.FileName);
                    result = GetTipoRiepilogo(fileInfo.Name);
                }
                return result;
            }
        }

        public static string GetTipoRiepilogo(string fileName)
        {
            string result = "";
            string regExRPG = @"RPG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m";
            string regExRPM = @"RPM_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m";
            Regex r = new Regex(regExRPG);
            if (r.IsMatch(fileName))
                result = "RPG";
            else
            {
                r = new Regex(regExRPM);
                if (r.IsMatch(fileName))
                    result = "RPM";
            }
            return result;
        }

        public static DateTime? GetDataFileRiepilogo(string fileName, out string descData, out int contatore)
        {
            descData = "";
            contatore = 0;
            DateTime? result = null;
            string regExRPG = @"RPG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m";
            string regExRPM = @"RPM_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m";
            string tipo = "";
            Regex r = new Regex(regExRPG);
            if (r.IsMatch(fileName))
            {
                tipo = "RPG";
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), int.Parse(fileName.Substring(12, 2)));
            }
            else
            {
                r = new Regex(regExRPM);
                if (r.IsMatch(fileName))
                {
                    tipo = "RPM";
                    result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), 1);
                }
            }

            if (!string.IsNullOrEmpty(tipo) && result != null)
            {
                string cDescr = (tipo == "RPG" ? "Giornaliero" : "Mensile");
                string cValue = "";
                if (tipo == "RPG")
                    cValue = result.Value.ToLongDateString();
                else
                {
                    cValue = result.Value.ToLongDateString();
                    cValue = cValue.Split(' ')[2] + " " + cValue.Split(' ')[3];
                }
                if (!string.IsNullOrEmpty(cValue) && cValue.Contains(" "))
                {
                    string tempValue = cValue;
                    cValue = "";
                    foreach (string segItem in tempValue.Split(' '))
                    {
                        cValue += (cValue.Trim() == "" ? "" : " ") + (segItem.Length > 1 ? segItem.Substring(0, 1).ToUpper() + segItem.Substring(1) : segItem);
                    }
                }
                
                string cProg = fileName.Substring(15, 3);
                int.TryParse(cProg, out contatore);
                descData = string.Format("{0} {1} prog.{2}", cDescr, cValue, cProg);
            }

            return result;
        }

        public static string GetFilePdf(string FilePdfToSave, string fileName)
        {
            string result = GetFilePdf(FilePdfToSave, new List<string>() { fileName });
            return result;
        }

        public delegate void delegateOutProcess(long index, long counter, string message);

        public static event delegateOutProcess OutProcess;

        private static void RaiseOutProcess(long index, long counter, string message)
        {
            if (OutProcess != null)
                OutProcess(index, counter, message);
        }

        public static string GetFilePdf(string FilePdfToSave, List<string> fileNames)
        {
            string result = "";
            Exception error = null;
            System.Collections.SortedList PagineModuliC1C2 = new System.Collections.SortedList();
            long counter = fileNames.Count;
            long index = 0;
            foreach (string fileName in fileNames)
            {
                index += 1;
                RaiseOutProcess(index, counter, "lettura");
                RiepilogoRPGRPM riepilogo = null;
                if (GetDataFromFile(fileName, out riepilogo, out error))
                {
                    RaiseOutProcess(index, counter, "costruzione dati");
                    System.Collections.SortedList pagineRiepilogo = GetModuliFromData(riepilogo);
                    if (pagineRiepilogo != null && pagineRiepilogo.Count > 0)
                    {
                        RaiseOutProcess(index, counter, "aggiunta");
                        foreach (System.Collections.DictionaryEntry oItemModuloC1C2Dict in pagineRiepilogo)
                        {
                            PagineModuliC1C2.Add(PagineModuliC1C2.Count, oItemModuloC1C2Dict.Value);
                        }
                    }
                }
            }

            if (PagineModuliC1C2.Count > 0)
            {
                result = MakeFilePdf.Make(FilePdfToSave, PagineModuliC1C2);
            }

            return result;
        }



        public static bool GetDataFromFile(string fileName, out RiepilogoRPGRPM riepilogo, out Exception error)
        {
            bool result = false;
            error = null;

            XmlDocument document = null;
            string tipo = "";
            string tagRiepilogo = "";
            string Sostituzione = "";
            string Data = "";
            string Mese = "";
            string DataGenerazione = "";
            string OraGenerazione = "";
            string ProgressivoGenerazione = "";

            riepilogo = null;

            string nomeDelFile = "";

            try
            {
                System.IO.FileInfo fInfo = new System.IO.FileInfo(fileName);
                nomeDelFile = fInfo.Name;
                tipo = GetTipoRiepilogo(fInfo.Name);
                if (fInfo.Exists)
                {
                    document = new XmlDocument();
                    try
                    {
                        document.Load(fileName);
                    }
                    catch (Exception exP7m)
                    {
                        try
                        {
                            document = null;
                            string xml = System.IO.File.ReadAllText(fileName);
                            if (xml.Contains(@"<?xml"))
                            {
                                xml = xml.Substring(xml.IndexOf(@"<?xml"));
                                if (xml.Contains(@"</RiepilogoGiornaliero>"))
                                {
                                    xml = xml.Substring(0, xml.IndexOf(@"</RiepilogoGiornaliero>") + 23);
                                    tagRiepilogo = "RiepilogoGiornaliero";
                                }
                                else if (xml.Contains(@"</RiepilogoMensile>"))
                                {
                                    xml = xml.Substring(0, xml.IndexOf(@"</RiepilogoMensile>") + 19);
                                    tagRiepilogo = "RiepilogoMensile";
                                }
                                document = new XmlDocument();
                                document.LoadXml(xml);
                            }
                        }
                        catch (Exception ex)
                        {
                            document = null;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                //throw ex;
            }

            if (document != null)
            {
                try
                {
                    
                    XmlNodeList nodeList;
                    nodeList = document.GetElementsByTagName(tagRiepilogo);
                    if (nodeList != null && nodeList.Count == 1)
                    {
                        XmlNode nodeRiepilogo = nodeList[0];
                        if (nodeRiepilogo.Attributes != null)
                        {
                            /* 
                             * <RiepilogoGiornaliero Sostituzione="N" Data="20220227" DataGenerazione="20220227" OraGenerazione="210203" ProgressivoGenerazione="1">
                             * <RiepilogoMensile Sostituzione="S" Mese="202111" DataGenerazione="20211113" OraGenerazione="221931" ProgressivoGenerazione="4">
                             * */
                            foreach (XmlAttribute attr in nodeRiepilogo.Attributes)
                            {
                                switch (attr.Name)
                                {
                                    case "Sostituzione": { Sostituzione = attr.Value; break; }
                                    case "Data": { Data = attr.Value; break; }
                                    case "Mese": { Mese = attr.Value; break; }
                                    case "DataGenerazione": { DataGenerazione = attr.Value; break; }
                                    case "OraGenerazione": { OraGenerazione = attr.Value; break; }
                                    case "ProgressivoGenerazione": { ProgressivoGenerazione = attr.Value; break; }
                                }
                            }
                        }

                        riepilogo = new RiepilogoRPGRPM(nomeDelFile);
                        riepilogo.Tipo = tipo;
                        riepilogo.Sostituzione = Sostituzione;
                        if (tipo == "RPG" && Data.Length == 8)
                            riepilogo.Data = new DateTime(int.Parse(Data.Substring(0, 4)), int.Parse(Data.Substring(4, 2)), int.Parse(Data.Substring(6, 2)));
                        else if (tipo == "RPM" && Mese.Length == 6)
                            riepilogo.Mese = new DateTime(int.Parse(Mese.Substring(0, 4)), int.Parse(Mese.Substring(4, 2)), 1);
                        if (DataGenerazione.Length == 8)
                            riepilogo.DataGenerazione = new DateTime(int.Parse(DataGenerazione.Substring(0, 4)), int.Parse(DataGenerazione.Substring(4, 2)), int.Parse(DataGenerazione.Substring(6, 2)));
                        riepilogo.OraGenerazione = OraGenerazione;
                        riepilogo.ProgressivoGenerazione = int.Parse(ProgressivoGenerazione);

                        nodeList = document.GetElementsByTagName("Titolare");
                        if (nodeList != null && nodeList.Count == 1)
                        {
                            XmlNode nodeTitolare = nodeList[0];
                            if (nodeTitolare.ChildNodes.Count > 0)
                            {
                                riepilogo.Titolare = new Titolare();
                                CopyXmlToObject(typeof(Titolare), riepilogo.Titolare, nodeTitolare);
                            }
                        }

                        if (riepilogo.Titolare != null)
                        {
                            riepilogo.Titolare.Organizzatore = new List<Organizzatore>();
                            XmlNodeList nodeListOrganizzatori = document.GetElementsByTagName("Organizzatore");
                            foreach (XmlNode nodeOrg in nodeListOrganizzatori)
                            {
                                if (nodeOrg.ChildNodes.Count > 0)
                                {
                                    Organizzatore organizzatore = new Organizzatore();
                                    CopyXmlToObject(typeof(Organizzatore), organizzatore, nodeOrg);
                                    organizzatore.NodoDatiOrganizzatore = nodeOrg;
                                    riepilogo.Titolare.Organizzatore.Add(organizzatore);
                                }
                            }
                        }

                        if (riepilogo.Titolare != null && riepilogo.Titolare.Organizzatore.Count > 0)
                        {
                            foreach (Organizzatore organizzatore in riepilogo.Titolare.Organizzatore)
                            {
                                foreach (XmlNode nodeEvento in organizzatore.NodoDatiOrganizzatore.ChildNodes)
                                {
                                    if (nodeEvento.Name != "Evento") continue;
                                    
                                    if (organizzatore.Evento == null) organizzatore.Evento = new List<Evento>();
                                    
                                    Evento evento = new Evento();
                                    CopyXmlToObject(typeof(Evento), evento, nodeEvento);
                                    evento.IsMensile = riepilogo.Tipo == "RPM";

                                    nodeEvento.ChildNodes.OfType<XmlNode>().ToList().Where(nodeMultiGenere => nodeMultiGenere.Name == "MultiGenere").ToList().ForEach(nodeMultiGenere =>
                                    {
                                        nodeMultiGenere.ChildNodes.OfType<XmlNode>().ToList().Where(nodeTitoliOpere => nodeTitoliOpere.Name == "TitoliOpere").ToList().ForEach(nodeTitoliOperare =>
                                        {
                                            TitoliOpere titoliOpere = new TitoliOpere();
                                            CopyXmlToObject(typeof(TitoliOpere), titoliOpere, nodeTitoliOperare);
                                            evento.MultiGenere.TitoliOpere.Add(titoliOpere);
                                        });
                                    });

                                    nodeEvento.ChildNodes.OfType<XmlNode>().ToList().Where(nodeOrdineDiPosto => nodeOrdineDiPosto.Name == "OrdineDiPosto").ToList().ForEach(nodeOrdineDiPosto =>
                                    {
                                        OrdineDiPosto ordineDiPosto = new OrdineDiPosto();
                                        CopyXmlToObject(typeof(OrdineDiPosto), ordineDiPosto, nodeOrdineDiPosto);
                                        evento.OrdineDiPosto.Add(ordineDiPosto);
                                        List<string> tagsBiglietti = new List<string>() { "TitoliAccesso", "TitoliAnnullati", "TitoliAccessoIVAPreassolta", "TitoliIVAPreassoltaAnnullati", "BigliettiAbbonamento", "BigliettiAbbonamentoAnnullati" };
                                        
                                        nodeOrdineDiPosto.ChildNodes.OfType<XmlNode>().ToList().Where(nodeBiglietti => tagsBiglietti.Contains(nodeBiglietti.Name)).ToList().ForEach(nodeBiglietti =>
                                        {
                                            switch (nodeBiglietti.Name)
                                            {
                                                case "TitoliAccesso":
                                                    {
                                                        TitoliAccesso biglietti = new TitoliAccesso();
                                                        CopyXmlToObject(typeof(TitoliAccesso), biglietti, nodeBiglietti);
                                                        ordineDiPosto.TitoliAccesso.Add(biglietti);
                                                        break;
                                                    }
                                                case "TitoliAnnullati":
                                                    {
                                                        TitoliAccesso biglietti = new TitoliAccesso();
                                                        CopyXmlToObject(typeof(TitoliAccesso), biglietti, nodeBiglietti);
                                                        ordineDiPosto.TitoliAnnullati.Add(biglietti);
                                                        break;
                                                    }
                                                case "TitoliAccessoIVAPreassolta":
                                                    {
                                                        TitoliAccessoIVAPreassolta biglietti = new TitoliAccessoIVAPreassolta();
                                                        CopyXmlToObject(typeof(TitoliAccessoIVAPreassolta), biglietti, nodeBiglietti);
                                                        ordineDiPosto.TitoliAccessoIVAPreassolta.Add(biglietti);
                                                        break;
                                                    }
                                                case "TitoliIVAPreassoltaAnnullati":
                                                    {
                                                        TitoliAccessoIVAPreassolta biglietti = new TitoliAccessoIVAPreassolta();
                                                        CopyXmlToObject(typeof(TitoliAccessoIVAPreassolta), biglietti, nodeBiglietti);
                                                        ordineDiPosto.TitoliIVAPreassoltaAnnullati.Add(biglietti);
                                                        break;
                                                    }
                                                case "BigliettiAbbonamento":
                                                    {
                                                        BigliettiAbbonamento biglietti = new BigliettiAbbonamento();
                                                        CopyXmlToObject(typeof(BigliettiAbbonamento), biglietti, nodeBiglietti);
                                                        ordineDiPosto.BigliettiAbbonamento.Add(biglietti);
                                                        break;
                                                    }
                                                case "BigliettiAbbonamentoAnnullati":
                                                    {
                                                        BigliettiAbbonamento biglietti = new BigliettiAbbonamento();
                                                        CopyXmlToObject(typeof(BigliettiAbbonamento), biglietti, nodeBiglietti);
                                                        ordineDiPosto.BigliettiAbbonamentoAnnullati.Add(biglietti);
                                                        break;
                                                    }
                                            }
                                        });
                                    });

                                    organizzatore.Evento.Add(evento);
                                }
                            }
                        }

                    }
                }
                catch (Exception exParsingXMl)
                {
                    error = new Exception("File non valido " + exParsingXMl.Message);
                }

            }
            else
                error = new Exception("File non valido");
            result = error == null;

            return result;
        }

        public static System.Collections.SortedList GetModuliFromData(RiepilogoRPGRPM riepilogo)
        {
            List<string> tipiTitoloOmaggio = GetTipiTitoloOmaggio();
            List<TipoEventoDescriptor> tipiEvento = TipoEventoDescriptor.GetTipiEvento();
            int countEvento = 0;
            System.Collections.SortedList PagineModuliC1C2 = new System.Collections.SortedList();
            if (riepilogo.Titolare != null && riepilogo.Titolare.Organizzatore != null && riepilogo.Titolare.Organizzatore.Count > 0)
            {
                if (riepilogo.Titolare != null && riepilogo.Titolare.Organizzatore != null)
                {
                    riepilogo.Titolare.Organizzatore.ForEach(organizzatore =>
                    {
                        if (organizzatore.Evento != null)
                        {
                            organizzatore.Evento.ForEach(evento =>
                            {
                                countEvento += 1;
                                TipoEventoDescriptor tipoEventoDescriptor = null;
                                string cDescTipoEvento = "";
                                decimal nPercIVA = 0;
                                decimal nPercISI = 0;
                                decimal nPercOmaggiEccedenti = 0;
                                string cLuogoLocale = "";
                                string cTitoloEvento = "";
                                string cEsecutore = "";
                                string cNazionalita = "";
                                string cAutore = "";
                                string cProduttoreCinema = "";

                                try
                                {
                                    tipoEventoDescriptor = tipiEvento.FirstOrDefault(i => i.Codice == evento.MultiGenere.TipoGenere);
                                    cDescTipoEvento = (tipoEventoDescriptor != null ? tipoEventoDescriptor.Descrizione : evento.MultiGenere.TipoGenere);
                                    nPercIVA = 0;
                                    nPercISI = 0;
                                    nPercOmaggiEccedenti = 0;
                                    cLuogoLocale = "";
                                    cTitoloEvento = "";
                                    cEsecutore = "";
                                    cNazionalita = "";
                                    cAutore = "";
                                    cProduttoreCinema = "";

                                    GetImposte(evento, out nPercIVA, out nPercISI, out nPercOmaggiEccedenti);

                                    evento.MultiGenere.TitoliOpere.ForEach(titolo =>
                                    {
                                        cTitoloEvento += (!string.IsNullOrEmpty(titolo.Titolo) ? (cTitoloEvento == "" ? "" : " ") + titolo.Titolo : "");
                                        cEsecutore += (!string.IsNullOrEmpty(titolo.Esecutore) ? (cEsecutore == "" ? "" : " ") + titolo.Esecutore : "");
                                        cNazionalita += (!string.IsNullOrEmpty(titolo.Nazionalita) ? (cNazionalita == "" ? "" : " ") + titolo.Nazionalita : "");
                                        cAutore += (!string.IsNullOrEmpty(titolo.Autore) ? (cAutore == "" ? "" : " ") + titolo.Autore : "");
                                        cProduttoreCinema += (!string.IsNullOrEmpty(titolo.ProduttoreCinema) ? (cProduttoreCinema == "" ? "" : " ") + titolo.ProduttoreCinema : "");
                                        cProduttoreCinema += (!string.IsNullOrEmpty(titolo.Distributore) ? (cProduttoreCinema == "" ? "" : " ") + titolo.Distributore : "");
                                    });

                                    clsModuloC1C2 oModuloC1C2 = new clsModuloC1C2(riepilogo.DataOraGenerazione,
                                                                                  riepilogo.Titolare.SistemaEmissione,
                                                                                  (riepilogo.Tipo == "RPG" ? "G" : "M"),
                                                                                  riepilogo.Titolare.Denominazione,
                                                                                  riepilogo.Titolare.CodiceFiscale,
                                                                                  organizzatore.Denominazione, organizzatore.CodiceFiscale,
                                                                                  evento.Intrattenimento.TipoTassazione, evento.MultiGenere.TipoGenere, cDescTipoEvento,
                                                                                  nPercIVA, nPercISI, evento.Intrattenimento.Incidenza,
                                                                                  evento.Locale.CodiceLocale, evento.Locale.Denominazione, cLuogoLocale,
                                                                                  cTitoloEvento, cEsecutore, cNazionalita, cAutore, cProduttoreCinema, evento.DataOraEvento);

                                    oModuloC1C2.NomeFileRiepilogo = riepilogo.NomeFileRiepilogo;
                                    oModuloC1C2.Progressivo = riepilogo.ProgressivoGenerazione;
                                    oModuloC1C2.Giorno = (riepilogo.Tipo == "RPG" ? riepilogo.Data : riepilogo.Mese);

                                    PagineModuliC1C2.Add(PagineModuliC1C2.Count, oModuloC1C2);


                                    List<string> listaSettoreTipiTitoloEmessi = new List<string>();
                                    List<clsModuloC1row> omaggiRifSettori = new List<clsModuloC1row>();
                                    clsModuloC1row omaggioRifEvento = new clsModuloC1row("", "", 0, "", 0, 0, 0, 0, 0, null, 0);


                                    evento.OrdineDiPosto.ForEach(ordineDiPosto =>
                                    {
                                    // aggiungo biglietti (omaggi esclusi)
                                    ordineDiPosto.TitoliAccesso.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, false, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("N", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                        ordineDiPosto.TitoliAccessoIVAPreassolta.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, false, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("F", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                        ordineDiPosto.BigliettiAbbonamento.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, false, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("B", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                    // aggiungo gli annullati se non precedentemente trattati (omaggi esclusi)
                                    ordineDiPosto.TitoliAnnullati.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, true, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("N", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                        ordineDiPosto.TitoliIVAPreassoltaAnnullati.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, true, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("F", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                        ordineDiPosto.BigliettiAbbonamentoAnnullati.ForEach(biglietti =>
                                        {
                                            if (!tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                decimal IncassoLordo = 0;
                                                decimal IncassoPrevendita = 0;
                                                decimal IVALorda = 0;
                                                decimal PrezzoUnitario = 0;
                                                decimal ImponibileImpostaIntrattenimenti = 0;
                                                decimal ImpostaIntrattenimenti = 0;
                                                decimal ImponibileIVA = 0;
                                                decimal ImportoPrestazione = 0;
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;

                                                GetValoriBiglietto(evento, ordineDiPosto, biglietti, true, out Quantita, out IncassoLordo, out IncassoPrevendita, out IVALorda, out PrezzoUnitario, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA, out ImportoPrestazione, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("B", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, Quantita, IncassoLordo, IncassoPrevendita, ImponibileImpostaIntrattenimenti, ImpostaIntrattenimenti, ImponibileIVA, IVALorda, NumeroTitoliAnnullati));

                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                if (rifOmaggioSettore == null)
                                                {
                                                    rifOmaggioSettore = new clsModuloC1row("", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, PrezzoUnitario, 0, 0, 0, 0, 0, 0, 0, 0);
                                                    omaggiRifSettori.Add(rifOmaggioSettore);
                                                }
                                                else if (rifOmaggioSettore.PrezzoUnitario < PrezzoUnitario)
                                                    rifOmaggioSettore.PrezzoUnitario = PrezzoUnitario;
                                            }
                                        });

                                    // OMAGGI

                                    // determino il biglietto più alto per evento
                                    omaggioRifEvento.PrezzoUnitario = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.Max<clsModuloC1row>(rif => rif.PrezzoUnitario) : 0);

                                    // aggiungo biglietti SOLO OMAGGI
                                    ordineDiPosto.TitoliAccesso.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, false, omaggiRifSettori, omaggioRifEvento, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("N", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, rifOmaggioSettore, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));
                                            }
                                        });

                                        ordineDiPosto.TitoliAccessoIVAPreassolta.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, false, omaggiRifSettori, omaggioRifEvento, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("F", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, rifOmaggioSettore, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));
                                            }
                                        });

                                        ordineDiPosto.BigliettiAbbonamento.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                clsModuloC1row rifOmaggioSettore = omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo == biglietti.TipoTitolo);
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, false, omaggiRifSettori, omaggioRifEvento, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("B", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, rifOmaggioSettore, NumeroTitoliAnnullati));

                                                if (!listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                                    listaSettoreTipiTitoloEmessi.Add(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo));
                                            }
                                        });

                                    // aggiungo gli annullati se non precedentemente trattati (omaggi esclusi)
                                    ordineDiPosto.TitoliAnnullati.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, true, null, null, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("N", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, null, NumeroTitoliAnnullati));
                                            }
                                        });

                                        ordineDiPosto.TitoliIVAPreassoltaAnnullati.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, true, null, null, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("F", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, null, NumeroTitoliAnnullati));
                                            }
                                        });

                                        ordineDiPosto.BigliettiAbbonamentoAnnullati.ForEach(biglietti =>
                                        {
                                            if (tipiTitoloOmaggio.Contains(biglietti.TipoTitolo) && !listaSettoreTipiTitoloEmessi.Contains(string.Format("{0}.{1}", ordineDiPosto.CodiceOrdine, biglietti.TipoTitolo)))
                                            {
                                                long Quantita = 0;
                                                long NumeroTitoliAnnullati = 0;
                                                long QtaOmaggiEccedenti_IVA = 0;
                                                long QtaOmaggiEccedenti_ISI = 0;
                                                decimal IvaOmaggiEccedenti = 0;
                                                decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
                                                GetValoriBigliettoOmaggio(evento, ordineDiPosto, biglietti, true, null, null, out Quantita, out QtaOmaggiEccedenti_IVA, out QtaOmaggiEccedenti_ISI, out IvaOmaggiEccedenti, out ImpostaIntrattenimentoOmaggiEccedenti, out NumeroTitoliAnnullati);
                                                oModuloC1C2.AddRowBiglietto(new clsModuloC1row("B", ordineDiPosto.CodiceOrdine, ordineDiPosto.Capienza, biglietti.TipoTitolo, Quantita, QtaOmaggiEccedenti_IVA, QtaOmaggiEccedenti_ISI, IvaOmaggiEccedenti, ImpostaIntrattenimentoOmaggiEccedenti, null, NumeroTitoliAnnullati));
                                            }
                                        });
                                    });

                                }
                                catch (Exception ex)
                                {
                                    string errore = "";

                                    errore = string.Format("{0}\r\n{1}\r\n{2}\r\n{3}\r\n{4}\r\n{5}\r\n{6}\r\n{7}\r\n{8}\r\n{9}\r\n{10}\r\n{11}\r\n{12}",
                                                           "ERRORE SU UN EVENTO",
                                                           cDescTipoEvento,
                                                           nPercIVA,
                                                           nPercISI,
                                                           nPercOmaggiEccedenti,
                                                           cLuogoLocale,
                                                           cTitoloEvento,
                                                           cEsecutore,
                                                           cNazionalita,
                                                           cAutore,
                                                           cProduttoreCinema,
                                                           ex.Message,
                                                           ex.ToString());
                                    throw new Exception(errore);
                                }
                            });
                        }
                    });
                }
            }
            return PagineModuliC1C2;
        }

        private static void GetValoriBiglietto(Evento evento, OrdineDiPosto ordineDiPosto, TitoliAccessoIVAPreassolta biglietti, bool isAnnullati,
                                               out long Quantita,
                                               out decimal IncassoLordo,
                                               out decimal IncassoPrevendita,
                                               out decimal IVALorda,
                                               out decimal PrezzoUnitario,
                                               out decimal ImponibileImpostaIntrattenimenti,
                                               out decimal ImpostaIntrattenimenti,
                                               out decimal ImponibileIVA,
                                               out decimal ImportoPrestazione,
                                               out long NumeroTitoliAnnullati)
        {
            TitoliAccessoIVAPreassolta annullati = null;
            Quantita = 0;
            IncassoLordo = 0;
            IncassoPrevendita = 0;
            IVALorda = 0;
            PrezzoUnitario = 0;
            ImponibileImpostaIntrattenimenti = 0;
            ImpostaIntrattenimenti = 0;
            ImponibileIVA = 0;
            ImportoPrestazione = 0;
            NumeroTitoliAnnullati = 0;

            if (!isAnnullati)
            {
                Quantita = biglietti.Quantita;
                IncassoLordo = (biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0);
                PrezzoUnitario = (IncassoLordo > 0 && biglietti.Quantita > 0 ? IncassoLordo / biglietti.Quantita : 0);
                IVALorda = (biglietti.IVAFigurativa > 0 ? biglietti.IVAFigurativa / 100 : 0);

                annullati = ordineDiPosto.TitoliIVAPreassoltaAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                    IncassoLordo = System.Math.Max(IncassoLordo - (annullati.ImportoFigurativo > 0 ? annullati.ImportoFigurativo / 100 : 0), 0);
                    IVALorda = System.Math.Max(IVALorda - (annullati.IVAFigurativa > 0 ? annullati.IVAFigurativa / 100 : 0), 0);
                    if (PrezzoUnitario == 0 && annullati.ImportoFigurativo > 0)
                        PrezzoUnitario = (annullati.ImportoFigurativo / 100) / annullati.Quantita;
                }

                if (Quantita > 0)
                {
                    ImponibileImpostaIntrattenimenti = 0;
                    ImpostaIntrattenimenti = 0;
                    ImponibileIVA = 0;
                    if (PrezzoUnitario > 0)
                        CalcImposte(evento, PrezzoUnitario, Quantita, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA);
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
                IncassoLordo = 0;
                PrezzoUnitario = ((biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0) > 0 && biglietti.Quantita > 0 ? (biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0) / biglietti.Quantita : 0);
                IVALorda = 0;
                ImponibileImpostaIntrattenimenti = 0;
                ImpostaIntrattenimenti = 0;
                ImponibileIVA = 0;
            }
        }

        private static void GetValoriBiglietto(Evento evento, OrdineDiPosto ordineDiPosto, BigliettiAbbonamento biglietti, bool isAnnullati,
                                               out long Quantita,
                                               out decimal IncassoLordo,
                                               out decimal IncassoPrevendita,
                                               out decimal IVALorda,
                                               out decimal PrezzoUnitario,
                                               out decimal ImponibileImpostaIntrattenimenti,
                                               out decimal ImpostaIntrattenimenti,
                                               out decimal ImponibileIVA,
                                               out decimal ImportoPrestazione,
                                               out long NumeroTitoliAnnullati)
        {
            TitoliAccessoIVAPreassolta annullati = null;
            Quantita = 0;
            IncassoLordo = 0;
            IncassoPrevendita = 0;
            IVALorda = 0;
            PrezzoUnitario = 0;
            ImponibileImpostaIntrattenimenti = 0;
            ImpostaIntrattenimenti = 0;
            ImponibileIVA = 0;
            ImportoPrestazione = 0;
            NumeroTitoliAnnullati = 0;

            if (!isAnnullati)
            {
                Quantita = biglietti.Quantita;
                IncassoLordo = (biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0);
                PrezzoUnitario = (IncassoLordo > 0 && biglietti.Quantita > 0 ? IncassoLordo / biglietti.Quantita : 0);
                IVALorda = (biglietti.IVAFigurativa > 0 ? biglietti.IVAFigurativa / 100 : 0);

                annullati = ordineDiPosto.TitoliIVAPreassoltaAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                    IncassoLordo = System.Math.Max(IncassoLordo - (annullati.ImportoFigurativo > 0 ? annullati.ImportoFigurativo / 100 : 0), 0);
                    IVALorda = System.Math.Max(IVALorda - (annullati.IVAFigurativa > 0 ? annullati.IVAFigurativa / 100 : 0), 0);
                    if (PrezzoUnitario == 0 && annullati.ImportoFigurativo > 0)
                        PrezzoUnitario = (annullati.ImportoFigurativo / 100) / annullati.Quantita;
                }

                if (Quantita > 0)
                {
                    ImponibileImpostaIntrattenimenti = 0;
                    ImpostaIntrattenimenti = 0;
                    ImponibileIVA = 0;
                    if (PrezzoUnitario > 0)
                        CalcImposte(evento, PrezzoUnitario, Quantita, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA);
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
                IncassoLordo = 0;
                PrezzoUnitario = ((biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0) > 0 && biglietti.Quantita > 0 ? (biglietti.ImportoFigurativo > 0 ? biglietti.ImportoFigurativo / 100 : 0) / biglietti.Quantita : 0);
                IVALorda = 0;
                ImponibileImpostaIntrattenimenti = 0;
                ImpostaIntrattenimenti = 0;
                ImponibileIVA = 0;
            }
        }

        private static void GetValoriBiglietto(Evento evento, OrdineDiPosto ordineDiPosto, TitoliAccesso biglietti, bool isAnnullati,
                                               out long Quantita,
                                               out decimal IncassoLordo, 
                                               out decimal IncassoPrevendita,
                                               out decimal IVALorda,
                                               out decimal PrezzoUnitario,
                                               out decimal ImponibileImpostaIntrattenimenti,
                                               out decimal ImpostaIntrattenimenti,
                                               out decimal ImponibileIVA,
                                               out decimal ImportoPrestazione,
                                               out long NumeroTitoliAnnullati)
        {
            TitoliAccesso annullati = null;
            Quantita = 0;
            IncassoLordo = 0;
            IncassoPrevendita = 0;
            IVALorda = 0;
            PrezzoUnitario = 0;
            ImponibileImpostaIntrattenimenti = 0;
            ImpostaIntrattenimenti = 0;
            ImponibileIVA = 0;
            ImportoPrestazione = 0;
            NumeroTitoliAnnullati = 0;
            if (!isAnnullati)
            {
                Quantita = biglietti.Quantita;
                IncassoLordo = (biglietti.CorrispettivoLordo > 0 ? biglietti.CorrispettivoLordo / 100 : 0);
                PrezzoUnitario = (IncassoLordo > 0 && biglietti.Quantita > 0 ? IncassoLordo / biglietti.Quantita : 0);

                IncassoPrevendita = (biglietti.Prevendita > 0 ? biglietti.Prevendita / 100 : 0); ;
                IVALorda = (biglietti.IVACorrispettivo > 0 ? biglietti.IVACorrispettivo / 100 : 0) +
                           (biglietti.IVAPrevendita > 0 ? biglietti.IVAPrevendita / 100 : 0);
                
                ImportoPrestazione = (biglietti.ImportoPrestazione > 0 ? biglietti.ImportoPrestazione / 100 : 0);

                

                annullati = ordineDiPosto.TitoliAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                    IncassoLordo = System.Math.Max(IncassoLordo - (annullati.CorrispettivoLordo > 0 ? annullati.CorrispettivoLordo / 100 : 0), 0);
                    IncassoPrevendita = System.Math.Max(IncassoPrevendita - (annullati.Prevendita > 0 ? annullati.Prevendita / 100 : 0), 0);
                    IVALorda = System.Math.Max(IVALorda - (annullati.IVACorrispettivo > 0 ? annullati.IVACorrispettivo / 100 : 0)
                                                        - (annullati.IVAPrevendita > 0 ? annullati.IVAPrevendita / 100 : 0), 0);
                    if (PrezzoUnitario == 0 && annullati.CorrispettivoLordo > 0)
                    {
                        PrezzoUnitario = (annullati.CorrispettivoLordo / 100) / annullati.Quantita;
                    }
                }

                if (Quantita > 0)
                {
                    ImponibileImpostaIntrattenimenti = 0;
                    ImpostaIntrattenimenti = 0;
                    ImponibileIVA = 0;
                    if (PrezzoUnitario > 0)
                        CalcImposte(evento, PrezzoUnitario, Quantita, out ImponibileImpostaIntrattenimenti, out ImpostaIntrattenimenti, out ImponibileIVA);
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
                IncassoLordo = 0;
                PrezzoUnitario = ((biglietti.CorrispettivoLordo > 0 ? biglietti.CorrispettivoLordo / 100 : 0) > 0 && biglietti.Quantita > 0 ? (biglietti.CorrispettivoLordo > 0 ? biglietti.CorrispettivoLordo / 100 : 0) / biglietti.Quantita : 0);
                IncassoPrevendita = 0;
                IVALorda = 0;
                ImportoPrestazione = 0;
                ImponibileImpostaIntrattenimenti = 0;
                ImpostaIntrattenimenti = 0;
                ImponibileIVA = 0;
            }
            
        }

        private static void CalcImposte(Evento evento, decimal PrezzoUnitario, long Qta, out decimal nOUT_ImponibileImpostaIntrattenimenti, out decimal nOUT_ImpostaIntrattenimenti, out decimal nOUT_ImponibileIVA)
        {
            nOUT_ImponibileImpostaIntrattenimenti = 0;
            nOUT_ImpostaIntrattenimenti = 0;
            nOUT_ImponibileIVA = 0; 
            decimal percIVA = 0;
            decimal percISI = 0;
            decimal percOmaEcc = 0;
            GetImposte(evento, out percIVA, out percISI, out percOmaEcc);

            decimal prezzoLordo = PrezzoUnitario;
            decimal valIVA = 0;
            decimal valISI = 0;
            decimal imponibileIntra = 0;
            decimal imponibileIVA = 0;
            if (percISI == 0 || evento.Intrattenimento.Incidenza < 51)
            {
                if (percIVA > 0)
                {
                    imponibileIVA = prezzoLordo / (1 + (percIVA / 100));
                    valIVA = System.Math.Round(imponibileIVA / 100 * percIVA, 2);
                    if (imponibileIVA + valIVA < prezzoLordo) 
                        valIVA = valIVA + (prezzoLordo - (imponibileIVA + valIVA));
                    imponibileIVA = System.Math.Round(prezzoLordo - valIVA, 2);
                    nOUT_ImponibileIVA = imponibileIVA * Qta;
                }
            }
            else
            {
                decimal percImposte = 0;
                decimal imponibile = 0;
                if (evento.Intrattenimento.Incidenza == 100)
                {
                    percImposte = percIVA + percISI;
                    if (percImposte > 0)
                    {
                        imponibile = System.Math.Round(prezzoLordo / (1 + (percImposte / 100)), 2);
                    }

                    if (percIVA > 0)
                        valIVA = System.Math.Round(imponibile / 100 * percIVA, 2);
                    imponibileIntra = imponibile;
                    valISI = System.Math.Round(imponibileIntra / 100 * percISI, 2);
                    if (imponibile + valIVA + valISI < prezzoLordo)
                    {
                        if (percIVA > 0)
                            valIVA = valIVA + (prezzoLordo - (imponibile + valIVA + valISI));
                        else if (percISI > 0)
                            valISI = valISI + (prezzoLordo - (imponibile + valIVA + valISI));
                    }
                    nOUT_ImponibileIVA = valIVA * Qta;
                    nOUT_ImponibileImpostaIntrattenimenti = imponibile * Qta;
                    nOUT_ImpostaIntrattenimenti = valISI * Qta;
                }
                else
                {
                    decimal lordoIntra = 0;
                    decimal lordoSpett = 0;
                    decimal valIVAintra = 0;
                    decimal valIVASpett = 0;
                    percImposte = percIVA + percISI;
                    if (percImposte > 0)
                    {
                        lordoIntra = System.Math.Round(prezzoLordo / 100 * evento.Intrattenimento.Incidenza, 2);
                        lordoSpett = prezzoLordo - lordoIntra;

                        if (lordoIntra > 0)
                        {
                            imponibileIntra = System.Math.Round(lordoIntra / (1 + (percImposte / 100)), 2);
                            if (percIVA > 0)
                                valIVAintra = System.Math.Round(imponibileIntra / 100 * percIVA, 2);
                        }

                        if (lordoSpett > 0)
                        {
                            if (percIVA > 0)
                            {
                                imponibile = System.Math.Round(lordoSpett / (1 + (percIVA / 100)), 2);
                                valIVASpett = System.Math.Round(imponibile / 100 * percIVA, 2);
                                if (imponibile + valIVASpett < lordoSpett)
                                    valIVASpett = valIVASpett + (lordoSpett - (imponibile + valIVASpett));
                            }
                        }

                        if (percIVA > 0)
                            valIVA = valIVAintra + valIVASpett;

                        if (imponibileIntra + imponibile + valIVA + valISI < prezzoLordo)
                        {
                            if (percIVA > 0)
                                valIVA = valIVA + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                            else if (percISI > 0)
                                valISI = valISI + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                        }

                        nOUT_ImponibileIVA = valIVA * Qta;
                        nOUT_ImponibileImpostaIntrattenimenti = imponibile * Qta;
                        nOUT_ImpostaIntrattenimenti = valISI * Qta;
                    }
                }
            }
        }

        private static void GetValoriBigliettoOmaggio(Evento evento, OrdineDiPosto ordineDiPosto, TitoliAccesso biglietti, bool isAnnullati, List<clsModuloC1row> omaggiRifSettori, clsModuloC1row omaggioRifEvento,
                                                      out long Quantita, out long QtaOmaggiEccedenti_IVA, out long QtaOmaggiEccedenti_ISI, 
                                                      out decimal IvaOmaggiEccedenti,
                                                      out decimal ImpostaIntrattenimentoOmaggiEccedenti, 
                                                      out long NumeroTitoliAnnullati)
        {
            TitoliAccesso annullati = null;
            Quantita = 0;
            QtaOmaggiEccedenti_IVA = 0;
            QtaOmaggiEccedenti_ISI = 0;
            IvaOmaggiEccedenti = 0;
            ImpostaIntrattenimentoOmaggiEccedenti = 0;
            NumeroTitoliAnnullati = 0;
            if (!isAnnullati)
            {
                Quantita = biglietti.Quantita;
                annullati = ordineDiPosto.TitoliAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                }

                if (Quantita > 0 && evento.IsMensile)
                {
                    decimal percIVA = 0;
                    decimal percISI = 0;
                    decimal percOmaggiEccedenti = 0;
                    GetImposte(evento, out percIVA, out percISI, out percOmaggiEccedenti);
                    long omaggiRiconosciuti = (long)(ordineDiPosto.Capienza == 0 ? 0 : System.Math.Round(ordineDiPosto.Capienza / 100 * percOmaggiEccedenti, 0));
                    long omaggiEccedenti = Quantita - omaggiRiconosciuti;
                    if (omaggiEccedenti > 0)
                    {
                        clsModuloC1row rifOmaggio = null;
                        if (evento.MultiGenere.TipoGenere == "60" || evento.MultiGenere.TipoGenere == "61")
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo.EndsWith(biglietti.TipoTitolo.Substring(1, 1))) : null);
                        if (rifOmaggio == null)
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine) : omaggioRifEvento);

                        if (rifOmaggio != null && rifOmaggio.PrezzoUnitario > 0)
                        {
                            decimal prezzoLordo = rifOmaggio.PrezzoUnitario;
                            decimal valIVA = 0;
                            decimal valISI = 0;
                            decimal imponibileIntra = 0;
                            decimal imponibileIVA = 0;
                            if (percISI == 0 || evento.Intrattenimento.Incidenza < 51)
                            {
                                QtaOmaggiEccedenti_IVA = omaggiEccedenti;
                                if (percIVA > 0)
                                {
                                    imponibileIVA = prezzoLordo / (1 + (percIVA / 100));
                                    valIVA = System.Math.Round(imponibileIVA / 100 * percIVA, 2);
                                    if (imponibileIVA + valIVA < prezzoLordo) valIVA = valIVA + (prezzoLordo - (imponibileIVA + valIVA));
                                    imponibileIVA = System.Math.Round(prezzoLordo - valIVA, 2);
                                }
                            }
                            else
                            {
                                decimal percImposte = 0;
                                decimal imponibile = 0;
                                if (evento.Intrattenimento.Incidenza == 100)
                                {
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        imponibile = System.Math.Round(prezzoLordo / (1 + (percImposte / 100)), 2);
                                    }

                                    if (percIVA > 0)
                                        valIVA = System.Math.Round(imponibile / 100 * percIVA, 2);
                                    imponibileIntra = imponibile;
                                    valISI = System.Math.Round(imponibileIntra / 100 * percISI, 2);
                                    if (imponibile + valIVA + valISI < prezzoLordo)
                                    {
                                        if (percIVA > 0)
                                            valIVA = valIVA + (prezzoLordo - (imponibile + valIVA + valISI));
                                        else if (percISI > 0)
                                            valISI = valISI + (prezzoLordo - (imponibile + valIVA + valISI));
                                    }
                                    if (prezzoLordo <= 50)
                                        valIVA = 0;
                                }
                                else
                                {
                                    decimal lordoIntra = 0;
                                    decimal lordoSpett = 0;
                                    decimal valIVAintra = 0;
                                    decimal valIVASpett = 0;
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        lordoIntra = System.Math.Round(prezzoLordo / 100 * evento.Intrattenimento.Incidenza, 2);
                                        lordoSpett = prezzoLordo - lordoIntra;

                                        if (lordoIntra > 0)
                                        {
                                            imponibileIntra = System.Math.Round(lordoIntra / (1 + (percImposte / 100)), 2);
                                            if (percIVA > 0)
                                                valIVAintra = System.Math.Round(imponibileIntra / 100 * percIVA, 2);
                                        }

                                        if (lordoSpett > 0)
                                        {
                                            if (percIVA > 0)
                                            {
                                                imponibile = System.Math.Round(lordoSpett / (1 + (percIVA / 100)), 2);
                                                valIVASpett = System.Math.Round(imponibile / 100 * percIVA, 2);
                                                if (imponibile + valIVASpett < lordoSpett)
                                                    valIVASpett = valIVASpett + (lordoSpett - (imponibile + valIVASpett));
                                            }
                                        }

                                        if (percIVA > 0)
                                            valIVA = valIVAintra + valIVASpett;

                                        if (imponibileIntra + imponibile + valIVA + valISI < prezzoLordo)
                                        {
                                            if (percIVA > 0)
                                                valIVA = valIVA + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                            else if (percISI > 0)
                                                valISI = valISI + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                        }

                                        if (prezzoLordo < 50)
                                            valIVA = 0;
                                    }
                                }
                            }
                            IvaOmaggiEccedenti = valIVA * QtaOmaggiEccedenti_IVA;
                            ImpostaIntrattenimentoOmaggiEccedenti = valISI * QtaOmaggiEccedenti_ISI;
                        }
                    }
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
            }
        }

        private static void GetValoriBigliettoOmaggio(Evento evento, OrdineDiPosto ordineDiPosto, TitoliAccessoIVAPreassolta biglietti, bool isAnnullati, List<clsModuloC1row> omaggiRifSettori, clsModuloC1row omaggioRifEvento,
                                                      out long Quantita, out long QtaOmaggiEccedenti_IVA, out long QtaOmaggiEccedenti_ISI,
                                                      out decimal IvaOmaggiEccedenti,
                                                      out decimal ImpostaIntrattenimentoOmaggiEccedenti,
                                                      out long NumeroTitoliAnnullati)
        {
            TitoliAccessoIVAPreassolta annullati = null;
            Quantita = 0;
            QtaOmaggiEccedenti_IVA = 0;
            QtaOmaggiEccedenti_ISI = 0;
            IvaOmaggiEccedenti = 0;
            ImpostaIntrattenimentoOmaggiEccedenti = 0;
            NumeroTitoliAnnullati = 0;
            if (!isAnnullati && evento.IsMensile)
            {
                Quantita = biglietti.Quantita;
                annullati = ordineDiPosto.TitoliIVAPreassoltaAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                }

                if (Quantita > 0)
                {
                    decimal percIVA = 0;
                    decimal percISI = 0;
                    decimal percOmaggiEccedenti = 0;
                    GetImposte(evento, out percIVA, out percISI, out percOmaggiEccedenti);
                    long omaggiRiconosciuti = (long)(ordineDiPosto.Capienza == 0 ? 0 : System.Math.Round(ordineDiPosto.Capienza / 100 * percOmaggiEccedenti, 0));
                    long omaggiEccedenti = Quantita - omaggiRiconosciuti;
                    if (omaggiEccedenti > 0)
                    {
                        clsModuloC1row rifOmaggio = null;
                        if (evento.MultiGenere.TipoGenere == "60" || evento.MultiGenere.TipoGenere == "61")
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo.EndsWith(biglietti.TipoTitolo.Substring(1, 1))) : null);
                        if (rifOmaggio == null)
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine) : omaggioRifEvento);

                        if (rifOmaggio != null && rifOmaggio.PrezzoUnitario > 0)
                        {
                            decimal prezzoLordo = rifOmaggio.PrezzoUnitario;
                            decimal valIVA = 0;
                            decimal valISI = 0;
                            decimal imponibileIntra = 0;
                            decimal imponibileIVA = 0;
                            if (percISI == 0 || evento.Intrattenimento.Incidenza < 51)
                            {
                                QtaOmaggiEccedenti_IVA = omaggiEccedenti;
                                if (percIVA > 0)
                                {
                                    imponibileIVA = prezzoLordo / (1 + (percIVA / 100));
                                    valIVA = System.Math.Round(imponibileIVA / 100 * percIVA, 2);
                                    if (imponibileIVA + valIVA < prezzoLordo) valIVA = valIVA + (prezzoLordo - (imponibileIVA + valIVA));
                                    imponibileIVA = System.Math.Round(prezzoLordo - valIVA, 2);
                                }
                            }
                            else
                            {
                                decimal percImposte = 0;
                                decimal imponibile = 0;
                                if (evento.Intrattenimento.Incidenza == 100)
                                {
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        imponibile = System.Math.Round(prezzoLordo / (1 + (percImposte / 100)), 2);
                                    }

                                    if (percIVA > 0)
                                        valIVA = System.Math.Round(imponibile / 100 * percIVA, 2);
                                    imponibileIntra = imponibile;
                                    valISI = System.Math.Round(imponibileIntra / 100 * percISI, 2);
                                    if (imponibile + valIVA + valISI < prezzoLordo)
                                    {
                                        if (percIVA > 0)
                                            valIVA = valIVA + (prezzoLordo - (imponibile + valIVA + valISI));
                                        else if (percISI > 0)
                                            valISI = valISI + (prezzoLordo - (imponibile + valIVA + valISI));
                                    }
                                    if (prezzoLordo <= 50)
                                        valIVA = 0;
                                }
                                else
                                {
                                    decimal lordoIntra = 0;
                                    decimal lordoSpett = 0;
                                    decimal valIVAintra = 0;
                                    decimal valIVASpett = 0;
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        lordoIntra = System.Math.Round(prezzoLordo / 100 * evento.Intrattenimento.Incidenza, 2);
                                        lordoSpett = prezzoLordo - lordoIntra;

                                        if (lordoIntra > 0)
                                        {
                                            imponibileIntra = System.Math.Round(lordoIntra / (1 + (percImposte / 100)), 2);
                                            if (percIVA > 0)
                                                valIVAintra = System.Math.Round(imponibileIntra / 100 * percIVA, 2);
                                        }

                                        if (lordoSpett > 0)
                                        {
                                            if (percIVA > 0)
                                            {
                                                imponibile = System.Math.Round(lordoSpett / (1 + (percIVA / 100)), 2);
                                                valIVASpett = System.Math.Round(imponibile / 100 * percIVA, 2);
                                                if (imponibile + valIVASpett < lordoSpett)
                                                    valIVASpett = valIVASpett + (lordoSpett - (imponibile + valIVASpett));
                                            }
                                        }

                                        if (percIVA > 0)
                                            valIVA = valIVAintra + valIVASpett;

                                        if (imponibileIntra + imponibile + valIVA + valISI < prezzoLordo)
                                        {
                                            if (percIVA > 0)
                                                valIVA = valIVA + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                            else if (percISI > 0)
                                                valISI = valISI + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                        }

                                        if (prezzoLordo < 50)
                                            valIVA = 0;
                                    }
                                }
                            }
                            IvaOmaggiEccedenti = valIVA * QtaOmaggiEccedenti_IVA;
                            ImpostaIntrattenimentoOmaggiEccedenti = valISI * QtaOmaggiEccedenti_ISI;
                        }
                    }
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
            }
        }

        private static void GetValoriBigliettoOmaggio(Evento evento, OrdineDiPosto ordineDiPosto, BigliettiAbbonamento biglietti, bool isAnnullati, List<clsModuloC1row> omaggiRifSettori, clsModuloC1row omaggioRifEvento,
                                                      out long Quantita, out long QtaOmaggiEccedenti_IVA, out long QtaOmaggiEccedenti_ISI,
                                                      out decimal IvaOmaggiEccedenti,
                                                      out decimal ImpostaIntrattenimentoOmaggiEccedenti,
                                                      out long NumeroTitoliAnnullati)
        {
            BigliettiAbbonamento annullati = null;
            Quantita = 0;
            QtaOmaggiEccedenti_IVA = 0;
            QtaOmaggiEccedenti_ISI = 0;
            IvaOmaggiEccedenti = 0;
            ImpostaIntrattenimentoOmaggiEccedenti = 0;
            NumeroTitoliAnnullati = 0;
            if (!isAnnullati && evento.IsMensile)
            {
                Quantita = biglietti.Quantita;
                annullati = ordineDiPosto.BigliettiAbbonamentoAnnullati.FirstOrDefault(a => a.TipoTitolo == biglietti.TipoTitolo);

                if (annullati != null)
                {
                    NumeroTitoliAnnullati = annullati.Quantita;
                    Quantita = System.Math.Max(Quantita - NumeroTitoliAnnullati, 0);
                }

                if (Quantita > 0)
                {
                    decimal percIVA = 0;
                    decimal percISI = 0;
                    decimal percOmaggiEccedenti = 0;
                    GetImposte(evento, out percIVA, out percISI, out percOmaggiEccedenti);
                    long omaggiRiconosciuti = (long)(ordineDiPosto.Capienza == 0 ? 0 : System.Math.Round(ordineDiPosto.Capienza / 100 * percOmaggiEccedenti, 0));
                    long omaggiEccedenti = Quantita - omaggiRiconosciuti;
                    if (omaggiEccedenti > 0)
                    {
                        clsModuloC1row rifOmaggio = null;
                        if (evento.MultiGenere.TipoGenere == "60" || evento.MultiGenere.TipoGenere == "61")
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine && r.TipoTitolo.EndsWith(biglietti.TipoTitolo.Substring(1, 1))) : null);
                        if (rifOmaggio == null)
                            rifOmaggio = (omaggiRifSettori.Count > 0 ? omaggiRifSettori.FirstOrDefault(r => r.Settore == ordineDiPosto.CodiceOrdine) : omaggioRifEvento);

                        if (rifOmaggio != null && rifOmaggio.PrezzoUnitario > 0)
                        {
                            decimal prezzoLordo = rifOmaggio.PrezzoUnitario;
                            decimal valIVA = 0;
                            decimal valISI = 0;
                            decimal imponibileIntra = 0;
                            decimal imponibileIVA = 0;
                            if (percISI == 0 || evento.Intrattenimento.Incidenza < 51)
                            {
                                QtaOmaggiEccedenti_IVA = omaggiEccedenti;
                                if (percIVA > 0)
                                {
                                    imponibileIVA = prezzoLordo / (1 + (percIVA / 100));
                                    valIVA = System.Math.Round(imponibileIVA / 100 * percIVA, 2);
                                    if (imponibileIVA + valIVA < prezzoLordo) valIVA = valIVA + (prezzoLordo - (imponibileIVA + valIVA));
                                    imponibileIVA = System.Math.Round(prezzoLordo - valIVA, 2);
                                }
                            }
                            else
                            {
                                decimal percImposte = 0;
                                decimal imponibile = 0;
                                if (evento.Intrattenimento.Incidenza == 100)
                                {
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        imponibile = System.Math.Round(prezzoLordo / (1 + (percImposte / 100)), 2);
                                    }

                                    if (percIVA > 0)
                                        valIVA = System.Math.Round(imponibile / 100 * percIVA, 2);
                                    imponibileIntra = imponibile;
                                    valISI = System.Math.Round(imponibileIntra / 100 * percISI, 2);
                                    if (imponibile + valIVA + valISI < prezzoLordo)
                                    {
                                        if (percIVA > 0)
                                            valIVA = valIVA + (prezzoLordo - (imponibile + valIVA + valISI));
                                        else if (percISI > 0)
                                            valISI = valISI + (prezzoLordo - (imponibile + valIVA + valISI));
                                    }
                                    if (prezzoLordo <= 50)
                                        valIVA = 0;
                                }
                                else
                                {
                                    decimal lordoIntra = 0;
                                    decimal lordoSpett = 0;
                                    decimal valIVAintra = 0;
                                    decimal valIVASpett = 0;
                                    percImposte = percIVA + percISI;
                                    if (percImposte > 0)
                                    {
                                        lordoIntra = System.Math.Round(prezzoLordo / 100 * evento.Intrattenimento.Incidenza, 2);
                                        lordoSpett = prezzoLordo - lordoIntra;

                                        if (lordoIntra > 0)
                                        {
                                            imponibileIntra = System.Math.Round(lordoIntra / (1 + (percImposte / 100)), 2);
                                            if (percIVA > 0)
                                                valIVAintra = System.Math.Round(imponibileIntra / 100 * percIVA, 2);
                                        }

                                        if (lordoSpett > 0)
                                        {
                                            if (percIVA > 0)
                                            {
                                                imponibile = System.Math.Round(lordoSpett / (1 + (percIVA / 100)), 2);
                                                valIVASpett = System.Math.Round(imponibile / 100 * percIVA, 2);
                                                if (imponibile + valIVASpett < lordoSpett)
                                                    valIVASpett = valIVASpett + (lordoSpett - (imponibile + valIVASpett));
                                            }
                                        }

                                        if (percIVA > 0)
                                            valIVA = valIVAintra + valIVASpett;

                                        if (imponibileIntra + imponibile + valIVA + valISI < prezzoLordo)
                                        {
                                            if (percIVA > 0)
                                                valIVA = valIVA + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                            else if (percISI > 0)
                                                valISI = valISI + (prezzoLordo - (imponibileIntra + imponibile + valIVA + valISI));
                                        }

                                        if (prezzoLordo < 50)
                                            valIVA = 0;
                                    }
                                }
                            }
                            IvaOmaggiEccedenti = valIVA * QtaOmaggiEccedenti_IVA;
                            ImpostaIntrattenimentoOmaggiEccedenti = valISI * QtaOmaggiEccedenti_ISI;
                        }
                    }
                }
            }
            else
            {
                NumeroTitoliAnnullati = biglietti.Quantita;
                Quantita = 0;
            }
        }

        private static void GetImposte(Evento evento, out decimal percIVA, out decimal percISI, out decimal percOmaggiEccedenti)
        {
            percIVA = 0;
            percISI = 0;
            TipoEventoDescriptor tipoEvento = TipoEventoDescriptor.GetTipoEvento(evento.MultiGenere.TipoGenere);
            if (tipoEvento == null)
                tipoEvento = new TipoEventoDescriptor() { Codice = "01", Descrizione = @"CINEMA", PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5 };
            percIVA = tipoEvento.PercIVA;
            percISI = tipoEvento.PercISI;
            percOmaggiEccedenti = tipoEvento.PercOmaggiEccedenti;
        }

        public static List<string> GetTipiTitoloOmaggio()
        {
            List<string> tipiTitoloOmaggio = new List<string>() {
            "CO",
            "OA",
            "OB",
            "OC",
            "OD",
            "OE",
            "OF",
            "OG",
            "OH",
            "OI",
            "OJ",
            "OK",
            "OL",
            "OM",
            "ON",
            "OO",
            "OP",
            "OQ",
            "OR",
            "OS",
            "OT",
            "OU",
            "OV",
            "OW",
            "OX",
            "OY",
            "OZ",
            "O1",
            "O2",
            "O3",
            "O4",
            "O5",
            "O6",
            "O7",
            "O8",
            "O9",
            "PO",
            "WA",
            "WB",
            "WC",
            "WD",
            "WE",
            "WF",
            "WG",
            "WH",
            "WI",
            "WJ",
            "WK",
            "WL",
            "WM",
            "WN",
            "WO",
            "WP",
            "WQ",
            "WR",
            "WS",
            "WT",
            "WU",
            "WV",
            "WW",
            "WX",
            "WY",
            "WZ",
            "W1",
            "W2",
            "W3",
            "W4",
            "W5",
            "W6",
            "W7",
            "W8",
            "W9",
            "YA",
            "YB",
            "YC",
            "YD",
            "YE",
            "YF",
            "YG",
            "YH",
            "YI",
            "YJ",
            "YK",
            "YL",
            "YM",
            "YN",
            "YO",
            "YP",
            "YQ",
            "YR",
            "YS",
            "YT",
            "YU",
            "YV",
            "YW",
            "YX",
            "YY",
            "YZ",
            "Y1",
            "Y2",
            "Y3",
            "Y4",
            "Y5",
            "Y6",
            "Y7",
            "Y8",
            "Y9",
            "ZA",
            "ZB",
            "ZC",
            "ZD",
            "ZE",
            "ZF",
            "ZG",
            "ZH",
            "ZI",
            "ZJ",
            "ZK",
            "ZL",
            "ZM",
            "ZN",
            "ZO",
            "ZP",
            "ZQ",
            "ZR",
            "ZS",
            "ZT",
            "ZU",
            "ZV",
            "ZW",
            "ZX",
            "ZY",
            "ZZ",
            "Z1",
            "Z2",
            "Z3",
            "Z4",
            "Z5",
            "Z6",
            "Z7",
            "Z8",
            "Z9"};
            return tipiTitoloOmaggio;
        }

        private static void CopyXmlToObject(Type typeObject, object objectValue, XmlNode node)
        {
            if (objectValue != null && objectValue.ToString().EndsWith(node.Name + "]") && node.ChildNodes != null && node.ChildNodes.Count > 0)
            {
                string f = "";
            }
            else
            {
                foreach (XmlNode nNode in node.ChildNodes)
                {
                    foreach (PropertyInfo pInfo in typeObject.GetProperties())
                    {
                        if (!pInfo.CanWrite)
                        {
                            continue;
                        }
                        CustomAttributeData customAttr = pInfo.CustomAttributes.FirstOrDefault();
                        if (customAttr != null)
                        {
                            if (customAttr.NamedArguments != null && customAttr.NamedArguments.Count > 0 && customAttr.NamedArguments[0].TypedValue.Value.ToString() != "")
                            {
                                string name = customAttr.NamedArguments[0].TypedValue.Value.ToString();
                                if (name == nNode.Name)
                                {
                                    if (pInfo.PropertyType.GetTypeInfo().Name.ToUpper() == "STRING")
                                    {
                                        if (nNode.InnerText != "")
                                            pInfo.SetValue(objectValue, nNode.InnerText);
                                        else if (nNode.Attributes != null && nNode.Attributes.Count > 0)
                                            pInfo.SetValue(objectValue, nNode.Attributes[0].Value.ToString());
                                    }
                                    else if (pInfo.PropertyType.GetTypeInfo().Name.ToUpper() == "INT16")
                                    {
                                        Int16 intValue = 0;
                                        if (nNode.InnerText != "")
                                            Int16.TryParse(nNode.InnerText, out intValue);
                                        else if (nNode.Attributes != null && nNode.Attributes.Count > 0)
                                            Int16.TryParse(nNode.Attributes[0].Value.ToString(), out intValue);
                                        pInfo.SetValue(objectValue, intValue);
                                    }
                                    else if (pInfo.PropertyType.GetTypeInfo().Name.ToUpper() == "INT32")
                                    {
                                        Int32 intValue = 0;
                                        if (nNode.InnerText != "")
                                            Int32.TryParse(nNode.InnerText, out intValue);
                                        else if (nNode.Attributes != null && nNode.Attributes.Count > 0)
                                            Int32.TryParse(nNode.Attributes[0].Value.ToString(), out intValue);
                                        pInfo.SetValue(objectValue, intValue);
                                    }
                                    else if (pInfo.PropertyType.GetTypeInfo().Name.ToUpper() == "DATETIME")
                                    {
                                        DateTime? dateValue = null;
                                        if (nNode.InnerText.Length == 8)
                                        {
                                            dateValue = new DateTime(int.Parse(nNode.InnerText.Substring(0, 4)), int.Parse(nNode.InnerText.Substring(4, 2)), int.Parse(nNode.InnerText.Substring(6, 2)));
                                        }
                                        else if (nNode.Attributes != null && nNode.Attributes.Count > 0 && nNode.Attributes[0].Value.ToString().Length == 8)
                                            dateValue = new DateTime(int.Parse(nNode.Attributes[0].Value.ToString().Substring(0, 4)), int.Parse(nNode.Attributes[0].Value.ToString().Substring(4, 2)), int.Parse(nNode.Attributes[0].Value.ToString().Substring(6, 2)));
                                        pInfo.SetValue(objectValue, dateValue);
                                    }
                                    else
                                    {
                                        if (nNode.ChildNodes != null && nNode.ChildNodes.Count > 0)
                                        {
                                            CopyXmlToObject(pInfo.PropertyType, pInfo.GetValue(objectValue), nNode);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class RiepilogoRPGRPM
    {
        public string Sostituzione { get; set; }
        public DateTime Data { get; set;}
        public DateTime Mese { get; set; }

        public DateTime DataGenerazione { get; set; }
        public string OraGenerazione { get; set; }
        public int ProgressivoGenerazione { get; set; }

        public string Tipo { get; set; }

        public string NomeFileRiepilogo { get; set; }

        public DateTime DataOraGenerazione
        {
            get
            {
                DateTime result = this.DataGenerazione;
                if (!string.IsNullOrEmpty(this.OraGenerazione))
                {
                    result = (OraGenerazione.Length >= 4 ? this.DataGenerazione.AddHours(int.Parse(OraGenerazione.Substring(0, 2))).AddMinutes(int.Parse(OraGenerazione.Substring(2, 2))) : this.DataGenerazione);
                    if (OraGenerazione.Length == 6)
                        result = result.AddSeconds(int.Parse(OraGenerazione.Substring(4, 2)));
                }
                return result;
            }
        }
        /*
         * 
         * 
         * case "Sostituzione": { Sostituzione = attr.Value; break; }
                                    case "Data": { Data = attr.Value; break; }
                                    case "Mese": { Mese = attr.Value; break; }
                                    case "DataGenerazione": { DataGenerazione = attr.Value; break; }
                                    case "OraGenerazione": { OraGenerazione = attr.Value; break; }
                                    case "ProgressivoGenerazione": { ProgressivoGenerazione = attr.Value; break; }*/
        public Titolare Titolare { get; set; }

        public RiepilogoRPGRPM(string nomeFileRiepilogo)
        {
            this.NomeFileRiepilogo = nomeFileRiepilogo;
            this.Titolare = new Titolare();
        }
    }

    public class Titolare
    {

        [XmlElement(ElementName = "Denominazione")]
        public string Denominazione { get; set; }

        [XmlElement(ElementName = "CodiceFiscale")]
        public string CodiceFiscale { get; set; }

        [XmlElement(ElementName = "SistemaEmissione")]
        public string SistemaEmissione { get; set; }

        public List<Organizzatore> Organizzatore { get; set; }
    }

    public class Organizzatore
    {
        // using System.Xml.Serialization;
        // XmlSerializer serializer = new XmlSerializer(typeof(Organizzatore));
        // using (StringReader reader = new StringReader(xml))
        // {
        //    var test = (Organizzatore)serializer.Deserialize(reader);
        // }

        [XmlElement(ElementName = "Denominazione")]
        public string Denominazione { get; set; }

        [XmlElement(ElementName = "CodiceFiscale")]
        public string CodiceFiscale { get; set; }

        [XmlElement(ElementName = "TipoOrganizzatore")]
        public string TipoOrganizzatore { get; set; }

        public XmlNode NodoDatiOrganizzatore { get; set; }

        public List<Evento> Evento { get; set; }


    }

    public class Intrattenimento
    {

        [XmlElement(ElementName = "TipoTassazione")]
        public string TipoTassazione { get; set; }

        [XmlElement(ElementName = "Incidenza")]

        public int Incidenza { get; set; }

        public Intrattenimento()
        {
        }
    }

    
    public class Locale
    {

        [XmlElement(ElementName = "Denominazione")]
        public string Denominazione { get; set; }

        [XmlElement(ElementName = "CodiceLocale")]
        public string CodiceLocale { get; set; }
    }

    public class TitoliOpere
    {

        [XmlElement(ElementName = "Titolo")]
        public string Titolo { get; set; }

        [XmlElement(ElementName = "ProduttoreCinema")]
        public string ProduttoreCinema { get; set; }

        [XmlElement(ElementName = "Autore")]
        public string Autore { get; set; }

        [XmlElement(ElementName = "Esecutore")]
        public string Esecutore { get; set; }

        [XmlElement(ElementName = "Nazionalita")]
        public string Nazionalita { get; set; }

        [XmlElement(ElementName = "Distributore")]
        public string Distributore { get; set; }
    }

    
    public class MultiGenere
    {

        [XmlElement(ElementName = "TipoGenere")]
        public string TipoGenere { get; set; }

        [XmlElement(ElementName = "IncidenzaGenere")]
        public int IncidenzaGenere { get; set; }

        [XmlElement(ElementName = "TitoliOpere")]
        public List<TitoliOpere> TitoliOpere { get; set; }

        public MultiGenere()
        {
            this.TitoliOpere = new List<TitoliOpere>();
        }
    }

    public class TipoEventoDescriptor
    {
        public string Codice { get; set; }
        public string Descrizione { get; set; }

        public decimal PercIVA { get; set; }
        public decimal PercISI { get; set; }
        public decimal PercOmaggiEccedenti { get; set; }

        public static TipoEventoDescriptor GetTipoEvento(string codice)
        {
            return GetTipiEvento().FirstOrDefault(t => t.Codice == codice);
        }
        public static List<TipoEventoDescriptor> GetTipiEvento()
        {
            List<TipoEventoDescriptor> tipiEvento = new List<TipoEventoDescriptor>() {
            new TipoEventoDescriptor() { Codice = "01", Descrizione = @"CINEMA", PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5 },
            new TipoEventoDescriptor() { Codice = "04", Descrizione = @"PROIEZIONI IN LOCALI CINEMA DIVERSE DA SPETTACOLO", PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5 },
            new TipoEventoDescriptor() { Codice = "07", Descrizione = @"TELEDIFFUSIONE IN FORMA CODIFICATA NEI LOCALI APERTI AL PUBBLICO", PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5 },
            new TipoEventoDescriptor() { Codice = "08", Descrizione = @"DIFFUSIONE RADIO/TV CON ACCESSO CONDIZIONATO", PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5 },
            new TipoEventoDescriptor() { Codice = "42", Descrizione = @"EVENTI DIVERSI DA SPETTACOLO O INTRATTENIMENTO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "43", Descrizione = @"EVENTI ALL'ESTERO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "45", Descrizione = @"TEATRO PROSA" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "46", Descrizione = @"TEATRO PROSA DIALETTALE" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "47", Descrizione = @"TEATRO REPERTORIO NAPOLETANO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "48", Descrizione = @"TEATRO LIRICO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "49", Descrizione = @"BALLETTO CLASSICO E MODERNO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "50", Descrizione = @"OPERETTA" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "51", Descrizione = @"RIVISTE - COMMEDIE MUSICALE" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "52", Descrizione = @"CONCERTI CLASSICI" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "53", Descrizione = @"CONCERTI MUSICA LEGGERA" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "54", Descrizione = @"ARTE VARIA (IVA 10%)" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "55", Descrizione = @"BURATTINI - MARIONETTE" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "56", Descrizione = @"RECITALS LETTERARI" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "57", Descrizione = @"CONCERTI BANDISTICI-CORALI" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "58", Descrizione = @"CONCERTI JAZZ" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "59", Descrizione = @"CONCERTI DI DANZA" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "72", Descrizione = @"EVENTI DI RILIEVO INTERNAZIONALE CON ALIQUOTA AGEVOLATA" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "75", Descrizione = @"CIRCO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "76", Descrizione = @"SPETTACOLI VIAGGIANTI" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "77", Descrizione = @"PARCHI DIVERTIMENTO E ACQUATICI (CON PREVALENZA ATTIVITA' DELLO SPETTACOLO VIAGGIANTE)" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "90", Descrizione = @"MANIFESTAZIONI MISTE (ALL'APERTO)" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "97", Descrizione = @"ALTRE ATTIVITA' DI SPETTACOLO CONGIUNTE CON ALTRE NON DI SPETTACOLO" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "98", Descrizione = @"ALTRI SPETTACOLI O INTRATTENIMENTI (IN ALBERGHI E VILLAGGI TURISTICI)" , PercISI = 0, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "30", Descrizione = @"CASINO' (INGRESSI)", PercISI = 60, PercIVA = 22, PercOmaggiEccedenti = 5 },
            new TipoEventoDescriptor() { Codice = "60", Descrizione = @"BALLO CON MUSICA DAL VIVO" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "61", Descrizione = @"BALLO CON MUSICA PREREGISTRATA" , PercISI = 16, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "64", Descrizione = @"CONCERTINI CON MUSICA PREREGISTRATA" , PercISI = 16, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "65", Descrizione = @"CONCERTINI CON MUSICA DAL VIVO" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "68", Descrizione = @"CONCERTI FOLKLORISTICI" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "70", Descrizione = @"FIERE" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "71", Descrizione = @"MOSTRE" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "74", Descrizione = @"ARTE VARIA (IVA 22%)" , PercISI = 0, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "84", Descrizione = @"BOWLING" , PercISI = 8, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "85", Descrizione = @"NOLEGGIO GO-KARTS" , PercISI = 8, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "91", Descrizione = @"MULTIMEDIALITA'" , PercISI = 8, PercIVA = 22, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "33", Descrizione = @"CASINO' (PROVENTI DEL GIOCO)" , PercISI = 10, PercIVA = 0, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "78", Descrizione = @"PARCHI DA DIVERTIMENTO E ACQUATICI (SENZA PREVALENZA ATTIVITA' DELLO SPETTACOLO VIAGGIANTE)" , PercISI = 8, PercIVA = 10, PercOmaggiEccedenti = 5},
            new TipoEventoDescriptor() { Codice = "41", Descrizione = @"MUSEI" , PercISI = 0, PercIVA = 0, PercOmaggiEccedenti = 5}
            };
            return tipiEvento;
        }
    }
    
    public class TitoliAccesso
    {

        [XmlElement(ElementName = "TipoTitolo")]
        public string TipoTitolo { get; set; }

        [XmlElement(ElementName = "Quantita")]
        public int Quantita { get; set; }

        [XmlElement(ElementName = "CorrispettivoLordo")]
        public int CorrispettivoLordo { get; set; }

        [XmlElement(ElementName = "Prevendita")]
        public int Prevendita { get; set; }

        [XmlElement(ElementName = "IVACorrispettivo")]
        public int IVACorrispettivo { get; set; }

        [XmlElement(ElementName = "IVAPrevendita")]
        public int IVAPrevendita { get; set; }

        [XmlElement(ElementName = "ImportoPrestazione")]
        public int ImportoPrestazione { get; set; }
    }

    public class TitoliAccessoIVAPreassolta
    {

        [XmlElement(ElementName = "TipoTitolo")]
        public string TipoTitolo { get; set; }

        [XmlElement(ElementName = "Quantita")]
        public int Quantita { get; set; }

        [XmlElement(ElementName = "ImportoFigurativo")]
        public int ImportoFigurativo { get; set; }

        [XmlElement(ElementName = "IVAFigurativa")]
        public int IVAFigurativa { get; set; }
    }

    public class BigliettiAbbonamento : TitoliAccessoIVAPreassolta
    {

        [XmlElement(ElementName = "CodiceFiscale")]
        public string CodiceFiscale { get; set; }

        [XmlElement(ElementName = "CodiceAbbonamento")]
        public string CodiceAbbonamento { get; set; }
    }

    public class OrdineDiPosto
    {

        [XmlElement(ElementName = "CodiceOrdine")]
        public string CodiceOrdine { get; set; }

        [XmlElement(ElementName = "Capienza")]
        public int Capienza { get; set; }

        public List<TitoliAccesso> TitoliAccesso { get; set; }
        public List<TitoliAccesso> TitoliAnnullati { get; set; }
        public List<TitoliAccessoIVAPreassolta> TitoliAccessoIVAPreassolta { get; set; }
        public List<TitoliAccessoIVAPreassolta> TitoliIVAPreassoltaAnnullati { get; set; }
        public List<BigliettiAbbonamento> BigliettiAbbonamento { get; set; }
        public List<BigliettiAbbonamento> BigliettiAbbonamentoAnnullati { get; set; }

        public OrdineDiPosto()
        {
            this.TitoliAccesso = new List<TitoliAccesso>();
            this.TitoliAnnullati = new List<TitoliAccesso>();
            this.TitoliIVAPreassoltaAnnullati = new List<TitoliAccessoIVAPreassolta>();
            this.TitoliAccessoIVAPreassolta = new List<TitoliAccessoIVAPreassolta>();
            this.TitoliIVAPreassoltaAnnullati = new List<TitoliAccessoIVAPreassolta>();
            this.BigliettiAbbonamento = new List<BigliettiAbbonamento>();
            this.BigliettiAbbonamentoAnnullati = new List<BigliettiAbbonamento>();
        }

    }
    
    public class Evento
    {

        [XmlElement(ElementName = "Intrattenimento")]
        public Intrattenimento Intrattenimento { get; set; }

        [XmlElement(ElementName = "Locale")]
        public Locale Locale { get; set; }

        [XmlElement(ElementName = "DataEvento")]
        public DateTime DataEvento { get; set; }

        [XmlElement(ElementName = "OraEvento")]
        public string OraEvento { get; set; }

        [XmlElement(ElementName = "MultiGenere")]
        public MultiGenere MultiGenere { get; set; }

        [XmlElement(ElementName = "OrdineDiPosto")]
        public List<OrdineDiPosto> OrdineDiPosto { get; set; }

        public bool IsMensile { get; set; }

        public DateTime DataOraEvento => (OraEvento.Length == 4 ? this.DataEvento.AddHours(int.Parse(OraEvento.Substring(0, 2))).AddMinutes(int.Parse(OraEvento.Substring(2, 2))) : this.DataEvento);

        public Evento()
        {
            this.Intrattenimento = new Intrattenimento();
            this.Locale = new Locale();
            this.MultiGenere = new MultiGenere();
            this.OrdineDiPosto = new List<OrdineDiPosto>();
        }
    }

    public class clsModuloC1rowAltriProventi
    {
        public DateTime Giorno = DateTime.MinValue;
        public string TipoProvento = "";
        public string DescTipoProvento = "";
        public string TipoGenerePrevalente = "";
        public string DescTipoGenerePrevalente = "";
        public decimal Corrispettivo = 0;
        public decimal ImponibileIVA = 0;
        public decimal IVA = 0;
        public decimal ImponibileImposta = 0;
        public decimal ImpostaIntra = 0;

        public static System.Collections.SortedList ListCodiciTipoProvento = null;

        public clsModuloC1rowAltriProventi()
        {
        }

        public clsModuloC1rowAltriProventi(DateTime Giorno, string TipoProvento, string DescTipoProvento, string TipoGenerePrevalente, string DescTipoGenerePrevalente, decimal Corrispettivo, decimal ImponibileIVA, decimal IVA, decimal ImponibileImposta, decimal ImpostaIntra)
        {
            this.Giorno = Giorno;
            this.TipoProvento = TipoProvento;
            this.DescTipoProvento = DescTipoProvento;
            this.TipoGenerePrevalente = TipoGenerePrevalente;
            this.DescTipoGenerePrevalente = DescTipoGenerePrevalente;
            this.Corrispettivo = Corrispettivo;
            this.ImponibileIVA = ImponibileIVA;
            this.IVA = IVA;
            this.ImponibileImposta = ImponibileImposta;
            this.ImpostaIntra = ImpostaIntra;
        }

        
    }

    public class clsModuloC1row
    {
        public string TitoloIvaPreassolta = "";
        public string Settore = "";
        public Int64 Capiena = 0;
        public string TipoTitolo = "";
        public decimal PrezzoUnitario = 0;
        public Int64 NumeroTitoliEmessi = 0;
        public decimal IncassoLordo = 0;
        public decimal IncassoPrevendita = 0;
        public decimal ImponibileImpostaIntrattenimenti = 0;
        public decimal ImpostaIntrattenimenti = 0;
        public decimal InponibileIVA = 0;
        public decimal IVALorda = 0;
        public Int64 NumeroTitoliAnnullati = 0;

        //public Int64 QtaOmaggiEccedenti = 0;
        public Int64 QtaOmaggiEccedentiIVA = 0;
        public Int64 QtaOmaggiEccedentiISI = 0;
        public decimal IvaOmaggiEccedenti = 0;
        public decimal ImpostaIntrattenimentoOmaggiEccedenti = 0;
        public clsModuloC1row RifOmaggioEccedente = null;

        // Costruzione tipo biglietto normale
        public clsModuloC1row(string TitoloIvaPreassolta, string Settore, Int64 Capiena, string TipoTitolo, decimal PrezzoUnitario, Int64 NumeroTitoliEmessi,
                              decimal IncassoLordo, decimal IncassoPrevendita, decimal ImponibileImpostaIntrattenimenti,
                              decimal ImpostaIntrattenimenti, decimal InponibileIVA, decimal IVALorda, Int64 NumeroTitoliAnnullati)
        {
            this.TitoloIvaPreassolta = TitoloIvaPreassolta;
            this.Settore = Settore;
            this.Capiena = Capiena;
            this.TipoTitolo = TipoTitolo;
            this.PrezzoUnitario = PrezzoUnitario;
            this.NumeroTitoliEmessi = NumeroTitoliEmessi;
            this.IncassoLordo = IncassoLordo;
            this.IncassoPrevendita = IncassoPrevendita;
            this.ImponibileImpostaIntrattenimenti = ImponibileImpostaIntrattenimenti;
            this.ImpostaIntrattenimenti = ImpostaIntrattenimenti;
            this.InponibileIVA = InponibileIVA;
            this.IVALorda = IVALorda;
            this.NumeroTitoliAnnullati = NumeroTitoliAnnullati;
        }

        // Costruzione tipo biglietto di riferimento all'omaggio
        //public clsModuloC1row(string TitoloIvaPreassolta, string Settore, Int64 Capiena, string TipoTitolo, Int64 NumeroTitoliEmessi,
        //                      Int64 QtaOmaggiEccedenti, decimal IvaOmaggiEccedenti, decimal ImpostaIntrattenimentoOmaggiEccedenti,
        //                      clsModuloC1row RifOmaggioEccedente, Int64 NumeroTitoliAnnullati)
        public clsModuloC1row(string TitoloIvaPreassolta, string Settore, Int64 Capiena, string TipoTitolo, Int64 NumeroTitoliEmessi,
                              Int64 QtaOmaggiEccedenti_IVA, Int64 QtaOmaggiEccedenti_ISI, decimal IvaOmaggiEccedenti, decimal ImpostaIntrattenimentoOmaggiEccedenti,
                              clsModuloC1row RifOmaggioEccedente, Int64 NumeroTitoliAnnullati)
        {
            this.TitoloIvaPreassolta = TitoloIvaPreassolta;
            this.Settore = Settore;
            this.Capiena = Capiena;
            this.TipoTitolo = TipoTitolo;
            this.PrezzoUnitario = 0;
            this.NumeroTitoliEmessi = NumeroTitoliEmessi;
            this.ImpostaIntrattenimentoOmaggiEccedenti = ImpostaIntrattenimentoOmaggiEccedenti;
            this.IvaOmaggiEccedenti = IvaOmaggiEccedenti;
            this.ImpostaIntrattenimenti = ImpostaIntrattenimentoOmaggiEccedenti;
            this.IVALorda = IvaOmaggiEccedenti;
            this.NumeroTitoliAnnullati = NumeroTitoliAnnullati;
            this.RifOmaggioEccedente = RifOmaggioEccedente;
            //this.QtaOmaggiEccedenti = QtaOmaggiEccedenti;
            this.QtaOmaggiEccedentiIVA = QtaOmaggiEccedenti_IVA;
            this.QtaOmaggiEccedentiISI = QtaOmaggiEccedenti_ISI;
        }
    }

    public class clsModuloC2row
    {
        public string TitoloIvaPreassolta = "";
        public string TipoTitolo = "";
        public string DescTipoTitolo = "";
        public string CodiceAbbonamento = "";
        public string SpettacoloIntrattenimento = "";
        public string TipoTurno = "";
        public Int64 NumeroEventiAbilitati = 0;
        public Int64 QtaEmessi = 0;
        public Int64 QtaAnnullati = 0;

        public decimal IncassoLordo = 0;
        public decimal IVALorda = 0;
        public decimal Netto = 0;

        public clsModuloC2row(string TitoloIvaPreassolta, string TipoTitolo, string CodiceAbbonamento, string SpettacoloIntrattenimento, string TipoTurno,
                              Int64 NumeroEventiAbilitati, Int64 QtaEmessi, Int64 QtaAnnullati,
                              decimal IncassoLordo, decimal IVALorda, decimal Netto, string DescTipoTitolo)
        {
            this.TitoloIvaPreassolta = TitoloIvaPreassolta;
            this.TipoTitolo = TipoTitolo;
            this.CodiceAbbonamento = CodiceAbbonamento;
            this.SpettacoloIntrattenimento = SpettacoloIntrattenimento;
            this.TipoTurno = TipoTurno;
            this.NumeroEventiAbilitati = NumeroEventiAbilitati;
            this.QtaEmessi = QtaEmessi;
            this.QtaAnnullati = QtaAnnullati;
            this.IncassoLordo = IncassoLordo;
            this.IVALorda = IVALorda;
            this.Netto = Netto;
            this.DescTipoTitolo = DescTipoTitolo;
        }
    }

    // classe per modulo C1 titoli di accesso
    public class clsModuloC1page
    {
        public System.Collections.SortedList RowsIvaDaAssolve = new System.Collections.SortedList();
        public System.Collections.SortedList RowsIvaPreAssolta = new System.Collections.SortedList();
        private Int64 nAbsoluteStartPage = 0;
        public Int64 EventPage = 0;
        public clsModuloC1C2 RefModulo = null;

        public Int64 nCountRowsIvaDaAssolvere = 0;
        public Int64 nCountRowsIvaPreassolta = 0;


        public clsModuloC1page()
        {
        }

        public Int64 AbsoluteStartPage
        {
            get
            {
                return this.nAbsoluteStartPage;
            }
            set
            {
                this.nAbsoluteStartPage = value;
            }
        }
    }

    // classe per modulo C1 retro altri proventi evento
    public class clsModuloC1AltriProventipage
    {
        public System.Collections.SortedList RowsAltriProventi = new System.Collections.SortedList();
        private Int64 nAbsoluteStartPage = 0;
        public Int64 AltriProventiPage = 0;
        public clsModuloC1C2 RefModulo = null;

        public clsModuloC1AltriProventipage()
        {
        }

        public Int64 AbsoluteStartPage
        {
            get
            {
                return this.nAbsoluteStartPage;
            }
            set
            {
                this.nAbsoluteStartPage = value;
            }
        }
    }

    // Classe per modulo C2
    public class clsModuloC2page
    {
        public System.Collections.SortedList RowsAbbonamenti = new System.Collections.SortedList();
        private Int64 nAbsoluteStartPage = 0;
        public Int64 AbbonPage = 0;
        public clsModuloC1C2 RefModulo = null;

        public clsModuloC2page()
        {
        }

        public Int64 AbsoluteStartPage
        {
            get
            {
                return this.nAbsoluteStartPage;
            }
            set
            {
                this.nAbsoluteStartPage = value;
            }
        }
    }
    public class clsModuloC1C2
    {
        public System.Collections.SortedList PagesBiglietti = new System.Collections.SortedList();
        public System.Collections.SortedList PagesAltriProventiEvento = new System.Collections.SortedList();
        public System.Collections.SortedList PagesAbbonamenti = new System.Collections.SortedList();
        private Int64 nAbsoluteStartPage = 0;

        private const int MaxRigheBiglietti = 11;
        private const int MaxRigheAltriProventi = 30;
        private const int MaxRigheAbbonamenti = 30;

        public DateTime DataIns = DateTime.MinValue;
        public string CodiceSistema = "";
        public string GiornalieroMensile = "";
        public string DenominazioneTitolare = "";
        public string CodiceFiscaleTitolare = "";
        public string DenominazioneOrganizzatore = "";
        public string CodiceFiscaleOrganizzatore = "";
        public string SpettacoloIntrattenimento = "";
        public string TipoEvento = "";
        public string DescTipoEvento = "";
        public decimal PercIVA = 0;
        public decimal PercISI = 0;
        public decimal IncidenzaIntrattenimento = 0;
        public string CodiceLocale = "";
        public string DenominazioneLocale = "";
        public string LuogoLocale = "";
        public string TitoloEvento = "";
        public string Esecutore = "";
        public string Nazionalita = "";
        public string Autore = "";
        public string ProduttoreCinema = "";
        public DateTime DataOraEvento = DateTime.MinValue;

        public DateTime Giorno { get; set; }
        public long Progressivo { get; set; }
        public string NomeFileRiepilogo { get; set; }


        public clsModuloC1C2(DateTime dDataIns, string cCodiceSistema, string cGiornalieroMensile, string cDenominazioneTitolare, string cCodiceFiscaleTitolare, string cDenominazioneOrganizzatore,
                             string cCodiceFiscaleOrganizzatore, string cSpettacoloIntrattenimento, string cTipoEvento, string cDescTipoEvento, Decimal nPercIVA, Decimal nPercISI, Decimal nIncidenzaIntrattenimento,
                             string cCodiceLocale, string cDenominazioneLocale, string cLuogoLocale, string cTitoloEvento, string cEsecutore, string cNazionalita,
                             string cAutore, string cProduttoreCinema, DateTime dDataOraEvento)
        {
            this.DataIns = dDataIns;
            this.CodiceSistema = cCodiceSistema;
            this.GiornalieroMensile = cGiornalieroMensile;
            this.DenominazioneTitolare = cDenominazioneTitolare;
            this.CodiceFiscaleTitolare = cCodiceFiscaleTitolare;
            this.DenominazioneOrganizzatore = cDenominazioneOrganizzatore;
            this.CodiceFiscaleOrganizzatore = cCodiceFiscaleOrganizzatore;
            this.SpettacoloIntrattenimento = cSpettacoloIntrattenimento;
            this.TipoEvento = cTipoEvento;
            this.DescTipoEvento = cDescTipoEvento;
            this.PercIVA = nPercIVA;
            this.PercISI = nPercISI;
            this.IncidenzaIntrattenimento = nIncidenzaIntrattenimento;
            this.CodiceLocale = cCodiceLocale;
            this.DenominazioneLocale = cDenominazioneLocale;
            this.LuogoLocale = cLuogoLocale;
            this.TitoloEvento = cTitoloEvento;
            this.Esecutore = cEsecutore;
            this.Nazionalita = cNazionalita;
            this.Autore = cAutore;
            this.ProduttoreCinema = cProduttoreCinema;
            this.DataOraEvento = dDataOraEvento;
        }

        public void AddRowBiglietto(clsModuloC1row oRow)
        {
            bool lFindPage = false;
            foreach (System.Collections.DictionaryEntry ItemPageDict in this.PagesBiglietti)
            {
                clsModuloC1page oPage = (clsModuloC1page)ItemPageDict.Value;

                if (oRow.TitoloIvaPreassolta == "N")
                {
                    if (oPage.nCountRowsIvaDaAssolvere + (oRow.RifOmaggioEccedente != null ? 1 : 0) < MaxRigheBiglietti)
                    {
                        oPage.nCountRowsIvaDaAssolvere += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                        oPage.RowsIvaDaAssolve.Add(oPage.RowsIvaDaAssolve.Count + 1, oRow);
                        lFindPage = true;
                        break;
                    }
                }
                else
                {
                    if (oPage.nCountRowsIvaPreassolta + (oRow.RifOmaggioEccedente != null ? 1 : 0) < MaxRigheBiglietti)
                    {
                        oPage.nCountRowsIvaPreassolta += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                        oPage.RowsIvaPreAssolta.Add(oPage.RowsIvaPreAssolta.Count + 1, oRow);
                        lFindPage = true;
                        break;
                    }
                }
            }
            if (!lFindPage)
            {
                clsModuloC1page oPage = new clsModuloC1page();
                this.PagesBiglietti.Add(this.PagesBiglietti.Count + 1, oPage);
                oPage.EventPage = this.PagesBiglietti.Count;
                oPage.RefModulo = this;
                if (oRow.TitoloIvaPreassolta == "N")
                {
                    oPage.nCountRowsIvaDaAssolvere += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                    oPage.RowsIvaDaAssolve.Add(oPage.RowsIvaDaAssolve.Count + 1, oRow);
                }
                else
                {
                    oPage.nCountRowsIvaPreassolta += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                    oPage.RowsIvaPreAssolta.Add(oPage.RowsIvaDaAssolve.Count + 1, oRow);
                }
            }
        }

        public void AddRowBigliettoAnnullato(clsModuloC1row oRow)
        {
            clsModuloC1row rowFind = null;
            foreach (System.Collections.DictionaryEntry ItemPageDict in this.PagesBiglietti)
            {
                clsModuloC1page oPage = (clsModuloC1page)ItemPageDict.Value;

                if (oRow.TitoloIvaPreassolta == "N")
                {
                    if (oPage.nCountRowsIvaDaAssolvere > 0)
                    {
                        foreach (System.Collections.DictionaryEntry ItemModuloC1Dict in oPage.RowsIvaDaAssolve)
                        {
                            clsModuloC1row itemRow = (clsModuloC1row)ItemModuloC1Dict.Value;
                            if (itemRow.TipoTitolo == oRow.TipoTitolo)
                            {
                                rowFind = itemRow;
                                break;
                            }
                        }
                    }
                    if (rowFind != null) break;
                }
                else
                {
                    if (oPage.nCountRowsIvaPreassolta > 0)
                    {
                        foreach (System.Collections.DictionaryEntry ItemModuloC1Dict in oPage.RowsIvaPreAssolta)
                        {
                            clsModuloC1row itemRow = (clsModuloC1row)ItemModuloC1Dict.Value;
                            if (itemRow.TipoTitolo == oRow.TipoTitolo && itemRow.TitoloIvaPreassolta == oRow.TitoloIvaPreassolta)
                            {
                                rowFind = itemRow;
                                break;
                            }
                        }
                    }
                    if (rowFind != null) break;
                }
            }
            if (rowFind == null)
            {
                clsModuloC1page oPage = new clsModuloC1page();
                this.PagesBiglietti.Add(this.PagesBiglietti.Count + 1, oPage);
                oPage.EventPage = this.PagesBiglietti.Count;
                oPage.RefModulo = this;
                if (oRow.TitoloIvaPreassolta == "N")
                {
                    oPage.nCountRowsIvaDaAssolvere += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                    oPage.RowsIvaDaAssolve.Add(oPage.RowsIvaDaAssolve.Count + 1, oRow);
                }
                else
                {
                    oPage.nCountRowsIvaPreassolta += 1 + (oRow.RifOmaggioEccedente != null ? 1 : 0);
                    oPage.RowsIvaPreAssolta.Add(oPage.RowsIvaDaAssolve.Count + 1, oRow);
                }
            }
            else
            {
                rowFind.NumeroTitoliAnnullati += rowFind.NumeroTitoliAnnullati;
            }
        }

        public void AddRowAltroProventoEvento(clsModuloC1rowAltriProventi oProvento, bool proventiGenerici)
        {
            bool lFindPage = false;
            foreach (System.Collections.DictionaryEntry ItemPageDict in this.PagesAltriProventiEvento)
            {
                clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ItemPageDict.Value;
                long countRows = 0;

                foreach (System.Collections.DictionaryEntry itemRow in oPage.RowsAltriProventi)
                {
                    clsModuloC1rowAltriProventi itemProvento = (clsModuloC1rowAltriProventi)itemRow.Value;
                    countRows += 1;
                    if (itemProvento.TipoProvento.Length + itemProvento.DescTipoProvento.Length + 1 > 46)
                    {
                        countRows += 1;
                    }
                    if (proventiGenerici)
                    {
                        countRows += 1;
                        if (itemProvento.TipoGenerePrevalente.Length + itemProvento.DescTipoGenerePrevalente.Length + 1 > 46)
                        {
                            countRows += 1;
                        }
                    }
                }
                if (countRows < MaxRigheAltriProventi)
                {
                    lFindPage = true;
                    oPage.RowsAltriProventi.Add(oPage.RowsAltriProventi.Count + 1, oProvento);
                }
                else
                {
                    lFindPage = false;
                }

                //if (proventiGenerici)
                //{
                //    if ((oPage.RowsAltriProventi.Count * 2) < MaxRigheAltriProventi)
                //    {
                //        lFindPage = true;
                //        oPage.RowsAltriProventi.Add(oPage.RowsAltriProventi.Count + 1, oProvento);
                //    }
                //}
                //else
                //{
                //    if (oPage.RowsAltriProventi.Count < MaxRigheAltriProventi)
                //    {
                //        lFindPage = true;
                //        oPage.RowsAltriProventi.Add(oPage.RowsAltriProventi.Count + 1, oProvento);
                //    }
                //}
            }
            if (!lFindPage)
            {
                clsModuloC1AltriProventipage oPage = new clsModuloC1AltriProventipage();
                this.PagesAltriProventiEvento.Add(this.PagesAltriProventiEvento.Count + 1, oPage);
                oPage.AltriProventiPage = this.PagesAltriProventiEvento.Count;
                oPage.RefModulo = this;
                oPage.RowsAltriProventi.Add(oPage.RowsAltriProventi.Count + 1, oProvento);
            }
        }

        public void AddRowAbbonamento(clsModuloC2row oRow)
        {
            bool lFindPage = false;
            foreach (System.Collections.DictionaryEntry ItemPageDict in this.PagesAbbonamenti)
            {
                clsModuloC2page oPage = (clsModuloC2page)ItemPageDict.Value;

                int nCountRigheAbbonamenti = 0;
                foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                {
                    clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                    if (oRowAbbon.TitoloIvaPreassolta == "F")
                        nCountRigheAbbonamenti += 2;
                    else
                        nCountRigheAbbonamenti += 1;
                }

                if (nCountRigheAbbonamenti < MaxRigheAbbonamenti)
                {
                    oPage.RowsAbbonamenti.Add(oPage.RowsAbbonamenti.Count + 1, oRow);
                    lFindPage = true;
                    break;
                }
            }
            if (!lFindPage)
            {
                clsModuloC2page oPage = new clsModuloC2page();
                this.PagesAbbonamenti.Add(this.PagesAbbonamenti.Count + 1, oPage);
                oPage.AbbonPage = this.PagesAbbonamenti.Count;
                oPage.RefModulo = this;
                oPage.RowsAbbonamenti.Add(oPage.RowsAbbonamenti.Count + 1, oRow);
            }
        }

        public Int64 AbsoluteStartPage
        {
            get
            {
                return this.nAbsoluteStartPage;
            }
            set
            {
                this.nAbsoluteStartPage = value;
                Int64 nCnt = value;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    oPage.AbsoluteStartPage = nCnt;
                    nCnt += 1;
                }
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesAltriProventiEvento)
                {
                    clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ModuloC1pageDict.Value;
                    oPage.AbsoluteStartPage = nCnt;
                    nCnt += 1;
                }
                foreach (System.Collections.DictionaryEntry ModuloC2pageDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ModuloC2pageDict.Value;
                    oPage.AbsoluteStartPage = nCnt;
                    nCnt += 1;
                }
            }
        }

        


        public static string GetDescGiornoMese(DateTime GiornoMese, string GiornalieroMensile)
        {
            string cRet = "";

            if (GiornalieroMensile == "H")
            {
                cRet = GiornoMese.Hour.ToString("00") + ":" + GiornoMese.Minute.ToString("00");
            }
            else
            {
                if (GiornalieroMensile != "M")
                {
                    cRet = GiornoMese.Day.ToString("00") + " ";
                }

                switch (GiornoMese.Month)
                {
                    case 1:
                        {
                            cRet += "GENNAIO";
                            break;
                        }
                    case 2:
                        {
                            cRet += "FEBBRAIO";
                            break;
                        }
                    case 3:
                        {
                            cRet += "MARZO";
                            break;
                        }
                    case 4:
                        {
                            cRet += "APRILE";
                            break;
                        }
                    case 5:
                        {
                            cRet += "MAGGIO";
                            break;
                        }
                    case 6:
                        {
                            cRet += "GIUGNO";
                            break;
                        }
                    case 7:
                        {
                            cRet += "LUGLIO";
                            break;
                        }
                    case 8:
                        {
                            cRet += "AGOSTO";
                            break;
                        }
                    case 9:
                        {
                            cRet += "SETTEMBRE";
                            break;
                        }
                    case 10:
                        {
                            cRet += "OTTOBRE";
                            break;
                        }
                    case 11:
                        {
                            cRet += "NOVEMBRE";
                            break;
                        }
                    case 12:
                        {
                            cRet += "DICEMBRE";
                            break;
                        }
                }

                cRet += " " + GiornoMese.Year.ToString("0000");
            }

            return cRet;
        }

        public static string EnlargeText(string Text)
        {
            StringBuilder oRet = new StringBuilder();
            foreach (char cSingleChar in Text)
            {
                if (oRet.ToString().Trim() != "")
                {
                    oRet.Append(' ');
                }
                oRet.Append(cSingleChar);
            }
            return oRet.ToString();
        }

        public string CodiceLocaleLarge
        {
            get
            {
                return EnlargeText(this.CodiceLocale);
            }
        }

        public string CodiceFiscaleTitolareLarge
        {
            get
            {
                return EnlargeText(this.CodiceFiscaleTitolare);
            }
        }

        public string CodiceFiscaleOrganizzatoreLarge
        {
            get
            {
                return EnlargeText(this.CodiceFiscaleOrganizzatore);
            }
        }

        public Int64 TotIvaDaAssolvereNumeroTitoliEmessi
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.NumeroTitoliEmessi;
                    }
                }
                return nRet;
            }
        }

        public Int64 TotIvaPreAssoltaNumeroTitoliEmessi
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.NumeroTitoliEmessi;
                    }
                }
                return nRet;
            }
        }

        public Int64 TotEventoNumeroTitoliEmessi
        {
            get
            {
                Int64 nRet = this.TotIvaDaAssolvereNumeroTitoliEmessi + this.TotIvaPreAssoltaNumeroTitoliEmessi;
                return nRet;
            }
        }

        public Int64 TotIvaDaAssolvereNumeroTitoliAnnullati
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.NumeroTitoliAnnullati;
                    }
                }
                return nRet;
            }
        }

        public Int64 TotIvaPreAssoltaNumeroTitoliAnnullati
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.NumeroTitoliAnnullati;
                    }
                }
                return nRet;
            }
        }

        public Int64 TotEventoNumeroTitoliAnnullati
        {
            get
            {
                Int64 nRet = this.TotIvaDaAssolvereNumeroTitoliAnnullati + this.TotIvaPreAssoltaNumeroTitoliAnnullati;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereIncassoLordo
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IncassoLordo;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaIncassoLordo
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IncassoLordo;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoIncassoLordo
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereIncassoLordo + this.TotIvaPreAssoltaIncassoLordo;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereIncassoPrevendita
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IncassoPrevendita;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaIncassoPrevendita
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IncassoPrevendita;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoIncassoPrevendita
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereIncassoPrevendita + this.TotIvaPreAssoltaIncassoPrevendita;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereImponibileImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.ImponibileImpostaIntrattenimenti;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaImponibileImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.ImponibileImpostaIntrattenimenti;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoImponibileImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereImponibileImpostaIntrattenimenti + this.TotIvaPreAssoltaImponibileImpostaIntrattenimenti;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.ImpostaIntrattenimenti;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.ImpostaIntrattenimenti;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoImpostaIntrattenimenti
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereImpostaIntrattenimenti + this.TotIvaPreAssoltaImpostaIntrattenimenti;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereImponibileIVA
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.InponibileIVA;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaImponibileIVA
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.InponibileIVA;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoImponibileIVA
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereImponibileIVA + this.TotIvaPreAssoltaImponibileIVA;
                return nRet;
            }
        }

        public decimal TotIvaDaAssolvereIVALorda
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IVALorda;
                    }
                }
                return nRet;
            }
        }

        public decimal TotIvaPreAssoltaIVALorda
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        nRet += oRow.IVALorda;
                    }
                }
                return nRet;
            }
        }

        public decimal TotEventoIVALorda
        {
            get
            {
                decimal nRet = this.TotIvaDaAssolvereIVALorda + this.TotIvaPreAssoltaIVALorda;
                return nRet;
            }
        }

        public Int64 TotAbbonamentiEmessi
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        nRet += oRowAbbon.QtaEmessi;
                    }
                }
                return nRet;
            }
        }

        public Int64 TotAbbonamentiAnnullati
        {
            get
            {
                Int64 nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        nRet += oRowAbbon.QtaAnnullati;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAbbonamentiIncassoLordo
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        nRet += oRowAbbon.IncassoLordo;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAbbonamentiIVALorda
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        if (oRowAbbon.TitoloIvaPreassolta == "N")
                            nRet += oRowAbbon.IVALorda;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAbbonamentiIVAPreassolta
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        if (oRowAbbon.TitoloIvaPreassolta == "F")
                            nRet += oRowAbbon.IVALorda;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAbbonamentiNetto
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAbbonDict in this.PagesAbbonamenti)
                {
                    clsModuloC2page oPage = (clsModuloC2page)ItemPageAbbonDict.Value;
                    foreach (System.Collections.DictionaryEntry ItemRowAbbonDict in oPage.RowsAbbonamenti)
                    {
                        clsModuloC2row oRowAbbon = (clsModuloC2row)ItemRowAbbonDict.Value;
                        nRet += oRowAbbon.Netto;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAltriProventiCorrispettivo
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAltriProvEventoDict in this.PagesAltriProventiEvento)
                {
                    clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ItemPageAltriProvEventoDict.Value;

                    foreach (System.Collections.DictionaryEntry ItemRowAltriProvDict in oPage.RowsAltriProventi)
                    {
                        clsModuloC1rowAltriProventi oRowAltriProv = (clsModuloC1rowAltriProventi)ItemRowAltriProvDict.Value;
                        nRet += oRowAltriProv.Corrispettivo;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAltriProventiIVA
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAltriProvEventoDict in this.PagesAltriProventiEvento)
                {
                    clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ItemPageAltriProvEventoDict.Value;

                    foreach (System.Collections.DictionaryEntry ItemRowAltriProvDict in oPage.RowsAltriProventi)
                    {
                        clsModuloC1rowAltriProventi oRowAltriProv = (clsModuloC1rowAltriProventi)ItemRowAltriProvDict.Value;
                        nRet += oRowAltriProv.IVA;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAltriProventiImponibileImposta
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAltriProvEventoDict in this.PagesAltriProventiEvento)
                {
                    clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ItemPageAltriProvEventoDict.Value;

                    foreach (System.Collections.DictionaryEntry ItemRowAltriProvDict in oPage.RowsAltriProventi)
                    {
                        clsModuloC1rowAltriProventi oRowAltriProv = (clsModuloC1rowAltriProventi)ItemRowAltriProvDict.Value;
                        nRet += oRowAltriProv.ImponibileImposta;
                    }
                }
                return nRet;
            }
        }

        public decimal TotAltriProventiImpostaIntra
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ItemPageAltriProvEventoDict in this.PagesAltriProventiEvento)
                {
                    clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ItemPageAltriProvEventoDict.Value;

                    foreach (System.Collections.DictionaryEntry ItemRowAltriProvDict in oPage.RowsAltriProventi)
                    {
                        clsModuloC1rowAltriProventi oRowAltriProv = (clsModuloC1rowAltriProventi)ItemRowAltriProvDict.Value;
                        nRet += oRowAltriProv.ImpostaIntra;
                    }
                }
                return nRet;
            }
        }




        public decimal TotImponibileIVAOmaggiEccedenti_DaAssolvere
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        if (oRow.TitoloIvaPreassolta == "N" && oRow.RifOmaggioEccedente != null && oRow.QtaOmaggiEccedentiIVA > 0 && oRow.IvaOmaggiEccedenti > 0)
                        {
                            nRet += oRow.RifOmaggioEccedente.InponibileIVA * oRow.QtaOmaggiEccedentiIVA;
                        }
                    }
                }
                return nRet;
            }
        }


        public decimal TotImponibileIVAOmaggiEccedenti_Preassolta
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        if (oRow.TitoloIvaPreassolta != "N" && oRow.RifOmaggioEccedente != null && oRow.QtaOmaggiEccedentiIVA > 0 && oRow.IvaOmaggiEccedenti > 0)
                        {
                            nRet += oRow.RifOmaggioEccedente.InponibileIVA * oRow.QtaOmaggiEccedentiIVA;
                        }
                    }
                }
                return nRet;
            }
        }

        public decimal TotImponibileIVAOmaggiEccedenti_Totale
        {
            get
            {
                decimal nRet = TotImponibileIVAOmaggiEccedenti_DaAssolvere + TotImponibileIVAOmaggiEccedenti_Preassolta;
                return nRet;
            }
        }


        public decimal TotImponibileISIOmaggiEccedenti_DaAssolvere
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        if (oRow.TitoloIvaPreassolta == "N" && oRow.RifOmaggioEccedente != null && oRow.QtaOmaggiEccedentiISI > 0 && oRow.ImpostaIntrattenimentoOmaggiEccedenti > 0)
                        {
                            nRet += oRow.RifOmaggioEccedente.ImponibileImpostaIntrattenimenti * oRow.QtaOmaggiEccedentiISI;
                        }
                    }
                }
                return nRet;
            }
        }


        public decimal TotImponibileISIOmaggiEccedenti_Preassolta
        {
            get
            {
                decimal nRet = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1pageDict in this.PagesBiglietti)
                {
                    clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                    foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                    {
                        clsModuloC1row oRow = (clsModuloC1row)ModuloC1rowDict.Value;
                        if (oRow.TitoloIvaPreassolta != "N" && oRow.RifOmaggioEccedente != null && oRow.QtaOmaggiEccedentiISI > 0 && oRow.ImpostaIntrattenimentoOmaggiEccedenti > 0)
                        {
                            nRet += oRow.RifOmaggioEccedente.ImponibileImpostaIntrattenimenti * oRow.QtaOmaggiEccedentiISI;
                        }
                    }
                }
                return nRet;
            }
        }

        public decimal TotImponibileISIOmaggiEccedenti_Totale
        {
            get
            {
                decimal nRet = TotImponibileISIOmaggiEccedenti_DaAssolvere + TotImponibileISIOmaggiEccedenti_Preassolta;
                return nRet;
            }
        }
    }
}
