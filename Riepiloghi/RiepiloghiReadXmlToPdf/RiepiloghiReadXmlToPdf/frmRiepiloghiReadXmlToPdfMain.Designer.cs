﻿
namespace RiepiloghiReadXmlToPdf
{
    partial class frmRiepiloghiReadXmlToPdfMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRiepiloghiReadXmlToPdfMain));
            this.pnlFile = new System.Windows.Forms.Panel();
            this.btnInverti = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnFile = new System.Windows.Forms.Button();
            this.lblFile = new System.Windows.Forms.Label();
            this.lblInfo = new RiepiloghiReadXmlToPdf.frmRiepiloghiReadXmlToPdfMain.clsLabelProgress();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.chkRPM = new System.Windows.Forms.CheckBox();
            this.chkRPG = new System.Windows.Forms.CheckBox();
            this.lblFiltri = new System.Windows.Forms.Label();
            this.pnlPath = new System.Windows.Forms.Panel();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnPath = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.btnPathExtract = new System.Windows.Forms.Button();
            this.chkSubFolders = new System.Windows.Forms.CheckBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPDF = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlData = new System.Windows.Forms.Panel();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.pnlFile.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlPath.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFile
            // 
            this.pnlFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFile.Controls.Add(this.btnInverti);
            this.pnlFile.Controls.Add(this.panel1);
            this.pnlFile.Controls.Add(this.panel2);
            this.pnlFile.Controls.Add(this.pnlPath);
            this.pnlFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFile.Location = new System.Drawing.Point(0, 0);
            this.pnlFile.Name = "pnlFile";
            this.pnlFile.Size = new System.Drawing.Size(778, 225);
            this.pnlFile.TabIndex = 0;
            // 
            // btnInverti
            // 
            this.btnInverti.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnInverti.Location = new System.Drawing.Point(0, 192);
            this.btnInverti.Name = "btnInverti";
            this.btnInverti.Size = new System.Drawing.Size(75, 31);
            this.btnInverti.TabIndex = 3;
            this.btnInverti.Text = "Inverti";
            this.btnInverti.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtFile);
            this.panel1.Controls.Add(this.btnFile);
            this.panel1.Controls.Add(this.lblFile);
            this.panel1.Controls.Add(this.lblInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 108);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(776, 84);
            this.panel1.TabIndex = 1;
            // 
            // txtFile
            // 
            this.txtFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFile.Location = new System.Drawing.Point(94, 4);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(601, 37);
            this.txtFile.TabIndex = 1;
            // 
            // btnFile
            // 
            this.btnFile.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFile.Location = new System.Drawing.Point(695, 4);
            this.btnFile.MaximumSize = new System.Drawing.Size(75, 27);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(75, 27);
            this.btnFile.TabIndex = 2;
            this.btnFile.Text = "...";
            this.btnFile.UseVisualStyleBackColor = true;
            // 
            // lblFile
            // 
            this.lblFile.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblFile.Location = new System.Drawing.Point(4, 4);
            this.lblFile.MaximumSize = new System.Drawing.Size(123, 27);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(90, 27);
            this.lblFile.TabIndex = 0;
            this.lblFile.Text = "Singolo File:";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.Gold;
            this.lblInfo.Counter = ((long)(0));
            this.lblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblInfo.Index = ((long)(0));
            this.lblInfo.Location = new System.Drawing.Point(4, 39);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(766, 39);
            this.lblInfo.TabIndex = 3;
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dateTimePicker);
            this.panel2.Controls.Add(this.chkDate);
            this.panel2.Controls.Add(this.chkRPM);
            this.panel2.Controls.Add(this.chkRPG);
            this.panel2.Controls.Add(this.lblFiltri);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 73);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4);
            this.panel2.Size = new System.Drawing.Size(776, 35);
            this.panel2.TabIndex = 2;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Location = new System.Drawing.Point(350, 4);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(360, 37);
            this.dateTimePicker.TabIndex = 3;
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkDate.Location = new System.Drawing.Point(328, 4);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(22, 25);
            this.chkDate.TabIndex = 4;
            this.chkDate.UseVisualStyleBackColor = true;
            // 
            // chkRPM
            // 
            this.chkRPM.AutoSize = true;
            this.chkRPM.Checked = true;
            this.chkRPM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRPM.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkRPM.Location = new System.Drawing.Point(216, 4);
            this.chkRPM.Name = "chkRPM";
            this.chkRPM.Size = new System.Drawing.Size(112, 25);
            this.chkRPM.TabIndex = 2;
            this.chkRPM.Text = "Mensili";
            this.chkRPM.UseVisualStyleBackColor = true;
            // 
            // chkRPG
            // 
            this.chkRPG.AutoSize = true;
            this.chkRPG.Checked = true;
            this.chkRPG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRPG.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkRPG.Location = new System.Drawing.Point(72, 4);
            this.chkRPG.Name = "chkRPG";
            this.chkRPG.Size = new System.Drawing.Size(144, 25);
            this.chkRPG.TabIndex = 1;
            this.chkRPG.Text = "Giornalieri";
            this.chkRPG.UseVisualStyleBackColor = true;
            // 
            // lblFiltri
            // 
            this.lblFiltri.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblFiltri.Location = new System.Drawing.Point(4, 4);
            this.lblFiltri.Name = "lblFiltri";
            this.lblFiltri.Size = new System.Drawing.Size(68, 25);
            this.lblFiltri.TabIndex = 0;
            this.lblFiltri.Text = "Filtri:";
            this.lblFiltri.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPath
            // 
            this.pnlPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPath.Controls.Add(this.txtPath);
            this.pnlPath.Controls.Add(this.btnPath);
            this.pnlPath.Controls.Add(this.lblPath);
            this.pnlPath.Controls.Add(this.btnPathExtract);
            this.pnlPath.Controls.Add(this.chkSubFolders);
            this.pnlPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPath.Location = new System.Drawing.Point(0, 0);
            this.pnlPath.Name = "pnlPath";
            this.pnlPath.Padding = new System.Windows.Forms.Padding(4);
            this.pnlPath.Size = new System.Drawing.Size(776, 73);
            this.pnlPath.TabIndex = 0;
            // 
            // txtPath
            // 
            this.txtPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPath.Location = new System.Drawing.Point(71, 4);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(549, 37);
            this.txtPath.TabIndex = 1;
            this.txtPath.Text = "c:\\riepiloghi";
            // 
            // btnPath
            // 
            this.btnPath.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPath.Location = new System.Drawing.Point(620, 4);
            this.btnPath.MaximumSize = new System.Drawing.Size(75, 27);
            this.btnPath.Name = "btnPath";
            this.btnPath.Size = new System.Drawing.Size(75, 27);
            this.btnPath.TabIndex = 2;
            this.btnPath.Text = "...";
            this.btnPath.UseVisualStyleBackColor = true;
            // 
            // lblPath
            // 
            this.lblPath.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPath.Location = new System.Drawing.Point(4, 4);
            this.lblPath.MaximumSize = new System.Drawing.Size(125, 27);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(67, 27);
            this.lblPath.TabIndex = 0;
            this.lblPath.Text = "Cartella:";
            this.lblPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPathExtract
            // 
            this.btnPathExtract.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPathExtract.Location = new System.Drawing.Point(695, 4);
            this.btnPathExtract.MaximumSize = new System.Drawing.Size(75, 27);
            this.btnPathExtract.Name = "btnPathExtract";
            this.btnPathExtract.Size = new System.Drawing.Size(75, 27);
            this.btnPathExtract.TabIndex = 3;
            this.btnPathExtract.Text = "cerca";
            this.btnPathExtract.UseVisualStyleBackColor = true;
            // 
            // chkSubFolders
            // 
            this.chkSubFolders.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chkSubFolders.Location = new System.Drawing.Point(4, 40);
            this.chkSubFolders.Name = "chkSubFolders";
            this.chkSubFolders.Size = new System.Drawing.Size(766, 27);
            this.chkSubFolders.TabIndex = 4;
            this.chkSubFolders.Text = "anche le sottocartelle";
            this.chkSubFolders.UseVisualStyleBackColor = true;
            // 
            // pnlButtons
            // 
            this.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlButtons.Controls.Add(this.btnClear);
            this.pnlButtons.Controls.Add(this.btnPDF);
            this.pnlButtons.Controls.Add(this.btnExit);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 478);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(778, 66);
            this.pnlButtons.TabIndex = 1;
            // 
            // btnClear
            // 
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClear.Location = new System.Drawing.Point(154, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(151, 58);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Azzera";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnPDF
            // 
            this.btnPDF.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPDF.Location = new System.Drawing.Point(622, 3);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(151, 58);
            this.btnPDF.TabIndex = 1;
            this.btnPDF.Text = "Genera Pdf";
            this.btnPDF.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExit.Location = new System.Drawing.Point(3, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(151, 58);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Esci";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 225);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(778, 253);
            this.pnlData.TabIndex = 0;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // frmRiepiloghiReadXmlToPdfMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(778, 544);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.pnlFile);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRiepiloghiReadXmlToPdfMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lettura Xml Riepiloghi";
            this.pnlFile.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlPath.ResumeLayout(false);
            this.pnlPath.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFile;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkRPM;
        private System.Windows.Forms.CheckBox chkRPG;
        private System.Windows.Forms.Label lblFiltri;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Panel pnlPath;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnPath;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Button btnPathExtract;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckBox chkSubFolders;
        private System.Windows.Forms.Button btnClear;
        private frmRiepiloghiReadXmlToPdfMain.clsLabelProgress lblInfo;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.CheckBox chkDate;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button btnInverti;
    }
}

