﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiepiloghiReadXmlToPdf
{
    public class MakeFilePdf
    {
        public static System.Collections.SortedList oFonts;

        public static event FileRiepilogo.delegateOutProcess OutProcess;

        private static void RaiseOutProcess(long index, long counter, string message)
        {
            if (OutProcess != null)
                OutProcess(index, counter, message);
        }

        public static string Make(string FilePdfToSave, System.Collections.SortedList PagineModuliC1C2)
        {
            string result = "";
            Exception oError = null;
            #region "Pdf"

            List<string> TipiTitoloOmaggio = FileRiepilogo.GetTipiTitoloOmaggio();
            // Generazione PDF
            if (PagineModuliC1C2 != null && PagineModuliC1C2.Count > 0)
            {
                long counter = PagineModuliC1C2.Count;
                long index = 0;
                clsModuloC1C2 oModuloC1C2 = null;
                // Contatore pagine totale
                Int64 nTotPagine = 0;
                foreach (System.Collections.DictionaryEntry oItemModuloC1C2Dict in PagineModuliC1C2)
                {
                    index += 1;
                    RaiseOutProcess(index, counter, "pagine Pdf");
                    oModuloC1C2 = (clsModuloC1C2)oItemModuloC1C2Dict.Value;
                    oModuloC1C2.AbsoluteStartPage = nTotPagine + 1;
                    nTotPagine += oModuloC1C2.PagesBiglietti.Count + oModuloC1C2.PagesAltriProventiEvento.Count + oModuloC1C2.PagesAbbonamenti.Count;
                }

                // Creazione documento PDF
                clsPdfDocument oDoc = new clsPdfDocument();

                // Impostazione dimensione pagine
                oDoc.DocPdf.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                oDoc.DocPdf.SetMargins(10, 10, 30, 30);

                // Apertura del documento
                oDoc.DocPdf.Open();

                // Inizializzazione dei fotns
                InitFonts(out oError);

                // Impostazione colore per scrittura e disegno
                oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);
                oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
                oDoc.Writer.DirectContent.SetLineWidth(.1f);

                // ciclo moduli
                foreach (System.Collections.DictionaryEntry oItemModuloC1C2Dict in PagineModuliC1C2)
                {
                    oModuloC1C2 = (clsModuloC1C2)oItemModuloC1C2Dict.Value;

                    if (oModuloC1C2.PagesBiglietti.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry ModuloC1pageDict in oModuloC1C2.PagesBiglietti)
                        {
                            clsModuloC1page oPage = (clsModuloC1page)ModuloC1pageDict.Value;
                            PrintPageBiglietti(oModuloC1C2.Giorno, oModuloC1C2.GiornalieroMensile, oModuloC1C2.Progressivo, oPage, oDoc, nTotPagine, out oError, TipiTitoloOmaggio, oModuloC1C2.NomeFileRiepilogo);
                        }
                    }
                    else if (nTotPagine == 0)
                    {
                        clsModuloC1page oPage = new clsModuloC1page();
                        oPage.RefModulo = oModuloC1C2;
                        oPage.AbsoluteStartPage = 1;
                        oPage.EventPage = 1;
                        PrintPageBiglietti(oModuloC1C2.Giorno, oModuloC1C2.GiornalieroMensile, oModuloC1C2.Progressivo, oPage, oDoc, 1, out oError, TipiTitoloOmaggio, oModuloC1C2.NomeFileRiepilogo);
                    }

                    foreach (System.Collections.DictionaryEntry ModuloC1pageDict in oModuloC1C2.PagesAltriProventiEvento)
                    {
                        clsModuloC1AltriProventipage oPage = (clsModuloC1AltriProventipage)ModuloC1pageDict.Value;
                        PrintPageAltriProventiEvento(oModuloC1C2.Giorno, oModuloC1C2.GiornalieroMensile, oModuloC1C2.Progressivo, oPage, oDoc, nTotPagine, out oError, oModuloC1C2.NomeFileRiepilogo);
                    }

                    foreach (System.Collections.DictionaryEntry ModuloC2pageDict in oModuloC1C2.PagesAbbonamenti)
                    {
                        clsModuloC2page oPage = (clsModuloC2page)ModuloC2pageDict.Value;
                        PrintPageAbbonamenti(oModuloC1C2.Giorno, oModuloC1C2.GiornalieroMensile, oModuloC1C2.Progressivo, oPage, oDoc, nTotPagine, out oError, oModuloC1C2.NomeFileRiepilogo);
                    }
                }

                // Chiusura documento PDF
                oDoc.DocPdf.Close();

                byte[] Buffer = oDoc.PdfStream.GetBuffer();

                if (!string.IsNullOrEmpty(FilePdfToSave) && !string.IsNullOrWhiteSpace(FilePdfToSave))
                {
                    SaveToFile(FilePdfToSave, oDoc.PdfStream);
                    result = FilePdfToSave;
                }
                oDoc.Dispose();

                RaiseOutProcess(0, 0, "terminato.");
            }

            #endregion

            

            return result;
        }

        // Inizializza i font per la stampa in pdf
        public static void InitFonts(out Exception oError)
        {
            oError = null;
            oFonts = new System.Collections.SortedList();
            //System.Collections.SortedList oFonts = new System.Collections.SortedList();
            try
            {
                oFonts.Add("cour", iTextSharp.text.pdf.BaseFont.CreateFont("cour.ttf", iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED));
                oFonts.Add("courbd", iTextSharp.text.pdf.BaseFont.CreateFont("courbd.ttf", iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED));
            }
            catch (Exception ex)
            {
                oError = ex;
            }
        }

        // Inizializzazione font per stampa in pdf
        public static iTextSharp.text.pdf.BaseFont GetFont(string Name)
        {
            iTextSharp.text.pdf.BaseFont oRet = (iTextSharp.text.pdf.BaseFont)oFonts[Name];
            return oRet;
        }

        #region parti del pdf

        // Stampa modulo pagina biglietti
        private static void PrintPageBiglietti(DateTime Giorno, string GiornalieroMensile, Int64 Progressivo, clsModuloC1page oPage, clsPdfDocument oDoc, Int64 nTotPagine, out Exception oError, List<string> TipiTitoloOmaggio, string cNomeFileRiepilogo)
        {
            oError = null;
            // Se non è la primissima pagina del report ==> Nuova pagina
            if (oPage.AbsoluteStartPage > 1)
            {
                oDoc.DocPdf.NewPage();
                oDoc.Writer.DirectContent.SetLineWidth(.1f);
            }

            //PrintGridPage(oDoc);

            iTextSharp.text.pdf.BaseFont oFontLabelsSmall = GetFont("cour");
            float nSizeFontLabelsSmall = 6;
            iTextSharp.text.pdf.BaseFont oFontLabelsSmallBold = GetFont("courbd");
            float nSizeFontLabelsSmallBold = 7;
            iTextSharp.text.pdf.BaseFont oFontValues = GetFont("cour");
            float nSizeFontValues = 10;
            float nSizeFontValuesRifOmaggi = 6;
            iTextSharp.text.pdf.BaseFont oFontValuesBold = GetFont("courbd");
            iTextSharp.text.pdf.BaseFont oFontTotalePagina = GetFont("courbd");
            float nSizeFontTotalePagina = 10;
            iTextSharp.text.pdf.BaseFont oFontSIAE = GetFont("cour");
            float nSizeFontSIAE = 8;
            iTextSharp.text.pdf.BaseFont oFontTimbroFirma = GetFont("cour");
            float nSizeTimbroFirma = 6;

            float nSizeFontValuesTOTRifOmaggi = 8;

            // Se riepilogo vuoto
            if (oPage.RowsIvaDaAssolve.Count == 0 && oPage.RowsIvaPreAssolta.Count == 0)
            {
                oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.GRAY);
                oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.GRAY);
                PrintOut(new PointF(30, 39.5F), "NESSUN EVENTO", oDoc, oFontValuesBold, 60);
                oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);
                oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
            }
            else
            {
                long countRigheIVA_N = 0;
                long countRigheIVA_F = 0;
                long countRigheIVA_B = 0;
                foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
                {
                    clsModuloC1row oModuloC1row = (clsModuloC1row)ModuloC1rowDict.Value;
                    if (oModuloC1row.TitoloIvaPreassolta == "N") countRigheIVA_N += 1;
                    if (oModuloC1row.TitoloIvaPreassolta == "F") countRigheIVA_F += 1;
                    if (oModuloC1row.TitoloIvaPreassolta == "B") countRigheIVA_B += 1;
                }
                foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
                {
                    clsModuloC1row oModuloC1row = (clsModuloC1row)ModuloC1rowDict.Value;
                    if (oModuloC1row.TitoloIvaPreassolta == "N") countRigheIVA_N += 1;
                    if (oModuloC1row.TitoloIvaPreassolta == "F") countRigheIVA_F += 1;
                    if (oModuloC1row.TitoloIvaPreassolta == "B") countRigheIVA_B += 1;
                }

                bool lEmptyEvento = ((countRigheIVA_N + countRigheIVA_F + countRigheIVA_B) == 0);
                if (lEmptyEvento)
                {
                    oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.GRAY);
                    oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.GRAY);
                    PrintOut(new PointF(20, 39.5F), "NESSUN INGRESSO", oDoc, oFontValuesBold, 60);
                    oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);
                    oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
                }
            }

            // LAYOUT FISSO SCRITTE
            PrintOut(new PointF(60, 1), "Pag." + oPage.AbsoluteStartPage.ToString() + " di " + nTotPagine.ToString(), oDoc, oFontValuesBold, nSizeFontValues);


            PrintOut(new PointF(120, 1), "Allegato C.1 fronte", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            if (!string.IsNullOrEmpty(cNomeFileRiepilogo) && !string.IsNullOrWhiteSpace(cNomeFileRiepilogo))
            {
                PrintOut(new PointF(2, 1.5F), cNomeFileRiepilogo, oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            }

            PrintOut(new PointF(2, 3), "Riepilogo giornaliero del", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(50, 3), "Riepilogo mensile del", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(90, 3), "Trasmetto in data", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 5), "QUADRO A - EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(18, 5), "[   ] spettacolo", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(35, 5), "[   ] intrattenimento", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            //PrintOut(new PointF(65, 5), "INCIDENZA INTRATTENIMENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(52F, 4.2F), "IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            PrintOut(new PointF(52F, 5.0F), "EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            PrintOut(new PointF(58F, 4.2F), "IMPOSTA ", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            PrintOut(new PointF(58F, 5.0F), "INTRATTANENIMENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            PrintOut(new PointF(72F, 4.2F), "INCIDENZA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            PrintOut(new PointF(72F, 5.0F), "INTRATTENIMENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);

            PrintOut(new PointF(2, 7), "ORGANIZZATORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 7), "CODICE FISCALE/P.IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 9), "TITOLARE SISTEMA DI EMISSIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 9), "CODICE FISCALE/P.IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 11), "CODICE SISTEMA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 13), "DENOMINAZIONE LOCALE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(92, 13), "CODICE LOCALE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(11.8F, 15), "COMUNE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            //PrintOut(new PointF(0, 0), "PROVINCIA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 17), "TIPO EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 19), "TITOLO DELL'OPERA/MANIFESTAZIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(96, 19), "AUTORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            if (oPage.RefModulo.TipoEvento == "01")
            {
                PrintOut(new PointF(2, 21), "PRODUTTORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
                PrintOut(new PointF(85.4F, 21), "NAZIONALITA' DEL FILM", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            }
            else
                PrintOut(new PointF(2, 21), "COMPAGNIA/ESECUTORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);


            PrintOut(new PointF(2, 23), "NUMERO OPERE RAPPRESENTATE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(65, 23), "DATA EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(98, 23), "ORA INIZIO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 25), "QUADRO B - RIEPILOGO TITOLI D'ACCESSO PER EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(90, 25), "*Gli importi sono espressi in EURO al netto degli annullati", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintLine(new PointF(2, 26), new PointF(135, 26), oDoc);
            PrintLine(new PointF(2, 29), new PointF(135, 29), oDoc);
            PrintLine(new PointF(2, 45), new PointF(135, 45), oDoc);
            PrintLine(new PointF(31, 48), new PointF(135, 48), oDoc);

            PrintLine(new PointF(2, 26), new PointF(2, 45), oDoc);
            PrintLine(new PointF(8, 26), new PointF(8, 45), oDoc);
            PrintLine(new PointF(15, 26), new PointF(15, 45), oDoc);
            PrintLine(new PointF(24, 26), new PointF(24, 45), oDoc);
            PrintLine(new PointF(31, 26), new PointF(31, 48), oDoc);
            PrintLine(new PointF(38, 26), new PointF(38, 48), oDoc);
            PrintLine(new PointF(52, 26), new PointF(52, 48), oDoc);
            PrintLine(new PointF(67, 26), new PointF(67, 48), oDoc);
            PrintLine(new PointF(82, 26), new PointF(82, 48), oDoc);
            PrintLine(new PointF(97, 26), new PointF(97, 48), oDoc);
            PrintLine(new PointF(112, 26), new PointF(112, 48), oDoc);
            PrintLine(new PointF(127, 26), new PointF(127, 48), oDoc);
            PrintLine(new PointF(135, 26), new PointF(135, 48), oDoc);


            PrintLine(new PointF(2, 49), new PointF(135, 49), oDoc);
            PrintLine(new PointF(2, 65), new PointF(135, 65), oDoc);
            PrintLine(new PointF(31, 68), new PointF(135, 68), oDoc);

            PrintLine(new PointF(2, 49), new PointF(2, 65), oDoc);
            PrintLine(new PointF(8, 49), new PointF(8, 65), oDoc);
            PrintLine(new PointF(15, 49), new PointF(15, 65), oDoc);
            PrintLine(new PointF(24, 49), new PointF(24, 65), oDoc);
            PrintLine(new PointF(31, 49), new PointF(31, 68), oDoc);
            PrintLine(new PointF(38, 49), new PointF(38, 68), oDoc);
            PrintLine(new PointF(52, 49), new PointF(52, 68), oDoc);
            PrintLine(new PointF(67, 49), new PointF(67, 68), oDoc);
            PrintLine(new PointF(82, 49), new PointF(82, 68), oDoc);
            PrintLine(new PointF(97, 49), new PointF(97, 68), oDoc);
            PrintLine(new PointF(112, 49), new PointF(112, 68), oDoc);
            PrintLine(new PointF(127, 49), new PointF(127, 68), oDoc);
            PrintLine(new PointF(135, 49), new PointF(135, 68), oDoc);


            PrintLine(new PointF(31, 68.5F), new PointF(135, 68.5F), oDoc);
            PrintLine(new PointF(31, 71), new PointF(135, 71), oDoc);

            PrintLine(new PointF(31, 68.5F), new PointF(31, 71), oDoc);
            PrintLine(new PointF(38, 68.5F), new PointF(38, 71), oDoc);
            PrintLine(new PointF(52, 68.5F), new PointF(52, 71), oDoc);
            PrintLine(new PointF(67, 68.5F), new PointF(67, 71), oDoc);
            PrintLine(new PointF(82, 68.5F), new PointF(82, 71), oDoc);
            PrintLine(new PointF(97, 68.5F), new PointF(97, 71), oDoc);
            PrintLine(new PointF(112, 68.5F), new PointF(112, 71), oDoc);
            PrintLine(new PointF(127, 68.5F), new PointF(127, 71), oDoc);
            PrintLine(new PointF(135, 68.5F), new PointF(135, 71), oDoc);


            // Riquadro per biglietti IVA da assolvere
            PrintOut(new PointF(2.5F, 27.8F), "SETTORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(9, 27.8F), "CAPIENZA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(16, 27.8F), "TIPO TITOLI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(24.5F, 27.3F), " PREZZO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(24.5F, 28.3F), "UNITARIO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(31.5F, 27.3F), "N° TITOLI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(31.5F, 28.3F), " EMESSI*", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(40, 27.8F), "INCASSO LORDO*", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(52.5F, 27.8F), "INCASSO PREVENDITA*", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(69, 27.3F), "IMPONIBILE IMPOSTA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(69, 28.3F), " INTRATTENIMENTI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(84.5F, 27.3F), "    IMPOSTA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(84.5F, 28.3F), "INTRATTENIMENTI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(99.5F, 27.8F), "IMPONIBILE  IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(115.5F, 27.8F), "IVA   LORDA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(128, 27.3F), "N° TITOLI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(128, 28.3F), "ANNULLATI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(8, 47), "TOTALE IVA da assolvere", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            // Riquadro per biglietti IVA preassolta

            PrintOut(new PointF(9, 67), "TOTALE IVA preassolta", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(24, 70), "TOTALE", oDoc, oFontTotalePagina, nSizeFontTotalePagina);

            // Riempimento

            // FLAG GIORNALIERO MENSILE
            if (GiornalieroMensile == "G")
            {
                PrintOut(new PointF(21, 3), clsModuloC1C2.GetDescGiornoMese(Giorno, GiornalieroMensile), oDoc, oFontValuesBold, nSizeFontValues);
            }
            else if (GiornalieroMensile == "M" || GiornalieroMensile == "D")
            {
                PrintOut(new PointF(66, 3), clsModuloC1C2.GetDescGiornoMese(Giorno, GiornalieroMensile), oDoc, oFontValuesBold, nSizeFontValues);
            }

            // TRASMESSO IN DATA
            PrintOut(new PointF(103, 3), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataIns, "G"), oDoc, oFontValuesBold, nSizeFontValues);

            // SPETTACOLO INTRATTENIEMENTO
            if (oPage.RefModulo.SpettacoloIntrattenimento == "S")
            {
                PrintOut(new PointF(19.2F, 5.1F), "X", oDoc, oFontValuesBold, nSizeFontValues);
            }
            else if (oPage.RefModulo.SpettacoloIntrattenimento == "I")
            {
                PrintOut(new PointF(36.2F, 5.1F), "X", oDoc, oFontValuesBold, nSizeFontValues);
            }

            // INCIDENZA INTRATTENIMENTO
            //PrintOut(new PointF(83, 5), oPage.RefModulo.IncidenzaIntrattenimento.ToString().PadLeft(3) + "%", oDoc, oFontValuesBold, nSizeFontValues);

            // PERCENTUALE IVA EVENTO
            PrintOut(new PointF(52F, 6.25F), oPage.RefModulo.PercIVA.ToString().Trim() + "%", oDoc, oFontValuesBold, nSizeFontValues);
            if (oPage.RefModulo.SpettacoloIntrattenimento == "I")
            {
                // PERCENTUALE IMPOSTA INTRATTENIMENTO
                PrintOut(new PointF(58F, 6.25F), oPage.RefModulo.PercISI.ToString().Trim() + "%", oDoc, oFontValuesBold, nSizeFontValues);
                // PERCENTUAL INCIDENZA INTRATTENIMENTO
                PrintOut(new PointF(72F, 6.25F), oPage.RefModulo.IncidenzaIntrattenimento.ToString().Trim() + "%", oDoc, oFontValuesBold, nSizeFontValues);
            }

            // DENOMINAZIONE ORGANIZZATORE
            PrintOut(new PointF(12, 7), oPage.RefModulo.DenominazioneOrganizzatore, oDoc, oFontValuesBold, nSizeFontValues);

            // CODICE FISCALE ORGANIZZATORE
            PrintLine(new PointF(103, 5.7F), new PointF(135, 5.7F), oDoc);
            PrintLine(new PointF(103, 7.3F), new PointF(135, 7.3F), oDoc);
            for (int nHorz = 0; nHorz <= 32; nHorz += 2)
            {
                PrintLine(new PointF(103 + nHorz, 5.7F), new PointF(103 + nHorz, 7.3F), oDoc);
            }
            PrintOut(new PointF(103.5F, 7), oPage.RefModulo.CodiceFiscaleOrganizzatoreLarge, oDoc, oFontValuesBold, nSizeFontValues);

            // DENOMINAZIONE TITOLARE
            PrintOut(new PointF(24, 9), oPage.RefModulo.DenominazioneTitolare, oDoc, oFontValuesBold, nSizeFontValues);

            // CODICE FISCALE TITOLARE
            PrintLine(new PointF(103, 7.7F), new PointF(135, 7.7F), oDoc);
            PrintLine(new PointF(103, 9.3F), new PointF(135, 9.3F), oDoc);
            for (int nHorz = 0; nHorz <= 32; nHorz += 2)
            {
                PrintLine(new PointF(103 + nHorz, 7.7F), new PointF(103 + nHorz, 9.3F), oDoc);
            }
            PrintOut(new PointF(103.5F, 9), oPage.RefModulo.CodiceFiscaleTitolareLarge, oDoc, oFontValuesBold, nSizeFontValues);

            // CODICE SISTEMA DI EMISSIONE
            PrintOut(new PointF(24, 11), oPage.RefModulo.CodiceSistema, oDoc, oFontValuesBold, nSizeFontValues);

            if (oPage.RowsIvaDaAssolve.Count > 0 || oPage.RowsIvaPreAssolta.Count > 0)
            {
                // DENOMINAZIONE LOCALE
                PrintOut(new PointF(18, 13), oPage.RefModulo.DenominazioneLocale, oDoc, oFontValuesBold, nSizeFontValues);

                // CODICE LOCALE
                //PrintOut(new PointF(90.5F, 13), oPage.RefModulo.CodiceLocaleLarge, oDoc, oFontValuesBold, nSizeFontValues);
                //PrintLine(new PointF(90, 11.7F), new PointF(116, 11.7F), oDoc);
                //PrintLine(new PointF(90, 13.3F), new PointF(116, 13.3F), oDoc);
                PrintOut(new PointF(103.5F, 13), oPage.RefModulo.CodiceLocaleLarge, oDoc, oFontValuesBold, nSizeFontValues);
                PrintLine(new PointF(103, 11.7F), new PointF(129, 11.7F), oDoc);
                PrintLine(new PointF(103, 13.3F), new PointF(129, 13.3F), oDoc);
                for (int nHorz = 0; nHorz <= 26; nHorz += 2)
                {
                    //PrintLine(new PointF(90 + nHorz, 11.7F), new PointF(90 + nHorz, 13.3F), oDoc);
                    PrintLine(new PointF(103 + nHorz, 11.7F), new PointF(103 + nHorz, 13.3F), oDoc);
                }

                // COMUNE
                PrintOut(new PointF(18, 15), oPage.RefModulo.LuogoLocale, oDoc, oFontValuesBold, nSizeFontValues);
                //// PROVINCIA
                //PrintOut(new PointF(0, 0), "", oDoc, oFontValuesBold, nSizeFontValues);

                // TIPO EVENTO
                PrintOut(new PointF(11, 17), oPage.RefModulo.TipoEvento + " " + oPage.RefModulo.DescTipoEvento, oDoc, oFontValuesBold, nSizeFontValues);

                // TITOLO EVENTO
                PrintOut(new PointF(25, 19), oPage.RefModulo.TitoloEvento, oDoc, oFontValuesBold, nSizeFontValues);


                // AUTORE
                PrintOut(new PointF(101, 19), oPage.RefModulo.Autore, oDoc, oFontValuesBold, nSizeFontValues);
                if (oPage.RefModulo.TipoEvento == "01")
                {
                    // PRODUTTORE/DISTRIBUTORE
                    PrintOut(new PointF(16, 21), oPage.RefModulo.ProduttoreCinema, oDoc, oFontValuesBold, nSizeFontValues);
                    // NAZIONALITA
                    PrintOut(new PointF(101, 21), oPage.RefModulo.Nazionalita, oDoc, oFontValuesBold, nSizeFontValues);
                }
                else
                    // ESECUTORE
                    PrintOut(new PointF(16, 21), oPage.RefModulo.Esecutore, oDoc, oFontValuesBold, nSizeFontValues);

                // NUMERO OPERE RAPPRESENTATE
                PrintOut(new PointF(22, 23), "1", oDoc, oFontValuesBold, nSizeFontValues);
                // DATA EVENTO
                PrintOut(new PointF(74, 23), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "G"), oDoc, oFontValuesBold, nSizeFontValues);
                // ORA EVENTO
                PrintOut(new PointF(106, 23), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "H"), oDoc, oFontValuesBold, nSizeFontValues);
            }

            float nRow = 0;

            nRow = 30.2F;


            //decimal nTotImponibileOmaggiEccedentiIVA_N = 0;
            //decimal nTotImponibileOmaggiEccedentiISI_N = 0;

            //decimal nTotImponibileOmaggiEccedentiIVA_B = 0;
            //decimal nTotImponibileOmaggiEccedentiISI_B = 0;

            //decimal nTotImponibileOmaggiEccedentiIVA_TOT = 0;
            //decimal nTotImponibileOmaggiEccedentiISI_TOT = 0;

            // RIGHE BIGLIETTI CON IVA DA ASSOLVERE
            foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaDaAssolve)
            {
                clsModuloC1row oModuloC1row = (clsModuloC1row)ModuloC1rowDict.Value;

                if (string.IsNullOrEmpty(oModuloC1row.TitoloIvaPreassolta) || string.IsNullOrWhiteSpace(oModuloC1row.TitoloIvaPreassolta))
                    continue;
                // SETTORE
                PrintOut(new PointF(3, nRow), oModuloC1row.Settore, oDoc, oFontValues, nSizeFontValues);
                // CAPIENZA
                PrintOut(new PointF(9, nRow), oModuloC1row.Capiena.ToString("#,##0").PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                // TIPO TITOLO
                PrintOut(new PointF(18.5F, nRow), oModuloC1row.TipoTitolo, oDoc, oFontValues, nSizeFontValues);
                // PREZZO UNITARIO
                PrintOut(new PointF(23.6F, nRow), oModuloC1row.PrezzoUnitario.ToString("#,##0.00").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // QTA EMESSI
                PrintOut(new PointF(30.5F, nRow), oModuloC1row.NumeroTitoliEmessi.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // INCASSO LORDO
                PrintOut(new PointF(39, nRow), oModuloC1row.IncassoLordo.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // INCASSO PREVENDITA
                PrintOut(new PointF(54, nRow), oModuloC1row.IncassoPrevendita.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(69, nRow), oModuloC1row.ImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(84, nRow), oModuloC1row.ImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IVA
                PrintOut(new PointF(99, nRow), oModuloC1row.InponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IVA LORDA
                PrintOut(new PointF(114, nRow), oModuloC1row.IVALorda.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // ANNULLI
                PrintOut(new PointF(127, nRow), oModuloC1row.NumeroTitoliAnnullati.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);

                if (TipiTitoloOmaggio.Contains(oModuloC1row.TipoTitolo) && (oModuloC1row.IVALorda > 0 || oModuloC1row.ImpostaIntrattenimenti > 0))
                {
                    PrintOut(new PointF(136, nRow), "Ecc.", oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                    if (oModuloC1row.RifOmaggioEccedente != null)
                    {
                        if (nRow > 30.2F)
                        {
                            PrintLine(new PointF(2, nRow - 1.1F), new PointF(135, nRow - 1.1F), oDoc);
                        }

                        nRow += 1F;

                        // SETTORE
                        PrintOut(new PointF(3.68F, nRow), oModuloC1row.RifOmaggioEccedente.Settore, oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // TIPO TITOLO
                        PrintOut(new PointF(19.15F, nRow), oModuloC1row.RifOmaggioEccedente.TipoTitolo, oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // PREZZO UNITARIO
                        PrintOut(new PointF(24.4F, nRow), oModuloC1row.RifOmaggioEccedente.PrezzoUnitario.ToString("#,##0.00").PadLeft(10, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // QTA ECCEDENTE
                        //PrintOut(new PointF(32F, nRow), "Ecc." + oModuloC1row.QtaOmaggiEccedenti.ToString("#,##0").PadLeft(5, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        PrintOut(new PointF(32F, nRow), "Ecc." + System.Math.Max(oModuloC1row.QtaOmaggiEccedentiIVA, oModuloC1row.QtaOmaggiEccedentiISI).ToString("#,##0").PadLeft(5, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);

                        // Indicazione riferimento
                        PrintOut(new PointF(39F, nRow), "<- rif. eccedenza ->", oDoc, oFontValues, nSizeFontValuesRifOmaggi);



                        if (oPage.RefModulo.SpettacoloIntrattenimento == "I")
                        {
                            // IMPONIBILE IMPOSTA INTRATTENIMENTI
                            PrintOut(new PointF(67.2F, nRow), "Impon.Int.rif." + oModuloC1row.RifOmaggioEccedente.ImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                            // IMPOSTA INTRATTENIMENTI
                            PrintOut(new PointF(82.2F, nRow), "Impos.Int.rif." + (oModuloC1row.RifOmaggioEccedente.ImpostaIntrattenimenti.ToString("#,##0.00").Trim() + " x" + oModuloC1row.QtaOmaggiEccedentiISI.ToString().Trim()).PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        }

                        // IMPONIBILE IVA
                        //PrintOut(new PointF(103.8F, nRow), "Imponib.IVA rif." + oModuloC1row.RifOmaggioEccedente.InponibileIVA.ToString("#,##0.00").PadLeft(12, '#'), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        if (oModuloC1row.RifOmaggioEccedente.IVALorda > 0)
                        {
                            PrintOut(new PointF(97.2F, nRow), "Impon.IVA rif." + oModuloC1row.RifOmaggioEccedente.InponibileIVA.ToString("#,##0.00").PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                            // IVA LORDA
                            PrintOut(new PointF(112.2F, nRow), "IVA di rif." + (oModuloC1row.RifOmaggioEccedente.IVALorda.ToString("#,##0.00").Trim() + " x" + oModuloC1row.QtaOmaggiEccedentiIVA.ToString().Trim()).PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        }

                        nRow += .45F;

                        if (nRow <= 44)
                        {
                            PrintLine(new PointF(2, nRow + .35F), new PointF(135, nRow + .35F), oDoc);
                        }
                    }
                }

                nRow += 1.45F;
            }


            nRow = 47;

            // Eventuali totali IVA da assolvere
            if (oPage.EventPage == oPage.RefModulo.PagesBiglietti.Count)
            {
                // QTA EMESSI
                PrintOut(new PointF(30.5F, nRow), oPage.RefModulo.TotIvaDaAssolvereNumeroTitoliEmessi.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // INCASSO LORDO
                PrintOut(new PointF(39, nRow), oPage.RefModulo.TotIvaDaAssolvereIncassoLordo.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // INCASSO PREVENDITA
                PrintOut(new PointF(54, nRow), oPage.RefModulo.TotIvaDaAssolvereIncassoPrevendita.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IMPOSTA INTRATTENIMENTI
                if (oPage.RefModulo.TotImponibileISIOmaggiEccedenti_DaAssolvere > 0)
                {
                    PrintOut(new PointF(69, nRow - .6f), oPage.RefModulo.TotIvaDaAssolvereImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(69, nRow + .6f), "+ecc" + oPage.RefModulo.TotImponibileISIOmaggiEccedenti_DaAssolvere.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);
                }
                else
                    PrintOut(new PointF(69, nRow), oPage.RefModulo.TotIvaDaAssolvereImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(84, nRow), oPage.RefModulo.TotIvaDaAssolvereImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IVA
                if (oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_DaAssolvere > 0)
                {
                    PrintOut(new PointF(99, nRow - .6f), oPage.RefModulo.TotIvaDaAssolvereImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(99, nRow + .6f), "+ecc" + oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_DaAssolvere.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);

                }
                else
                    PrintOut(new PointF(99, nRow), oPage.RefModulo.TotIvaDaAssolvereImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IVA LORDA
                PrintOut(new PointF(114, nRow), oPage.RefModulo.TotIvaDaAssolvereIVALorda.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // ANNULLI
                PrintOut(new PointF(127, nRow), oPage.RefModulo.TotIvaDaAssolvereNumeroTitoliAnnullati.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
            }

            nRow = 50.2F;

            // RIGHE BIGLIETTI CON IVA PREASSOLTA
            foreach (System.Collections.DictionaryEntry ModuloC1rowDict in oPage.RowsIvaPreAssolta)
            {
                clsModuloC1row oModuloC1row = (clsModuloC1row)ModuloC1rowDict.Value;

                if (string.IsNullOrEmpty(oModuloC1row.TitoloIvaPreassolta) || string.IsNullOrWhiteSpace(oModuloC1row.TitoloIvaPreassolta))
                    continue;

                // SETTORE
                PrintOut(new PointF(3, nRow), oModuloC1row.Settore, oDoc, oFontValues, nSizeFontValues);
                // CAPIENZA
                PrintOut(new PointF(9, nRow), oModuloC1row.Capiena.ToString("#,##0").PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                // TIPO TITOLO
                PrintOut(new PointF(18.5F, nRow), oModuloC1row.TipoTitolo, oDoc, oFontValues, nSizeFontValues);
                // PREZZO UNITARIO
                PrintOut(new PointF(23.6F, nRow), oModuloC1row.PrezzoUnitario.ToString("#,##0.00").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // QTA EMESSI
                PrintOut(new PointF(30.5F, nRow), oModuloC1row.NumeroTitoliEmessi.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // INCASSO LORDO
                PrintOut(new PointF(39, nRow), oModuloC1row.IncassoLordo.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // INCASSO PREVENDITA
                PrintOut(new PointF(54, nRow), oModuloC1row.IncassoPrevendita.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(69, nRow), oModuloC1row.ImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(84, nRow), oModuloC1row.ImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IVA
                PrintOut(new PointF(99, nRow), oModuloC1row.InponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IVA LORDA
                PrintOut(new PointF(114, nRow), oModuloC1row.IVALorda.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // ANNULLI
                PrintOut(new PointF(127, nRow), oModuloC1row.NumeroTitoliAnnullati.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);

                if (TipiTitoloOmaggio.Contains(oModuloC1row.TipoTitolo) && (oModuloC1row.IVALorda > 0 || oModuloC1row.ImpostaIntrattenimenti > 0))
                {
                    PrintOut(new PointF(136, nRow), "Ecc.", oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                    if (oModuloC1row.RifOmaggioEccedente != null)
                    {
                        if (nRow > 50.2F)
                        {
                            PrintLine(new PointF(2, nRow - 1.1F), new PointF(135, nRow - 1.1F), oDoc);
                        }
                        nRow += 1F;

                        // SETTORE
                        PrintOut(new PointF(3.68F, nRow), oModuloC1row.RifOmaggioEccedente.Settore, oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // TIPO TITOLO
                        PrintOut(new PointF(19.15F, nRow), oModuloC1row.RifOmaggioEccedente.TipoTitolo, oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // PREZZO UNITARIO
                        PrintOut(new PointF(24.4F, nRow), oModuloC1row.RifOmaggioEccedente.PrezzoUnitario.ToString("#,##0.00").PadLeft(10, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // QTA ECCEDENTE
                        //PrintOut(new PointF(32F, nRow), "Ecc." + oModuloC1row.QtaOmaggiEccedenti.ToString("#,##0").PadLeft(5, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        PrintOut(new PointF(32F, nRow), "Ecc." + System.Math.Max(oModuloC1row.QtaOmaggiEccedentiIVA, oModuloC1row.QtaOmaggiEccedentiISI).ToString("#,##0").PadLeft(5, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        // Indicazione riferimento
                        PrintOut(new PointF(39F, nRow), "<- rif. eccedenza ->", oDoc, oFontValues, nSizeFontValuesRifOmaggi);

                        if (oPage.RefModulo.SpettacoloIntrattenimento == "I")
                        {
                            // IMPONIBILE IMPOSTA INTRATTENIMENTI
                            PrintOut(new PointF(67.2F, nRow), "Impon.Int.rif." + oModuloC1row.RifOmaggioEccedente.ImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                            // IMPOSTA INTRATTENIMENTI
                            PrintOut(new PointF(82.2F, nRow), "Impos.Int.rif." + (oModuloC1row.RifOmaggioEccedente.ImpostaIntrattenimenti.ToString("#,##0.00").Trim() + " x" + oModuloC1row.QtaOmaggiEccedentiISI.ToString().Trim()).PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        }


                        // IMPONIBILE IVA
                        //PrintOut(new PointF(103.8F, nRow), "Imponib.IVA rif." + oModuloC1row.RifOmaggioEccedente.InponibileIVA.ToString("#,##0.00").PadLeft(12, '#'), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        if (oModuloC1row.RifOmaggioEccedente.IVALorda > 0)
                        {
                            PrintOut(new PointF(97.2F, nRow), "Impon.IVA rif." + oModuloC1row.RifOmaggioEccedente.InponibileIVA.ToString("#,##0.00").PadLeft(9, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                            // IVA LORDA
                            PrintOut(new PointF(112.2F, nRow), "IVA di rif." + (oModuloC1row.RifOmaggioEccedente.IVALorda.ToString("#,##0.00") + " x" + oModuloC1row.QtaOmaggiEccedentiIVA.ToString().Trim()).PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValuesRifOmaggi);
                        }
                        nRow += .45F;
                        if (nRow < 64)
                        {
                            PrintLine(new PointF(2, nRow), new PointF(135, nRow), oDoc);
                        }
                    }
                }

                nRow += 1.45F;
            }

            nRow = 67;

            // Eventuali totali IVA preassolta
            if (oPage.EventPage == oPage.RefModulo.PagesBiglietti.Count)
            {
                // QTA EMESSI
                PrintOut(new PointF(30.5F, nRow), oPage.RefModulo.TotIvaPreAssoltaNumeroTitoliEmessi.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // INCASSO LORDO
                PrintOut(new PointF(39, nRow), oPage.RefModulo.TotIvaPreAssoltaIncassoLordo.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // INCASSO PREVENDITA
                PrintOut(new PointF(54, nRow), oPage.RefModulo.TotIvaPreAssoltaIncassoPrevendita.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPONIBILE IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(69, nRow), oPage.RefModulo.TotIvaPreAssoltaImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                if (oPage.RefModulo.TotImponibileISIOmaggiEccedenti_Preassolta > 0)
                {
                    PrintOut(new PointF(69, nRow - .6f), oPage.RefModulo.TotIvaPreAssoltaImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(69, nRow + .6f), "+ecc" + oPage.RefModulo.TotImponibileISIOmaggiEccedenti_Preassolta.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);
                }
                else
                    PrintOut(new PointF(69, nRow), oPage.RefModulo.TotIvaPreAssoltaImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(84, nRow), oPage.RefModulo.TotIvaPreAssoltaImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPONIBILE IVA
                if (oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_Preassolta > 0)
                {
                    PrintOut(new PointF(99, nRow - .6f), oPage.RefModulo.TotIvaPreAssoltaImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(99, nRow + .6f), "+ecc." + oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_Preassolta.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);
                }
                else
                    PrintOut(new PointF(99, nRow), oPage.RefModulo.TotIvaPreAssoltaImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IVA LORDA
                PrintOut(new PointF(114, nRow), oPage.RefModulo.TotIvaPreAssoltaIVALorda.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // ANNULLI
                PrintOut(new PointF(127, nRow), oPage.RefModulo.TotIvaPreAssoltaNumeroTitoliAnnullati.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
            }

            nRow = 70;

            // Eventuali totali 
            if (oPage.EventPage == oPage.RefModulo.PagesBiglietti.Count)
            {
                // QTA EMESSI
                PrintOut(new PointF(30.5F, nRow), oPage.RefModulo.TotEventoNumeroTitoliEmessi.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
                // INCASSO LORDO
                PrintOut(new PointF(39, nRow), oPage.RefModulo.TotEventoIncassoLordo.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // INCASSO PREVENDITA
                PrintOut(new PointF(54, nRow), oPage.RefModulo.TotEventoIncassoPrevendita.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IMPONIBILE IMPOSTA INTRATTENIMENTI
                if (oPage.RefModulo.TotImponibileISIOmaggiEccedenti_Totale > 0)
                {
                    PrintOut(new PointF(69, nRow - .4f), oPage.RefModulo.TotEventoImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(69, nRow + .8f), "+ecc" + oPage.RefModulo.TotImponibileISIOmaggiEccedenti_Totale.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);
                    PrintOut(new PointF(69, nRow + 2.2f), (oPage.RefModulo.TotEventoImponibileImpostaIntrattenimenti + oPage.RefModulo.TotImponibileISIOmaggiEccedenti_Totale).ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                }
                else
                    PrintOut(new PointF(69, nRow), oPage.RefModulo.TotEventoImponibileImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPOSTA INTRATTENIMENTI
                PrintOut(new PointF(84, nRow), oPage.RefModulo.TotEventoImpostaIntrattenimenti.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);

                // IMPONIBILE IVA
                if (oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_Totale > 0)
                {
                    PrintOut(new PointF(99, nRow - .4f), oPage.RefModulo.TotEventoImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(99, nRow + .8f), "+ecc" + oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_Totale.ToString("#,##0.00").PadLeft(11), oDoc, oFontValues, nSizeFontValuesTOTRifOmaggi);
                    PrintOut(new PointF(99, nRow + 2.2f), (oPage.RefModulo.TotEventoImponibileIVA + oPage.RefModulo.TotImponibileIVAOmaggiEccedenti_Totale).ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                }
                else
                    PrintOut(new PointF(99, nRow), oPage.RefModulo.TotEventoImponibileIVA.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // IVA LORDA
                PrintOut(new PointF(114, nRow), oPage.RefModulo.TotEventoIVALorda.ToString("#,##0.00").PadLeft(12), oDoc, oFontValues, nSizeFontValues);
                // ANNULLI
                PrintOut(new PointF(127, nRow), oPage.RefModulo.TotEventoNumeroTitoliAnnullati.ToString("#,##0").PadLeft(7), oDoc, oFontValues, nSizeFontValues);
            }

            PrintOut(new PointF(15, 73.5F), "SIAE", oDoc, oFontSIAE, nSizeFontSIAE);
            PrintOut(new PointF(12.5F, 74.5F), "Timbro e Firma", oDoc, oFontTimbroFirma, nSizeTimbroFirma);
            oDoc.Writer.DirectContent.Arc(70, 65, 150, 33, 0, 360);
            oDoc.Writer.DirectContent.Stroke();

            PrintOut(new PointF(29, 73.5F), "SIAE DI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(35, 73.7F), new PointF(52, 73.7F), oDoc);

            PrintOut(new PointF(60, 73.5F), "TITOLARE SISTEMA DI EMISSIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(60, 73.7F), new PointF(80.3F, 73.7F), oDoc);
            PrintOut(new PointF(65.5F, 74.5F), "ORGANIZZATORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            //PrintOut(new PointF(0, 0), "FIRMA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(99, 73.5F), "FIRMA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(103, 73.7F), new PointF(127, 73.7F), oDoc);



            // NUMERO DI PAGINA IN FONDO
            long nTotPagesEvento = oPage.RefModulo.PagesBiglietti.Count;

            if (oPage.RefModulo.PagesAltriProventiEvento != null && oPage.RefModulo.PagesAltriProventiEvento.Count > 0)
            {
                nTotPagesEvento = oPage.RefModulo.PagesBiglietti.Count + oPage.RefModulo.PagesAltriProventiEvento.Count;
            }

            PrintOut(new PointF(3, 78), "Numero pagina per questo evento " + oPage.EventPage.ToString() + " di " + nTotPagesEvento.ToString(), oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            //if (oPage.EventPage < oPage.RefModulo.PagesBiglietti.Count)
            if (oPage.EventPage < nTotPagesEvento)
            {
                PrintOut(new PointF(113, 78), "Questo evento prosegue a pagina " + (oPage.EventPage + 1).ToString() + " >>", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmall);
            }
        }

        // Stampa modulo Altri Proventi
        private static void PrintPageAltriProventiEvento(DateTime Giorno, string GiornalieroMensile, Int64 Progressivo, clsModuloC1AltriProventipage oPage, clsPdfDocument oDoc, Int64 nTotPagine, out Exception oError, string cNomeFileRiepilogo)
        {
            oError = null;
            // Se non è la primissima pagina del report ==> Nuova pagina
            if (oPage.AbsoluteStartPage > 1)
            {
                oDoc.DocPdf.NewPage();
                oDoc.Writer.DirectContent.SetLineWidth(.1f);
            }

            //PrintGridPage(oDoc);

            // LAYOUT FISSO
            iTextSharp.text.pdf.BaseFont oFontLabelsSmall = GetFont("cour");
            iTextSharp.text.pdf.BaseFont oFontLabelsSmallBold = GetFont("courbd");
            float nSizeFontLabelsSmallBold = 7;
            iTextSharp.text.pdf.BaseFont oFontValues = GetFont("cour");
            float nSizeFontValues = 10;
            iTextSharp.text.pdf.BaseFont oFontValuesBold = GetFont("courbd");
            iTextSharp.text.pdf.BaseFont oFontTotalePagina = GetFont("courbd");
            iTextSharp.text.pdf.BaseFont oFontSIAE = GetFont("cour");
            iTextSharp.text.pdf.BaseFont oFontTimbroFirma = GetFont("cour");
            float nSizeTimbroFirma = 6;

            // LAYOUT FISSO SCRITTE

            PrintOut(new PointF(110, .7F), "Pag. " + oPage.AbsoluteStartPage.ToString() + " di " + nTotPagine.ToString(), oDoc, oFontValuesBold, nSizeFontValues);

            bool lAltriProventiGenerici = string.IsNullOrEmpty(oPage.RefModulo.CodiceLocale);
            bool lAltriProventiGenericiGiorno = oPage.RefModulo.GiornalieroMensile == "D" || oPage.RefModulo.GiornalieroMensile == "G";

            if (lAltriProventiGenerici)
            {
                if (lAltriProventiGenericiGiorno)
                {
                    // RIEPILOGO MENSILE PROVENTI NON COLLEGATI A SINGOLI EVENTI
                    PrintOut(new PointF(2, 1), "QUADRO C - PROVENTI CONSEGUITI IN OCCASIONE DEL GIORNO", oDoc, oFontValuesBold, nSizeFontValues);
                    PrintOut(new PointF(101, 1.7F), "Pagina Quadro C per questo giorno " + oPage.AltriProventiPage.ToString() + " di " + oPage.RefModulo.PagesAltriProventiEvento.Count.ToString(), oDoc, oFontTimbroFirma, nSizeTimbroFirma);
                }
                else
                {
                    PrintOut(new PointF(2, 1), "QUADRO C - PROVENTI CONSEGUITI NEL MESE", oDoc, oFontValuesBold, nSizeFontValues);
                    PrintOut(new PointF(101, 1.7F), "Pagina Quadro C per questo mese " + oPage.AltriProventiPage.ToString() + " di " + oPage.RefModulo.PagesAltriProventiEvento.Count.ToString(), oDoc, oFontTimbroFirma, nSizeTimbroFirma);
                }
            }
            else
            {
                PrintOut(new PointF(2, 1), "QUADRO C - PROVENTI CONSEGUITI IN OCCASIONE DELL'EVENTO", oDoc, oFontValuesBold, nSizeFontValues);
                PrintOut(new PointF(101, 1.7F), "Pagina Quadro C per questo evento " + oPage.AltriProventiPage.ToString() + " di " + oPage.RefModulo.PagesAltriProventiEvento.Count.ToString(), oDoc, oFontTimbroFirma, nSizeTimbroFirma);
            }


            PrintOut(new PointF(2, 4), "Organizzatore", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 4), "Codice Fiscale", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 6), "Titolare del sistema di emissione", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 6), "Codice Fiscale", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 8), "Codice sistema di emissione", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 10), "TITOLO DELL'OPERA/MANIFESTAZIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            if (!lAltriProventiGenerici)
            {
                PrintOut(new PointF(2, 12), "CODICE LOCALE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
                PrintOut(new PointF(50, 12), "DATA EVENTO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
                PrintOut(new PointF(80, 12), "ORA INIZIO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            }

            PrintOut(new PointF(87, 70), "Gli importi sono espressi in EURO", oDoc, oFontTimbroFirma, nSizeTimbroFirma);

            //PrintOut(new PointF(2, 15), "Quadro B - Abbonamenti", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintLine(new PointF(2, 15.5F), new PointF(125, 15.5F), oDoc);
            PrintLine(new PointF(2, 18.5F), new PointF(125, 18.5F), oDoc);
            PrintLine(new PointF(2, 65), new PointF(125, 65), oDoc);
            PrintLine(new PointF(50, 68), new PointF(125, 68), oDoc);

            PrintLine(new PointF(2, 15.5F), new PointF(2, 65), oDoc);
            PrintLine(new PointF(50, 15.5F), new PointF(50, 68), oDoc);
            PrintLine(new PointF(65, 15.5F), new PointF(65, 68), oDoc);
            PrintLine(new PointF(80, 15.5F), new PointF(80, 68), oDoc);
            PrintLine(new PointF(95, 15.5F), new PointF(95, 68), oDoc);
            PrintLine(new PointF(110, 15.5F), new PointF(110, 68), oDoc);
            PrintLine(new PointF(125, 15.5F), new PointF(125, 68), oDoc);

            PrintOut(new PointF(3, 17.5F), "Tipo provento (1)", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(53, 17), "Importo Lordo", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(66, 17), "Imponibile Imposta", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(67, 18), "Intrattenimento", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(85, 17), "Imposta", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(82, 18), "Intrattenimenti", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(98, 17), "Imponibile  IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(114, 17), "IVA  Lorda", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(45, 67), "Totali", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            // Riempimento

            // DENOMINAZIONE ORGANIZZATORE
            PrintOut(new PointF(26, 4), oPage.RefModulo.DenominazioneOrganizzatore, oDoc, oFontValuesBold, nSizeFontValues);
            // CODICE FISCALE ORGANIZZATORE
            PrintOut(new PointF(100, 4), oPage.RefModulo.CodiceFiscaleOrganizzatore, oDoc, oFontValuesBold, nSizeFontValues);

            // DENOMINAZIONE TITOLARE
            PrintOut(new PointF(26, 6), oPage.RefModulo.DenominazioneTitolare, oDoc, oFontValuesBold, nSizeFontValues);
            // CODICE FISCALE TITOLARE
            PrintOut(new PointF(100, 6), oPage.RefModulo.CodiceFiscaleTitolare, oDoc, oFontValuesBold, nSizeFontValues);

            // CODICE SISTEMA DI EMISSIONE
            PrintOut(new PointF(26, 8), oPage.RefModulo.CodiceSistema, oDoc, oFontValuesBold, nSizeFontValues);

            if (lAltriProventiGenerici)
            {
                // GIORNO
                if (lAltriProventiGenericiGiorno)
                    PrintOut(new PointF(60, 12), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "G"), oDoc, oFontValuesBold, nSizeFontValues);
                else
                    PrintOut(new PointF(60, 12), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "M"), oDoc, oFontValuesBold, nSizeFontValues);
            }
            else
            {
                // TITOLO EVENTO
                PrintOut(new PointF(26, 10), oPage.RefModulo.TitoloEvento, oDoc, oFontValuesBold, nSizeFontValues);
                // CODICE_LOCALE
                PrintOut(new PointF(26, 12), oPage.RefModulo.CodiceLocale, oDoc, oFontValuesBold, nSizeFontValues);
                // DATA EVENTO
                PrintOut(new PointF(60, 12), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "G"), oDoc, oFontValuesBold, nSizeFontValues);
                // ORA EVENTO
                PrintOut(new PointF(88, 12), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataOraEvento, "H"), oDoc, oFontValuesBold, nSizeFontValues);
            }

            float nRow = 21;

            bool lAltriProventiVuoti = false;
            try
            {
                bool lFindProventiValorizzati = false;
                foreach (System.Collections.DictionaryEntry ModuloC1rowAPDict in oPage.RowsAltriProventi)
                {
                    clsModuloC1rowAltriProventi oModuloC1rowAP = (clsModuloC1rowAltriProventi)ModuloC1rowAPDict.Value;
                    if (oModuloC1rowAP.TipoProvento.Trim() != "" ||
                        oModuloC1rowAP.Corrispettivo > 0 ||
                        oModuloC1rowAP.ImponibileImposta > 0 ||
                        oModuloC1rowAP.ImpostaIntra > 0 ||
                        oModuloC1rowAP.IVA > 0)
                    {
                        lFindProventiValorizzati = true;
                        break;
                    }
                }
                if (!lFindProventiValorizzati &&
                    oPage.RefModulo.TotAltriProventiCorrispettivo == 0 &&
                    oPage.RefModulo.TotAltriProventiImponibileImposta == 0 &&
                    oPage.RefModulo.TotAltriProventiImpostaIntra == 0 &&
                    oPage.RefModulo.TotAltriProventiIVA == 0)
                {
                    lAltriProventiVuoti = true;
                }
            }
            catch (Exception)
            {
            }
            if (lAltriProventiVuoti)
            {
                oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.GRAY);
                oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.GRAY);
                PrintOut(new PointF(15, 39.5F), "NESSUN PROVENTO", oDoc, oFontValuesBold, 60);
                oDoc.Writer.DirectContent.SetColorFill(iTextSharp.text.BaseColor.BLACK);
                oDoc.Writer.DirectContent.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
            }
            else
            {
                foreach (System.Collections.DictionaryEntry ModuloC1rowAPDict in oPage.RowsAltriProventi)
                {
                    clsModuloC1rowAltriProventi oModuloC1rowAP = (clsModuloC1rowAltriProventi)ModuloC1rowAPDict.Value;

                    string cDesTipoProvento = oModuloC1rowAP.TipoProvento;

                    if (oModuloC1rowAP.DescTipoProvento.Trim() != "")
                    {
                        cDesTipoProvento += " " + oModuloC1rowAP.DescTipoProvento.Trim();
                    }
                    //if (lAltriProventiGenerici)
                    //    cDesTipoProvento += "\r\n" + oModuloC1rowAP.TipoGenerePrevalente + " " + oModuloC1rowAP.DescTipoGenerePrevalente;

                    PrintOut(new PointF(3, nRow), (cDesTipoProvento.Length > 46 ? cDesTipoProvento.Substring(0, 46) : cDesTipoProvento), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(51, nRow), oModuloC1rowAP.Corrispettivo.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(66, nRow), oModuloC1rowAP.ImponibileImposta.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(81, nRow), oModuloC1rowAP.ImpostaIntra.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(96, nRow), (oModuloC1rowAP.Corrispettivo - oModuloC1rowAP.IVA).ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(111, nRow), oModuloC1rowAP.IVA.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                    nRow += 1.3F;

                    if (cDesTipoProvento.Length > 46)
                    {
                        PrintOut(new PointF(3, nRow), " " + cDesTipoProvento.Substring(46), oDoc, oFontValues, nSizeFontValues);
                        nRow += 1.3F;
                    }

                    if (lAltriProventiGenerici)
                    {
                        string cDesGenerePrevalente = "Genere prevalente " + oModuloC1rowAP.TipoGenerePrevalente + " " + oModuloC1rowAP.DescTipoGenerePrevalente;
                        PrintOut(new PointF(3, nRow), (cDesGenerePrevalente.Length > 46 ? cDesGenerePrevalente.Substring(0, 46) : cDesGenerePrevalente), oDoc, oFontValues, nSizeFontValues);
                        nRow += 1.3F;
                        if (cDesGenerePrevalente.Length > 46)
                        {
                            PrintOut(new PointF(3, nRow), " " + cDesGenerePrevalente.Substring(46), oDoc, oFontValues, nSizeFontValues);
                            nRow += 1.3F;
                        }
                    }
                }
            }
            // Eventuali totali 
            if (oPage.AltriProventiPage == oPage.RefModulo.PagesAltriProventiEvento.Count)
            {
                nRow = 67;
                PrintOut(new PointF(51, nRow), oPage.RefModulo.TotAltriProventiCorrispettivo.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(66, nRow), oPage.RefModulo.TotAltriProventiImponibileImposta.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(81, nRow), oPage.RefModulo.TotAltriProventiImpostaIntra.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(96, nRow), (oPage.RefModulo.TotAltriProventiCorrispettivo - oPage.RefModulo.TotAltriProventiIVA).ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(111, nRow), oPage.RefModulo.TotAltriProventiIVA.ToString("#,##0.00").PadLeft(12, ' '), oDoc, oFontValues, nSizeFontValues);
            }

            //PrintOut(new PointF(15, 73.5F), "SIAE", oDoc, oFontSIAE, nSizeFontSIAE);
            //PrintOut(new PointF(12.5F, 74.5F), "Timbro e Firma", oDoc, oFontTimbroFirma, nSizeTimbroFirma);
            //oDoc.Writer.DirectContent.Arc(70, 65, 150, 33, 0, 360);
            //oDoc.Writer.DirectContent.Stroke();

            //PrintOut(new PointF(29, 73.5F), "SIAE DI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            //PrintLine(new PointF(35, 73.7F), new PointF(52, 73.7F), oDoc);

            PrintOut(new PointF(60, 73.5F), "TITOLARE SISTEMA DI EMISSIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(60, 73.7F), new PointF(81, 73.7F), oDoc);
            PrintOut(new PointF(65.5F, 74.5F), "ORGANIZZATORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(99, 73.5F), "FIRMA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(103, 73.7F), new PointF(125, 73.7F), oDoc);

            PrintOut(new PointF(113, 75), "Allegato C.1 - retro", oDoc, oFontTimbroFirma, nSizeTimbroFirma);

            long nTotPagesEvento = oPage.RefModulo.PagesAltriProventiEvento.Count;
            long nStartPageAltriProventiEvento = 0;
            if (oPage.RefModulo.PagesBiglietti != null && oPage.RefModulo.PagesBiglietti.Count > 0)
            {
                nStartPageAltriProventiEvento = oPage.RefModulo.PagesBiglietti.Count;
                nTotPagesEvento += oPage.RefModulo.PagesBiglietti.Count;
            }
            long nCurrentPage = nStartPageAltriProventiEvento + oPage.AltriProventiPage;
            PrintOut(new PointF(3, 78), "Numero pagina per questo evento " + nCurrentPage.ToString() + " di " + nTotPagesEvento.ToString(), oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
        }

        // Stampa modulo pagina abbonamenti
        private static void PrintPageAbbonamenti(DateTime Giorno, string GiornalieroMensile, Int64 Progressivo, clsModuloC2page oPage, clsPdfDocument oDoc, Int64 nTotPagine, out Exception oError, string cNomeFileRiepilogo)
        {
            oError = null;
            // Se non è la primissima pagina del report ==> Nuova pagina
            if (oPage.AbsoluteStartPage > 1)
            {
                oDoc.DocPdf.NewPage();
                oDoc.Writer.DirectContent.SetLineWidth(.1f);
            }

            //PrintGridPage(oDoc);

            // LAYOUT FISSO
            iTextSharp.text.pdf.BaseFont oFontLabelsSmall = GetFont("cour");
            iTextSharp.text.pdf.BaseFont oFontLabelsSmallBold = GetFont("courbd");
            float nSizeFontLabelsSmallBold = 7;
            iTextSharp.text.pdf.BaseFont oFontValues = GetFont("cour");
            float nSizeFontValues = 10;
            iTextSharp.text.pdf.BaseFont oFontValuesBold = GetFont("courbd");
            iTextSharp.text.pdf.BaseFont oFontTotalePagina = GetFont("courbd");
            iTextSharp.text.pdf.BaseFont oFontSIAE = GetFont("cour");
            float nSizeFontSIAE = 8;
            iTextSharp.text.pdf.BaseFont oFontTimbroFirma = GetFont("cour");
            float nSizeTimbroFirma = 6;

            // LAYOUT FISSO SCRITTE

            PrintOut(new PointF(110, .7F), "Pag. " + oPage.AbsoluteStartPage.ToString() + " di " + nTotPagine.ToString(), oDoc, oFontValuesBold, nSizeFontValues);

            PrintOut(new PointF(2, 1), "RIEPILOGO ABBONAMENTI", oDoc, oFontValuesBold, nSizeFontValues);
            PrintOut(new PointF(50, 1), "ALLEGATO C.2 fronte", oDoc, oFontValuesBold, nSizeFontValues);

            PrintOut(new PointF(110, 1.7F), "Pagina All.C2 Abbonamenti " + oPage.AbbonPage.ToString() + " di " + oPage.RefModulo.PagesAbbonamenti.Count.ToString(), oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 4), "Riepilogo giornaliero del", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(50, 4), "Riepilogo mensile del", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(90, 4), "Trasmetto in data", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            if (!string.IsNullOrEmpty(cNomeFileRiepilogo) && !string.IsNullOrWhiteSpace(cNomeFileRiepilogo))
            {
                PrintOut(new PointF(2, 2.8F), cNomeFileRiepilogo, oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            }

            PrintOut(new PointF(2, 9), "Organizzatore", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 9), "Codice Fiscale", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 11), "Titolare del sistema di emissione", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(87, 11), "Codice Fiscale", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 13), "Codice sistema di emissione", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(80, 13), "Gli importi sono espressi in EURO", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(2, 15), "Quadro B - Abbonamenti", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintLine(new PointF(2, 15.5F), new PointF(137, 15.5F), oDoc);
            PrintLine(new PointF(2, 18.5F), new PointF(137, 18.5F), oDoc);
            PrintLine(new PointF(2, 68), new PointF(137, 68), oDoc);

            PrintLine(new PointF(2, 15.5F), new PointF(2, 68), oDoc);
            PrintLine(new PointF(36, 15.5F), new PointF(36, 68), oDoc);
            PrintLine(new PointF(46, 15.5F), new PointF(46, 68), oDoc);
            PrintLine(new PointF(63, 15.5F), new PointF(63, 68), oDoc);
            PrintLine(new PointF(71, 15.5F), new PointF(71, 68), oDoc);
            PrintLine(new PointF(78, 15.5F), new PointF(78, 68), oDoc);
            PrintLine(new PointF(90, 15.5F), new PointF(90, 68), oDoc);
            PrintLine(new PointF(99, 15.5F), new PointF(99, 68), oDoc);
            PrintLine(new PointF(105, 15.5F), new PointF(105, 68), oDoc);
            PrintLine(new PointF(117, 15.5F), new PointF(117, 68), oDoc);
            PrintLine(new PointF(137, 15.5F), new PointF(137, 68), oDoc);
            //PrintLine(new PointF(2, nBottomTotali), new PointF(137, nBottomTotali), oDoc);

            PrintOut(new PointF(3, 17.5F), "Tipo titolo", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(38, 17), "Codice", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(37, 18), "Abbonamento", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(47, 17), "Intrattenimento", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(48, 18), "Spettacolo", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(64.2F, 17), "Fisso", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(64, 18), "Libero", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(72.3F, 17), "Numero", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(72, 18), "Venduti", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(79, 17), "Importo Lordo", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(79, 18), "Incassato", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(91, 17), "Abbonamenti", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(91, 18), "Annullati", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(100, 17), "Numero", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintOut(new PointF(100, 18), "Eventi", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(106, 17), "IVA", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            PrintOut(new PointF(126, 17), "Netto", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

            // Riempimento

            // FLAG GIORNALIERO MENSILE
            if (GiornalieroMensile == "G")
            {
                PrintOut(new PointF(21, 4), clsModuloC1C2.GetDescGiornoMese(Giorno, GiornalieroMensile), oDoc, oFontValuesBold, nSizeFontValues);
            }
            else if (GiornalieroMensile == "M" || GiornalieroMensile == "D")
            {
                PrintOut(new PointF(66, 4), clsModuloC1C2.GetDescGiornoMese(Giorno, GiornalieroMensile), oDoc, oFontValuesBold, nSizeFontValues);
            }

            // TRASMESSO IN DATA
            PrintOut(new PointF(103, 4), clsModuloC1C2.GetDescGiornoMese(oPage.RefModulo.DataIns, "G"), oDoc, oFontValuesBold, nSizeFontValues);

            // DENOMINAZIONE ORGANIZZATORE
            PrintOut(new PointF(26, 9), oPage.RefModulo.DenominazioneOrganizzatore, oDoc, oFontValuesBold, nSizeFontValues);
            // CODICE FISCALE ORGANIZZATORE
            PrintOut(new PointF(100, 9), oPage.RefModulo.CodiceFiscaleOrganizzatore, oDoc, oFontValuesBold, nSizeFontValues);

            // DENOMINAZIONE TITOLARE
            PrintOut(new PointF(26, 11), oPage.RefModulo.DenominazioneTitolare, oDoc, oFontValuesBold, nSizeFontValues);
            // CODICE FISCALE TITOLARE
            PrintOut(new PointF(100, 11), oPage.RefModulo.CodiceFiscaleTitolare, oDoc, oFontValuesBold, nSizeFontValues);

            // CODICE SISTEMA DI EMISSIONE
            PrintOut(new PointF(26, 13), oPage.RefModulo.CodiceSistema, oDoc, oFontValuesBold, nSizeFontValues);

            float nRow = 21;

            foreach (System.Collections.DictionaryEntry ModuloC2rowDict in oPage.RowsAbbonamenti)
            {
                clsModuloC2row oModuloC2row = (clsModuloC2row)ModuloC2rowDict.Value;

                string cDescTipoTitolo = oModuloC2row.TipoTitolo;

                if (oModuloC2row.DescTipoTitolo.Trim() != "")
                {
                    cDescTipoTitolo += " " + oModuloC2row.DescTipoTitolo.Trim();
                    if (cDescTipoTitolo.Length > 32)
                    {
                        cDescTipoTitolo = cDescTipoTitolo.Substring(0, 32);
                    }
                }

                PrintOut(new PointF(3, nRow), cDescTipoTitolo, oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(37, nRow), oModuloC2row.CodiceAbbonamento, oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(47, nRow), (oModuloC2row.SpettacoloIntrattenimento == "S" ? "Spettacolo" : "Intrattenimento"), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(64, nRow), (oModuloC2row.TipoTurno == "L" ? "Libero" : "Fisso"), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(72, nRow), oModuloC2row.QtaEmessi.ToString().PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(79, nRow), oModuloC2row.IncassoLordo.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(93, nRow), oModuloC2row.QtaAnnullati.ToString().PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(99, nRow), oModuloC2row.NumeroEventiAbilitati.ToString().PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                if (oModuloC2row.TitoloIvaPreassolta == "F")
                    PrintOut(new PointF(106, nRow), ("(" + oModuloC2row.IVALorda.ToString("#,##0.00").Trim() + ")").PadLeft(11), oDoc, oFontValues, nSizeFontValues);
                else
                    PrintOut(new PointF(106, nRow), oModuloC2row.IVALorda.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(126, nRow), oModuloC2row.Netto.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);
                nRow += 1.3F;
                if (oModuloC2row.TitoloIvaPreassolta == "F")
                {
                    PrintOut(new PointF(4, nRow), "(certificato tramite fattura)", oDoc, oFontValues, nSizeFontValues);
                    nRow += 1.3F;
                }
            }

            // Eventuali totali 
            if (oPage.AbbonPage == oPage.RefModulo.PagesAbbonamenti.Count)
            {
                PrintOut(new PointF(72, 70), oPage.RefModulo.TotAbbonamentiEmessi.ToString().PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(79, 70), oPage.RefModulo.TotAbbonamentiIncassoLordo.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(93, 70), oPage.RefModulo.TotAbbonamentiAnnullati.ToString().PadLeft(5), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(106, 70), oPage.RefModulo.TotAbbonamentiIVALorda.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);
                PrintOut(new PointF(126, 70), oPage.RefModulo.TotAbbonamentiNetto.ToString("#,##0.00").PadLeft(10), oDoc, oFontValues, nSizeFontValues);

                if (oPage.RefModulo.TotAbbonamentiIVAPreassolta > 0)
                {
                    PrintOut(new PointF(88, 72), "IVA Certif. Fatt.", oDoc, oFontValues, nSizeFontValues);
                    PrintOut(new PointF(106, 72), ("(" + oPage.RefModulo.TotAbbonamentiIVAPreassolta.ToString("#,##0.00").Trim() + ")").PadLeft(11), oDoc, oFontValues, nSizeFontValues);
                }
            }

            PrintOut(new PointF(15, 73.5F), "SIAE", oDoc, oFontSIAE, nSizeFontSIAE);
            PrintOut(new PointF(12.5F, 74.5F), "Timbro e Firma", oDoc, oFontTimbroFirma, nSizeTimbroFirma);
            oDoc.Writer.DirectContent.Arc(70, 65, 150, 33, 0, 360);
            oDoc.Writer.DirectContent.Stroke();

            PrintOut(new PointF(29, 73.5F), "SIAE DI", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(35, 73.7F), new PointF(52, 73.7F), oDoc);

            PrintOut(new PointF(65.5F, 73.5F), "ORGANIZZATORE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);
            PrintLine(new PointF(60, 73.7F), new PointF(100, 73.7F), oDoc);
            PrintOut(new PointF(60, 74.5F), "TITOLARE SISTEMA DI EMISSIONE", oDoc, oFontLabelsSmallBold, nSizeFontLabelsSmallBold);

        }

        private static float GetWidthPrintArea(clsPdfDocument oPdfDoc)
        {
            return oPdfDoc.DocPdf.PageSize.Width - oPdfDoc.DocPdf.LeftMargin - oPdfDoc.DocPdf.RightMargin;
        }

        // Dimensione verticale della pagina
        private static float GetHeightPrintArea(clsPdfDocument oPdfDoc)
        {
            return oPdfDoc.DocPdf.PageSize.Height - oPdfDoc.DocPdf.TopMargin - oPdfDoc.DocPdf.BottomMargin;
        }

        private static iTextSharp.text.pdf.PdfPCell GetDeafultCell(bool Borders)
        {
            return GetDeafultCell(Borders, "", null, iTextSharp.text.Element.ALIGN_LEFT);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDefaultCell(bool Borders, string cContent, iTextSharp.text.Font oFont)
        {
            return GetDeafultCell(Borders, cContent, oFont, iTextSharp.text.Element.ALIGN_LEFT);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDefaultCell(bool Borders, string cContent, iTextSharp.text.Font oFont, int nAlign)
        {
            return GetDeafultCell(Borders, cContent, oFont, nAlign);
        }

        private static iTextSharp.text.pdf.PdfPCell GetDeafultCell(bool Borders, string cContent, iTextSharp.text.Font oFont, int nAlign)
        {
            iTextSharp.text.pdf.PdfPCell oCell = new iTextSharp.text.pdf.PdfPCell();
            if (Borders)
            {
                oCell.Border = iTextSharp.text.Rectangle.RIGHT_BORDER | iTextSharp.text.Rectangle.LEFT_BORDER | iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER;
            }
            else
            {
                oCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            }
            oCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            oCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            oCell.PaddingBottom = 3;
            if (cContent.Trim() != "")
            {
                iTextSharp.text.Paragraph oParagraph = new iTextSharp.text.Paragraph(cContent, oFont);
                oParagraph.Alignment = nAlign;
                oParagraph.Trim();
                oCell.AddElement(oParagraph);
                oCell.Normalize();
            }
            return oCell;
        }

        


        public static bool SaveToFile(string FileName, System.IO.MemoryStream oStream)
        {
            FileStream oFileStream = System.IO.File.Create(FileName, oStream.GetBuffer().Length);
            oFileStream.Write(oStream.GetBuffer(), 0, oStream.GetBuffer().Length);
            oFileStream.Flush();
            oFileStream.Close();
            oFileStream.Dispose();
            return true;
        }

        // Stampa di una stringa
        private static void PrintOut(PointF oPoint, string cText, clsPdfDocument oDoc, iTextSharp.text.pdf.BaseFont oFont, float nSize)
        {
            oDoc.Writer.DirectContent.BeginText();
            oDoc.Writer.DirectContent.SetFontAndSize(oFont, nSize);
            oDoc.Writer.DirectContent.MoveText((oPoint.X * 6) + oDoc.DocPdf.LeftMargin, oDoc.DocPdf.PageSize.Height - (oPoint.Y * 7) - oDoc.DocPdf.TopMargin);
            oDoc.Writer.DirectContent.ShowTextKerned(cText);
            oDoc.Writer.DirectContent.EndText();
        }

        // Stampa linea
        private static void PrintLine(PointF Start, PointF Stop, clsPdfDocument oDoc)
        {
            oDoc.Writer.DirectContent.MoveTo(oDoc.DocPdf.LeftMargin + (Start.X * 6), oDoc.DocPdf.PageSize.Height - oDoc.DocPdf.TopMargin - (Start.Y * 7));
            oDoc.Writer.DirectContent.LineTo(oDoc.DocPdf.LeftMargin + (Start.X * 6), oDoc.DocPdf.PageSize.Height - oDoc.DocPdf.TopMargin - (Start.Y * 7));
            oDoc.Writer.DirectContent.LineTo(oDoc.DocPdf.LeftMargin + (Stop.X * 6), oDoc.DocPdf.PageSize.Height - oDoc.DocPdf.TopMargin - (Stop.Y * 7));
            oDoc.Writer.DirectContent.Stroke();
        }
        #endregion
    }

    public class clsPdfDocument
    {
        public iTextSharp.text.Document DocPdf = null;
        public iTextSharp.text.pdf.PdfWriter Writer = null;
        public System.IO.MemoryStream PdfStream;

        public clsPdfDocument()
        {
            this.DocPdf = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4_LANDSCAPE, 15, 15, 15, 15);
            this.PdfStream = new MemoryStream();
            //this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, new FileStream(FilePdf, FileMode.Create));
            this.Writer = iTextSharp.text.pdf.PdfWriter.GetInstance(this.DocPdf, this.PdfStream);
        }

        public void Dispose()
        {
            if (this.DocPdf != null)
            {
                this.DocPdf.Dispose();
            }
            if (this.Writer != null)
            {
                this.Writer.Dispose();
            }
            if (this.PdfStream != null)
            {
                try
                {
                    this.PdfStream.Close();
                    this.PdfStream.Dispose();
                }
                catch (Exception)
                {
                }
            }
        }
    }

    
}
