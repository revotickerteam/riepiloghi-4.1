﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RiepiloghiReadXmlToPdf
{
    public partial class frmRiepiloghiReadXmlToPdfMain : Form
    {

        public class clsButton : Button
        {
            private bool isChecked = false;
            private bool isLastRiepilogo = false;
            private int countRiepiloghi = 0;

            private bool isLastRiepilogoMouseMove = false;

            public bool IsChecked
            {
                get { return this.isChecked; }
                set { this.isChecked = value; this.Invalidate(); }
            }

            public bool IsLastRiepilogo
            {
                get { return this.isLastRiepilogo; }
                set { this.isLastRiepilogo = value; this.Invalidate(); }
            }

            public int CountRiepiloghi
            {
                get { return this.countRiepiloghi; }
                set { this.countRiepiloghi = value; this.Invalidate(); }
            }

            public bool IsLastRiepilogoMouseMove
            {
                get { return this.isLastRiepilogoMouseMove; }
                set { this.isLastRiepilogoMouseMove = value; this.Invalidate(); } 
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                base.OnPaint(e);

                if (this.isChecked)
                {
                    Point location = new Point(3, (this.ClientSize.Height - CheckBoxRenderer.GetGlyphSize(e.Graphics, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal).Height) / 2);
                    CheckBoxRenderer.DrawCheckBox(e.Graphics, location, System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal);
                }

                if (this.isLastRiepilogo && this.countRiepiloghi > 0)
                {
                    StringFormat st = new StringFormat();
                    st.Alignment = StringAlignment.Far;
                    st.LineAlignment = StringAlignment.Center;
                    string lastDescr = string.Format("+{0}", this.countRiepiloghi.ToString());
                    if (this.isLastRiepilogoMouseMove)
                    {
                        SizeF size = e.Graphics.MeasureString(lastDescr, this.Font);
                        System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                        path.AddRectangle(new RectangleF(this.ClientSize.Width - size.Width - 2, 2, size.Width, this.ClientSize.Height - 4));
                        e.Graphics.FillPath(Brushes.DarkOrange, path);
                    }
                    e.Graphics.DrawString(lastDescr, this.Font, Brushes.Black, this.ClientRectangle, st);
                }
            }

            public Rectangle GetRectIsLastRiepilogoMouseMove()
            {
                Rectangle r = new Rectangle(0, 0, 0, 0);
                if (this.isLastRiepilogo && this.countRiepiloghi > 0)
                {
                    StringFormat st = new StringFormat();
                    st.Alignment = StringAlignment.Far;
                    st.LineAlignment = StringAlignment.Center;
                    string lastDescr = string.Format("+{0}", this.countRiepiloghi.ToString());
                    Graphics g = this.CreateGraphics();
                    SizeF size = g.MeasureString(lastDescr, this.Font);
                    System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                    path.AddRectangle(new RectangleF(this.ClientSize.Width - size.Width, 2, size.Width, this.ClientSize.Height - 4));
                }
                return r;
            }
        }

        public class clsLabelProgress : Label
        {
            private long index = 0;
            private long counter = 0;

            public clsLabelProgress()
            {
                SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            }

            public long Index
            {
                get { return this.index; }
                set { this.index = value; this.Invalidate(); }
            }

            public long Counter
            {
                get { return this.counter; }
                set { this.counter = value; this.Invalidate(); }
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                base.OnPaint(e);

                if (this.index > 0 && this.counter > 0)
                {
                    System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                    float inverseValue = this.ClientSize.Width - (index * (this.ClientSize.Width - 2) / counter);
                    path.AddRectangle(new RectangleF(1, this.ClientSize.Height - 3, this.ClientSize.Width - 1 - (inverseValue), this.ClientSize.Height - 1));
                    e.Graphics.FillPath(Brushes.Orange, path);
                }
            }
        }

        private List<System.IO.FileInfo> listaFiles { get; set; }

        public List<System.IO.FileInfo> ListaFiles
        {
            get { return this.listaFiles; }
            set
            {
                this.listaFiles = value;
                this.txtFile.Text = "";
                this.btnPDF.Enabled = false;
                this.btnInverti.Enabled = false;
                this.btnClear.Enabled = this.listaFiles != null && this.listaFiles.Count > 0;
                if (this.pnlData.Controls.Count > 0)
                {
                    foreach (clsButton btn in this.pnlData.Controls)
                    {
                        btn.MouseClick -= Btn_Click;
                        btn.MouseMove -= Btn_MouseMove;
                        btn.MouseLeave -= Btn_MouseLeave;
                    }
                    this.pnlData.Controls.Clear();
                }

                if (this.listaFiles != null && this.listaFiles.Count > 0)
                {
                    foreach (System.IO.FileInfo fInfo in this.listaFiles)
                    {
                        int countRiepiloghi = 0;
                        int maxRiepilogo = 0;
                        int contatore = 0;
                        clsButton btn = new clsButton();
                        btn.Tag = fInfo;
                        btn.Height = 50;
                        string cValue = "";
                        string cTipo = FileRiepilogo.GetTipoRiepilogo(fInfo.Name);
                        int nQtaRiepiloghi = 0;
                        DateTime? dataRiepilogo = FileRiepilogo.GetDataFileRiepilogo(fInfo.Name, out cValue, out contatore);
                        if (dataRiepilogo != null)
                        {
                            btn.Text = cValue;
                            int maxContatore = 0;
                            this.listaFiles.ForEach(f => {
                                string cTipoItem = FileRiepilogo.GetTipoRiepilogo(f.Name);
                                if (cTipoItem == cTipo)
                                {
                                    string cValueItem = "";
                                    int contatoreItem = 0;
                                    DateTime? dDataItem = FileRiepilogo.GetDataFileRiepilogo(f.Name, out cValueItem, out contatoreItem);
                                    if (dDataItem != null && dDataItem == dataRiepilogo)
                                    {
                                        nQtaRiepiloghi += 1;
                                        if (contatoreItem > maxContatore)
                                            maxContatore = contatoreItem;
                                    }
                                }
                            });
                            btn.IsLastRiepilogo = (contatore == maxContatore);
                            if (btn.IsLastRiepilogo)
                            {
                                btn.CountRiepiloghi = nQtaRiepiloghi - 1;
                                btn.IsChecked = true;
                            }
                            btn.Visible = btn.IsLastRiepilogo;
                        }
                        else
                        {
                            btn.Text = fInfo.Name;
                        }
                        btn.Dock = DockStyle.Top;
                        this.pnlData.Controls.Add(btn);
                        btn.MouseClick += Btn_Click;
                        btn.MouseMove += Btn_MouseMove;
                        btn.MouseLeave += Btn_MouseLeave;
                    }
                }
                this.btnPDF.Enabled = this.pnlData.Controls.Count > 0;
                this.btnInverti.Enabled = this.pnlData.Controls.Count > 0;
            }
        }

        private void Btn_MouseLeave(object sender, EventArgs e)
        {
            clsButton btn = (clsButton)sender;
            btn.IsLastRiepilogoMouseMove = false;
        }

        private void Btn_MouseMove(object sender, MouseEventArgs e)
        {
            clsButton btn = (clsButton)sender;
            Rectangle r = btn.GetRectIsLastRiepilogoMouseMove();
            if (btn.IsLastRiepilogo && e.X >= r.Left)
                btn.IsLastRiepilogoMouseMove = true;
            else
                btn.IsLastRiepilogoMouseMove = false;
        }

        private void Btn_Click(object sender, MouseEventArgs e)
        {
            try
            {
                clsButton btn = (clsButton)sender;
                System.IO.FileInfo fInfo = (System.IO.FileInfo)btn.Tag;

                bool clickToCheck = true;

                if (btn.IsLastRiepilogo)
                {
                    if (e.X > btn.ClientSize.Width - 30)
                        clickToCheck = false;
                }

                if (clickToCheck)
                {
                    btn.IsChecked = !btn.IsChecked;
                    if (btn.IsChecked)
                    {
                        this.txtFile.Text = fInfo.FullName;
                        if (this.txtFile.Text.Length > 0)
                        {
                            this.txtFile.SelectionLength = 0;
                            this.txtFile.SelectionStart = this.txtFile.Text.Length;
                            this.txtFile.ScrollToCaret();
                        }
                    }
                }
                else
                {
                    if (btn.IsLastRiepilogo)
                    {
                        int contatore = 0;
                        string cValue = "";
                        string cTipo = FileRiepilogo.GetTipoRiepilogo(fInfo.Name);
                        DateTime? dataRiepilogo = FileRiepilogo.GetDataFileRiepilogo(fInfo.Name, out cValue, out contatore);
                        if (dataRiepilogo != null)
                        {
                            foreach (clsButton btnItem in this.pnlData.Controls)
                            {
                                if (btnItem.Equals(btn))
                                    continue;
                                System.IO.FileInfo fInfoItem = (System.IO.FileInfo)btnItem.Tag;
                                string cTipoItem = FileRiepilogo.GetTipoRiepilogo(fInfoItem.Name);
                                if (cTipoItem == cTipo)
                                {
                                    string cValueItem = "";
                                    int contatoreItem = 0;
                                    DateTime? dDataItem = FileRiepilogo.GetDataFileRiepilogo(fInfoItem.Name, out cValueItem, out contatoreItem);
                                    if (dDataItem != null && dDataItem == dataRiepilogo)
                                    {
                                        btnItem.Visible = !btnItem.Visible;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public frmRiepiloghiReadXmlToPdfMain()
        {
            InitializeComponent();
            this.Shown += FrmRiepiloghiReadXmlToPdfMain_Shown;
        }

        private void FrmRiepiloghiReadXmlToPdfMain_Shown(object sender, EventArgs e)
        {
            this.btnExit.Click += BtnExit_Click;
            this.btnPath.Click += BtnPath_Click;
            this.btnPathExtract.Click += BtnPathExtract_Click;
            this.btnFile.Click += BtnFile_Click;
            this.btnPDF.Click += BtnPDF_Click;
            this.btnClear.Click += BtnClear_Click;
            this.chkDate.CheckedChanged += ChkDate_CheckedChanged;
            this.txtFile.TextChanged += TxtFile_TextChanged;
            this.btnPDF.Enabled = false;
            this.btnClear.Enabled = false;
            this.HelpButtonClicked += FrmRiepiloghiReadXmlToPdfMain_HelpButtonClicked;
            FileRiepilogo.OutProcess += FileRiepilogo_OutProcess;
            MakeFilePdf.OutProcess += FileRiepilogo_OutProcess;
            this.btnInverti.Enabled = false;
            this.btnInverti.Click += BtnInverti_Click;
        }

        private void BtnInverti_Click(object sender, EventArgs e)
        {
            long counterChecked = 0;
            foreach (clsButton btn in this.pnlData.Controls)
            {
                if (btn.Visible)
                {
                    btn.IsChecked = (!btn.IsChecked);
                }
                else
                    btn.IsChecked = false;
            }
        }

        private void FrmRiepiloghiReadXmlToPdfMain_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            System.IO.FileInfo fInfoApp = new System.IO.FileInfo(Application.ExecutablePath);
            string file = fInfoApp.Directory.FullName + @"\RiepiloghiReadXmlToPdf.pdf";
            if (System.IO.File.Exists(file))
            {
                Process process = new Process();
                process.StartInfo = new ProcessStartInfo(file);
                process.Start();
            }
        }

        private void ChkDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dateTimePicker.Enabled = this.chkDate.Checked;
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            this.ListaFiles = null;
            this.txtFile.Text = "";
        }

        private void TxtFile_TextChanged(object sender, EventArgs e)
        {
            this.btnPDF.Enabled = false;
            this.lblInfo.Text = "";
            System.IO.FileInfo fInfo = new System.IO.FileInfo(this.txtFile.Text);
            if (fInfo.Exists)
            {
                string tipo = FileRiepilogo.GetTipoRiepilogo(fInfo.Name);
                if (!string.IsNullOrEmpty(tipo))
                {
                    string cValue = "";
                    int contatore = 0;
                    if (FileRiepilogo.GetDataFileRiepilogo(fInfo.Name, out cValue, out contatore) != null)
                    {
                        this.lblInfo.Text = cValue;
                        this.btnPDF.Enabled = true;
                    }
                }
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            /*C:\Sviluppo\ANDREA\riepiloghi-4.1\Riepiloghi\RiepiloghiReadXmlToPdf\testFiles*/
            this.ListaFiles = null;
            this.txtFile.Text = "";
            this.openFileDialog.Multiselect = false;
            this.openFileDialog.CheckFileExists = true;
            if (this.chkRPG.Checked && this.chkRPM.Checked)
            {
                this.openFileDialog.Filter = "All Files (*.*)|*.*|p7m Files (*.p7m)|*.p7m|xsi Files (*.xsi)|*.xsi";
            }
            else
            {
                if (this.chkRPG.Checked)
                    this.openFileDialog.Filter = "All Files (*.*)|*.*" + "|Giornalieri p7m Files (RPG*.p7m)|RPG*.p7m|Giornalieri xsi Files (RPG*.xsi)|RPG*.xsi";
                else
                    this.openFileDialog.Filter = "All Files (*.*)|*.*" + "|Mensili p7m Files (RPM*.p7m)|RPM*.p7m|Mensili xsi Files (RPM*.xsi)|RPM*.xsi";
            }
            if (this.openFileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(this.openFileDialog.FileName))
            {
                this.txtFile.Text = this.openFileDialog.FileName;
            }
        }

        private void BtnPathExtract_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtPath.Text))
            {
                DateTime? dateFilter = null;
                if (this.chkDate.Checked) dateFilter = this.dateTimePicker.Value.Date;
                this.UseWaitCursor = true;
                this.Refresh();
                Application.DoEvents();
                
                try
                {
                    this.ListaFiles = this.GetFiles(this.txtPath.Text, this.chkSubFolders.Checked, dateFilter, true);
                }
                catch (Exception)
                {
                }
                this.UseWaitCursor = false;
                this.Refresh();
                Application.DoEvents();
            }
        }

        private List<System.IO.FileInfo> GetFiles(string path, bool subFolders, DateTime? dateFilter, bool root)
        {
            List<System.IO.FileInfo> files = new List<System.IO.FileInfo>();
            List<System.IO.FileInfo> result = new List<System.IO.FileInfo>();

            System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(path);
            if (subFolders)
            {
                foreach (System.IO.DirectoryInfo subDir in dirInfo.GetDirectories())
                {
                    List<System.IO.FileInfo> subFiles = GetFiles(subDir.FullName, subFolders, dateFilter, false);
                    if (subFiles != null && subFiles.Count > 0)
                        subFiles.ForEach(f => files.Add(f));
                }
            }
            foreach (System.IO.FileInfo fInfo in dirInfo.GetFiles())
            {
                string tipo = FileRiepilogo.GetTipoRiepilogo(fInfo.Name);
                if (!string.IsNullOrEmpty(tipo))
                {
                    DateTime dCompare = new DateTime(int.Parse(fInfo.Name.Substring(4, 4)), int.Parse(fInfo.Name.Substring(9, 2)), (tipo == "RPM" ? 1 : int.Parse(fInfo.Name.Substring(12, 2))));
                    if (((this.chkRPG.Checked && tipo == "RPG") || (this.chkRPM.Checked && tipo == "RPM")) &&
                        (dateFilter == null || (tipo == "RPG" && dateFilter.Value.Date == dCompare.Date) || (tipo == "RPM" && dateFilter.Value.Year == dCompare.Year && dateFilter.Value.Month == dCompare.Month)))
                        files.Add(fInfo);
                }
            }

            if (root)
            {
                List<clsItemFileSorted> xresult = new List<clsItemFileSorted>();
                files.ForEach(f =>
                {
                    xresult.Add(new clsItemFileSorted(f));
                });
                

                xresult.OrderByDescending(x => x.Key).ToList<clsItemFileSorted>().ForEach(z =>
                {
                    result.Add(z.FInfo);
                });
            }

            return files;
        }

        private class clsItemFileSorted
        {
            public System.IO.FileInfo FInfo { get; set; }

            public clsItemFileSorted(System.IO.FileInfo fInfo)
            {
                this.FInfo = fInfo;
            }

            public string Key
            {
                get
                {
                    string cValue = "";
                    int contatore = 0;
                    DateTime? dataRiepilogo = FileRiepilogo.GetDataFileRiepilogo(this.FInfo.Name, out cValue, out contatore);
                    if (dataRiepilogo != null)
                        return dataRiepilogo.Value.Year.ToString("0000") + dataRiepilogo.Value.Month.ToString("00") + dataRiepilogo.Value.Day.ToString("00") + contatore.ToString("000");
                    else
                        return this.FInfo.Name;
                }
            }
        }

        private void BtnPath_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(this.folderBrowserDialog.SelectedPath))
            {
                this.txtPath.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            string fileResult = "";
            List<string> files = new List<string>();
            this.lblInfo.Text = "";
            long index = 0;
            long counter = 0;
            foreach (clsButton btn in this.pnlData.Controls)
            {
                if (btn.IsChecked)
                    counter += 1;
            }
            foreach (clsButton btn in this.pnlData.Controls)
            {
                if (btn.IsChecked)
                {
                    index += 1;
                    this.lblInfo.Text = string.Format("{0} di {1}", index.ToString(), counter.ToString());
                    this.lblInfo.Index = index;
                    this.lblInfo.Counter = counter;
                    System.IO.FileInfo fInfo = (System.IO.FileInfo)btn.Tag;
                    if (!files.Contains(fInfo.FullName)) files.Add(fInfo.FullName);
                }
            }
            
            if (counter == 0)
                if (!string.IsNullOrEmpty(this.txtFile.Text) && !files.Contains(this.txtFile.Text)) files.Add(this.txtFile.Text);

            if (files.Count > 0)
            {
                this.saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                this.saveFileDialog.CheckFileExists = false;
                if (this.saveFileDialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(this.saveFileDialog.FileName))
                {
                    fileResult = this.saveFileDialog.FileName;
                }
            }


            if (!string.IsNullOrEmpty(fileResult))
            {
                this.UseWaitCursor = true;
                this.Refresh();
                Application.DoEvents();

                try
                {
                    if (System.IO.File.Exists(fileResult))
                        System.IO.File.Delete(fileResult);
                    
                    fileResult = FileRiepilogo.GetFilePdf(fileResult, files);
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    MessageBox.Show(error);
                }
                this.UseWaitCursor = false;
                this.Refresh();
                Application.DoEvents();

                if (System.IO.File.Exists(fileResult))
                {
                    if (MessageBox.Show("Aprire il pdf ?", "Pdf generato", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Process process = new Process();
                        process.StartInfo = new ProcessStartInfo(fileResult);
                        process.Start();
                        process.WaitForExit();
                        process.Dispose();
                    }
                }
                else
                    MessageBox.Show("Impossibile generare il file.");
            }
        }

        private void FileRiepilogo_OutProcess(long index, long counter, string message)
        {
            if (this.InvokeRequired)
            {
                FileRiepilogo.delegateOutProcess d = new FileRiepilogo.delegateOutProcess(this.FileRiepilogo_OutProcess);
                this.Invoke(d, index, counter, message);
            }
            else
            {
                if (index + counter > 0)
                    this.lblInfo.Text = string.Format("{0} di {1} {2}", index.ToString(), counter.ToString(), message);
                else
                    this.lblInfo.Text = string.Format("{0}", message);
                this.lblInfo.Index = index;
                this.lblInfo.Counter = counter;
                this.lblFile.Refresh();
                this.Refresh();
                Application.DoEvents();
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
