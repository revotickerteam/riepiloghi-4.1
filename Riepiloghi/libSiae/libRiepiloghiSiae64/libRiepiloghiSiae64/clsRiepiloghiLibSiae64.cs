﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace libRiepiloghiSiae64
{
    public class clsRiepiloghiLibSiae64 : libRiepiloghiBase.iLib_SIAE_Provider
    {
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSN(Byte[] serial);

        //int GetSNML(BYTE serial [8]);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSNML(Byte[] serial, int nSlot);

        //int Initialize(int nSlot);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int Initialize(int nSlot);

        //int Finalize(void);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int Finalize();

        //int FinalizeML(int nSlot);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int FinalizeML(int nSlot);

        private const Int32 C_OK = 0x0000;
        private const Int32 C_CONTEXT_ERROR = 0x0001;
        private const Int32 C_NOT_INITIALIZED = 0x0002;
        private const Int32 C_ALREADY_INITIALIZED = 0x0003;
        private const Int32 C_NO_CARD = 0x0004;
        private const Int32 C_UNKNOWN_CARD = 0x0005;
        private const Int32 C_WRONG_LENGTH = 0x6282;
        private const Int32 C_WRONG_TYPE = 0x6981;
        private const Int32 C_NOT_AUTHORIZED = 0x6982;
        private const Int32 C_PIN_BLOCKED = 0x6983;
        private const Int32 C_WRONG_DATA = 0x6A80;
        private const Int32 C_FILE_NOT_FOUND = 0x6A82;
        private const Int32 C_RECORD_NOT_FOUND = 0x6A83;
        private const Int32 C_WRONG_LEN = 0x6A85;
        private const Int32 C_UNKNOWN_OBJECT = 0x6A88;
        private const Int32 C_ALREADY_EXISTS = 0x6A89;
        private const Int32 C_GENERIC_ERROR = 0xFFFF;

        // ADDED CONSTANTS
        private const Int32 C_IS_CARD_IN = 0x0020;
        private const Int32 C_CARD_IS_NOT_IN = 0x0021;

        // 
        private const Int32 C_WRONG_PIN = 0x63C4;

        public string GetSerialNumber(int Slot, out Exception Error)
        {
            string result = "";
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;

            try
            {
                ret = Initialize(Slot);
                Exception errInitialize = null;
                bool lInitialized = this.InitializeCARD(Slot, out errInitialize);
                if (errInitialize != null && errInitialize.Message == "Already Initialized")
                {
                    Exception errFinalize = null;
                    this.FinalizeCARD(Slot, out errFinalize);
                    lInitialized = this.InitializeCARD(Slot, out errInitialize);
                }
                if (lInitialized && errInitialize == null)
                {
                    ret = GetSNML(sn, Slot);
                    if (ret != 0)
                    {
                        Error = new Exception(getErrorMessage(ret));
                        result = "";
                    }
                    else
                    {
                        System.Text.Encoding enc = System.Text.Encoding.ASCII;
                        result = enc.GetString(sn).Trim('\0');
                    }
                }
                else if (errInitialize != null)
                {
                    Error = new Exception((Error != null ? Error.Message + "\r\n" : "") + errInitialize.Message);
                }

                if (lInitialized)
                {
                    Exception errFinalize = null;
                    this.FinalizeCARD(Slot, out errFinalize);
                    if (errFinalize != null)
                    {
                        Error = new Exception((Error != null ? Error.Message + "\r\n" : "") + errFinalize.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Error = e;
                result = "";
            }
            return result;
        }

        public bool InitializeCARD(int Slot, out Exception Error)
        {
            bool result = false;
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;

            try
            {
                ret = Initialize(Slot);
                result = (ret == 0);
                if (ret != 0)
                {
                    Error = new Exception(getErrorMessage(ret));
                }
            }
            catch (Exception e)
            {
                result = false;
                Error = e;
            }
            return result;
        }

        public bool FinalizeCARD(int Slot, out Exception Error)
        {
            bool result = false;
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;

            try
            {
                ret = FinalizeML(Slot);
                result = (ret == 0);
                if (ret != 0)
                {
                    Error = new Exception(getErrorMessage(ret));
                }
            }
            catch (Exception e)
            {
                result = false;
                Error = e;
            }
            return result;
        }

        private string getErrorMessage(Int32 cod)
        {
            string s1;

            switch (cod)
            {
                case C_OK: s1 = "OK"; break;

                case C_CONTEXT_ERROR: s1 = "Context Error"; break;

                case C_NOT_INITIALIZED: s1 = "Not Initialized"; break;

                case C_ALREADY_INITIALIZED: s1 = "Already Initialized"; break;

                case C_NO_CARD: s1 = "No Card"; break;

                case C_UNKNOWN_CARD: s1 = "Unknown Card"; break;

                case C_WRONG_LENGTH: s1 = "Wrong Length"; break;

                case C_WRONG_TYPE: s1 = "Wrong Type"; break;

                case C_NOT_AUTHORIZED: s1 = "Not Authorized"; break;

                case C_PIN_BLOCKED: s1 = "Pin Blocked"; break;

                case C_WRONG_DATA: s1 = "Wrong Data"; break;

                case C_FILE_NOT_FOUND: s1 = "File Not Found"; break;

                case C_RECORD_NOT_FOUND: s1 = "Record Not Found"; break;

                case C_WRONG_LEN: s1 = "Wrong Len"; break;

                case C_UNKNOWN_OBJECT: s1 = "Unknown Object"; break;

                case C_ALREADY_EXISTS: s1 = "Already Exist"; break;

                case C_GENERIC_ERROR: s1 = "Errore Generico"; break;


                case C_WRONG_PIN: s1 = "Pin Errato"; break;


                default: s1 = "Errore sconosciuto " + cod.ToString(); break;
            }

            return s1;
        }

        public bool FirmaFile(string Pin, int Slot, string FileDaFirmare, out Exception Error)
        {
            Error = null;
            bool result = false;
            try
            {
                SIAELib.SmartCardLibrary.SCLibrary scLibSA = new SIAELib.SmartCardLibrary.SCLibrary();
                long elapsedMs = 0;
                string errorMessage = "";
                int errorCode = 0;

                result = scLibSA.PKCS7Sign(Pin, FileDaFirmare, "", 1, ref elapsedMs, ref errorMessage, ref errorCode, Slot);
                if (!result)
                {
                    string cErrorMessage = (errorMessage != null && !string.IsNullOrEmpty(errorMessage) && errorMessage.Trim() != "" ? errorMessage : "Generazione EMAIL con firma");
                    if (errorCode > 0)
                        cErrorMessage = errorCode.ToString().Trim() + " " + cErrorMessage;
                    Error = new Exception(string.Format("Errore {0}", cErrorMessage));
                }
                else
                {
                    result = System.IO.File.Exists(FileDaFirmare + ".p7m");
                    if (!result)
                        Error = new Exception(string.Format("Errore, impossobile trovare il file {0}", FileDaFirmare + ".p7m"));
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
            }
            return result;
        }

        public bool GeneraEmail(string Pin, int Slot, string FileEml, string FileAllegato, string Mittente, string Destinatario, string Corpo, string Soggetto, out Exception Error)
        {
            Error = null;
            bool result = false;
            try
            {
                SIAELib.SmartCardLibrary.SCLibrary scLibSA = new SIAELib.SmartCardLibrary.SCLibrary();
                long elapsedMs = 0;
                string errorMessage = "";
                int errorCode = 0;

                System.IO.FileInfo oFileInfoAllegato = new System.IO.FileInfo(FileAllegato);
                string parFileAllegato = oFileInfoAllegato.Name + "|" + oFileInfoAllegato.FullName;
                try
                {
                    result = scLibSA.SMIMESign(Pin, FileEml, Mittente, Destinatario, Soggetto, "", Corpo, parFileAllegato, 1, ref elapsedMs, ref errorMessage, ref errorCode, Slot);

                    if (!result)
                    {
                        string cErrorMessage = (errorMessage != null && !string.IsNullOrEmpty(errorMessage) && errorMessage.Trim() != "" ? errorMessage : "Generazione EMAIL con firma");
                        if (errorCode > 0)
                            cErrorMessage = errorCode.ToString().Trim() + " " + cErrorMessage;
                        Error = new Exception(string.Format("Errore {0}", cErrorMessage));
                    }
                    else
                    {
                        result = System.IO.File.Exists(FileEml);
                        if (!result)
                            Error = new Exception(string.Format("Errore, impossobile trovare il file {0}", FileEml));
                    }
                }
                catch (Exception exSMIME)
                {
                    Error = new Exception(string.Format("Errore durante la generazione SMIME {0}", exSMIME.ToString()));
                    result = false;
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
            }
            return result; 
        }

    }
}
