﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace libRiepiloghiSiae
{
    public class clsRiepiloghiLibSiae : libRiepiloghiBase.iLib_SIAE_Provider
    {
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSN(Byte[] serial);

        //int GetSNML(BYTE serial [8]);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSNML(Byte[] serial, int nSlot);

        //int Initialize(int nSlot);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int Initialize(int nSlot);

        //int Finalize(void);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int Finalize();

        //int FinalizeML(int nSlot);
        [DllImport("LibSiae.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int FinalizeML(int nSlot);

        private const Int32 C_OK = 0x0000;
        private const Int32 C_CONTEXT_ERROR = 0x0001;
        private const Int32 C_NOT_INITIALIZED = 0x0002;
        private const Int32 C_ALREADY_INITIALIZED = 0x0003;
        private const Int32 C_NO_CARD = 0x0004;
        private const Int32 C_UNKNOWN_CARD = 0x0005;
        private const Int32 C_WRONG_LENGTH = 0x6282;
        private const Int32 C_WRONG_TYPE = 0x6981;
        private const Int32 C_NOT_AUTHORIZED = 0x6982;
        private const Int32 C_PIN_BLOCKED = 0x6983;
        private const Int32 C_WRONG_DATA = 0x6A80;
        private const Int32 C_FILE_NOT_FOUND = 0x6A82;
        private const Int32 C_RECORD_NOT_FOUND = 0x6A83;
        private const Int32 C_WRONG_LEN = 0x6A85;
        private const Int32 C_UNKNOWN_OBJECT = 0x6A88;
        private const Int32 C_ALREADY_EXISTS = 0x6A89;
        private const Int32 C_GENERIC_ERROR = 0xFFFF;

        // ADDED CONSTANTS
        private const Int32 C_IS_CARD_IN = 0x0020;
        private const Int32 C_CARD_IS_NOT_IN = 0x0021;

        // 
        private const Int32 C_WRONG_PIN = 0x63C4;

        List<string> providerOperations = new List<string>();
        string dllPath = "";
        public string DllPath { get => dllPath; set => dllPath = value; }

        public string GetSerialNumber(int Slot, out Exception Error)
        {
            providerOperations = new List<string>();
            string result = "";
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;
            providerOperations.Add("GetSerialNumber START");
            try
            {
                string currentPath = System.Environment.CurrentDirectory;
                
                string dllPathDestination = "";

                if (dllPath != "")
                {
                    System.IO.FileInfo oDllInfo = new System.IO.FileInfo(dllPath);
                    dllPathDestination = oDllInfo.Directory.FullName;
                }

                if (dllPathDestination != "" && currentPath != dllPathDestination)
                {
                    try
                    {
                        providerOperations.Add(string.Format("GetSerialNumber cambio path {0} {1}", currentPath, dllPathDestination));
                        System.Environment.CurrentDirectory = dllPathDestination;
                    }
                    catch (Exception exChangePath)
                    {
                        providerOperations.Add(string.Format("GetSerialNumber cambio path errore {0}", exChangePath.ToString()));
                    }
                }
                providerOperations.Add("GetSerialNumber (1) lInitialized = this.InitializeCARD(Slot, out errInitialize) " + Slot.ToString());
                Exception errInitialize = null;
                bool lInitialized = this.InitializeCARD(Slot, out errInitialize);
                providerOperations.Add("GetSerialNumber (1) lInitialized = this.InitializeCARD(Slot, out errInitialize) " + lInitialized.ToString() + " errInitialize" + (errInitialize != null ? errInitialize.ToString() : ""));

                if (errInitialize != null && errInitialize.Message == "Already Initialized")
                {
                    providerOperations.Add("GetSerialNumber this.FinalizeCARD(Slot, out errFinalize)");
                    Exception errFinalize = null;
                    this.FinalizeCARD(Slot, out errFinalize);
                    providerOperations.Add("GetSerialNumber this.FinalizeCARD(Slot, out errFinalize) errFinalize " + (errFinalize != null ? errFinalize.ToString() : ""));
                    providerOperations.Add("GetSerialNumber (2) lInitialized = this.InitializeCARD(Slot, out errInitialize) " + Slot.ToString());
                    lInitialized = this.InitializeCARD(Slot, out errInitialize);
                    providerOperations.Add("GetSerialNumber (2) lInitialized " + lInitialized.ToString() + " errInitialize" + (errInitialize != null ? errInitialize.ToString() : ""));
                }
                if (lInitialized && errInitialize == null)
                {
                    providerOperations.Add("GetSerialNumber lInitialized && errInitialize == null");
                    providerOperations.Add("GetSerialNumber ret = GetSNML(sn, Slot)");
                    ret = GetSNML(sn, Slot);
                    providerOperations.Add("GetSerialNumber ret = GetSNML(sn, Slot) ret " + ret);
                    if (ret != 0)
                    {
                        Error = new Exception(getErrorMessage(ret));
                        providerOperations.Add("GetSerialNumber ret = GetSNML(sn, Slot) Error " + Error.ToString());
                        result = "";
                    }
                    else
                    {
                        providerOperations.Add("GetSerialNumber ret = GetSNML(sn, Slot) OK");
                        providerOperations.Add("GetSerialNumber System.Text.Encoding enc = System.Text.Encoding.ASCII");
                        providerOperations.Add("GetSerialNumber result = enc.GetString(sn).Trim('\0')");
                        System.Text.Encoding enc = System.Text.Encoding.ASCII;
                        result = enc.GetString(sn).Trim('\0');
                        providerOperations.Add("GetSerialNumber result = enc.GetString(sn).Trim('\0') result " + result);
                    }
                }
                else if (errInitialize != null)
                {
                    providerOperations.Add("GetSerialNumber (3) errInitialize" + (errInitialize != null ? errInitialize.ToString() : ""));
                    Error = new Exception((Error != null ? Error.Message + "\r\n" : "") + errInitialize.Message);
                }

                if (lInitialized)
                {
                    providerOperations.Add("GetSerialNumber FINALE lInitialized " + lInitialized.ToString());
                    providerOperations.Add("GetSerialNumber FINALE this.FinalizeCARD(Slot, out errFinalize)");
                    Exception errFinalize = null;
                    this.FinalizeCARD(Slot, out errFinalize);
                    providerOperations.Add("GetSerialNumber FINALE this.FinalizeCARD(Slot, out errFinalize) errFinalize " + (errFinalize != null ? errFinalize.ToString() : ""));
                    if (errFinalize != null)
                    {
                        Error = new Exception((Error != null ? Error.Message + "\r\n" : "") + errFinalize.Message);
                    }
                }

                if (dllPathDestination != "" && currentPath != dllPathDestination)
                {
                    try
                    {
                        providerOperations.Add(string.Format("GetSerialNumber ritorno path {0} {1}", currentPath, dllPathDestination));
                        System.Environment.CurrentDirectory = currentPath;
                    }
                    catch (Exception exReturnPath)
                    {
                        providerOperations.Add(string.Format("GetSerialNumber cambio path errore {0}", exReturnPath.ToString()));
                    }
                }
            }
            catch (Exception e)
            {
                providerOperations.Add("GetSerialNumber ERRORE GENERICO " + e.ToString());
                Error = e;
                result = "";
            }
            providerOperations.Add("GetSerialNumber END result " + result);
            return result;
        }

        public bool InitializeCARD(int Slot, out Exception Error)
        {
            providerOperations.Add("InitializeCARD START " + Slot.ToString());
            bool result = false;
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;

            try
            {
                providerOperations.Add("InitializeCARD Initialize(Slot) " + Slot.ToString());
                ret = Initialize(Slot);
                result = (ret == 0);
                providerOperations.Add("InitializeCARD ret = Initialize(Slot) ret " + ret.ToString());
                if (ret != 0)
                {
                    Error = new Exception(getErrorMessage(ret));
                }
                providerOperations.Add("InitializeCARD ret = Initialize(Slot) ret " + ret.ToString() + " Error " + (Error != null ? Error.ToString() : ""));
            }
            catch (Exception e)
            {
                providerOperations.Add("InitializeCARD ERRORE GENERICO " + e.ToString());
                result = false;
                Error = e;
            }
            providerOperations.Add("InitializeCARD END " + result.ToString());
            return result;
        }

        public bool FinalizeCARD(int Slot, out Exception Error)
        {
            providerOperations.Add("FinalizeCARD START " + Slot.ToString());
            bool result = false;
            Byte[] sn = new Byte[8];
            Error = null;
            int ret = 0;

            try
            {
                providerOperations.Add("FinalizeCARD ret = FinalizeML(Slot);");
                ret = FinalizeML(Slot);
                result = (ret == 0);
                providerOperations.Add("FinalizeCARD ret = FinalizeML(Slot); ret " + ret.ToString());
                if (ret != 0)
                {
                    Error = new Exception(getErrorMessage(ret));
                    providerOperations.Add("FinalizeCARD ERRORE  " + Error.ToString());
                }
            }
            catch (Exception e)
            {
                providerOperations.Add("FinalizeCARD ERRORE GENERICO " + e.ToString());
                result = false;
                Error = e;
            }
            providerOperations.Add("FinalizeCARD END result " + result.ToString());
            return result;
        }

        private string getErrorMessage(Int32 cod)
        {
            string s1;

            switch (cod)
            {
                case C_OK: s1 = "OK"; break;

                case C_CONTEXT_ERROR: s1 = "Context Error"; break;

                case C_NOT_INITIALIZED: s1 = "Not Initialized"; break;

                case C_ALREADY_INITIALIZED: s1 = "Already Initialized"; break;

                case C_NO_CARD: s1 = "No Card"; break;

                case C_UNKNOWN_CARD: s1 = "Unknown Card"; break;

                case C_WRONG_LENGTH: s1 = "Wrong Length"; break;

                case C_WRONG_TYPE: s1 = "Wrong Type"; break;

                case C_NOT_AUTHORIZED: s1 = "Not Authorized"; break;

                case C_PIN_BLOCKED: s1 = "Pin Blocked"; break;

                case C_WRONG_DATA: s1 = "Wrong Data"; break;

                case C_FILE_NOT_FOUND: s1 = "File Not Found"; break;

                case C_RECORD_NOT_FOUND: s1 = "Record Not Found"; break;

                case C_WRONG_LEN: s1 = "Wrong Len"; break;

                case C_UNKNOWN_OBJECT: s1 = "Unknown Object"; break;

                case C_ALREADY_EXISTS: s1 = "Already Exist"; break;

                case C_GENERIC_ERROR: s1 = "Errore Generico"; break;


                case C_WRONG_PIN: s1 = "Pin Errato"; break;


                default: s1 = "Errore sconosciuto " + cod.ToString(); break;
            }

            return s1;
        }

        public bool FirmaFile(string Pin, int Slot, string FileDaFirmare, out Exception Error)
        {
            providerOperations = new List<string>();
            providerOperations.Add(string.Format("FirmaFile START\r\nPin:{0}\r\nSlot:{1}\r\nFileDaFirmare:{2}", Pin, Slot.ToString(), FileDaFirmare));
            Error = null;
            bool result = false;
            try
            {
                providerOperations.Add("FirmaFile SIAELib.SmartCardLibrary.SCLibrary");
                SIAELib.SmartCardLibrary.SCLibrary scLibSA = new SIAELib.SmartCardLibrary.SCLibrary();
                long elapsedMs = 0;
                string errorMessage = "";
                int errorCode = 0;
                providerOperations.Add("FirmaFile scLibSA.PKCS7Sign");
                result = scLibSA.PKCS7Sign(Pin, FileDaFirmare, "", 1, ref elapsedMs, ref errorMessage, ref errorCode, Slot);
                if (!result)
                {
                    string cErrorMessage = (errorMessage != null && !string.IsNullOrEmpty(errorMessage) && errorMessage.Trim() != "" ? errorMessage : "Generazione EMAIL con firma");
                    if (errorCode > 0)
                        cErrorMessage = errorCode.ToString().Trim() + " " + cErrorMessage;
                    Error = new Exception(string.Format("Errore {0}", cErrorMessage));
                }
                else
                {
                    providerOperations.Add("FirmaFile Verifica esistenza file firmato");
                    result = System.IO.File.Exists(FileDaFirmare + ".p7m");
                    providerOperations.Add("FirmaFile Verifica esistenza file NON firmato");
                    result = System.IO.File.Exists(FileDaFirmare);
                    if (!result)
                        Error = new Exception(string.Format("Errore, impossobile trovare il file {0}", FileDaFirmare + ".p7m"));
                }


                //try
                //{
                //    providerOperations.Add("FirmaFile scLibSA.Finalize");
                //    scLibSA.Finalize(ref errorMessage, ref errorCode);
                //    providerOperations.Add(string.Format("FirmaFile scLibSA.Finalize errorMessage {0}, errorCode {1}", (errorMessage != null ? errorMessage : ""), errorCode));
                //}
                //catch (Exception ex)
                //{

                //}

                //try
                //{
                //    providerOperations.Add("FirmaFile scLibSA.FinalizeML");
                //    scLibSA.FinalizeML(ref errorMessage, ref errorCode, Slot);
                //    providerOperations.Add(string.Format("FirmaFile scLibSA.FinalizeML errorMessage {0}, errorCode {1}", (errorMessage != null ? errorMessage : ""), errorCode));
                //}
                //catch (Exception ex)
                //{

                //}

            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
            }
            providerOperations.Add(string.Format("FirmaFile END Result: {0} Error {1}", result.ToString(), (Error != null ? Error.ToString() : "")));
            return result;
        }

        public bool GeneraEmail(string Pin, int Slot, string FileEml, string FileAllegato, string Mittente, string Destinatario, string Corpo, string Soggetto, out Exception Error)
        {
            providerOperations = new List<string>();
            providerOperations.Add(string.Format("GeneraEmail START\r\nPin:{0}\r\nSlot:{1}\r\nFileEml:{2}\r\nFileAllegato:{3}\r\nMittente:{4}\r\nDestinatario{5}\r\nCorpo:{6}\r\nSoggetto:{7}",
                                                 Pin, Slot.ToString(), FileEml, FileAllegato + " esiste:" + System.IO.File.Exists(FileAllegato), Mittente, Destinatario, Corpo, Soggetto));
            Error = null;
            bool result = false;
            try
            {
                //string fileDiBackup = FileAllegato + ".bck";
                //providerOperations.Add(string.Format("backup del file {0} esiste:{1} {2}", FileAllegato, System.IO.File.Exists(FileAllegato), fileDiBackup));
                //if (System.IO.File.Exists(fileDiBackup))
                //{
                //    providerOperations.Add(string.Format("backup del file ne esiste una copia precedente la elimino {0} ", fileDiBackup));
                //    System.IO.File.Delete(fileDiBackup);
                //}
                //providerOperations.Add(string.Format("backup del file COPIA {0} {1}", FileAllegato, fileDiBackup));
                //System.IO.File.Copy(FileAllegato, fileDiBackup);
                //providerOperations.Add(string.Format("backup del file {0} esiste:{1} {2} esiste:{3}", FileAllegato, System.IO.File.Exists(FileAllegato), fileDiBackup, System.IO.File.Exists(fileDiBackup)));
                //System.IO.File.Exists(FileAllegato)

                providerOperations.Add(string.Format("prima dell'istanza {0} esiste:{1}", FileAllegato, System.IO.File.Exists(FileAllegato)));

                providerOperations.Add("GeneraEmail SIAELib.SmartCardLibrary.SCLibrary");
                
                SIAELib.SmartCardLibrary.SCLibrary scLibSA = new SIAELib.SmartCardLibrary.SCLibrary();

                providerOperations.Add(string.Format("dopo dell'istanza {0} esiste:{1}", FileAllegato, System.IO.File.Exists(FileAllegato)));

                long elapsedMs = 0;
                string errorMessage = "";
                int errorCode = 0;

                System.IO.FileInfo oFileInfoAllegato = new System.IO.FileInfo(FileAllegato);
                providerOperations.Add(string.Format("GeneraEmail, esiste il file {0} ? {1}", oFileInfoAllegato.FullName, System.IO.File.Exists(oFileInfoAllegato.FullName)));
                //FileAllegato
                providerOperations.Add(string.Format("GeneraEmail (2), esiste il file {0} ? {1}", oFileInfoAllegato.FullName, System.IO.File.Exists(FileAllegato)));

                string parFileAllegato = oFileInfoAllegato.Name + "|" + FileAllegato;
                try
                {
                    providerOperations.Add(string.Format("GeneraEmail scLibSA.SMIMESign\r\nparFileAllegato:{0}", parFileAllegato));
                    result = scLibSA.SMIMESign(Pin, FileEml, Mittente, Destinatario, Soggetto, "", Corpo, parFileAllegato, 1, ref elapsedMs, ref errorMessage, ref errorCode, Slot);

                    if (!result)
                    {
                        string cErrorMessage = (errorMessage != null && !string.IsNullOrEmpty(errorMessage) && errorMessage.Trim() != "" ? errorMessage : "Generazione EMAIL con firma");
                        if (errorCode > 0)
                            cErrorMessage = errorCode.ToString().Trim() + " " + cErrorMessage;
                        Error = new Exception(string.Format("Errore {0}", cErrorMessage));
                    }
                    else
                    {
                        if (errorMessage != null || !string.IsNullOrEmpty(errorMessage) || !string.IsNullOrWhiteSpace(errorMessage) || errorCode > 0)
                        {
                            providerOperations.Add(string.Format("GeneraEmail errorMessage contiene valori: [{0}] errorCode {1}", errorMessage, errorCode));
                        }
                        else
                        {
                            providerOperations.Add("GeneraEmail errorMessage non contiene valori");
                        }
                        providerOperations.Add(string.Format("GeneraEmail errorCode: {0}", errorCode));

                        providerOperations.Add(string.Format("GeneraEmail verifica esistenza FileEml: {0}", FileEml));
                        result = System.IO.File.Exists(FileEml);
                        if (!result)
                            Error = new Exception(string.Format("Errore, impossobile trovare il file {0}", FileEml));
                    }
                }
                catch (Exception exSMIME)
                {
                    Error = new Exception(string.Format("Errore durante la generazione SMIME {0}", exSMIME.ToString()));
                    providerOperations.Add(exSMIME.Message);
                    providerOperations.Add(exSMIME.ToString());
                    result = false;
                }

                //try
                //{
                //    providerOperations.Add("GeneraEmail scLibSA.Finalize");
                //    scLibSA.Finalize(ref errorMessage, ref errorCode);
                //    providerOperations.Add(string.Format("GeneraEmail scLibSA.Finalize errorMessage {0}, errorCode {1}", (errorMessage != null ? errorMessage : ""), errorCode));
                //}
                //catch (Exception ex)
                //{

                //}

                //try
                //{
                //    providerOperations.Add("GeneraEmail scLibSA.FinalizeML");
                //    scLibSA.FinalizeML(ref errorMessage, ref errorCode, Slot);
                //    providerOperations.Add(string.Format("GeneraEmail scLibSA.FinalizeML errorMessage {0}, errorCode {1}", (errorMessage != null ? errorMessage : ""), errorCode));
                //}
                //catch (Exception ex)
                //{

                //}
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
            }
            providerOperations.Add(string.Format("GeneraEmail END result:{0} Error:{1}", result.ToString(), (Error != null ? Error.ToString() : "")));
            return result;
        }

        public bool InviaEmail(string FileEml, string host, int port, string user, string password, List<string> cc, List<string> bcc, out Exception Error)
        {
            providerOperations = new List<string>();
            providerOperations.Add(string.Format("InviaEmail START\r\nFileEml:{0}\r\nhost:{1}\r\nport{2}\r\nuser:{3}\r\npassword:{4}",
                                                 FileEml, host, port.ToString(), user, password));
            Error = null;
            bool result = false;
            try
            {
                providerOperations.Add("InviaEmail MailLib.Mail.setSmtpServer");
                MailLib.Mail.setSmtpServer(host, port, user, password);

                providerOperations.Add("InviaEmail ccCollection");
                System.Net.Mail.MailAddressCollection ccCollection = new System.Net.Mail.MailAddressCollection();
                if (cc != null && cc.Count > 0)
                {
                    foreach(string item in cc)
                    {
                        ccCollection.Add(item);
                    }
                }
                
                providerOperations.Add("InviaEmail bccCollection");
                System.Net.Mail.MailAddressCollection bccCollection = new System.Net.Mail.MailAddressCollection();
                if (bcc != null && bcc.Count > 0)
                {
                    foreach (string item in bcc)
                    {
                        bccCollection.Add(item);
                    }
                }
                else
                {
                    //bccCollection.Add("andrea@creaweb.it");
                    //bccCollection.Add("francesco.g@creaweb.it");
                }

                string errorMessage = "";
                int errorCode = 0;
                providerOperations.Add("InviaEmail  MailLib.Mail.sendEmailFromEmlFile");
                result = MailLib.Mail.sendEmailFromEmlFile(FileEml, ccCollection, bccCollection, ref errorMessage, ref errorCode);

                if (errorMessage == null)
                {
                    errorMessage = "";
                }

                if (!result)
                {
                    Error = new Exception(string.Format("Errore spedizione email {0} {1}", errorCode.ToString(), errorMessage));
                }

            }
            catch (Exception ex)
            {
                Error = new Exception(string.Format("Errore spedizione email {0}", ex.ToString()));
                result = false;
            }
            providerOperations.Add(string.Format("InviaEmail END result:{0} Error:{1}", result.ToString(), (Error != null ? Error.ToString() : "")));
            return result;
        }

        public List<object> RiceviEmail(string accountEmail, string user, string password, string pop3Server, int port, bool userSsl, object LocalInboxProvider, string codiceSistema, out Exception error)
        {
            error = null;
            providerOperations = new List<string>();
            providerOperations.Add(string.Format("RiceviEmail START\r\naccountEmail:{0}\r\nuser:{1}\r\npassword:{2}\r\npop3Server:{3}\r\nport:{4}\r\nuserSsl:{5}\r\nLocalInboxProvider:{6}", accountEmail, user, password, pop3Server, port.ToString(), userSsl.ToString(), (LocalInboxProvider == null ? "NULL" : "Valorizzato")));
            MailLib.IUidsLocalInboxProvider LocalInboxProviderObj = null;
            try
            {
                if (LocalInboxProvider != null)
                    LocalInboxProviderObj = (MailLib.IUidsLocalInboxProvider)LocalInboxProvider;
            }
            catch (Exception)
            {
            }
            
            List<object> result = null;
            try
            {
                providerOperations.Add("RiceviEmail MailLib.Mail.readMessagesPop3");
                result = MailLib.Mail.readMessagesPop3(accountEmail, user, password, pop3Server, port, userSsl, LocalInboxProviderObj, codiceSistema, out error);
            }
            catch (Exception ex)
            {
                error = new Exception(string.Format("Errore spedizione email {0}", ex.ToString()));
            }
            providerOperations.Add(string.Format("RiceviEmail END result items:{0} Error:{1}", (result != null ? result.Count.ToString() : ""), (error != null ? error.ToString() : "")));
            return result;
        }

        public List<string> ProviderOperations()
        {
            return providerOperations;
        }
    }

}
