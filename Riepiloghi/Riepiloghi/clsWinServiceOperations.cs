﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Wintic.Data;
using System.Diagnostics;

namespace Riepiloghi
{

    public class clsWinServiceConfig
    {

        public const string WinServiceAPPLICATION_TYPE_CONSOLEMODE = "CONSOLE-ONE-TIME";
        public const string WinServiceAPPLICATION_TYPE_CONSOLEWAIT = "CONSOLE-WAIT";
        public const string WinServiceAPPLICATION_TYPE_WIN_DESKTOP = "WINDOWS-FORM";
        public const string WinServiceAPPLICATION_TYPE_WIN_SERVICE = "WINDOWS-SERVICE";
        public const string WinServiceAPPLICATION_TYPE_WEB_APIMODE = "WEB-API";

        public const string WinServiceCOMUNICATION_TYPE_DIRECT = "DIRECT";
        public const string WinServiceCOMUNICATION_TYPE_VIA_DB = "VIA_DB";
        public const string WinServiceCOMUNICATION_TYPE_SOCKET = "SOCKET";

        public string MachineName { get; set; }
        public bool LogEnabled { get; set; }
        public string ApplicationType { get; set; }
        public string ComunicationMode { get; set; }
        public string ListenerIP { get; set; }
        public int ListenerPORT { get; set; }

        public TimeSpan PollingTimeSpan { get; set; }

        public clsWinServiceConfig()
        {
            this.MachineName = "";
            this.LogEnabled = false;
            this.ListenerIP = "";
            this.ListenerPORT = 0;
            this.ApplicationType = "";
            this.ComunicationMode = "";
            this.PollingTimeSpan = new TimeSpan();
        }

        public static clsWinServiceConfig GetWinServiceConfig(Wintic.Data.IConnection Connection, out Exception Error, bool CheckMachineName, string parCheckTypeApplication)
        {
            Error = null;
            clsWinServiceConfig result = new clsWinServiceConfig();

            String cHeaderMethod = "Configurazione Servizio Riepiloghi";

            try
            {
                result.MachineName = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_MACHINE_NAME", "");
                result.ApplicationType = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_APPLICATION_TYPE", "");
                result.ComunicationMode = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_COMUNICATION_MODE", "");

                bool lDebug = System.Diagnostics.Debugger.IsAttached;
                Dictionary<string, List<string>> ValidConfigurations = new Dictionary<string, List<string>>();

                ValidConfigurations.Add(WinServiceAPPLICATION_TYPE_CONSOLEMODE, new List<string>() { WinServiceCOMUNICATION_TYPE_VIA_DB });
                ValidConfigurations.Add(WinServiceAPPLICATION_TYPE_CONSOLEWAIT, new List<string>() { WinServiceCOMUNICATION_TYPE_VIA_DB, WinServiceCOMUNICATION_TYPE_SOCKET });
                ValidConfigurations.Add(WinServiceAPPLICATION_TYPE_WIN_DESKTOP, new List<string>() { WinServiceCOMUNICATION_TYPE_VIA_DB, WinServiceCOMUNICATION_TYPE_SOCKET });
                ValidConfigurations.Add(WinServiceAPPLICATION_TYPE_WIN_SERVICE, new List<string>() { WinServiceCOMUNICATION_TYPE_VIA_DB, WinServiceCOMUNICATION_TYPE_SOCKET });
                ValidConfigurations.Add(WinServiceAPPLICATION_TYPE_WEB_APIMODE, new List<string>() { WinServiceCOMUNICATION_TYPE_DIRECT });

                if (Error == null && result.MachineName.Trim() == "")
                {
                    Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_MACHINE_NAME non valido.");
                }
                else if (!ValidConfigurations.ContainsKey(result.ApplicationType))
                {
                    Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_APPLICATION_TYPE sconosciuto.");
                }
                else if (!ValidConfigurations[result.ApplicationType].Contains(result.ComunicationMode))
                {
                    Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_COMUNICATION_MODE non valido.");
                }
                else if (result.ComunicationMode.Trim() == WinServiceCOMUNICATION_TYPE_SOCKET)
                {
                    result.ListenerIP = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_SOCKET_LISTENER_IP", "");
                    string cPort = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_SOCKET_LISTENER_PORT", "");
                    int nPort = 0;
                    if (!int.TryParse(cPort, out nPort))
                        nPort = 0;
                    result.ListenerPORT = nPort;
                    if (result.ListenerIP.Trim() == "" || result.ListenerPORT == 0)
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_SOCKET_LISTENER_IP, WINSERVICE_SOCKET_LISTENER_PORT non validi.");
                }
                else if (result.ComunicationMode == WinServiceCOMUNICATION_TYPE_VIA_DB)
                {
                    string cPolling = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_POLLING_TIME", "00:01:00");
                    if (cPolling.Contains(":"))
                    {
                        try
                        {
                            result.PollingTimeSpan = new TimeSpan(int.Parse(cPolling.Split(':')[0]), int.Parse(cPolling.Split(':')[1]), int.Parse(cPolling.Split(':')[2]));
                        }
                        catch (Exception)
                        {
                            result.PollingTimeSpan = new TimeSpan();
                            Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_POLLING_TIME non valido.");
                        }
                    }
                    else
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server generazione riepiloghi: WINSERVICE_POLLING_TIME non valido.");
                }

                if (Error == null && CheckMachineName && !lDebug)
                {
                    if (Environment.MachineName.Trim().ToUpper() != result.MachineName.Trim().ToUpper())
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, string.Format("Server generazione riepiloghi: WINSERVICE_MACHINE_NAME non valido, configurato {0}, in esecuzione {1}.", result.MachineName, Environment.MachineName));
                }

                if (Error == null && !string.IsNullOrEmpty(parCheckTypeApplication) && !string.IsNullOrWhiteSpace(parCheckTypeApplication) && !lDebug)
                {
                    if (parCheckTypeApplication != result.ApplicationType)
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, string.Format("Server generazione riepiloghi: WINSERVICE_APPLICATION_TYPE non valido, configurato {0}, in esecuzione {1}.", result.ApplicationType, parCheckTypeApplication));
                }
            }
            catch (Exception ex)
            {
                Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Errore", ex);
            }

            return result;
        }
    }

    public class clsWinServiceClientOperation
    {
        #region "SendOut messaggi"

        public delegate void delegateMessaggesWinServiceClientOperation(string message, Exception errore = null);
        public event delegateMessaggesWinServiceClientOperation MessaggesWinServiceClientOperation;

        public void RaiseMessaggesWinServiceClientOperation(string message, Exception error = null)
        {
            if (MessaggesWinServiceClientOperation != null) MessaggesWinServiceClientOperation(message, error);
        }

        #endregion

        #region "variabili in caso di socket server"

        private clsWinServiceModel_response_Operation responseOperation = null;
        private bool receivedRequestOperation = false;

        #endregion

        #region "variabili per modalità diretta"

        private string ApplicationType = "";

        #endregion

        public clsWinServiceClientOperation(string applicationType)
        {
            this.ApplicationType = applicationType;
        }

        public bool PushOperazione(Wintic.Data.IConnection Connection, string codiceSistema, string accountId, Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi, string operationCode, string tipo, DateTime? giorno, long? prog, out long IdOperazione, out clsRiepilogoGenerato dettaglioRiepilogo, out Exception Error)
        {
            bool result = false;
            IdOperazione = 0;
            Error = null;
            dettaglioRiepilogo = null;
            string cHeaderMethod = "Richiesta Operazione a Server Riepiloghi";

            RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, "configurazione"));
            clsWinServiceConfig config = clsWinServiceConfig.GetWinServiceConfig(Connection, out Error, (this.ApplicationType == clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE), (this.ApplicationType == clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE ? this.ApplicationType : ""));
            if (Error != null)
                RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, "configurazione"), Error);

            string securityToken = "";
            bool lRequestCreated = false;

            if (Error == null)
            {
                try
                {
                    StringBuilder oSB = null;
                    IRecordSet oRS = null;
                    clsParameters oPars = null;
                    oSB = new StringBuilder("SELECT NVL(MIN(IDOPERAZIONE), 0) AS IDOPERAZIONE FROM RIEPILOGHI_OPERAZIONI_REQ WHERE STATO_OPERAZIONE < 2");
                    oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString());
                    result = (oRS.EOF || oRS.Fields("IDOPERAZIONE").IsNull || long.Parse(oRS.Fields("IDOPERAZIONE").Value.ToString()) == 0);
                    if (!result)
                    {
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Server riepiloghi occupato.");
                    }
                    oRS.Close();

                    if (Error != null)
                        RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, "verifica operazioni in corso"), Error);

                    if (result)
                    {
                        oSB = new StringBuilder("SELECT SEQ_IDOPERAZIONE.NEXTVAL AS IDOPERAZIONE FROM DUAL");
                        oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString());
                        result = !oRS.EOF && !oRS.Fields("IDOPERAZIONE").IsNull && long.TryParse(oRS.Fields("IDOPERAZIONE").Value.ToString(), out IdOperazione);
                        if (!result)
                        {
                            Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Impossibile determinare un nuovo id operazione.");
                        }
                        oRS.Close();
                    }

                    if (Error != null)
                        RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, "nuova idoperazione"), Error);

                    if (result)
                    {
                        if (giorno != null)
                        {
                            giorno = giorno.GetValueOrDefault().Date;

                            if (tipo != null && tipo == "M")
                            {
                                giorno = new DateTime(giorno.GetValueOrDefault().Year, giorno.GetValueOrDefault().Month, 1);
                            }
                        }

                        clsWinServiceModel_Token requestToken = new clsWinServiceModel_Token();
                        requestToken.AccountId = accountId;
                        requestToken.CodiceSistema = codiceSistema;
                        requestToken.IdOperazione = IdOperazione;
                        requestToken.OperationCode = operationCode;
                        securityToken = clsSecuritySocket.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(requestToken));

                        oSB = new StringBuilder();
                        oPars = new clsParameters();

                        oSB.Append("INSERT INTO RIEPILOGHI_OPERAZIONI_REQ");

                        oSB.Append(" (IDOPERAZIONE , ACCOUNTID, CODICE_SISTEMA, OPERATION_CODE, DATA_INS, DATA_USE, SECURITY_TOKEN, STATO_OPERAZIONE");

                        oPars.Add(":pIDOPERAZIONE", IdOperazione);
                        oPars.Add(":pACCOUNTID", accountId);
                        oPars.Add(":pCODICE_SISTEMA", codiceSistema);
                        oPars.Add(":pOPERATION_CODE", operationCode);
                        oPars.Add(":pSECURITY_TOKEN", securityToken);

                        if (giorno != null)
                        {
                            oSB.Append(" , GIORNO");
                            oPars.Add(":pGIORNO", giorno);
                        }

                        if (tipo != null)
                        {
                            oSB.Append(" , TIPO");
                            oPars.Add(":pTIPO", tipo);
                        }

                        if (prog != null)
                        {
                            oSB.Append(" , PROG");
                            oPars.Add(":pPROG", prog);
                        }

                        oSB.Append(" )");

                        oSB.Append(" VALUES ");
                        oSB.Append(" (:pIDOPERAZIONE, :pACCOUNTID, :pCODICE_SISTEMA, :pOPERATION_CODE, SYSDATE, NULL, :pSECURITY_TOKEN, 0");

                        if (giorno != null)
                        {
                            oSB.Append(" , :pGIORNO");
                        }

                        if (tipo != null)
                        {
                            oSB.Append(" , :pTIPO");
                        }

                        if (prog != null)
                        {
                            oSB.Append(" , :pPROG");
                        }

                        oSB.Append(" )");

                        lRequestCreated = long.Parse(Connection.ExecuteNonQuery(oSB.ToString(), oPars, true).ToString()) > 0;
                        result = lRequestCreated;
                    }

                    if (result)
                    {
                        // esecuzione diretta
                        if (config.ApplicationType == clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE)
                        {
                            result = PushDirect(Connection, config, percorsi, out Error, operationCode, securityToken, tipo, giorno, prog, out dettaglioRiepilogo);
                        }
                        else if (config.ComunicationMode == clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_SOCKET) // SE LA CONFIGURAZIONE E' CON SOCKET VERIFICO SUBITO LA RISPOSTA DELLA PRESA IN CARICO
                        {
                            result = PushSocket(config, out Error, operationCode, securityToken, tipo, giorno, prog, out dettaglioRiepilogo);
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Errore generico", ex);
                    result = false;
                }
            }


            if (!result && lRequestCreated && IdOperazione > 0)
            {
                try
                {
                    Connection.ExecuteNonQuery("DELETE FROM RIEPILOGHI_OPERAZIONI_REQ WHERE IDOPERAZIONE = :pIDOPERAZIONE", new clsParameters(":pIDOPERAZIONE", IdOperazione), true);
                }
                catch (Exception)
                {
                }
            }

            return result;
        }


        #region "Push con socket"

        private bool PushSocket(clsWinServiceConfig config, out Exception Error, string operationCode, string securityToken, string tipo, DateTime? giorno, long? prog, out clsRiepilogoGenerato dettaglioRiepilogo)
        {
            dettaglioRiepilogo = null;
            Error = null;
            bool result = false;
            string cHeaderMethod = "Richiesta Operazione a Server Riepiloghi SOCKET";
            try
            {
                Riepiloghi.SocketClientRiepiloghi oClient = new Riepiloghi.SocketClientRiepiloghi(config.ListenerIP, config.ListenerPORT);

                oClient.OnReceived += OClient_OnReceived;

                try
                {
                    result = oClient.Open();
                }
                catch (Exception)
                {
                    result = false;
                }

                if (!result)
                {
                    Error = oClient.Error;
                }
                else
                {
                    try
                    {
                        Riepiloghi.clsWinServiceModel_request_Operation oSocketRequest = new Riepiloghi.clsWinServiceModel_request_Operation();
                        oSocketRequest.Code = operationCode;
                        oSocketRequest.Token = securityToken;
                        if (operationCode == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO)
                        {
                            oSocketRequest.RequestRiepilogo = new Riepiloghi.clsWinServiceModel_request_Riepilogo();
                            oSocketRequest.RequestRiepilogo.GiornalieroMensile = tipo;
                            oSocketRequest.RequestRiepilogo.Giorno = (DateTime)giorno;
                            oSocketRequest.RequestRiepilogo.Progressivo = (long)prog;

                            dettaglioRiepilogo = new clsRiepilogoGenerato();
                            dettaglioRiepilogo.Tipo = tipo;
                            dettaglioRiepilogo.Giorno = (DateTime)giorno;
                            dettaglioRiepilogo.Progressivo = (long)prog;
                        }
                        else if (operationCode == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_SEND_EMAIL && tipo != null && giorno != null && prog != null)
                        {
                            oSocketRequest.RequestRiepilogo = new Riepiloghi.clsWinServiceModel_request_Riepilogo();
                            oSocketRequest.RequestRiepilogo.GiornalieroMensile = tipo;
                            oSocketRequest.RequestRiepilogo.Giorno = (DateTime)giorno;
                            oSocketRequest.RequestRiepilogo.Progressivo = (long)prog;
                            dettaglioRiepilogo = new clsRiepilogoGenerato();
                            dettaglioRiepilogo.Tipo = tipo;
                            dettaglioRiepilogo.Giorno = (DateTime)giorno;
                            dettaglioRiepilogo.Progressivo = (long)prog;
                        }
                        string cRequest = Newtonsoft.Json.JsonConvert.SerializeObject(oSocketRequest);
                        this.responseOperation = null;
                        this.receivedRequestOperation = false;
                        result = oClient.Send(cRequest);
                        if (!result)
                        {
                            Error = oClient.Error;
                        }
                        else
                        {
                            result = false;
                            System.Diagnostics.Stopwatch oStopWatch = new System.Diagnostics.Stopwatch();
                            oStopWatch.Start();
                            while (!this.receivedRequestOperation && oStopWatch.ElapsedMilliseconds < 10000000)
                            {
                                System.Threading.Thread.Sleep(10);
                            }
                            oStopWatch.Stop();

                            result = (this.receivedRequestOperation && this.responseOperation != null);

                            if (!result)
                            {
                                Error = clsRiepiloghi.GetNewException(cHeaderMethod, (oClient.Error == null ? "Nessuna risposta dal socket del server riepiloghi." : oClient.Error.Message));
                            }
                        }

                    }
                    catch (Exception exSend)
                    {
                        Error = clsRiepiloghi.GetNewException(cHeaderMethod, "errore durante la richiesta al socket del server riepiloghi.", exSend);
                        result = false;
                    }

                    oClient.Close();
                }

                oClient.OnReceived -= OClient_OnReceived;
                oClient.Dispose();
                oClient = null;
            }
            catch (Exception exGen)
            {
                Error = clsRiepiloghi.GetNewException(cHeaderMethod, "errore generico durante la richiesta al socket del server riepiloghi.", exGen);
                result = false;
            }
            return result;
        }

        private void OClient_OnReceived(SocketClientRiepiloghi socket, string data)
        {
            try
            {
                receivedRequestOperation = true;
                this.responseOperation = Newtonsoft.Json.JsonConvert.DeserializeObject<Riepiloghi.clsWinServiceModel_response_Operation>(data);
            }
            catch (Exception ex)
            {
                if (socket.Error != null)
                    socket.Error = new Exception(ex.Message + " " + socket.Error.Message);
                else
                    socket.Error = ex;
            }
        }


        #endregion

        #region "Push con metodo diretto"


        private bool PushDirect(Wintic.Data.IConnection Connection, clsWinServiceConfig config, Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi, out Exception Error, string operationCode, string securityToken, string tipo, DateTime? giorno, long? prog, out clsRiepilogoGenerato dettaglioRiepilogo)
        {
            dettaglioRiepilogo = null;
            Error = null;
            bool result = false;
            string cHeaderMethod = "Richiesta Operazione a Server Riepiloghi DIRECT";
            clsWinServiceModel_request_Operation request = null;
            bool lRequestPending = false;
            
            RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, ""));

            try
            {
                try
                {
                    clsWinServiceModel_Token token = null;
                    lRequestPending = clsWinServiceServerOperation.GetOperationToExecute(Connection, out request, out token, out Error, this.ApplicationType);
                    
                    if (Error != null)
                        RaiseMessaggesWinServiceClientOperation(string.Format("{0} {1}", cHeaderMethod, "Operazione da eseguire"), Error);

                    if (lRequestPending && Error == null)
                    {
                        clsWinServiceServerOperation oServer = new clsWinServiceServerOperation(Connection);
                        result = oServer.ElaboraRichiestaRiepilogo(request, token, percorsi, out Error, out dettaglioRiepilogo);
                    }
                    
                }
                catch (Exception ex)
                {
                    Error = clsRiepiloghi.GetNewException(cHeaderMethod, "Errore generico DIRECT." + "\r\n" + ex.Message, ex);
                }
            }
            catch (Exception exGen)
            {
                Error = clsRiepiloghi.GetNewException(cHeaderMethod, "errore generico durante la richiesta server riepiloghi diretto.", exGen);
                result = false;
            }


            return result;
        }

        #endregion
    }

    public class clsWinServiceServerOperation
    {

        public string DataSource = "";
        public Wintic.Data.IConnection DirectConnection = null;

        public delegate void delegateMessaggesWinServiceServerOperation(string message, Exception errore = null);
        public event delegateMessaggesWinServiceServerOperation MessaggesWinServiceServerOperation;

        public void RaiseMessaggesWinServiceServerOperation(string message, Exception error = null)
        {
            if (MessaggesWinServiceServerOperation != null) MessaggesWinServiceServerOperation(message, error);
        }

        public clsWinServiceServerOperation(string dataSource)
        {
            this.DataSource = dataSource;
        }

        public clsWinServiceServerOperation(Wintic.Data.IConnection directConnection)
        {
            this.DirectConnection = directConnection;
        }

        public static bool GetOperationToExecute(Wintic.Data.IConnection Connection, out clsWinServiceModel_request_Operation request, out clsWinServiceModel_Token token, out Exception Error, string applicationType)
        {
            request = null;
            token = null;
            Error = null;
            bool result = false;

            try
            {
                clsWinServiceConfig config = clsWinServiceConfig.GetWinServiceConfig(Connection, out Error, true, applicationType);

                if (Error == null)
                {
                    StringBuilder oSB = null;
                    IRecordSet oRS = null;
                    clsParameters oPars = null;

                    long IdOperazione = 0;

                    oSB = new StringBuilder("SELECT RIEPILOGHI_OPERAZIONI_REQ.* FROM RIEPILOGHI_OPERAZIONI_REQ, (SELECT NVL(MIN(IDOPERAZIONE), 0) AS IDOPERAZIONE FROM RIEPILOGHI_OPERAZIONI_REQ WHERE STATO_OPERAZIONE = 0) X WHERE RIEPILOGHI_OPERAZIONI_REQ.IDOPERAZIONE = X.IDOPERAZIONE");
                    oRS = (IRecordSet)Connection.ExecuteQuery(oSB.ToString());
                    if (!oRS.EOF && !oRS.Fields("IDOPERAZIONE").IsNull && long.TryParse(oRS.Fields("IDOPERAZIONE").Value.ToString(), out IdOperazione) && IdOperazione > 0)
                    {
                        result = true;
                        Connection.ExecuteNonQuery("UPDATE RIEPILOGHI_OPERAZIONI_REQ SET STATO_OPERAZIONE = 1 WHERE IDOPERAZIONE = :pIDOPERAZIONE", new Wintic.Data.clsParameters(":pIDOPERAZIONE", IdOperazione), true);

                        request = new clsWinServiceModel_request_Operation();
                        token = new clsWinServiceModel_Token();

                        if (!oRS.Fields("ACCOUNTID").IsNull && !oRS.Fields("CODICE_SISTEMA").IsNull && !oRS.Fields("DATA_INS").IsNull && !oRS.Fields("OPERATION_CODE").IsNull && !oRS.Fields("SECURITY_TOKEN").IsNull)
                        {
                            token.AccountId = oRS.Fields("ACCOUNTID").Value.ToString();
                            token.CodiceSistema = oRS.Fields("CODICE_SISTEMA").Value.ToString();
                            token.DataIns = (DateTime)oRS.Fields("DATA_INS").Value;
                            token.IdOperazione = IdOperazione;
                            token.OperationCode = oRS.Fields("OPERATION_CODE").Value.ToString();
                            request.Token = clsSecuritySocket.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(token));

                            try
                            {
                                clsWinServiceModel_Token oTokenToCheck = Newtonsoft.Json.JsonConvert.DeserializeObject<clsWinServiceModel_Token>(clsSecuritySocket.Decrypt(oRS.Fields("SECURITY_TOKEN").Value.ToString()));
                                result = (token.AccountId == oTokenToCheck.AccountId &&
                                          token.CodiceSistema == oTokenToCheck.CodiceSistema &&
                                          token.IdOperazione == oTokenToCheck.IdOperazione &&
                                          token.OperationCode == oTokenToCheck.OperationCode);
                            }
                            catch (Exception)
                            {
                                result = false;
                            }

                            
                            if (!result)
                            {
                                Error = new Exception("Token non valido");
                            }
                            else
                            {
                                request.Code = token.OperationCode;
                                request.RequestRiepilogo = new clsWinServiceModel_request_Riepilogo();
                                if (!oRS.Fields("GIORNO").IsNull)
                                    request.RequestRiepilogo.Giorno = (DateTime)oRS.Fields("GIORNO").Value;
                                if (!oRS.Fields("TIPO").IsNull)
                                    request.RequestRiepilogo.GiornalieroMensile = oRS.Fields("TIPO").Value.ToString();
                                if (!oRS.Fields("PROG").IsNull)
                                {
                                    long prog = 0;
                                    long.TryParse(oRS.Fields("PROG").Value.ToString(), out prog);
                                    request.RequestRiepilogo.Progressivo = prog;
                                }
                            }
                        }
                        else
                            Error = new Exception("Token non valido");
                    }
                    oRS.Close();
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
            }

            if (!result)
            {
                request = null;
                token = null;
            }

            return result;
        }

        public bool ElaboraRichiestaRiepilogo(clsWinServiceModel_request_Operation request, clsWinServiceModel_Token token, Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi, out Exception Error, out clsRiepilogoGenerato dettaglioRiepilogo)
        {
            dettaglioRiepilogo = null;
            bool result = false;
            Error = null;
            String cHeaderError = "Server ripieloghi, elabora richiesta riepilogo";
            libRiepiloghiBase.iLib_SIAE_Provider providerSIAE = null;
            Wintic.Data.IConnection oConnection = null;
            System.Reflection.Assembly oAssembly = null;

            RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, ""));

            try
            {
                long IdOperazione = token.IdOperazione;
                DateTime Giorno = request.RequestRiepilogo.Giorno;
                string GiornalieroMensile = request.RequestRiepilogo.GiornalieroMensile;
                string cFileLogP7M = "";
                string cFileRpgP7M = "";
                string cFileEmlP7M = "";


                // se esiste una connessione diretta la uso senza poi chiuderla
                if (this.DirectConnection != null)
                    oConnection = this.DirectConnection;
                else
                {
                    oConnection = new Wintic.Data.oracle.CConnectionOracle();
                    oConnection.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", this.DataSource);
                    oConnection.Open();
                    if (oConnection.State != System.Data.ConnectionState.Open)
                    {
                        Error = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore di connessione al database");
                    }
                }

                if (Error != null)
                    RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, ""), Error);

                if (Error == null)
                {
                    try
                    {
                        List<string> operations = new List<string>();
                        
                        RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, "Istanza provider SIAE"));

                        providerSIAE = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out Error, out operations, out oAssembly);

                        if (Error != null)
                            RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, "Istanza provider SIAE"), Error);

                        //providerSIAE = new libRiepiloghiSiae.clsRiepiloghiLibSiae();
                        string cDettaglioOper = "";
                        foreach (string itemOperation in operations)
                            cDettaglioOper += (cDettaglioOper.Trim() == "" ? "" : "\r\n") + itemOperation;

                        RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, cDettaglioOper));

                        clsRiepiloghi.OperazioniDiCreazione_UPD(oConnection, IdOperazione, "INIT_LIB_SIAE", 0, "InizializzazioneLibSiae0", cDettaglioOper, Error);
                        if (Error != null || providerSIAE == null)
                            Error = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore di inizializzazione librerie smartcard " + Error.Message);

                        if (Error == null)
                        {
                            RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, "Avvio creazione riepilogo"));
                            result = clsRiepiloghi.CreaRiepilogo(IdOperazione, Giorno, GiornalieroMensile, oConnection, percorsi, providerSIAE, out Error, out cFileLogP7M, out cFileRpgP7M, out cFileEmlP7M, out dettaglioRiepilogo);
                        }
                    }
                    catch (Exception exConnessione)
                    {
                        Error = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore di connessione al database", exConnessione);
                        if (Error != null)
                            RaiseMessaggesWinServiceServerOperation(string.Format("{0} {1}", cHeaderError, "Istanza provider SIAE/connessione"), Error);
                    }

                    try
                    {

                        clsParameters oPRResultOperation = new clsParameters();
                        oPRResultOperation.Add(":pIDOPERAZIONE", IdOperazione);


                        StringBuilder oSBResultOperation = new StringBuilder();
                        oSBResultOperation.Append("UPDATE RIEPILOGHI_OPERAZIONI_REQ SET");
                        oSBResultOperation.Append(" STATO_OPERAZIONE = :pSTATO_OPERAZIONE");

                        if (Error != null)
                        {
                            oPRResultOperation.Add(":pSTATO_OPERAZIONE", 2);
                            oSBResultOperation.Append(" , ERRORE = :pERRORE");
                            oPRResultOperation.Add(":pERRORE", Error.Message);
                        }
                        else
                            oPRResultOperation.Add(":pSTATO_OPERAZIONE", 10);

                        oSBResultOperation.Append(" WHERE IDOPERAZIONE = :pIDOPERAZIONE");

                        oConnection.ExecuteNonQuery(oSBResultOperation.ToString(), oPRResultOperation, true);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                Error = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore generico", ex);
            }


            // se non ho usato una connessione diretta chiudo quella locale creata in questa procedura
            if (this.DirectConnection == null)
            {
                try
                {
                    if (oConnection != null)
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oConnection = null;
                    }
                }
                catch (Exception ex)
                {
                }
            }

            if (oAssembly != null)
            {
                // dovrei scaricarlo
            }
            return result;
        }

        public bool SpedizioneEmail(DateTime Giorno, string GiornalieroMensile, Int64 Progressivo, IConnection oConnection, out Exception Error)
        {
            Error = null;
            bool result = false;

            libRiepiloghiBase.iLib_SIAE_Provider oSiaeProvider = null;

            return result;
        }
    }

    public class clsWinServiceServerPollingDbOperation
    {
        private string ServiceName = "";
        private TimeSpan PollingTimeSpan = new TimeSpan();
        private System.Threading.Thread ThreadExecuteServer = null;
        public bool Polling = false;
        public bool RequestStopPolling = false;
        public bool ExecutingServer = false;
        public Exception Error = null;
        public string ApplicationType = "";

        public delegate void delegateEventServerDatabaseRiepiloghi_Execute(clsWinServiceModel_request_Operation request, out Exception error);
        public event delegateEventServerDatabaseRiepiloghi_Execute EventServerDatabaseRiepiloghi_Execute;

        public delegate void delegateOutput(string message, Exception error);
        public event delegateOutput Output;


        public clsWinServiceServerPollingDbOperation(string dataSource, TimeSpan pollingTimeSpan, string applicationType)
        {
            this.ServiceName = dataSource;
            this.PollingTimeSpan = new TimeSpan(pollingTimeSpan.Hours, pollingTimeSpan.Minutes, pollingTimeSpan.Seconds);
            this.ApplicationType = applicationType;
        }

        public void StartPolling()
        {
            this.RequestStopPolling = false;
            this.ThreadExecuteServer = new System.Threading.Thread(StartPolling_Internal);
            this.ThreadExecuteServer.Start();
        }



        private void StartPolling_Internal()
        {
            this.Polling = true;
            SendOutput("Inizio polling", Error);
            try
            {

                while (!RequestStopPolling)
                {
                    SendOutput("Inizio ciclo di polling");
                    bool lRequestPending = false;
                    clsWinServiceModel_request_Operation request = null;
                    Wintic.Data.IConnection Connection = null;
                    try
                    {
                        Connection = clsWinServiceOperations.GetConnection(this.ServiceName);
                        clsWinServiceModel_Token token = null;
                        if (Connection != null && Connection.State == System.Data.ConnectionState.Open)
                        {
                            lRequestPending = clsWinServiceServerOperation.GetOperationToExecute(Connection, out request, out token, out Error, this.ApplicationType);
                        }
                        else
                        {
                            Error = new Exception("Polling: Errore connessione database");
                        }

                        try
                        {
                            if (Connection != null)
                            {
                                Connection.Close();
                                Connection.Dispose();
                                Connection = null;
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                    }
                    catch (Exception ex)
                    {
                        Error = ex;
                        SendOutput("Errore ciclo di polling", Error);
                    }

                    if (lRequestPending)
                    {
                        SendOutput("Richiesta pendente...", Error);
                        try
                        {
                            if (this.EventServerDatabaseRiepiloghi_Execute != null)
                            {
                                this.EventServerDatabaseRiepiloghi_Execute(request, out Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            Error = ex;
                            SendOutput("Errore esecuzione richiesta", Error);
                        }
                    }

                    if (RequestStopPolling)
                    {
                        SendOutput("Richiesta interruzione ciclo di polling", Error);
                    }
                    else
                    {
                        // Attesa
                        SendOutput("Inizio attesa ciclo di polling");
                        Stopwatch StopWatchWait = new Stopwatch();
                        StopWatchWait.Start();
                        while (StopWatchWait.ElapsedMilliseconds < this.PollingTimeSpan.TotalMilliseconds && !RequestStopPolling)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                        StopWatchWait.Stop();
                        SendOutput("Fine attesa ciclo di polling");
                    }
                    SendOutput("Fine ciclo di polling");

                }
            }
            catch (Exception ex)
            {
                Error = ex;
                SendOutput("Errore generico esecuzione Polling", Error);
            }
            this.Polling = false;
            SendOutput("Fine polling", Error);
        }

        public void StopPolling()
        {
            this.RequestStopPolling = true;
            while (Polling)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void SendOutput(string message)
        {
            SendOutput(message, null);
        }

        private void SendOutput(string message, Exception error)
        {
            if (Output != null) Output(message, error);
        }
    }

    public static class clsWinServiceOperations
    {

        public static Wintic.Data.IConnection GetConnection(string ServiceName)
        {
            Wintic.Data.IConnection oConnection = new Wintic.Data.oracle.CConnectionOracle();
            oConnection.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0}", ServiceName);
            try
            {
                oConnection.Open();
            }
            catch (Exception ex)
            {
            }
            return oConnection;
        }
    }

}
