﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Riepiloghi
{
    class clsFunzioniCarta
    {
        // Prende il numero di carta
        public static string GetSerial(int Slot, out Exception oError)
        {
            string cCodiceCarta = "";
            bool lRet = false;
            string cHeaderError = "Lettura smartcard";
            oError = null;
            try
            {
                string cFileResult = clsRiepiloghi.GetDirApp() + "\\GetSN.txt";
                System.Diagnostics.Process oRun = new System.Diagnostics.Process();
                oRun.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                oRun.StartInfo.WorkingDirectory = clsRiepiloghi.GetDirApp();
                oRun.StartInfo.FileName = "libSiae.exe";
                oRun.StartInfo.Arguments = "GET_SN SLOT=" + Slot.ToString();
                oRun.Start();
                oRun.WaitForExit();
                if (System.IO.File.Exists(cFileResult))
                {
                    string cResult = System.IO.File.ReadAllText(cFileResult);
                    if (cResult.Trim().StartsWith("SERIAL_NUMBER="))
                    {
                        cCodiceCarta = cResult.Replace("SERIAL_NUMBER=", "").Substring(0, 8);
                    }
                }
                lRet = (nRet == 0);
                if (!lRet)
                {
                    oError = clsRiepiloghi.GetNewException(cHeaderError, "connessione alla smartcard.");
                }
            }
            catch (Exception exInit)
            {
                oError = clsRiepiloghi.GetNewException(cHeaderError, "connessione alla smartcard.", exInit);
            }

            if (lRet)
            {
                try
                {
                    nRet = GetSN(out SerialNumber[0]);
                    lRet = (nRet == 0);
                    if (!lRet)
                    {
                        oError = clsRiepiloghi.GetNewException(cHeaderError, "lettura del codice della smartcard.");
                    }
                    else
                    {
                        StringBuilder oSB = new StringBuilder();
                        foreach (byte oByte in SerialNumber)
                        {
                            oSB.Append(System.Convert.ToChar(oByte));
                        }
                        cCodiceCarta = oSB.ToString();
                    }
                }
                catch (Exception exGetSN)
                {
                    oError = clsRiepiloghi.GetNewException(cHeaderError, "lettura del codice della smartcard.", exGetSN);
                }

                try
                {
                    nRet = Finalize();
                    lRet = lRet && (nRet == 0);
                    if (!lRet)
                    {
                        oError = clsRiepiloghi.GetNewException(cHeaderError, "disconnessione dalla smartcard.");
                    }
                }
                catch (Exception exFinalize)
                {
                    oError = clsRiepiloghi.GetNewException(cHeaderError, "disconnessione dalla smartcard.", exFinalize);
                }
            }


            return cCodiceCarta;
        }
    }
}
