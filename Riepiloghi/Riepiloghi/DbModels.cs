﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OracleClient;
using RcLib;

namespace Riepiloghi
{
    public static class myDbManager {
        public static DatabaseManager Current() { return new DatabaseManager("System.Data.OracleClient"); }
    }
    
    public class P_TEST_CURSORI : DbMapTable
    {
        public DbMapFieldString CCODICECARTA;
        public DbMapFieldDecimal NPROGRESSIVO;
        public DbMapFieldString CSIGILLO;
        public DbMapFieldCursor LOGCUR;
        public DbMapFieldCursor ERRCUR;


        public DbMapFieldCursor CAUCUR;

        
        public P_TEST_CURSORI(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "P_TEST_CURSORI";


            Command.CommandType = CommandType.StoredProcedure;
            CCODICECARTA = new DbMapFieldString(this, "CCODICECARTA");
            CCODICECARTA.Parameter.Direction = ParameterDirection.Input;
            CCODICECARTA.Parameter.Size = 32767;


            NPROGRESSIVO = new DbMapFieldDecimal(this, "NPROGRESSIVO");
            NPROGRESSIVO.Parameter.Direction = ParameterDirection.Input;
            CSIGILLO = new DbMapFieldString(this, "CSIGILLO");
            CSIGILLO.Parameter.Direction = ParameterDirection.Input;


            CSIGILLO.Parameter.Size = 32767;
            LOGCUR = new DbMapFieldCursor(this, "LOGCUR");
            LOGCUR.Parameter.Direction = ParameterDirection.Output;
            LOGCUR.CursorName = "LOGCUR";
            LOGCUR.IsSingleRow = true;


            LOGCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;
            ERRCUR = new DbMapFieldCursor(this, "ERRCUR");
            ERRCUR.Parameter.Direction = ParameterDirection.Output;
            ERRCUR.CursorName = "ERRCUR";
            ERRCUR.IsSingleRow = true;


            ERRCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;
            CAUCUR = new DbMapFieldCursor(this, "CAUCUR");
            CAUCUR.Parameter.Direction = ParameterDirection.Output;
            CAUCUR.CursorName = "CAUCUR";
            CAUCUR.IsSingleRow = false;


            CAUCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

        }


    }


    public class clsP_GET_LTA_RCA : DbMapTable
    {
        public DbMapFieldDateTime dGiornoMese;
        public DbMapFieldCursor ref_curLTA;
        public DbMapFieldDecimal nProgressivo;
        public DbMapFieldCursor ref_curRCA;
        public DbMapFieldDecimal nMAX_LOG_ID_OUT;

        public clsP_GET_LTA_RCA(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "WTIC.P_GET_LTA_RCA";
            Command.CommandType = CommandType.StoredProcedure;

            //P_RIEPILOGHI_CREATE(dGiornoMese , cGiornalieroMensile , cFlagStorico , ref_curLOGTRANS OUT SYS_REFCURSOR, nProgNextLOG OUT NUMBER, ref_curRIEPILOGHI_BASE OUT SYS_REFCURSOR, ref_curTITOLI_EVENTI OUT SYS_REFCURSOR)

            dGiornoMese = new DbMapFieldDateTime(this, "dGiornoMese");
            dGiornoMese.Parameter.Direction = ParameterDirection.Input;

            ref_curLTA = new DbMapFieldCursor(this, "ref_curLTA");
            ref_curLTA.Parameter.Direction = ParameterDirection.Output;
            ref_curLTA.CursorName = "ref_curLTA";
            ref_curLTA.IsSingleRow = false;
            ref_curLTA.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            nProgressivo = new DbMapFieldDecimal(this, "nProgressivo");
            nProgressivo.Parameter.Direction = ParameterDirection.Input;

            ref_curRCA = new DbMapFieldCursor(this, "ref_curRCA");
            ref_curRCA.Parameter.Direction = ParameterDirection.Output;
            ref_curRCA.CursorName = "ref_curRCA";
            ref_curRCA.IsSingleRow = false;
            ref_curRCA.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            nMAX_LOG_ID_OUT = new DbMapFieldDecimal(this, "nMAX_LOG_ID_OUT");
            nMAX_LOG_ID_OUT.Parameter.Direction = ParameterDirection.Output;
        }
    }

    public class clsP_REQ_ANNULLO_TRANSAZIONE: DbMapTable
    {
        public DbMapFieldString cCodiceCarta;
        public DbMapFieldDecimal nProgressivo;
        public DbMapFieldString cSigillo;
        public DbMapFieldDecimal nFlagLta;

        public DbMapFieldCursor logCUR;
        public DbMapFieldCursor errCUR;
        public DbMapFieldCursor wrnCUR;
        public DbMapFieldCursor cauCUR;
        public DbMapFieldCursor logANN;
        

        public clsP_REQ_ANNULLO_TRANSAZIONE(DatabaseManager parDatabaseManager):base(parDatabaseManager) {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "WTIC.P_REQ_ANNULLO_TRANSAZIONE";
            Command.CommandType = CommandType.StoredProcedure;

            cCodiceCarta = new DbMapFieldString(this, "cCodiceCarta");
            cCodiceCarta.Parameter.Direction = ParameterDirection.Input;
            cCodiceCarta.Parameter.Size = 32767;

            nProgressivo = new DbMapFieldDecimal(this, "nProgressivo");
            nProgressivo.Parameter.Direction = ParameterDirection.Input;

            cSigillo = new DbMapFieldString(this, "cSigillo");
            cSigillo.Parameter.Direction = ParameterDirection.Input;
            cSigillo.Parameter.Size = 32767;

            nFlagLta = new DbMapFieldDecimal(this, "nFlagLta");
            nFlagLta.Parameter.Direction = ParameterDirection.Input;

            logCUR = new DbMapFieldCursor(this, "logCUR");
            logCUR.Parameter.Direction = ParameterDirection.Output;
            logCUR.CursorName = "logCUR";
            logCUR.IsSingleRow = true;
            logCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            errCUR = new DbMapFieldCursor(this, "errCUR");
            errCUR.Parameter.Direction = ParameterDirection.Output;
            errCUR.CursorName = "errCUR";
            errCUR.IsSingleRow = true;
            errCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;


            wrnCUR = new DbMapFieldCursor(this, "wrnCUR");
            wrnCUR.Parameter.Direction = ParameterDirection.Output;
            wrnCUR.CursorName = "wrnCUR";
            wrnCUR.IsSingleRow = true;
            wrnCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            cauCUR = new DbMapFieldCursor(this, "cauCUR");
            cauCUR.Parameter.Direction = ParameterDirection.Output;
            cauCUR.CursorName = "cauCUR";
            cauCUR.IsSingleRow = false;
            cauCUR.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            logANN = new DbMapFieldCursor(this, "logANN");
            logANN.Parameter.Direction = ParameterDirection.Output;
            logANN.CursorName = "logANN";
            logANN.IsSingleRow = true;
            logANN.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;
        }


    }

    public class clsP_RIEPILOGHI_CREATE_OBJECT : DbMapTable
    {
        public DbMapFieldDateTime dGiornoMese;
        public DbMapFieldString cGiornalieroMensile;
        public DbMapFieldString cFlagStorico;
        public DbMapFieldDecimal nProgNextLOG;
        public DbMapFieldDecimal nMAX_LOG_ID_OUT;

        public DbMapFieldCursor ref_curLOGTRANS;
        public DbMapFieldCursor ref_curRIEPILOGHI_BASE;
        public DbMapFieldCursor ref_curTITOLI_EVENTI;

        public DbMapFieldCursor ref_curLTA;
        public DbMapFieldDecimal nProgNextLTA;
        public DbMapFieldCursor ref_curRCA;

        public clsP_RIEPILOGHI_CREATE_OBJECT(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "WTIC.P_RIEPILOGHI_CREATE";
            Command.CommandType = CommandType.StoredProcedure;

            //P_RIEPILOGHI_CREATE(dGiornoMese , cGiornalieroMensile , cFlagStorico , ref_curLOGTRANS OUT SYS_REFCURSOR, nProgNextLOG OUT NUMBER, ref_curRIEPILOGHI_BASE OUT SYS_REFCURSOR, ref_curTITOLI_EVENTI OUT SYS_REFCURSOR)

            dGiornoMese = new DbMapFieldDateTime(this, "dGiornoMese");
            dGiornoMese.Parameter.Direction = ParameterDirection.Input;

            cGiornalieroMensile = new DbMapFieldString(this, "cGiornalieroMensile");
            cGiornalieroMensile.Parameter.Direction = ParameterDirection.Input;
            cGiornalieroMensile.Parameter.Size = 32767;

            cFlagStorico = new DbMapFieldString(this, "cFlagStorico");
            cFlagStorico.Parameter.Direction = ParameterDirection.Input;
            cFlagStorico.Parameter.Size = 32767;

            nProgNextLOG = new DbMapFieldDecimal(this, "nProgNextLOG");
            nProgNextLOG.Parameter.Direction = ParameterDirection.Output;

            ref_curLOGTRANS = new DbMapFieldCursor(this, "ref_curLOGTRANS");
            ref_curLOGTRANS.Parameter.Direction = ParameterDirection.Output;
            ref_curLOGTRANS.CursorName = "ref_curLOGTRANS";
            ref_curLOGTRANS.IsSingleRow = false;
            ref_curLOGTRANS.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            ref_curRIEPILOGHI_BASE = new DbMapFieldCursor(this, "ref_curRIEPILOGHI_BASE");
            ref_curRIEPILOGHI_BASE.Parameter.Direction = ParameterDirection.Output;
            ref_curRIEPILOGHI_BASE.CursorName = "ref_curRIEPILOGHI_BASE";
            ref_curRIEPILOGHI_BASE.IsSingleRow = false;
            ref_curRIEPILOGHI_BASE.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            ref_curTITOLI_EVENTI = new DbMapFieldCursor(this, "ref_curTITOLI_EVENTI");
            ref_curTITOLI_EVENTI.Parameter.Direction = ParameterDirection.Output;
            ref_curTITOLI_EVENTI.CursorName = "ref_curTITOLI_EVENTI";
            ref_curTITOLI_EVENTI.IsSingleRow = false;
            ref_curTITOLI_EVENTI.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;


            ref_curLTA = new DbMapFieldCursor(this, "ref_curLTA");
            ref_curLTA.Parameter.Direction = ParameterDirection.Output;
            ref_curLTA.CursorName = "ref_curLTA";
            ref_curLTA.IsSingleRow = false;
            ref_curLTA.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            nProgNextLTA = new DbMapFieldDecimal(this, "nProgNextLTA");
            nProgNextLTA.Parameter.Direction = ParameterDirection.Output;

            ref_curRCA = new DbMapFieldCursor(this, "ref_curRCA");
            ref_curRCA.Parameter.Direction = ParameterDirection.Output;
            ref_curRCA.CursorName = "ref_curRCA";
            ref_curRCA.IsSingleRow = false;
            ref_curRCA.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            nMAX_LOG_ID_OUT = new DbMapFieldDecimal(this, "nMAX_LOG_ID_OUT");
            nMAX_LOG_ID_OUT.Parameter.Direction = ParameterDirection.Output;
        }


    }


    public class clsP_CALC_RIEPILOGHI_PRINT_OBJECT : DbMapTable
    {
        public DbMapFieldDateTime dDataEmiInizio;
        public DbMapFieldDateTime dDataEmiFine;
        public DbMapFieldString cGiornalieroMensile;
        public DbMapFieldString cFlagStorico;
        public DbMapFieldDecimal nMAX_LOG_ID;
        public DbMapFieldDecimal nIdFilterExtIdVend;

        public DbMapFieldCursor ref_curRIEPILOGHI_PRINT;
        public DbMapFieldCursor ref_curTITOLI_EVENTI;

        public clsP_CALC_RIEPILOGHI_PRINT_OBJECT(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "WTIC.P_CALC_RIEPILOGHI_PRINT";
            Command.CommandType = CommandType.StoredProcedure;

            dDataEmiInizio = new DbMapFieldDateTime(this, "dDataEmiInizio");
            dDataEmiInizio.Parameter.Direction = ParameterDirection.Input;

            dDataEmiFine = new DbMapFieldDateTime(this, "dDataEmiFine");
            dDataEmiFine.Parameter.Direction = ParameterDirection.Input;

            cGiornalieroMensile = new DbMapFieldString(this, "cGiornalieroMensile");
            cGiornalieroMensile.Parameter.Direction = ParameterDirection.Input;
            cGiornalieroMensile.Parameter.Size = 32767;

            cFlagStorico = new DbMapFieldString(this, "cFlagStorico");
            cFlagStorico.Parameter.Direction = ParameterDirection.Input;
            cFlagStorico.Parameter.Size = 32767;

            nMAX_LOG_ID = new DbMapFieldDecimal(this, "nMAX_LOG_ID");
            nMAX_LOG_ID.Parameter.Direction = ParameterDirection.Input;

            nIdFilterExtIdVend = new DbMapFieldDecimal(this, "nIdFilterExtIdVend");
            nIdFilterExtIdVend.Parameter.Direction = ParameterDirection.Input;

            ref_curRIEPILOGHI_PRINT = new DbMapFieldCursor(this, "ref_curRIEPILOGHI_PRINT");
            ref_curRIEPILOGHI_PRINT.Parameter.Direction = ParameterDirection.Output;
            ref_curRIEPILOGHI_PRINT.CursorName = "ref_curRIEPILOGHI_PRINT";
            ref_curRIEPILOGHI_PRINT.IsSingleRow = false;
            ref_curRIEPILOGHI_PRINT.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

            ref_curTITOLI_EVENTI = new DbMapFieldCursor(this, "ref_curTITOLI_EVENTI");
            ref_curTITOLI_EVENTI.Parameter.Direction = ParameterDirection.Output;
            ref_curTITOLI_EVENTI.CursorName = "ref_curTITOLI_EVENTI";
            ref_curTITOLI_EVENTI.IsSingleRow = false;
            ref_curTITOLI_EVENTI.ParameterAs<OracleParameter>().OracleType = OracleType.Cursor;

        }

    }

    public class clsP_CALC_IVAISI_R_OMAGGI_ECC_OBJECT_P : DbMapTable
    {
        public DbMapFieldString cTipoEvento;
        public DbMapFieldString cSpettacoloIntrattenimento;
        public DbMapFieldDateTime dDataValidita;
        public DbMapFieldDecimal nIncidenzaIntrattenimento;
        public DbMapFieldDecimal nCorrispettivoTitolo;
        public DbMapFieldDecimal nPercIva;
        public DbMapFieldDecimal nPercIsi;
        public DbMapFieldDecimal nNettoTitolo;
        public DbMapFieldDecimal nImponibileImpostaIntra;
        public DbMapFieldDecimal nIvaTitolo;
        public DbMapFieldDecimal nIsiTotale;
        public DbMapFieldString cFlagIrripartibile;
        public DbMapFieldDecimal nIdCalend;

        public clsP_CALC_IVAISI_R_OMAGGI_ECC_OBJECT_P(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "WTIC.P_CALC_IVAISI_R_OMAGGI_ECC_P";
            Command.CommandType = CommandType.StoredProcedure;

            cTipoEvento = new DbMapFieldString(this, "cTipoEvento");
            cTipoEvento.Parameter.Direction = ParameterDirection.Input;
            cTipoEvento.Parameter.Size = 32767;

            cSpettacoloIntrattenimento = new DbMapFieldString(this, "cSpettacoloIntrattenimento");
            cSpettacoloIntrattenimento.Parameter.Direction = ParameterDirection.Input;
            cSpettacoloIntrattenimento.Parameter.Size = 32767;

            dDataValidita = new DbMapFieldDateTime(this, "dDataValidita");
            dDataValidita.Parameter.Direction = ParameterDirection.Input;

            nIncidenzaIntrattenimento = new DbMapFieldDecimal(this, "nIncidenzaIntrattenimento");
            nIncidenzaIntrattenimento.Parameter.Direction = ParameterDirection.Input;

            nCorrispettivoTitolo = new DbMapFieldDecimal(this, "nCorrispettivoTitolo");
            nCorrispettivoTitolo.Parameter.Direction = ParameterDirection.Input;

            nPercIva = new DbMapFieldDecimal(this, "nPercIva");
            nPercIva.Parameter.Direction = ParameterDirection.InputOutput;

            nPercIsi = new DbMapFieldDecimal(this, "nPercIsi");
            nPercIsi.Parameter.Direction = ParameterDirection.Input;

            nNettoTitolo = new DbMapFieldDecimal(this, "nNettoTitolo");
            nNettoTitolo.Parameter.Direction = ParameterDirection.Output;

            nImponibileImpostaIntra = new DbMapFieldDecimal(this, "nImponibileImpostaIntra");
            nImponibileImpostaIntra.Parameter.Direction = ParameterDirection.Output;

            nIvaTitolo = new DbMapFieldDecimal(this, "nIvaTitolo");
            nIvaTitolo.Parameter.Direction = ParameterDirection.Output;

            nIsiTotale = new DbMapFieldDecimal(this, "nIsiTotale");
            nIsiTotale.Parameter.Direction = ParameterDirection.Output;

            cFlagIrripartibile = new DbMapFieldString(this, "cFlagIrripartibile");
            cFlagIrripartibile.Parameter.Direction = ParameterDirection.Output;
            cFlagIrripartibile.Parameter.Size = 32767;

            nIdCalend = new DbMapFieldDecimal(this, "nIdCalend");
            nIdCalend.Parameter.Direction = ParameterDirection.Input;
        }
    }

    #region "Serialiazzazione LOG"

    public class clsXml_Complementare
    {
        public string CodicePrestazione { get; set; }
        public string ImportoPrestazione { get; set; }
    }

    public class clsXml_Partecipante
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
    }

    public class clsXml_TitoloAccesso
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "Annullamento")]
        public string Annullamento { get; set; }
        public string CorrispettivoLordo { get; set; }
        public string Prevendita { get; set; }
        public string IVACorrispettivo { get; set; }
        public string IVAPrevendita { get; set; }
        public string ImportoFigurativo { get; set; }
        public string IVAFigurativa { get; set; }

        public string CodiceLocale { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string TipoGenere { get; set; }
        public string Titolo { get; set; }
        //public clsXml_Complementare[] Complementare { get; set; }
        public clsXml_Partecipante Partecipante { get; set; }
    }

    public class clsXml_BigliettoAbbonamento
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "Annullamento")]
        public string Annullamento { get; set; }
        public string CodiceLocale { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string TipoGenere { get; set; }
        public string Titolo { get; set; }
        public string CodiceAbbonamento { get; set; }
        public long? ProgressivoAbbonamento { get; set; }
        public string CodiceFiscale { get; set; }
        public string ImportoFigurativo { get; set; }
        public string IVAFigurativa { get; set; }
        public clsXml_Partecipante Partecipante { get; set; }
    }

    public class clsXml_Turno
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "valore")]
        public string valore { get; set; }
    }

    public class clsXml_Abbonamento
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "Annullamento")]
        public string Annullamento { get; set; }
        public string CodiceAbbonamento { get; set; }
        public long? ProgressivoAbbonamento { get; set; }
        public clsXml_Turno Turno { get; set; }
        public long? QuantitaEventiAbilitati { get; set; }
        public string Validita { get; set; }
        public string Rateo { get; set; }
        public string RateoIntrattenimenti { get; set; }
        public string RateoIVA { get; set; }

        public string CorrispettivoLordo { get; set; }
        public string Prevendita { get; set; }
        public string IVACorrispettivo { get; set; }
        public string IVAPrevendita { get; set; }
        public string ImportoFigurativo { get; set; }
        public string IVAFigurativa { get; set; }
        public clsXml_Partecipante Partecipante { get; set; }
    }

    public class clsXml_Transazione
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "CFOrganizzatore")]
        public string CFOrganizzatore { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CFTitolare")]
        public string CFTitolare { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "IVAPreassolta")]
        public string IVAPreassolta { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "TipoTassazione")]
        public string TipoTassazione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Valuta")]
        public string Valuta { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "SistemaEmissione")]
        public string SistemaEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CartaAttivazione")]
        public string CartaAttivazione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "SigilloFiscale")]
        public string SigilloFiscale { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataEmissione")]
        public string DataEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraEmissione")]
        public string OraEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "NumeroProgressivo")]
        public long NumeroProgressivo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "TipoTitolo")]
        public string TipoTitolo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodiceOrdine")]
        public string CodiceOrdine { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Causale")]
        public string Causale { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Posto")]
        public string Posto { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodiceRichiedenteEmissioneSigillo")]
        public string CodiceRichiedenteEmissioneSigillo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Prestampa")]
        public string Prestampa { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "ImponibileIntrattenimenti")]
        public string ImponibileIntrattenimenti { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OriginaleAnnullato")]
        public string OriginaleAnnullato { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CartaOriginaleAnnullato")]
        public string CartaOriginaleAnnullato { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CausaleAnnullamento")]
        public string CausaleAnnullamento { get; set; }
        public clsXml_TitoloAccesso TitoloAccesso { get; set; }
        public clsXml_Abbonamento Abbonamento { get; set; }
        public clsXml_BigliettoAbbonamento BigliettoAbbonamento { get; set; }

        //public clsXml_RiferimentoAnnullamento RiferimentoAnnullamento { get; set; }
    }

    [System.Xml.Serialization.XmlRoot(ElementName = "LogTransazione", Namespace = "")]
    public class clsXml_LogTransazione
    {
        [System.Xml.Serialization.XmlElement("Transazione")]
        public List<clsXml_Transazione> Transazione { get; set; }
    }

    public static class clsXml_SerializeLOG
    {

        

        public static string SerializeTransazione(clsXml_LogTransazione oTransazione)
        {
            string value = "";
            using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
            {
                System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
                xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
                xmlWriterSettings.OmitXmlDeclaration = true;
                xmlWriterSettings.Indent = true;
                xmlWriterSettings.CloseOutput = false;
                using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
                {
                    System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    xmlNameSpaces.Add("", "");

                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(clsXml_LogTransazione));
                    xmlSerializer.Serialize(xmlWriter, oTransazione, xmlNameSpaces);
                    value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
                    xmlWriter.Dispose();
                }
                xmlStream.Dispose();
            }
            return value;
        }

        public static string SerializeTransazioni(clsXml_LogTransazione oLogTransazione, out Exception oError)
        {
            oError = null;
            string value = "";
            try
            {
                using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
                {
                    System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
                    xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
                    xmlWriterSettings.OmitXmlDeclaration = false;
                    xmlWriterSettings.Indent = true;
                    xmlWriterSettings.CloseOutput = false;
                    System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    xmlNameSpaces.Add("", "");

                    using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(clsXml_LogTransazione));

                        xmlWriter.WriteDocType("LogTransazione", null, "Log_v0040_20190627.dtd", null);

                        xmlSerializer.Serialize(xmlWriter, oLogTransazione, xmlNameSpaces);

                        xmlWriter.Dispose();
                    }
                    value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
                    xmlStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                oError = ex; // clsRiepiloghi.GetNewException("Serializzazione transazioni", "Errore durante la serialiazzione", ex);
                value = "";
            }
            return value;
        }

        public static string SerializeTransazioni(DataTable tabTransazioni, out Exception oError)
        {
            clsXml_LogTransazione oLogTransazione = new clsXml_LogTransazione();
            oLogTransazione.Transazione = new List<clsXml_Transazione>();
            oError = null;
            string value = "";
            try
            {
                foreach(DataRow oRow in tabTransazioni.Rows)
                {
                    clsXml_Transazione transazione = new clsXml_Transazione();

                    transazione.CFOrganizzatore = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? "" : oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString());
                    transazione.CFTitolare = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_TITOLARE"].ToString()) ? "" : oRow["CODICE_FISCALE_TITOLARE"].ToString());
                    transazione.IVAPreassolta = (string.IsNullOrEmpty(oRow["TITOLO_IVA_PREASSOLTA"].ToString()) ? "" : oRow["TITOLO_IVA_PREASSOLTA"].ToString());
                    transazione.TipoTassazione = (string.IsNullOrEmpty(oRow["SPETTACOLO_INTRATTENIMENTO"].ToString()) ? "" : oRow["SPETTACOLO_INTRATTENIMENTO"].ToString());
                    transazione.Valuta = (string.IsNullOrEmpty(oRow["VALUTA"].ToString()) ? "" : oRow["VALUTA"].ToString());
                    transazione.SistemaEmissione = (string.IsNullOrEmpty(oRow["CODICE_SISTEMA"].ToString()) ? "" : oRow["CODICE_SISTEMA"].ToString());
                    transazione.CartaAttivazione = (string.IsNullOrEmpty(oRow["CODICE_CARTA"].ToString()) ? "" : oRow["CODICE_CARTA"].ToString());
                    transazione.SigilloFiscale = (string.IsNullOrEmpty(oRow["SIGILLO"].ToString()) ? "" : oRow["SIGILLO"].ToString());
                    transazione.DataEmissione = (string.IsNullOrEmpty(oRow["DATA_EMISSIONE_ANNULLAMENTO"].ToString()) ? "" : oRow["DATA_EMISSIONE_ANNULLAMENTO"].ToString());
                    transazione.OraEmissione = (string.IsNullOrEmpty(oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString()) ? "" : oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO"].ToString()))
                        transazione.NumeroProgressivo = Int64.Parse(oRow["PROGRESSIVO_TITOLO"].ToString());
                    transazione.TipoTitolo = (string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? "" : oRow["TIPO_TITOLO"].ToString());
                    transazione.CodiceOrdine = (string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? "" : oRow["ORDINE_DI_POSTO"].ToString());
                    transazione.Causale = (string.IsNullOrEmpty(oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString()) ? "" : oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString());
                    transazione.Posto = (string.IsNullOrEmpty(oRow["POSTO"].ToString()) ? "" : oRow["POSTO"].ToString());
                    transazione.CodiceRichiedenteEmissioneSigillo = (string.IsNullOrEmpty(oRow["CODICE_PUNTO_VENDITA"].ToString()) ? "" : oRow["CODICE_PUNTO_VENDITA"].ToString());
                    transazione.Prestampa = (string.IsNullOrEmpty(oRow["PRESTAMPA"].ToString()) ? "" : oRow["PRESTAMPA"].ToString());
                    transazione.ImponibileIntrattenimenti = "";
                    if (!string.IsNullOrEmpty(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString()))
                        transazione.ImponibileIntrattenimenti = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString()));

                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_ANNULLATI"].ToString()))
                        transazione.OriginaleAnnullato = Int64.Parse(oRow["PROGRESSIVO_ANNULLATI"].ToString()).ToString().Trim();
                    transazione.CartaOriginaleAnnullato = (string.IsNullOrEmpty(oRow["CARTA_ORIGINALE_ANNULLATO"].ToString()) ? "" : oRow["CARTA_ORIGINALE_ANNULLATO"].ToString().Trim());
                    transazione.CausaleAnnullamento = (string.IsNullOrEmpty(oRow["CAUSALE_ANNULLAMENTO"].ToString()) ? "" : oRow["CAUSALE_ANNULLAMENTO"].ToString().Trim());

                    string cTitoloAbbonamento = (string.IsNullOrEmpty(oRow["TITOLO_ABBONAMENTO"].ToString()) ? "" : oRow["TITOLO_ABBONAMENTO"].ToString());

                    if (cTitoloAbbonamento == "T")
                    {
                        if (transazione.IVAPreassolta == "N")
                        {
                            transazione.TitoloAccesso = new clsXml_TitoloAccesso();

                            transazione.TitoloAccesso.Annullamento = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "N" : (oRow["ANNULLATO"].ToString() == "A" ? "S" : "N"));

                            transazione.TitoloAccesso.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? "" : oRow["CODICE_LOCALE"].ToString());
                            transazione.TitoloAccesso.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? "" : oRow["DATA_EVENTO"].ToString());
                            transazione.TitoloAccesso.TipoGenere = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? "" : oRow["TIPO_EVENTO"].ToString());
                            transazione.TitoloAccesso.Titolo = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? "" : oRow["TITOLO_EVENTO"].ToString());
                            transazione.TitoloAccesso.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? "" : oRow["ORA_EVENTO"].ToString());

                            transazione.TitoloAccesso.CorrispettivoLordo = "";
                            transazione.TitoloAccesso.Prevendita = "";
                            transazione.TitoloAccesso.IVACorrispettivo = "";
                            transazione.TitoloAccesso.IVAPrevendita = "";

                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_TITOLO"].ToString())) transazione.TitoloAccesso.CorrispettivoLordo = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_TITOLO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_TITOLO"].ToString())) transazione.TitoloAccesso.IVACorrispettivo = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_TITOLO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_PREVENDITA"].ToString())) transazione.TitoloAccesso.Prevendita = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_PREVENDITA"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_PREVENDITA"].ToString())) transazione.TitoloAccesso.IVAPrevendita = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_PREVENDITA"].ToString()));

                            List<clsXml_Complementare> listaComplementari = new List<clsXml_Complementare>();
                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE1"].ToString()) && oRow["CODICE_PRESTAZIONE1"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE1"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE1"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE1"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE2"].ToString()) && oRow["CODICE_PRESTAZIONE2"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE2"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE2"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE2"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE3"].ToString()) && oRow["CODICE_PRESTAZIONE3"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE3"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE3"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            //if (listaComplementari.Count > 0)
                            //{
                            //    transazione.TitoloAccesso.Complementare = listaComplementari.ToArray();
                            //}
                        }
                        else if (transazione.IVAPreassolta == "F")
                        {
                            transazione.TitoloAccesso = new clsXml_TitoloAccesso();

                            transazione.TitoloAccesso.Annullamento = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "N" : (oRow["ANNULLATO"].ToString() == "A" ? "S" : "N"));

                            transazione.TitoloAccesso.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? "" : oRow["CODICE_LOCALE"].ToString());
                            transazione.TitoloAccesso.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? "" : oRow["DATA_EVENTO"].ToString());
                            transazione.TitoloAccesso.TipoGenere = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? "" : oRow["TIPO_EVENTO"].ToString());
                            transazione.TitoloAccesso.Titolo = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? "" : oRow["TITOLO_EVENTO"].ToString());
                            transazione.TitoloAccesso.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? "" : oRow["ORA_EVENTO"].ToString());

                            transazione.TitoloAccesso.ImportoFigurativo = "";
                            transazione.TitoloAccesso.IVAFigurativa = "";
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_FIGURATIVO"].ToString())) transazione.TitoloAccesso.ImportoFigurativo = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_FIGURATIVO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_FIGURATIVA"].ToString())) transazione.TitoloAccesso.IVAFigurativa = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_FIGURATIVA"].ToString()));

                            List<clsXml_Complementare> listaComplementari = new List<clsXml_Complementare>();
                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE1"].ToString()) && oRow["CODICE_PRESTAZIONE1"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE1"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE1"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE1"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE2"].ToString()) && oRow["CODICE_PRESTAZIONE2"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE2"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE2"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE2"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            if (!string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE3"].ToString()) && oRow["CODICE_PRESTAZIONE3"].ToString().Trim() != "")
                            {
                                clsXml_Complementare oComplementare = new clsXml_Complementare();
                                oComplementare.CodicePrestazione = oRow["CODICE_PRESTAZIONE3"].ToString();
                                oComplementare.ImportoPrestazione = "";
                                if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) oComplementare.ImportoPrestazione = GetCurrencyValueIntPer100(decimal.Parse(oRow["IMPORTO_PRESTAZIONE3"].ToString()));
                                listaComplementari.Add(oComplementare);
                            }

                            //if (listaComplementari.Count > 0)
                            //{
                            //    transazione.TitoloAccesso.Complementare = listaComplementari.ToArray();
                            //}

                        }
                        else if (transazione.IVAPreassolta == "B")
                        {
                            transazione.BigliettoAbbonamento = new clsXml_BigliettoAbbonamento();

                            transazione.BigliettoAbbonamento.Annullamento = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "N" : (oRow["ANNULLATO"].ToString() == "A" ? "S" : "N"));

                            transazione.BigliettoAbbonamento.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? "" : oRow["CODICE_LOCALE"].ToString());
                            transazione.BigliettoAbbonamento.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? "" : oRow["DATA_EVENTO"].ToString());
                            transazione.BigliettoAbbonamento.TipoGenere = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? "" : oRow["TIPO_EVENTO"].ToString());
                            transazione.BigliettoAbbonamento.Titolo = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? "" : oRow["TITOLO_EVENTO"].ToString());
                            transazione.BigliettoAbbonamento.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? "" : oRow["ORA_EVENTO"].ToString());

                            transazione.BigliettoAbbonamento.CodiceAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString());
                            transazione.BigliettoAbbonamento.ProgressivoAbbonamento = 0;
                            if (!string.IsNullOrEmpty(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString())) transazione.BigliettoAbbonamento.ProgressivoAbbonamento = Int64.Parse(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString());

                            transazione.BigliettoAbbonamento.ImportoFigurativo = "";
                            transazione.BigliettoAbbonamento.IVAFigurativa = "";
                            transazione.BigliettoAbbonamento.CodiceFiscale = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_FISCALE_ABBONAMENTO"].ToString());
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_FIGURATIVO"].ToString())) transazione.BigliettoAbbonamento.ImportoFigurativo = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_FIGURATIVO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_FIGURATIVA"].ToString())) transazione.BigliettoAbbonamento.IVAFigurativa = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_FIGURATIVA"].ToString()));
                        }
                    }
                    else if (cTitoloAbbonamento == "A")
                    {
                        transazione.Abbonamento = new clsXml_Abbonamento();

                        transazione.Abbonamento.Annullamento = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "N" : (oRow["ANNULLATO"].ToString() == "A" ? "S" : "N"));
                        transazione.Abbonamento.CodiceAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_ABBONAMENTO"].ToString());
                        transazione.Abbonamento.ProgressivoAbbonamento = 0;
                        if (!string.IsNullOrEmpty(oRow["NUM_PROG_ABBONAMENTO"].ToString())) transazione.Abbonamento.ProgressivoAbbonamento = Int64.Parse(oRow["NUM_PROG_ABBONAMENTO"].ToString());
                        transazione.Abbonamento.Turno = new clsXml_Turno();
                        transazione.Abbonamento.Turno.valore = "";
                        transazione.Abbonamento.Turno.valore = (string.IsNullOrEmpty(oRow["TIPO_TURNO"].ToString()) ? "L" : (oRow["TIPO_TURNO"].ToString() == "F" ? "F" : "L"));

                        transazione.Abbonamento.QuantitaEventiAbilitati = 0;
                        if (!string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString())) transazione.Abbonamento.QuantitaEventiAbilitati = Int64.Parse(oRow["NUMERO_EVENTI_ABILITATI"].ToString());

                        transazione.Abbonamento.Validita = (string.IsNullOrEmpty(oRow["DATA_LIMITE_VALIDITA"].ToString()) ? "" : oRow["DATA_LIMITE_VALIDITA"].ToString()); ;
                        transazione.Abbonamento.Rateo = "";
                        transazione.Abbonamento.RateoIVA = "";
                        transazione.Abbonamento.RateoIntrattenimenti = "";

                        if (!string.IsNullOrEmpty(oRow["RATEO_EVENTO"].ToString())) transazione.Abbonamento.Rateo = GetCurrencyValueIntPer100(decimal.Parse(oRow["RATEO_EVENTO"].ToString()));
                        if (!string.IsNullOrEmpty(oRow["IVA_RATEO"].ToString())) transazione.Abbonamento.RateoIVA = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_RATEO"].ToString()));
                        if (!string.IsNullOrEmpty(oRow["RATEO_IMPONIBILE_INTRA"].ToString())) transazione.Abbonamento.RateoIntrattenimenti = GetCurrencyValueIntPer100(decimal.Parse(oRow["RATEO_IMPONIBILE_INTRA"].ToString()));

                        if (transazione.IVAPreassolta == "N")
                        {
                            transazione.Abbonamento.CorrispettivoLordo = "";
                            transazione.Abbonamento.IVACorrispettivo = "";
                            transazione.Abbonamento.Prevendita = "";
                            transazione.Abbonamento.IVAPrevendita = "";
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_TITOLO"].ToString())) transazione.Abbonamento.CorrispettivoLordo = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_TITOLO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_TITOLO"].ToString())) transazione.Abbonamento.IVACorrispettivo = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_TITOLO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_PREVENDITA"].ToString())) transazione.Abbonamento.Prevendita = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_PREVENDITA"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_PREVENDITA"].ToString())) transazione.Abbonamento.IVAPrevendita = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_PREVENDITA"].ToString()));
                        }
                        else if (transazione.IVAPreassolta == "F" || transazione.IVAPreassolta == "B")
                        {
                            transazione.Abbonamento.ImportoFigurativo = "";
                            transazione.Abbonamento.IVAFigurativa = "";
                            if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_FIGURATIVO"].ToString())) transazione.Abbonamento.ImportoFigurativo = GetCurrencyValueIntPer100(decimal.Parse(oRow["CORRISPETTIVO_FIGURATIVO"].ToString()));
                            if (!string.IsNullOrEmpty(oRow["IVA_FIGURATIVA"].ToString())) transazione.Abbonamento.IVAFigurativa = GetCurrencyValueIntPer100(decimal.Parse(oRow["IVA_FIGURATIVA"].ToString()));
                        }
                    }

                    oLogTransazione.Transazione.Add(transazione);
                }
                value = SerializeTransazioni(oLogTransazione, out oError); 
            }
            catch (Exception ex)
            {
                oError = ex; // clsRiepiloghi.GetNewException("Serializzazione tabella transazioni", "Errore", ex);
                value = "";
            }
            return value;
        }



        private static string GetValueIntToString(decimal value)
        {
            decimal nIntValue = System.Math.Truncate(value);
            long? result = long.Parse(nIntValue.ToString());
            return result.ToString().Trim();
        }

        private static string GetCurrencyValueIntPer100(decimal value)
        {
            decimal nIntValue = System.Math.Truncate(value * 100);
            long? result = long.Parse(nIntValue.ToString());
            return result.ToString().Trim();
        }
    }

    #endregion


    #region "Serializzazione Lta"

    public class clsXml_LTA_Partecipante
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string DataNascita { get; set; }
        public string LuogoNascita { get; set; }
    }

    public class clsXml_LTA_TitoloAccesso
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "SistemaEmissione")]
        public string SistemaEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CartaAttivazione")]
        public string CartaAttivazione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "ProgressivoFiscale")]
        public string ProgressivoFiscale { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "SigilloFiscale")]
        public string SigilloFiscale { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataEmissione")]
        public string DataEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraEmissione")]
        public string OraEmissione { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataLTA")]
        public string DataLTA { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraLTA")]
        public string OraLTA { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "TipoTitolo")]
        public string TipoTitolo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodiceOrdine")]
        public string CodiceOrdine { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CorrispettivoLordo")]
        public string CorrispettivoLordo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Posto")]
        public string Posto { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Abbonamento")]
        public string Abbonamento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CFAbbonamento")]
        public string CFAbbonamento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodiceAbbonamento")]
        public string CodiceAbbonamento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "ProgressivoAbbonamento")]
        public string ProgressivoAbbonamento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "QEventiAbilitati")]
        public string QEventiAbilitati { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Annullamento")]
        public string Annullamento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataANN")]
        public string DataANN { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraANN")]
        public string OraANN { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CartaAttivazioneANN")]
        public string CartaAttivazioneANN { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "ProgressivoFiscaleANN")]
        public string ProgressivoFiscaleANN { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "SigilloFiscaleANN")]
        public string SigilloFiscaleANN { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodSupporto")]
        public string CodSupporto { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "IdSupporto")]
        public string IdSupporto { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "IdSupAlt")]
        public string IdSupAlt { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Stato")]
        public string Stato { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataIngresso")]
        public string DataIngresso { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraIngresso")]
        public string OraIngresso { get; set; }

        public clsXml_LTA_Partecipante Partecipante { get; set; }
    }

    
    public class clsXml_LTA_Supporto
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "TipoSupportoId")]
        public string TipoSupportoId { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodSupportoId")]
        public string CodSupportoId { get; set; }

        public string GetChiaveSupporto()
        {
            return this.TipoSupportoId + this.CodSupportoId;
        }
    }

    public class clsXml_LTA_Evento :IDisposable
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "CFOrganizzatore")]
        public string CFOrganizzatore { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CodiceLocale")]
        public string CodiceLocale { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataEvento")]
        public string DataEvento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraEvento")]
        public string OraEvento { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "Titolo")]
        public string Titolo { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "TipoGenere")]
        public string TipoGenere { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataApertura")]
        public string DataApertura { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "OraApertura")]
        public string OraApertura { get; set; }

        [System.Xml.Serialization.XmlElement("Supporto")]
        public List<clsXml_LTA_Supporto> Supporto { get; set; }

        [System.Xml.Serialization.XmlElement("TitoloAccesso")]
        public List<clsXml_LTA_TitoloAccesso> TitoloAccesso { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public List<clsXml_LTA_TitoloAccesso> ListaTitoliHide { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, clsXml_LTA_Supporto> ListaSupportoHide { get; set; }

        public void Dispose()
        {
        }

        public string GetChiaveEvento()
        {
            return this.CFOrganizzatore + this.CodiceLocale + this.DataEvento + this.OraEvento + this.Titolo + this.TipoGenere + this.DataApertura + this.OraApertura;
        }
}


    [System.Xml.Serialization.XmlRoot(ElementName = "LTA_Giornaliera", Namespace = "")]
    public class clsXml_LTA_Giornaliera
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "SistemaCA")]
        public string SistemaCA { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "CFTitolareCA")]
        public string CFTitolareCA { get; set; }

        [System.Xml.Serialization.XmlAttribute(AttributeName = "DataLTA")]
        public string DataLTA { get; set; }

        [System.Xml.Serialization.XmlElement("LTA_Evento")]
        public List<clsXml_LTA_Evento> LTA_Evento { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, clsXml_LTA_Evento> ListaEventiHide { get; set; }
    }

    public static class clsXml_SerializeLTA
    {
        public static string SerialiazeLta(DataTable ltaTable, out Exception oError)
        {
            oError = null;
            string result = "";

            try
            {
                clsXml_LTA_Giornaliera oLtaGiornaliera = null;
                clsXml_LTA_Evento eventoCorrente = null;

                foreach (DataRow oRow in ltaTable.Rows)
                {
                    if (oLtaGiornaliera == null)
                    {
                        oLtaGiornaliera = new clsXml_LTA_Giornaliera();
                        oLtaGiornaliera.CFTitolareCA = (!string.IsNullOrEmpty(oRow["CF_TITOLARE_CA"].ToString()) ? oRow["CF_TITOLARE_CA"].ToString() : "");
                        oLtaGiornaliera.DataLTA = (!string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? oRow["DATA_EVENTO"].ToString() : "");
                        oLtaGiornaliera.SistemaCA = (!string.IsNullOrEmpty(oRow["CODICE_SISTEMA_CA"].ToString()) ? oRow["CODICE_SISTEMA_CA"].ToString() : "");
                        oLtaGiornaliera.ListaEventiHide = new Dictionary<string, clsXml_LTA_Evento>();
                    }

                    // verifica chiave evento
                    using (clsXml_LTA_Evento evento_riga = new clsXml_LTA_Evento())
                    {
                        evento_riga.CFOrganizzatore = (!string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString() : "");
                        evento_riga.CodiceLocale = (!string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? oRow["CODICE_LOCALE"].ToString() : "");
                        evento_riga.DataEvento = (!string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? oRow["DATA_EVENTO"].ToString() : "");
                        evento_riga.OraEvento = (!string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? oRow["ORA_EVENTO"].ToString() : "");
                        evento_riga.Titolo = (!string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? oRow["TITOLO_EVENTO"].ToString() : "");
                        evento_riga.TipoGenere = (!string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? oRow["TIPO_EVENTO"].ToString() : "");
                        evento_riga.DataApertura = (!string.IsNullOrEmpty(oRow["DATA_APERTURA_ACCESSI"].ToString()) ? oRow["DATA_APERTURA_ACCESSI"].ToString() : "");
                        evento_riga.OraApertura = (!string.IsNullOrEmpty(oRow["ORA_APERTURA_ACCESSI"].ToString()) ? oRow["ORA_APERTURA_ACCESSI"].ToString() : "");
                        evento_riga.ListaSupportoHide = new Dictionary<string, clsXml_LTA_Supporto>();
                        evento_riga.ListaTitoliHide = new List<clsXml_LTA_TitoloAccesso>();

                        if (eventoCorrente == null || (eventoCorrente.GetChiaveEvento() != evento_riga.GetChiaveEvento()))
                        {
                            if (oLtaGiornaliera.ListaEventiHide.ContainsKey(evento_riga.GetChiaveEvento()))
                            {
                                eventoCorrente = oLtaGiornaliera.ListaEventiHide[evento_riga.GetChiaveEvento()];
                            }
                            else
                            {
                                eventoCorrente = evento_riga;
                                oLtaGiornaliera.ListaEventiHide.Add(eventoCorrente.GetChiaveEvento(), eventoCorrente);
                            }
                        }
                    }

                    clsXml_LTA_Partecipante Partecipante_Riga = new clsXml_LTA_Partecipante();
                    Partecipante_Riga.Cognome = (!string.IsNullOrEmpty(oRow["COGNOME_PARTECIPANTE"].ToString()) ? oRow["COGNOME_PARTECIPANTE"].ToString() : "");
                    Partecipante_Riga.Nome = (!string.IsNullOrEmpty(oRow["NOME_PARTECIPANTE"].ToString()) ? oRow["NOME_PARTECIPANTE"].ToString() : "");
                    Partecipante_Riga.LuogoNascita = (!string.IsNullOrEmpty(oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString()) ? oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString() : "");
                    Partecipante_Riga.DataNascita = (!string.IsNullOrEmpty(oRow["DATA_NASCITA_PARTECIPANTE"].ToString()) ? oRow["DATA_NASCITA_PARTECIPANTE"].ToString() : "");

                    clsXml_LTA_Supporto Supporto_Riga = new clsXml_LTA_Supporto();
                    Supporto_Riga.CodSupportoId = (!string.IsNullOrEmpty(oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString() : "");
                    Supporto_Riga.TipoSupportoId = (!string.IsNullOrEmpty(oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString() : "");

                    if (!eventoCorrente.ListaSupportoHide.ContainsKey(Supporto_Riga.GetChiaveSupporto()))
                    {
                        eventoCorrente.ListaSupportoHide.Add(Supporto_Riga.GetChiaveSupporto(), Supporto_Riga);
                    }

                    clsXml_LTA_TitoloAccesso TitoloAccesso_Riga = new clsXml_LTA_TitoloAccesso();

                    TitoloAccesso_Riga.SistemaEmissione = (!string.IsNullOrEmpty(oRow["CODICE_SISTEMA_EMISSIONE"].ToString()) ? oRow["CODICE_SISTEMA_EMISSIONE"].ToString() : "");
                    TitoloAccesso_Riga.CartaAttivazione = (!string.IsNullOrEmpty(oRow["CODICE_CARTA_EMISSIONE"].ToString()) ? oRow["CODICE_CARTA_EMISSIONE"].ToString() : "");
                    TitoloAccesso_Riga.ProgressivoFiscale = (!string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO"].ToString()) ? oRow["PROGRESSIVO_TITOLO"].ToString() : "");
                    TitoloAccesso_Riga.SigilloFiscale = (!string.IsNullOrEmpty(oRow["SIGILLO"].ToString()) ? oRow["SIGILLO"].ToString() : "");
                    TitoloAccesso_Riga.DataEmissione = (!string.IsNullOrEmpty(oRow["DATA_EMISSIONE"].ToString()) ? oRow["DATA_EMISSIONE"].ToString() : "");
                    TitoloAccesso_Riga.OraEmissione = (!string.IsNullOrEmpty(oRow["ORA_EMISSIONE"].ToString()) ? oRow["ORA_EMISSIONE"].ToString() : "");
                    TitoloAccesso_Riga.DataLTA = (!string.IsNullOrEmpty(oRow["DATA_INSERIMENTO_LTA"].ToString()) ? oRow["DATA_INSERIMENTO_LTA"].ToString() : "");
                    TitoloAccesso_Riga.OraLTA = (!string.IsNullOrEmpty(oRow["ORA_INSERIMENTO_LTA"].ToString()) ? oRow["ORA_INSERIMENTO_LTA"].ToString() : "");
                    TitoloAccesso_Riga.TipoTitolo = (!string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? oRow["TIPO_TITOLO"].ToString() : "");
                    TitoloAccesso_Riga.CodiceOrdine = (!string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? oRow["ORDINE_DI_POSTO"].ToString() : "");
                    TitoloAccesso_Riga.CorrispettivoLordo = "0";
                    decimal nCorrispettivoLordo = 0;
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_LORDO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["CORRISPETTIVO_LORDO"].ToString()) && decimal.TryParse(oRow["CORRISPETTIVO_LORDO"].ToString(), out nCorrispettivoLordo))
                    {
                        TitoloAccesso_Riga.CorrispettivoLordo = GetCurrencyValueIntPer100(nCorrispettivoLordo);
                    }

                    TitoloAccesso_Riga.Posto = (!string.IsNullOrEmpty(oRow["POSTO"].ToString()) ? oRow["POSTO"].ToString() : "");

                    if (!string.IsNullOrEmpty(oRow["CODICE_ABBONAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["CODICE_ABBONAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["CF_ORGANIZZATORE_ABBONAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["CF_ORGANIZZATORE_ABBONAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["NUM_PROG_ABBONAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["NUM_PROG_ABBONAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString()) && !string.IsNullOrWhiteSpace(oRow["NUMERO_EVENTI_ABILITATI"].ToString()))
                    {
                        TitoloAccesso_Riga.Abbonamento = "S";
                        TitoloAccesso_Riga.CFAbbonamento = oRow["CF_ORGANIZZATORE_ABBONAMENTO"].ToString();
                        TitoloAccesso_Riga.CodiceAbbonamento = oRow["CODICE_ABBONAMENTO"].ToString();
                        TitoloAccesso_Riga.ProgressivoAbbonamento = oRow["NUM_PROG_ABBONAMENTO"].ToString();
                        TitoloAccesso_Riga.QEventiAbilitati = oRow["NUMERO_EVENTI_ABILITATI"].ToString();
                    }
                    else
                    {
                        // VERIFICARE SE NELL'XML DEVONO O MENO COMPARIRE SE NULLI A PARTE "N"
                        TitoloAccesso_Riga.Abbonamento = "N";
                        TitoloAccesso_Riga.CFAbbonamento = null;
                        TitoloAccesso_Riga.CodiceAbbonamento = null;
                        TitoloAccesso_Riga.ProgressivoAbbonamento = null;
                        TitoloAccesso_Riga.QEventiAbilitati = null;
                    }


                    if (!string.IsNullOrEmpty(oRow["DATA_ANNULLAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["DATA_ANNULLAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["ORA_ANNULLAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["ORA_ANNULLAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["CODICE_CARTA_ANNULLAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["CODICE_CARTA_ANNULLAMENTO"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO_ANN"].ToString()) && !string.IsNullOrWhiteSpace(oRow["PROGRESSIVO_TITOLO_ANN"].ToString()) &&
                        !string.IsNullOrEmpty(oRow["SIGILLO_ANNULLAMENTO"].ToString()) && !string.IsNullOrWhiteSpace(oRow["SIGILLO_ANNULLAMENTO"].ToString()))
                    {
                        TitoloAccesso_Riga.Annullamento = "S";
                        TitoloAccesso_Riga.DataANN = oRow["DATA_ANNULLAMENTO"].ToString();
                        TitoloAccesso_Riga.OraANN = oRow["ORA_ANNULLAMENTO"].ToString();
                        TitoloAccesso_Riga.CartaAttivazioneANN = oRow["CODICE_CARTA_ANNULLAMENTO"].ToString();
                        TitoloAccesso_Riga.ProgressivoFiscaleANN = oRow["PROGRESSIVO_TITOLO_ANN"].ToString();
                        TitoloAccesso_Riga.SigilloFiscaleANN = oRow["SIGILLO_ANNULLAMENTO"].ToString();
                    }
                    else
                    {
                        // VERIFICARE SE NELL'XML DEVONO O MENO COMPARIRE SE NULLI A PARTE "N"
                        TitoloAccesso_Riga.Annullamento = "N";
                        TitoloAccesso_Riga.DataANN = null;
                        TitoloAccesso_Riga.OraANN = null;
                        TitoloAccesso_Riga.CartaAttivazioneANN = null;
                        TitoloAccesso_Riga.ProgressivoFiscaleANN = null;
                        TitoloAccesso_Riga.SigilloFiscaleANN = null;
                    }

                    TitoloAccesso_Riga.CodSupporto = (!string.IsNullOrEmpty(oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString() : "");
                    TitoloAccesso_Riga.IdSupporto = (!string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP"].ToString()) ? oRow["IDENTIFICATIVO_SUPP"].ToString() : "");
                    TitoloAccesso_Riga.IdSupAlt = (!string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP_ALT"].ToString()) ? oRow["IDENTIFICATIVO_SUPP_ALT"].ToString() : "");
                    TitoloAccesso_Riga.Stato = (!string.IsNullOrEmpty(oRow["STATO_TITOLO"].ToString()) ? oRow["STATO_TITOLO"].ToString() : "");
                    TitoloAccesso_Riga.DataIngresso = (!string.IsNullOrEmpty(oRow["DATA_INGRESSO_CA"].ToString()) ? oRow["DATA_INGRESSO_CA"].ToString() : "");
                    TitoloAccesso_Riga.OraIngresso = (!string.IsNullOrEmpty(oRow["ORA_INGRESSO_CA"].ToString()) ? oRow["ORA_INGRESSO_CA"].ToString() : "");
                    eventoCorrente.ListaTitoliHide.Add(TitoloAccesso_Riga);
                }


                if (oLtaGiornaliera != null)
                {
                    if (oLtaGiornaliera.ListaEventiHide != null && oLtaGiornaliera.ListaEventiHide.Count > 0)
                    {
                        List<clsXml_LTA_Evento> eventiPerQuestaLta = new List<clsXml_LTA_Evento>();
                        foreach (KeyValuePair<string, clsXml_LTA_Evento> itemDictEvento in oLtaGiornaliera.ListaEventiHide)
                        {
                            clsXml_LTA_Evento itemEvento = itemDictEvento.Value;
                            itemEvento.TitoloAccesso = new List<clsXml_LTA_TitoloAccesso>(itemEvento.ListaTitoliHide.ToArray());
                            List<clsXml_LTA_Supporto> supportiPerQuestoEvento = new List<clsXml_LTA_Supporto>();
                            foreach(KeyValuePair<string, clsXml_LTA_Supporto> itemDictSupporto in itemEvento.ListaSupportoHide)
                            {
                                supportiPerQuestoEvento.Add(itemDictSupporto.Value);
                            }
                            itemEvento.Supporto = supportiPerQuestoEvento;
                            eventiPerQuestaLta.Add(itemEvento);
                        }
                        oLtaGiornaliera.LTA_Evento = new List<clsXml_LTA_Evento>(eventiPerQuestaLta.ToArray());
                    }
                    result = SerializeLtaGionaliera(oLtaGiornaliera, out oError);
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }

            return result;
        }

        private static string SerializeLtaGionaliera(clsXml_LTA_Giornaliera oLtaGiornaliera, out Exception oError)
        {
            oError = null;
            string value = "";
            try
            {
                using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
                {
                    System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
                    xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
                    xmlWriterSettings.OmitXmlDeclaration = false;
                    xmlWriterSettings.Indent = true;
                    xmlWriterSettings.CloseOutput = false;
                    
                    System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    xmlNameSpaces.Add("", "");

                    using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(clsXml_LTA_Giornaliera));
                        xmlWriter.WriteDocType("LTA_Giornaliera", null, "Lta_v0001_20081106.dtd", null);
                        xmlSerializer.Serialize(xmlWriter, oLtaGiornaliera, xmlNameSpaces);
                        xmlWriter.Dispose();
                    }
                    value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
                    xmlStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                oError = ex; // clsRiepiloghi.GetNewException("Serializzazione transazioni", "Errore durante la serialiazzione", ex);
                value = "";
            }
            return value;
        }

        private static string GetCurrencyValueIntPer100(decimal value)
        {
            decimal nIntValue = System.Math.Truncate(value * 100);
            long? result = long.Parse(nIntValue.ToString());
            return result.ToString().Trim();
        }


        

    }



    #endregion


    #region "Serializzazione RCA"



    public class clsXml_RCA_TotaleTipoTitolo : IDisposable
    {
        public string TipoTitolo { get; set; }
        public long TotaleTitoliLTA { get; set; }
        public long TotaleTitoliNoAccessoTradiz { get; set; }
        public long TotaleTitoliNoAccessoDigitali { get; set; }
        public long TotaleTitoliAutomatizzatiTradiz { get; set; }
        public long TotaleTitoliAutomatizzatiDigitali { get; set; }
        public long TotaleTitoliManualiTradiz { get; set; }
        public long TotaleTitoliManualiDigitali { get; set; }
        public long TotaleTitoliAnnullatiTradiz { get; set; }
        public long TotaleTitoliAnnullatiDigitali { get; set; }
        public long TotaleTitoliDaspatiTradiz { get; set; }
        public long TotaleTitoliDaspatiDigitali { get; set; }
        public long TotaleTitoliRubatiTradiz { get; set; }
        public long TotaleTitoliRubatiDigitali { get; set; }
        public long TotaleTitoliBLTradiz { get; set; }
        public long TotaleTitoliBLDigitali { get; set; }

        public void Dispose()
        {
        }
    }

    public class clsXml_RCA_TotaleTipoTitoloAbbonamento : IDisposable
    {
        public string TipoTitoloAbbonamento { get; set; }
        public long TotaleTitoliAbbLTA { get; set; }
        public long TotaleTitoliAbbNoAccessoTradiz { get; set; }
        public long TotaleTitoliAbbNoAccessoDigitali { get; set; }
        public long TotaleTitoliAbbAutomatizzatiTradiz { get; set; }
        public long TotaleTitoliAbbAutomatizzatiDigitali { get; set; }
        public long TotaleTitoliAbbManualiTradiz { get; set; }
        public long TotaleTitoliAbbManualiDigitali { get; set; }
        public long TotaleTitoliAbbAnnullatiTradiz { get; set; }
        public long TotaleTitoliAbbAnnullatiDigitali { get; set; }
        public long TotaleTitoliAbbDaspatiTradiz { get; set; }
        public long TotaleTitoliAbbDaspatiDigitali { get; set; }
        public long TotaleTitoliAbbRubatiTradiz { get; set; }
        public long TotaleTitoliAbbRubatiDigitali { get; set; }
        public long TotaleTitoliAbbBLTradiz { get; set; }
        public long TotaleTitoliAbbBLDigitali { get; set; }

        public void Dispose()
        {
        }
    }

    public class clsXml_RCA_Titoli :IDisposable
    {
        public string CodiceOrdinePosto { get; set; }
        public string Capienza { get; set; }

        [System.Xml.Serialization.XmlElement("TotaleTipoTitolo")]
        public List<clsXml_RCA_TotaleTipoTitolo> TotaleTipoTitolo { get; set; }

        public void Dispose()
        {
        }
    }
    public class clsXml_RCA_Abbonamenti : IDisposable
    {
        public string CodiceOrdinePosto { get; set; }
        public string Capienza { get; set; }

        [System.Xml.Serialization.XmlElement("TotaleTipoTitoloAbbonamento")]
        public List<clsXml_RCA_TotaleTipoTitoloAbbonamento> TotaleTipoTitoloAbbonamento { get; set; }

        public void Dispose()
        {
        }
    }

    public class clsXml_RCA_SistemaEmissione : IDisposable
    {
        public string CodiceSistemaEmissione { get; set; }

        [System.Xml.Serialization.XmlElement("Titoli")]
        public List<clsXml_RCA_Titoli> Titoli { get; set; }

        [System.Xml.Serialization.XmlElement("Abbonamenti")]
        public List<clsXml_RCA_Abbonamenti> Abbonamenti { get; set; }

        public void Dispose()
        {
        }
    }

    public class clsXml_RCA_Evento : IDisposable
    {
        public string CFOrganizzatore { get; set; }
        public string DenominazioneOrganizzatore { get; set; }
        public string TipologiaOrganizzatore { get; set; }
        public string SpettacoloIntrattenimento { get; set; }
        public string IncidenzaIntrattenimento { get; set; }
        public string DenominazioneLocale { get; set; }
        public string CodiceLocale { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string TipoGenere { get; set; }
        public string TitoloEvento { get; set; }
        public string Autore { get; set; }
        public string Esecutore { get; set; }
        public string NazionalitaFilm { get; set; }
        public string NumOpereRappresentate { get; set; }

        [System.Xml.Serialization.XmlElement("SistemaEmissione")]
        public List<clsXml_RCA_SistemaEmissione> SistemaEmissione { get; set; }

        public string GetChiaveEventoRCA()
        {
            return CFOrganizzatore + DenominazioneLocale + TipologiaOrganizzatore + SpettacoloIntrattenimento + IncidenzaIntrattenimento + DenominazioneLocale + CodiceLocale + DataEvento + OraEvento + TipoGenere + TitoloEvento;
        }

        public void Dispose()
        {
        }
    }

    public class clsXml_RCA_Titolare
    {
        public string DenominazioneTitolareCA { get; set; }
        public string CFTitolareCA { get; set; }
        public string CodiceSistemaCA { get; set; }
        public string DataRiepilogo { get; set; }
        public string DataGenerazioneRiepilogo { get; set; }
        public string OraGenerazioneRiepilogo { get; set; }
        public string ProgressivoRiepilogo { get; set; }
    }

    [System.Xml.Serialization.XmlRoot(ElementName = "RiepilogoControlloAccessi", Namespace = "")]
    public class clsXml_RCA_RiepilogoControlloAccessi
    {
        [System.Xml.Serialization.XmlAttribute(AttributeName = "Sostituzione")]
        public string Sostituzione { get; set; }

        public clsXml_RCA_Titolare Titolare { get; set; }

        [System.Xml.Serialization.XmlElement("Evento")]
        public List<clsXml_RCA_Evento> Evento { get; set; }
    }

    public static class clsXml_SerializeRCA
    {
        public static string SerialiazeRca(DataTable rcaTable, out Exception oError)
        {
            oError = null;
            string result = "";

            try
            {
                clsXml_RCA_RiepilogoControlloAccessi oRcaGiornaliera = null;
                clsXml_RCA_Evento eventoCorrente = null;
                //clsXml_RCA_SistemaEmissione sistemaEmissioneCorrente = null;

                Dictionary<string, clsXml_RCA_Evento> listaEventi = new Dictionary<string, clsXml_RCA_Evento>();

                foreach (DataRow oRow in rcaTable.Rows)
                {
                    if (oRcaGiornaliera == null)
                    {
                        oRcaGiornaliera = new clsXml_RCA_RiepilogoControlloAccessi();
                        
                        oRcaGiornaliera.Titolare = new clsXml_RCA_Titolare();
                        oRcaGiornaliera.Titolare.CFTitolareCA = (!string.IsNullOrEmpty(oRow["CF_TITOLARE_CA"].ToString()) ? oRow["CF_TITOLARE_CA"].ToString() : "");
                        oRcaGiornaliera.Titolare.DenominazioneTitolareCA = (!string.IsNullOrEmpty(oRow["TITOLARE"].ToString()) ? oRow["TITOLARE"].ToString() : "");
                        oRcaGiornaliera.Titolare.CodiceSistemaCA = (!string.IsNullOrEmpty(oRow["CODICE_SISTEMA_CA"].ToString()) ? oRow["CODICE_SISTEMA_CA"].ToString() : "");
                        oRcaGiornaliera.Titolare.DataRiepilogo = (!string.IsNullOrEmpty(oRow["DATA_RIEPILOGO"].ToString()) ? oRow["DATA_RIEPILOGO"].ToString() : "");
                        oRcaGiornaliera.Titolare.DataGenerazioneRiepilogo = (!string.IsNullOrEmpty(oRow["DATA_GENERAZIONE_RIEPILOGO"].ToString()) ? oRow["DATA_GENERAZIONE_RIEPILOGO"].ToString() : "");
                        oRcaGiornaliera.Titolare.OraGenerazioneRiepilogo = (!string.IsNullOrEmpty(oRow["ORA_GENERAZIONE_RIEPILOGO"].ToString()) ? oRow["ORA_GENERAZIONE_RIEPILOGO"].ToString() : "");
                        oRcaGiornaliera.Evento = new List<clsXml_RCA_Evento>();
                        long nProgressivo = 0;

                        if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_RIEPILOGO"].ToString()) && long.TryParse(oRow["PROGRESSIVO_RIEPILOGO"].ToString(), out nProgressivo))
                        {
                            oRcaGiornaliera.Titolare.ProgressivoRiepilogo = nProgressivo.ToString();
                            oRcaGiornaliera.Sostituzione = (nProgressivo > 1 ? "S" : "N");
                        }
                        else
                        {
                            oRcaGiornaliera.Titolare.ProgressivoRiepilogo = "1";
                            oRcaGiornaliera.Sostituzione = "N";
                        }
                    }

                    using (clsXml_RCA_Evento evento_riga = new clsXml_RCA_Evento())
                    {
                        evento_riga.CFOrganizzatore = (!string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString() : "");
                        evento_riga.DenominazioneOrganizzatore = (!string.IsNullOrEmpty(oRow["DENOMINAZIONE_ORGANIZZATORE"].ToString()) ? oRow["DENOMINAZIONE_ORGANIZZATORE"].ToString() : "");
                        evento_riga.TipologiaOrganizzatore = (!string.IsNullOrEmpty(oRow["TIPOLOGIA_ORGANIZZATORE"].ToString()) ? oRow["TIPOLOGIA_ORGANIZZATORE"].ToString() : "");

                        evento_riga.SpettacoloIntrattenimento = (!string.IsNullOrEmpty(oRow["SPETTACOLO_INTRATTENIMENTO"].ToString()) ? oRow["SPETTACOLO_INTRATTENIMENTO"].ToString() : "");
                        evento_riga.IncidenzaIntrattenimento = (!string.IsNullOrEmpty(oRow["INCIDENZA_INTRATTENIMENTO"].ToString()) ? oRow["INCIDENZA_INTRATTENIMENTO"].ToString() : "");
                        evento_riga.DenominazioneLocale = (!string.IsNullOrEmpty(oRow["DENOMINAZIONE_LOCALE"].ToString()) ? oRow["DENOMINAZIONE_LOCALE"].ToString() : "");
                        evento_riga.CodiceLocale = (!string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? oRow["CODICE_LOCALE"].ToString() : "");
                        evento_riga.DataEvento = (!string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? oRow["DATA_EVENTO"].ToString() : "");
                        evento_riga.OraEvento = (!string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? oRow["ORA_EVENTO"].ToString() : "");
                        evento_riga.TipoGenere = (!string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? oRow["TIPO_EVENTO"].ToString() : "");
                        evento_riga.TitoloEvento = (!string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? oRow["TITOLO_EVENTO"].ToString() : "");
                        evento_riga.Autore = (!string.IsNullOrEmpty(oRow["AUTORE"].ToString()) ? oRow["AUTORE"].ToString() : "");
                        evento_riga.Esecutore = (!string.IsNullOrEmpty(oRow["ESECUTORE"].ToString()) ? oRow["ESECUTORE"].ToString() : "");
                        evento_riga.NazionalitaFilm = (!string.IsNullOrEmpty(oRow["NAZIONALITA"].ToString()) ? oRow["NAZIONALITA"].ToString() : "");
                        evento_riga.NumOpereRappresentate = (!string.IsNullOrEmpty(oRow["NUMERO_OPERE_RAPPRESENTATE"].ToString()) ? oRow["NUMERO_OPERE_RAPPRESENTATE"].ToString() : "");
                        evento_riga.SistemaEmissione = new List<clsXml_RCA_SistemaEmissione>();

                        if (eventoCorrente == null || eventoCorrente.GetChiaveEventoRCA() != evento_riga.GetChiaveEventoRCA())
                        {
                            if (listaEventi.ContainsKey(evento_riga.GetChiaveEventoRCA()))
                            {
                                eventoCorrente = listaEventi[evento_riga.GetChiaveEventoRCA()];
                            }
                            else
                            {
                                eventoCorrente = evento_riga;
                                oRcaGiornaliera.Evento.Add(eventoCorrente);
                                listaEventi.Add(eventoCorrente.GetChiaveEventoRCA(), eventoCorrente);
                            }

                        }

                        using (clsXml_RCA_SistemaEmissione sistema_emissione_riga = new clsXml_RCA_SistemaEmissione())
                        {
                            clsXml_RCA_SistemaEmissione sistemaCorrente = null;

                            sistema_emissione_riga.CodiceSistemaEmissione = (!string.IsNullOrEmpty(oRow["CODICE_SISTEMA_EMISSIONE"].ToString()) ? oRow["CODICE_SISTEMA_EMISSIONE"].ToString() : "");

                            long nFlagAbbonamenti = 0;
                            bool lAbbonamenti = (!string.IsNullOrEmpty(oRow["TF"].ToString()) && long.TryParse(oRow["TF"].ToString(), out nFlagAbbonamenti) && nFlagAbbonamenti > 0);
                            bool lBiglietti = (!lAbbonamenti);

                            if (lBiglietti) sistema_emissione_riga.Titoli = new List<clsXml_RCA_Titoli>();
                            if (lAbbonamenti) sistema_emissione_riga.Abbonamenti = new List<clsXml_RCA_Abbonamenti>();

                            foreach (clsXml_RCA_SistemaEmissione itemSistema in eventoCorrente.SistemaEmissione)
                            {
                                if (itemSistema.CodiceSistemaEmissione == sistema_emissione_riga.CodiceSistemaEmissione)
                                {
                                    sistemaCorrente = itemSistema;
                                    break;
                                }
                            }

                            if (sistemaCorrente == null)
                            {
                                sistemaCorrente = sistema_emissione_riga;
                                eventoCorrente.SistemaEmissione.Add(sistemaCorrente);
                            }

                            if (lBiglietti)
                            {
                                if (sistemaCorrente.Titoli == null) sistemaCorrente.Titoli = new List<clsXml_RCA_Titoli>();
                                using (clsXml_RCA_Titoli titoli_riga = new clsXml_RCA_Titoli())
                                {
                                    clsXml_RCA_Titoli titoliCorrente = null;

                                    titoli_riga.CodiceOrdinePosto = (!string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? oRow["ORDINE_DI_POSTO"].ToString() : "");
                                    titoli_riga.Capienza = (!string.IsNullOrEmpty(oRow["CAPIENZA_SETTORE"].ToString()) ? oRow["CAPIENZA_SETTORE"].ToString() : "");
                                    titoli_riga.TotaleTipoTitolo = new List<clsXml_RCA_TotaleTipoTitolo>();

                                    foreach (clsXml_RCA_Titoli itemTitolo in sistemaCorrente.Titoli)
                                    {
                                        if (itemTitolo.CodiceOrdinePosto == titoli_riga.CodiceOrdinePosto)
                                        {
                                            titoliCorrente = itemTitolo;
                                            break;
                                        }
                                    }

                                    if (titoliCorrente == null)
                                    {
                                        titoliCorrente = titoli_riga;
                                        sistemaCorrente.Titoli.Add(titoliCorrente);
                                    }

                                    using (clsXml_RCA_TotaleTipoTitolo totale_titolo_riga = new clsXml_RCA_TotaleTipoTitolo())
                                    {

                                        clsXml_RCA_TotaleTipoTitolo totaleCorrente = null;

                                        totale_titolo_riga.TipoTitolo = (!string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? oRow["TIPO_TITOLO"].ToString() : "");
                                        totale_titolo_riga.TotaleTitoliLTA = GetValueQta(oRow, "TOT");
                                        totale_titolo_riga.TotaleTitoliNoAccessoTradiz = GetValueQta(oRow, "TOT_VT");
                                        totale_titolo_riga.TotaleTitoliNoAccessoDigitali = GetValueQta(oRow, "TOT_VD");
                                        totale_titolo_riga.TotaleTitoliAutomatizzatiTradiz = GetValueQta(oRow, "TOT_ZT");
                                        totale_titolo_riga.TotaleTitoliAutomatizzatiDigitali = GetValueQta(oRow, "TOT_ZD");
                                        totale_titolo_riga.TotaleTitoliManualiTradiz = GetValueQta(oRow, "TOT_MT");
                                        totale_titolo_riga.TotaleTitoliManualiDigitali = GetValueQta(oRow, "TOT_MD");
                                        totale_titolo_riga.TotaleTitoliRubatiTradiz = GetValueQta(oRow, "TOT_FT");
                                        totale_titolo_riga.TotaleTitoliRubatiDigitali = GetValueQta(oRow, "TOT_FD");
                                        totale_titolo_riga.TotaleTitoliDaspatiTradiz = GetValueQta(oRow, "TOT_DT");
                                        totale_titolo_riga.TotaleTitoliDaspatiDigitali = GetValueQta(oRow, "TOT_DD");
                                        totale_titolo_riga.TotaleTitoliAnnullatiTradiz = GetValueQta(oRow, "TOT_AT");
                                        totale_titolo_riga.TotaleTitoliAnnullatiDigitali = GetValueQta(oRow, "TOT_AD");
                                        totale_titolo_riga.TotaleTitoliBLTradiz = GetValueQta(oRow, "TOT_BT");
                                        totale_titolo_riga.TotaleTitoliBLDigitali = GetValueQta(oRow, "TOT_BD");

                                        foreach (clsXml_RCA_TotaleTipoTitolo itemTotale in titoliCorrente.TotaleTipoTitolo)
                                        {
                                            if (itemTotale.TipoTitolo == totale_titolo_riga.TipoTitolo)
                                            {
                                                totaleCorrente = itemTotale;
                                                break;
                                            }
                                        }

                                        if (totaleCorrente == null)
                                        {
                                            totaleCorrente = totale_titolo_riga;
                                            titoliCorrente.TotaleTipoTitolo.Add(totaleCorrente);
                                        }
                                        else
                                        {
                                            totaleCorrente.TotaleTitoliLTA += totale_titolo_riga.TotaleTitoliLTA;
                                            totaleCorrente.TotaleTitoliNoAccessoTradiz += totale_titolo_riga.TotaleTitoliNoAccessoTradiz;
                                            totaleCorrente.TotaleTitoliNoAccessoDigitali += totale_titolo_riga.TotaleTitoliNoAccessoDigitali;
                                            totaleCorrente.TotaleTitoliAutomatizzatiTradiz += totale_titolo_riga.TotaleTitoliAutomatizzatiTradiz;
                                            totaleCorrente.TotaleTitoliAutomatizzatiDigitali += totale_titolo_riga.TotaleTitoliAutomatizzatiDigitali;
                                            totaleCorrente.TotaleTitoliManualiTradiz += totale_titolo_riga.TotaleTitoliManualiTradiz;
                                            totaleCorrente.TotaleTitoliManualiDigitali += totale_titolo_riga.TotaleTitoliManualiDigitali;
                                            totaleCorrente.TotaleTitoliAnnullatiTradiz += totale_titolo_riga.TotaleTitoliAnnullatiTradiz;
                                            totaleCorrente.TotaleTitoliAnnullatiDigitali += totale_titolo_riga.TotaleTitoliAnnullatiDigitali;
                                            totaleCorrente.TotaleTitoliDaspatiTradiz += totale_titolo_riga.TotaleTitoliDaspatiTradiz;
                                            totaleCorrente.TotaleTitoliDaspatiDigitali += totale_titolo_riga.TotaleTitoliDaspatiDigitali;
                                            totaleCorrente.TotaleTitoliRubatiTradiz += totale_titolo_riga.TotaleTitoliRubatiTradiz;
                                            totaleCorrente.TotaleTitoliRubatiDigitali += totale_titolo_riga.TotaleTitoliRubatiDigitali;
                                            totaleCorrente.TotaleTitoliBLTradiz += totale_titolo_riga.TotaleTitoliBLTradiz;
                                            totaleCorrente.TotaleTitoliBLDigitali += totale_titolo_riga.TotaleTitoliBLDigitali;
                                        }
                                    }


                                }
                                //clsXml_RCA_Titoli
                                //clsXml_RCA_Titoli
                                //sistemaCorrente.Titoli.
                            }

                            if (lAbbonamenti)
                            {
                                if (sistemaCorrente.Abbonamenti == null) sistemaCorrente.Abbonamenti = new List<clsXml_RCA_Abbonamenti>();
                                using (clsXml_RCA_Abbonamenti abbonamenti_riga = new clsXml_RCA_Abbonamenti())
                                {
                                    clsXml_RCA_Abbonamenti abbonamentoCorrente = null;

                                    abbonamenti_riga.CodiceOrdinePosto = (!string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? oRow["ORDINE_DI_POSTO"].ToString() : "");
                                    abbonamenti_riga.Capienza = (!string.IsNullOrEmpty(oRow["CAPIENZA_SETTORE"].ToString()) ? oRow["CAPIENZA_SETTORE"].ToString() : "");
                                    abbonamenti_riga.TotaleTipoTitoloAbbonamento = new List<clsXml_RCA_TotaleTipoTitoloAbbonamento>();

                                    foreach (clsXml_RCA_Abbonamenti itemAbbonamento in sistemaCorrente.Abbonamenti)
                                    {
                                        if (itemAbbonamento.CodiceOrdinePosto == abbonamenti_riga.CodiceOrdinePosto)
                                        {
                                            abbonamentoCorrente = itemAbbonamento;
                                            break;
                                        }
                                    }

                                    if (abbonamentoCorrente == null)
                                    {
                                        abbonamentoCorrente = abbonamenti_riga;
                                        sistemaCorrente.Abbonamenti.Add(abbonamentoCorrente);
                                    }

                                    using (clsXml_RCA_TotaleTipoTitoloAbbonamento totale_titolo_abbonamento_riga = new clsXml_RCA_TotaleTipoTitoloAbbonamento())
                                    {

                                        clsXml_RCA_TotaleTipoTitoloAbbonamento totaleAbbonamentoCorrente = null;

                                        totale_titolo_abbonamento_riga.TipoTitoloAbbonamento = (!string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? oRow["TIPO_TITOLO"].ToString() : "");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbLTA = GetValueQta(oRow, "TOT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbNoAccessoTradiz = GetValueQta(oRow, "TOT_VT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbNoAccessoDigitali = GetValueQta(oRow, "TOT_VD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbAutomatizzatiTradiz = GetValueQta(oRow, "TOT_ZT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbAutomatizzatiDigitali = GetValueQta(oRow, "TOT_ZD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbManualiTradiz = GetValueQta(oRow, "TOT_MT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbManualiDigitali = GetValueQta(oRow, "TOT_MD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbAnnullatiTradiz = GetValueQta(oRow, "TOT_AT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbAnnullatiDigitali = GetValueQta(oRow, "TOT_AD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbDaspatiTradiz = GetValueQta(oRow, "TOT_DT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbDaspatiDigitali = GetValueQta(oRow, "TOT_DD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbRubatiTradiz = GetValueQta(oRow, "TOT_FT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbRubatiDigitali = GetValueQta(oRow, "TOT_FD");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbBLTradiz = GetValueQta(oRow, "TOT_BT");
                                        totale_titolo_abbonamento_riga.TotaleTitoliAbbBLDigitali = GetValueQta(oRow, "TOT_BD");

                                        foreach (clsXml_RCA_TotaleTipoTitoloAbbonamento itemTotale in abbonamentoCorrente.TotaleTipoTitoloAbbonamento)
                                        {
                                            if (itemTotale.TipoTitoloAbbonamento == totale_titolo_abbonamento_riga.TipoTitoloAbbonamento)
                                            {
                                                totaleAbbonamentoCorrente = itemTotale;
                                                break;
                                            }
                                        }

                                        if (totaleAbbonamentoCorrente == null)
                                        {
                                            totaleAbbonamentoCorrente = totale_titolo_abbonamento_riga;
                                            abbonamentoCorrente.TotaleTipoTitoloAbbonamento.Add(totaleAbbonamentoCorrente);
                                        }
                                        else
                                        {

                                            totaleAbbonamentoCorrente.TotaleTitoliAbbLTA += totale_titolo_abbonamento_riga.TotaleTitoliAbbLTA;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbNoAccessoTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbNoAccessoTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbNoAccessoDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbNoAccessoDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbAutomatizzatiTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbAutomatizzatiTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbAutomatizzatiDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbAutomatizzatiDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbManualiTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbManualiTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbManualiDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbManualiDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbAnnullatiTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbAnnullatiTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbAnnullatiDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbAnnullatiDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbDaspatiTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbDaspatiTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbDaspatiDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbDaspatiDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbRubatiTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbRubatiTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbRubatiDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbRubatiDigitali;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbBLTradiz += totale_titolo_abbonamento_riga.TotaleTitoliAbbBLTradiz;
                                            totaleAbbonamentoCorrente.TotaleTitoliAbbBLDigitali += totale_titolo_abbonamento_riga.TotaleTitoliAbbBLDigitali;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (oRcaGiornaliera != null)
                {
                    result = SerializeRcaGionaliera(oRcaGiornaliera, out oError);
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            return result;
        }

        private static string SerializeRcaGionaliera(clsXml_RCA_RiepilogoControlloAccessi oRcaGiornaliera, out Exception oError)
        {
            oError = null;
            string value = "";
            try
            {
                using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
                {
                    System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
                    xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
                    xmlWriterSettings.OmitXmlDeclaration = false;
                    xmlWriterSettings.Indent = true;
                    xmlWriterSettings.CloseOutput = false;

                    System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    xmlNameSpaces.Add("", "");

                    using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(clsXml_RCA_RiepilogoControlloAccessi));
                        //if (writeDebugDocType)
                        //    xmlWriter.WriteDocType("RiepilogoControlloAccessi", null, "ControlloAccessi_v0001_20080626.dtd", null);
                        xmlSerializer.Serialize(xmlWriter, oRcaGiornaliera, xmlNameSpaces);
                        xmlWriter.Dispose();
                    }
                    value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
                    xmlStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                oError = ex; // clsRiepiloghi.GetNewException("Serializzazione transazioni", "Errore durante la serialiazzione", ex);
                value = "";
            }
            return value;
        }

        private static long GetValueQta(DataRow oRow, string fieldName)
        {
            long result = 0;
            long value = 0;

            try
            {
                if (!string.IsNullOrEmpty(oRow[fieldName].ToString()) &&
                    !string.IsNullOrWhiteSpace(oRow[fieldName].ToString()) &&
                    long.TryParse(oRow[fieldName].ToString(), out value))
                {
                    result = value;
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

    }


    #endregion

    #region "SCS"

    public class clsP_SCS_SALE_NEW : DbMapTable
    {
        public DbMapFieldInt32 nIDSCSO;
        public DbMapFieldString cACCOUNTID;
        public DbMapFieldInt32 nIDOPERATORE;
        public DbMapFieldDateTime dPERIODO_INIZIO;
        public DbMapFieldDateTime dPERIODO_FINE;
        public DbMapFieldInt32 nIDSALA1;
        public DbMapFieldInt32 nIDSALA2;
        public DbMapFieldString cMETODO;


        public clsP_SCS_SALE_NEW(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "CINEMA.P_SCS_SALE_NEW";
            Command.CommandType = CommandType.StoredProcedure;

            nIDSCSO = new DbMapFieldInt32(this, "nIDSCSO");
            nIDSCSO.Parameter.Direction = ParameterDirection.InputOutput;

            cACCOUNTID = new DbMapFieldString(this, "cACCOUNTID");
            cACCOUNTID.Parameter.Direction = ParameterDirection.Input;

            nIDOPERATORE = new DbMapFieldInt32(this, "nIDOPERATORE");
            nIDOPERATORE.Parameter.Direction = ParameterDirection.Input;

            dPERIODO_INIZIO = new DbMapFieldDateTime(this, "dInizio");
            dPERIODO_INIZIO.Parameter.Direction = ParameterDirection.Input;

            dPERIODO_FINE = new DbMapFieldDateTime(this, "dFine");
            dPERIODO_FINE.Parameter.Direction = ParameterDirection.Input;

            nIDSALA1 = new DbMapFieldInt32(this, "idsala1");
            nIDSALA1.Parameter.Direction = ParameterDirection.Input;

            nIDSALA2 = new DbMapFieldInt32(this, "idsala2");
            nIDSALA2.Parameter.Direction = ParameterDirection.Input;

            cMETODO = new DbMapFieldString(this, "metodo");
            cMETODO.Parameter.Direction = ParameterDirection.Input;
        }


    }

    public class clsP_SCS_SALE_CNF : DbMapTable
    {
        public DbMapFieldInt32 nIDSCSO;
        public DbMapFieldInt32 nSTATO;
        public DbMapFieldString cACCOUNTID;
        public DbMapFieldInt32 nIDOPERATORE;

        public clsP_SCS_SALE_CNF(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "CINEMA.P_SCS_SALE_CNF";
            Command.CommandType = CommandType.StoredProcedure;

            nIDSCSO = new DbMapFieldInt32(this, "nIDSCSO");
            nIDSCSO.Parameter.Direction = ParameterDirection.Input;

            nSTATO = new DbMapFieldInt32(this, "stato");
            nSTATO.Parameter.Direction = ParameterDirection.Input;

            cACCOUNTID = new DbMapFieldString(this, "cACCOUNTID");
            cACCOUNTID.Parameter.Direction = ParameterDirection.Input;

            nIDOPERATORE = new DbMapFieldInt32(this, "nIDOPERATORE");
            nIDOPERATORE.Parameter.Direction = ParameterDirection.Input;


        }


    }

    public class clsP_SCS_SALE_ANN : DbMapTable
    {
        public DbMapFieldInt32 nIDSCSO;
        public DbMapFieldString cACCOUNTID;
        public DbMapFieldInt32 nIDOPERATORE;

        public clsP_SCS_SALE_ANN(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "CINEMA.P_SCS_SALE_ANN";
            Command.CommandType = CommandType.StoredProcedure;

            nIDSCSO = new DbMapFieldInt32(this, "nIDSCSO");
            nIDSCSO.Parameter.Direction = ParameterDirection.InputOutput;

            cACCOUNTID = new DbMapFieldString(this, "cACCOUNTID");
            cACCOUNTID.Parameter.Direction = ParameterDirection.Input;

            nIDOPERATORE = new DbMapFieldInt32(this, "nIDOPERATORE");
            nIDOPERATORE.Parameter.Direction = ParameterDirection.Input;
        }
    }

    public class clsP_SCS_SALE_ARK : DbMapTable
    {
        public DbMapFieldInt32 nIDSCSO;

        public clsP_SCS_SALE_ARK(DatabaseManager parDatabaseManager) : base(parDatabaseManager)
        {
            PrefixParameter = String.Empty;
            Command.CommandTimeout = 30;
            Command.CommandText = "CINEMA.P_SCS_SALE_ARK";
            Command.CommandType = CommandType.StoredProcedure;

            nIDSCSO = new DbMapFieldInt32(this, "nIDSCSO");
            nIDSCSO.Parameter.Direction = ParameterDirection.InputOutput;
        }


    }

    #endregion
}
