﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections;
using Fleck;

namespace Riepiloghi
{

    public class clsWinServiceModel_request_Riepilogo
    {
        public string GiornalieroMensile { get; set; }
        public DateTime Giorno { get; set; }

        public long Progressivo { get; set; }

    }

    public class clsWinServiceModel_Token
    {
        public long IdOperazione { get; set; }
        public string CodiceSistema { get; set; }
        public string AccountId { get; set; }
        public string OperationCode { get; set; }
        public DateTime DataIns { get; set; }
        public DateTime DataUse { get; set; }
    }

    public class clsWinServiceModel_request_Operation
    {
        public string Token { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public clsWinServiceModel_request_Riepilogo RequestRiepilogo { get; set; }

        public static string CODE_OP_REQUEST_RIEPILOGO = "REQUEST_RIEPILOGO";
        public static string CODE_OP_REQUEST_SEND_EMAIL = "SEND_EMAIL";
        public static string CODE_OP_REQUEST_RECEIVE_EMAIL = "RECEIVE_EMAIL";


        public static string CODE_OP_REQUEST_CHECKUPDATES = "CHECK_UPDATES";

        public static string CODE_OP_REQUEST_MASTERIZZA = "CHECK_UPDATES";

        public static string CODE_OP_REQUEST_ENABLED_LOGS = "ENABLE_LOGS";
        public static string CODE_OP_REQUEST_DISABLED_LOGS = "DISABLE_LOGS";

        public static string CODE_OP_RESPONSE_OK = "OK";
        public static string CODE_OP_RESPONSE_START_SERVER = "START_SERVER";
        public static string CODE_OP_RESPONSE_STOP_SERVER = "STOP_SERVER";
        public static string CODE_OP_RESPONSE_ERR_GENERIC = "GENERIC_ERROR";
        public static string CODE_OP_RESPONSE_ERR_INVALID_GIORNALIERO_MENSILE = "INVALID_GIORNALIERO_MENSILE";
        public static string CODE_OP_RESPONSE_ERR_INVALID_GIORNO = "INVALID_GIORNO";
        public static string CODE_OP_RESPONSE_ERR_LOCK_GIORNALIERO_MENSILE = "GENERAZIONE_IN_CORSO";


    }

    public class clsWinServiceModel_response_Operation
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
    }


    public static class clsSecuritySocket
    {
        public static string key = "F5AE450FE3118D0B7EDF48AF";
        public static string Encrypt(string toEncrypt)
        {
            string parCryptKey = clsSecuritySocket.key;
            return Encrypt(toEncrypt, parCryptKey, false);
        }

        private static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider hashmd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            System.Security.Cryptography.TripleDESCryptoServiceProvider tdes = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = System.Security.Cryptography.CipherMode.ECB;
            tdes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            System.Security.Cryptography.ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt)
        {
            string parCryptKey = clsSecuritySocket.key;
            return Decrypt(toDecrypt, parCryptKey, false);
        }

        private static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                System.Security.Cryptography.MD5CryptoServiceProvider hashmd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            System.Security.Cryptography.TripleDESCryptoServiceProvider tdes = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = System.Security.Cryptography.CipherMode.ECB;
            tdes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

            System.Security.Cryptography.ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        internal static clsWinServiceModel_Token CheckSocketTokenOperazione(Wintic.Data.IConnection Connection, string request, out Exception Error)
        {
            bool result = false;
            Error = null;

            clsWinServiceModel_Token oTokenRequest = null;
            try
            {
                oTokenRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<clsWinServiceModel_Token>(Decrypt(request));
            }
            catch (Exception ex)
            {
                Error = new Exception("Richiesta non valida");
                oTokenRequest = null;
            }

            if (oTokenRequest != null)
            {
                try
                {
                    StringBuilder oSB = new StringBuilder();
                    Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();
                    Wintic.Data.IRecordSet oRS;

                    oSB.Append("SELECT * FROM RIEPILOGHI_OPERAZIONI_REQ WHERE IDOPERAZIONE = :pIDOPERAZIONE");
                    oPars.Add(":pIDOPERAZIONE", oTokenRequest.IdOperazione);
                    oRS = (Wintic.Data.IRecordSet)Connection.ExecuteQuery(oSB.ToString(), oPars);
                    if (!oRS.EOF)
                    {
                        if (!oRS.Fields("IDOPERAZIONE").IsNull && 
                            !oRS.Fields("ACCOUNTID").IsNull && 
                            !oRS.Fields("CODICE_SISTEMA").IsNull && 
                            !oRS.Fields("OPERATION_CODE").IsNull && 
                            !oRS.Fields("SECURITY_TOKEN").IsNull &&
                            !oRS.Fields("STATO_OPERAZIONE").IsNull &&
                            oTokenRequest.IdOperazione.ToString().Trim() == oRS.Fields("IDOPERAZIONE").Value.ToString().Trim() &&
                            oTokenRequest.AccountId == oRS.Fields("ACCOUNTID").Value.ToString() &&
                            oTokenRequest.CodiceSistema == oRS.Fields("CODICE_SISTEMA").Value.ToString() &&
                            oTokenRequest.OperationCode == oRS.Fields("OPERATION_CODE").Value.ToString())
                        {
                            string SecurityToken = oRS.Fields("SECURITY_TOKEN").Value.ToString();

                            if (SecurityToken == request)
                            {
                                if (long.Parse(oRS.Fields("STATO_OPERAZIONE").Value.ToString()) == 0)
                                {
                                    result = true;
                                    Connection.ExecuteNonQuery("UPDATE RIEPILOGHI_OPERAZIONI_REQ SET STATO_OPERAZIONE = 1 WHERE IDOPERAZIONE = :pIDOPERAZIONE", new Wintic.Data.clsParameters(":pIDOPERAZIONE", oTokenRequest.IdOperazione), true);
                                }
                                else
                                {
                                    if (long.Parse(oRS.Fields("STATO_OPERAZIONE").Value.ToString()) == 1)
                                        Error = new Exception("Token già utilizzato, operazione in corso");
                                    else if (long.Parse(oRS.Fields("STATO_OPERAZIONE").Value.ToString()) == 2)
                                        Error = new Exception("Token già utilizzato operazione terminata");
                                    else
                                        Error = new Exception("Token già utilizzato");
                                }
                            }
                            else
                            {
                                Error = new Exception("Token non ");
                            }
                        }
                        else
                        {
                            Error = new Exception("Token non valido");
                        }
                    }
                    else
                    {
                        Error = new Exception("Token non valido");
                    }
                    oRS.Close();

                }
                catch (Exception ex)
                {
                    Error = new Exception("Richiesta Utilizzo Socket Token, Errore generico " + ex.Message);
                    result = false;
                }
            }
            else
                Error = new Exception("Token non valido");

            if (Error != null)
                oTokenRequest = null;

            return oTokenRequest;
        }

        internal static bool CloseSocketTokenOperazione(Wintic.Data.IConnection Connection, clsWinServiceModel_Token oTokenRequest, Exception Error)
        {
            bool result = false;

            try
            {
                StringBuilder oSB = new StringBuilder();
                Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();

                oPars.Add(":pIDOPERAZIONE", oTokenRequest.IdOperazione);
                oPars.Add(":pSTATO_OPERAZIONE", 2);

                oSB.Append("UPDATE RIEPILOGHI_OPERAZIONI_REQ SET ");
                oSB.Append(" STATO_OPERAZIONE = :pSTATO_OPERAZIONE ");

                if (Error != null)
                {
                    oSB.Append(" , ERRORE = :pERRORE");
                    oPars.Add(":pERRORE", (Error.Message.Length > 4000 ? Error.Message.Substring(0, 3999) : Error.Message));
                }
                oSB.Append(" WHERE IDOPERAZIONE = :pIDOPERAZIONE");
                Connection.ExecuteNonQuery(oSB.ToString(), oPars, true);
            }
            catch (Exception ex)
            {
            }
            return result;
        }

    }

    public class SocketServerRiepiloghi
    {
        private ArrayList oClients = new ArrayList();
        public int nPortListen = 0;
        private IPAddress IndirizzoIP = null;
        public Exception ExceptionServerSocket = null;
        public string IndirizzoIPString = "";
        public String ServiceName = "SERVERX";
        public string ApplicationType = "";

        private Fleck.WebSocketServer oListenerFleck;
        private List<Fleck.IWebSocketConnection> allSockets = new List<Fleck.IWebSocketConnection>();

        public delegate void delegateEventServerSocketRiepiloghi_In(clsWinServiceModel_request_Operation request);
        public delegate void delegateEventServerSocketRiepiloghi_Out(clsWinServiceModel_response_Operation response);
        public event delegateEventServerSocketRiepiloghi_In EventServerSocketRiepiloghi_In;
        public event delegateEventServerSocketRiepiloghi_Out EventServerSocketRiepiloghi_Out;

        public delegate void delegateEventServerSocketRiepiloghi_Execute(clsWinServiceModel_request_Operation request, out Exception error);
        public event delegateEventServerSocketRiepiloghi_Execute EventServerSocketRiepiloghi_Execute;

        public delegate void delegateOnClientConnected(IWebSocketConnection oClient);
        public event delegateOnClientConnected OnClientConnected;
        public delegate void delegateOnClientClosed(IWebSocketConnection oClient);
        public event delegateOnClientConnected OnClientClosed;
        public delegate void delegateOnClientRefused(string message, IWebSocketConnection oClient);
        public event delegateOnClientRefused OnClientRefused;

        public bool Listening = false;

        public SocketServerRiepiloghi(string cIP, int port, string serviceName, string applicationType)
        {
            this.IndirizzoIP = null;
            this.nPortListen = port;
            this.IndirizzoIPString = "";
            this.ServiceName = serviceName;
            this.ApplicationType = applicationType;
            try
            {
                this.IndirizzoIPString = cIP;
                if (cIP == "127.0.0.1")
                {
                    this.IndirizzoIP = IPAddress.Parse(cIP);
                }
                else
                {
                    foreach (IPAddress oIP in Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork))
                    {
                        if (oIP.ToString() == cIP)
                        {
                            this.IndirizzoIP = oIP;
                            break;
                        }
                    }
                }

                if (this.IndirizzoIP == null)
                {
                    this.ExceptionServerSocket = new Exception("IP non valido");
                }
            }
            catch (Exception ex)
            {
                this.ExceptionServerSocket = ex;
            }
        }


        public bool StartListen()
        {
            bool lRet = false;
            clsWinServiceModel_response_Operation response = new clsWinServiceModel_response_Operation();
            response.Code = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_START_SERVER;
            response.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_OK;
            response.Message = "Avvio listener...";
            this.Raise_Out(response);

            if (this.IndirizzoIP != null)
            {
                Fleck.FleckLog.Level = Fleck.LogLevel.Debug;
                this.oListenerFleck = new WebSocketServer(string.Format("ws://{0}:{1}", this.IndirizzoIPString, this.nPortListen));
                lRet = true;

                this.oListenerFleck.Start(socket =>
                {
                    socket.OnOpen = () =>
                    {
                        allSockets.Add(socket);
                        try
                        {
                            if (this.OnClientConnected != null) this.OnClientConnected(socket);
                        }
                        catch (Exception)
                        {
                        }
                    };

                    socket.OnClose = () =>
                    {
                        allSockets.Remove(socket);
                        try
                        {
                            if (this.OnClientClosed != null) this.OnClientClosed(socket);
                        }
                        catch (Exception)
                        {
                        }
                    };

                    socket.OnMessage = message =>
                    {
                        Interpreter(message, socket);
                    };

                });


            }

            if (lRet)
            {
                this.Listening = true;
                response.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_OK;
                response.Message = "Listener avviato.";
                this.Raise_Out(response);
            }
            else
            {
                this.Listening = false;
                response.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_ERR_GENERIC;
                response.Message = "Listener NON avviato.";
                this.Raise_Out(response);
            }
            return lRet;
        }

        public void StopListener()
        {
            this.Listening = false;
            if (this.oListenerFleck != null)
            {
                clsWinServiceModel_response_Operation response = new clsWinServiceModel_response_Operation();
                response.Code = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_STOP_SERVER;
                response.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_OK;
                response.Message = "Stop server socket";
                this.Raise_Out(response);
                string message = Newtonsoft.Json.JsonConvert.SerializeObject(response);
                string messageCrypt = clsSecuritySocket.Encrypt(message);
                allSockets.ToList().ForEach(s => s.Send(messageCrypt));
                allSockets.ToList().ForEach(s => s.Close());
                this.oListenerFleck.Dispose();
                this.oListenerFleck = null;
            }
        }

        public void Interpreter(string messageCrypto, IWebSocketConnection oClient)
        {
            string message = clsSecuritySocket.Decrypt(messageCrypto);
            bool lContinue = false;

            clsWinServiceModel_request_Operation request = null;
            clsWinServiceModel_Token token = null;

            try
            {
                request = Newtonsoft.Json.JsonConvert.DeserializeObject<clsWinServiceModel_request_Operation>( message);
            }
            catch (Exception EX)
            {
                request = null;
            }

            try
            {
                if (request != null && !string.IsNullOrEmpty(request.Token))
                {
                    this.Raise_In(request);

                    clsWinServiceModel_response_Operation response = new clsWinServiceModel_response_Operation();
                    response.Code = request.Code;

                    Exception Error = null;
                    Wintic.Data.IConnection oConnection = null;
                    try
                    {
                        oConnection = clsWinServiceOperations.GetConnection(this.ServiceName);
                        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open)
                        {
                            token = clsSecuritySocket.CheckSocketTokenOperazione(oConnection, request.Token, out Error);
                            lContinue = token != null && Error == null;

                            if (lContinue)
                            {
                                clsWinServiceConfig config = clsWinServiceConfig.GetWinServiceConfig(oConnection, out Error, true, this.ApplicationType);
                                lContinue = (Error == null);
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        if (oConnection != null)
                        {
                            oConnection.Close();
                            oConnection.Dispose();
                            oConnection = null;
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    response.ErrorCode = (Error == null ? "0" : "1");
                    response.Message = (Error == null ? "Richiesta presa in carico" : Error.Message);

                    this.Raise_Out(response);

                    string messageResponse = Newtonsoft.Json.JsonConvert.SerializeObject(response);
                    string messageResponseCrypt = clsSecuritySocket.Encrypt(messageResponse);
                    oClient.Send(messageResponseCrypt);
                   
                }
            }
            catch (Exception)
            {
            }

            if (lContinue)
            {
                Interpreter_internal(request, token, oClient);
            }
            else
            {
                if (this.OnClientRefused != null)
                {
                    try
                    {
                        this.OnClientRefused(messageCrypto, oClient);
                    }
                    catch (Exception)
                    {
                    }
                }
                clsWinServiceModel_response_Operation response = new clsWinServiceModel_response_Operation();
                response.Code = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_ERR_GENERIC;
                response.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_ERR_GENERIC;
                response.Message = "ByeBye";
                this.SendToClient(oClient, response);
                oClient.Close();
            }
        }

        private void Interpreter_internal(clsWinServiceModel_request_Operation request, clsWinServiceModel_Token token, IWebSocketConnection oClient)
        {
            Exception error = null;
            try
            {
                //if (this.EventServerSocketRiepiloghi_Execute != null) EventServerSocketRiepiloghi_Execute(request);


                if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO || 
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_SEND_EMAIL ||
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RECEIVE_EMAIL ||
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES ||
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_STOP_SERVER ||
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_ENABLED_LOGS ||
                    request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DISABLED_LOGS)
                    if (this.EventServerSocketRiepiloghi_Execute != null) EventServerSocketRiepiloghi_Execute(request, out error);
            }
            catch (Exception ex)
            {
            }
        }

        public void SendToClient(IWebSocketConnection socket, clsWinServiceModel_response_Operation Message)
        {
            string cDataToSend = Newtonsoft.Json.JsonConvert.SerializeObject(Message);
            string cDataToSendCrypt = clsSecuritySocket.Encrypt(cDataToSend);
            socket.Send(cDataToSendCrypt);
        }

        public void SendToAllClients(clsWinServiceModel_response_Operation Message)
        {
            string cDataToSend = Newtonsoft.Json.JsonConvert.SerializeObject(Message);
            string cDataToSendCrypt = clsSecuritySocket.Encrypt(cDataToSend);
            allSockets.ToList().ForEach(s => s.Send(cDataToSendCrypt));
        }

        private void Raise_In(clsWinServiceModel_request_Operation request)
        {
            if (this.EventServerSocketRiepiloghi_In != null)
            {
                this.EventServerSocketRiepiloghi_In(request);
            }
        }

        private void Raise_Out(clsWinServiceModel_response_Operation response)
        {
            if (this.EventServerSocketRiepiloghi_Out != null)
            {
                this.EventServerSocketRiepiloghi_Out(response);
            }
        }
    }

    public class SocketClientRiepiloghi : IDisposable
    {
        private WebSocket4Net.WebSocket oSocket;
        private bool lOpened = false;
        private bool lReceived = false;
        public Exception Error = null;
        public string DataReceived = "";
        public List<string> ListDataReceived = new List<string>();

        public delegate void delegateOpened(SocketClientRiepiloghi socket);
        public delegate void delegateClosed(SocketClientRiepiloghi socket);
        public delegate void delegateError(SocketClientRiepiloghi socket, Exception e);
        public delegate void delegateReceived(SocketClientRiepiloghi socket, string data);

        public event delegateOpened OnOpened;
        public event delegateClosed OnClosed;
        public event delegateError OnError;
        public event delegateReceived OnReceived;

        public SocketClientRiepiloghi(string cIpAddress, int nPort)
        {
            this.oSocket = new WebSocket4Net.WebSocket(string.Format("ws://{0}:{1}/websocket/rbm16-d", cIpAddress, nPort.ToString()));
            this.oSocket.Opened += OSocket_Opened;
            this.oSocket.Error += OSocket_Error;
            this.oSocket.Closed += OSocket_Closed;
            this.oSocket.MessageReceived += OSocket_MessageReceived;
        }
        public bool IsOpened()
        {
            return (oSocket != null ? oSocket.State == WebSocket4Net.WebSocketState.Open : false);
        }
        public bool Open()
        {
            bool lRet = false;
            this.lOpened = false;
            this.Error = null;
            this.ListDataReceived = new List<string>();
            try
            {
                DateTime dStart = DateTime.Now;
                this.oSocket.Open();
                while (!this.lOpened && (DateTime.Now - dStart).TotalMilliseconds < 5000)
                {
                    System.Threading.Thread.Sleep(10);
                }
                if (oSocket.State == WebSocket4Net.WebSocketState.Connecting)
                {
                    while (!this.lOpened && (DateTime.Now - dStart).TotalMilliseconds < 5000)
                    {
                        System.Threading.Thread.Sleep(10);
                    }
                }
                lRet = (this.lOpened && this.Error == null);
            }
            catch (Exception ex)
            {
                lRet = false;
                this.Error = ex;
            }
            return lRet;
        }

        public bool Close()
        {
            bool lRet = false;
            this.Error = null;
            this.ListDataReceived = new List<string>();
            try
            {
                DateTime dStart = DateTime.Now;
                this.oSocket.Close();
                while (this.lOpened && (DateTime.Now - dStart).TotalMilliseconds < 5000)
                {
                    System.Threading.Thread.Sleep(10);
                }
                lRet = (!this.lOpened && this.Error == null);
            }
            catch (Exception ex)
            {
                lRet = false;
                this.Error = ex;
            }
            return lRet;
        }

        public bool Send(string message)
        {
            bool lRet = false;

            this.lReceived = false;
            this.DataReceived = "";
            this.Error = null;
            try
            {
                DateTime dStart = DateTime.Now;
                string messageEncrypted = clsSecuritySocket.Encrypt(message);
                this.oSocket.Send(messageEncrypted);
                while (!this.lReceived && (DateTime.Now - dStart).TotalMilliseconds < 5000)
                {
                    System.Threading.Thread.Sleep(10);
                }
                lRet = (this.lReceived && this.Error == null);
            }
            catch (Exception ex)
            {
                lRet = false;
                this.Error = ex;
            }
            return lRet;
        }


        private void OSocket_Opened(object sender, EventArgs e)
        {
            this.lOpened = true;
            if (this.OnOpened != null) this.OnOpened(this);
        }
        private void OSocket_Closed(object sender, EventArgs e)
        {
            this.lOpened = false;
            if (this.OnClosed != null) this.OnClosed(this);
        }
        private void OSocket_Error(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            this.Error = e.Exception;
            if (this.OnError != null) this.OnError(this, e.Exception);
        }

        private void OSocket_MessageReceived(object sender, WebSocket4Net.MessageReceivedEventArgs e)
        {
            this.lReceived = true;
            this.DataReceived = clsSecuritySocket.Decrypt(e.Message);
            this.ListDataReceived.Add(this.DataReceived);
            if (this.OnReceived != null) this.OnReceived(this, this.DataReceived);
        }

        #region IDisposable Support

        private bool disposedValue = false; // Per rilevare chiamate ridondanti

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: eliminare lo stato gestito (oggetti gestiti).
                }

                // TODO: liberare risorse non gestite (oggetti non gestiti) ed eseguire sotto l'override di un finalizzatore.
                if (this.oSocket != null)
                {
                    while (this.oSocket.State == WebSocket4Net.WebSocketState.Connecting)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                    if (this.oSocket.State == WebSocket4Net.WebSocketState.Open)
                    {
                        this.oSocket.Close();
                    }
                    while (this.oSocket.State == WebSocket4Net.WebSocketState.Closing)
                    {
                        System.Threading.Thread.Sleep(100);
                    }

                    this.oSocket.Opened -= OSocket_Opened;
                    this.oSocket.Error -= OSocket_Error;
                    this.oSocket.Closed -= OSocket_Closed;
                    this.oSocket.MessageReceived -= OSocket_MessageReceived;

                    this.oSocket.Dispose();
                    this.oSocket = null;
                }

                // TODO: impostare campi di grandi dimensioni su Null.
                this.lOpened = false;
                this.lReceived = false;
                this.Error = null;
                this.DataReceived = "";
                this.ListDataReceived = null;

                disposedValue = true;
            }
        }

        // TODO: eseguire l'override di un finalizzatore solo se Dispose(bool disposing) include il codice per liberare risorse non gestite.
        // ~SocketClientRiepiloghi() {
        //   // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
        //   Dispose(false);
        // }

        // Questo codice viene aggiunto per implementare in modo corretto il criterio Disposable.
        public void Dispose()
        {
            // Non modificare questo codice. Inserire il codice di pulizia in Dispose(bool disposing) sopra.
            Dispose(true);
            // TODO: rimuovere il commento dalla riga seguente se è stato eseguito l'override del finalizzatore.
            GC.SuppressFinalize(this);
        }
        #endregion


    }
}
