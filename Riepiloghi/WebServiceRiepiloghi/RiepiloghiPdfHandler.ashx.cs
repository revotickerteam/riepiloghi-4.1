﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Wintic.Data;

namespace WebServiceRiepiloghi
{
    /// <summary>
    /// Summary description for RiepiloghiPdfHandler
    /// </summary>
    /// 

    // http://localhost:50828/RiepiloghiPdfHandler.ashx?token=q2dUmdLrkNOs%2bMBhKeA4euIZkEy9Y27naI6V63gTZJn/vo7hQmT3ATbELryggn5gUaSj7wZ3LZ0xsPI4i7tMFw==&servicename=WIN10X64SERVERORACLE&type=RPD&GIORNO=01-09-2016
    // http://crea-andrea/WebServiceRiepiloghi/RiepiloghiPdfHandler.ashx?token=q2dUmdLrkNOs%2bMBhKeA4euIZkEy9Y27naI6V63gTZJn/vo7hQmT3ATbELryggn5gUaSj7wZ3LZ0xsPI4i7tMFw==&servicename=WIN10X64SERVERORACLE&type=RPD&GIORNO=01-09-2016

    public class RiepiloghiPdfHandler : IHttpHandler
    {
        private string Token = "";

        private bool lRiepiloghi = false;
        private string GiornalieroMensile = "";
        private DateTime Giorno = DateTime.MinValue;
        private Int64 ProgRiepilogo = 0;
        private bool lDettaglioOmaggiEccedenti = false;

        private bool lTransazioni = false;
        private bool lPeriodoValido = false;
        private bool lAnnulli = false;
        private bool lUnaTransazionePerPagina = false;
        private DateTime DataEmiInizio = DateTime.MinValue;
        private DateTime DataEmiFine = DateTime.MinValue;

        private bool lRicercaTransazione = false;
        private bool lBordero = false;
        private string Carta = "";
        private string Sigillo = "";
        private Int64 Progressivo = 0;

        private Int64 IdFilterExtIdVend = 0;

        private string cPdfToken = "";
        private string cPdfData = "";

        public void ProcessRequest(HttpContext context)
        {
            Exception oError = null;
            string cFilePdf = "";
            bool lRet = false;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.clsRiepiloghi.PathRisorse = HttpContext.Current.Server.MapPath("//RisorseWeb");
            if (this.GetParameters(context, out oError))
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                }

                if (oError == null)
                {
                    try
                    {
                        String[] aCodiciLocali = null;
                        List<string> listCodiciLocali = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);
                        aCodiciLocali = listCodiciLocali.ToArray();

                        // Richietsa stampa riepiloghi o similazione
                        if (lRiepiloghi)
                        {
                            if (GiornalieroMensile == "D")      // SIMULAZIONE MENSILE PER DATA EVENTO
                            {
                                
                                lRet = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_STAMPA_SIMULAZIONE_SALA", this.Token, out oError);

                                if (lRet)
                                {
                                    string FilePdfBuffered = Riepiloghi.clsRiepiloghi.GetTempDirApp() + "\\" + libRiepiloghiBase.clsLibSigillo.GetNomeRiepilogoBase(Giorno, GiornalieroMensile, 0) + ".pdf";
                                    lRet = Riepiloghi.clsRiepiloghi.PrintToPDF_SimulazioneMensile(Giorno, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, this.lDettaglioOmaggiEccedenti, out oError, "", out cFilePdf, aCodiciLocali, 0);
                                }
                            }
                            else
                            {
                                lRet = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_STAMPA", this.Token, out oError);

                                if (lRet)
                                {
                                    // se non viene passato il progressivo del riepilog prendo l'ultimo generato
                                    if (this.ProgRiepilogo == 0)
                                    {
                                        this.ProgRiepilogo = Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError);
                                        lRet = (this.ProgRiepilogo > 0);
                                    }
                                    else
                                    {
                                        if (this.ProgRiepilogo <= Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError))
                                        {
                                            lRet = (oError == null);
                                        }
                                    }

                                    if (this.ProgRiepilogo > 0)
                                    {
                                        lRet = Riepiloghi.clsRiepiloghi.PrintRiepilogoToPDF(Giorno, GiornalieroMensile, ProgRiepilogo, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, lDettaglioOmaggiEccedenti, out oError, "", out cFilePdf, null, 0);
                                    }
                                    else
                                    {
                                        lRet = false;
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Riepilogo", "Riepilogo non generato.");
                                    }
                                }
                            }
                        }

                        // Log delle transazioni
                        if (lTransazioni || lAnnulli)
                        {
                            lRet = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", this.Token, out oError);
                            if (lRet)
                            {
                                lRet = Riepiloghi.clsRiepiloghi.PrintTransazioni(DataEmiInizio, DataEmiFine, lAnnulli, lUnaTransazionePerPagina, null, null, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, out oError, "", out cFilePdf, IdFilterExtIdVend, listCodiciLocali);
                            }
                        }

                        // ricerca transazione
                        if (lRicercaTransazione)
                        {
                            lRet = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", this.Token, out oError);
                            if (lRet)
                            {
                                lRet = Riepiloghi.clsRiepiloghi.PrintTransazioneCARTA_SIGILLO_PROGRESSIVO(Carta, Sigillo, Progressivo, oConnection, out oError, "", out cFilePdf);
                            }
                        }

                        if (lBordero)
                        {
                            cPdfData = libBordero.Bordero.GetTokenPdfGenerated(oConnection, cPdfToken, out oError);
                            lRet = (oError == null);

                            if (lRet)
                            {
                                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }

                                context.Response.Cache.SetExpires(DateTime.Now.AddDays(1));
                                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                                context.Response.Cache.SetValidUntilExpires(false);
                                context.Response.ContentType = "application/pdf";
                                byte[] Buffer = System.Convert.FromBase64String(cPdfData);
                                context.Response.AddHeader("content-length", Buffer.Length.ToString());
                                context.Response.BinaryWrite(Buffer);
                                context.Response.Flush();
                                context.Response.End();
                            }
                            else
                            {
                                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                                context.Response.ContentType = "text/plain";
                                if (oError != null)
                                {
                                    context.Response.Write("Errore: " + oError.Message);
                                }
                                context.Response.Flush();
                                context.Response.End();
                            }
                        }
                        else
                        {
                            if (lRet)
                            {
                                lRet = (cFilePdf.Trim() != "" && System.IO.File.Exists(cFilePdf));
                            }

                            if (lRet)
                            {
                                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }

                                context.Response.Cache.SetExpires(DateTime.Now.AddDays(1));
                                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                                context.Response.Cache.SetValidUntilExpires(false);
                                context.Response.ContentType = "application/pdf";
                                byte[] Buffer = System.IO.File.ReadAllBytes(cFilePdf);
                                context.Response.AddHeader("content-length", Buffer.Length.ToString());
                                context.Response.BinaryWrite(Buffer);
                                context.Response.Flush();
                                context.Response.End();

                                //try
                                //{
                                //    System.IO.File.Delete(cFilePdf);
                                //}
                                //catch
                                //{
                                //}
                            }
                            else
                            {
                                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                                context.Response.ContentType = "text/plain";
                                if (oError != null)
                                {
                                    context.Response.Write("Errore: " + oError.Message);
                                }
                                context.Response.Flush();
                                context.Response.End();
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                        if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
                    }
                }
                else
                {
                    if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                    context.Response.ContentType = "text/plain";
                    if (oError != null)
                    {
                        context.Response.Write("parametri di connessione non validi: " + oError.Message);
                    }
                    else
                    {
                        context.Response.Write("parametri connessione non validi.");
                    }
                }
            }
            else
            {
                if (oConnection != null) { oConnection.Close(); oConnection.Dispose(); }
                context.Response.ContentType = "text/plain";
                if (oError != null)
                {
                    context.Response.Write("parametri non validi: " + oError.Message);
                }
                else
                {
                    context.Response.Write("parametri non validi.");
                }
                context.Response.Flush();
                context.Response.End();
            }
        }

        private object GetSingleParameter(HttpContext context, string paramName)
        {
            object oValue = null;
            try
            {
                oValue = context.Request[paramName].ToString();
            }
            catch
            {
                oValue = "";
            }
            return oValue;
        }
        
        // Lettura parametri
        private bool GetParameters(HttpContext context, out Exception oError)
        {
            bool lRet = false;
            oError = null;

            if (GetSingleParameter(context, "token").ToString().Trim() != "")
            {
                this.Token = GetSingleParameter(context, "token").ToString().Trim();
            }
           
            if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPG") // Stampa Riepilogo giornaliero
            {
                GiornalieroMensile = "G";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPM") // Stampa Riepilogo mensile
            {
                GiornalieroMensile = "M";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "RPD")// Stampa Simulazione mensile per data evento
            {
                GiornalieroMensile = "D";
                lRiepiloghi = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "LOG") // Stampa log delle transazioni
            {
                lTransazioni = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "ANN") // Stampa log transazioni annullate
            {
                lAnnulli = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "SRC") // Stampa Ricerca transazione
            {
                lRicercaTransazione = true;
            }
            else if (GetSingleParameter(context, "type").ToString().Trim().ToUpper() == "BRD") // Stampa item Bordero
            {
                lBordero = true;
                lRet = true;
            }
            else if (GetSingleParameter(context, "idfilter").ToString().Trim() != "")
            {
                if (!Int64.TryParse(GetSingleParameter(context, "idfilter").ToString(), out this.IdFilterExtIdVend))
                {
                    this.IdFilterExtIdVend = 0;
                }
            }

            if (lRiepiloghi)
            {
                if (GetSingleParameter(context, "giorno").ToString().Trim() != "" && DateTime.TryParse(GetSingleParameter(context, "giorno").ToString(), out Giorno))
                {
                    lRet = true;
                }
                else
                {
                    lRet = false;
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Riepilogo", "Giorno non valido.");
                }
            }

            if (lTransazioni || lAnnulli)
            {
                lPeriodoValido = DateTime.TryParse(GetSingleParameter(context, "dal").ToString(), out DataEmiInizio) &&
                                 DateTime.TryParse(GetSingleParameter(context, "al").ToString(), out DataEmiFine);

                if (lPeriodoValido)
                {
                    lPeriodoValido = DataEmiInizio <= DataEmiFine;
                }

                if (lPeriodoValido)
                {
                    DataEmiInizio = DataEmiInizio.Date;
                    DataEmiFine = DataEmiFine.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                if (!lPeriodoValido)
                {
                    lRet = false;
                    if (lTransazioni)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Transazioni", "Periodo non valido.");
                    }
                    else
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Transazioni Annullate", "Periodo non valido.");
                    }
                }
                else
                {
                    lRet = true;
                }

                lUnaTransazionePerPagina = false;
                if (GetSingleParameter(context, "style").ToString().Trim().ToUpper() == "TAB")
                {
                    lUnaTransazionePerPagina = false;
                }
                else
                {
                    lUnaTransazionePerPagina = true;
                }
            }

            if (lRicercaTransazione)
            {

                if (GetSingleParameter(context, "carta").ToString().Trim().Length == 8)
                {
                    Carta = GetSingleParameter(context, "carta").ToString().Trim().ToUpper();
                    if (GetSingleParameter(context, "sigillo").ToString().Trim().Length > 0)
                    {
                        Sigillo = GetSingleParameter(context, "sigillo").ToString().Trim().ToUpper();
                        lRet = (Sigillo.Length == 16);
                        if (!lRet)
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Sigillo", "Sigillo non valido: [" + GetSingleParameter(context, "sigillo") + "]");
                        }
                    }
                    else
                    {
                        if (Int64.TryParse(GetSingleParameter(context, "progressivo").ToString().Trim(), out Progressivo))
                        {
                            lRet = true;
                        }
                        else
                        {
                            lRet = false;
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Progressivo", "Progressivo non valido: [" + GetSingleParameter(context, "progressivo") + "]");
                        }
                    }
                }
                else
                {
                    lRet = false;
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa transazione per Carta e Sigillo/Progressivo", "Carta non valida: [" + GetSingleParameter(context, "carta") + "]");
                }

            }

            if (lBordero)
            {
                try
                {
                    cPdfToken = GetSingleParameter(context, "pdf_token").ToString().Trim();
                    if (cPdfToken != null && cPdfToken.Length > 0)
                    {
                        lRet = true;
                    }
                    else
                    {
                        lRet = false;
                    }
                }
                catch (Exception ex)
                {
                    oError = ex;
                    lRet = false;
                }

                if (!lRet)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Stampa Bordero", "Borderò non valido.");
                }
            }

            return lRet;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}