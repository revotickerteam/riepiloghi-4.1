﻿// app.js
angular.module('generalApp', [])
.controller('menuController', function ($scope, $http) {
    $scope.currentPage = 'WebClient.html';
    $scope.menuItems = [];
    var urlMenuRiepiloghi = "App_Code/ClientRiepiloghi.asmx/GetMenuRiepiloghi?fromPage=" + $scope.currentPage;
    $http.get(urlMenuRiepiloghi)
                       .success(function (data) {
                           $scope.menuItems = data;
                       })
})
.controller('generalController', function ($scope, $http) {
    $scope.CodiceSistema = '';
    var urlCodiceSistema = "App_Code/ClientRiepiloghi.asmx/GetCodiceSistema";
    $http.get(urlCodiceSistema)
                       .success(function (data) {
                           $scope.CodiceSistema = data.codice_sistema;
                       });

})
.controller('dataController', function ($scope, $filter) {
    $scope.dCalendario = Date.now();

    $scope.days = [{ Description: "Lunedi", Index: 1, Simbol: "L" },
                   { Description: "Martedi", Index: 2, Simbol: "M" },
                   { Description: "Mercoledi", Index: 3, Simbol: "M" },
                   { Description: "Giovedi", Index: 4, Simbol: "G" },
                   { Description: "Venerdi", Index: 5, Simbol: "V" },
                   { Description: "Sabato", Index: 6, Simbol: "S" },
                   { Description: "Domenica", Index: 7, Simbol: "D"}];

    $scope.getIndexDayOfWeek = function (dDate) {
        var nIndex = 0;
        var cDay = $filter('date')(dDate, 'EEE');
        if (cDay == "Mon") {
            nIndex = 1;
        }
        else if (cDay == "Tue") {
            nIndex = 2;
        }
        else if (cDay == "Wed") {
            nIndex = 3;
        }
        else if (cDay == "Thu") {
            nIndex = 4;
        }
        else if (cDay == "Fri") {
            nIndex = 5;
        }
        else if (cDay == "Sat") {
            nIndex = 6;
        }
        else if (cDay == "Sun") {
            nIndex = 7;
        }
        return nIndex;
    }

    $scope.getFirstDayCalendar = function (dDate) {
        var dRet = new Date($filter('date')(dDate, 'yyyy'), $filter('date')(dDate, 'MM'), 0);
        dRet.setDate(1);
        var nIndex = $scope.getIndexDayOfWeek(dRet);
        if (nIndex > 1) {
            dRet.setDate(-nIndex + 2);
        }
        return dRet;
    }

    $scope.getLastDayCalendar = function (dDate) {
        var dRet = new Date($filter('date')(dDate, 'yyyy'), $filter('date')(dDate, 'MM'), 0);
        var nIndex = $scope.getIndexDayOfWeek(dRet);
        if (nIndex < 7) {
            dRet = new Date($filter('date')(dDate, 'yyyy'), $filter('date')(dDate, 'MM'), 2);
            if (nIndex < 6) {
                dRet.setDate(6 - nIndex);
            }
        }
        return dRet;
    }

    $scope.getNumberOfDay = function (dDate) {
        return $filter('date')(dDate, 'dd');
    };

    $scope.getArrayCalendar = function (dDate) {
        var dStart = $scope.getFirstDayCalendar(dDate);
        var dStop = $scope.getLastDayCalendar(dDate);
        var nDays = parseInt((dStop - dStart) / (24 * 3600 * 1000)) + 1;
        var nRows = (nDays / 7);
        if (nRows != parseInt(nRows)) {
            nRows = parseInt(nRows) + 1;
        }
        var days = [nRows]
        var i = 0;
        for (nRow = 0; nRow < nRows; nRow++) {
            var aRow = [7];
            var Row = { Days: [7], Index: nRow };
            for (nDay = 0; nDay < 7; nDay++) {
                Row.Days[nDay] = { Value: $scope.addDays(dStart, i), Index: nDay };
                i++;
            }
            days[nRow] = Row;
        }
        return days;
    }


    $scope.addDays = function (date, amount) {
        var tzOff = date.getTimezoneOffset() * 60 * 1000,
            t = date.getTime(),
            d = new Date(),
            tzOff2;

        t += (1000 * 60 * 60 * 24) * amount;
        d.setTime(t);

        tzOff2 = d.getTimezoneOffset() * 60 * 1000;
        if (tzOff != tzOff2) {
            var diff = tzOff2 - tzOff;
            t += diff;
            d.setTime(t);
        }

        return d;
    }

    $scope.IsDateCalendar = function (day, dDate) {
        return $filter('date')(day, 'yyyy-MM-dd') == $filter('date')(dDate, 'yyyy-MM-dd');
    }

    $scope.GetMonthDesc = function (date) {
        var Month = $filter('date')(date, 'MM');
        var cRet = "";
        if (Month == "01") {
            cRet = "Gennaio";
        }
        else if (Month == "02") {
            cRet = "Febbrario";
        }
        else if (Month == "03") {
            cRet = "Marzo";
        }
        else if (Month == "04") {
            cRet = "Aprile";
        }
        else if (Month == "05") {
            cRet = "Maggio";
        }
        else if (Month == "06") {
            cRet = "Giugno";
        }
        else if (Month == "07") {
            cRet = "Luglio";
        }
        else if (Month == "08") {
            cRet = "Agosto";
        }
        else if (Month == "09") {
            cRet = "Settembre";
        }
        else if (Month == "10") {
            cRet = "Ottobre";
        }
        else if (Month == "11") {
            cRet = "Novembre";
        }
        else if (Month == "12") {
            cRet = "Dicembre";
        }
        return cRet;
    }

    $scope.aCalendario = $scope.getArrayCalendar($scope.dCalendario);

    $scope.SetDataCella = function (value) {
        $scope.dCalendario = value;
        $scope.aCalendario = $scope.getArrayCalendar($scope.dCalendario);
    };

});


