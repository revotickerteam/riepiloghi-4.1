﻿// app.js
angular.module('transazioniApp', [])
.controller('menuController', function ($scope, $http) {
    $scope.currentPage = 'Transazioni.html';
    $scope.menuItems = [];
    var urlMenuRiepiloghi = "App_Code/ClientRiepiloghi.asmx/GetMenuRiepiloghi?fromPage=" + $scope.currentPage;
    $http.get(urlMenuRiepiloghi)
                       .success(function (data) {
                           $scope.menuItems = data;
                       })
})
.controller('transazioniController', function ($scope, $filter, $http) {

    $scope.RowSelectedIndex = -1;
    $scope.RowSelected;
    $scope.currentPage = 'Transazioni.html';
    $scope.menuItems = [];
    var urlMenuRiepiloghi = "App_Code/ClientRiepiloghi.asmx/GetMenuRiepiloghi?fromPage=" + $scope.currentPage;
    $http.get(urlMenuRiepiloghi)
                       .success(function (data) {
                           $scope.menuItems = data;
                       });

    $scope.CodiceSistema = '';
    var urlCodiceSistema = "App_Code/ClientRiepiloghi.asmx/GetCodiceSistema";
    $http.get(urlCodiceSistema)
                       .success(function (data) {
                           $scope.CodiceSistema = data.codice_sistema;
                       });



    $scope.sortType = '';
    $scope.sortDesc = '';
    $scope.sortReverse = false;
    $scope.loadingTransazioni = false;
    
    $scope.dInizio = $filter('date')(Date.now(), 'yyyy-MM-dd');
    $scope.dTermin = $filter('date')(Date.now(), 'yyyy-MM-dd');
    $scope.nPage = 0;
    $scope.nPages = 0;
    $scope.aPages = [];
    $scope.nRowsPerPage = 100;
    $scope.nRowsTransazioni = 0;
    $scope.filterKeyLoading = '';
    var lastWeek = $filter('date')($scope.dInizio, 'yyyy-MM-dd');




    var urlCampi = "App_Code/ClientRiepiloghi.asmx/GetCampiTransazioni";
    $http.get(urlCampi)
                       .success(function (data) {
                           $scope.fields = data;
                           //$scope.sortType = $scope.fields[0].Key;
                           //$scope.sortDesc = $scope.fields[0].Desc;
                       });

    $scope.getTransazioniFunction = function () {
        $scope.loadingTransazioni = true;

        var urlTransazioni = "App_Code/ClientRiepiloghi.asmx/GetTransazioni?StartStringDate=" + $filter('date')($scope.dInizio, 'yyyyMMdd') + "&StopStringDate=" + $filter('date')($scope.dTermin, 'yyyyMMdd') + "&Page=" + $scope.nPage.toString() + "&RowsPerPage=" + $scope.nRowsPerPage.toString() + "&Filters=";
        for (i = 0; i < $scope.fields.length; i++) {
            if ($scope.fields[i].FilterValue != '') {
                urlTransazioni += $scope.fields[i].FieldName + "=" + $scope.fields[i].FilterValue + ";";
                break;
            }
        }
        $http.get(urlTransazioni)
                           .success(function (data, status, header) {
                               $scope.nPage = data.page;
                               $scope.nRowsTransazioni = data.count;
                               $scope.nPages = data.pages;
                               $scope.aPages = [$scope.nPages];
                               for (var i = 0; i < $scope.nPages - 1; i++) {
                                   $scope.aPages[i] = i + 1;
                               }
                               $scope.rows = data.message;
                           })
                           .finally(function () {
                               $scope.loadingTransazioni = false;
                               //$scope.$apply();
                           });
    }

    $scope.SetRowIndexSelected = function (Row) {
        $scope.RowSelectedIndex = Row["_IndexRow"];
        $scope.RowSelected = Row;
    }

    $scope.dropboxitemselected = function (SingleFilter) {
        for (i = 0; i < $scope.fields.length; i++) {
            if ($scope.fields[i].FieldName == SingleFilter.FieldName) {
                $scope.filterKeyLoading = SingleFilter.FieldName;
                if (SingleFilter.FilterValue.toString() == "") {
                    $scope.fields[i].FilterValue = "";
                    $scope.fields[i].FilterDesc = "";
                }
                else {
                    $scope.fields[i].FilterValue = SingleFilter.FilterValue;
                    $scope.fields[i].FilterDesc = SingleFilter.FieldDesc;
                }
                $scope.filterKeyLoading = '';
                break;
            }
        }
    }

    $scope.orderFunction = function (FieldName) {
        if ($scope.sortType != FieldName) {
            $scope.sortType = FieldName;
            $scope.sortReverse = false;
        }
        else {
            $scope.sortReverse = !$scope.sortReverse;
        }

        for (i = 0; i < $scope.fields.length; i++) {
            if ($scope.fields[i].FieldName == FieldName) {
                $scope.sortDesc = $scope.fields[i].FieldDesc;
                break;
            }
        }
    }

    $scope.filterFunction = function (element) {
        var lFilterOK = true;
        for (i = 0; i < $scope.fields.length; i++) {
            var value = new String(element[$scope.fields[i].FieldName]).toUpperCase();
            var filter = new String($scope.fields[i].FilterValue).toUpperCase();
            if (filter != '' && !value.match(filter)) {
                lFilterOK = false;
                break;
            }
        }
        return lFilterOK;
    };

});

