﻿angular.module('transazioniApp', [])
.controller('menuController', function ($scope, $http) {
    $scope.currentPage = 'Transazioni.html';
    $scope.menuItems = [];
    var urlMenuRiepiloghi = "App_Code/ClientRiepiloghi.asmx/GetMenuRiepiloghi?fromPage=" + $scope.currentPage;
    $http.get(urlMenuRiepiloghi)
                       .success(function (data) {
                           $scope.menuItems = data;
                       })
})
.controller('tableController', function ($scope, $filter, $http) {
    $scope.getTableFunction = function () {
        var urlTable = "App_Code/ClientRiepiloghi.asmx/GetTable?Query=" + $scope.Query;
        $http.get(urlTable)
                           .success(function (data, status, header) {
                               $scope.TableCols = data.Cols;
                               $scope.TableRows = data.Rows;
                           })
    };
};