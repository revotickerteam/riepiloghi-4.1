﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Text;

/// <summary>
    /// Descrizione di riepilogo per WebServiceToRiepiloghi
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Per consentire la chiamata di questo servizio Web dallo script utilizzando ASP.NET AJAX, rimuovere il commento dalla riga seguente. 
    [System.Web.Script.Services.ScriptService]
    public class WebServiceToRiepiloghi : System.Web.Services.WebService
    {
        WebServiceRiepiloghi.ServiceRiepiloghi oService;

        private string DataSource = "serverx";
        private string UserName = "crea";
        private string Password = "crea";
        
        public WebServiceToRiepiloghi()
        {
            this.oService = new WebServiceRiepiloghi.ServiceRiepiloghi();
        }

        public class ResponseCodiceSistema
        {
            public string codice_sistema = "";
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetCodiceSistema(string Token)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseCodiceSistema oResult = oService.GetCodiceSistema(Token);
            ResponseCodiceSistema data = new ResponseCodiceSistema();
            data.codice_sistema = oResult.CodiceSistema;
            Context.Response.Write(JsonConvert.SerializeObject(data));
        }

        public class ResponseItemMenuRiepiloghi
        {
            public string page = "";
            public string description = "";
            public string className = "";

            public ResponseItemMenuRiepiloghi()
            {
            }

            public ResponseItemMenuRiepiloghi(string Page, string Description)
            {
                this.page = Page;
                this.description = Description;
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetMenuRiepiloghi(string fromPage)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            List<ResponseItemMenuRiepiloghi> oResult = new List<ResponseItemMenuRiepiloghi>();
            oResult.Add(new ResponseItemMenuRiepiloghi("WebClient.html", "Home"));
            oResult.Add(new ResponseItemMenuRiepiloghi("Transazioni.html", "Transazioni Fiscali"));
            oResult.Add(new ResponseItemMenuRiepiloghi("RiepiloghiGiornalieri.html", "Riepiloghi Giornalieri"));
            oResult.Add(new ResponseItemMenuRiepiloghi("RiepiloghiMensili.html", "Riepiloghi Mensili"));
            oResult.Add(new ResponseItemMenuRiepiloghi("Annulli.html", "Annulli"));
            oResult.Add(new ResponseItemMenuRiepiloghi("FunzioniDiServizio.html", "Funzioni di servizio"));
            oResult.Add(new ResponseItemMenuRiepiloghi("SimulazioneMensile.html", "Simulazione Mensile per Data Evento"));
            oResult.Add(new ResponseItemMenuRiepiloghi("Configurazione.html", "Configurazione"));

            foreach (ResponseItemMenuRiepiloghi ItemMenu in oResult)
            {
                if (ItemMenu.page == fromPage)
                {
                    ItemMenu.className = "active";
                }
            }

            Context.Response.Write(JsonConvert.SerializeObject(oResult));
        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetCampiTransazioni()
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLogTransazioni oResult = oService.GetListaCampiLogTransazioni("");
            ResponseTransazioni data = new ResponseTransazioni();
            List<ItemCampo> ListaCampi = new List<ItemCampo>();
            ItemCampo oItem;

            foreach (WebServiceRiepiloghi.ServiceRiepiloghi.clsResultBaseCampoLogTransazioni Campo in oResult.Campi)
            {
                oItem = new ItemCampo(Campo.Campo, Campo.DescCampo, Campo.Tipo);
                if (oItem.FieldName == "ANNULLATO")
                {
                    oItem.FieldDesc = "Emissione/Annullo";
                }
                foreach (Riepiloghi.clsFiltroCampoTransazione ItemPreFiltro in oResult.Filtri)
                {
                    if (Campo.Campo == ItemPreFiltro.Campo)
                    {
                        oItem.FieldPreFilters = new List<ItemCampo>();
                        ItemCampo oValorePreFiltro;
                        foreach (Riepiloghi.clsValoreFiltroCampoTransazione ItemValoreFiltro in ItemPreFiltro.ValoriPredefiniti)
                        {
                            if (oItem.FieldPreFilters.Count == 0)
                            {
                                oValorePreFiltro = new ItemCampo(oItem.FieldName, "Tutti", ItemPreFiltro.Tipo);
                                oValorePreFiltro.FilterValue = "";
                                oItem.FieldPreFilters.Add(oValorePreFiltro);
                            }
                            oValorePreFiltro = new ItemCampo(oItem.FieldName, ItemValoreFiltro.Descrizione, ItemPreFiltro.Tipo);
                            oValorePreFiltro.FilterValue = ItemValoreFiltro.ValoreStringa;
                            oItem.FieldPreFilters.Add(oValorePreFiltro);
                        }


                        break;
                    }
                }
                ListaCampi.Add(oItem);
            }


            Context.Response.Write(JsonConvert.SerializeObject(ListaCampi));
        }

        //[WebMethod]
        //[System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        //public void GetValoriFiltroCampo(string key, string type)
        //{
        //    Context.Response.Clear();
        //    Context.Response.ContentType = "application/json";

        //    List<ItemCampo> oRet = new List<ItemCampo>();
        //    WebServiceRiepiloghi.ServiceRiepiloghi.ResultPreFiltro oPreFiltro = oService.GetValoriPreFiltro("serverx", "crea", "crea", key);
        //    if (oPreFiltro != null && oPreFiltro.Valori != null)
        //    {
        //        List<ItemCampo> oValues = new List<ItemCampo>();
        //        ItemCampo oValorePreFiltro;
        //        foreach (Riepiloghi.clsValoreFiltroCampoTransazione ItemValoreFiltro in oPreFiltro.Valori)
        //        {
        //            if (oValues.Count == 0)
        //            {
        //                oValorePreFiltro = new ItemCampo(key, "Tutti", type);
        //                oValorePreFiltro.FilterValue = "";
        //                oValues.Add(oValorePreFiltro);
        //            }
        //            oValorePreFiltro = new ItemCampo(key, ItemValoreFiltro.Descrizione, type);
        //            oValorePreFiltro.FilterValue = ItemValoreFiltro.ValoreStringa;
        //            oValues.Add(oValorePreFiltro);
        //        }
        //        Context.Response.Write(JsonConvert.SerializeObject(oValues));
        //    }
        //    else
        //    {
        //        Context.Response.Write("");
        //    }
        //}

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetTransazioni(string Token, string StartStringDate, string StopStringDate, int Page, int RowsPerPage, string Filters)
        {
            string cSerialize = "";
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            DateTime dStart;
            DateTime dStop;
            
            ResponseTransazioni data = new ResponseTransazioni();
            WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseGetTransazioni oResult = null;
            WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLogTransazioni oResultCampi = null;

            if (DateTime.TryParseExact(StartStringDate, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out dStart) &&
                DateTime.TryParseExact(StopStringDate, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out dStop))
            {
                if (Filters.Trim() == "")
                {
                    oResult = oService.GetTransazioni(Token, dStart, dStop, "TRUE", "TRUE", 0);
                }
                else
                {
                    oResult = oService.GetTransazioniStringFilter(Token, dStart, dStop, "TRUE", Filters, "TRUE", 0);
                    
                }
                
                oResultCampi = oService.GetListaCampiLogTransazioni("");
            }
            if (oResult != null && oResult.TableTransazioni != null)
            {
                if (Page == 0)
                {
                    data.page = 1;
                }
                else
                {
                    data.page = Page;
                }
                data.rowsperpage = RowsPerPage;
                data.count = oResult.TableTransazioni.Rows.Count;
                data.pages = (int)System.Math.Floor((decimal)data.count / (decimal)data.rowsperpage);
                //data.message = ConvertDataTable(oResult.TableTransazioni, oResultCampi, data.page, data.rowsperpage);
                data.message = ConvertDataTable(oResult.TableTransazioni, oResultCampi, 0, 0);
                cSerialize = JsonConvert.SerializeObject(data);
            }
            else
            {
                //cSerialiaze = js.Serialize(data);
                cSerialize = JsonConvert.SerializeObject(data);
            }
            Context.Response.Write(cSerialize);
        }

        public List<Dictionary<String, Object>> ConvertDataTable(DataTable dataTable, WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLogTransazioni oResultCampi)
        {
            return ConvertDataTable(dataTable, oResultCampi, 0, 0);
        }

        public List<Dictionary<String, Object>> ConvertDataTable(DataTable dataTable, WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseCampiLogTransazioni oResultCampi, int nPage, int nRowsPerPage)
        {
            Newtonsoft.Json.JsonSerializer oS = new Newtonsoft.Json.JsonSerializer();
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            System.IO.StringWriter oT;


            System.Web.Script.Serialization.JavaScriptSerializer serializer =
                   new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<String, Object>> tableRows = new List<Dictionary<String, Object>>();

            Dictionary<String, Object> row;

            Dictionary<string, string> dataTypes = new Dictionary<string, string>();
            foreach (WebServiceRiepiloghi.ServiceRiepiloghi.clsResultBaseCampoLogTransazioni Campo in oResultCampi.Campi)
            {
                dataTypes.Add(Campo.Campo, Campo.Tipo);
            }

            int nFirstRow = ((nPage - 1) * nRowsPerPage) + 1;
            int nLastRow = nFirstRow + nRowsPerPage;
            int nIndexRow = 0;
            foreach (DataRow dr in dataTable.Rows)
            {
                nIndexRow += 1;
                if (nRowsPerPage == 0 || (nIndexRow >= nFirstRow && nIndexRow <= nLastRow))
                {
                    row = new Dictionary<String, Object>();
                    row.Add("_IndexRow", nIndexRow);
                    foreach (DataColumn col in dataTable.Columns)
                    {
                        object value = dr[col];
                        if (value != null && value.ToString() != "")
                        {
                            if (dataTypes.ContainsKey(col.ColumnName) && dataTypes[col.ColumnName] == "D")
                            {
                                try
                                {
                                    value = ((DateTime)dr[col]).ToShortDateString();
                                }
                                catch
                                {
                                }

                            }
                        }
                        row.Add(col.ColumnName, value);
                    }
                    tableRows.Add(row);
                }
                else if (nIndexRow > nLastRow)
                {
                    break;
                }
            }
            //return serializer.Serialize(tableRows);
            //return JsonConvert.SerializeObject(tableRows);
            return tableRows;

        }

        public class ResponseTransazioni
        {
            public  List<Dictionary<String, Object>> message;
            public int rowsperpage = 0;
            public int count = 0;
            public int pages = 0;
            public int page = 0;
        }

        public class ItemCampo
        {
            public string FieldName = "";
            public string FieldDesc = "";
            public string FieldFormat = "";
            public string FieldType = "";
            public List<ItemCampo> FieldPreFilters = new List<ItemCampo>();
            public string FilterValue = "";
            public string FilterDesc = "";

            public ItemCampo()
            {
            }

            public string canSelect
            {
                get 
                {
                        return (this.FieldPreFilters.Count > 0).ToString().Trim().ToUpper();
                }
            }

            public ItemCampo(string cKey, string cDesc, string cTipo)
            {
                this.FieldName = cKey;
                this.FieldDesc = cDesc;
                this.FieldType = cTipo;
                switch (cTipo)
                {
                    case "S": { this.FieldFormat = "'string'"; break; }
                    case "D": { this.FieldFormat = "'date:short'"; break; }
                    case "N": { this.FieldFormat = "'number'"; break; }
                    case "C": { this.FieldFormat = "'number'"; break; }
                }
            }
        }

        //[WebMethod]
        //[System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        //public void GetTable(string DataSource, string Query)
        //{
        //    string cSerialize = "";
        //    Context.Response.Clear();
        //    Context.Response.ContentType = "application/json";
        //    WebServiceRiepiloghi.ServiceRiepiloghi.ResultBaseGenericTable oTable = oService.GetGenericTable(DataSource, Query);
        //    cSerialize = JsonConvert.SerializeObject(oTable);
        //    Context.Response.Write(cSerialize);
        //}

    }
