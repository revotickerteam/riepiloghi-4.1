﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Wintic.Data;
using Wintic.Data.oracle;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Text;

namespace WebServiceRiepiloghi
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServiceRiepiloghi : System.Web.Services.WebService
    {
        // Classe di risposta base
        public class ResultBase
        {
            [XmlElement("statusok")]
            public bool StatusOK = true;

            [XmlElement("statusmessage")]
            public string StatusMessage = "";

            [XmlElement("statuscode")]
            public Int64 StatusCode = 0;

            [XmlElement("ExecutionTime")]
            public string execution_time = "";

            public ResultBase()
            {
            }

            public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }
        }

        #region "funzioni di base"

        // funzione che Recupera la connessione
        public static Wintic.Data.oracle.CConnectionOracle GetConnectionInternal(out Exception oError)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                string ServiceName = System.Configuration.ConfigurationManager.ConnectionStrings["internalConnectionString"].ConnectionString;
                if (ServiceName.Contains("/") && ServiceName.Contains("@"))
                {
                    oCon.ConnectionString = string.Format("user={0};password={1};data source={2};", ServiceName.Split('@')[0].Split('/')[0], ServiceName.Split('@')[0].Split('/')[1], ServiceName.Split('@')[1]);
                }
                else
                    oCon.ConnectionString = string.Format("user=wtic;password=obelix;data source={0};", ServiceName);
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch(Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
            }
            return oCon;
        }

        private static void WriteTokenRequest(IConnection ConnectionInternal, string Token)
        {
            try
            {
                Int64 nTOKEN_REQUEST_ID = Int64.Parse(ConnectionInternal.ExecuteNonQuery("INSERT INTO RP_TOKENS_REQUEST (TOKEN_STRING) VALUES (:pTOKEN_STRING)", "TOKEN_REQUEST_ID", new clsParameters(":pTOKEN_STRING", Token), true).ToString());
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                clsParameters oPars = new clsParameters();
                oPars.Add(":pTOKEN_JSON", Riepiloghi.clsToken.SerializeToken(oToken));
                oPars.Add(":pTOKEN_REQUEST_ID", nTOKEN_REQUEST_ID);
                ConnectionInternal.ExecuteNonQuery("UPDATE RP_TOKENS_REQUEST SET TOKEN_JSON = :pTOKEN_JSON WHERE TOKEN_REQUEST_ID = :pTOKEN_REQUEST_ID", oPars, true);
            }
            catch (Exception ex)
            {
            }
            
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnection(IConnection ConnectionInternal, string Token, string CodiceSistema, out Exception oError)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                WriteTokenRequest(ConnectionInternal, Token);
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA WHERE CODICE_SISTEMA = :pCODICE_SISTEMA", new clsParameters(":pCODICE_SISTEMA", CodiceSistema));

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione.", exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.", exGetConnection);
                }
            }
            return oCon;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnection(IConnection ConnectionInternal, string Token, out Exception oError)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                WriteTokenRequest(ConnectionInternal, Token);
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA WHERE CODICE_SISTEMA = :pCODICE_SISTEMA", new clsParameters(":pCODICE_SISTEMA", oToken.CodiceSistema.CodiceSistema));

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione." + exGetConnection.ToString(), exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
                }
            }
            return oCon;
        }

        // Converte una stringa nella modalità storico: TRUE, FALSE, CHECK
        private Riepiloghi.clsRiepiloghi.DefModalitaStorico GetModalitaStoricoFromString(string cValue)
        {
            Riepiloghi.clsRiepiloghi.DefModalitaStorico oModalitaStorico = Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check;
            foreach (Riepiloghi.clsRiepiloghi.DefModalitaStorico ItemEnum in Enum.GetValues(typeof(Riepiloghi.clsRiepiloghi.DefModalitaStorico)))
            {
                if (ItemEnum.ToString().Trim().ToUpper() == cValue.Trim().ToUpper())
                {
                    oModalitaStorico = ItemEnum;
                    break;
                }
            }
            return oModalitaStorico;
        }

        public static bool CheckUserOperazione(IConnection oConnection, string Operazione, string Token, out Exception oError)
        {
            bool lRet = false;
            oError = null;
            Riepiloghi.clsToken oToken = null;
            if (Riepiloghi.clsToken.CheckToken(oConnection, Token, out oError, out oToken))
            {
                lRet = Riepiloghi.clsRiepiloghi.CheckIdOperazioneAccount(oConnection, Operazione, oToken, out oError);
            }
            return lRet;
        }

        [WebMethod]
        public ResultBase TestEmail(string cTo)
        {
            ResultBase oRet = new ResultBase();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cFrom = "";
            string cSmtp = "";
            int nPort = 0;
            bool lUseDefaultCredentials = true;
            bool lEnableSsl = false;
            string cEmailUser = "";
            string cEmailPassword = "";
            string cDeliveryMethod = "";

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    string cToCC = "";
                    string cToCCN = "";
                    string cSubject = "Messaggio di test";
                    string cMessage = "Messaggio di test";
                    bool lRet = Riepiloghi.clsEmail.SendEmailGetProperties(oConnectionInternal, out oError, out cFrom, out cSmtp, out nPort, out lUseDefaultCredentials, out lEnableSsl, out cEmailUser, out cEmailPassword, out cDeliveryMethod);
                    if (lRet && oError == null)
                    {
                        lRet = Riepiloghi.clsEmail.SendMail(cFrom, cSmtp, nPort, lUseDefaultCredentials, lEnableSsl, cEmailUser, cEmailPassword, cDeliveryMethod, cTo, cToCC, cToCCN, cSubject, cMessage, null, true, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("TestEmail", "Errore imprevisto TestEmail.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    string cMessage = oError.Message + "\r\n" +
                                                "[" + cFrom + "]\r\n" +
                                                "[" + cSmtp + "]\r\n" +
                                                "[" + nPort.ToString() + "]\r\n" +
                                                "[" + lUseDefaultCredentials.ToString() + "]\r\n" +
                                                "[" + lEnableSsl.ToString() + "]\r\n" +
                                                "[" + cEmailUser + "]\r\n" +
                                                "[" + cEmailPassword + "]\r\n" +
                                                "[" + cFrom + "]\r\n" +
                                                "";

                    oRet = new ResultBase(false, cMessage, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
                else
                {
                    oRet = new ResultBase(false, "OK", 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oRet;
        }


        public class ResultBaseAdminCodici : ResultBase
        {
            public Riepiloghi.clsAdminCodici Codici = null;

            public ResultBaseAdminCodici()
                : base()
            { }

            public ResultBaseAdminCodici(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAdminCodici(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsAdminCodici oCodici)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Codici = oCodici;
            }
        }

        [WebMethod]
        public ResultBaseAdminCodici GetAdminCodici(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAdminCodici oRet = new ResultBaseAdminCodici();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAdminCodici AdminCodici = new Riepiloghi.clsAdminCodici();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfile", Token );
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        AdminCodici = Riepiloghi.clsAdminCodici.GetAdminCodici(oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Amministrazione Codici Riepiloghi", "Errore imprevisto nella lettura Admministrazione Codici dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAdminCodici(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AdminCodici);
                }
                else
                {
                    oRet = new ResultBaseAdminCodici(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AdminCodici);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseAdminCodici SetAdminCodici(string Token, Riepiloghi.clsAdminCodici Codici)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAdminCodici oRet = new ResultBaseAdminCodici();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAdminCodici oAdminCodiciModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAdminCodici", Token, new Dictionary<string, object>() { { "Codici", Codici } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Codici != null)
                            {

                                oAdminCodiciModified = Riepiloghi.clsAdminCodici.SetAdminCodici(Codici, oConnectionInternal, out oError);
                            }
                            else
                            {
                                oError = new Exception("Lista Codici non valida.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Amministrazione Codici Riepiloghi", "Errore imprevisto nella modifica dell'Amministrazione Codici dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAdminCodici(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAdminCodiciModified);
                }
                else
                {
                    oRet = new ResultBaseAdminCodici(true, "Modifica Profili Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAdminCodiciModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        [WebMethod]
        public ResultBaseCodiceSistema SetNewAdminCodice(string Token, string ConnectionString, string DescrizioneCodiceSistema)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = new ResultBaseCodiceSistema();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsCodiceSistema oCodiceSistemaNew = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAdminCodici", Token, new Dictionary<string, object>() { { "Descrizione", DescrizioneCodiceSistema }, { "Connessione", ConnectionString } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                        if (ConnectionString == null | string.IsNullOrEmpty(ConnectionString) || ConnectionString.Trim() == "" ||
                            DescrizioneCodiceSistema == null | string.IsNullOrEmpty(DescrizioneCodiceSistema) || DescrizioneCodiceSistema.Trim() == "")
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione nuovo Codice Sistema", "Descrizione e Connessione obbligatori.");
                        }
                        else
                        {

                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                oCodiceSistemaNew = Riepiloghi.clsAdminCodici.SetNewCodiceSistema(ConnectionString, DescrizioneCodiceSistema, oConnectionInternal, out oError);
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione nuovo Codice Sistema", "Errore imprevisto nella creazione del nuovo Codice Sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "", "");
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "Nuovo Codice Sistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oCodiceSistemaNew.CodiceSistema, oCodiceSistemaNew.Descrizione);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseProfile : ResultBase
        {
            public Riepiloghi.clsProfile Profile = null;

            public ResultBaseProfile()
                : base()
            { }

            public ResultBaseProfile(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseProfile(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsProfile oProfile)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Profile = oProfile;
            }
        }

        [WebMethod]
        public ResultBaseProfile GetProfile(string Token, Int64 ProfileId)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile Profile = new Riepiloghi.clsProfile();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfile", Token, new Dictionary<string, object>() { { "ProfileId", ProfileId } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Profile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Profilo Riepiloghi", "Errore imprevisto nella lettura del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Profile);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Profile);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        [WebMethod]
        public ResultBaseProfile CreateProfile(string Token, string cDescrizione, Int64 ParentProfileId)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile oProfileModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreateProfile", Token, new Dictionary<string, object>() { { "cDescrizione", cDescrizione }, { "ParentProfileId", ParentProfileId } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);


                        if (ParentProfileId == 0)
                        {
                            ParentProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);
                            if (ParentProfileId == 0)
                            {
                                oError = new Exception("Profilo superiore non valido.");
                            }
                        }

                        if (oError == null)
                        {
                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                if (cDescrizione != null && cDescrizione.Trim() != "")
                                {
                                    oProfileModified = Riepiloghi.clsProfile.CreateProfile(cDescrizione, ParentProfileId, oConnectionInternal, out oError);
                                }
                                else
                                {
                                    oError = new Exception("Descrizione non valida.");
                                }
                            }
                            catch (Exception ex)
                            {
                                oError = new Exception("CreateProfile", ex);
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Profilo Riepiloghi", "Errore imprevisto nella creazione del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Creazione Profilo Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            //try
            //{
            //    System.IO.File.AppendAllText(@"E:\LOG-IIS\LOGRP.TXT", "\r\n" + "GetProfile ret" + "\r\n" + Newtonsoft.Json.JsonConvert.SerializeObject(oRet));
            //}
            //catch (Exception)
            //{
            //}

            return oRet;
        }

        [WebMethod]
        public ResultBaseProfile SetProfile(string Token, Riepiloghi.clsProfile Profile)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile oProfileModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetProfile", Token, new Dictionary<string, object>() { { "Profile", Profile } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Profile != null)
                            {
                                //System.Collections.SortedList oListaCampi = null;
                                //// carico i nomi dei campi del log dal codice sistema per eventuali filtri
                                //if (oToken.CodiceSistema != null)
                                //{
                                //    foreach (Riepiloghi.clsCodiceSistema oCodiceSistema in Profile.CodiciSistema)
                                //    {
                                //        if (oCodiceSistema.Enabled && oCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                //        {
                                //            if (oCodiceSistema.Filters != null && oCodiceSistema.Filters.Count > 0)
                                //            {

                                //                IConnection oConnectionCodiceSistema = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                                //                if (oError == null)
                                //                {
                                //                    oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnectionCodiceSistema, out oError);
                                //                    if (oListaCampi == null || oListaCampi.Count == 0)
                                //                    {
                                //                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi log transazioni", "Lista campi log transazioni non caricati.");
                                //                    }
                                //                }
                                //            }
                                //            break;
                                //        }
                                //    }
                                //}



                                oProfileModified = Riepiloghi.clsProfile.SetProfile(Profile, oConnectionInternal, out oError);
                            }
                            else
                            {
                                oError = new Exception("Profilo non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Profili Riepiloghi", "Errore imprevisto nella modifica del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Modifica Profili Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseProfileList : ResultBase
        {
            public List<Riepiloghi.clsProfile> ProfileList = null;

            public ResultBaseProfileList()
                : base()
            { }

            public ResultBaseProfileList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseProfileList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsProfile> oProfileList)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.ProfileList = oProfileList;
            }
        }

        [WebMethod]
        public ResultBaseProfileList GetProfileList(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfileList oRet = new ResultBaseProfileList();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsProfile> ProfileList = new List<Riepiloghi.clsProfile>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfileList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        ProfileList = Riepiloghi.clsProfile.GetProfileList(oToken.AccountId, oConnectionInternal, out oError);
                    }

                    //try
                    //{
                    //    foreach (Riepiloghi.clsProfile ITEM in ProfileList)
                    //    {
                    //        if (ITEM.ProfileId == 2)
                    //        {
                    //            ITEM.CodiciSistema[0].Enabled = true;
                    //            SetProfile(Token, ITEM);
                    //        }
                    //    }
                    //}
                    //catch (Exception)
                    //{

                    //    throw;
                    //}
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Profili Riepiloghi", "Errore imprevisto nella lettura dei Profili dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfileList(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), ProfileList);
                }
                else
                {
                    oRet = new ResultBaseProfileList(true, "Lista Accounts Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), ProfileList);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            return oRet;
        }

        public class ResultBaseAccount : ResultBase
        {
            public Riepiloghi.clsAccount Account = null;

            public ResultBaseAccount()
                :base()
            {}

            public ResultBaseAccount(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAccount(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsAccount oAccount)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Account = oAccount;
            }
        }

        [WebMethod]
        public ResultBaseAccount GetAccount(string Token, string AccountId)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount Account = new Riepiloghi.clsAccount();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetAccount", Token, new Dictionary<string, object>() { { "AccountId", AccountId } });

                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    if (AccountId == null || AccountId.Trim() == "")
                    {
                        AccountId = oToken.AccountId;
                    }

                    Account = Riepiloghi.clsAccount.GetAccount(AccountId, oConnectionInternal, out oError, true, true);
                    if (oToken.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema == "00000000")
                    {
                        List<Riepiloghi.clsCategoriaOperazioniRiepiloghi> tempCatOperazioni = new List<Riepiloghi.clsCategoriaOperazioniRiepiloghi>();
                        foreach (Riepiloghi.clsCategoriaOperazioniRiepiloghi oCategoria in Account.Operazioni)
                        {
                            if (oCategoria.IdCategoria == 1)
                            {
                                tempCatOperazioni.Add(oCategoria);
                                //break;
                            }
                            else if (oCategoria.IdCategoria == 5)
                            {
                                Riepiloghi.clsCategoriaOperazioniRiepiloghi oCategoriaConfig = new Riepiloghi.clsCategoriaOperazioniRiepiloghi(oCategoria.IdCategoria, oCategoria.Descrizione, 99);
                                foreach (Riepiloghi.clsOperazioneRiepiloghi oOperazione in oCategoria.ListaOperazioni)
                                {
                                    if (oOperazione.IdOperazione != "CONFIG_RIEPILOGHI")
                                    {
                                        oCategoriaConfig.ListaOperazioni.Add(oOperazione);
                                    }
                                }
                                tempCatOperazioni.Add(oCategoriaConfig);
                                //break;
                            }
                        }
                        Account.Operazioni = tempCatOperazioni;
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Errore imprevisto nella lettura dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Account);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Account);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return oRet;
        }

        [WebMethod]
        public ResultBaseAccount CreateAccount(string Token, string url, string Email, string Nome, string Cognome, Int64 ProfileId)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccount = new Riepiloghi.clsAccount();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreateAccount", Token, new Dictionary<string, object>() { { "Email", Email }, { "Nome", Nome }, { "Cognome", Cognome }, { "Url", url } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Email != null && Email.Trim() != "")
                            {
                                if (Nome != null && Nome.Trim() != "")
                                {
                                    if (Cognome != null && Cognome.Trim() != "")
                                    {
                                        oAccount = Riepiloghi.clsAccount.CreateAccount(oToken.AccountId, url, Email, Nome, Cognome, ProfileId, oConnectionInternal, out oError);
                                    }
                                    else
                                    {
                                        oError = new Exception("Cognome Account non valido.");
                                    }
                                }
                                else
                                {
                                    oError = new Exception("Nome Account non valido.");
                                }
                            }
                            else
                            {
                                oError = new Exception("Email Account non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {

                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Creazione Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        [WebMethod]
        public ResultBaseAccount SetAccount(string Token, Riepiloghi.clsAccount Account)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccountModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAccount", Token, new Dictionary<string, object>() { { "Account", Account } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Account != null)
                            {
                                oAccountModified = Riepiloghi.clsAccount.SetAccount(oToken.AccountId, oConnectionInternal, out oError, Account);
                            }
                            else
                            {
                                oError = new Exception("Account non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Modifica Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseAccount RenewAccountPassword(string Email, string url)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccount = new Riepiloghi.clsAccount();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);

                if (oError == null && (Email == null || Email.Trim() == ""))
                {
                    oError = new Exception("Email non valida");
                }

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "RenewAccountPassword", "", new Dictionary<string, object>() { { "Email", Email }, { "Url", url } });

                    string AccountId = Riepiloghi.clsAccount.GetAccountByEmail(Email, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        if (AccountId.Trim() != "")
                        {
                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                // Spedizione email per modifica password
                                bool lRet = Riepiloghi.clsEmail.SendMailAccountCreateRenew(AccountId, "RIEPILOGHI_WEB", url, oConnectionInternal, out oError);
                                if (!lRet)
                                {
                                    oError = new Exception("Errore nella spedizione della email.");
                                }
                                else
                                {
                                    oAccount = Riepiloghi.clsAccount.GetAccount(AccountId, oConnectionInternal, out oError, true, false);
                                }
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }

                        }
                        else
                        {
                            oError = new Exception("Email non valida");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Reimposta password Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Reimposta password Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseAccount SetAccountPassword(string Token, string Email, string Password)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccountModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAccountPassword", Token, new Dictionary<string, object>() { { "Email", Email }, { "Password", Password } });

                    oConnectionInternal.BeginTransaction();
                    try
                    {
                        oAccountModified = Riepiloghi.clsAccount.SetAccountPassword(Token, oConnectionInternal, out oError, Email, "RIEPILOGHI_WEB", Password);
                    }
                    catch (Exception ex)
                    {
                        oError = ex;
                    }
                    finally
                    {
                        if (oError != null)
                        {
                            oConnectionInternal.RollBack();
                        }
                        else
                        {
                            oConnectionInternal.EndTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Password Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Modifica Password Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        public class ResultBaseAccountList : ResultBase
        {
            public List<Riepiloghi.clsAccount> AccountList = null;

            public ResultBaseAccountList()
                : base()
            { }

            public ResultBaseAccountList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAccountList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsAccount> oAccountList)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.AccountList = oAccountList;
            }
        }

        [WebMethod]
        public ResultBaseAccountList GetAccountList(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccountList oRet = new ResultBaseAccountList();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsAccount> AccountList = new List<Riepiloghi.clsAccount>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetAccountList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        AccountList = Riepiloghi.clsAccount.GetAccountsList(oToken.AccountId, oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Accounts Riepiloghi", "Errore imprevisto nella lettura degli Accounts dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccountList(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AccountList);
                }
                else
                {
                    oRet = new ResultBaseAccountList(true, "Lista Accounts Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AccountList);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

           
            return oRet;
        }

        #endregion

        #region "funzioni di Consultazione"

        #region "Configurazione"

        public class ResultBaseProprietaRiepiloghi : ResultBase
        {
            public string Proprieta = "";
            public string Valore = "";

            public ResultBaseProprietaRiepiloghi()
            {
            }

            public ResultBaseProprietaRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Proprieta)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Proprieta = Proprieta;
                this.Valore = "";
            }
        }

        public class ResultBaseListProprietaRiepiloghi : ResultBase
        {
            public List<Riepiloghi.clsProprietaRiepiloghi> ListaProprieta = new List<Riepiloghi.clsProprietaRiepiloghi>();

            public ResultBaseListProprietaRiepiloghi()
            {

            }

            public ResultBaseListProprietaRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                :base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {

            }
        }

        [WebMethod]
        public ResultBaseProprietaRiepiloghi GetProprietaRiepiloghi(string Token, string Proprieta, string DefaultValue)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProprietaRiepiloghi oRet = new ResultBaseProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cValore = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "Proprieta", Proprieta }, { "DefaultValue", DefaultValue} });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            cValore = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, Proprieta, DefaultValue);
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Proprieta riepiloghi" + Proprieta, "Errore imprevisto nella lettura della proprietà.", ex);
            }
            finally
            {

                if (oError != null)
                {
                    oRet = new ResultBaseProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta);
                }
                else
                {
                    oRet = new ResultBaseProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta); ;
                    oRet.Valore = cValore;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        [WebMethod]
        public ResultBaseProprietaRiepiloghi SetProprietaRiepiloghi(string Token, string Proprieta, string Valore)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProprietaRiepiloghi oRet = new ResultBaseProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "Proprieta", Proprieta }, { "Valore", Valore } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("SELECT PROPRIETA, VALORE, DESCR, REGULAR_EXPRESSION FROM PROPRIETA_RIEPILOGHI WHERE PROPRIETA = :pPROPRIETA");
                            IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), new clsParameters(":pPROPRIETA", Proprieta));
                            if (!oRS.EOF)
                            {
                                Riepiloghi.clsProprietaRiepiloghi oProprieta = new Riepiloghi.clsProprietaRiepiloghi();
                                oProprieta.Proprieta = (oRS.Fields("PROPRIETA").IsNull ? "" : oRS.Fields("PROPRIETA").Value.ToString());
                                if (oProprieta.Proprieta.Trim() != "")
                                {
                                    oProprieta.Descrizione = (oRS.Fields("DESCR").IsNull ? "" : oRS.Fields("DESCR").Value.ToString());
                                    oProprieta.RegularExpression = (oRS.Fields("REGULAR_EXPRESSION").IsNull ? "" : oRS.Fields("REGULAR_EXPRESSION").Value.ToString());
                                }
                                bool lSave = false;

                                if (oProprieta.RegularExpression.Trim() != "")
                                {
                                    try
                                    {
                                        System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex(oProprieta.RegularExpression);
                                        lSave = oRegex.IsMatch(Valore);
                                    }
                                    catch (Exception ex)
                                    {
                                        lSave = false;
                                    }
                                    if (!lSave)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Proprietà riepiloghi " + Proprieta, "Valore non valido.");
                                    }
                                }
                                else
                                {
                                    lSave = true;
                                }

                                if (lSave)
                                {
                                    oProprieta.Valore = Valore;
                                    clsParameters oPars = new clsParameters();
                                    oPars.Add(":pPROPRIETA", oProprieta.Proprieta);

                                    oSB = new StringBuilder();
                                    if (oProprieta.Valore.Trim() == "")
                                    {
                                        oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = NULL WHERE PROPRIETA = :pPROPRIETA");
                                        oConnection.ExecuteNonQuery(oSB.ToString(), oPars, true);
                                    }
                                    else
                                    {
                                        oPars.Add(":pVALORE", oProprieta.Valore);
                                        oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = :pVALORE WHERE PROPRIETA = :pPROPRIETA");
                                        oConnection.ExecuteNonQuery(oSB.ToString(), oPars, true);
                                    }
                                }
                            }
                            oRS.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Proprieta riepiloghi " + Proprieta, "Errore imprevisto nella modifica della proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta);
                }
                else
                {
                    oRet = new ResultBaseProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta); ;
                    oRet.Valore = Valore;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;

        }

        [WebMethod]
        public ResultBaseListProprietaRiepiloghi GetListaProprietaRiepiloghi(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseListProprietaRiepiloghi oRet = new ResultBaseListProprietaRiepiloghi();
            List<Riepiloghi.clsProprietaRiepiloghi> oLista = new List<Riepiloghi.clsProprietaRiepiloghi>();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetListaProprietaRiepiloghi", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oLista = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);

                            Riepiloghi.clsProprietaRiepiloghi oProprietaIpServerFiscale = new Riepiloghi.clsProprietaRiepiloghi();
                            oProprietaIpServerFiscale.Proprieta = "IP_SERVER_FISCALE";
                            oProprietaIpServerFiscale.Descrizione = "IP Server Fiscale";
                            oProprietaIpServerFiscale.Valore = "";
                            oProprietaIpServerFiscale.RegularExpression = "";
                            oProprietaIpServerFiscale.WebRegularExpression = "";
                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in oLista)
                            {
                                if (oItemProprieta.Proprieta == "WINSERVICE_SOCKET_LISTENER_IP")
                                {
                                    oProprietaIpServerFiscale.RegularExpression = oItemProprieta.RegularExpression;
                                    oProprietaIpServerFiscale.WebRegularExpression = oItemProprieta.WebRegularExpression;
                                    break;
                                }
                            }
                            IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT IP FROM SMART_SOCKET");
                            if (!oRS.EOF && !oRS.Fields("IP").IsNull)
                            {
                                oProprietaIpServerFiscale.Valore = oRS.Fields("IP").Value.ToString();
                            }
                            oRS.Close();
                            if (oProprietaIpServerFiscale.Valore.Trim() != "")
                            {
                                oLista.Add(oProprietaIpServerFiscale);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Proprieta riepiloghi", "Errore imprevisto nella lettura della proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
                else
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                    oRet.ListaProprieta = oLista;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseListProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseListProprietaRiepiloghi SetListaProprietaRiepiloghi(string Token, ResultBaseListProprietaRiepiloghi ResultLista)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseListProprietaRiepiloghi oRet = new ResultBaseListProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsProprietaRiepiloghi> oLista = new List<Riepiloghi.clsProprietaRiepiloghi>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetListaProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "ResultLista", ResultLista } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            List<Riepiloghi.clsProprietaRiepiloghi> oListaCorrente = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);
                            bool lSave = true;

                            List<Riepiloghi.clsProprietaRiepiloghi> listaToSave = new List<Riepiloghi.clsProprietaRiepiloghi>();
                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in ResultLista.ListaProprieta)
                            {
                                if (oItemProprieta.Proprieta != "IP_SERVER_FISCALE")
                                {
                                    listaToSave.Add(oItemProprieta);
                                }
                            }

                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                            {
                                bool lFind = false;
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprietaCorrente in oListaCorrente)
                                {
                                    if (oItemProprieta.Proprieta == oItemProprietaCorrente.Proprieta)
                                    {
                                        lFind = true;
                                        break;
                                    }
                                }
                                if (!lFind)
                                {
                                    lSave = false;
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Proprietà non valida " + oItemProprieta.Proprieta);
                                    break;
                                }
                            }

                            if (lSave)
                            {
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                                {
                                    bool lMatch = false;
                                    foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprietaCorrente in oListaCorrente)
                                    {
                                        if (oItemProprieta.Proprieta == oItemProprietaCorrente.Proprieta)
                                        {
                                            if (oItemProprietaCorrente.RegularExpression.Trim() != "")
                                            {
                                                try
                                                {
                                                    System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex(oItemProprietaCorrente.RegularExpression);
                                                    lMatch = oRegex.IsMatch(oItemProprieta.Valore);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lMatch = false;
                                                }
                                            }
                                            else
                                            {
                                                lMatch = true;
                                            }
                                            break;
                                        }
                                    }
                                    if (!lMatch)
                                    {
                                        lSave = false;
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Valore di Proprietà non valida " + oItemProprieta.Proprieta + " " + oItemProprieta.Valore);
                                        break;
                                    }
                                }
                            }

                            if (lSave)
                            {
                                oConnection.BeginTransaction();
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                                {
                                    //if (oItemProprieta.Proprieta != "IP_SERVER_FISCALE")
                                    //{
                                        
                                    //}

                                    try
                                    {
                                        clsParameters oPars = new clsParameters();
                                        oPars.Add(":pPROPRIETA", oItemProprieta.Proprieta);

                                        StringBuilder oSB = new StringBuilder();
                                        if (oItemProprieta.Valore.Trim() == "")
                                        {
                                            oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = NULL WHERE PROPRIETA = :pPROPRIETA");
                                            oConnection.ExecuteNonQuery(oSB.ToString(), oPars, false);
                                        }
                                        else
                                        {
                                            oPars.Add(":pVALORE", oItemProprieta.Valore);
                                            oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = :pVALORE WHERE PROPRIETA = :pPROPRIETA");
                                            oConnection.ExecuteNonQuery(oSB.ToString(), oPars, false);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Errore nella modifica della " + oItemProprieta.Proprieta + " " + oItemProprieta.Valore, ex);
                                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore SALVATAGGIO", ex);
                                        break;
                                    }
                                }
                                if (oError == null)
                                {
                                    oConnection.EndTransaction();
                                }
                                else
                                {
                                    oConnection.RollBack();
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oLista = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Errore imprevisto nella modifica della lista delle proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
                else
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                    oRet.ListaProprieta = oLista;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseListProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        [WebMethod]
        public ResultBaseProprietaRiepiloghi GetPathRPG(string Token)
        {
            return GetProprietaRiepiloghi(Token, "PATH_RPG", "c:\\riepiloghi\\giornalieri");
        }

        [WebMethod]
        public ResultBaseProprietaRiepiloghi GetPathRPM(string Token)
        {
            return GetProprietaRiepiloghi(Token, "PATH_RPM", "c:\\riepiloghi\\mensili");
        }

        [WebMethod]
        public ResultBaseProprietaRiepiloghi GetPathEML(string Token)
        {
            return GetProprietaRiepiloghi(Token, "PATH_EMAIL", "c:\\riepiloghi\\mailsmime");
        }


        public class ResultBaseFilesRiepiloghi : ResultBase
        {
            public DateTime Giorno = DateTime.MinValue;
            public Int64 Progressivo = 0;
            public string Tipo = "";
            public string NomeFile = "";

            public ResultBaseFilesRiepiloghi()
            {
            }

            public ResultBaseFilesRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime Giorno, string Tipo)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Giorno = Giorno;
                this.Tipo = Tipo;
            }
            
            public ResultBaseFilesRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime Giorno, string Tipo, string NomeFile)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Giorno = Giorno;
                this.Tipo = Tipo;
                this.NomeFile = NomeFile;
            }

            public static ResultBaseFilesRiepiloghi Create(string cGiorno, string Tipo, out Exception oError)
            {
                ResultBaseFilesRiepiloghi oRet = new ResultBaseFilesRiepiloghi();
                oError = null;
                oRet.Tipo = Tipo;
                if (!DateTime.TryParseExact(cGiorno, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out oRet.Giorno))
                {
                    oError = new Exception("Formato data non valido.");
                    oRet.StatusMessage = oError.Message;
                    oRet.StatusCode = 1;
                }
                return oRet;
            }
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeLogGiornoBase(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00");
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;    
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeLogGiorno(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00") + ".txt";
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeLogSearchIncrementale(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00") + "_*.txt";
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeEmailGiornoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "EML", out oError);
            if (oError == null)
            {
                oRet.NomeFile = libRiepiloghiBase.clsLibSigillo.GetNomeEmailGiornoBase(oRet.Giorno, GiornalieroMensile, nProgressivo);
                oRet.Progressivo = nProgressivo;
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeEmailGiorno(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "EML", out oError);
            if (oError == null)
            {
                ResultBaseProprietaRiepiloghi oPath = GetPathEML(Token);
                if (oPath.Valore.Trim() != "")
                {
                    string cPath = oPath.Valore;
                    ResultBaseFilesRiepiloghi oBase = GetNomeEmailGiornoBase(cGiorno, GiornalieroMensile, nProgressivo);
                    if (oBase.NomeFile.Trim() != "")
                    {
                        oRet.NomeFile = cPath + "\\" + oBase.NomeFile;
                        oRet.Progressivo = nProgressivo;
                    }
                    else
                    {
                        oRet.StatusCode = oBase.StatusCode;
                        oRet.StatusMessage = oBase.StatusMessage;
                    }
                }
                else
                {
                    oRet.StatusCode = oPath.StatusCode;
                    oRet.StatusMessage = oPath.StatusMessage;
                }

            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeRiepilogoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, (GiornalieroMensile == "G" ? "RPG" : "RPM"), out oError);
            if (oError == null)
            {
                oRet.NomeFile = libRiepiloghiBase.clsLibSigillo.GetNomeRiepilogoBase(oRet.Giorno, GiornalieroMensile, nProgressivo);
                oRet.Progressivo = nProgressivo;
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }
        
        [WebMethod]
        public ResultBaseFilesRiepiloghi GetNomeRiepilogo(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, (GiornalieroMensile == "G" ? "RPG" : "RPM"), out oError);
            if (oError == null)
            {
                ResultBaseProprietaRiepiloghi oPath = (GiornalieroMensile == "G" ? GetPathRPG(Token) : GetPathRPM(Token));
                if (oPath.Valore.Trim() != "")
                {
                    string cPath = (GiornalieroMensile == "G" ? GetPathRPG(Token).Valore : GetPathRPM(Token).Valore);
                    ResultBaseFilesRiepiloghi oBase = GetNomeRiepilogoBase(cGiorno, GiornalieroMensile, nProgressivo);
                    if (oBase.NomeFile.Trim() != "")
                    {
                        oRet.NomeFile = cPath + "\\" + oBase.NomeFile;
                        oRet.Progressivo = nProgressivo;
                    }
                    else
                    {
                        oRet.StatusCode = oBase.StatusCode;
                        oRet.StatusMessage = oBase.StatusMessage;
                    }
                }
                else
                {
                    oRet.StatusCode = oPath.StatusCode;
                    oRet.StatusMessage = oPath.StatusMessage;
                }
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        #endregion

        #region "Login"
        public class ResultBaseToken : ResultBase
        {
            public string Token = "";

            public ResultBaseToken()
            {
            }

            public ResultBaseToken(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cToken) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Token = cToken;
            }
        }

        [WebMethod]
        public ResultBaseToken GetToken(string Login, string Password)
        {
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cToken = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    cToken = Riepiloghi.clsToken.GetToken(oConnectionInternal, out oError, Login, Password);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
            }
            else
            {
                oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken); ;
            }
            return oRet;
        }

        [WebMethod]
        public ResultBaseToken GetTokenUser(string Login, string Password, string User)
        {
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cToken = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    cToken = Riepiloghi.clsToken.GetTokenUser(oConnectionInternal, out oError, Login, Password, User);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
            }
            else
            {
                oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken); ;
            }

            string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return oRet;
        }

        [WebMethod]
        public ResultBaseToken GetTokenCodiceSistema(string Token, string CodiceSistema)
        {
            string cToken = "";
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    cToken = Riepiloghi.clsToken.GetTokenCodiceSistema(oConnectionInternal, out oError, Token, CodiceSistema);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
            }
            else
            {
                oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken); ;
            }

            string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return oRet;
        }

        public class ResultBaseAccountCodiciSistemaAbilitati : ResultBase
        {
            public Riepiloghi.clsCodiceSistema[] ListaCodiciSistema = new Riepiloghi.clsCodiceSistema[] { };

            public ResultBaseAccountCodiciSistemaAbilitati()
            {
            }

            public ResultBaseAccountCodiciSistemaAbilitati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsCodiceSistema[] oCodiciSistema) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.ListaCodiciSistema = oCodiciSistema;
            }
        }

        [WebMethod]
        public ResultBaseAccountCodiciSistemaAbilitati GetCodiciSistemaAccount(string Token)
        {
            ResultBaseAccountCodiciSistemaAbilitati oRet = null;
            List<Riepiloghi.clsCodiceSistema> oLista = new List<Riepiloghi.clsCodiceSistema>();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, true);
                    oLista = oAccount.CodiciSistema;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Codici Sistema", "Errore imprevisto nella richiesta della lista dei codici sistema.", ex);
            }
            finally
            {
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseAccountCodiciSistemaAbilitati(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oLista.ToArray());
            }
            else
            {
                oRet = new ResultBaseAccountCodiciSistemaAbilitati(true, "Lista Codici Sistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oLista.ToArray()); ;
            }
            return oRet;
        }


        #endregion

        #region "Codice sistema"

        public class ResultBaseParametriServerFiscale : ResultBase
        {
            public Int64 IdSocket = 0;
            public string ServerIP = "";
            public int Port = 0;
            public string Client_ID = "";
            public string Password = "";

            public ResultBaseParametriServerFiscale()
            {
            }

            public ResultBaseParametriServerFiscale(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }

            public ResultBaseParametriServerFiscale(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.IdSocket = oParametriServerFiscale.IdSocket;
                this.ServerIP = oParametriServerFiscale.ServerIP;
                this.Port = oParametriServerFiscale.Port;
                this.Client_ID = oParametriServerFiscale.Client_ID;
                this.Password = oParametriServerFiscale.Password;
            }
        }

        [WebMethod]
        public ResultBaseParametriServerFiscale GetParametriServerFiscale(string Token)
        {
            ResultBaseParametriServerFiscale oRet = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oParametriServerFiscale = Riepiloghi.clsRiepiloghi.GetParametriServerFiscale(oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseParametriServerFiscale(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                oRet = new ResultBaseParametriServerFiscale(true, "Parametri", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oParametriServerFiscale); ;
            }
            return oRet;
        }

        // Classe di risposta con anche codice sistema
        public class ResultBaseCodiceSistema : ResultBase
        {
            public string CodiceSistema = "";
            public string Descrizione = "";
            public ResultBaseCodiceSistema()
            {
            }

            public ResultBaseCodiceSistema(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceSistema, string cDescrizione) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.CodiceSistema = cCodiceSistema;
                this.Descrizione = cDescrizione;
            }
        }

        [WebMethod]
        public ResultBaseCodiceSistema CheckInfoCodiceSistema(string Token, string CodiceSistema)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cCodiceSistema = "";
            string cDescrizione = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CheckInfoCodiceSistema", Token, new Dictionary<string, object>() { { "CodiceSistema", CodiceSistema } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        
                        Riepiloghi.clsRiepiloghi.CheckUpdateCodiciLocaliPerCodiceSistema(oConnectionInternal, false, CodiceSistema, out oError);
                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            if (oError == null)
                            {
                                cCodiceSistema = Riepiloghi.clsRiepiloghi.GetCodiceSistema(oConnection, out oError);
                                if (oError == null)
                                {

                                }
                                Riepiloghi.clsCodiceSistema oCodiceSistema = Riepiloghi.clsCodiceSistema.GetCodiceSistemaBase(cCodiceSistema, oConnectionInternal, out oError);
                                if (oError == null)
                                {
                                    cDescrizione = oCodiceSistema.Descrizione;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "", "");
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "CodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cCodiceSistema, cDescrizione);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseCodiceSistema", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        // Richiesta codice sistema
        [WebMethod]
        public ResultBaseCodiceSistema GetCodiceSistema(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cCodiceSistema = "";
            string cDescrizione = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetCodiceSistema", Token);
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        if (oToken != null && oToken.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema == "00000000")
                        {
                            cCodiceSistema = oToken.CodiceSistema.CodiceSistema;
                            if (Riepiloghi.clsAccount.CheckAccountCodiceSistema(oToken.AccountId, oToken.CodiceSistema.CodiceSistema, oConnectionInternal, out oError) && oError == null)
                            {
                                cDescrizione = "solo configurazione";
                            }
                        }
                        else
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            if (oError == null)
                            {
                                cCodiceSistema = Riepiloghi.clsRiepiloghi.GetCodiceSistema(oConnection, out oError);
                                Riepiloghi.clsCodiceSistema oCodiceSistema = Riepiloghi.clsCodiceSistema.GetCodiceSistemaBase(cCodiceSistema, oConnectionInternal, out oError);
                                if (oError == null)
                                {
                                    cDescrizione = oCodiceSistema.Descrizione;
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "", "");
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "CodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cCodiceSistema, cDescrizione);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseCodiceSistema", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        // SMART_CARD
        public class ResultBaseSmartCard : ResultBase
        {
            public string CODICE_SISTEMA = "";
            public string NOME_FIRMATARIO = "";
            public string COGNOME_FIRMATARIO = "";
            public string CF_FIRMATARIO = "";
            public string LOCAZIONE = "";
            public string EMAIL_TITOLARE = "";
            public string EMAIL_SIAE = "";
            public string TITOLARE = "";
            public string CF_TITOLARE = "";
            public string RI_TITOLARE = "";
            public string NAZIONE = "";
            public string NR_PROTO = "";
            public DateTime DATA_APPROVAZIONE = DateTime.MinValue;
            public string RAPPR_LEGALE = "";
            public string VERSION = "";
            public string SN_CARD = "";
            public int ENABLED = 0;

            public ResultBaseSmartCard()
            {
            }

            public ResultBaseSmartCard(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }
        }

        [WebMethod]
        public ResultBaseSmartCard GetInfoSmartCard(string Token, string SerialNumber)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            ResultBaseSmartCard oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetInfoSmartCard", Token, new Dictionary<string, object>() { { "SerialNumber", SerialNumber } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            clsParameters oPars = new clsParameters();
                            oPars.Add(":pCODICE_SISTEMA", oToken.CodiceSistema.CodiceSistema);
                            oPars.Add(":pSN_CARD", SerialNumber);
                            oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT CODICE_SISTEMA,NOME_FIRMATARIO,COGNOME_FIRMATARIO,CF_FIRMATARIO,LOCAZIONE,EMAIL_TITOLARE,EMAIL_SIAE,TITOLARE,CF_TITOLARE,RI_TITOLARE,NAZIONE,NR_PROTO,DATA_APPROVAZIONE,RAPPR_LEGALE,VERSION,SN_CARD,ENABLED FROM SMART_CARD WHERE CODICE_SISTEMA = :pCODICE_SISTEMA AND SN_CARD = :pSN_CARD", oPars);
                            if (!oRS.EOF)
                            {
                                oResult = new ResultBaseSmartCard(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.CODICE_SISTEMA = oRS.Fields("CODICE_SISTEMA").Value.ToString();
                                oResult.NOME_FIRMATARIO = oRS.Fields("NOME_FIRMATARIO").Value.ToString();
                                oResult.COGNOME_FIRMATARIO = oRS.Fields("COGNOME_FIRMATARIO").Value.ToString();
                                oResult.CF_FIRMATARIO = oRS.Fields("CF_FIRMATARIO").Value.ToString();
                                oResult.LOCAZIONE = oRS.Fields("LOCAZIONE").Value.ToString();
                                oResult.EMAIL_TITOLARE = oRS.Fields("EMAIL_TITOLARE").Value.ToString();
                                oResult.EMAIL_SIAE = oRS.Fields("EMAIL_SIAE").Value.ToString();
                                oResult.TITOLARE = oRS.Fields("TITOLARE").Value.ToString();
                                oResult.CF_TITOLARE = oRS.Fields("CF_TITOLARE").Value.ToString();
                                oResult.RI_TITOLARE = oRS.Fields("RI_TITOLARE").Value.ToString();
                                oResult.NAZIONE = oRS.Fields("NAZIONE").Value.ToString();
                                oResult.NR_PROTO = oRS.Fields("NR_PROTO").Value.ToString();
                                oResult.DATA_APPROVAZIONE = (DateTime)oRS.Fields("DATA_APPROVAZIONE").Value;
                                oResult.RAPPR_LEGALE = oRS.Fields("RAPPR_LEGALE").Value.ToString();
                                oResult.VERSION = oRS.Fields("VERSION").Value.ToString();
                                oResult.SN_CARD = oRS.Fields("SN_CARD").Value.ToString();
                                oResult.ENABLED = int.Parse(oRS.Fields("ENABLED").Value.ToString());
                            }
                            else
                            {
                                //lock = FieldAccessException;
                                oError = new Exception("Carta non valida");
                            }
                            oRS.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oResult = new ResultBaseSmartCard(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseSmartCard", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            
            return oResult;
        }

        #endregion

        #region "Controllo server fiscale"

        // Controllo serve fiscale acceso
        [WebMethod]
        public ResultBase CheckServerFiscale(string Token)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            bool lRet = false;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            lRet = Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione server fiscale", "Errore imprevisto nella connessione al server fiscale.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                return new ResultBase(lRet, lRet.ToString(), 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
        }

        #endregion

        #region "GetTransazioni, GetTransazioniAnnullati, GetTransazioneCARTA_PROGRESSIVO, GetTransazioneCARTA_SIGILLO"

        public class ResultPreFiltro : ResultBase
        {
            public string Campo = "";
            //public List<Riepiloghi.clsValoreFiltroCampoTransazione> Valori = new List<Riepiloghi.clsValoreFiltroCampoTransazione>();
            //public List<Riepiloghi.clsFiltroCampoTransazione> ListaCampi = new List<Riepiloghi.clsFiltroCampoTransazione>();

            public Riepiloghi.clsValoreFiltroCampoTransazione[] Valori = new Riepiloghi.clsValoreFiltroCampoTransazione[] { };
            public Riepiloghi.clsFiltroCampoTransazione[] ListaCampi = new Riepiloghi.clsFiltroCampoTransazione[] { };

            public ResultPreFiltro()
            {
            }

            //public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string NomeCampo, List<Riepiloghi.clsValoreFiltroCampoTransazione> ListaValori)
            //    : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            //{
            //    this.Campo = NomeCampo;
            //    this.Valori = ListaValori.ToArray();
            //}

            //public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsFiltroCampoTransazione> ListaCampi)
            //    : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            //{
            //    this.ListaCampi = ListaCampi.ToArray();
            //}

            public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string NomeCampo, Riepiloghi.clsValoreFiltroCampoTransazione[] ListaValori)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Campo = NomeCampo;
                this.Valori = ListaValori;
            }

            public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsFiltroCampoTransazione[] ListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.ListaCampi = ListaCampi;
            }
        }

        [WebMethod]
        public ResultPreFiltro GetCampiPreFiltro(string Token)
        {
            DateTime dStart = DateTime.Now;
            ResultPreFiltro oResult = new ResultPreFiltro();
            Riepiloghi.clsFiltroCampoTransazione[] oListaCampi = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oListaCampi = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostati(oConnection, out oError);
                            if (oListaCampi == null)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi di filtro", "Lista campi di filtro non caricati.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi di filtro", "Errore imprevisto durante la richiesta della lista dei campi di filtro.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultPreFiltro(true, "Lista campi di filtro", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
            }
            else
            {
                oResult = new ResultPreFiltro(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null, null);
            }
            return oResult;
        }

        [WebMethod]
        public ResultPreFiltro GetValoriPreFiltro(string Token, string NomeCampo)
        {
            DateTime dStart = DateTime.Now;
            ResultPreFiltro oResult = new ResultPreFiltro();
            Riepiloghi.clsFiltroCampoTransazione oFiltro = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oFiltro = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostatiCampo(oConnection, out oError, NomeCampo, true);
                            if (oFiltro == null)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista valori di filtro", "Lista valori di filtro non caricati.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista valori di filtro", "Errore imprevisto durante la richiesta della lista dei valori di filtro.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultPreFiltro(true, "Lista valori di filtro", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oFiltro.Campo, oFiltro.ValoriPredefiniti);
            }
            else
            {
                oResult = new ResultPreFiltro(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null, null);
            }
            return oResult;
        }

        // Classe per filtri transazioni
        [XmlRoot("dictionary")]
        public class clsDizionarioFiltri<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }
        }

        // Classe parametro per estrarre transazioni filtrate
        public class clsFiltroTransazioni
        {
            public clsDizionarioFiltri<string, object> Filtri;
            
            public clsFiltroTransazioni()
            {
            }
            
            public clsFiltroTransazioni(IConnection Connection, string cFiltro, out Exception oError)
            {
                oError = null;
                this.Filtri = DecodeFiltri(cFiltro, Connection, out oError);
            }

            public clsFiltroTransazioni(clsDizionarioFiltri<string, object> IFiltri)
            {
                this.Filtri = IFiltri;
            }

            // Compone una lista di filtri per la lista delle transazioni
            public static clsDizionarioFiltri<string, object> DecodeFiltri(string cFiltri, IConnection oConnection, out Exception oError)
            {
                clsDizionarioFiltri<string, object> oRet = null;
                oError = null;
                if (cFiltri.Trim() != "")
                {
                    System.Collections.SortedList oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError);
                    if (oError == null && oListaCampi != null && oListaCampi.Count > 0)
                    {
                        oRet = new clsDizionarioFiltri<string, object>();
                        if (!cFiltri.Contains(";"))
                        {
                            cFiltri += ";";
                        }
                        foreach (string cValoreFiltro in cFiltri.Split(';'))
                        {
                            if (cValoreFiltro.Contains("="))
                            {
                                string cCampo = cValoreFiltro.Split('=')[0];
                                string cValue = cValoreFiltro.Split('=')[1];
                                Riepiloghi.clsCampoTransazione oCampo = null;
                                foreach (System.Collections.DictionaryEntry ItemCampoDic in oListaCampi)
                                {
                                    Riepiloghi.clsCampoTransazione oItemCampo = (Riepiloghi.clsCampoTransazione)ItemCampoDic.Value;
                                    if (oItemCampo.Campo == cCampo)
                                    {
                                        oCampo = oItemCampo;
                                        break;
                                    }
                                }

                                if (oCampo == null)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "campo filtro [" + cCampo + "] non valido.");
                                }
                                else
                                {
                                    if (oCampo.Tipo == "S")
                                    {
                                        if (cValue.Length > oCampo.Dimensione)
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore TESTO per il campo " + cCampo + " troppo lungo " + cValue);
                                        }
                                        else
                                        {
                                            oRet.Add(oCampo.Campo, cValue.PadRight(oCampo.Dimensione));
                                        }

                                    }
                                    else if (oCampo.Tipo == "N")
                                    {
                                        Int64 nValue = 0;
                                        if (Int64.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore NUMERICO per il campo " + cCampo + " non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "C")
                                    {
                                        decimal nValue = 0;
                                        if (decimal.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore IMPORTO per il campo " + cCampo + " [0,00] non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "D")
                                    {
                                        DateTime dValue;
                                        if (cValue.Length == 8)
                                        {
                                            try
                                            {
                                                dValue = new DateTime(int.Parse(cValue.Substring(0, 4)), int.Parse(cValue.Substring(4, 2)), int.Parse(cValue.Substring(6, 2)));
                                                oRet.Add(oCampo.Campo, dValue);
                                            }
                                            catch
                                            {
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore DATA per il campo " + cCampo + " [YYYYMMDD] non valido " + cValue);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (oError == null)
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Errore durante la lettura dei campi di filtro.");
                        }
                    }
                }
                return oRet;
            }
        }

        // Classe con i campi del log delle transazioni
        public class clsResultTransazione
        {
            public string CodiceFiscaleOrganizzatore { get; set; }
            public string CodiceFiscaleTitolare { get; set; }
            public string TitoloAbbonamento { get; set; }
            public string TitoloIvaPreassolta { get; set; }
            public string SpettacoloIntrattenimento { get; set; }
            public string Valuta { get; set; }
            public decimal? ImponibileIntrattenimenti { get; set; }
            public string OrdineDiPosto { get; set; }
            public string Posto { get; set; }
            public string TipoTitolo { get; set; }
            public string Annullato { get; set; }
            public decimal? ProgressivoAnnullati { get; set; }
            public DateTime? DataEmissioneAnnullamento { get; set; }
            public string OraEmissioneAnnullamento { get; set; }
            public Int64? ProgressivoTitolo { get; set; }
            public string CodicePuntoVendita { get; set; }
            public string Sigillo { get; set; }
            public string CodiceSistema { get; set; }
            public string CodiceCarta { get; set; }
            public string Prestampa { get; set; }
            public string CodiceLocale { get; set; }
            public string DataEvento { get; set; }
            public string TipoEvento { get; set; }
            public string TitoloEvento { get; set; }
            public string OraEvento { get; set; }
            public string CausaleOmaggioRiduzione { get; set; }
            public string TipoTurno { get; set; }
            public Int64? NumeroEventiAbilitati { get; set; }
            public string DataLimiteValidita { get; set; }
            public DateTime? DataScadenzaAbbonamento { get; set; }
            public string CodiceAbbonamento { get; set; }
            public decimal? NumProgAbbonamento { get; set; }
            public decimal? RateoEvento { get; set; }
            public decimal? IvaRateo { get; set; }
            public decimal? RateoImponibileIntra { get; set; }
            public string CausaleOmaggioRiduzioneOpen { get; set; }
            public decimal? CorrispettivoTitolo { get; set; }
            public decimal? CorrispettivoPrevendita { get; set; }
            public decimal? IvaTitolo { get; set; }
            public decimal? IvaPrevendita { get; set; }
            public decimal? CorrispettivoFigurativo { get; set; }
            public decimal? IvaFigurativa { get; set; }
            public string CodiceFiscaleAbbonamento { get; set; }
            public string CodiceBigliettoAbbonamento { get; set; }
            public Int64? NumProgBigliettoAbbonamento { get; set; }
            public string CodicePrestazione1 { get; set; }
            public decimal? ImportoPrestazione1 { get; set; }
            public string CodicePrestazione2 { get; set; }
            public decimal? ImportoPrestazione2 { get; set; }
            public string CodicePrestazione3 { get; set; }
            public decimal? ImportoPrestazione3 { get; set; }
            public string CartaOriginaleAnnullato { get; set; }
            public string CausaleAnnullamento { get; set; }

            public string PartecipanteNome { get; set; }
            public string PartecipanteCognome { get; set; }
            public string AcqregAutenticazione { get; set; }
            public string AcqregCodiceunivocoAcq { get; set; }
            public string AcqregIndirizzoipReg { get; set; }
            public DateTime? AcqregDataoraReg { get; set; }
            public string AcqtranCodiceunivocoNumTran { get; set; }
            public string AcqtranCellulareAcq { get; set; }
            public string AcqtranEmailAcq { get; set; }
            public string AcqtranIndirizzoIpTran { get; set; }
            public DateTime? AcqtranDataorainiziocheckout { get; set; }
            public DateTime? AcqtranDataoraesecuzionePag { get; set; }
            public string AcqtranCro { get; set; }
            public string AcqtranMetodoSpedTitolo { get; set; }
            public string AcqtranIndirizzoSpedTitolo { get; set; }
            public string DigitaleTradizionale { get; set; }


            public clsResultTransazione()
            {
            }

            public clsResultTransazione(IRecordSet oRS)
            {
                if (oRS != null && !oRS.EOF)
                {
                    this.CodiceFiscaleOrganizzatore = (oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").Value.ToString());
                    this.CodiceFiscaleTitolare = (oRS.Fields("CODICE_FISCALE_TITOLARE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_TITOLARE").Value.ToString());
                    this.TitoloAbbonamento = (oRS.Fields("TITOLO_ABBONAMENTO").IsNull ? "" : oRS.Fields("TITOLO_ABBONAMENTO").Value.ToString());
                    this.TitoloIvaPreassolta = (oRS.Fields("TITOLO_IVA_PREASSOLTA").IsNull ? "" : oRS.Fields("TITOLO_IVA_PREASSOLTA").Value.ToString());
                    this.SpettacoloIntrattenimento = (oRS.Fields("SPETTACOLO_INTRATTENIMENTO").IsNull ? "" : oRS.Fields("SPETTACOLO_INTRATTENIMENTO").Value.ToString());
                    this.Valuta = (oRS.Fields("VALUTA").IsNull ? "" : oRS.Fields("VALUTA").Value.ToString());

                    if (!oRS.Fields("IMPONIBILE_INTRATTENIMENTI").IsNull) this.ImponibileIntrattenimenti = decimal.Parse(oRS.Fields("IMPONIBILE_INTRATTENIMENTI").Value.ToString());

                    this.OrdineDiPosto = (oRS.Fields("ORDINE_DI_POSTO").IsNull ? "" : oRS.Fields("ORDINE_DI_POSTO").Value.ToString());
                    this.Posto = (oRS.Fields("POSTO").IsNull ? "" : oRS.Fields("POSTO").Value.ToString());
                    this.TipoTitolo = (oRS.Fields("TIPO_TITOLO").IsNull ? "" : oRS.Fields("TIPO_TITOLO").Value.ToString());
                    this.Annullato = (oRS.Fields("ANNULLATO").IsNull ? "" : oRS.Fields("ANNULLATO").Value.ToString());
                    if (!oRS.Fields("PROGRESSIVO_ANNULLATI").IsNull) this.ProgressivoAnnullati = Int64.Parse(oRS.Fields("PROGRESSIVO_ANNULLATI").Value.ToString());
                    if (!oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").IsNull) this.DataEmissioneAnnullamento = (DateTime)oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").Value;
                    this.OraEmissioneAnnullamento = (oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").Value.ToString());
                    if (!oRS.Fields("PROGRESSIVO_TITOLO").IsNull) this.ProgressivoTitolo = Int64.Parse(oRS.Fields("PROGRESSIVO_TITOLO").Value.ToString());
                    this.CodicePuntoVendita = (oRS.Fields("CODICE_PUNTO_VENDITA").IsNull ? "" : oRS.Fields("CODICE_PUNTO_VENDITA").Value.ToString());
                    this.Sigillo = (oRS.Fields("SIGILLO").IsNull ? "" : oRS.Fields("SIGILLO").Value.ToString());
                    this.CodiceSistema = (oRS.Fields("CODICE_SISTEMA").IsNull ? "" : oRS.Fields("CODICE_SISTEMA").Value.ToString());
                    this.CodiceCarta = (oRS.Fields("CODICE_CARTA").IsNull ? "" : oRS.Fields("CODICE_CARTA").Value.ToString());
                    this.Prestampa = (oRS.Fields("PRESTAMPA").IsNull ? "" : oRS.Fields("PRESTAMPA").Value.ToString());
                    this.CodiceLocale = (oRS.Fields("CODICE_LOCALE").IsNull ? "" : oRS.Fields("CODICE_LOCALE").Value.ToString());

                    this.DataEvento = (oRS.Fields("DATA_EVENTO").IsNull ? "" : oRS.Fields("DATA_EVENTO").Value.ToString());

                    this.TipoEvento = (oRS.Fields("TIPO_EVENTO").IsNull ? "" : oRS.Fields("TIPO_EVENTO").Value.ToString());
                    this.TitoloEvento = (oRS.Fields("TITOLO_EVENTO").IsNull ? "" : oRS.Fields("TITOLO_EVENTO").Value.ToString());
                    this.OraEvento = (oRS.Fields("ORA_EVENTO").IsNull ? "" : oRS.Fields("ORA_EVENTO").Value.ToString());
                    this.CausaleOmaggioRiduzione = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").Value.ToString());
                    this.TipoTurno = (oRS.Fields("TIPO_TURNO").IsNull ? "" : oRS.Fields("TIPO_TURNO").Value.ToString());
                    if (!oRS.Fields("NUMERO_EVENTI_ABILITATI").IsNull) this.NumeroEventiAbilitati = Int64.Parse(oRS.Fields("NUMERO_EVENTI_ABILITATI").Value.ToString());
                    this.DataLimiteValidita = (oRS.Fields("DATA_LIMITE_VALIDITA").IsNull ? "" : oRS.Fields("DATA_LIMITE_VALIDITA").Value.ToString());
                    if (!oRS.Fields("DATA_SCADENZA_ABBONAMENTO").IsNull) this.DataScadenzaAbbonamento = (DateTime)oRS.Fields("DATA_SCADENZA_ABBONAMENTO").Value;
                    this.CodiceAbbonamento = (oRS.Fields("CODICE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("NUM_PROG_ABBONAMENTO").IsNull) this.NumProgAbbonamento = Int64.Parse(oRS.Fields("NUM_PROG_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("RATEO_EVENTO").IsNull) this.RateoEvento = decimal.Parse(oRS.Fields("RATEO_EVENTO").Value.ToString());
                    if (!oRS.Fields("IVA_RATEO").IsNull) this.IvaRateo = decimal.Parse(oRS.Fields("IVA_RATEO").Value.ToString());
                    if (!oRS.Fields("RATEO_IMPONIBILE_INTRA").IsNull) this.RateoImponibileIntra = decimal.Parse(oRS.Fields("RATEO_IMPONIBILE_INTRA").Value.ToString());
                    this.CausaleOmaggioRiduzioneOpen = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_TITOLO").IsNull) this.CorrispettivoTitolo = decimal.Parse(oRS.Fields("CORRISPETTIVO_TITOLO").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_PREVENDITA").IsNull) this.CorrispettivoPrevendita = decimal.Parse(oRS.Fields("CORRISPETTIVO_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("IVA_TITOLO").IsNull) this.IvaTitolo = decimal.Parse(oRS.Fields("IVA_TITOLO").Value.ToString());
                    if (!oRS.Fields("IVA_PREVENDITA").IsNull) this.IvaPrevendita = decimal.Parse(oRS.Fields("IVA_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_FIGURATIVO").IsNull) this.CorrispettivoFigurativo = decimal.Parse(oRS.Fields("CORRISPETTIVO_FIGURATIVO").Value.ToString());
                    if (!oRS.Fields("IVA_FIGURATIVA").IsNull) this.IvaFigurativa = decimal.Parse(oRS.Fields("IVA_FIGURATIVA").Value.ToString());
                    this.CodiceFiscaleAbbonamento = (oRS.Fields("CODICE_FISCALE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ABBONAMENTO").Value.ToString());
                    this.CodiceBigliettoAbbonamento = (oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").IsNull) this.NumProgBigliettoAbbonamento = Int64.Parse(oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").Value.ToString());
                    this.CodicePrestazione1 = (oRS.Fields("CODICE_PRESTAZIONE1").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE1").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE1").IsNull) this.ImportoPrestazione1 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE1").Value.ToString());
                    this.CodicePrestazione2 = (oRS.Fields("CODICE_PRESTAZIONE2").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE2").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE2").IsNull) this.ImportoPrestazione2 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE2").Value.ToString());
                    this.CodicePrestazione3 = (oRS.Fields("CODICE_PRESTAZIONE3").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE3").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE3").IsNull) this.ImportoPrestazione3 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE3").Value.ToString());
                    this.CartaOriginaleAnnullato = (oRS.Fields("CARTA_ORIGINALE_ANNULLATO").IsNull ? "" : oRS.Fields("CARTA_ORIGINALE_ANNULLATO").Value.ToString());
                    this.CausaleAnnullamento = (oRS.Fields("CAUSALE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("CAUSALE_ANNULLAMENTO").Value.ToString());

                    this.PartecipanteNome = (oRS.Fields("PARTECIPANTE_NOME").IsNull ? "" : oRS.Fields("PARTECIPANTE_NOME").Value.ToString());
                    this.PartecipanteCognome = (oRS.Fields("PARTECIPANTE_COGNOME").IsNull ? "" : oRS.Fields("PARTECIPANTE_COGNOME").Value.ToString());
                    this.AcqregAutenticazione = (oRS.Fields("ACQREG_AUTENTICAZIONE").IsNull ? "" : oRS.Fields("ACQREG_AUTENTICAZIONE").Value.ToString());
                    this.AcqregCodiceunivocoAcq = (oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").IsNull ? "" : oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").Value.ToString());
                    this.AcqregIndirizzoipReg = (oRS.Fields("ACQREG_INDIRIZZOIP_REG").IsNull ? "" : oRS.Fields("ACQREG_INDIRIZZOIP_REG").Value.ToString());
                    if (!oRS.Fields("ACQREG_DATAORA_REG").IsNull) this.AcqregDataoraReg = (DateTime)oRS.Fields("ACQREG_DATAORA_REG").Value;
                    this.AcqtranCodiceunivocoNumTran = (oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").Value.ToString());
                    this.AcqtranCellulareAcq = (oRS.Fields("ACQTRAN_CELLULARE_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_CELLULARE_ACQ").Value.ToString());
                    this.AcqtranEmailAcq = (oRS.Fields("ACQTRAN_EMAIL_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_EMAIL_ACQ").Value.ToString());
                    this.AcqtranIndirizzoIpTran = (oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").Value.ToString());
                    if (!oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").IsNull) this.AcqtranDataorainiziocheckout = (DateTime)oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").Value;
                    if (!oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").IsNull) this.AcqtranDataoraesecuzionePag = (DateTime)oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").Value;
                    this.AcqtranCro = (oRS.Fields("ACQTRAN_CRO").IsNull ? "" : oRS.Fields("ACQTRAN_CRO").Value.ToString());
                    this.AcqtranMetodoSpedTitolo = (oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").Value.ToString());
                    this.AcqtranIndirizzoSpedTitolo = (oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").Value.ToString());
                    this.DigitaleTradizionale = (oRS.Fields("DIGITALE_TRADIZIONALE").IsNull ? "" : oRS.Fields("DIGITALE_TRADIZIONALE").Value.ToString());
                }
            }



            public clsResultTransazione(System.Data.DataRow oRow)
            {
                if (oRow != null)
                {
                    this.CodiceFiscaleOrganizzatore = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? "" : oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString());
                    this.CodiceFiscaleTitolare = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_TITOLARE"].ToString()) ? "" : oRow["CODICE_FISCALE_TITOLARE"].ToString());
                    this.TitoloAbbonamento = (string.IsNullOrEmpty(oRow["TITOLO_ABBONAMENTO"].ToString()) ? "" : oRow["TITOLO_ABBONAMENTO"].ToString());
                    this.TitoloIvaPreassolta = (string.IsNullOrEmpty(oRow["TITOLO_IVA_PREASSOLTA"].ToString()) ? "" : oRow["TITOLO_IVA_PREASSOLTA"].ToString());
                    this.SpettacoloIntrattenimento = (string.IsNullOrEmpty(oRow["SPETTACOLO_INTRATTENIMENTO"].ToString()) ? "" : oRow["SPETTACOLO_INTRATTENIMENTO"].ToString());
                    this.Valuta = (string.IsNullOrEmpty(oRow["VALUTA"].ToString()) ? "" : oRow["VALUTA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString())) this.ImponibileIntrattenimenti = decimal.Parse(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString());
                    this.OrdineDiPosto = (string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? "" : oRow["ORDINE_DI_POSTO"].ToString());
                    this.Posto = (string.IsNullOrEmpty(oRow["POSTO"].ToString()) ? "" : oRow["POSTO"].ToString());
                    this.TipoTitolo = (string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? "" : oRow["TIPO_TITOLO"].ToString());
                    this.Annullato = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "" : oRow["ANNULLATO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_ANNULLATI"].ToString())) this.ProgressivoAnnullati = Int64.Parse(oRow["PROGRESSIVO_ANNULLATI"].ToString());
                    if (!string.IsNullOrEmpty(oRow["DATA_EMISSIONE_ANNULLAMENTO"].ToString())) this.DataEmissioneAnnullamento = (DateTime)oRow["DATA_EMISSIONE_ANNULLAMENTO"];
                    this.OraEmissioneAnnullamento = (string.IsNullOrEmpty(oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString()) ? "" : oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO"].ToString())) this.ProgressivoTitolo = Int64.Parse(oRow["PROGRESSIVO_TITOLO"].ToString());
                    this.CodicePuntoVendita = (string.IsNullOrEmpty(oRow["CODICE_PUNTO_VENDITA"].ToString()) ? "" : oRow["CODICE_PUNTO_VENDITA"].ToString());
                    this.Sigillo = (string.IsNullOrEmpty(oRow["SIGILLO"].ToString()) ? "" : oRow["SIGILLO"].ToString());
                    this.CodiceSistema = (string.IsNullOrEmpty(oRow["CODICE_SISTEMA"].ToString()) ? "" : oRow["CODICE_SISTEMA"].ToString());
                    this.CodiceCarta = (string.IsNullOrEmpty(oRow["CODICE_CARTA"].ToString()) ? "" : oRow["CODICE_CARTA"].ToString());
                    this.Prestampa = (string.IsNullOrEmpty(oRow["PRESTAMPA"].ToString()) ? "" : oRow["PRESTAMPA"].ToString());
                    this.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? "" : oRow["CODICE_LOCALE"].ToString());
                    this.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? "" : oRow["DATA_EVENTO"].ToString());
                    this.TipoEvento = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? "" : oRow["TIPO_EVENTO"].ToString());
                    this.TitoloEvento = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? "" : oRow["TITOLO_EVENTO"].ToString());
                    this.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? "" : oRow["ORA_EVENTO"].ToString());
                    this.CausaleOmaggioRiduzione = (string.IsNullOrEmpty(oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString()) ? "" : oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString());
                    this.TipoTurno = (string.IsNullOrEmpty(oRow["TIPO_TURNO"].ToString()) ? "" : oRow["TIPO_TURNO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString())) this.NumeroEventiAbilitati =  Int64.Parse(oRow["NUMERO_EVENTI_ABILITATI"].ToString());
                    this.DataLimiteValidita = (string.IsNullOrEmpty(oRow["DATA_LIMITE_VALIDITA"].ToString()) ? "" : oRow["DATA_LIMITE_VALIDITA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["DATA_SCADENZA_ABBONAMENTO"].ToString())) this.DataScadenzaAbbonamento = (DateTime)oRow["DATA_SCADENZA_ABBONAMENTO"];
                    this.CodiceAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUM_PROG_ABBONAMENTO"].ToString())) this.NumProgAbbonamento = Int64.Parse(oRow["NUM_PROG_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["RATEO_EVENTO"].ToString())) this.RateoEvento = decimal.Parse(oRow["RATEO_EVENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_RATEO"].ToString())) this.IvaRateo = decimal.Parse(oRow["IVA_RATEO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["RATEO_IMPONIBILE_INTRA"].ToString())) this.RateoImponibileIntra = decimal.Parse(oRow["RATEO_IMPONIBILE_INTRA"].ToString());
                    this.CausaleOmaggioRiduzioneOpen = (string.IsNullOrEmpty(oRow["CAUSALE_OMAGGIO_RIDUZIONE_OPEN"].ToString()) ? "" : oRow["CAUSALE_OMAGGIO_RIDUZIONE_OPEN"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_TITOLO"].ToString())) this.CorrispettivoTitolo = decimal.Parse(oRow["CORRISPETTIVO_TITOLO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_PREVENDITA"].ToString())) this.CorrispettivoPrevendita = decimal.Parse(oRow["CORRISPETTIVO_PREVENDITA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_TITOLO"].ToString())) this.IvaTitolo = decimal.Parse(oRow["IVA_TITOLO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_PREVENDITA"].ToString())) this.IvaPrevendita = decimal.Parse(oRow["IVA_PREVENDITA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_FIGURATIVO"].ToString())) this.CorrispettivoFigurativo = decimal.Parse(oRow["CORRISPETTIVO_FIGURATIVO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_FIGURATIVA"].ToString())) this.IvaFigurativa = decimal.Parse(oRow["IVA_FIGURATIVA"].ToString());
                    this.CodiceFiscaleAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_FISCALE_ABBONAMENTO"].ToString());
                    this.CodiceBigliettoAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString())) this.NumProgBigliettoAbbonamento = Int64.Parse(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString());
                    this.CodicePrestazione1 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE1"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE1"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE1"].ToString())) this.ImportoPrestazione1 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE1"].ToString());
                    this.CodicePrestazione2 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE2"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE2"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) this.ImportoPrestazione2 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE2"].ToString());
                    this.CodicePrestazione3 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE3"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE3"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) this.ImportoPrestazione3 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE3"].ToString());
                    this.CartaOriginaleAnnullato = (string.IsNullOrEmpty(oRow["CARTA_ORIGINALE_ANNULLATO"].ToString()) ? "" : oRow["CARTA_ORIGINALE_ANNULLATO"].ToString());
                    this.CausaleAnnullamento = (string.IsNullOrEmpty(oRow["CAUSALE_ANNULLAMENTO"].ToString()) ? "" : oRow["CAUSALE_ANNULLAMENTO"].ToString());

                    this.PartecipanteNome = (string.IsNullOrEmpty(oRow["PARTECIPANTE_NOME"].ToString()) ? "" : oRow["PARTECIPANTE_NOME"].ToString());
                    this.PartecipanteCognome = (string.IsNullOrEmpty(oRow["PARTECIPANTE_COGNOME"].ToString()) ? "" : oRow["PARTECIPANTE_COGNOME"].ToString());
                    this.AcqregAutenticazione = (string.IsNullOrEmpty(oRow["ACQREG_AUTENTICAZIONE"].ToString()) ? "" : oRow["ACQREG_AUTENTICAZIONE"].ToString());
                    this.AcqregCodiceunivocoAcq = (string.IsNullOrEmpty(oRow["ACQREG_CODICEUNIVOCO_ACQ"].ToString()) ? "" : oRow["ACQREG_CODICEUNIVOCO_ACQ"].ToString());
                    this.AcqregIndirizzoipReg = (string.IsNullOrEmpty(oRow["ACQREG_INDIRIZZOIP_REG"].ToString()) ? "" : oRow["ACQREG_INDIRIZZOIP_REG"].ToString());
                    if (!string.IsNullOrEmpty(oRow["ACQREG_DATAORA_REG"].ToString())) this.AcqregDataoraReg = (DateTime)oRow["ACQREG_DATAORA_REG"];
                    this.AcqtranCodiceunivocoNumTran = (string.IsNullOrEmpty(oRow["ACQTRAN_CODICEUNIVOCO_NUM_TRAN"].ToString()) ? "" : oRow["ACQTRAN_CODICEUNIVOCO_NUM_TRAN"].ToString());
                    this.AcqtranCellulareAcq = (string.IsNullOrEmpty(oRow["ACQTRAN_CELLULARE_ACQ"].ToString()) ? "" : oRow["ACQTRAN_CELLULARE_ACQ"].ToString());
                    this.AcqtranEmailAcq = (string.IsNullOrEmpty(oRow["ACQTRAN_EMAIL_ACQ"].ToString()) ? "" : oRow["ACQTRAN_EMAIL_ACQ"].ToString());
                    this.AcqtranIndirizzoIpTran = (string.IsNullOrEmpty(oRow["ACQTRAN_INDIRIZZO_IP_TRAN"].ToString()) ? "" : oRow["ACQTRAN_INDIRIZZO_IP_TRAN"].ToString());
                    if (!string.IsNullOrEmpty(oRow["ACQTRAN_DATAORAINIZIOCHECKOUT"].ToString())) this.AcqtranDataorainiziocheckout = (DateTime)oRow["ACQTRAN_DATAORAINIZIOCHECKOUT"];
                    if (!string.IsNullOrEmpty(oRow["ACQTRAN_DATAORAESECUZIONE_PAG"].ToString())) this.AcqtranDataoraesecuzionePag = (DateTime)oRow["ACQTRAN_DATAORAESECUZIONE_PAG"];
                    this.AcqtranCro = (string.IsNullOrEmpty(oRow["ACQTRAN_CRO"].ToString()) ? "" : oRow["ACQTRAN_CRO"].ToString());
                    this.AcqtranMetodoSpedTitolo = (string.IsNullOrEmpty(oRow["ACQTRAN_METODO_SPED_TITOLO"].ToString()) ? "" : oRow["ACQTRAN_METODO_SPED_TITOLO"].ToString());
                    this.AcqtranIndirizzoSpedTitolo = (string.IsNullOrEmpty(oRow["ACQTRAN_INDIRIZZO_SPED_TITOLO"].ToString()) ? "" : oRow["ACQTRAN_INDIRIZZO_SPED_TITOLO"].ToString());
                    this.DigitaleTradizionale = (string.IsNullOrEmpty(oRow["DIGITALE_TRADIZIONALE"].ToString()) ? "" : oRow["DIGITALE_TRADIZIONALE"].ToString());

                }
            }
        }

        // Classe singolo campo log transazioni
        public class clsResultBaseCampoLogTransazioni 
        {
            public string Campo = "";
            public string Tipo = "";
            public int Inizio = 0;
            public int Dimensione = 0;
            public string DescCampo = "";
            public string Descrizione = "";

            public clsResultBaseCampoLogTransazioni()
            {
            }
            
            public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione)
            {
                this.Campo = cCampo;
                this.Tipo = cTipo;
                this.Inizio = nInizio;
                this.Dimensione = nDimensione;
                this.DescCampo = cDescCampo;
                this.Descrizione = cDescrizione;
            }
        }

        // Risultato base che contiene un array di descrizione dei campi del log delle transazioni 
        public class ResultBaseCampiLogTransazioni : ResultBase
        {
            public clsResultBaseCampoLogTransazioni[] Campi = null;
            public Riepiloghi.clsFiltroCampoTransazione[] Filtri = null;

            public ResultBaseCampiLogTransazioni()
            {
            }

            public ResultBaseCampiLogTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oListaCampi != null && oListaCampi.Count > 0)
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[oListaCampi.Count];
                    int Index = 0;
                    foreach (System.Collections.DictionaryEntry ItemCampoDict in oListaCampi)
                    {
                        Riepiloghi.clsCampoTransazione oCampo = (Riepiloghi.clsCampoTransazione)ItemCampoDict.Value;
                        this.Campi[Index] = new clsResultBaseCampoLogTransazioni(oCampo.Campo, oCampo.Tipo, oCampo.Inizio, oCampo.Dimensione, oCampo.DescCampo, oCampo.Descrizione);
                        Index += 1;
                    }
                }
                else
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[0];
                }
            }

            public ResultBaseCampiLogTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi, Riepiloghi.clsFiltroCampoTransazione[] oListaFiltri)
                : this(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oListaCampi)
            {
                if (oListaFiltri != null && oListaFiltri.Length > 0)
                {
                    this.Filtri = new Riepiloghi.clsFiltroCampoTransazione[oListaFiltri.Length];
                    int Index = 0;
                    foreach (Riepiloghi.clsFiltroCampoTransazione oFiltro in oListaFiltri)
                    {
                        this.Filtri[Index] = new Riepiloghi.clsFiltroCampoTransazione(oFiltro.Campo, oFiltro.Tipo, oFiltro.Predefinito);
                        this.Filtri[Index].ValoriPredefiniti = oFiltro.ValoriPredefiniti;
                        Index += 1;
                    }
                }
                else
                {
                    this.Filtri = new Riepiloghi.clsFiltroCampoTransazione[0];
                }
            }
        }

        // Risultato base che contiene un array di clsResulttransazioni (lista delle transazioni)
        public class ResultBaseGetTransazioni : ResultBase
        {
            public clsResultTransazione[] Transazioni = null;
            public System.Data.DataTable TableTransazioni = null;

            public ResultBaseGetTransazioni()
            {
                
            }

            public ResultBaseGetTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = (lStatusOK && oRS != null);
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                if (oRS != null && !oRS.EOF)
                {
                    if (CodiciLocaliAbilitati != null)
                    {
                        System.Data.DataTable tableResult = new System.Data.DataTable();
                        foreach (System.Data.DataColumn column in ((System.Data.DataTable)oRS).Columns)
                        {
                            System.Data.DataColumn oCol = new System.Data.DataColumn(column.ColumnName, column.DataType,column.Expression,column.ColumnMapping);
                            tableResult.Columns.Add(oCol);
                        }
                        foreach (System.Data.DataRow row in ((System.Data.DataTable)oRS).Rows)
                        {
                            if (row["TITOLO_ABBONAMENTO"].ToString() == "A" || CodiciLocaliAbilitati.Contains(row["CODICE_LOCALE"].ToString()))
                            {
                                System.Data.DataRow oRow = tableResult.NewRow();
                                oRow.ItemArray = row.ItemArray;
                                tableResult.Rows.Add(oRow);
                            }
                        }


                        this.Transazioni = new clsResultTransazione[tableResult.Rows.Count];
                        int Index = 0;
                        foreach (System.Data.DataRow oRow in tableResult.Rows)
                        {
                            this.Transazioni[Index] = new clsResultTransazione(oRow);
                            Index += 1;
                        }
                       
                        if (PreserveDataTable)
                        {
                            this.TableTransazioni = tableResult;
                            this.TableTransazioni.TableName = "TRANSAZIONI";
                        }
                    }
                    else
                    {
                        this.Transazioni = new clsResultTransazione[(int)oRS.RecordCount];
                        int Index = 0;
                        while (!oRS.EOF)
                        {
                            this.Transazioni[Index] = new clsResultTransazione(oRS);
                            Index += 1;
                            oRS.MoveNext();
                        }
                        if (PreserveDataTable)
                        {
                            this.TableTransazioni = (System.Data.DataTable)oRS;
                            this.TableTransazioni.TableName = "TRANSAZIONI";
                        }
                    }
                }
                else
                {
                    this.Transazioni = new clsResultTransazione[0];
                }

            }
        }

        // Risultato transazioni per periodo che contiene un array di clsResulttransazioni (lista delle transazioni)
        // ed il dettaglio della richiesta con data inizio, data fine e modalità storico
        public class ResultTransazionePeriodo : ResultBaseGetTransazioni
        {
            public DateTime DataEmiInizio;
            public DateTime DataEmiFine;
            public string ModalitaStorico = "";

            public ResultTransazionePeriodo()
            {
            }

            public ResultTransazionePeriodo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataEmiInizio, DateTime dDataEmiFine, string cModalitaStorico, IRecordSet oRS, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, PreserveDataTable, CodiciLocaliAbilitati)
            {
                this.DataEmiInizio = dDataEmiInizio;
                this.DataEmiFine = dDataEmiFine;
                this.ModalitaStorico = cModalitaStorico;
            }
        }

        [WebMethod]
        public ResultBaseCampiLogTransazioni GetListaCampiLogTransazioni(string Token)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCampiLogTransazioni oResult = null;
            Exception oError = null;
            System.Collections.SortedList oListaCampi = null;
            Riepiloghi.clsFiltroCampoTransazione[] oListaFiltri = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError);
                            if (oListaCampi == null || oListaCampi.Count == 0)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi log transazioni", "Lista campi log transazioni non caricati.");
                            }
                            else
                            {
                                oListaFiltri = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostati(oConnection, out oError);
                                if (oError != null)
                                {
                                    oListaFiltri = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Richiesta campi log transazioni", "Errore imprevisto durante la richiesta della lista dei campi log transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                if (oListaFiltri == null)
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
                }
                else
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi, oListaFiltri);
                }
                
            }
            else
            {
                oResult = new ResultBaseCampiLogTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            return oResult;
        }

        [WebMethod]
        public ResultBaseCampiLogTransazioni GetListaCampiLogTransazioniCS(string Token, string CodiceSistema)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCampiLogTransazioni oResult = null;
            Exception oError = null;
            System.Collections.SortedList oListaCampi = null;
            Riepiloghi.clsFiltroCampoTransazione[] oListaFiltri = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        //Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount()
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false);
                        if (oError == null)
                            if (oAccount == null)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi", "Account non valido");

                        if (oError == null)
                        {
                            bool lFindCS = false;
                            if (oAccount.CodiciSistema != null && oAccount.CodiciSistema.Count > 0)
                            {
                                foreach (Riepiloghi.clsCodiceSistema oCodiceSistemaAccount in oAccount.CodiciSistema)
                                {
                                    lFindCS = (oCodiceSistemaAccount.CodiceSistema == CodiceSistema);
                                    if (lFindCS) break;
                                }
                            }

                            if (!lFindCS)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi", "Codice Sistema non valido per questo Account.");
                        }

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, CodiceSistema, out oError);
                            if (oError == null)
                            {
                                oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError);
                                if (oListaCampi == null || oListaCampi.Count == 0)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi log transazioni", "Lista campi log transazioni non caricati.");
                                }
                                else
                                {
                                    oListaFiltri = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostati(oConnection, out oError);
                                    if (oError != null)
                                    {
                                        oListaFiltri = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Richiesta campi log transazioni", "Errore imprevisto durante la richiesta della lista dei campi log transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                if (oListaFiltri == null)
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
                }
                else
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi, oListaFiltri);
                }

            }
            else
            {
                oResult = new ResultBaseCampiLogTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            return oResult;
        }


        // Lista delle transazioni per periodo
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioni(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string PreserveDataTable, Int64 IdFilterExtIdVend)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetTransazioni", Token, new Dictionary<string, object>() { { "dDataEmiInizio", dDataEmiInizio }, { "dDataEmiFine", dDataEmiFine }, { "ModalitaStorico", ModalitaStorico }, { "PreserveDataTable", PreserveDataTable }, { "IdFilterExtIdVend", IdFilterExtIdVend } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);

                        if (oError == null)
                        {
                            CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);
                            if (oError == null)
                            {
                                oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, null, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oError == null)
                {
                    try
                    {
                        oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                    }
                    catch (Exception ex)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
                    }
                }

                if (oError != null)
                {
                    oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultTransazionePeriodo", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Lista delle transazioni per periodo + classe contenente i filtri
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniStringFilter(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string FiltroString, string PreserveDataTable, Int64 IdFilterExtIdVend)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                            if (oError == null)
                            {
                                System.Collections.SortedList oFiltriTransazioni = null;
                                clsFiltroTransazioni Filtro = new clsFiltroTransazioni(oConnection, FiltroString, out oError);

                                if (oError == null)
                                {
                                    bool lExistsFilterCodiceLocale = false;
                                    if (Filtro != null)
                                    {
                                        oFiltriTransazioni = new System.Collections.SortedList();
                                        foreach (KeyValuePair<string, object> Item in Filtro.Filtri)
                                        {
                                            lExistsFilterCodiceLocale = lExistsFilterCodiceLocale || Item.Key == "CODICE_LOCALE";
                                            if (!oFiltriTransazioni.ContainsKey(Item.Key))
                                            {
                                                oFiltriTransazioni.Add(Item.Key, Item.Value);
                                            }
                                        }
                                    }
                                    if (oError == null)
                                    {
                                        oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, oFiltriTransazioni, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE",  null);
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniClassFilter(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, clsFiltroTransazioni Filtro, string PreserveDataTable, Int64 IdFilterExtIdVend)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);

                        if (oError == null)
                        {
                            CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                            if (oError == null)
                            {
                                System.Collections.SortedList oFiltriTransazioni = null;
                                if (Filtro != null)
                                {
                                    oFiltriTransazioni = new System.Collections.SortedList();
                                    foreach (KeyValuePair<string, object> Item in Filtro.Filtri)
                                    {
                                        if (!oFiltriTransazioni.ContainsKey(Item.Key))
                                        {
                                            oFiltriTransazioni.Add(Item.Key, Item.Value);
                                        }
                                    }
                                }
                                if (oError == null)
                                {
                                    oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataEmiInizio, dDataEmiFine, oFiltriTransazioni, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }


        // Lista delle transazioni annullate per perido (annullanti ed annullati)
        [WebMethod]
        public ResultTransazionePeriodo GetTransazioniAnnullati(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, string PreserveDataTable, Int64 IdFilterExtIdVend)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);
                            if (oError == null)
                            {
                                oRS = Riepiloghi.clsRiepiloghi.GetTransazioniAnnullati(dDataEmiInizio, dDataEmiFine, oConnection, this.GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni annullate", "Errore imprevisto durante lettura delle transazioni annullate.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE", CodiciLocaliAbilitati);
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Risultato transazione per carta e sigillo o progressivo che contiene un array di clsResulttransazioni (lista delle transazioni, una sola)
        // ed il dettaglio della richiesta con carta e sigillo o progressivo
        public class ResultTransazioneCartaSigilloProgressivo : ResultBaseGetTransazioni
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;

            public ResultTransazioneCartaSigilloProgressivo()
            {
            }

            public ResultTransazioneCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Carta, string Sigillo, Int64 Progressivo, IRecordSet oRS, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, PreserveDataTable, null)
            {
                this.Carta = Carta;
                this.Sigillo = Sigillo;
                this.Progressivo = Progressivo;
            }
        }

        // Ricerca transazione carta e progressivo
        [WebMethod]
        public ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_PROGRESSIVO(string Token, string Carta, Int64 Progressivo, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(Carta, Progressivo, oConnection, out oError);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivo", "Errore imprevisto durante lettura delle transazioni per carta e progressivo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca transazione carta e sigillo
        [WebMethod]
        public ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_SIGILLO(string Token, string Carta, string Sigillo, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_SIGILLO(Carta, Sigillo, oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e sigillo", "Errore imprevisto durante lettura delle transazioni per carta e sigillo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
        public void GetTable(string Token)
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            DateTime dStart = new DateTime(2015,01,01);
            DateTime dStop = new DateTime(2016,01,01);
            ResultBaseGetTransazioni oResult = this.GetTransazioni(Token, dStart, dStop, "TRUE", "TRUE", 0);
            string cResult = ConvertDataTableTojSonString(oResult.TableTransazioni);
            Context.Response.Write(js.Serialize(cResult));
        }


        private String ConvertDataTableTojSonString(System.Data.DataTable dataTable)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<String, Object>> tableRows = new List<Dictionary<String, Object>>();
            Dictionary<String, Object> row;
            foreach (System.Data.DataRow dr in dataTable.Rows)
            {
                row = new Dictionary<String, Object>();
                foreach (System.Data.DataColumn col in dataTable.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                tableRows.Add(row);
            }
            return serializer.Serialize(tableRows);
        }  


        #endregion

        #region "Ritorno delle tabelle di base"

        // Classe generica Dictionary per ritorno tabelle standard CODICE e DESCRIZIONE
        [XmlRoot("dictionary")]
        public class clsDizionarioTabella<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);
                    
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }

        }

        public class GenericColumn
        {
            public string Field = "";
            public string Type = "";

            public GenericColumn()
            {
            }

            public GenericColumn(System.Data.DataColumn oCol)
            {
                this.Field = oCol.ColumnName;
                if (oCol.DataType == typeof(string))
                {
                    this.Type = "S";
                }
                else if (oCol.DataType == typeof(DateTime))
                {
                    this.Type = "D";
                }
                else if (oCol.DataType == typeof(decimal) || oCol.DataType == typeof(int))
                {
                    this.Type = "N";
                }
            }

            public static List<object> GetRow(System.Data.DataRow oRow)
            {
                List<object> SingleRow = new List<object>();
                foreach (System.Data.DataColumn oCol in oRow.Table.Columns)
                {
                    SingleRow.Add(oRow[oCol.ColumnName]);
                }
                return SingleRow;
            }

            public static clsDizionarioTabella<String, GenericColumn> GetCols(System.Data.DataTable oTable)
            {
                clsDizionarioTabella<String, GenericColumn> oColumns = new clsDizionarioTabella<string, GenericColumn>();
                foreach (System.Data.DataColumn oCol in oTable.Columns)
                {
                    GenericColumn oColValue = new GenericColumn(oCol);
                    oColumns.Add(oCol.ColumnName, oColValue);
                }
                return oColumns;
            }

            public static List<List<object>> GetRows(System.Data.DataTable oTable)
            {
                List<List<object>> oRowsValue = new List<List<object>>();
                foreach (System.Data.DataRow oRow in oTable.Rows)
                {
                    oRowsValue.Add(GetRow(oRow));
                }
                return oRowsValue;
            }
        }

        public class ResultBaseGenericTable : ResultBase
        {
            public System.Data.DataTable oTable;

            public ResultBaseGenericTable()
            {
            }

            public ResultBaseGenericTable(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                System.Data.DataTable oTableSource = (System.Data.DataTable)oRS;
                this.oTable = new System.Data.DataTable("TABLE");
                foreach (System.Data.DataColumn oCol in oTableSource.Columns)
                {
                    System.Data.DataColumn oNewCol = new System.Data.DataColumn(oCol.ColumnName, oCol.DataType, oCol.Expression, oCol.ColumnMapping);
                    this.oTable.Columns.Add(oNewCol);
                }

                foreach (System.Data.DataRow oRow in oTableSource.Rows)
                {
                    System.Data.DataRow oNewRow = this.oTable.NewRow();
                    foreach (System.Data.DataColumn oCol in this.oTable.Columns)
                    {
                        oNewRow[oCol.ColumnName] = oRow[oCol.ColumnName];
                    }
                    this.oTable.Rows.Add(oNewRow);
                }
            }
        }

        private ResultBaseGenericTable GetGenericTable(string Token, string QueryTabella)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseGenericTable oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery(QueryTabella);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseGenericTable(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseGenericTable(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            //if (oRS != null) { oRS.Close(); }
            return oResult;
        }

        // Classe per risultato tabella standard
        public class ResultBaseTabella : ResultBase
        {
            public clsDizionarioTabella<string, string> Righe = new clsDizionarioTabella<string, string>();

            public ResultBaseTabella()
            {
            }

            public ResultBaseTabella(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        if (!Righe.ContainsKey(oRS.Fields("codice").Value.ToString()))
                        {
                            Righe.Add(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                        }
                        oRS.MoveNext();
                    }
                }
            }
        }

        private ResultBaseTabella GetTabellaStandard(string Token, string QueryTabella)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseTabella oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery(QueryTabella);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseTabella(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseTabella(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;

        }

        // VR_TIPOEVENTO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoEvento(string Token)
        {
            return this.GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOEVENTO ORDER BY CODICE");
        }

        // VR_ORDINIDIPOSTO
        [WebMethod]
        public ResultBaseTabella GetTabellaOrdiniDiPosto(string Token)
        {
            return this.GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_ORDINIDIPOSTO ORDER BY CODICE");
        }

        // VR_TIPOTITOLO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoTitolo(string Token)
        {
            return this.GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOTITOLO ORDER BY CODICE");
        }

        // VR_TIPO_TITOLO_OMAGGIO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoTitoloOmaggio(string Token)
        {
            return this.GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPO_TITOLO_OMAGGIO ORDER BY CODICE");
        }

        // VR_TIPOPROVENTO
        [WebMethod]
        public ResultBaseTabella GetTabellaTipoProvento(string Token)
        {
            return this.GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOPROVENTO ORDER BY CODICE");
        }

        // classe per VR_CRITERIO_OMAGGI_CONFIG
        public class ResultBaseCriteriOmaggioConfig : ResultBase
        {
            public clsDizionarioTabella<Int64, Riepiloghi.clsCriterioOmaggiConfig> Righe = new clsDizionarioTabella<long, Riepiloghi.clsCriterioOmaggiConfig>();

            public ResultBaseCriteriOmaggioConfig()
            {
            }

            public ResultBaseCriteriOmaggioConfig(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        Int64 nOrdine = 0;
                        Int64 nEnabled = 0;
                        if (!oRS.Fields("ordine").IsNull && 
                            Int64.TryParse(oRS.Fields("ordine").Value.ToString(), out nOrdine) && 
                            Int64.TryParse(oRS.Fields("enabled").Value.ToString(), out nEnabled))
                        {
                            Riepiloghi.clsCriterioOmaggiConfig oConfig = new Riepiloghi.clsCriterioOmaggiConfig(nOrdine, nEnabled, oRS.Fields("origine").Value.ToString(), oRS.Fields("ordine_di_posto").Value.ToString(), oRS.Fields("tipo_titolo").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                            if (!Righe.ContainsKey(oConfig.Ordine))
                            {
                                Righe.Add(oConfig.Ordine, oConfig);
                            }
                        }

                        oRS.MoveNext();
                    }
                }
            }
        }

        // VR_CRITERIO_OMAGGI_CONFIG
        [WebMethod]
        public ResultBaseCriteriOmaggioConfig GetTabellaCriteriOmaggiConfig(string Token)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCriteriOmaggioConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT * FROM VR_CRITERIO_OMAGGI_CONFIG ORDER BY ORDINE");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;
        }

        // VR_FILM
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_FILM(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_FILM ORDER BY TITOLO");
        }

        // VR_PACCHETTO
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_PACCHETTO(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_PACCHETTO ORDER BY DESCRIZIONE");
        }

        // VR_ORGANIZZATORI
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_ORGANIZZATORI(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_ORGANIZZATORI ORDER BY RAG_SOC");
        }

        // VR_NOLEGGIO
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_NOLEGGIO(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_NOLEGGIO ORDER BY RAG_SOC");
        }

        // VR_PRODUTTORI
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_PRODUTTORI(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_PRODUTTORI ORDER BY RAG_SOC");
        }

        // VR_ODPBIGLIETTO
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_ODPBIGLIETTO_DESCR(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_ODPBIGLIETTO_DESCR ORDER BY IDSALA, ODP, IDTIPOBIGL");
        }

        // VR_TIPO_BIGLIETTO
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_TIPO_BIGLIETTO(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_TIPO_BIGLIETTO ORDER BY IDTIPOBIGL");
        }

        // VR_SALE
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_SALE(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_SALE ORDER BY IDSALA");
        }

        // VR_SALE_LUOGO
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_SALE_LUOGO(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_SALE_LUOGO ORDER BY IDSALA");
        }

        // VR_POSTI
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_POSTI(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_POSTI ORDER BY IDSALA, FILA, COLONNA");
        }

        // VR_TE_IMPOSTE
        [WebMethod]
        public ResultBaseGenericTable GetTabellaVR_TE_IMPOSTE(string Token)
        {
            return this.GetGenericTable(Token, "SELECT * FROM VR_TE_IMPOSTE");
        }

        #endregion

        #region "Incidenza Intrattenimento"

        public class ResultBaseIncidenzaIntrattenimento : ResultBase
        {
            public Riepiloghi.clsIncidenzaIntrattenimentoPerEvento[] Eventi = new Riepiloghi.clsIncidenzaIntrattenimentoPerEvento[0];
            public DateTime InizioPeriodo = DateTime.MinValue;
            public DateTime FinePeriodo = DateTime.MaxValue;
            public bool ExistsEventoSenzaPercentuale = false;

            public ResultBaseIncidenzaIntrattenimento()
                :base()
            {

            }

            public ResultBaseIncidenzaIntrattenimento(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                :base (lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {

            }
        }

        [WebMethod]
        public ResultBaseIncidenzaIntrattenimento CheckIncidenzaIntrattenimento(string Token, DateTime InizioPeriodo, DateTime FinePeriodo)
        {
            ResultBaseIncidenzaIntrattenimento oRet = new ResultBaseIncidenzaIntrattenimento();
            oRet.InizioPeriodo = InizioPeriodo;
            oRet.FinePeriodo = FinePeriodo;

            DateTime dStart = DateTime.Now;
            Exception oError = null;
            bool lExistsEventoSenzaPercentuale = false;
            List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oLista = new List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento>();

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            if (InizioPeriodo > FinePeriodo)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Periodo non valido.");
                            }
                            else
                            {
                                if (InizioPeriodo.Date == FinePeriodo.Date || InizioPeriodo.Date == FinePeriodo.Date.AddDays(-1))
                                {
                                    List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oListaCompleta = Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.GetIncidenzaIntrattenimentoPerEvento(oConnection, oRet.InizioPeriodo, oRet.FinePeriodo, out lExistsEventoSenzaPercentuale, out oError);

                                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                                    Riepiloghi.clsAccount Account = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false);

                                    foreach (Riepiloghi.clsIncidenzaIntrattenimentoPerEvento oItem in oListaCompleta)
                                    {
                                        bool lAppend = false;
                                        foreach (Riepiloghi.clsCodiceSistema oCodiceSistema in Account.CodiciSistema)
                                        {
                                            if (oCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oCodiceSistema.CodiciLocale)
                                                {
                                                    lAppend = (oCodiceLocale.CodiceLocale == oItem.CodiceLocale);
                                                    if (lAppend) { break; }
                                                }
                                            }
                                            if (lAppend) { break; }
                                        }
                                        if (lAppend)
                                            oLista.Add(oItem);
                                    }
                                }
                                else
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Periodo troppo esteso.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Errore imprevisto nella lettura degli eventi per verifica incidenza intrattenimento.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(true, "Incidenza Intrattenimento:" + (oLista.Count == 0 ? "Nessun evento di tipo Intrattenimento nel periodo." : (oLista.Count == 1 ? "un evento" : oLista.Count.ToString() + " eventi") + " di tipo Intrattenimento nel periodo."), 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                oRet.ExistsEventoSenzaPercentuale = lExistsEventoSenzaPercentuale;
                oRet.Eventi = oLista.ToArray();
            }

            oRet.InizioPeriodo = InizioPeriodo;
            oRet.FinePeriodo = FinePeriodo;

            return oRet;
        }

        [WebMethod]
        public ResultBaseIncidenzaIntrattenimento SetIncidenzaIntrattenimento(string Token, ResultBaseIncidenzaIntrattenimento IncidenzaIntrattenimentoEventi)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseIncidenzaIntrattenimento oRet = new ResultBaseIncidenzaIntrattenimento();
            bool lExistsEventoSenzaPercentuale = false;
            List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oLista = new List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento>();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            if (Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.SetIncidenzaIntrattenimentoPerEvento(oConnection, IncidenzaIntrattenimentoEventi.Eventi, out oError))
                            {
                                oLista = Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.GetIncidenzaIntrattenimentoPerEvento(oConnection, oRet.InizioPeriodo, oRet.FinePeriodo, out lExistsEventoSenzaPercentuale, out oError);
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("SetIncidenzaIntrattenimento", "Errore imprevisto nella lettura degli eventi per verifica incidenza intrattenimento.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                oRet.ExistsEventoSenzaPercentuale = lExistsEventoSenzaPercentuale;
                oRet.Eventi = oLista.ToArray();
            }
            else
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(true, "SetIncidenzaIntrattenimento", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
            }
            oRet.InizioPeriodo = IncidenzaIntrattenimentoEventi.InizioPeriodo;
            oRet.FinePeriodo = IncidenzaIntrattenimentoEventi.FinePeriodo;

            return oRet;
        }

        #endregion

        #endregion

        #region "Operazioni"

        #region "Annulli"

        public class clsWSCausaleAnnullo
        {
            public string Codice = "";
            public string Descrizione = "";

            public clsWSCausaleAnnullo()
            { }

            public clsWSCausaleAnnullo(string codice, string descrizione)
            {
                this.Codice = codice;
                this.Descrizione = descrizione;
            }
        }

        public class clsWSCheckAnnullo
        {
            public string Carta = "";
            public string Sigillo = "";
            public long? Progressivo = 0;
            public long Error = 0;
            public string Descrizione = "";
            public List<string> Warnings = new List<string>();
            public List<clsWSCausaleAnnullo> Causali = new List<clsWSCausaleAnnullo>();
            public clsResultTransazione Transazione = null;

            public clsWSCheckAnnullo()
            {

            }
        }

        // classe di Controllo per annullo transazione
        public class ResultBaseCheckAnnulloTransazioni : ResultBase
        {
            public List<clsWSCheckAnnullo> CheckTransazioni = new List<clsWSCheckAnnullo>();

            public ResultBaseCheckAnnulloTransazioni()
            {
            }

            public ResultBaseCheckAnnulloTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
            }
        }

        // classe per annullo transazione
        //public class ResultBaseAnnulloTransazione : ResultBaseCheckAnnulloTransazioni
        //{
        //    public string CartaAnnullante = "";
        //    public string SigilloAnnullante = "";
        //    public Int64 ProgressivoAnnullante = 0;
        //    public clsResultTransazione TransazioneAnnullante = null;

        //    public ResultBaseAnnulloTransazione()
        //    {
        //    }

        //    public ResultBaseAnnulloTransazione(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceCarta, string cSigillo, Int64 nProgressivo, string cCodiceCartaAnnullante, string cSigilloAnnullante, Int64 nProgressivoAnnullante)
        //        : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, cCodiceCarta, cSigillo, nProgressivo)
        //    {
        //        this.CartaAnnullante = cCodiceCartaAnnullante;
        //        this.SigilloAnnullante = cSigilloAnnullante;
        //        this.ProgressivoAnnullante = nProgressivoAnnullante;
        //    }
        //}

        [WebMethod]
        public ResultBaseCheckAnnulloTransazioni CheckAnnulloTransazioni(string Token, string Carta, string Sigillo, Int64 primoProgressivo, Int64 ultimoProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cDescrizione = "";
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<Riepiloghi.clsCheckAnnulloTransazione> oListaCheckAnnulli = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);

                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ANNULLI", Token, out oError) || true)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oListaCheckAnnulli = Riepiloghi.clsRiepiloghi.CheckAnnulloTransazione(Carta, Sigillo, primoProgressivo, ultimoProgressivo, oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBaseCheckAnnulloTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                ResultBaseCheckAnnulloTransazioni oResult = new ResultBaseCheckAnnulloTransazioni(true, cDescrizione, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                if (oListaCheckAnnulli != null)
                {
                    foreach (Riepiloghi.clsCheckAnnulloTransazione item in oListaCheckAnnulli)
                    {
                        clsWSCheckAnnullo wsItem = new clsWSCheckAnnullo();
                        wsItem.Carta = item.Carta;
                        wsItem.Error = item.Error;
                        wsItem.Descrizione = item.Descrizione;
                        wsItem.Progressivo = item.Progressivo;
                        wsItem.Sigillo = item.Sigillo;
                        if (item.Transazione != null && item.Transazione.Rows != null && item.Transazione.Rows.Count > 0)
                            wsItem.Transazione = new clsResultTransazione(item.Transazione.Rows[0]);

                        if (item.Warnings != null && item.Warnings.Count > 0)
                        {
                            wsItem.Warnings = new List<string>();
                            foreach(KeyValuePair<long, string> itemWarning in item.Warnings)
                            {
                                wsItem.Warnings.Add(itemWarning.Value);
                            }
                        }

                        if (wsItem.Error == 0)
                        {
                            if (item.Causali != null && item.Causali.Count > 0)
                            {
                                wsItem.Causali = new List<clsWSCausaleAnnullo>();
                                foreach (KeyValuePair<string, string> itemCausale in item.Causali)
                                {
                                    wsItem.Causali.Add(new clsWSCausaleAnnullo(itemCausale.Key, itemCausale.Value));
                                }
                            }
                        }

                        oResult.CheckTransazioni.Add(wsItem);
                    }
                }
                return oResult;
            }
        }

        //[WebMethod]
        //public ResultBaseAnnulloTransazione AnnulloTransazione(string Token, string Carta, string Sigillo, Int64? Progressivo, string CausaleAnnullamento)
        //{
        //    ResultBaseAnnulloTransazione oResult = null;
        //    Riepiloghi.clsLogRequest oLogRequest = null;
        //    DateTime dStart = DateTime.Now;
        //    Exception oError = null;
        //    string cDescrizione = "";
        //    bool lWarning = false;
        //    Int64 nRet = 0;
        //    string CartaAnnullante = "";
        //    string SigilloAnnullante = "";
        //    Int64 ProgressivoAnnullante = 0;

        //    IConnection oConnectionInternal = null;
        //    IConnection oConnection = null;

        //    IRecordSet oRSTransazione = null;
        //    IRecordSet oRSTransazioneAnnullo = null;

        //    try
        //    {
        //        oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
        //        if (oError == null)
        //        {
        //            oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "AnnulloTransazione", Token, new Dictionary<string, object>() { { "Carta", Carta }, { "Sigillo", (Sigillo == null ? "" : Sigillo) }, { "Progressivo", (Progressivo == null ? "" : Progressivo.ToString()) } });

        //            if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ANNULLI", Token, out oError))
        //            {
        //                oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
        //                if (oError == null)
        //                {
        //                    //nRet = Riepiloghi.clsRiepiloghi.CheckAnnulloTransazione(Carta, Sigillo, Progressivo, Progressivo, oConnection, out oError, out cDescrizione, out lWarning, out oRSTransazione);

        //                    if (nRet == 0 || lWarning)
        //                    {
        //                        //if (Riepiloghi.clsRiepiloghi.AnnulloTransazione(Carta, Sigillo, Progressivo, CausaleAnnullamento, oConnection, out oError, out CartaAnnullante, out SigilloAnnullante, out ProgressivoAnnullante, out oRSTransazione, out oRSTransazioneAnnullo))
        //                        //{
        //                        //}
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
        //    }
        //    finally
        //    {
        //        if (oError != null)
        //        {
        //            //oResult = new ResultBaseAnnulloTransazione(false, oError.ToString(), nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo, "", "", 0);
        //        }
        //        else
        //        {
        //            //oResult = new ResultBaseAnnulloTransazione((nRet == 0 || lWarning), cDescrizione, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, Progressivo, CartaAnnullante, SigilloAnnullante, ProgressivoAnnullante);
        //            oResult.Transazione = new clsResultTransazione(oRSTransazione);
        //            oResult.TransazioneAnnullante = new clsResultTransazione(oRSTransazioneAnnullo);
        //        }
        //        if (oLogRequest != null)
        //        {
        //            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultAnnulloTransazione", oResult);
        //        }

        //        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
        //        if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
        //    }

        //    return oResult;
        //}

        #endregion
        
        #region "Generazione Riepilogo"

        // Classe per singola riga del riepilogo da generare
        public class clsResultBaseRiepilogoDaGenerare
        {
            public string GiornalieroMensile = "";
            public DateTime GiornoMese = DateTime.MinValue;
            public DateTime UltimaEmissione = DateTime.MinValue;
            public DateTime DataOraGenerazione = DateTime.MinValue;
            public string DescGenerazione = "";

            public clsResultBaseRiepilogoDaGenerare()
            {
            }

            public clsResultBaseRiepilogoDaGenerare(string cGiornalieroMensile, DateTime dGiornoMese, DateTime dUltimaEmissione, DateTime dDataOraGenerazione, string cDescGenerazione)
            {
                this.GiornalieroMensile = cGiornalieroMensile;
                this.GiornoMese = dGiornoMese;
                this.UltimaEmissione = dUltimaEmissione;
                this.DataOraGenerazione = dDataOraGenerazione;
                this.DescGenerazione = cDescGenerazione;
            }
        }

        // Risposta del servizio
        public class ResultBaseRiepiloghiDaGenerare : ResultBase
        {
            public clsResultBaseRiepilogoDaGenerare[] RiepiloghiDaGenerare = null;
            public System.Data.DataTable TableRiepiloghiDaGenerare = null;
            public string GiornalieroMensile = "";

            public ResultBaseRiepiloghiDaGenerare()
            {
            }

            public ResultBaseRiepiloghiDaGenerare(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string GiornalieroMensile, IRecordSet oRS, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.GiornalieroMensile = GiornalieroMensile;
                if (oRS != null && !oRS.EOF)
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[(int)oRS.RecordCount];
                    int Index = 0;
                    while (!oRS.EOF)
                    {
                        this.RiepiloghiDaGenerare[Index] = new clsResultBaseRiepilogoDaGenerare(oRS.Fields("giornaliero_mensile").Value.ToString(),
                                                                                                (oRS.Fields("giorno_mese").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("giorno_mese").Value),
                                                                                                (oRS.Fields("ultima_emissione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("ultima_emissione").Value),
                                                                                                (oRS.Fields("dataora_generazione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("dataora_generazione").Value),
                                                                                                oRS.Fields("desc_generazione").Value.ToString());
                        Index += 1;
                        oRS.MoveNext();
                    }
                    if (PreserveDataTable)
                    {
                        this.TableRiepiloghiDaGenerare = (System.Data.DataTable)oRS;
                        this.TableRiepiloghiDaGenerare.TableName = "RIEPILOGHI_DA_GENERARE";
                    }
                }
                else
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[0];
                }
            }
        }

        [WebMethod]
        public ResultBaseRiepiloghiDaGenerare GetRiepiloghiDaGenerare(string Token, string GiornalieroMensile, string PreserveDataTable)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseRiepiloghiDaGenerare oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_CREAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetRiepiloghiDaGenerare(oConnection, GiornalieroMensile, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura riepiloghi da generare", "Errore imprevisto durante la lettura dei riepiloghi da generare.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        public class ResultBaseGenerazioneRiepilogo : ResultBase
        {

            public string GiornalieroMensile = "";
            public DateTime GiornoMese = DateTime.MinValue;
            public DateTime DataOraGenerazione = DateTime.MinValue;
            public DateTime DataOraSpedizione = DateTime.MinValue;
            public DateTime DataOraMasterizzazione = DateTime.MinValue;

            public string FileLogFirmato = "";
            public string FileRiepilogo = "";
            public string FileEmail = "";
            public Int64 Progressivo = 0;

            public string FileLogNonFirmato = "";
            public string FileRiepilogoNonFirmato = "";
            public byte[] BytesLogNonFirmato = new byte[] { };
            public byte[] BytesRiepilogoNonFirmato = new byte[] { };
            public bool AsyncMethod = false;
            public string IpSocket = "";
            public int PortSocket = 0;

            public ResultBaseGenerazioneRiepilogo()
            {
            }

            public ResultBaseGenerazioneRiepilogo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string GiornalieroMensile, DateTime GiornoMese, string cFileLogFirmato, string cFileRiepilogo, string cFileEmail, Int64 Progressivo) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.GiornalieroMensile = GiornalieroMensile;
                this.GiornoMese = GiornoMese;
                this.FileLogFirmato = cFileLogFirmato;
                this.FileRiepilogo = cFileRiepilogo;
                this.FileEmail = cFileEmail;
                this.Progressivo = Progressivo;
            }
        }

        public class clsRiepilogoGenerato
        {
            public string GiornalieroMensile = "";
            public DateTime GiornoMese = DateTime.MinValue;
            public DateTime DataOraGenerazione = DateTime.MinValue;
            public DateTime DataOraSpedizione = DateTime.MinValue;
            public int StatoSpedizione = 0;
            public string DescrizioneStatoSpedizione = "Non spedito";
            public DateTime DataOraMasterizzazione = DateTime.MinValue;
            public int StatoMasterizzazione = 0;

            public string FileLogFirmato = "";
            public string FileRiepilogo = "";
            public string FileEmail = "";
            public Int64 Progressivo = 0;

            public string FileLogNonFirmato = "";
            public string FileRiepilogoNonFirmato = "";
            public byte[] BytesLogNonFirmato = new byte[] { };
            public byte[] BytesRiepilogoNonFirmato = new byte[] { };

            public clsRiepilogoGenerato()
            {

            }

            public clsRiepilogoGenerato(string GiornalieroMensile, DateTime GiornoMese, string cFileLogFirmato, string cFileRiepilogo, string cFileEmail, Int64 Progressivo, DateTime DataOraGenerazione, DateTime DataOraSpedizione, DateTime DataOraMasterizzazione)
            {
                this.GiornalieroMensile = GiornalieroMensile;
                this.GiornoMese = GiornoMese;
                this.Progressivo = Progressivo;
                this.FileLogFirmato = cFileLogFirmato;
                this.FileRiepilogo = cFileRiepilogo;
                this.FileEmail = cFileEmail;
                this.DataOraGenerazione = DataOraGenerazione;
                this.DataOraSpedizione = DataOraSpedizione;
                this.DataOraMasterizzazione = DataOraMasterizzazione;
            }

        }

        public class ResultBaseRiepiloghiGenerati : ResultBase
        {
            public clsRiepilogoGenerato[] RiepiloghiGenerati = null;

            public ResultBaseRiepiloghiGenerati()
            {
            }

            public ResultBaseRiepiloghiGenerati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && !oRS.EOF)
                {
                    this.RiepiloghiGenerati = new clsRiepilogoGenerato[(int)oRS.RecordCount];
                    int Index = 0;
                    while (!oRS.EOF)
                    {
                        this.RiepiloghiGenerati[Index] = new clsRiepilogoGenerato(oRS.Fields("TIPO").Value.ToString(),
                                                                                  (oRS.Fields("GIORNO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("GIORNO").Value),
                                                                                  "",
                                                                                  oRS.Fields("NOME").Value.ToString(),
                                                                                  "",
                                                                                  Int64.Parse(oRS.Fields("PROG").Value.ToString()), 
                                                                                  (oRS.Fields("DATA_GENERAZIONE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_GENERAZIONE").Value),
                                                                                  (oRS.Fields("MAIL_DATE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("MAIL_DATE").Value),
                                                                                  DateTime.MinValue);
                        Index += 1;
                        oRS.MoveNext();
                    }
                }
                else
                {
                    this.RiepiloghiGenerati = new clsRiepilogoGenerato[0];
                }
            }

        }

        // Generazione del riepilogo con passaggio data in formato stringa yyyyMMdd
        [WebMethod]
        public ResultBaseGenerazioneRiepilogo CreaRiepilogoDateString(string Token, string Giorno, string GiornalieroMensile)
        {
            DateTime dStart = DateTime.Now;
            DateTime dGiorno;
            if (DateTime.TryParseExact(Giorno, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out dGiorno))
            {

                return CreaRiepilogo(Token, dGiorno, GiornalieroMensile);
            }
            else
            {
                Exception oError = new Exception("Formato data non supporato.");
                return (new ResultBaseGenerazioneRiepilogo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, DateTime.MinValue, "", "", "", 0));
            }
        }

        private bool receivedRequestCreazioneRiepilogo = false;
        private Riepiloghi.clsWinServiceModel_response_Operation responseCreazioneRiepiloghi = null;

        // Generazione del riepilogo con passaggio data in formato data
        [WebMethod]
        public ResultBaseGenerazioneRiepilogo CreaRiepilogo(string Token, DateTime Giorno, string GiornalieroMensile)
        {
            this.receivedRequestCreazioneRiepilogo = false;
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            string cFileLogFirmato = "";
            string cFileRiepilogoFirmato = "";
            string cFileEmailFirmato = "";
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            string PathXslt = HttpContext.Current.Server.MapPath("RisorseWeb");
            Int64 Progressivo = 0;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreaRiepilogo", Token, new Dictionary<string, object>() { { "Giorno", Giorno }, { "GiornalieroMensile", GiornalieroMensile } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_CREAZIONE", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_CREAZIONE", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            string cSocketIP = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "WINSERVICE_SOCKET_LISTENER_IP", "");
                            string cSocketPort = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "WINSERVICE_SOCKET_LISTENER_PORT", "");
                            int nSocketPort = 0;

                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "cSocketIP", cSocketIP);
                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "cSocketPort", cSocketPort);

                            if (cSocketIP != null && cSocketIP.Trim() != "" && cSocketPort != null && cSocketPort.Trim() != "" && int.TryParse(cSocketPort, out nSocketPort) && nSocketPort > 0)
                            {
                                Riepiloghi.SocketClientRiepiloghi oClient = new Riepiloghi.SocketClientRiepiloghi(cSocketIP, nSocketPort);
                                oClient.OnReceived += OClient_OnReceived;
                                lRet = oClient.Open();
                                string socketToken = "";
                                if (lRet)
                                {
                                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                    //socketToken = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.AccountId, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, out oError);
                                    lRet = (oError == null && !string.IsNullOrEmpty(socketToken));
                                }

                                if (lRet)
                                {
                                    Riepiloghi.clsWinServiceModel_request_Operation requestCreaRiepilogo = new Riepiloghi.clsWinServiceModel_request_Operation();
                                    requestCreaRiepilogo.Code = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO;
                                    requestCreaRiepilogo.Token = socketToken;
                                    requestCreaRiepilogo.RequestRiepilogo = new Riepiloghi.clsWinServiceModel_request_Riepilogo();
                                    requestCreaRiepilogo.RequestRiepilogo.GiornalieroMensile = GiornalieroMensile;
                                    requestCreaRiepilogo.RequestRiepilogo.Giorno = Giorno.Date;

                                    string cRequest = Newtonsoft.Json.JsonConvert.SerializeObject(requestCreaRiepilogo);
                                    lRet = oClient.Send(cRequest);
                                    if (lRet)
                                    {
                                        lRet = false;
                                        while (!this.receivedRequestCreazioneRiepilogo)
                                        {
                                            System.Threading.Thread.Sleep(10);
                                        }
                                        //lRet = oClient.ListDataReceived.Contains("<Riepilogo Message='Avvio generazione riepilogo...'/>");
                                        lRet = this.responseCreazioneRiepiloghi != null;
                                    }
                                    oClient.Close();
                                }
                                oClient.OnReceived -= OClient_OnReceived;

                                if (lRet)
                                {
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, "Generazione del Riepilogo in corso", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                                    oResult.AsyncMethod = true;
                                    oResult.IpSocket = cSocketIP;
                                    oResult.PortSocket = nSocketPort;
                                }
                                else
                                {
                                    lRet = false;
                                    if (oClient.Error != null)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al Server Socket", oClient.Error.Message);
                                    }
                                    else
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al Server Socket", "Errore di connessione al Server Socket");
                                    }
                                    
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                                    oClient.Close();
                                }
                            }
                            else
                            {
                                //lRet = Riepiloghi.clsRiepiloghi.CreaRiepilogo(Giorno, GiornalieroMensile, oConnection, PathXslt, out oError, out cFileLogFirmato, out cFileRiepilogoFirmato, out cFileEmailFirmato);
                                if (lRet)
                                {
                                    Progressivo = Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError);
                                }

                                if (lRet && oError == null)
                                {
                                    string cDesc = "Riepilogo " + (GiornalieroMensile == "G" ? "Giornaliero" : "Mensile") + " generato correttamente:" +
                                                   (cFileLogFirmato.Trim() == "" ? "" : "\r\n" + "File di log " + cFileLogFirmato) +
                                                   (cFileRiepilogoFirmato.Trim() == "" ? "" : "\r\n" + "Riepilogo " + cFileRiepilogoFirmato) +
                                                   (cFileEmailFirmato.Trim() == "" ? "" : "\r\n" + "Mail " + cFileEmailFirmato);
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, cDesc, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                                }
                                else
                                {
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                                }
                            }
                        }
                        else
                        {
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                    }
                }
                else
                {
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, Giorno, cFileLogFirmato, cFileRiepilogoFirmato, cFileEmailFirmato, Progressivo);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        private void OClient_OnReceived(Riepiloghi.SocketClientRiepiloghi socket, string data)
        {
            try
            {
                this.responseCreazioneRiepiloghi = Newtonsoft.Json.JsonConvert.DeserializeObject<Riepiloghi.clsWinServiceModel_response_Operation>(data);
                this.receivedRequestCreazioneRiepilogo = true;
            }
            catch (Exception)
            {
            }
        }

        //private void OnConnectToSocketServer(IAsyncResult ar)
        //{
        //    clsSocketClient sock = (clsSocketClient)ar.AsyncState;
        //    try
        //    {
        //        if (sock.Connected)
        //        {
        //            sock.StatoSocketCreazioneRiepiloghi = 1;
        //            SetupRecieveCallback(sock);
        //        }
        //        else
        //        {
        //            sock.StatoSocketCreazioneRiepiloghi = -1;
        //            sock.Error = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al server socket", "Errore di connessione");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Invoke(m_AddMessage, new string[] { "Unusual error during Connect!" + "\r\n" + ex.Message });
        //    }
        //}

        //private void SetupRecieveCallback(clsSocketClient sock)
        //{
        //    try
        //    {
        //        sock.AsyncCallback_OnReceiveData = new AsyncCallback(OnReceiveDataFromSocketServer);
        //        sock.BeginReceive(sock.Buffer, 0, sock.Buffer.Length, System.Net.Sockets.SocketFlags.None, sock.AsyncCallback_OnReceiveData, sock);
        //    }
        //    catch (Exception ex)
        //    {
        //        sock.StatoSocketCreazioneRiepiloghi = -1;
        //        sock.Error = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al server socket", "Errore di connessione");
        //    }
        //}

        //public void OnReceiveDataFromSocketServer(IAsyncResult ar)
        //{
        //    clsSocketClient sock = (clsSocketClient)ar.AsyncState;
        //    try
        //    {
        //        int nBytesRec = sock.EndReceive(ar);
        //        if (nBytesRec > 0)
        //        {
        //            string sRecieved = Encoding.ASCII.GetString(sock.Buffer, 0, nBytesRec);
        //            SetupRecieveCallback(sock);
        //            try
        //            {
        //                System.Xml.XmlDocument oDoc = new System.Xml.XmlDocument();
        //                oDoc.LoadXml(sRecieved);
        //                foreach (System.Xml.XmlNode oNode in oDoc.ChildNodes)
        //                {
        //                    foreach (System.Xml.XmlAttribute oAttribute in oNode.Attributes)
        //                    {
        //                        if (oAttribute.Name == "Message")
        //                        {
        //                            sock.Message = oAttribute.Value.ToString();
        //                        }
        //                        if (oAttribute.Name == "Status" && oAttribute.Value.ToString() == "READY")
        //                        {
        //                            sock.StatoSocketCreazioneRiepiloghi = 1;
        //                            string cRequest = string.Format("<Riepilogo GiornalieroMensile='{0}' Giorno='{1}' User='{2}' Password='{3}'/>", new object[] { sock.GiornalieroMensile, sock.Giorno.ToString("yyyyMMdd"), "crea", "crea" });
        //                            sock.Send(cRequest);
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }
        //        }
        //        else
        //        {
        //            // If no data was recieved then the connection is probably dead
        //            sock.StatoSocketCreazioneRiepiloghi = -1;
        //            sock.Error = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al server socket", "Connessione interrotta");
        //            sock.Shutdown(System.Net.Sockets.SocketShutdown.Both);
        //            sock.Close();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        public class ResultBaseSingoloRiepilogoGenerato : ResultBase
        {
            public clsRiepilogoGenerato RiepilogoGenerato = null;

            public ResultBaseSingoloRiepilogoGenerato()
            {
                this.RiepilogoGenerato = new clsRiepilogoGenerato();
                this.RiepilogoGenerato.BytesLogNonFirmato = new byte[] { };
                this.RiepilogoGenerato.BytesRiepilogoNonFirmato = new byte[] { };
                this.RiepilogoGenerato.DataOraGenerazione = DateTime.MinValue;
                this.RiepilogoGenerato.DataOraMasterizzazione = DateTime.MinValue;
                this.RiepilogoGenerato.DataOraSpedizione = DateTime.MinValue;
                this.RiepilogoGenerato.DescrizioneStatoSpedizione = "";
                this.RiepilogoGenerato.FileEmail = "";
                this.RiepilogoGenerato.FileLogFirmato = "";
                this.RiepilogoGenerato.FileLogNonFirmato = "";
                this.RiepilogoGenerato.FileRiepilogo = "";
                this.RiepilogoGenerato.FileRiepilogoNonFirmato = "";
                this.RiepilogoGenerato.GiornalieroMensile = "";
                this.RiepilogoGenerato.GiornoMese = DateTime.MinValue;
                this.RiepilogoGenerato.Progressivo = 0;
                this.RiepilogoGenerato.StatoMasterizzazione = 0;
                this.RiepilogoGenerato.StatoSpedizione = 0;
            }

            public ResultBaseSingoloRiepilogoGenerato(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.RiepilogoGenerato = new clsRiepilogoGenerato();
                this.RiepilogoGenerato.BytesLogNonFirmato = new byte[] { };
                this.RiepilogoGenerato.BytesRiepilogoNonFirmato = new byte[] { };
                this.RiepilogoGenerato.DataOraGenerazione = DateTime.MinValue;
                this.RiepilogoGenerato.DataOraMasterizzazione = DateTime.MinValue;
                this.RiepilogoGenerato.DataOraSpedizione = DateTime.MinValue;
                this.RiepilogoGenerato.DescrizioneStatoSpedizione = "";
                this.RiepilogoGenerato.FileEmail = "";
                this.RiepilogoGenerato.FileLogFirmato = "";
                this.RiepilogoGenerato.FileLogNonFirmato = "";
                this.RiepilogoGenerato.FileRiepilogo = "";
                this.RiepilogoGenerato.FileRiepilogoNonFirmato = "";
                this.RiepilogoGenerato.GiornalieroMensile = "";
                this.RiepilogoGenerato.GiornoMese = DateTime.MinValue;
                this.RiepilogoGenerato.Progressivo = 0;
                this.RiepilogoGenerato.StatoMasterizzazione = 0;
                this.RiepilogoGenerato.StatoSpedizione = 0;
            }
        }

        [WebMethod]
        public ResultBaseSingoloRiepilogoGenerato GetRiepilogoGenerato(string Token, DateTime Giorno, string GiornalieroMensile)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseSingoloRiepilogoGenerato oRet = new ResultBaseSingoloRiepilogoGenerato();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            oRet.RiepilogoGenerato.GiornalieroMensile = GiornalieroMensile;
            oRet.RiepilogoGenerato.GiornoMese = Giorno;
            oRet.RiepilogoGenerato.Progressivo = 0;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "LISTA_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            long nProgRiepilogo = Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError);
                            if (oError == null)
                            {
                                oRet.RiepilogoGenerato.Progressivo = nProgRiepilogo;
                                StringBuilder oSB = new StringBuilder();
                                oSB.Append("SELECT * FROM RIEPILOGHI WHERE GIORNO = :pGIORNO AND TIPO = :pGIORNALIERO_MENSILE AND PROG = :pPROG_RIEPILOGO");
                                clsParameters oPars = new clsParameters();
                                oPars.Add(":pGIORNO", Giorno.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
                                oPars.Add(":pPROG_RIEPILOGO", nProgRiepilogo);

                                IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), oPars);
                                if (!oRS.EOF)
                                {
                                    oRet.RiepilogoGenerato = new clsRiepilogoGenerato(oRS.Fields("TIPO").Value.ToString(),
                                                                                     (oRS.Fields("GIORNO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("GIORNO").Value),
                                                                                     "",
                                                                                     oRS.Fields("NOME").Value.ToString(),
                                                                                     "",
                                                                                     Int64.Parse(oRS.Fields("PROG").Value.ToString()),
                                                                                     (oRS.Fields("DATA_GENERAZIONE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_GENERAZIONE").Value),
                                                                                     (oRS.Fields("MAIL_DATE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("MAIL_DATE").Value),
                                                                                     DateTime.MinValue);
                                }
                                oRS.Close();
                            }
                            else
                            {
                                oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            }
                        }
                        else
                        {
                            oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        }
                    }
                    else
                    {
                        oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    }
                }
                else
                {
                    oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("lISTA rIEPILOGHI", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseRiepiloghiGenerati GetRiepiloghiGenerati(string Token)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseRiepiloghiGenerati oRet = new ResultBaseRiepiloghiGenerati();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "LISTA_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT * FROM RIEPILOGHI ORDER BY DATA_INS DESC");
                            oRet = new ResultBaseRiepiloghiGenerati(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
                        }
                        else
                        {
                            oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        }
                    }
                    else
                    {
                        oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    }
                }
                else
                {
                    oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Riepiloghi", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseOperazioneInCorso : ResultBase
        {
            public Riepiloghi.clsSequenzaCreazioneRiepilogo SequenzaOperazioneInCorso = new Riepiloghi.clsSequenzaCreazioneRiepilogo();

            public ResultBaseOperazioneInCorso()
            {
            }

            public ResultBaseOperazioneInCorso(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
            }
        }

        [WebMethod]
        public ResultBaseOperazioneInCorso GetOperazioneInCorso(string Token)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseOperazioneInCorso oRet = new ResultBaseOperazioneInCorso();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CHECK_OPERAZIONE_IN_CORSO", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oRet.SequenzaOperazioneInCorso = Riepiloghi.clsRiepiloghi.SequenzaCreazioneRiepilogo(oConnection, null, 0, 0, true, false);
                        }
                        else
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        }
                    }
                    else
                    {
                            oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    }
                }
                else
                {
                        oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Operazione In Corso", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        [WebMethod]
        public ResultBaseOperazioneInCorso GetIdOperazioneInCorso(string Token, Int64 IdOperazione)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseOperazioneInCorso oRet = new ResultBaseOperazioneInCorso();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CHECK_OPERAZIONE_IN_CORSO", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                        if (oError == null)
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oRet.SequenzaOperazioneInCorso = Riepiloghi.clsRiepiloghi.SequenzaCreazioneRiepilogo(oConnection, null, IdOperazione, 0, true, true);
                        }
                        else
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        }
                    }
                    else
                    {
                        oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    }
                }
                else
                {
                    oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Operazione In Corso", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }



        #endregion

        #region "Debug"

        private ResultBase resultDebugOperation = null;

        [WebMethod]
        public ResultBase DebugOperation(string Token)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBase oResult = new ResultBase();

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "DebugOperation", Token, null);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "DEBUG_OPERATION", Token, out oError) || true)
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "DEBUG_OPERATION", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            string cSocketIP = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "WINSERVICE_SOCKET_LISTENER_IP", "");
                            string cSocketPort = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "WINSERVICE_SOCKET_LISTENER_PORT", "");
                            int nSocketPort = 0;

                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "cSocketIP", cSocketIP);
                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "cSocketPort", cSocketPort);

                            if (cSocketIP != null && cSocketIP.Trim() != "" && cSocketPort != null && cSocketPort.Trim() != "" && int.TryParse(cSocketPort, out nSocketPort) && nSocketPort > 0)
                            {
                                Riepiloghi.SocketClientRiepiloghi oClient = new Riepiloghi.SocketClientRiepiloghi(cSocketIP, nSocketPort);
                                oClient.OnReceived += OClientDebug_OnReceived;
                                lRet = oClient.Open();
                                string socketToken = "";
                                if (lRet)
                                {
                                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                    //socketToken = Riepiloghi.clsSecuritySocket.PushSocketTokenOperazione(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.AccountId, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DEBUG, out oError);
                                    lRet = (oError == null && !string.IsNullOrEmpty(socketToken));
                                }

                                if (lRet)
                                {
                                    //Riepiloghi.clsWinServiceModel_request_Operation requestDebug = new Riepiloghi.clsWinServiceModel_request_Operation();
                                    //requestDebug.Code = Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DEBUG;
                                    //requestDebug.Token = socketToken;
                                    //string cRequest = Newtonsoft.Json.JsonConvert.SerializeObject(requestDebug);
                                    //this.resultDebugOperation = null;
                                    //lRet = oClient.Send(cRequest);
                                    //if (lRet)
                                    //{
                                    //    lRet = false;
                                    //    while (this.resultDebugOperation == null)
                                    //    {
                                    //        System.Threading.Thread.Sleep(10);
                                    //    }
                                    //    //lRet = oClient.ListDataReceived.Contains("<Riepilogo Message='Avvio generazione riepilogo...'/>");
                                    //    lRet = this.resultDebugOperation != null;
                                    //}
                                    oClient.Close();
                                }
                                oClient.OnReceived -= OClientDebug_OnReceived;

                                if (lRet)
                                {
                                    oResult = new ResultBase(lRet, this.resultDebugOperation.StatusMessage, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                }
                                else
                                {
                                    lRet = false;
                                    if (oClient.Error != null)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al Server Socket", oClient.Error.Message);
                                    }
                                    else
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione al Server Socket", "Errore di connessione al Server Socket");
                                    }

                                    oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                    oClient.Close();
                                }
                            }
                        }
                        else
                        {
                            oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        }
                    }
                    else
                    {
                        oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    }
                }
                else
                {
                    oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
                oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        private void OClientDebug_OnReceived(Riepiloghi.SocketClientRiepiloghi socket, string data)
        {
            try
            {
                Riepiloghi.clsWinServiceModel_response_Operation response = Newtonsoft.Json.JsonConvert.DeserializeObject<Riepiloghi.clsWinServiceModel_response_Operation>(data);
                this.resultDebugOperation = new ResultBase(true, response.Message, 0, "");
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #endregion

        #region "Bordero"

        public class ResultItemInfoBordero
        {
            public DateTime GiornoCompetenza { get; set; }
            public List<string> Descrizioni { get; set; }
            public string StringId { get; set; }
            public ResultItemInfoBordero()
            {
                this.GiornoCompetenza = DateTime.MinValue;
                this.Descrizioni = new List<string>();
                this.StringId = "";
            }

            public ResultItemInfoBordero(DateTime giornoCompetenza, Dictionary<string, string> infoFiltri, string stringId)
            {
                this.GiornoCompetenza = giornoCompetenza;
                this.Descrizioni = new List<string>();
                foreach (KeyValuePair<string, string> item in infoFiltri)
                {
                    this.Descrizioni.Add(item.Value);
                }
                this.StringId = stringId;
            }
        }

        public class ResultBaseBorderoDati : ResultBase
        {
            public DateTime DataOraInizio;
            public DateTime DataOraFine;
            public List<ResultItemInfoBordero> ListaBordero { get; set; }


            public ResultBaseBorderoDati() :base()
            {
            }

            public ResultBaseBorderoDati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataOraInizio, DateTime dDataOraFine) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.DataOraInizio = dDataOraInizio;
                this.DataOraFine = dDataOraFine;
                this.ListaBordero = new List<ResultItemInfoBordero>();
            }
        }

        [WebMethod]
        public ResultBaseBorderoDati CalcBordero(string Token, DateTime dDataOraInizio, DateTime dDataOraFine, bool lFlagGrpDataEve, bool lFlagGrpOrg, bool lFlagGrpNol, bool lFlagGrpPro, bool lFlagGrpTev, bool lFlagGrpSal, bool lFlagGrpTit)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoDati oResult = null;
            List<ResultItemInfoBordero> lista = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                string fontPath = HttpContext.Current.Server.MapPath("//RisorseWeb//Font");
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CALC_BORDERO", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            Int64 nIdUxc = 0;
                            if (oError == null)
                            {
                                // prendo nIdUxc
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                                if (oError == null && nIdUxc > 0)
                                {
                                    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                    if (oError == null)
                                    {
                                        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                        List<string> codiciLocaliCorrenti = new List<string>();
                                        List<string> cfOrganizzatoriCorrenti = new List<string>();
                                        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                        {
                                            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                oCodiceSistemaCorrente = oItemCodiceSistema;
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCodiciLocali ||  oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }

                                                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                        {

                                        }
                                    }
                                }
                            }

                            Int64 IdRichiestaBordero = 0;
                            List<libBordero.Bordero.clsInfoDataSet> listaDati = null;
                            if (oError == null)
                            {
                                libBordero.Bordero.DeleteDatiRichiestaBorderoFull(oConnection, false);
                                oConnection.BeginTransaction();
                                try
                                {
                                    System.Data.DataSet oDataSetBordero = libBordero.Bordero.GetBordero(oConnection, nIdUxc, dDataOraInizio, dDataOraFine, ref IdRichiestaBordero, out oError);
                                    if (oError == null)
                                    {
                                        List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();
                                        libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);

                                        string cTipoBorderoMultiplo = "";  // suddivisione minima per data evento

                                        cTipoBorderoMultiplo += (lFlagGrpOrg ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_ORGANIZZATORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpNol ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_NOLEGGIATORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpPro ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_PRODUTTORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpTev ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_TIPI_EVENTO" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpSal ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_SALE" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpTit ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_TITOLI" : "");

                                        if (cTipoBorderoMultiplo.Trim() == "" && lFlagGrpDataEve)
                                        {
                                            cTipoBorderoMultiplo = "BORDERO_FILT_GIORNI"; // minima suddivisione per data evento
                                        }
                                        else if (cTipoBorderoMultiplo.Trim() != "" && lFlagGrpDataEve)
                                        {
                                            cTipoBorderoMultiplo += ",BORDERO_FILT_GIORNI";
                                        }

                                        listaDati = libBordero.Bordero.MultiBorderoStream(cTipoBorderoMultiplo, dDataOraInizio, dDataOraFine, oDataSetBordero, fontPath);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
                                }

                                oConnection.RollBack();
                                if (oError == null && IdRichiestaBordero > 0 && listaDati != null && listaDati.Count > 0)
                                {
                                    List<libBordero.Bordero.clsInfoDataSet> oListaFinale = libBordero.Bordero.SaveTokenPdfGenerated(oConnection, nIdUxc, dDataOraInizio, dDataOraFine, IdRichiestaBordero, listaDati, out oError);
                                    if (oListaFinale != null && oListaFinale.Count > 0)
                                    {
                                        lista = new List<ResultItemInfoBordero>();
                                        foreach(libBordero.Bordero.clsInfoDataSet item in oListaFinale)
                                        {
                                            lista.Add(new ResultItemInfoBordero(item.GiornoCompetenza, item.InfoFiltri, item.StringId));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoDati(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
                oResult.ListaBordero = lista;
            }
            else
            {
                oResult = new ResultBaseBorderoDati(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
            }
            
            return oResult;
        }


        public class ResultBaseBorderoConfig : ResultBase
        {
            public libBordero.Bordero.clsBordQuoteConfig Configurazione { get; set; }

            public ResultBaseBorderoConfig():base()
            { }
            public ResultBaseBorderoConfig(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libBordero.Bordero.clsBordQuoteConfig configurazione) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Configurazione = configurazione;
            }
        }

        public class ResultBaseBorderoSearch : ResultBase
        {
            public libBordero.Bordero.clsSearchEvento RichiestaEventi { get; set; }
            public ResultBaseBorderoSearch():base()
            { }

            public ResultBaseBorderoSearch(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libBordero.Bordero.clsSearchEvento searchEventi)
            {
                this.RichiestaEventi = searchEventi;
            }
        }

        [WebMethod]
        public ResultBaseBorderoConfig GetConfigBordero(string Token)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsBordQuoteConfig Configurazione = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONF_BORDERO", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            Int64 nIdUxc = 0;
                            if (oError == null)
                            {
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                                if (oError == null && nIdUxc > 0)
                                {
                                    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                    if (oError == null)
                                    {
                                        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                        List<string> codiciLocaliCorrenti = new List<string>();
                                        List<string> cfOrganizzatoriCorrenti = new List<string>();
                                        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                        {
                                            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                oCodiceSistemaCorrente = oItemCodiceSistema;
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }

                                                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                        {

                                        }
                                        else
                                        {
                                            Configurazione = libBordero.Bordero.GetConfigurazione(oConnection, nIdUxc);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }
            else
            {
                oResult = new ResultBaseBorderoConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }

            return oResult;
        }

        [WebMethod]
        public ResultBaseBorderoConfig SetConfigBordero(string Token, ResultBaseBorderoConfig data)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsBordQuoteConfig Configurazione = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONF_BORDERO", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            Int64 nIdUxc = 0;
                            if (oError == null)
                            {
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                                if (oError == null && nIdUxc > 0)
                                {
                                    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                    if (oError == null)
                                    {
                                        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                        List<string> codiciLocaliCorrenti = new List<string>();
                                        List<string> cfOrganizzatoriCorrenti = new List<string>();
                                        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                        {
                                            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                oCodiceSistemaCorrente = oItemCodiceSistema;
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }

                                                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                        {

                                        }
                                        else
                                        {
                                            Configurazione = libBordero.Bordero.SetConfigurazione(oConnection, nIdUxc, data.Configurazione);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }
            else
            {
                oResult = new ResultBaseBorderoConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }

            return oResult;
        }

        [WebMethod]
        public ResultBaseBorderoSearch BorderoSearchEvento(string Token, ResultBaseBorderoSearch data)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoSearch oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsSearchEvento Search = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONF_BORDERO", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError);
                            Int64 nIdUxc = 0;
                            if (oError == null)
                            {
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                                if (oError == null && nIdUxc > 0)
                                {
                                    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                    if (oError == null)
                                    {
                                        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                        List<string> codiciLocaliCorrenti = new List<string>();
                                        List<string> cfOrganizzatoriCorrenti = new List<string>();
                                        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                        {
                                            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                oCodiceSistemaCorrente = oItemCodiceSistema;
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }

                                                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                        {

                                        }
                                        else
                                        {
                                            Search = new libBordero.Bordero.clsSearchEvento();
                                            Search.TipoRicerca = data.RichiestaEventi.TipoRicerca;
                                            Search.CodiceLocale = data.RichiestaEventi.CodiceLocale;
                                            Search.InizioDataEvento = data.RichiestaEventi.InizioDataEvento;
                                            Search.FineDataEvento = data.RichiestaEventi.FineDataEvento;
                                            Search.FlagCodiceLocale = data.RichiestaEventi.FlagCodiceLocale;
                                            Search.FlagDataEvento = data.RichiestaEventi.FlagDataEvento;
                                            Search.FlagTitolo = data.RichiestaEventi.FlagTitolo;
                                            Search.Titolo = data.RichiestaEventi.Titolo;
                                            Search.Eventi = libBordero.Bordero.RicercaEventi(oConnection, nIdUxc, Search);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Ricerca evento per configurazione Bordero", "Errore imprevisto durante la ricerca.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoSearch(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Search);
            }
            else
            {
                oResult = new ResultBaseBorderoSearch(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Search);
            }

            return oResult;
        }

        #endregion

        //Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo
        //Riepiloghi.clsRiepiloghi.GetNomeEmailGiorno
        //Riepiloghi.clsRiepiloghi.GetNomeEmailGiornoBase
        //Riepiloghi.clsRiepiloghi.GetNomeLogGiorno
        //Riepiloghi.clsRiepiloghi.GetNomeLogGiornoBase
        //Riepiloghi.clsRiepiloghi.GetNomeLogSearchIncrementale
        //Riepiloghi.clsRiepiloghi.GetNomeRiepilogo
        //Riepiloghi.clsRiepiloghi.GetNomeRiepilogoBase
        //Riepiloghi.clsRiepiloghi.CreaRiepilogo
        //Riepiloghi.clsRiepiloghi.Master_EseguiMasterizzazione
        //Riepiloghi.clsRiepiloghi.SpedizioneMail

        //Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi

        //Riepiloghi.clsRiepiloghi.PrintRiepilogoToPDF
        //Riepiloghi.clsRiepiloghi.PrintToPDF_SimulazioneMensile
        //Riepiloghi.clsRiepiloghi.PrintTransazioneCARTA_SIGILLO_PROGRESSIVO
        //Riepiloghi.clsRiepiloghi.PrintTransazioni
    }
}