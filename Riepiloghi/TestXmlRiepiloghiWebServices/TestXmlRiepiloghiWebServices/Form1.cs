﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using libSiae;

namespace TestXmlRiepiloghiWebServices
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //this.button1.Click += new EventHandler(button1_Click);
            WSRiepiloghi.ServiceRiepiloghi oService = new WSRiepiloghi.ServiceRiepiloghi();
            WSRiepiloghi.ResultBaseIncidenzaIntrattenimento oResult = oService.CheckIncidenzaIntrattenimento("WIN10X64SERVERORACLE", "", new DateTime(2016, 1, 1), new DateTime(2016, 1, 1));
        }

        //void button1_Click(object sender, EventArgs e)
        //{
        //    System.Net.HttpWebRequest oRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create("http://localhost:50828/RiepiloghiPdfHandler.ashx?type=rpd&giorno=26-02-2014");
        //    try
        //    {
        //        WSRiepiloghi.ServiceRiepiloghi oService = new WSRiepiloghi.ServiceRiepiloghi();
        //        WSRiepiloghi.ResultBaseCodiceSistema oResult = oService.GetCodiceSistema("serverx", "", "");
        //        this.textBox1.Text = oResult.CodiceSistema;
        //        WSRiepiloghi.ResultTransazionePeriodo oResultTransazioniPeriodo = oService.GetTransazioni("serverx", "crea", "crea", new DateTime(2015, 1, 1), new DateTime(2015, 12, 31), "TRUE", "FALSE", 0);
        //        this.textBox1.Text = oResultTransazioniPeriodo.statusmessage;
        //    }
        //    catch (System.Net.WebException exWeb)
        //    {
        //        this.textBox1.Text = exWeb.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.textBox1.Text = ex.ToString();
        //    }
        //}

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    this.Test();
        //}

        //private void Test()
        //{
        //    string ServiceName = "serverx";
        //    string User = "crea";
        //    string Password = "crea";
        //    string GiornalieroMensile = "G";
        //    string Giorno = "20150226";
        //    Esegui(ServiceName, User, Password, Giorno, GiornalieroMensile);
        //}

        //private bool Esegui(string ServiceName, string User, string Password, string Giorno, string GiornalieroMensile)
        //{
        //    bool lRet = false;
        //    DateTime dGiorno = DateTime.MinValue;
        //    Int64 nProgressivo = 0;

        //    int nSlotCarta = 0;
        //    bool lCartaInLocale = false;
        //    bool lDatiRiepilogoCreati = false;
        //    libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale = null;
        //    bool lCartaRichiestaAlServerFiscale = false;
        //    List<string> oFileDaCancellare = new List<string>();

        //    Exception oError = null;
        //    string cHeaderError = "Generazione Riepilogo";

        //    WSRiepiloghi.ServiceRiepiloghi oService = null;
        //    lRet = DateTime.TryParseExact(Giorno, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out dGiorno);

        //    if (lRet)
        //    {
        //        try
        //        {
        //            string cPathRisorse = (new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)).DirectoryName;
        //            string cPathTemp = cPathRisorse + "\\temp";

        //            oService = new WSRiepiloghi.ServiceRiepiloghi();

        //            string cCodiceSistema = "";
        //            string cCodiceCarta = "";
        //            string cPinCarta = "";

        //            string cPathLogIncrementali = "";
        //            string cPathRiepilogo = "";

        //            string cFileLogNonFirmato = "";
        //            string cFileLogIncrementale = "";
        //            string cFileRiepilogo = "";
        //            string cPathEmail = "";
        //            string cMailFrom = "";
        //            string cMailTo = "";

        //            string cFileEmailP7M = "";
        //            string cFileLogIncrementaleP7M = "";
        //            string cFileRiepilogoP7M = "";

        //            try
        //            {
        //                WSRiepiloghi.ResultBaseCodiceSistema oResultCodiceSistema = null;
        //                oResultCodiceSistema = oService.GetCodiceSistema(ServiceName, User, Password);
        //                lRet = (oResultCodiceSistema != null && oResultCodiceSistema.CodiceSistema.Trim() != "");
        //                if (lRet)
        //                {
        //                    cCodiceSistema = oResultCodiceSistema.CodiceSistema;
        //                }
        //            }
        //            catch (Exception exGetCodiceSistema)
        //            {
        //                oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore richiesta codice sistema", exGetCodiceSistema);
        //                lRet = false;
        //            }

        //            try
        //            {
        //                WSRiepiloghi.ResultBaseProprietaRiepiloghi oResultCartaInLocale = oService.GetProprietaRiepiloghi(ServiceName, User, Password, "CARTA_LOCALE", "DISABILITATO");
        //                lRet = (oResultCartaInLocale != null && oResultCartaInLocale.statusok);
        //                if (lRet)
        //                {
        //                    lCartaInLocale = (oResultCartaInLocale.Valore == "ABILITATO");
        //                }
        //                else
        //                {
        //                    oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore richiesta FLAG CARTA IN LOCALE");
        //                }
        //            }
        //            catch (Exception exGetCartaInLocale)
        //            {
        //                oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore richiesta FLAG CARTA IN LOCALE", exGetCartaInLocale);
        //                lRet = false;
        //            }

        //            if (lRet && lCartaInLocale)
        //            {
        //                try
        //                {
        //                    WSRiepiloghi.ResultBaseProprietaRiepiloghi oResultSlotCarta = oService.GetProprietaRiepiloghi(ServiceName, User, Password, "SLOT_CARTA", "0");
        //                    lRet = (oResultSlotCarta != null && oResultSlotCarta.statusok);
        //                    if (lRet)
        //                    {
        //                        lRet = int.TryParse(oResultSlotCarta.Valore, out nSlotCarta);
        //                    }
        //                    if (!lRet)
        //                    {
        //                        oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore richiesta SLOT CARTA IN LOCALE");
        //                    }
        //                }
        //                catch (Exception exGetCartaInLocale)
        //                {
        //                    oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore richiesta SLOT CARTA IN LOCALE", exGetCartaInLocale);
        //                    lRet = false;
        //                }
        //            }

        //            // Verifica ed eventuale creazione cartella temporanea
        //            if (lRet)
        //            {
        //                lRet = System.IO.Directory.Exists(cPathTemp);
        //                if (!lRet)
        //                {
        //                    try
        //                    {
        //                        lRet = System.IO.Directory.CreateDirectory(cPathTemp).Exists;
        //                    }
        //                    catch (Exception)
        //                    {

        //                        throw;
        //                    }
        //                }
        //            }

        //            // Verifica ed eventuale creazione cartelle log incrementali
        //            if (lRet)
        //            {
        //                WSRiepiloghi.ResultBaseProprietaRiepiloghi oResultPathLogIncrementali = oService.GetPathLOG_INCREMENTALE(ServiceName, User, Password);
        //                lRet = (oResultPathLogIncrementali != null && oResultPathLogIncrementali.Valore.Trim() != "");
        //                if (lRet)
        //                {
        //                    cPathLogIncrementali = oResultPathLogIncrementali.Valore;
        //                    lRet = System.IO.Directory.Exists(cPathLogIncrementali);
        //                    if (!lRet)
        //                    {
        //                        try
        //                        {
        //                            lRet = System.IO.Directory.CreateDirectory(cPathLogIncrementali).Exists;
        //                        }
        //                        catch (Exception)
        //                        {

        //                            throw;
        //                        }
        //                    }
        //                }
        //            }

        //            // Verifica ed eventuale creazione cartelle riepiloghi
        //            if (lRet)
        //            {
        //                WSRiepiloghi.ResultBaseProprietaRiepiloghi oResultPathRiepiloghi = oService.GetPathRPG(ServiceName, User, Password);
        //                if (GiornalieroMensile == "M")
        //                {
        //                    oResultPathRiepiloghi = oService.GetPathRPM(ServiceName, User, Password);
        //                }
        //                else
        //                {
        //                    oResultPathRiepiloghi = oService.GetPathRPG(ServiceName, User, Password);
        //                }

        //                lRet = (oResultPathRiepiloghi != null && oResultPathRiepiloghi.Valore.Trim() != "");

        //                if (lRet)
        //                {
        //                    cPathRiepilogo = oResultPathRiepiloghi.Valore + "\\" + dGiorno.Year.ToString("0000") + "_" + dGiorno.Month.ToString("00");

        //                    lRet = System.IO.Directory.Exists(cPathRiepilogo);
        //                    if (!lRet)
        //                    {
        //                        try
        //                        {
        //                            lRet = System.IO.Directory.CreateDirectory(cPathRiepilogo).Exists;
        //                        }
        //                        catch (Exception)
        //                        {

        //                            throw;
        //                        }
        //                    }
        //                }
        //            }

        //            // Verifica ed eventuale creazione cartelle mail
        //            if (lRet)
        //            {
        //                WSRiepiloghi.ResultBaseProprietaRiepiloghi oResultPathMail = oService.GetPathEML(ServiceName, User, Password);
        //                lRet = (oResultPathMail != null && oResultPathMail.Valore.Trim() != "");
        //                if (lRet)
        //                {
        //                    cPathEmail = oResultPathMail.Valore;
        //                    lRet = System.IO.Directory.Exists(cPathEmail);
        //                    if (!lRet)
        //                    {
        //                        try
        //                        {
        //                            lRet = System.IO.Directory.CreateDirectory(cPathEmail).Exists;
        //                        }
        //                        catch (Exception)
        //                        {

        //                            throw;
        //                        }
        //                    }
        //                }
        //            }

        //            // Elaborazione
        //            if (lRet)
        //            {
        //                WSRiepiloghi.ResultBaseGenerazioneRiepilogo oResult = oService.CreaRiepilogoNonFirmatoDateString(ServiceName, User, Password, Giorno, GiornalieroMensile);
        //                nProgressivo = oResult.Progressivo;

        //                lRet = (oResult != null);

        //                if (lRet && GiornalieroMensile == "G")
        //                {
        //                    if (oResult.FileLogNonFirmato.Trim() != "")
        //                    {
        //                        lRet = (oResult.BytesLogNonFirmato.Length > 0);
        //                        if (lRet)
        //                        {
        //                            cFileLogNonFirmato = cPathRiepilogo + "\\" + oResult.FileLogNonFirmato;
        //                            System.IO.File.WriteAllBytes(cFileLogNonFirmato, oResult.BytesLogNonFirmato);
        //                        }

        //                    }
        //                }

        //                if (lRet)
        //                {
        //                    lRet = (oResult.FileRiepilogoNonFirmato.Trim() != "" && oResult.BytesRiepilogoNonFirmato.Length > 0);
        //                    if (lRet)
        //                    {
        //                        cFileRiepilogo = cPathRiepilogo + "\\" + oResult.FileRiepilogoNonFirmato;
        //                        System.IO.File.WriteAllBytes(cFileRiepilogo, oResult.BytesRiepilogoNonFirmato);
        //                    }
        //                }

        //                lDatiRiepilogoCreati = lRet;

        //                // Eventuale Richiesta
        //                if (lRet && !lCartaInLocale)
        //                {
        //                    // Richiedo al server una delle sue carte
        //                    WSRiepiloghi.ResultBaseParametriServerFiscale oResultParametriServerFiscale = oService.GetParametriServerFiscale(ServiceName, User, Password);
        //                    lRet = (oResultParametriServerFiscale != null);
        //                    if (lRet)
        //                    {
        //                        oParametriServerFiscale = new libRiepiloghiBase.clsParametriServerFiscale(oResultParametriServerFiscale.IdSocket, oResultParametriServerFiscale.ServerIP, oResultParametriServerFiscale.Port, oResultParametriServerFiscale.Client_ID, oResultParametriServerFiscale.Password);

        //                        lRet = libRiepiloghiBase.clsClientServerFiscale.RichiestaUtilizzoSmartCard(oParametriServerFiscale, out nSlotCarta);
        //                        lCartaRichiestaAlServerFiscale = lRet;
        //                        if (!lRet)
        //                        {
        //                            if (libRiepiloghiBase.clsClientServerFiscale.ExceptionClient == null)
        //                            {
        //                                oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore nella richiesta utilizzo smartcard al server fiscale.");
        //                            }
        //                            else
        //                            {
        //                                oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore nella richiesta utilizzo smartcard al server fiscale.", libRiepiloghiBase.clsClientServerFiscale.ExceptionClient);
        //                            }
        //                        }
        //                    }

        //                }

        //                // Connessione alla carta
        //                if (lRet)
        //                {
        //                    // Prendo il numero di serie della carta
        //                    try
        //                    {
        //                        cCodiceCarta = libRiepiloghiBase.clsLibSigillo.GetSerial(cPathRisorse, nSlotCarta, out oError);
        //                        lRet = (oError == null);
        //                    }
        //                    catch (Exception exInitCarta)
        //                    {
        //                        lRet = false;
        //                        oError = new Exception(cHeaderError + "\r\n" + "inizializzazione della smartcard", exInitCarta);
        //                        if (!lRet)
        //                        {

        //                        }
        //                    }
        //                }

        //                // richiesta informazioni della carta
        //                if (lRet)
        //                {
        //                    WSRiepiloghi.ResultBaseSmartCard oSmartCard = oService.GetInfoSmartCard(ServiceName, cCodiceSistema, cCodiceCarta);
        //                    lRet = (oSmartCard != null && oSmartCard.SN_CARD == cCodiceCarta && oSmartCard.ENABLED == 1);
        //                    if (lRet)
        //                    {
        //                        cMailFrom = oSmartCard.EMAIL_TITOLARE;
        //                        cMailTo = oSmartCard.EMAIL_SIAE;
        //                    }
        //                }

        //                // Richiesta del PIN
        //                if (lRet)
        //                {
        //                    cPinCarta = "27232862";
        //                }

        //                // Generazione log incrementale
        //                if (lRet && GiornalieroMensile == "G" && oResult.FileLogNonFirmato.Trim() != "")
        //                {
        //                    cFileLogIncrementale = libRiepiloghiBase.clsLibSigillo.CreaLogIncrementale(dGiorno, cFileLogNonFirmato, cPathLogIncrementali, cPathRiepilogo, out oError);
        //                    lRet = (oError == null);
        //                    oFileDaCancellare.Add(cFileLogNonFirmato);
        //                }

        //                // Firma dei files e generazione della mail
        //                if (lRet)
        //                {
        //                    cFileEmailP7M = cPathEmail + "\\" + libRiepiloghiBase.clsLibSigillo.GetNomeEmailGiornoBase(dGiorno, GiornalieroMensile, nProgressivo);
        //                    lRet = libRiepiloghiBase.clsLibSigillo.FirmaFilesGenerazioneMain(cPathRisorse, cCodiceSistema, cPinCarta, nSlotCarta, out oError, cFileLogIncrementale, cFileRiepilogo, cFileEmailP7M, cMailFrom, cMailTo, out cFileLogIncrementaleP7M, out cFileRiepilogoP7M);
        //                    lRet = (lRet && oError == null);

        //                    if (!lRet)
        //                    {
        //                        oFileDaCancellare.Add(cFileLogIncrementaleP7M);
        //                        oFileDaCancellare.Add(cFileRiepilogoP7M);
        //                        oFileDaCancellare.Add(cFileEmailP7M);
        //                    }

        //                    oFileDaCancellare.Add(cFileLogIncrementale);
        //                    oFileDaCancellare.Add(cFileRiepilogo);




        //                }

        //            }

        //        }
        //        catch (Exception exEsegui)
        //        {
        //            oError = libRiepiloghiBase.clsLibSigillo.GetNewException(cHeaderError, "Errore generico", exEsegui);
        //        }
        //        finally
        //        {
        //            // Rilascio della carta richiesta precedentemente al server
        //            if (lCartaRichiestaAlServerFiscale)
        //            {
        //                libRiepiloghiBase.clsClientServerFiscale.RichiestaRilascioSmartCard(oParametriServerFiscale);
        //            }
        //            if (lDatiRiepilogoCreati && oService != null)
        //            {
        //                try
        //                {
        //                    if (lRet)
        //                    {
        //                        WSRiepiloghi.ResultBaseGenerazioneRiepilogo oResultConfermaDati = oService.ConfermaDatiRiepilogoNonFirmatoDateString(ServiceName, User, Password, Giorno, GiornalieroMensile);
        //                        lRet = (oResultConfermaDati != null && oResultConfermaDati.statusok);
        //                    }
        //                    else
        //                    {
        //                        WSRiepiloghi.ResultBaseGenerazioneRiepilogo oResultEliminaDati = oService.EliminaDatiRiepilogoNonFirmatoDateString(ServiceName, User, Password, Giorno, nProgressivo.ToString(), GiornalieroMensile);
        //                        lRet = (oResultEliminaDati != null && oResultEliminaDati.statusok);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                }
        //            }
        //            foreach (string cFileDaCancellare in oFileDaCancellare)
        //            {
        //                try
        //                {
        //                    if (cFileDaCancellare.Trim() != "" && System.IO.File.Exists(cFileDaCancellare))
        //                    {
        //                        System.IO.File.Delete(cFileDaCancellare);
        //                    }
        //                }
        //                catch (Exception)
        //                {

        //                    throw;
        //                }
        //            }
        //        }
        //    }
        //    return lRet;
        //}

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    WinServiceRiepiloghi.clsWinServiceOperations.Start();
        //}

        private void button4_Click(object sender, EventArgs e)
        {
        }
    }
}
