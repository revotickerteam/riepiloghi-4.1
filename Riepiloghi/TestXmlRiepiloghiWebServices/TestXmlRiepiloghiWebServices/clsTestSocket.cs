﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace TestXmlRiepiloghiWebServices
{
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    public class clsSocketRiepiloghi
    {
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static bool StopListen = false;
        public static bool Listening = false;
        public static Socket listener = null;

        public clsSocketRiepiloghi()
        {
            new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public static void StartListening(int nPort)
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];
            StopListen = false;
            Listening = false;
            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, nPort);

            // Create a TCP/IP socket.
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                Listening = true;
                listener.Listen(100);

                while (!StopListen)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }
                listener.Close();
                Listening = false;
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }

            //Console.WriteLine("\nPress ENTER to continue...");
            //Console.Read();

        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket. 
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read 
                // more data.
                content = state.sb.ToString();
                if (content.IndexOf("</xml>") > -1)
                {
                    string User = "";
                    string Password = "";
                    string TipoRiepilogo = "";
                    string GiornoMese = "";

                    // All the data has been read from the 
                    // client. Display it on the console.
                    // Console.WriteLine("Read {0} bytes from socket. \n Data : {1}", content.Length, content);
                    // Echo the data back to the client.
                    try
                    {
                        System.Xml.XmlDocument oDoc = new System.Xml.XmlDocument();
                        oDoc.LoadXml(content);
                        foreach (System.Xml.XmlNode oNode in oDoc.ChildNodes)
                        {
                            if (oNode.Name == "GENERA_RIEPILOGO")
                            {
                                foreach (System.Xml.XmlAttribute oAttibute in oNode.Attributes)
                                {
                                    if (oAttibute.Name == "TIPO_RIEPILOGO")
                                    {
                                        TipoRiepilogo = oAttibute.Value.ToString();
                                    }
                                    else if (oAttibute.Name == "GIORNO_MESE")
                                    {
                                        GiornoMese = oAttibute.Value.ToString();
                                    }
                                    else if (oAttibute.Name == "USER")
                                    {
                                        User = oAttibute.Value.ToString();
                                    }
                                    else if (oAttibute.Name == "PASSWORD")
                                    {
                                        Password = oAttibute.Value.ToString();
                                    }
                                }
                                //clsWinServiceOperations.CreazioneRiepilogo(User, Password, GiornoMese, TipoRiepilogo);
                            }
                            else if (oNode.Name == "SN_CARD_PIN")
                            {
                                foreach (System.Xml.XmlAttribute oAttibute in oNode.Attributes)
                                {
                                    if (oAttibute.Name == "PIN")
                                    {
                                        //clsWinServiceOperations.Pin = oAttibute.Value.ToString();
                                    }
                                }

                            }
                        }
                    }
                    catch
                    {
                    }
                    Send(handler, content);
                }
                else
                {
                    // Not all data received. Get more.
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }
        }

        public static void SendToClient(string data)
        {
            try
            {
                Socket handler = listener;

                // Complete sending the data to the remote device.
                byte[] buffer = System.Text.Encoding.Default.GetBytes(data);
                int bytesSent = handler.Send(buffer);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }
        }
    }

}
