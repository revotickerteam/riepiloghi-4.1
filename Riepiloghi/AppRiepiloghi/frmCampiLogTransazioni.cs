﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AppRiepiloghi
{
    public partial class frmCampiLogTransazioni : Form
    {

        private int ItemValueSelected = -1;
        
        private class clsCheckListBox : CheckedListBox
        {
            public delegate void DelegateScroll(object sender , int TopIndex);
            public event DelegateScroll Scroll;
            private const int WM_VSCROLL = 0x0115;
            private const int SB_THUMBTRACK = 5;
            private const int SB_ENDSCROLL = 8;
            protected override void WndProc(ref Message m)
            {
                base.WndProc(ref m);
                if (m.Msg == WM_VSCROLL)
                {
                    int nfy = m.WParam.ToInt32() & 0xFFFF;
                    if (Scroll != null && (nfy == SB_THUMBTRACK || nfy == SB_ENDSCROLL))
                    {
                        this.Scroll(this, this.TopIndex);
                    }
                }
            }
        }

        private class clsItemCampo : WSRiepiloghi.clsResultBaseCampoLogTransazioni
        {
            public object Value = null;

            public clsItemCampo(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione)
            {
                this.Campo = cCampo;
                this.Tipo = cTipo;
                this.Inizio = nInizio;
                this.Dimensione = nDimensione;
                this.DescCampo = cDescCampo;
                this.Descrizione = cDescrizione;
            }

            public override string ToString()
            {
                return this.DescCampo + " (" + this.Descrizione + ")";
            }
        }

        public frmCampiLogTransazioni(WSRiepiloghi.ServiceRiepiloghi oService, string ServiceName, string User, string Password, bool lEditValues)
        {
            InitializeComponent();
            this.btnAbort.Click += new EventHandler(btnAbort_Click);
            this.btnOK.Click += new EventHandler(btnOK_Click);
            this.chkCampi.ItemCheck += new ItemCheckEventHandler(chkCampi_ItemCheck);
            this.chkCampi.SelectedIndexChanged += new EventHandler(chkCampi_SelectedIndexChanged);
            this.chkCampi.Scroll += new clsCheckListBox.DelegateScroll(chkCampi_Scroll);
            this.txtValue.TextChanged += new EventHandler(txtValue_TextChanged);
            this.dtValue.ValueChanged += new EventHandler(dtValue_ValueChanged);
            this.numValue.ValueChanged += new EventHandler(numValue_ValueChanged);
            this.InitCampiTransazioni(oService, ServiceName, User, Password, lEditValues);
        }

        private void chkCampi_Scroll(object sender, int TopIndex)
        {
            if (this.ValuesVisible)
            {
                this.CloseEdit();
                if (TopIndex >= 0)
                {
                    this.lstValori.TopIndex = TopIndex;
                }
            }
        }

        private void numValue_ValueChanged(object sender, EventArgs e)
        {
            if (this.numValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValori.Items[ItemValueSelected] = this.numValue.Value.ToString();
            }
        }

        private void dtValue_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValori.Items[ItemValueSelected] = this.dtValue.Value.ToShortDateString();
            }
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            if (this.txtValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValori.Items[ItemValueSelected] = this.txtValue.Text;
            }
        }

        private void chkCampi_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (this.ValuesVisible)
            //{
            //    this.CloseEdit();

            //    if (this.chkCampi.SelectedIndex >= 0)
            //    {
            //        ItemValueSelected = this.chkCampi.SelectedIndex;
            //        this.OpenEdit(this.chkCampi.SelectedIndex);
            //    }
            //}
        }

        private void chkCampi_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (this.ValuesVisible)
            {
                this.CloseEdit();
                if (e.NewValue == CheckState.Checked)
                {
                    ItemValueSelected = e.Index;
                    this.OpenEdit(e.Index);
                }
                else
                {
                    this.lstValori.Items[e.Index] = "";
                }
            }
        }

        private void CloseEdit()
        {
            this.txtValue.Enabled = false;
            this.dtValue.Enabled = false;
            this.numValue.Enabled = false;
            this.txtValue.Visible = false;
            this.dtValue.Visible = false;
            this.numValue.Visible = false;
        }

        private void OpenEdit(int Index)
        {
            Rectangle oRect = this.chkCampi.GetItemRectangle(Index);
            clsItemCampo oCampo = (clsItemCampo)this.chkCampi.Items[Index];
            Point oLocation = new Point(this.PointToClient(this.PointToScreen(this.lstValori.Location)).X, oRect.Y + this.lstValori.ItemHeight);
            int nWidth = this.lstValori.Width - SystemInformation.VerticalScrollBarWidth - 1;
            if (oCampo.Tipo == "S")
            {
                this.txtValue.Enabled = false;
                this.txtValue.Location = oLocation;
                this.txtValue.Width = nWidth;
                this.txtValue.MaxLength = oCampo.Dimensione;
                this.txtValue.Text = this.lstValori.Items[Index].ToString();
                this.txtValue.Visible = true;
                this.txtValue.BringToFront();
                this.txtValue.Enabled = true;
                this.txtValue.SelectAll();
                this.txtValue.Focus();
            }
            else if (oCampo.Tipo == "N")
            {
                this.numValue.Enabled = false;
                this.numValue.Location = oLocation;
                this.numValue.Width = nWidth;
                this.numValue.DecimalPlaces = 0;
                Int64 nValue = 0;
                if (Int64.TryParse(this.lstValori.Items[Index].ToString(), out nValue))
                {
                    this.numValue.Value = nValue;
                }
                else
                {
                    this.numValue.Value = 0;
                }
                this.numValue.Visible = true;
                this.numValue.BringToFront();
                this.numValue.Enabled = true;
                this.numValue.Focus();
            }
            else if (oCampo.Tipo == "C")
            {
                this.numValue.Enabled = false;
                this.numValue.Location = oLocation;
                this.numValue.Width = nWidth;
                this.numValue.DecimalPlaces = 2;
                decimal nValue = 0;
                if (decimal.TryParse(this.lstValori.Items[Index].ToString(), out nValue))
                {
                    this.numValue.Value = nValue;
                }
                else
                {
                    this.numValue.Value = 0;
                }
                this.numValue.Visible = true;
                this.numValue.BringToFront();
                this.numValue.Enabled = true;
                this.numValue.Focus();
            }
            else if (oCampo.Tipo == "D")
            {
                this.dtValue.Enabled = false;
                this.dtValue.Location = oLocation;
                this.dtValue.Width = nWidth;
                DateTime dValue;
                if (DateTime.TryParse(this.lstValori.Items[Index].ToString(), out dValue))
                {
                    this.dtValue.Value = dValue;
                }
                else
                {
                    this.dtValue.Value = DateTime.Now;
                }
                this.dtValue.Visible = true;
                this.dtValue.BringToFront();
                this.dtValue.Enabled = true;
                this.dtValue.Focus();
            }

        }

        public System.Collections.SortedList GetListCampi()
        {
            System.Collections.SortedList oRet = new System.Collections.SortedList();
            foreach (int Index in this.chkCampi.CheckedIndices)
            {
                clsItemCampo oCampo = (clsItemCampo)this.chkCampi.Items[Index];
                WSRiepiloghi.clsResultBaseCampoLogTransazioni oWSCampo = new WSRiepiloghi.clsResultBaseCampoLogTransazioni();
                oWSCampo.Campo = oCampo.Campo;
                oWSCampo.Tipo = oCampo.Tipo;
                oWSCampo.Inizio = oCampo.Inizio;
                oWSCampo.Dimensione = oCampo.Dimensione;
                oWSCampo.DescCampo = oCampo.DescCampo;
                oWSCampo.Descrizione = oCampo.Descrizione;
                oRet.Add(oWSCampo.Campo, oWSCampo);
            }
            return oRet;
        }

        private bool GetListValoriCheck()
        {
            System.Collections.SortedList oRet = this.GetListValori(true);
            return (oRet != null);
        }

        public System.Collections.SortedList GetListValori(bool Convert)
        {
            System.Collections.SortedList oRet = new System.Collections.SortedList();
            foreach (int Index in this.chkCampi.CheckedIndices)
            {
                clsItemCampo oCampo = (clsItemCampo)this.chkCampi.Items[Index];

                if (!oRet.ContainsKey(oCampo.Campo))
                {
                }

                if (oCampo.Tipo == "S")
                {
                    oRet.Add(oCampo.Campo, this.lstValori.Items[Index].ToString().PadRight(oCampo.Dimensione));
                }
                else if (oCampo.Tipo == "N")
                {
                    Int64 nValue = 0;
                    if (Int64.TryParse(this.lstValori.Items[Index].ToString(), out nValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, nValue.ToString("0".PadRight(oCampo.Dimensione, '0')));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, nValue);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Valore non valido per il campo [" + oCampo.DescCampo + "] " + this.lstValori.Items[Index].ToString(), "Filtri", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }
                }
                else if (oCampo.Tipo == "C")
                {
                    decimal nValue = 0;
                    if (decimal.TryParse(this.lstValori.Items[Index].ToString(), out nValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, (System.Math.Round(nValue, 2) * 100).ToString("0".PadRight(oCampo.Dimensione, '0')));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, System.Math.Round(nValue, 2));
                        }
                    }
                    else
                    {
                        MessageBox.Show("Valore non valido per il campo [" + oCampo.DescCampo + "] " + this.lstValori.Items[Index].ToString(), "Filtri", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                }
                else if (oCampo.Tipo == "D")
                {
                    DateTime dValue;
                    if (DateTime.TryParse(this.lstValori.Items[Index].ToString(), out dValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, dValue.Year.ToString("0000") + dValue.Month.ToString("00") + dValue.Day.ToString("00"));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, dValue.Date);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Valore non valido per il campo [" + oCampo.DescCampo + "] " + this.lstValori.Items[Index].ToString(), "Filtri", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }
                }
            }
            return oRet;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.GetListValoriCheck())
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void ResizePanels()
        {
            this.Refresh();
            if (this.grpValori.Visible)
            {
                this.grpCampi.Width = (this.ClientRectangle.Width - this.Padding.Horizontal) / 2;
            }
            this.btnAbort.Width = (this.pnlButtons.Width - this.pnlButtons.Padding.Horizontal) / 2;
        }

        private void InitCampiTransazioni(WSRiepiloghi.ServiceRiepiloghi oService, string ServiceName, string User, string Password, bool lEditValues)
        {
            if (lEditValues)
            {
                this.grpValori.Visible = true;
                this.grpValori.BringToFront();
            }
            else
            {
                this.grpValori.Visible = false;
                this.grpCampi.Dock = DockStyle.Fill;
            }
            this.ResizePanels();
            WSRiepiloghi.ResultBaseCampiLogTransazioni oResult = oService.GetListaCampiLogTransazioni(ServiceName, User, Password);
            if (oResult.statusok && oResult.Campi != null && oResult.Campi.Length > 0)
            {
                foreach (WSRiepiloghi.clsResultBaseCampoLogTransazioni oCampo in oResult.Campi)
                {
                    this.chkCampi.Items.Add(new clsItemCampo(oCampo.Campo, oCampo.Tipo, oCampo.Inizio, oCampo.Dimensione, oCampo.DescCampo, oCampo.Descrizione), !lEditValues);
                    this.lstValori.Items.Add(" ".PadRight(oCampo.Dimensione));
                }
            }
        }

        public bool ValuesVisible
        {
            get
            {
                return this.grpValori.Visible;
            }
            set
            {
                //this.splitter1.Visible = value;
                //this.pnlValori.Visible = value;
            }
        }
    }
}
