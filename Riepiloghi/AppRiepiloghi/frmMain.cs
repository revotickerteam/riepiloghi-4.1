﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AppRiepiloghi
{
    public partial class frmMain : Form
    {
        #region "Dichiarazioni"
        
        private string ServiceName = "";
        private string User = "";
        private string Password = "";
        private WSRiepiloghi.ResultBaseCodiceSistema CodiceSistema = null;
        private bool lRiepilogoAnnullati = false;

        private System.Collections.SortedList oFiltri = new System.Collections.SortedList();
        private WSRiepiloghi.ServiceRiepiloghi ServiceRiepiloghi;

        #endregion

        public frmMain()
        {
            InitializeComponent();

            this.InitForm();
        }

        // Inizializzazione della form
        private void InitForm()
        {
            this.pnlParametri.AutoScroll = false;
            this.grpConnessione.Visible = true;
            this.pnlRiepiloghi.Visible = false;
            
            // Assegno eventi
            this.btnExitConn.Click += new EventHandler(btnExit_Click);
            this.btnDisconnect.Click += new EventHandler(btnDisconnect_Click);

            // Eventi controlli di Connessione
            this.cmbConnessione.TextChanged += new EventHandler(cmbConnessione_Changed);
            this.cmbConnessione.SelectedValueChanged += new EventHandler(cmbConnessione_Changed);
            this.btnConnessione.Click += new EventHandler(btnConnessione_Click);

            // Eventi Transazioni
            this.btnTransazioni.Click += new EventHandler(btnTransazioni_Click);
            this.btnTransazioniLoad.Click += new EventHandler(btnTransazioniLoad_Click);
            this.btnTransazioniFiltri.Click += new EventHandler(btnTransazioniFiltri_Click);

            // Eventi Riepilogo Giornaliero
            this.pnlRiepilogo.SizeChanged += new EventHandler(pnlRiepilogo_SizeChanged);
            this.pnlRiepilogo.VisibleChanged += new EventHandler(pnlRiepilogo_VisibleChanged);
            this.btnGiornalieri.Click += new EventHandler(btnGiornalieri_Click);
        }

        // Termina applicazione
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (this.ServiceRiepiloghi != null)
            {
                this.ServiceRiepiloghi.Dispose();
            }
            this.Close();
            this.Dispose();
            Application.Exit();
        }
        
        // Combo connessione
        private void cmbConnessione_Changed(object sender, EventArgs e)
        {
            this.ServiceName = this.cmbConnessione.Text.Trim();
        }

        // bottone di Connessione
        private void btnConnessione_Click(object sender, EventArgs e)
        {
            try
            {
                bool lRet = false;
                this.ServiceRiepiloghi = new WSRiepiloghi.ServiceRiepiloghi();
                this.CodiceSistema = ServiceRiepiloghi.GetCodiceSistema(this.ServiceName, this.User, this.Password);
                lRet = CodiceSistema.statusok;
                if (lRet)
                {
                    this.InitFormConnected();
                }
            }
            catch
            {

            }
        }

        // bottone di Disconnessione
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            this.grpFunzioni.Text = "Codice sistema:";
            this.pnlRiepiloghi.Visible = false;
            this.HideParametriDati();
        }

        private void HideParametriDati()
        {
            this.HideDati();
            this.HideParametri();
        }

        // Inizializzazione form quando connessa ad un sistema
        private void InitFormConnected()
        {
            this.grpFunzioni.Text = "Codice sistema: " + this.CodiceSistema.CodiceSistema;
            this.pnlRiepiloghi.Visible = true;
            this.HideParametriDati();
        }

        // Nasconde pannello dei parametri
        private void HideParametri()
        {
            this.grpParametri.Visible = false;
            this.grpParametri.Text = "";
            foreach (System.Windows.Forms.Control oControl in this.pnlParametri.Controls)
            {
                oControl.Visible = false;
            }
        }

        // Nasconde pannello dei dati
        private void HideDati()
        {
            this.grpDati.Text = "";
            this.grpDati.Visible = false;
            this.dgvDati.DataSource = null;
        }

        // Dimensiona il gruppo di parametri in base al contenuto visibile
        private void ResizeGrpParametri()
        {
            this.Refresh();
            int nMaxBottom = 0;
            foreach (Control oControl in this.pnlParametri.Controls)
            {
                if (oControl.Visible && oControl.Bottom > nMaxBottom)
                {
                    nMaxBottom = oControl.Bottom;
                }
            }
            this.grpParametri.Height = 20 + this.grpParametri.Padding.Vertical + this.pnlParametri.Padding.Vertical + nMaxBottom;
        }


        #region "Transazioni"
        
        private void btnTransazioni_Click(object sender, EventArgs e)
        {
            this.HideParametriDati();
            this.ShowParametriTransazioni();
        }

        private void btnTransazioniFiltri_Click(object sender, EventArgs e)
        {
            frmCampiLogTransazioni oFrmCampi = new frmCampiLogTransazioni(this.ServiceRiepiloghi, this.ServiceName, this.User, this.Password, true);

            if (oFrmCampi.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.oFiltri = oFrmCampi.GetListValori(false);
            }
            else
            {
                if (this.oFiltri != null && this.oFiltri.Count > 0)
                {
                    if (MessageBox.Show("Eliminare tutti i filtri ?", "Filtri", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.oFiltri = new System.Collections.SortedList();
                    }
                }
            }
            oFrmCampi.Close();
            oFrmCampi.Dispose();
        }


        private void ShowParametriTransazioni()
        {
            this.grpParametri.Visible = true;
            this.pnlTransazioni.Visible = true;
            this.grpParametri.Text = "Log Transazioni";
            this.ResizeGrpParametri();
        }

        private void ShowParametriTransazioniAnnullati()
        {
            this.grpParametri.Visible = true;
            this.pnlTransazioni.Visible = true;
            this.grpParametri.Text = "Log Transazioni Annullate";
            this.ResizeGrpParametri();
        }

        // Avvio caricamento transazioni o transazioni annullate
        private void btnTransazioniLoad_Click(object sender, EventArgs e)
        {
            if (this.lRiepilogoAnnullati)
            {
                this.CaricaTransazioniAnnullate();
            }
            else
            {
                this.CaricaTransazioni();
            }
        }

        private void CaricaTransazioni()
        {
            string cFiltri = "";
            WSRiepiloghi.ResultTransazionePeriodo oResult = this.ServiceRiepiloghi.GetTransazioni(this.ServiceName, this.User, this.Password, this.dtTransazioniInizio.Value.Date, this.dtTransazioniFine.Value.Date, "CHECK", "TRUE"); 
            this.grpDati.Visible = true;
            this.grpDati.BringToFront();
            this.dgvDati.DataSource = oResult.TableTransazioni;
        }

        private void CaricaTransazioniAnnullate()
        {
            WSRiepiloghi.ResultTransazionePeriodo oResult = this.ServiceRiepiloghi.GetTransazioniAnnullati(this.ServiceName, this.User, this.Password, this.dtTransazioniInizio.Value.Date, this.dtTransazioniFine.Value.Date, "CHECK", "TRUE");
            this.grpDati.Visible = true;
            this.grpDati.BringToFront();
            this.dgvDati.DataSource = oResult.TableTransazioni;
        }

        #endregion

        #region "Riepilogo"

        private void btnGiornalieri_Click(object sender, EventArgs e)
        {
            this.HideParametriDati();
            this.ShowParametriRiepilogoGiornaliero();
        }

        private void pnlRiepilogo_VisibleChanged(object sender, EventArgs e)
        {
            this.RiepiloghiResizeButtons();
        }

        private void pnlRiepilogo_SizeChanged(object sender, EventArgs e)
        {
            this.RiepiloghiResizeButtons();
        }

        private void RiepiloghiResizeButtons()
        {
            this.btnRiepilogoGenera.Width = (this.pnlRiepilogo.Width - this.pnlRiepilogoGiorno.Width - this.pnlRiepilogo.Padding.Horizontal) / 2;
        }

        private void ShowParametriRiepilogoGiornaliero()
        {
            this.grpParametri.Visible = true;
            this.pnlRiepilogo.Visible = true;
            this.grpParametri.Text = "Riepilogo Giornaliero";
            this.ResizeGrpParametri();
        }

        #endregion

    }
}
