﻿namespace AppRiepiloghi
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpConnessione = new System.Windows.Forms.GroupBox();
            this.btnConnessione = new System.Windows.Forms.Button();
            this.btnExitConn = new System.Windows.Forms.Button();
            this.lblConnessione = new System.Windows.Forms.Label();
            this.cmbConnessione = new System.Windows.Forms.ComboBox();
            this.pnlRiepiloghi = new System.Windows.Forms.Panel();
            this.grpDati = new System.Windows.Forms.GroupBox();
            this.dgvDati = new System.Windows.Forms.DataGridView();
            this.grpParametri = new System.Windows.Forms.GroupBox();
            this.pnlParametri = new System.Windows.Forms.Panel();
            this.pnlTransazioni = new System.Windows.Forms.Panel();
            this.btnTransazioniLoad = new System.Windows.Forms.Button();
            this.pnlPeriodoTransazioni = new System.Windows.Forms.Panel();
            this.dtTransazioniFine = new System.Windows.Forms.DateTimePicker();
            this.lblTransazioniInizio = new System.Windows.Forms.Label();
            this.dtTransazioniInizio = new System.Windows.Forms.DateTimePicker();
            this.lblTransazioniFine = new System.Windows.Forms.Label();
            this.grpFunzioni = new System.Windows.Forms.GroupBox();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnFunzioniDiServizio = new System.Windows.Forms.Button();
            this.btnAnnulli = new System.Windows.Forms.Button();
            this.btnMensili = new System.Windows.Forms.Button();
            this.btnGiornalieri = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnTransazioni = new System.Windows.Forms.Button();
            this.pnlRiepilogo = new System.Windows.Forms.Panel();
            this.btnRiepilogoStampa = new System.Windows.Forms.Button();
            this.pnlRiepilogoGiorno = new System.Windows.Forms.Panel();
            this.lblRiepilogoGiorno = new System.Windows.Forms.Label();
            this.dtRiepilogoGiorno = new System.Windows.Forms.DateTimePicker();
            this.btnRiepilogoGenera = new System.Windows.Forms.Button();
            this.btnTransazioniFiltri = new System.Windows.Forms.Button();
            this.btnTransazioniOrdinamento = new System.Windows.Forms.Button();
            this.grpConnessione.SuspendLayout();
            this.pnlRiepiloghi.SuspendLayout();
            this.grpDati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDati)).BeginInit();
            this.grpParametri.SuspendLayout();
            this.pnlParametri.SuspendLayout();
            this.pnlTransazioni.SuspendLayout();
            this.pnlPeriodoTransazioni.SuspendLayout();
            this.grpFunzioni.SuspendLayout();
            this.pnlRiepilogo.SuspendLayout();
            this.pnlRiepilogoGiorno.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpConnessione
            // 
            this.grpConnessione.Controls.Add(this.btnConnessione);
            this.grpConnessione.Controls.Add(this.btnExitConn);
            this.grpConnessione.Controls.Add(this.lblConnessione);
            this.grpConnessione.Controls.Add(this.cmbConnessione);
            this.grpConnessione.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpConnessione.Location = new System.Drawing.Point(8, 0);
            this.grpConnessione.Name = "grpConnessione";
            this.grpConnessione.Padding = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.grpConnessione.Size = new System.Drawing.Size(768, 60);
            this.grpConnessione.TabIndex = 3;
            this.grpConnessione.TabStop = false;
            // 
            // btnConnessione
            // 
            this.btnConnessione.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConnessione.Location = new System.Drawing.Point(580, 16);
            this.btnConnessione.Name = "btnConnessione";
            this.btnConnessione.Size = new System.Drawing.Size(90, 36);
            this.btnConnessione.TabIndex = 1;
            this.btnConnessione.Text = "Connetti";
            this.btnConnessione.UseVisualStyleBackColor = true;
            // 
            // btnExitConn
            // 
            this.btnExitConn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExitConn.Location = new System.Drawing.Point(670, 16);
            this.btnExitConn.Name = "btnExitConn";
            this.btnExitConn.Size = new System.Drawing.Size(90, 36);
            this.btnExitConn.TabIndex = 2;
            this.btnExitConn.Text = "Uscita";
            this.btnExitConn.UseVisualStyleBackColor = true;
            // 
            // lblConnessione
            // 
            this.lblConnessione.AutoSize = true;
            this.lblConnessione.Location = new System.Drawing.Point(11, 26);
            this.lblConnessione.Name = "lblConnessione";
            this.lblConnessione.Size = new System.Drawing.Size(80, 16);
            this.lblConnessione.TabIndex = 0;
            this.lblConnessione.Text = "Connessione";
            this.lblConnessione.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbConnessione
            // 
            this.cmbConnessione.FormattingEnabled = true;
            this.cmbConnessione.Location = new System.Drawing.Point(93, 23);
            this.cmbConnessione.Name = "cmbConnessione";
            this.cmbConnessione.Size = new System.Drawing.Size(311, 24);
            this.cmbConnessione.TabIndex = 1;
            this.cmbConnessione.Text = "serverx";
            // 
            // pnlRiepiloghi
            // 
            this.pnlRiepiloghi.Controls.Add(this.grpDati);
            this.pnlRiepiloghi.Controls.Add(this.grpParametri);
            this.pnlRiepiloghi.Controls.Add(this.grpFunzioni);
            this.pnlRiepiloghi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRiepiloghi.Location = new System.Drawing.Point(8, 60);
            this.pnlRiepiloghi.Name = "pnlRiepiloghi";
            this.pnlRiepiloghi.Size = new System.Drawing.Size(768, 494);
            this.pnlRiepiloghi.TabIndex = 5;
            // 
            // grpDati
            // 
            this.grpDati.Controls.Add(this.dgvDati);
            this.grpDati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDati.Location = new System.Drawing.Point(0, 368);
            this.grpDati.Name = "grpDati";
            this.grpDati.Padding = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.grpDati.Size = new System.Drawing.Size(768, 126);
            this.grpDati.TabIndex = 8;
            this.grpDati.TabStop = false;
            this.grpDati.Visible = false;
            // 
            // dgvDati
            // 
            this.dgvDati.AllowUserToAddRows = false;
            this.dgvDati.AllowUserToDeleteRows = false;
            this.dgvDati.AllowUserToOrderColumns = true;
            this.dgvDati.BackgroundColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDati.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDati.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDati.Location = new System.Drawing.Point(8, 16);
            this.dgvDati.MultiSelect = false;
            this.dgvDati.Name = "dgvDati";
            this.dgvDati.ReadOnly = true;
            this.dgvDati.RowHeadersVisible = false;
            this.dgvDati.Size = new System.Drawing.Size(752, 102);
            this.dgvDati.TabIndex = 0;
            // 
            // grpParametri
            // 
            this.grpParametri.Controls.Add(this.pnlParametri);
            this.grpParametri.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpParametri.Location = new System.Drawing.Point(0, 95);
            this.grpParametri.Name = "grpParametri";
            this.grpParametri.Padding = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.grpParametri.Size = new System.Drawing.Size(768, 273);
            this.grpParametri.TabIndex = 7;
            this.grpParametri.TabStop = false;
            this.grpParametri.Visible = false;
            // 
            // pnlParametri
            // 
            this.pnlParametri.AutoScroll = true;
            this.pnlParametri.Controls.Add(this.pnlRiepilogo);
            this.pnlParametri.Controls.Add(this.pnlTransazioni);
            this.pnlParametri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlParametri.Location = new System.Drawing.Point(8, 16);
            this.pnlParametri.Name = "pnlParametri";
            this.pnlParametri.Size = new System.Drawing.Size(752, 249);
            this.pnlParametri.TabIndex = 1;
            // 
            // pnlTransazioni
            // 
            this.pnlTransazioni.Controls.Add(this.btnTransazioniLoad);
            this.pnlTransazioni.Controls.Add(this.pnlPeriodoTransazioni);
            this.pnlTransazioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTransazioni.Location = new System.Drawing.Point(0, 0);
            this.pnlTransazioni.Name = "pnlTransazioni";
            this.pnlTransazioni.Padding = new System.Windows.Forms.Padding(2);
            this.pnlTransazioni.Size = new System.Drawing.Size(752, 107);
            this.pnlTransazioni.TabIndex = 1;
            this.pnlTransazioni.Visible = false;
            // 
            // btnTransazioniLoad
            // 
            this.btnTransazioniLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTransazioniLoad.Location = new System.Drawing.Point(482, 2);
            this.btnTransazioniLoad.Name = "btnTransazioniLoad";
            this.btnTransazioniLoad.Size = new System.Drawing.Size(268, 103);
            this.btnTransazioniLoad.TabIndex = 2;
            this.btnTransazioniLoad.Text = "Carica";
            this.btnTransazioniLoad.UseVisualStyleBackColor = true;
            // 
            // pnlPeriodoTransazioni
            // 
            this.pnlPeriodoTransazioni.Controls.Add(this.btnTransazioniOrdinamento);
            this.pnlPeriodoTransazioni.Controls.Add(this.btnTransazioniFiltri);
            this.pnlPeriodoTransazioni.Controls.Add(this.dtTransazioniFine);
            this.pnlPeriodoTransazioni.Controls.Add(this.lblTransazioniInizio);
            this.pnlPeriodoTransazioni.Controls.Add(this.dtTransazioniInizio);
            this.pnlPeriodoTransazioni.Controls.Add(this.lblTransazioniFine);
            this.pnlPeriodoTransazioni.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlPeriodoTransazioni.Location = new System.Drawing.Point(2, 2);
            this.pnlPeriodoTransazioni.Name = "pnlPeriodoTransazioni";
            this.pnlPeriodoTransazioni.Size = new System.Drawing.Size(480, 103);
            this.pnlPeriodoTransazioni.TabIndex = 6;
            // 
            // dtTransazioniFine
            // 
            this.dtTransazioniFine.Location = new System.Drawing.Point(250, 30);
            this.dtTransazioniFine.Name = "dtTransazioniFine";
            this.dtTransazioniFine.Size = new System.Drawing.Size(222, 23);
            this.dtTransazioniFine.TabIndex = 5;
            // 
            // lblTransazioniInizio
            // 
            this.lblTransazioniInizio.AutoSize = true;
            this.lblTransazioniInizio.Location = new System.Drawing.Point(7, 11);
            this.lblTransazioniInizio.Name = "lblTransazioniInizio";
            this.lblTransazioniInizio.Size = new System.Drawing.Size(65, 16);
            this.lblTransazioniInizio.TabIndex = 0;
            this.lblTransazioniInizio.Text = "Dalla data";
            // 
            // dtTransazioniInizio
            // 
            this.dtTransazioniInizio.Location = new System.Drawing.Point(10, 30);
            this.dtTransazioniInizio.Name = "dtTransazioniInizio";
            this.dtTransazioniInizio.Size = new System.Drawing.Size(222, 23);
            this.dtTransazioniInizio.TabIndex = 3;
            // 
            // lblTransazioniFine
            // 
            this.lblTransazioniFine.AutoSize = true;
            this.lblTransazioniFine.Location = new System.Drawing.Point(247, 11);
            this.lblTransazioniFine.Name = "lblTransazioniFine";
            this.lblTransazioniFine.Size = new System.Drawing.Size(58, 16);
            this.lblTransazioniFine.TabIndex = 4;
            this.lblTransazioniFine.Text = "Alla data";
            // 
            // grpFunzioni
            // 
            this.grpFunzioni.Controls.Add(this.btnDisconnect);
            this.grpFunzioni.Controls.Add(this.btnFunzioniDiServizio);
            this.grpFunzioni.Controls.Add(this.btnAnnulli);
            this.grpFunzioni.Controls.Add(this.btnMensili);
            this.grpFunzioni.Controls.Add(this.btnGiornalieri);
            this.grpFunzioni.Controls.Add(this.button4);
            this.grpFunzioni.Controls.Add(this.btnTransazioni);
            this.grpFunzioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpFunzioni.Location = new System.Drawing.Point(0, 0);
            this.grpFunzioni.Name = "grpFunzioni";
            this.grpFunzioni.Padding = new System.Windows.Forms.Padding(8);
            this.grpFunzioni.Size = new System.Drawing.Size(768, 95);
            this.grpFunzioni.TabIndex = 6;
            this.grpFunzioni.TabStop = false;
            this.grpFunzioni.Text = "Codice sistema:";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDisconnect.Location = new System.Drawing.Point(548, 24);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(212, 63);
            this.btnDisconnect.TabIndex = 6;
            this.btnDisconnect.Text = "Disconnetti";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            // 
            // btnFunzioniDiServizio
            // 
            this.btnFunzioniDiServizio.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnFunzioniDiServizio.Location = new System.Drawing.Point(458, 24);
            this.btnFunzioniDiServizio.Name = "btnFunzioniDiServizio";
            this.btnFunzioniDiServizio.Size = new System.Drawing.Size(90, 63);
            this.btnFunzioniDiServizio.TabIndex = 4;
            this.btnFunzioniDiServizio.Text = "Funzioni di Servizio";
            this.btnFunzioniDiServizio.UseVisualStyleBackColor = true;
            // 
            // btnAnnulli
            // 
            this.btnAnnulli.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAnnulli.Location = new System.Drawing.Point(368, 24);
            this.btnAnnulli.Name = "btnAnnulli";
            this.btnAnnulli.Size = new System.Drawing.Size(90, 63);
            this.btnAnnulli.TabIndex = 3;
            this.btnAnnulli.Text = "Annulli";
            this.btnAnnulli.UseVisualStyleBackColor = true;
            // 
            // btnMensili
            // 
            this.btnMensili.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnMensili.Location = new System.Drawing.Point(278, 24);
            this.btnMensili.Name = "btnMensili";
            this.btnMensili.Size = new System.Drawing.Size(90, 63);
            this.btnMensili.TabIndex = 2;
            this.btnMensili.Text = "Riepiloghi Mensili";
            this.btnMensili.UseVisualStyleBackColor = true;
            // 
            // btnGiornalieri
            // 
            this.btnGiornalieri.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnGiornalieri.Location = new System.Drawing.Point(188, 24);
            this.btnGiornalieri.Name = "btnGiornalieri";
            this.btnGiornalieri.Size = new System.Drawing.Size(90, 63);
            this.btnGiornalieri.TabIndex = 1;
            this.btnGiornalieri.Text = "Riepiloghi Giornalieri";
            this.btnGiornalieri.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Left;
            this.button4.Location = new System.Drawing.Point(98, 24);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(90, 63);
            this.button4.TabIndex = 5;
            this.button4.Text = "Riepiloghi da Generare";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnTransazioni
            // 
            this.btnTransazioni.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnTransazioni.Location = new System.Drawing.Point(8, 24);
            this.btnTransazioni.Name = "btnTransazioni";
            this.btnTransazioni.Size = new System.Drawing.Size(90, 63);
            this.btnTransazioni.TabIndex = 0;
            this.btnTransazioni.Text = "Log Transazioni";
            this.btnTransazioni.UseVisualStyleBackColor = true;
            // 
            // pnlRiepilogo
            // 
            this.pnlRiepilogo.Controls.Add(this.btnRiepilogoStampa);
            this.pnlRiepilogo.Controls.Add(this.btnRiepilogoGenera);
            this.pnlRiepilogo.Controls.Add(this.pnlRiepilogoGiorno);
            this.pnlRiepilogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRiepilogo.Location = new System.Drawing.Point(0, 107);
            this.pnlRiepilogo.Name = "pnlRiepilogo";
            this.pnlRiepilogo.Padding = new System.Windows.Forms.Padding(2);
            this.pnlRiepilogo.Size = new System.Drawing.Size(752, 72);
            this.pnlRiepilogo.TabIndex = 2;
            this.pnlRiepilogo.Visible = false;
            // 
            // btnRiepilogoStampa
            // 
            this.btnRiepilogoStampa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRiepilogoStampa.Location = new System.Drawing.Point(610, 2);
            this.btnRiepilogoStampa.Name = "btnRiepilogoStampa";
            this.btnRiepilogoStampa.Size = new System.Drawing.Size(140, 68);
            this.btnRiepilogoStampa.TabIndex = 2;
            this.btnRiepilogoStampa.Text = "Stampa";
            this.btnRiepilogoStampa.UseVisualStyleBackColor = true;
            // 
            // pnlRiepilogoGiorno
            // 
            this.pnlRiepilogoGiorno.Controls.Add(this.lblRiepilogoGiorno);
            this.pnlRiepilogoGiorno.Controls.Add(this.dtRiepilogoGiorno);
            this.pnlRiepilogoGiorno.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlRiepilogoGiorno.Location = new System.Drawing.Point(2, 2);
            this.pnlRiepilogoGiorno.Name = "pnlRiepilogoGiorno";
            this.pnlRiepilogoGiorno.Size = new System.Drawing.Size(480, 68);
            this.pnlRiepilogoGiorno.TabIndex = 6;
            // 
            // lblRiepilogoGiorno
            // 
            this.lblRiepilogoGiorno.AutoSize = true;
            this.lblRiepilogoGiorno.Location = new System.Drawing.Point(7, 11);
            this.lblRiepilogoGiorno.Name = "lblRiepilogoGiorno";
            this.lblRiepilogoGiorno.Size = new System.Drawing.Size(45, 16);
            this.lblRiepilogoGiorno.TabIndex = 0;
            this.lblRiepilogoGiorno.Text = "Giorno";
            // 
            // dtRiepilogoGiorno
            // 
            this.dtRiepilogoGiorno.Location = new System.Drawing.Point(10, 30);
            this.dtRiepilogoGiorno.Name = "dtRiepilogoGiorno";
            this.dtRiepilogoGiorno.Size = new System.Drawing.Size(222, 23);
            this.dtRiepilogoGiorno.TabIndex = 3;
            // 
            // btnRiepilogoGenera
            // 
            this.btnRiepilogoGenera.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRiepilogoGenera.Location = new System.Drawing.Point(482, 2);
            this.btnRiepilogoGenera.Name = "btnRiepilogoGenera";
            this.btnRiepilogoGenera.Size = new System.Drawing.Size(128, 68);
            this.btnRiepilogoGenera.TabIndex = 7;
            this.btnRiepilogoGenera.Text = "Genera";
            this.btnRiepilogoGenera.UseVisualStyleBackColor = true;
            // 
            // btnTransazioniFiltri
            // 
            this.btnTransazioniFiltri.Location = new System.Drawing.Point(10, 59);
            this.btnTransazioniFiltri.Name = "btnTransazioniFiltri";
            this.btnTransazioniFiltri.Size = new System.Drawing.Size(222, 40);
            this.btnTransazioniFiltri.TabIndex = 6;
            this.btnTransazioniFiltri.Text = "Imposta Filtri";
            this.btnTransazioniFiltri.UseVisualStyleBackColor = true;
            // 
            // btnTransazioniOrdinamento
            // 
            this.btnTransazioniOrdinamento.Location = new System.Drawing.Point(250, 59);
            this.btnTransazioniOrdinamento.Name = "btnTransazioniOrdinamento";
            this.btnTransazioniOrdinamento.Size = new System.Drawing.Size(222, 40);
            this.btnTransazioniOrdinamento.TabIndex = 7;
            this.btnTransazioniOrdinamento.Text = "Imposta Ordinamento";
            this.btnTransazioniOrdinamento.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.pnlRiepiloghi);
            this.Controls.Add(this.grpConnessione);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Riepiloghi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.grpConnessione.ResumeLayout(false);
            this.grpConnessione.PerformLayout();
            this.pnlRiepiloghi.ResumeLayout(false);
            this.grpDati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDati)).EndInit();
            this.grpParametri.ResumeLayout(false);
            this.pnlParametri.ResumeLayout(false);
            this.pnlTransazioni.ResumeLayout(false);
            this.pnlPeriodoTransazioni.ResumeLayout(false);
            this.pnlPeriodoTransazioni.PerformLayout();
            this.grpFunzioni.ResumeLayout(false);
            this.pnlRiepilogo.ResumeLayout(false);
            this.pnlRiepilogoGiorno.ResumeLayout(false);
            this.pnlRiepilogoGiorno.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpConnessione;
        private System.Windows.Forms.Label lblConnessione;
        private System.Windows.Forms.ComboBox cmbConnessione;
        private System.Windows.Forms.Button btnConnessione;
        private System.Windows.Forms.Panel pnlRiepiloghi;
        private System.Windows.Forms.GroupBox grpDati;
        private System.Windows.Forms.GroupBox grpParametri;
        private System.Windows.Forms.GroupBox grpFunzioni;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnFunzioniDiServizio;
        private System.Windows.Forms.Button btnAnnulli;
        private System.Windows.Forms.Button btnMensili;
        private System.Windows.Forms.Button btnGiornalieri;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnTransazioni;
        private System.Windows.Forms.Button btnExitConn;
        private System.Windows.Forms.Panel pnlParametri;
        private System.Windows.Forms.Panel pnlTransazioni;
        private System.Windows.Forms.Button btnTransazioniLoad;
        private System.Windows.Forms.Label lblTransazioniInizio;
        private System.Windows.Forms.DateTimePicker dtTransazioniFine;
        private System.Windows.Forms.Label lblTransazioniFine;
        private System.Windows.Forms.DateTimePicker dtTransazioniInizio;
        private System.Windows.Forms.Panel pnlPeriodoTransazioni;
        private System.Windows.Forms.DataGridView dgvDati;
        private System.Windows.Forms.Panel pnlRiepilogo;
        private System.Windows.Forms.Button btnRiepilogoStampa;
        private System.Windows.Forms.Button btnRiepilogoGenera;
        private System.Windows.Forms.Panel pnlRiepilogoGiorno;
        private System.Windows.Forms.Label lblRiepilogoGiorno;
        private System.Windows.Forms.DateTimePicker dtRiepilogoGiorno;
        private System.Windows.Forms.Button btnTransazioniOrdinamento;
        private System.Windows.Forms.Button btnTransazioniFiltri;

    }
}