﻿namespace AppRiepiloghi
{
    partial class frmCampiLogTransazioni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpCampi = new System.Windows.Forms.GroupBox();
            this.chkCampi = new AppRiepiloghi.frmCampiLogTransazioni.clsCheckListBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.grpValori = new System.Windows.Forms.GroupBox();
            this.numValue = new System.Windows.Forms.NumericUpDown();
            this.dtValue = new System.Windows.Forms.DateTimePicker();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.lstValori = new System.Windows.Forms.ListBox();
            this.grpCampi.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.grpValori.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).BeginInit();
            this.SuspendLayout();
            // 
            // grpCampi
            // 
            this.grpCampi.Controls.Add(this.chkCampi);
            this.grpCampi.Dock = System.Windows.Forms.DockStyle.Left;
            this.grpCampi.Location = new System.Drawing.Point(0, 0);
            this.grpCampi.Name = "grpCampi";
            this.grpCampi.Size = new System.Drawing.Size(304, 362);
            this.grpCampi.TabIndex = 0;
            this.grpCampi.TabStop = false;
            this.grpCampi.Text = "Campi log transazioni";
            // 
            // chkCampi
            // 
            this.chkCampi.CheckOnClick = true;
            this.chkCampi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCampi.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCampi.FormattingEnabled = true;
            this.chkCampi.IntegralHeight = false;
            this.chkCampi.Location = new System.Drawing.Point(3, 19);
            this.chkCampi.Name = "chkCampi";
            this.chkCampi.Size = new System.Drawing.Size(298, 340);
            this.chkCampi.TabIndex = 0;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Controls.Add(this.btnAbort);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 362);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(500, 58);
            this.pnlButtons.TabIndex = 1;
            // 
            // btnOK
            // 
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOK.Location = new System.Drawing.Point(210, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(287, 52);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Conferma";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnAbort
            // 
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(3, 3);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(207, 52);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Abbandona";
            this.btnAbort.UseVisualStyleBackColor = true;
            // 
            // grpValori
            // 
            this.grpValori.Controls.Add(this.numValue);
            this.grpValori.Controls.Add(this.dtValue);
            this.grpValori.Controls.Add(this.txtValue);
            this.grpValori.Controls.Add(this.lstValori);
            this.grpValori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpValori.Location = new System.Drawing.Point(304, 0);
            this.grpValori.Name = "grpValori";
            this.grpValori.Size = new System.Drawing.Size(196, 362);
            this.grpValori.TabIndex = 2;
            this.grpValori.TabStop = false;
            this.grpValori.Text = "Valori";
            // 
            // numValue
            // 
            this.numValue.BackColor = System.Drawing.Color.Gainsboro;
            this.numValue.Enabled = false;
            this.numValue.Location = new System.Drawing.Point(39, 210);
            this.numValue.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numValue.Name = "numValue";
            this.numValue.Size = new System.Drawing.Size(120, 23);
            this.numValue.TabIndex = 10;
            this.numValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numValue.Visible = false;
            // 
            // dtValue
            // 
            this.dtValue.Enabled = false;
            this.dtValue.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtValue.Location = new System.Drawing.Point(37, 170);
            this.dtValue.Name = "dtValue";
            this.dtValue.Size = new System.Drawing.Size(110, 23);
            this.dtValue.TabIndex = 9;
            this.dtValue.Visible = false;
            // 
            // txtValue
            // 
            this.txtValue.BackColor = System.Drawing.Color.Gainsboro;
            this.txtValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValue.Enabled = false;
            this.txtValue.Location = new System.Drawing.Point(37, 130);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(100, 23);
            this.txtValue.TabIndex = 8;
            this.txtValue.Visible = false;
            this.txtValue.WordWrap = false;
            // 
            // lstValori
            // 
            this.lstValori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstValori.Enabled = false;
            this.lstValori.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstValori.FormattingEnabled = true;
            this.lstValori.IntegralHeight = false;
            this.lstValori.ItemHeight = 17;
            this.lstValori.Location = new System.Drawing.Point(3, 19);
            this.lstValori.Name = "lstValori";
            this.lstValori.Size = new System.Drawing.Size(190, 340);
            this.lstValori.TabIndex = 11;
            // 
            // frmCampiLogTransazioni
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(500, 420);
            this.Controls.Add(this.grpValori);
            this.Controls.Add(this.grpCampi);
            this.Controls.Add(this.pnlButtons);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmCampiLogTransazioni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Campi log delle transazioni";
            this.grpCampi.ResumeLayout(false);
            this.pnlButtons.ResumeLayout(false);
            this.grpValori.ResumeLayout(false);
            this.grpValori.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCampi;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnOK;
        private frmCampiLogTransazioni.clsCheckListBox chkCampi;
        private System.Windows.Forms.GroupBox grpValori;
        private System.Windows.Forms.NumericUpDown numValue;
        private System.Windows.Forms.DateTimePicker dtValue;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.ListBox lstValori;
    }
}