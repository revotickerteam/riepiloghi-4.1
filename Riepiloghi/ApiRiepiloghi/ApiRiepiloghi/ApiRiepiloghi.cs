﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Wintic.Data;
using Newtonsoft.Json;
using System.Net;
using System.Security.Cryptography;
using System.Data;
using libBordero;
using static libBordero.Bordero;
using Riepiloghi;
using System.Diagnostics;

namespace ApiRiepiloghi
{

    public class clsEnvironment
    {
        public string InternalConnectionString { get; set; }
        public string ExternalConnectionString { get; set; }
        public string PathRisorseWeb { get; set; }
        public string PathFont { get; set; } 
        public string PathTemp { get; set; }
        public string PathWriteRiepiloghi { get; set; }
    }

    public static class ServiceRiepiloghi 
    {
        public class ResultContentFile
        {
            public string ContentType { get; set; }
            public string FileName { get; set; }
            public string Buffer { get; set; }
        }

        // Classe di risposta base
        public class ResultBase
        {
            public bool StatusOK = true;

            public string StatusMessage = "";

            public Int64 StatusCode = 0;

            public string execution_time = "";

            public ResultContentFile contentFile { get; set; }

            public DateTime Sysdate { get; set; }

            public ResultBase()
            { 
            }

            public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }
        }

        #region "funzioni di base"

        // funzione che Recupera la connessione
        public static Wintic.Data.oracle.CConnectionOracle GetConnectionInternal(out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                oCon.ConnectionString = oEnvironment.InternalConnectionString;
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
            }
            return oCon;
        }

        private static void WriteTokenRequest(IConnection ConnectionInternal, string Token)
        {
            try
            {
                Int64 nTOKEN_REQUEST_ID = Int64.Parse(ConnectionInternal.ExecuteNonQuery("INSERT INTO RP_TOKENS_REQUEST (TOKEN_STRING) VALUES (:pTOKEN_STRING)", "TOKEN_REQUEST_ID", new clsParameters(":pTOKEN_STRING", Token), true).ToString());
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                clsParameters oPars = new clsParameters();
                oPars.Add(":pTOKEN_JSON", Riepiloghi.clsToken.SerializeToken(oToken));
                oPars.Add(":pTOKEN_REQUEST_ID", nTOKEN_REQUEST_ID);
                ConnectionInternal.ExecuteNonQuery("UPDATE RP_TOKENS_REQUEST SET TOKEN_JSON = :pTOKEN_JSON WHERE TOKEN_REQUEST_ID = :pTOKEN_REQUEST_ID", oPars, true);

                DeleteOldRequests(ConnectionInternal);
            }
            catch (Exception ex)
            {
            }

        }

        public static void DeleteOldRequests(IConnection ConnectionInternal)
        {
            // Pulizia cache richieste CACHE_OLD_REQUESTS_DAYS
            Exception oError = null;
            string cDAYS = Riepiloghi.clsRP_PROPERTY.GetPR_PROPERTY("TIMEOUT_TOKEN_MINUTES", ConnectionInternal, "", out oError).Value;
            long nDAYS = 0;
            if (!long.TryParse(cDAYS, out nDAYS))
            {
                nDAYS = 20;
            }


            clsParameters oPars = new clsParameters();

            // RP_REQUESTS
            try
            {
                oPars = new clsParameters(":pDAYS", nDAYS);
                ConnectionInternal.ExecuteNonQuery("DELETE FROM RP_REQUESTS WHERE SYSDATE - DATA_INS > :pDAYS", oPars, true);
            }
            catch (Exception)
            {
            }

            // RP_TOKENS
            try
            {
                oPars = new clsParameters(":pDAYS", nDAYS);
                ConnectionInternal.ExecuteNonQuery("DELETE FROM RP_TOKENS WHERE SYSDATE - DATA_AGG > :pDAYS", oPars, true);
            }
            catch (Exception)
            {
            }

            // RP_TOKENS_REQUEST
            try
            {
                oPars = new clsParameters(":pDAYS", nDAYS);
                ConnectionInternal.ExecuteNonQuery("DELETE FROM RP_TOKENS_REQUEST WHERE SYSDATE - DATA_INS > :pDAYS", oPars, true);
            }
            catch (Exception)
            {
            }

            // RP_TOKENS_PSW
            try
            {
                ConnectionInternal.ExecuteNonQuery("DELETE FROM RP_TOKENS_PSW WHERE DATA_SCAD_RENEW < SYSDATE", true);
            }
            catch (Exception)
            {
            }
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnectionAutoAuth(IConnection ConnectionInternal, string CodiceSistema, out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                //WriteTokenRequest(ConnectionInternal, Token);
                //Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA WHERE CODICE_SISTEMA = :pCODICE_SISTEMA", new clsParameters(":pCODICE_SISTEMA", CodiceSistema));

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione.", exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    //oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.ConnectionString = string.Format(oEnvironment.ExternalConnectionString, ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.", exGetConnection);
                }
            }
            return oCon;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnection(IConnection ConnectionInternal, string Token, string CodiceSistema, out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                WriteTokenRequest(ConnectionInternal, Token);
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA WHERE CODICE_SISTEMA = :pCODICE_SISTEMA", new clsParameters(":pCODICE_SISTEMA", CodiceSistema));

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione.", exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    //oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.ConnectionString = string.Format(oEnvironment.ExternalConnectionString, ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.", exGetConnection);
                }
            }
            return oCon;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnection(IConnection ConnectionInternal, string Token, out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            string ConnectionString = "";
            try
            {

                WriteTokenRequest(ConnectionInternal, Token);
                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                IRecordSet oRS = (IRecordSet)ConnectionInternal.ExecuteQuery("SELECT CONNECTION_STRING FROM RP_CODICI_SISTEMA WHERE CODICE_SISTEMA = :pCODICE_SISTEMA", new clsParameters(":pCODICE_SISTEMA", oToken.CodiceSistema.CodiceSistema));

                if (oRS.EOF)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Codice Sistema non valido.");
                }
                else if (oRS.Fields("CONNECTION_STRING").IsNull)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                }
                else
                {
                    ConnectionString = oRS.Fields("CONNECTION_STRING").Value.ToString();
                    if (ConnectionString.Trim() == "")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Connessione al Codice Sistema non configurato.");
                    }
                }

                oRS.Close();
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante il recupero della connessione." + exGetConnection.ToString(), exGetConnection);
            }

            if (oError == null)
            {
                try
                {
                    oCon = new Wintic.Data.oracle.CConnectionOracle();
                    //oCon.ConnectionString = "user=wtic;password=obelix;data source=#SERVICE_NAME#;".Replace("#SERVICE_NAME#", ConnectionString);
                    oCon.ConnectionString = string.Format(oEnvironment.ExternalConnectionString, ConnectionString);
                    oCon.Open();
                    if (oCon.State != System.Data.ConnectionState.Open)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                    }
                }
                catch (Exception exGetConnection)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
                }
            }
            return oCon;
        }

        // Converte una stringa nella modalità storico: TRUE, FALSE, CHECK
        private static Riepiloghi.clsRiepiloghi.DefModalitaStorico GetModalitaStoricoFromString(string cValue)
        {
            Riepiloghi.clsRiepiloghi.DefModalitaStorico oModalitaStorico = Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check;
            foreach (Riepiloghi.clsRiepiloghi.DefModalitaStorico ItemEnum in Enum.GetValues(typeof(Riepiloghi.clsRiepiloghi.DefModalitaStorico)))
            {
                if (ItemEnum.ToString().Trim().ToUpper() == cValue.Trim().ToUpper())
                {
                    oModalitaStorico = ItemEnum;
                    break;
                }
            }
            return oModalitaStorico;
        }

        public static bool CheckUserOperazione(IConnection oConnection, string Operazione, string Token, out Exception oError)
        {
            bool lRet = false;
            oError = null;
            Riepiloghi.clsToken oToken = null;
            if (Riepiloghi.clsToken.CheckToken(oConnection, Token, out oError, out oToken))
            {
                lRet = Riepiloghi.clsRiepiloghi.CheckIdOperazioneAccount(oConnection, Operazione, oToken, out oError);
            }
            return lRet;
        }

        public class ResultBaseAdminCodici : ResultBase
        {
            public Riepiloghi.clsAdminCodici Codici = null;

            public ResultBaseAdminCodici()
                : base()
            { }

            public ResultBaseAdminCodici(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAdminCodici(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsAdminCodici oCodici)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Codici = oCodici;
            }
        }

        public static ResultBaseAdminCodici GetAdminCodici(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAdminCodici oRet = new ResultBaseAdminCodici();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAdminCodici AdminCodici = new Riepiloghi.clsAdminCodici();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfile", Token);
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        AdminCodici = Riepiloghi.clsAdminCodici.GetAdminCodici(oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Amministrazione Codici Riepiloghi", "Errore imprevisto nella lettura Admministrazione Codici dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAdminCodici(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AdminCodici);
                }
                else
                {
                    oRet = new ResultBaseAdminCodici(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AdminCodici);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        public static ResultBaseAdminCodici SetAdminCodici(string Token, Riepiloghi.clsAdminCodici Codici, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAdminCodici oRet = new ResultBaseAdminCodici();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAdminCodici oAdminCodiciModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAdminCodici", Token, new Dictionary<string, object>() { { "Codici", Codici } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Codici != null)
                            {

                                oAdminCodiciModified = Riepiloghi.clsAdminCodici.SetAdminCodici(Codici, oConnectionInternal, out oError);
                            }
                            else
                            {
                                oError = new Exception("Lista Codici non valida.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Amministrazione Codici Riepiloghi", "Errore imprevisto nella modifica dell'Amministrazione Codici dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAdminCodici(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAdminCodiciModified);
                }
                else
                {
                    oRet = new ResultBaseAdminCodici(true, "Modifica Profili Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAdminCodiciModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseCodiceSistema SetNewAdminCodice(string Token, string ConnectionString, string DescrizioneCodiceSistema, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = new ResultBaseCodiceSistema();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsCodiceSistema oCodiceSistemaNew = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAdminCodici", Token, new Dictionary<string, object>() { { "Descrizione", DescrizioneCodiceSistema }, { "Connessione", ConnectionString } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_SISTEMI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                        if (ConnectionString == null | string.IsNullOrEmpty(ConnectionString) || ConnectionString.Trim() == "" ||
                            DescrizioneCodiceSistema == null | string.IsNullOrEmpty(DescrizioneCodiceSistema) || DescrizioneCodiceSistema.Trim() == "")
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione nuovo Codice Sistema", "Descrizione e Connessione obbligatori.");
                        }
                        else
                        {

                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                oCodiceSistemaNew = Riepiloghi.clsAdminCodici.SetNewCodiceSistema(ConnectionString, DescrizioneCodiceSistema, oConnectionInternal, out oError, oEnvironment.ExternalConnectionString);
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione nuovo Codice Sistema", "Errore imprevisto nella creazione del nuovo Codice Sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "Nuovo Codice Sistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oCodiceSistemaNew);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAdminCodici", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseProfile : ResultBase
        {
            public Riepiloghi.clsProfile Profile = null;

            public ResultBaseProfile()
                : base()
            { }

            public ResultBaseProfile(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseProfile(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsProfile oProfile)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Profile = oProfile;
            }
        }

        public static ResultBaseProfile GetProfile(string Token, Int64 ProfileId, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile Profile = new Riepiloghi.clsProfile();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfile", Token, new Dictionary<string, object>() { { "ProfileId", ProfileId } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Profile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Profilo Riepiloghi", "Errore imprevisto nella lettura del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Profile);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Profile);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseProfile CreateProfile(string Token, string cDescrizione, Int64 ParentProfileId, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile oProfileModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreateProfile", Token, new Dictionary<string, object>() { { "cDescrizione", cDescrizione }, { "ParentProfileId", ParentProfileId } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);


                        if (ParentProfileId == 0)
                        {
                            ParentProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);
                            if (ParentProfileId == 0)
                            {
                                oError = new Exception("Profilo superiore non valido.");
                            }
                        }

                        if (oError == null)
                        {
                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                if (cDescrizione != null && cDescrizione.Trim() != "")
                                {
                                    oProfileModified = Riepiloghi.clsProfile.CreateProfile(cDescrizione, ParentProfileId, oConnectionInternal, out oError);
                                }
                                else
                                {
                                    oError = new Exception("Descrizione non valida.");
                                }
                            }
                            catch (Exception ex)
                            {
                                oError = new Exception("CreateProfile", ex);
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Profilo Riepiloghi", "Errore imprevisto nella creazione del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Creazione Profilo Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            //try
            //{
            //    System.IO.File.AppendAllText(@"E:\LOG-IIS\LOGRP.TXT", "\r\n" + "GetProfile ret" + "\r\n" + Newtonsoft.Json.JsonConvert.SerializeObject(oRet));
            //}
            //catch (Exception)
            //{
            //}

            return oRet;
        }

        public static ResultBaseProfile SetProfile(string Token, Riepiloghi.clsProfile Profile, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfile oRet = new ResultBaseProfile();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsProfile oProfileModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetProfile", Token, new Dictionary<string, object>() { { "Profile", Profile } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Profile != null)
                            {
                                oProfileModified = Riepiloghi.clsProfile.SetProfile(Profile, oConnectionInternal, out oError);
                            }
                            else
                            {
                                oError = new Exception("Profilo non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Profili Riepiloghi", "Errore imprevisto nella modifica del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfile(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }
                else
                {
                    oRet = new ResultBaseProfile(true, "Modifica Profili Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oProfileModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResponseDataRiepiloghi SetProfile(string Token, Riepiloghi.clsProfile Profile, clsEnvironment oEnvironment, out Exception oError)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResponseDataRiepiloghi oRet = new ResponseDataRiepiloghi();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            oError = null;
            Riepiloghi.clsProfile oProfileModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetProfile", Token, new Dictionary<string, object>() { { "Profile", Profile } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Profile != null)
                            {
                                oProfileModified = Riepiloghi.clsProfile.SetProfile(Profile, oConnectionInternal, out oError);
                            }
                            else
                            {
                                oError = new Exception("Profilo non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Profili Riepiloghi", "Errore imprevisto nella modifica del profilo dei Riepiloghi.", ex);
            }
            finally
            {
                oRet = new ResponseDataRiepiloghi();
                if (oError == null)
                    oRet.Profile = oProfileModified;

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProfile", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseProfileList : ResultBase
        {
            public List<Riepiloghi.clsProfile> ProfileList = null;

            public ResultBaseProfileList()
                : base()
            { }

            public ResultBaseProfileList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseProfileList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsProfile> oProfileList)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.ProfileList = oProfileList;
            }
        }

        public static ResultBaseProfileList GetProfileList(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProfileList oRet = new ResultBaseProfileList();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsProfile> ProfileList = new List<Riepiloghi.clsProfile>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfileList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        ProfileList = Riepiloghi.clsProfile.GetProfileList(oToken.AccountId, oConnectionInternal, out oError);
                    }

                    //try
                    //{
                    //    foreach (Riepiloghi.clsProfile ITEM in ProfileList)
                    //    {
                    //        if (ITEM.ProfileId == 2)
                    //        {
                    //            ITEM.CodiciSistema[0].Enabled = true;
                    //            SetProfile(Token, ITEM);
                    //        }
                    //    }
                    //}
                    //catch (Exception)
                    //{

                    //    throw;
                    //}
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Profili Riepiloghi", "Errore imprevisto nella lettura dei Profili dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProfileList(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), ProfileList);
                }
                else
                {
                    oRet = new ResultBaseProfileList(true, "Lista Accounts Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), ProfileList);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            return oRet;
        }

        public static ResponseDataRiepiloghi GetProfileListProfiles(string Token, clsEnvironment oEnvironment, out Exception oError)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResponseDataRiepiloghi oRet = new ResponseDataRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            oError = null;
            List<Riepiloghi.clsProfile> ProfileList = new List<Riepiloghi.clsProfile>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProfileList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_PROFILES", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        ProfileList = Riepiloghi.clsProfile.GetProfileList(oToken.AccountId, oConnectionInternal, out oError);
                    }

                    //try
                    //{
                    //    foreach (Riepiloghi.clsProfile ITEM in ProfileList)
                    //    {
                    //        if (ITEM.ProfileId == 2)
                    //        {
                    //            ITEM.CodiciSistema[0].Enabled = true;
                    //            SetProfile(Token, ITEM);
                    //        }
                    //    }
                    //}
                    //catch (Exception)
                    //{

                    //    throw;
                    //}
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Profili Riepiloghi", "Errore imprevisto nella lettura dei Profili dei Riepiloghi.", ex);
            }
            finally
            {
                oRet = new ResponseDataRiepiloghi();
                if (oError == null)
                    oRet.Profiles = ProfileList;

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            return oRet;
        }

        public class ResultBaseAccount : ResultBase
        {
            public Riepiloghi.clsAccount Account = null;

            public ResultBaseAccount()
                : base()
            { }

            public ResultBaseAccount(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAccount(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsAccount oAccount)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Account = oAccount;
            }
        }

        private const string indexRiepiloghiAutoAuth = "E5AE450FE2119D0B7EDF48AF";

        public static ResultBaseToken RiepiloghiAutoAuth(PayLoadRiepiloghi parPayload, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cToken = "";

            bool lCodiceSistemaUnico = false;
            string cCodiceSistemaUnico = "";

            try
            {
                #region Decript richiesta

                string key = indexRiepiloghiAutoAuth;

                string valueRequest = parPayload.Token;
                try
                {
                    byte[] keyArray;
                    byte[] toEncryptArray = Convert.FromBase64String(parPayload.Token);
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                    tdes.Key = keyArray;
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    ICryptoTransform cTransform = tdes.CreateDecryptor();
                    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    string resultRequestDecrypt = UTF8Encoding.UTF8.GetString(resultArray);

                    if (resultRequestDecrypt != "Ch3T31oD1c0AF@re")
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("RiepiloghiAutoAuth", "richiesta non valida.");
                    }

                }
                catch (Exception ex)
                {
                    oError = ex;
                }

                #endregion

                if (oError == null)
                    oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "RiepiloghiAutoAuth", "", new Dictionary<string, object>() { { "Token", parPayload.Token } });

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);

                    if (lCodiceSistemaUnico && oError == null && cCodiceSistemaUnico != null && !string.IsNullOrEmpty(cCodiceSistemaUnico) && !string.IsNullOrWhiteSpace(cCodiceSistemaUnico))
                    {
                        oConnection = ServiceRiepiloghi.GetConnectionAutoAuth(oConnectionInternal, cCodiceSistemaUnico, out oError, oEnvironment);
                        if (oError == null)
                        {
                            string value = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "RIEPILOGHI_AUTO_USER_PASSWORD", "");
                            if (value != null && !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value) && value.Contains("/"))
                            {
                                #region cryptografia user/password riepiloghi automatici

                                try
                                {
                                    byte[] keyArray;
                                    byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(value);
                                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                                    tdes.Key = keyArray;
                                    tdes.Mode = CipherMode.ECB;
                                    tdes.Padding = PaddingMode.PKCS7;

                                    ICryptoTransform cTransform = tdes.CreateEncryptor();
                                    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                                    cToken = Convert.ToBase64String(resultArray, 0, resultArray.Length);
                                }
                                catch (Exception ex)
                                {
                                    oError = ex;
                                }

                                #endregion
                            }
                            else
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("RiepiloghiAutoAuth", "impostazione user/password riepiloghi automatici non valido.");
                        }
                    }
                    else
                    {
                        cToken = "";
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("RiepiloghiAutoAuth", "Sistema non unico, impossibile sopperire alla richiesta." + (oError != null ? oError.Message : ""));
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("RiepiloghiAutoAuth", "Errore imprevisto nella richiesta RiepiloghiAutoAuth.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }
                else
                {
                    oRet = new ResultBaseToken(true, "RiepiloghiAutoAuth", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }

                oRet.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "GetToken", oRet);
                }
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResponseRiepiloghi CheckToken(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResponseRiepiloghi oRet = new ResponseRiepiloghi();

            oRet.Data = new ResponseDataRiepiloghi();
            oRet.Status = new ApiRiepiloghi.ResultBase();
            oRet.Status.StatusCode = 0;
            oRet.Status.StatusMessage = "CheckToken";
            oRet.Status.execution_time = "";
            oRet.Status.StatusOK = true;

            DateTime dStart = DateTime.Now;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (string.IsNullOrEmpty(Token) || string.IsNullOrWhiteSpace(Token))
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Token errato.");
                        oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CheckToken", Token);
                    }
                    else
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CheckToken", Token, new Dictionary<string, object>() { { "AccountId", oToken.AccountId }, { "CodiceSistema", (oToken.CodiceSistema == null ? "" : oToken.CodiceSistema.CodiceSistema) } });
                        oRet.Data.Account = GetAccount(Token, oToken.AccountId, oEnvironment, oConnectionInternal, out oError);
                        if (oError == null)
                        {
                            oRet.Data.CodiceSistema = GetCodiceSistema(Token, oEnvironment, oConnectionInternal, out oError);
                            if (oToken.CodiceSistema != null)
                                oRet.Data.CodiceSistema.MultiCinema = oToken.CodiceSistema.MultiCinema;
                            if (oRet.Data.CodiceSistema != null && oRet.Data.Account != null && oRet.Data.Account.CodiciSistema != null)
                            {
                                if (oRet.Data.CodiceSistema.CFOrganizzatori == null)
                                    oRet.Data.CodiceSistema.CFOrganizzatori = new List<Riepiloghi.clsCFOrganizzatore>();

                                oRet.Data.Account.CodiciSistema
                                    .Where(cs => (cs.CodiceSistema.Equals(oRet.Data.CodiceSistema.CodiceSistema) 
                                                  ||
                                                  cs.CodiceSistema.StartsWith(oRet.Data.CodiceSistema.CodiceSistema + "."))
                                                 && cs.CFOrganizzatori != null 
                                                 && cs.CFOrganizzatori.Count > 0)
                                    .ToList()
                                    .ForEach(cs =>
                                    {
                                        cs.CFOrganizzatori
                                        .Where(org => oRet.Data.CodiceSistema.CFOrganizzatori.FirstOrDefault(codOrg => codOrg.CFOrganizzatore.Equals(org.CFOrganizzatore)) == null)
                                        .ToList()
                                        .ForEach(org =>
                                        {
                                            oRet.Data.CodiceSistema.CFOrganizzatori.Add(
                                                new Riepiloghi.clsCFOrganizzatore()
                                                {
                                                    CodiceSistema = oRet.Data.CodiceSistema.CodiceSistema,
                                                    CFOrganizzatore = org.CFOrganizzatore,
                                                    Descrizione = org.Descrizione,
                                                    Enabled = true
                                                }
                                                );
                                        });
                                    });
                            }
                        }

                        oRet.Status.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);
                    }

                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Errore imprevisto nella lettura dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                oRet.Status.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
                oRet.Status.StatusOK = (oError == null);
                oRet.Status.StatusMessage = (oError == null ? "CheckToken" : oError.Message);
                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oRet;
        }

        public static ResultBaseAccount GetAccount(string Token, string AccountId, clsEnvironment oEnvironment)
        {

            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount Account = null;
            IConnection oConnectionInternal = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetAccount", Token, new Dictionary<string, object>() { { "AccountId", AccountId } });
                    Account = GetAccount(Token, AccountId, oEnvironment, oConnectionInternal, out oError);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Errore imprevisto nella lettura dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Account);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Account);
                }

                oRet.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oRet;
        }

        private static Riepiloghi.clsAccount GetAccount(string Token, string AccountId, clsEnvironment oEnvironment, IConnection oConnectionInternal, out Exception oError)
        {            
            DateTime dStart = DateTime.Now;
            oError = null;
            Riepiloghi.clsAccount Account = null;
            try
            {
                if (string.IsNullOrEmpty(Token) || string.IsNullOrWhiteSpace(Token))
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Token errato.");
                }
                else
                {

                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    if (AccountId == null || AccountId.Trim() == "")
                    {
                        AccountId = oToken.AccountId;
                    }

                    if (string.IsNullOrEmpty(oToken.AccountId) || string.IsNullOrWhiteSpace(oToken.AccountId))
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Account errato.");
                    }
                    else
                    {

                        Account = Riepiloghi.clsAccount.GetAccount(AccountId, oConnectionInternal, out oError, true, true, true);
                        if (oToken.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema == "00000000")
                        {
                            List<Riepiloghi.clsCategoriaOperazioniRiepiloghi> tempCatOperazioni = new List<Riepiloghi.clsCategoriaOperazioniRiepiloghi>();
                            foreach (Riepiloghi.clsCategoriaOperazioniRiepiloghi oCategoria in Account.Operazioni)
                            {
                                if (oCategoria.IdCategoria == 1)
                                {
                                    tempCatOperazioni.Add(oCategoria);
                                    //break;
                                }
                                else if (oCategoria.IdCategoria == 5)
                                {
                                    Riepiloghi.clsCategoriaOperazioniRiepiloghi oCategoriaConfig = new Riepiloghi.clsCategoriaOperazioniRiepiloghi(oCategoria.IdCategoria, oCategoria.Descrizione, 99);
                                    foreach (Riepiloghi.clsOperazioneRiepiloghi oOperazione in oCategoria.ListaOperazioni)
                                    {
                                        if (oOperazione.IdOperazione != "CONFIG_RIEPILOGHI")
                                        {
                                            oCategoriaConfig.ListaOperazioni.Add(oOperazione);
                                        }
                                    }
                                    tempCatOperazioni.Add(oCategoriaConfig);
                                    //break;
                                }
                            }
                            Account.Operazioni = tempCatOperazioni;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Account Riepiloghi", "Errore imprevisto nella lettura dell'account dei Riepiloghi.", ex);
            }

            //string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return Account;
        }

        public static ResponseDataRiepiloghi CreateAccount(string Token, string url, string Email, string Nome, string Cognome, Int64 ProfileId, clsEnvironment oEnvironment, out Exception oError)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResponseDataRiepiloghi oRet = new ResponseDataRiepiloghi();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            oError = null;
            Riepiloghi.clsAccount oAccount = new Riepiloghi.clsAccount();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreateAccount", Token, new Dictionary<string, object>() { { "Email", Email }, { "Nome", Nome }, { "Cognome", Cognome }, { "Url", url } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Email != null && Email.Trim() != "")
                            {
                                if (Nome != null && Nome.Trim() != "")
                                {
                                    if (Cognome != null && Cognome.Trim() != "")
                                    {
                                        oAccount = Riepiloghi.clsAccount.CreateAccount(oToken.AccountId, url, Email, Nome, Cognome, ProfileId, oConnectionInternal, out oError);
                                    }
                                    else
                                    {
                                        oError = new Exception("Cognome Account non valido.");
                                    }
                                }
                                else
                                {
                                    oError = new Exception("Nome Account non valido.");
                                }
                            }
                            else
                            {
                                oError = new Exception("Email Account non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oAccount != null)
                    oRet.Account = oAccount;
                
                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseAccount SetAccount(string Token, Riepiloghi.clsAccount Account, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccountModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAccount", Token, new Dictionary<string, object>() { { "Account", Account } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        oConnectionInternal.BeginTransaction();
                        try
                        {
                            if (Account != null)
                            {
                                oAccountModified = Riepiloghi.clsAccount.SetAccount(oToken.AccountId, oConnectionInternal, out oError, Account);
                            }
                            else
                            {
                                oError = new Exception("Account non valido.");
                            }
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            if (oError != null)
                            {
                                oConnectionInternal.RollBack();
                            }
                            else
                            {
                                oConnectionInternal.EndTransaction();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Modifica Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseAccount RenewAccountPassword(string Email, string url, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccount = new Riepiloghi.clsAccount();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null && (Email == null || Email.Trim() == ""))
                {
                    oError = new Exception("Email non valida");
                }

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "RenewAccountPassword", "", new Dictionary<string, object>() { { "Email", Email }, { "Url", url } });

                    string AccountId = Riepiloghi.clsAccount.GetAccountByEmail(Email, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        if (AccountId.Trim() != "")
                        {
                            oConnectionInternal.BeginTransaction();
                            try
                            {
                                // Spedizione email per modifica password
                                bool lRet = Riepiloghi.clsEmail.SendMailAccountCreateRenew(AccountId, "RIEPILOGHI_WEB", url, oConnectionInternal, out oError);
                                if (!lRet)
                                {
                                    oError = new Exception("Errore nella spedizione della email.");
                                }
                                else
                                {
                                    oAccount = Riepiloghi.clsAccount.GetAccount(AccountId, oConnectionInternal, out oError, true, false, false);
                                }
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                if (oError != null)
                                {
                                    oConnectionInternal.RollBack();
                                }
                                else
                                {
                                    oConnectionInternal.EndTransaction();
                                }
                            }

                        }
                        else
                        {
                            oError = new Exception("Email non valida");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Reimposta password Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Reimposta password Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccount);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseAccount SetAccountPassword(string Token, string Email, string Password, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccount oRet = new ResultBaseAccount();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsAccount oAccountModified = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetAccountPassword", Token, new Dictionary<string, object>() { { "Email", Email }, { "Password", Password } });

                    oConnectionInternal.BeginTransaction();
                    try
                    {
                        oAccountModified = Riepiloghi.clsAccount.SetAccountPassword(Token, oConnectionInternal, out oError, Email, "RIEPILOGHI_WEB", Password);
                    }
                    catch (Exception ex)
                    {
                        oError = ex;
                    }
                    finally
                    {
                        if (oError != null)
                        {
                            oConnectionInternal.RollBack();
                        }
                        else
                        {
                            oConnectionInternal.EndTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Password Account Riepiloghi", "Errore imprevisto nella modifica dell'account dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccount(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }
                else
                {
                    oRet = new ResultBaseAccount(true, "Modifica Password Account Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oAccountModified);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccount", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public class ResultBaseAccountList : ResultBase
        {
            public List<Riepiloghi.clsAccount> AccountList = null;

            public ResultBaseAccountList()
                : base()
            { }

            public ResultBaseAccountList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            { }

            public ResultBaseAccountList(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsAccount> oAccountList)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.AccountList = oAccountList;
            }
        }

        public static ResultBaseAccountList GetAccountList(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseAccountList oRet = new ResultBaseAccountList();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsAccount> AccountList = new List<Riepiloghi.clsAccount>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetAccountList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        AccountList = Riepiloghi.clsAccount.GetAccountsList(oToken.AccountId, oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Accounts Riepiloghi", "Errore imprevisto nella lettura degli Accounts dei Riepiloghi.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseAccountList(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AccountList);
                }
                else
                {
                    oRet = new ResultBaseAccountList(true, "Lista Accounts Riepiloghi", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), AccountList);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            return oRet;
        }

        public static ResponseDataRiepiloghi GetAccountListProfile(string Token, clsEnvironment oEnvironment, out Exception oError)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResponseDataRiepiloghi oRet = new ResponseDataRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            oError = null;
            List<Riepiloghi.clsAccount> AccountList = new List<Riepiloghi.clsAccount>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetAccountList", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ACCOUNTS", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        AccountList = Riepiloghi.clsAccount.GetAccountsList(oToken.AccountId, oConnectionInternal, out oError);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Accounts Riepiloghi", "Errore imprevisto nella lettura degli Accounts dei Riepiloghi.", ex);
            }
            finally
            {
                oRet = new ResponseDataRiepiloghi();
                if (oError == null)
                    oRet.Accounts = AccountList;

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseAccountList", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            return oRet;
        }

        #endregion

        #region "funzioni di Consultazione" 

        #region "Configurazione"

        public class ResultBaseProprietaRiepiloghi : ResultBase
        {
            public string Proprieta = "";
            public string Valore = "";

            public ResultBaseProprietaRiepiloghi()
            {
            }

            public ResultBaseProprietaRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Proprieta)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Proprieta = Proprieta;
                this.Valore = "";
            }
        }

        public class ResultBaseListProprietaRiepiloghi : ResultBase
        {
            public List<Riepiloghi.clsProprietaRiepiloghi> ListaProprieta = new List<Riepiloghi.clsProprietaRiepiloghi>();

            public ResultBaseListProprietaRiepiloghi()
            {

            }

            public ResultBaseListProprietaRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {

            }
        }

        public static ResultBaseProprietaRiepiloghi GetProprietaRiepiloghi(string Token, string Proprieta, string DefaultValue, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProprietaRiepiloghi oRet = new ResultBaseProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cValore = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "Proprieta", Proprieta }, { "DefaultValue", DefaultValue } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            cValore = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, Proprieta, DefaultValue);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Proprieta riepiloghi" + Proprieta, "Errore imprevisto nella lettura della proprietà.", ex);
            }
            finally
            {

                if (oError != null)
                {
                    oRet = new ResultBaseProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta);
                }
                else
                {
                    oRet = new ResultBaseProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta); ;
                    oRet.Valore = cValore;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseProprietaRiepiloghi GetEmailCodiceSistema(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProprietaRiepiloghi oRet = new ResultBaseProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cValore = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetEmailCodiceSistema", Token, null);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            cValore = Riepiloghi.clsRiepiloghi.GetEmailCodiceSistema(oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("GetEmailCodiceSistema", "Errore imprevisto nella lettura della proprietà.", ex);
            }
            finally
            {

                if (oError != null)
                {
                    oRet = new ResultBaseProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "EmailCodiceSistema");
                }
                else
                {
                    oRet = new ResultBaseProprietaRiepiloghi(true, "EmailCodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), "EmailCodiceSistema"); ;
                    oRet.Valore = cValore;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseProprietaRiepiloghi SetProprietaRiepiloghi(string Token, string Proprieta, string Valore, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseProprietaRiepiloghi oRet = new ResultBaseProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "Proprieta", Proprieta }, { "Valore", Valore } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("SELECT PROPRIETA, VALORE, DESCR, REGULAR_EXPRESSION FROM PROPRIETA_RIEPILOGHI WHERE PROPRIETA = :pPROPRIETA");
                            IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), new clsParameters(":pPROPRIETA", Proprieta));
                            if (!oRS.EOF)
                            {
                                Riepiloghi.clsProprietaRiepiloghi oProprieta = new Riepiloghi.clsProprietaRiepiloghi();
                                oProprieta.Proprieta = (oRS.Fields("PROPRIETA").IsNull ? "" : oRS.Fields("PROPRIETA").Value.ToString());
                                if (oProprieta.Proprieta.Trim() != "")
                                {
                                    oProprieta.Descrizione = (oRS.Fields("DESCR").IsNull ? "" : oRS.Fields("DESCR").Value.ToString());
                                    oProprieta.RegularExpression = (oRS.Fields("REGULAR_EXPRESSION").IsNull ? "" : oRS.Fields("REGULAR_EXPRESSION").Value.ToString());
                                }
                                bool lSave = false;

                                if (oProprieta.RegularExpression.Trim() != "")
                                {
                                    try
                                    {
                                        System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex(oProprieta.RegularExpression);
                                        lSave = oRegex.IsMatch(Valore);
                                    }
                                    catch (Exception ex)
                                    {
                                        lSave = false;
                                    }
                                    if (!lSave)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Proprietà riepiloghi " + Proprieta, "Valore non valido.");
                                    }
                                }
                                else
                                {
                                    lSave = true;
                                }

                                if (lSave)
                                {
                                    oProprieta.Valore = Valore;
                                    clsParameters oPars = new clsParameters();
                                    oPars.Add(":pPROPRIETA", oProprieta.Proprieta);

                                    oSB = new StringBuilder();
                                    if (oProprieta.Valore.Trim() == "")
                                    {
                                        oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = NULL WHERE PROPRIETA = :pPROPRIETA");
                                        oConnection.ExecuteNonQuery(oSB.ToString(), oPars, true);
                                    }
                                    else
                                    {
                                        oPars.Add(":pVALORE", oProprieta.Valore);
                                        oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = :pVALORE WHERE PROPRIETA = :pPROPRIETA");
                                        oConnection.ExecuteNonQuery(oSB.ToString(), oPars, true);
                                    }
                                }
                            }
                            oRS.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Proprieta riepiloghi " + Proprieta, "Errore imprevisto nella modifica della proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta);
                }
                else
                {
                    oRet = new ResultBaseProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Proprieta); ;
                    oRet.Valore = Valore;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;

        }

        public static ResultBaseListProprietaRiepiloghi GetListaProprietaRiepiloghi(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseListProprietaRiepiloghi oRet = new ResultBaseListProprietaRiepiloghi();
            List<Riepiloghi.clsProprietaRiepiloghi> oLista = new List<Riepiloghi.clsProprietaRiepiloghi>();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetListaProprietaRiepiloghi", Token);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oLista = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);

                            Riepiloghi.clsProprietaRiepiloghi oProprietaIpServerFiscale = new Riepiloghi.clsProprietaRiepiloghi();
                            oProprietaIpServerFiscale.Proprieta = "IP_SERVER_FISCALE";
                            oProprietaIpServerFiscale.Descrizione = "IP Server Fiscale";
                            oProprietaIpServerFiscale.Valore = "";
                            oProprietaIpServerFiscale.RegularExpression = "";
                            oProprietaIpServerFiscale.WebRegularExpression = "";
                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in oLista)
                            {
                                if (oItemProprieta.Proprieta == "WINSERVICE_SOCKET_LISTENER_IP")
                                {
                                    oProprietaIpServerFiscale.RegularExpression = oItemProprieta.RegularExpression;
                                    oProprietaIpServerFiscale.WebRegularExpression = oItemProprieta.WebRegularExpression;
                                    break;
                                }
                            }
                            IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT IP FROM SMART_SOCKET");
                            if (!oRS.EOF && !oRS.Fields("IP").IsNull)
                            {
                                oProprietaIpServerFiscale.Valore = oRS.Fields("IP").Value.ToString();
                            }
                            oRS.Close();
                            if (oProprietaIpServerFiscale.Valore.Trim() != "")
                            {
                                oLista.Add(oProprietaIpServerFiscale);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Proprieta riepiloghi", "Errore imprevisto nella lettura della proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
                else
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                    oRet.ListaProprieta = oLista;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseListProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseListProprietaRiepiloghi SetListaProprietaRiepiloghi(string Token, ResultBaseListProprietaRiepiloghi ResultLista, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseListProprietaRiepiloghi oRet = new ResultBaseListProprietaRiepiloghi();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            List<Riepiloghi.clsProprietaRiepiloghi> oLista = new List<Riepiloghi.clsProprietaRiepiloghi>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "SetListaProprietaRiepiloghi", Token, new Dictionary<string, object>() { { "ResultLista", ResultLista } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONFIG_RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            List<Riepiloghi.clsProprietaRiepiloghi> oListaCorrente = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);
                            bool lSave = true;

                            List<Riepiloghi.clsProprietaRiepiloghi> listaToSave = new List<Riepiloghi.clsProprietaRiepiloghi>();
                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in ResultLista.ListaProprieta)
                            {
                                if (oItemProprieta.Proprieta != "IP_SERVER_FISCALE")
                                {
                                    listaToSave.Add(oItemProprieta);
                                }
                            }

                            foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                            {
                                bool lFind = false;
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprietaCorrente in oListaCorrente)
                                {
                                    if (oItemProprieta.Proprieta == oItemProprietaCorrente.Proprieta)
                                    {
                                        lFind = true;
                                        break;
                                    }
                                }
                                if (!lFind)
                                {
                                    lSave = false;
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Proprietà non valida " + oItemProprieta.Proprieta);
                                    break;
                                }
                            }

                            if (lSave)
                            {
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                                {
                                    bool lMatch = false;
                                    foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprietaCorrente in oListaCorrente)
                                    {
                                        if (oItemProprieta.Proprieta == oItemProprietaCorrente.Proprieta)
                                        {
                                            if (oItemProprietaCorrente.RegularExpression.Trim() != "")
                                            {
                                                try
                                                {
                                                    System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex(oItemProprietaCorrente.RegularExpression);
                                                    lMatch = oRegex.IsMatch(oItemProprieta.Valore);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lMatch = false;
                                                }
                                            }
                                            else
                                            {
                                                lMatch = true;
                                            }
                                            break;
                                        }
                                    }
                                    if (!lMatch)
                                    {
                                        lSave = false;
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Valore di Proprietà non valida " + oItemProprieta.Proprieta + " " + oItemProprieta.Valore);
                                        break;
                                    }
                                }
                            }

                            if (lSave)
                            {
                                oConnection.BeginTransaction();
                                foreach (Riepiloghi.clsProprietaRiepiloghi oItemProprieta in listaToSave)
                                {
                                    //if (oItemProprieta.Proprieta != "IP_SERVER_FISCALE")
                                    //{

                                    //}

                                    try
                                    {
                                        clsParameters oPars = new clsParameters();
                                        oPars.Add(":pPROPRIETA", oItemProprieta.Proprieta);

                                        StringBuilder oSB = new StringBuilder();
                                        if (oItemProprieta.Valore.Trim() == "")
                                        {
                                            oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = NULL WHERE PROPRIETA = :pPROPRIETA");
                                            oConnection.ExecuteNonQuery(oSB.ToString(), oPars, false);
                                        }
                                        else
                                        {
                                            oPars.Add(":pVALORE", oItemProprieta.Valore);
                                            oSB.Append("UPDATE PROPRIETA_RIEPILOGHI SET VALORE = :pVALORE WHERE PROPRIETA = :pPROPRIETA");
                                            oConnection.ExecuteNonQuery(oSB.ToString(), oPars, false);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Errore nella modifica della " + oItemProprieta.Proprieta + " " + oItemProprieta.Valore, ex);
                                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore SALVATAGGIO", ex);
                                        break;
                                    }
                                }
                                if (oError == null)
                                {
                                    oConnection.EndTransaction();
                                }
                                else
                                {
                                    oConnection.RollBack();
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oLista = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(oConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Modifica Lista Proprieta riepiloghi", "Errore imprevisto nella modifica della lista delle proprietà.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
                else
                {
                    oRet = new ResultBaseListProprietaRiepiloghi(true, "Proprietà", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                    oRet.ListaProprieta = oLista;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseListProprietaRiepiloghi", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseProprietaRiepiloghi GetPathRPG(string Token, clsEnvironment oEnvironment)
        {
            return GetProprietaRiepiloghi(Token, "PATH_RPG", "c:\\riepiloghi\\giornalieri", oEnvironment);
        }

        public static ResultBaseProprietaRiepiloghi GetPathRPM(string Token, clsEnvironment oEnvironment)
        {
            return GetProprietaRiepiloghi(Token, "PATH_RPM", "c:\\riepiloghi\\mensili", oEnvironment);
        }

        public static ResultBaseProprietaRiepiloghi GetPathEML(string Token, clsEnvironment oEnvironment)
        {
            return GetProprietaRiepiloghi(Token, "PATH_EMAIL", "c:\\riepiloghi\\mailsmime", oEnvironment);
        }


        public class ResultBaseFilesRiepiloghi : ResultBase
        {
            public DateTime Giorno = DateTime.MinValue;
            public Int64 Progressivo = 0;
            public string Tipo = "";
            public string NomeFile = "";

            public ResultBaseFilesRiepiloghi()
            {
            }

            public ResultBaseFilesRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime Giorno, string Tipo)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Giorno = Giorno;
                this.Tipo = Tipo;
            }

            public ResultBaseFilesRiepiloghi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime Giorno, string Tipo, string NomeFile)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Giorno = Giorno;
                this.Tipo = Tipo;
                this.NomeFile = NomeFile;
            }

            public static ResultBaseFilesRiepiloghi Create(string cGiorno, string Tipo, out Exception oError)
            {
                ResultBaseFilesRiepiloghi oRet = new ResultBaseFilesRiepiloghi();
                oError = null;
                oRet.Tipo = Tipo;
                if (!DateTime.TryParseExact(cGiorno, "yyyyMMdd", new System.Globalization.CultureInfo("it-IT"), System.Globalization.DateTimeStyles.None, out oRet.Giorno))
                {
                    oError = new Exception("Formato data non valido.");
                    oRet.StatusMessage = oError.Message;
                    oRet.StatusCode = 1;
                }
                return oRet;
            }
        }

        public static ResultBaseFilesRiepiloghi GetNomeLogGiornoBase(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00");
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeLogGiorno(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00") + ".txt";
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeLogSearchIncrementale(string cGiorno)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "LOG", out oError);
            if (oError == null)
            {
                oRet.NomeFile = "LOG_" + oRet.Giorno.Year.ToString("0000") + "_" + oRet.Giorno.Month.ToString("00") + "_" + oRet.Giorno.Day.ToString("00") + "_*.txt";
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeEmailGiornoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "EML", out oError);
            if (oError == null)
            {
                oRet.NomeFile = libRiepiloghiBase.clsLibSigillo.GetNomeEmailGiornoBase(oRet.Giorno, GiornalieroMensile, nProgressivo);
                oRet.Progressivo = nProgressivo;
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeEmailGiorno(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, "EML", out oError);
            if (oError == null)
            {
                ResultBaseProprietaRiepiloghi oPath = GetPathEML(Token, oEnvironment);
                if (oPath.Valore.Trim() != "")
                {
                    string cPath = oPath.Valore;
                    ResultBaseFilesRiepiloghi oBase = GetNomeEmailGiornoBase(cGiorno, GiornalieroMensile, nProgressivo);
                    if (oBase.NomeFile.Trim() != "")
                    {
                        oRet.NomeFile = cPath + "\\" + oBase.NomeFile;
                        oRet.Progressivo = nProgressivo;
                    }
                    else
                    {
                        oRet.StatusCode = oBase.StatusCode;
                        oRet.StatusMessage = oBase.StatusMessage;
                    }
                }
                else
                {
                    oRet.StatusCode = oPath.StatusCode;
                    oRet.StatusMessage = oPath.StatusMessage;
                }

            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeRiepilogoBase(string cGiorno, string GiornalieroMensile, Int64 nProgressivo)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, (GiornalieroMensile == "G" ? "RPG" : "RPM"), out oError);
            if (oError == null)
            {
                oRet.NomeFile = libRiepiloghiBase.clsLibSigillo.GetNomeRiepilogoBase(oRet.Giorno, GiornalieroMensile, nProgressivo);
                oRet.Progressivo = nProgressivo;
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        public static ResultBaseFilesRiepiloghi GetNomeRiepilogo(string Token, string cGiorno, string GiornalieroMensile, Int64 nProgressivo, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseFilesRiepiloghi oRet = ResultBaseFilesRiepiloghi.Create(cGiorno, (GiornalieroMensile == "G" ? "RPG" : "RPM"), out oError);
            if (oError == null)
            {
                ResultBaseProprietaRiepiloghi oPath = (GiornalieroMensile == "G" ? GetPathRPG(Token,  oEnvironment) : GetPathRPM(Token,  oEnvironment));
                if (oPath.Valore.Trim() != "")
                {
                    string cPath = (GiornalieroMensile == "G" ? GetPathRPG(Token, oEnvironment).Valore : GetPathRPM(Token, oEnvironment).Valore);
                    ResultBaseFilesRiepiloghi oBase = GetNomeRiepilogoBase(cGiorno, GiornalieroMensile, nProgressivo);
                    if (oBase.NomeFile.Trim() != "")
                    {
                        oRet.NomeFile = cPath + "\\" + oBase.NomeFile;
                        oRet.Progressivo = nProgressivo;
                    }
                    else
                    {
                        oRet.StatusCode = oBase.StatusCode;
                        oRet.StatusMessage = oBase.StatusMessage;
                    }
                }
                else
                {
                    oRet.StatusCode = oPath.StatusCode;
                    oRet.StatusMessage = oPath.StatusMessage;
                }
            }
            oRet.execution_time = Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart);
            return oRet;
        }

        #endregion


        #region "Login"

        public class ResultBaseToken : ResultBase
        {
            public string Token = "";
            public Riepiloghi.clsAccount Account = null;

            public ResultBaseToken()
            {
            }

            public ResultBaseToken(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cToken) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.Token = cToken;
            }
        }

        public static ResultBaseToken GetToken(string Login, string Password, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cToken = "";
            string accountId = "";

            bool lCodiceSistemaUnico = false;
            string cCodiceSistemaUnico = "";

            try
            {

                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetToken", "", new Dictionary<string, object>() { { "Login", Login }, { "Password", Password } });
                    DeleteOldRequests(oConnectionInternal);

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);

                    if (lCodiceSistemaUnico)
                    {
                        long idOperatore = 0;

                        // se la modalità è con il codice sistema unico ma il codice sistema non esiste in RIEPILOGHI lo creo la prima volta
                        if (string.IsNullOrEmpty(cCodiceSistemaUnico))
                        {
                            idOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFullCheck(oConnectionInternal, Login, Password, out accountId, out oError, oEnvironment.ExternalConnectionString);
                        }
                        else
                        {
                            idOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnico(oConnectionInternal, cCodiceSistemaUnico, Login, Password, out accountId, out oError, oEnvironment.ExternalConnectionString);
                        }

                        if (oError == null && idOperatore > 0 && !string.IsNullOrEmpty(accountId))
                        {
                            cToken = Riepiloghi.clsToken.GetTokenIdOperatoreAccountId(oConnectionInternal, idOperatore, accountId, out oError);
                        }
                    }
                    else
                        cToken = Riepiloghi.clsToken.GetToken(oConnectionInternal, out oError, Login, Password);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }
                else
                {
                    oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                    //if (!string.IsNullOrEmpty(accountId) && !string.IsNullOrWhiteSpace(accountId))
                    //{
                    //    oRet.Account = new Riepiloghi.clsAccount() { AccountId = accountId };
                    //}
                }

                oRet.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "GetToken", oRet);
                }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        public static ResultBaseToken GetTokenUser(string Login, string Password, string User, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cToken = "";
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetTokenUser", "", new Dictionary<string, object>() { { "Login", Login }, { "Password", Password } });
                    DeleteOldRequests(oConnectionInternal);

                    cToken = Riepiloghi.clsToken.GetTokenUser(oConnectionInternal, out oError, Login, Password, User);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oError != null)
                {

                    oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }
                else
                {
                    oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken); ;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "GetToken", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            //string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return oRet;
        }

        public static ResultBaseToken GetTokenCodiceSistema(string Token, string CodiceSistema, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            string cToken = "";
            ResultBaseToken oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetTokenCodiceSistema", Token);
                    cToken = Riepiloghi.clsToken.GetTokenCodiceSistema(oConnectionInternal, out oError, Token, CodiceSistema);
                }
                if (oError == null)
                {
                    try
                    {
                        CheckInfoCodiceSistema(oConnectionInternal, Token, CodiceSistema, oEnvironment, out oError);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Token", "Errore imprevisto nella richiesta del Token.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseToken(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }
                else
                {
                    oRet = new ResultBaseToken(true, "Token", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), cToken);
                }

                oRet.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "GetTokenCodiceSistema", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            //string jSonClass = Newtonsoft.Json.JsonConvert.SerializeObject(oRet);

            return oRet;
        }

        public class ResultBaseAccountCodiciSistemaAbilitati : ResultBase
        {
            public Riepiloghi.clsCodiceSistema[] ListaCodiciSistema = new Riepiloghi.clsCodiceSistema[] { };

            public ResultBaseAccountCodiciSistemaAbilitati()
            {
            }

            public ResultBaseAccountCodiciSistemaAbilitati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsCodiceSistema[] oCodiciSistema) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.ListaCodiciSistema = oCodiciSistema;
            }
        }

        public static ResultBaseAccountCodiciSistemaAbilitati GetCodiciSistemaAccount(string Token, clsEnvironment oEnvironment)
        {
            ResultBaseAccountCodiciSistemaAbilitati oRet = null;
            List<Riepiloghi.clsCodiceSistema> oLista = new List<Riepiloghi.clsCodiceSistema>();
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, true, true);
                    oLista = oAccount.CodiciSistema;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Codici Sistema", "Errore imprevisto nella richiesta della lista dei codici sistema.", ex);
            }
            finally
            {
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseAccountCodiciSistemaAbilitati(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oLista.ToArray());
            }
            else
            {
                oRet = new ResultBaseAccountCodiciSistemaAbilitati(true, "Lista Codici Sistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oLista.ToArray()); ;
            }
            return oRet;
        }


        #endregion

        #region "Codice sistema"

        public class ResultBaseParametriServerFiscale : ResultBase
        {
            public Int64 IdSocket = 0;
            public string ServerIP = "";
            public int Port = 0;
            public string Client_ID = "";
            public string Password = "";

            public ResultBaseParametriServerFiscale()
            {
            }

            public ResultBaseParametriServerFiscale(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }

            public ResultBaseParametriServerFiscale(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.IdSocket = oParametriServerFiscale.IdSocket;
                this.ServerIP = oParametriServerFiscale.ServerIP;
                this.Port = oParametriServerFiscale.Port;
                this.Client_ID = oParametriServerFiscale.Client_ID;
                this.Password = oParametriServerFiscale.Password;
            }
        }

        public static ResultBaseParametriServerFiscale GetParametriServerFiscale(string Token, clsEnvironment oEnvironment)
        {
            ResultBaseParametriServerFiscale oRet = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            libRiepiloghiBase.clsParametriServerFiscale oParametriServerFiscale = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oParametriServerFiscale = Riepiloghi.clsRiepiloghi.GetParametriServerFiscale(oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseParametriServerFiscale(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                oRet = new ResultBaseParametriServerFiscale(true, "Parametri", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oParametriServerFiscale); ;
            }
            return oRet;
        }

        // Classe di risposta con anche codice sistema
        public class ResultBaseCodiceSistema : ResultBase
        {
            //public string CodiceSistema = "";
            //public string Descrizione = "";
            public Riepiloghi.clsCodiceSistema CodiceSistema = null;

            public ResultBaseCodiceSistema()
            {
            }

            public ResultBaseCodiceSistema(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsCodiceSistema codiceSistema) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                this.CodiceSistema = codiceSistema;
                //this.Descrizione = cDescrizione;
            }
        }

        public static ResultBaseCodiceSistema CheckInfoCodiceSistema(IConnection oConnectionInternal, string Token, string parCodiceSistema, clsEnvironment oEnvironment, out Exception oError)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = null;
            IConnection oConnection = null;
            DateTime dStart = DateTime.Now;
            oError = null;
            string cCodiceSistema = "";
            Riepiloghi.clsCodiceSistema oCodiceSistema = null;
            bool closeConnectionInternal = oConnectionInternal == null;
            try
            {
                if (oConnectionInternal == null)
                    oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CheckInfoCodiceSistema", Token, new Dictionary<string, object>() { { "CodiceSistema", parCodiceSistema } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        string codiceSistema = (parCodiceSistema.Length > 8 && parCodiceSistema.Contains(".") && parCodiceSistema.Split('.')[0].Length == 8 ? parCodiceSistema.Split('.')[0] : parCodiceSistema);
                        Riepiloghi.clsRiepiloghi.CheckUpdateCodiciLocaliPerCodiceSistema(oConnectionInternal, false, codiceSistema, out oError, oEnvironment.ExternalConnectionString, null, out oConnection, false);
                        if (oError == null)
                        {
                            if (oConnection == null)
                                oConnection = Riepiloghi.clsRiepiloghi.GetConnectionByCodiceSistema(oConnectionInternal, codiceSistema, out oError, oEnvironment.ExternalConnectionString);

                            
                            if (oError == null)
                            {
                                cCodiceSistema = Riepiloghi.clsRiepiloghi.GetCodiceSistema(oConnection, out oError);
                                if (oError == null)
                                {
                                    oCodiceSistema = Riepiloghi.clsCodiceSistema.GetCodiceSistemaBase(cCodiceSistema, oConnectionInternal, out oError);
                                    if (parCodiceSistema.Length > 8 && parCodiceSistema.Contains(".") && parCodiceSistema.Split('.')[0].Length == 8)
                                    {
                                        oCodiceSistema.MultiCinema = Riepiloghi.clsMultiCinema.GetMultiCinema(oConnectionInternal, long.Parse(parCodiceSistema.Split('.')[1]), out oError);
                                    }
                                }
                            }

                            if (oError == null && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection))
                            {
                                clsRiepiloghi.CheckUpdateAccountOperatoriMultiCinema(oConnectionInternal, oConnection, false, out oError);
                            }

                            if (oError == null)
                            {
                                bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                                if (!Debugger.IsAttached && lCheckServerFiscale)
                                {
                                    if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "CodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oCodiceSistema);
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseCodiceSistema", oRet);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (closeConnectionInternal)
                    if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        // Richiesta codice sistema
        public static ResultBaseCodiceSistema GetCodiceSistema(string Token, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            ResultBaseCodiceSistema oRet = null;
            IConnection oConnectionInternal = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Riepiloghi.clsCodiceSistema oCodiceSistema = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetCodiceSistema", Token);
                    oCodiceSistema = GetCodiceSistema(Token, oEnvironment, oConnectionInternal, out oError);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oRet = new ResultBaseCodiceSistema(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
                else
                {
                    oRet = new ResultBaseCodiceSistema(true, "CodiceSistema", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oCodiceSistema);
                }

                oRet.Sysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseCodiceSistema", oRet);
                }

                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }

        private static Riepiloghi.clsCodiceSistema GetCodiceSistema(string Token, clsEnvironment oEnvironment, IConnection oConnectionInternal, out Exception oError)
        {
            IConnection oConnection = null;
            oError = null;
            Riepiloghi.clsCodiceSistema oCodiceSistema = null;
            try
            {
                if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                {
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    if (oToken != null && oToken.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema != null && oToken.CodiceSistema.CodiceSistema == "00000000")
                    {
                        if (Riepiloghi.clsAccount.CheckAccountCodiceSistema(oToken.AccountId, oToken.CodiceSistema.CodiceSistema, oConnectionInternal, out oError) && oError == null)
                        {
                            oCodiceSistema = oToken.CodiceSistema;
                        }
                    }
                    else
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            string cCodiceSistema = Riepiloghi.clsRiepiloghi.GetCodiceSistema(oConnection, out oError);
                            if (oError == null)
                            {
                                oCodiceSistema = Riepiloghi.clsCodiceSistema.GetCodiceSistemaBase(cCodiceSistema, oConnectionInternal, out oError);
                            }
                        }

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Codice sistema", "Errore imprevisto nella lettura del codice sistema.", ex);
            }

            return oCodiceSistema;
        }

        // SMART_CARD
        public class ResultBaseSmartCard : ResultBase
        {
            public string CODICE_SISTEMA = "";
            public string NOME_FIRMATARIO = "";
            public string COGNOME_FIRMATARIO = "";
            public string CF_FIRMATARIO = "";
            public string LOCAZIONE = "";
            public string EMAIL_TITOLARE = "";
            public string EMAIL_SIAE = "";
            public string TITOLARE = "";
            public string CF_TITOLARE = "";
            public string RI_TITOLARE = "";
            public string NAZIONE = "";
            public string NR_PROTO = "";
            public DateTime DATA_APPROVAZIONE = DateTime.MinValue;
            public string RAPPR_LEGALE = "";
            public string VERSION = "";
            public string SN_CARD = "";
            public int ENABLED = 0;

            public ResultBaseSmartCard()
            {
            }

            public ResultBaseSmartCard(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = lStatusOK;
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
            }
        }

        public static ResultBaseSmartCard GetInfoSmartCard(string Token, string SerialNumber, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            ResultBaseSmartCard oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetInfoSmartCard", Token, new Dictionary<string, object>() { { "SerialNumber", SerialNumber } });
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            clsParameters oPars = new clsParameters();
                            oPars.Add(":pCODICE_SISTEMA", oToken.CodiceSistema.CodiceSistema);
                            oPars.Add(":pSN_CARD", SerialNumber);
                            oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT CODICE_SISTEMA,NOME_FIRMATARIO,COGNOME_FIRMATARIO,CF_FIRMATARIO,LOCAZIONE,EMAIL_TITOLARE,EMAIL_SIAE,TITOLARE,CF_TITOLARE,RI_TITOLARE,NAZIONE,NR_PROTO,DATA_APPROVAZIONE,RAPPR_LEGALE,VERSION,SN_CARD,ENABLED FROM SMART_CARD WHERE CODICE_SISTEMA = :pCODICE_SISTEMA AND SN_CARD = :pSN_CARD", oPars);
                            if (!oRS.EOF)
                            {
                                oResult = new ResultBaseSmartCard(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.CODICE_SISTEMA = oRS.Fields("CODICE_SISTEMA").Value.ToString();
                                oResult.NOME_FIRMATARIO = oRS.Fields("NOME_FIRMATARIO").Value.ToString();
                                oResult.COGNOME_FIRMATARIO = oRS.Fields("COGNOME_FIRMATARIO").Value.ToString();
                                oResult.CF_FIRMATARIO = oRS.Fields("CF_FIRMATARIO").Value.ToString();
                                oResult.LOCAZIONE = oRS.Fields("LOCAZIONE").Value.ToString();
                                oResult.EMAIL_TITOLARE = oRS.Fields("EMAIL_TITOLARE").Value.ToString();
                                oResult.EMAIL_SIAE = oRS.Fields("EMAIL_SIAE").Value.ToString();
                                oResult.TITOLARE = oRS.Fields("TITOLARE").Value.ToString();
                                oResult.CF_TITOLARE = oRS.Fields("CF_TITOLARE").Value.ToString();
                                oResult.RI_TITOLARE = oRS.Fields("RI_TITOLARE").Value.ToString();
                                oResult.NAZIONE = oRS.Fields("NAZIONE").Value.ToString();
                                oResult.NR_PROTO = oRS.Fields("NR_PROTO").Value.ToString();
                                oResult.DATA_APPROVAZIONE = (DateTime)oRS.Fields("DATA_APPROVAZIONE").Value;
                                oResult.RAPPR_LEGALE = oRS.Fields("RAPPR_LEGALE").Value.ToString();
                                oResult.VERSION = oRS.Fields("VERSION").Value.ToString();
                                oResult.SN_CARD = oRS.Fields("SN_CARD").Value.ToString();
                                oResult.ENABLED = int.Parse(oRS.Fields("ENABLED").Value.ToString());
                            }
                            else
                            {
                                //lock = FieldAccessException;
                                oError = new Exception("Carta non valida");
                            }
                            oRS.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oResult = new ResultBaseSmartCard(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultBaseSmartCard", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oResult;
        }

        #endregion

        #region "Controllo server fiscale"

        // Controllo serve fiscale acceso
        public static ResultBase CheckServerFiscale(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            bool lRet = false;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            lRet = Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione server fiscale", "Errore imprevisto nella connessione al server fiscale.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                return new ResultBase(lRet, lRet.ToString(), 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
        }

        #endregion

        #region "GetTransazioni, GetTransazioniAnnullati, GetTransazioneCARTA_PROGRESSIVO, GetTransazioneCARTA_SIGILLO"

        public class ResultPreFiltro : ResultBase
        {
            public string Campo = "";
            //public List<Riepiloghi.clsValoreFiltroCampoTransazione> Valori = new List<Riepiloghi.clsValoreFiltroCampoTransazione>();
            //public List<Riepiloghi.clsFiltroCampoTransazione> ListaCampi = new List<Riepiloghi.clsFiltroCampoTransazione>();

            public Riepiloghi.clsValoreFiltroCampoTransazione[] Valori = new Riepiloghi.clsValoreFiltroCampoTransazione[] { };
            public Riepiloghi.clsFiltroCampoTransazione[] ListaCampi = new Riepiloghi.clsFiltroCampoTransazione[] { };

            public ResultPreFiltro()
            {
            }

            public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string NomeCampo, Riepiloghi.clsValoreFiltroCampoTransazione[] ListaValori)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Campo = NomeCampo;
                this.Valori = ListaValori;
            }

            public ResultPreFiltro(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsFiltroCampoTransazione[] ListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.ListaCampi = ListaCampi;
            }
        }
        public static ResultPreFiltro GetCampiPreFiltro(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultPreFiltro oResult = new ResultPreFiltro();
            Riepiloghi.clsFiltroCampoTransazione[] oListaCampi = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oListaCampi = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostati(oConnection, out oError);
                            if (oListaCampi == null)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi di filtro", "Lista campi di filtro non caricati.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi di filtro", "Errore imprevisto durante la richiesta della lista dei campi di filtro.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultPreFiltro(true, "Lista campi di filtro", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
            }
            else
            {
                oResult = new ResultPreFiltro(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null, null);
            }
            return oResult;
        }

        public static ResultPreFiltro GetValoriPreFiltro(string Token, string NomeCampo, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultPreFiltro oResult = new ResultPreFiltro();
            Riepiloghi.clsFiltroCampoTransazione oFiltro = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oFiltro = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostatiCampo(oConnection, out oError, NomeCampo, true);
                            if (oFiltro == null)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista valori di filtro", "Lista valori di filtro non caricati.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista valori di filtro", "Errore imprevisto durante la richiesta della lista dei valori di filtro.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultPreFiltro(true, "Lista valori di filtro", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oFiltro.Campo, oFiltro.ValoriPredefiniti);
            }
            else
            {
                oResult = new ResultPreFiltro(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null, null);
            }
            return oResult;
        }

        // Classe per filtri transazioni
        //[XmlRoot("dictionary")]
        public class clsDizionarioFiltri<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }
        }

        // Classe parametro per estrarre transazioni filtrate
        public class clsFiltroTransazioni
        {
            public clsDizionarioFiltri<string, object> Filtri;

            public clsFiltroTransazioni()
            {
            }

            public clsFiltroTransazioni(IConnection Connection, string cFiltro, out Exception oError)
            {
                oError = null;
                this.Filtri = DecodeFiltri(cFiltro, Connection, out oError);
            }

            public clsFiltroTransazioni(clsDizionarioFiltri<string, object> IFiltri)
            {
                this.Filtri = IFiltri;
            }

            // Compone una lista di filtri per la lista delle transazioni
            public static clsDizionarioFiltri<string, object> DecodeFiltri(string cFiltri, IConnection oConnection, out Exception oError)
            {
                clsDizionarioFiltri<string, object> oRet = null;
                oError = null;
                if (cFiltri.Trim() != "")
                {
                    System.Collections.SortedList oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError, false);
                    if (oError == null && oListaCampi != null && oListaCampi.Count > 0)
                    {
                        oRet = new clsDizionarioFiltri<string, object>();
                        if (!cFiltri.Contains(";"))
                        {
                            cFiltri += ";";
                        }
                        foreach (string cValoreFiltro in cFiltri.Split(';'))
                        {
                            if (cValoreFiltro.Contains("="))
                            {
                                string cCampo = cValoreFiltro.Split('=')[0];
                                string cValue = cValoreFiltro.Split('=')[1];
                                Riepiloghi.clsCampoTransazione oCampo = null;
                                foreach (System.Collections.DictionaryEntry ItemCampoDic in oListaCampi)
                                {
                                    Riepiloghi.clsCampoTransazione oItemCampo = (Riepiloghi.clsCampoTransazione)ItemCampoDic.Value;
                                    if (oItemCampo.Campo == cCampo)
                                    {
                                        oCampo = oItemCampo;
                                        break;
                                    }
                                }

                                if (oCampo == null)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "campo filtro [" + cCampo + "] non valido.");
                                }
                                else
                                {
                                    if (oCampo.Tipo == "S")
                                    {
                                        if (cValue.Length > oCampo.Dimensione)
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore TESTO per il campo " + cCampo + " troppo lungo " + cValue);
                                        }
                                        else
                                        {
                                            oRet.Add(oCampo.Campo, cValue.PadRight(oCampo.Dimensione));
                                        }

                                    }
                                    else if (oCampo.Tipo == "N")
                                    {
                                        Int64 nValue = 0;
                                        if (Int64.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore NUMERICO per il campo " + cCampo + " non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "C")
                                    {
                                        decimal nValue = 0;
                                        if (decimal.TryParse(cValue, out nValue))
                                        {
                                            oRet.Add(oCampo.Campo, nValue);
                                        }
                                        else
                                        {
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore IMPORTO per il campo " + cCampo + " [0,00] non valido " + cValue);
                                        }
                                    }
                                    else if (oCampo.Tipo == "D")
                                    {
                                        DateTime dValue;
                                        if (cValue.Length == 8)
                                        {
                                            try
                                            {
                                                dValue = new DateTime(int.Parse(cValue.Substring(0, 4)), int.Parse(cValue.Substring(4, 2)), int.Parse(cValue.Substring(6, 2)));
                                                oRet.Add(oCampo.Campo, dValue);
                                            }
                                            catch
                                            {
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Formato Valore DATA per il campo " + cCampo + " [YYYYMMDD] non valido " + cValue);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (oError == null)
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi per filtri", "Errore durante la lettura dei campi di filtro.");
                        }
                    }
                }
                return oRet;
            }
        }

        public class clsResult_LtaParziale
        {
            public string CfTitolareCa { get; set; }
            public string CodiceSistemaCa { get; set; }
            public string DataAperturaAccessi { get; set; }
            public string OraAperturaAccessi { get; set; }
            public DateTime? DataOraAperturaAccessi { get; set; }
            public decimal? CorrispettivoLordo { get; set; }
            public long? NumeroEventiAbilitati { get; set; }
            public string DataAnnullamento { get; set; }
            public string OraAnnullamento { get; set; }
            public string CodiceCartaAnnullamento { get; set; }
            public long? ProgressivoTitoloAnn { get; set; }
            public string SigilloAnnullamento { get; set; }
            public string CodiceSuppIdentificativo { get; set; }
            public string DescrSuppIdentificativo { get; set; }
            public string IdentificativoSupp { get; set; }
            public string IdentificativoSuppAlt { get; set; }
            public string DataNascitaPartecipante { get; set; }
            public string LuogoNascitaPartecipante { get; set; }
            public string DataInserimentoLta { get; set; }
            public string OraInserimentoLta { get; set; }
            public DateTime? DataOraInserimentoLta { get; set; }
            public string StatoTitolo { get; set; }
            public string DataIngressoCa { get; set; }
            public string OraIngressoCa { get; set; }
            public DateTime? DataOraIngressoCa { get; set; }

        }

        public class clsResult_LogParziale
        {
            public string CodiceFiscaleTitolare { get; set; }
            public string TitoloAbbonamento { get; set; }
            public string TitoloIvaPreassolta { get; set; }
            public string SpettacoloIntrattenimento { get; set; }
            public string Valuta { get; set; }
            public decimal? ImponibileIntrattenimenti { get; set; }
            public string CodiceRichiedenteEmissioneSigillo { get; set; }
            public string Prestampa { get; set; }
            public string CausaleOmaggioRiduzione { get; set; }
            public decimal? RateoEvento { get; set; }
            public decimal? IvaRateo { get; set; }
            public decimal? RateoImponibileIntra { get; set; }
            public string CausaleOmaggioRiduzioneOpen { get; set; }
            public decimal? CorrispettivoTitolo { get; set; }
            public decimal? CorrispettivoPrevendita { get; set; }
            public decimal? IvaTitolo { get; set; }
            public decimal? IvaPrevendita { get; set; }
            public decimal? CorrispettivoFigurativo { get; set; }
            public decimal? IvaFigurativa { get; set; }
            public string CodicePrestazione1 { get; set; }
            public decimal? ImportoPrestazione1 { get; set; }
            public string CodicePrestazione2 { get; set; }
            public decimal? ImportoPrestazione2 { get; set; }
            public string CodicePrestazione3 { get; set; }
            public decimal? ImportoPrestazione3 { get; set; }
            public string CausaleAnnullamento { get; set; }
            public string AcqregAutenticazione { get; set; }
            public string AcqregCodiceunivocoAcq { get; set; }
            public string AcqregIndirizzoipReg { get; set; }
            public DateTime? AcqregDataoraReg { get; set; }
            public string AcqtranCodiceunivocoNumTran { get; set; }
            public string AcqtranCellulareAcq { get; set; }
            public string AcqtranEmailAcq { get; set; }
            public string AcqtranIndirizzoIpTran { get; set; }
            public DateTime? AcqtranDataorainiziocheckout { get; set; }
            public DateTime? AcqtranDataoraesecuzionePag { get; set; }
            public string AcqtranCro { get; set; }
            public string AcqtranMetodoSpedTitolo { get; set; }
            public string AcqtranIndirizzoSpedTitolo { get; set; }
            public string DigitaleTradizionale { get; set; }
        }

        // Classe con i campi del log delle transazioni
        public class clsResultTransazione
        {
            public string CodiceFiscaleOrganizzatore { get; set; }
            public string CodiceFiscaleTitolare { get; set; }
            public string TitoloAbbonamento { get; set; }
            public string TitoloIvaPreassolta { get; set; }
            public string SpettacoloIntrattenimento { get; set; }
            public string Valuta { get; set; }
            public decimal? ImponibileIntrattenimenti { get; set; }
            public string OrdineDiPosto { get; set; }
            public string Posto { get; set; }
            public string TipoTitolo { get; set; }
            public string Annullato { get; set; }
            public decimal? ProgressivoAnnullati { get; set; }
            public string DataEmissioneAnnullamento { get; set; }
            public string OraEmissioneAnnullamento { get; set; }
            public DateTime? DataOraEmissioneAnnullamento { get; set; }
            public Int64? ProgressivoTitolo { get; set; }
            public string CodiceRichiedenteEmissioneSigillo { get; set; }
            public string Sigillo { get; set; }
            public string CodiceSistema { get; set; }
            public string CodiceCarta { get; set; }
            public string Prestampa { get; set; }
            public string CodiceLocale { get; set; }
            public string DataEvento { get; set; }
            public DateTime? DataOraEvento { get; set; }
            public string TipoEvento { get; set; }
            public string TitoloEvento { get; set; }
            public string OraEvento { get; set; }
            public string CausaleOmaggioRiduzione { get; set; }
            public string TipoTurno { get; set; }
            public Int64? NumeroEventiAbilitati { get; set; }
            public string DataLimiteValidita { get; set; }
            public DateTime? DataScadenzaAbbonamento { get; set; }
            public string CodiceAbbonamento { get; set; }
            public decimal? NumProgAbbonamento { get; set; }
            public decimal? RateoEvento { get; set; }
            public decimal? IvaRateo { get; set; }
            public decimal? RateoImponibileIntra { get; set; }
            public string CausaleOmaggioRiduzioneOpen { get; set; }
            public decimal? CorrispettivoTitolo { get; set; }
            public decimal? CorrispettivoPrevendita { get; set; }
            public decimal? IvaTitolo { get; set; }
            public decimal? IvaPrevendita { get; set; }
            public decimal? CorrispettivoFigurativo { get; set; }
            public decimal? IvaFigurativa { get; set; }
            public string CodiceFiscaleAbbonamento { get; set; }
            public string CodiceBigliettoAbbonamento { get; set; }
            public Int64? NumProgBigliettoAbbonamento { get; set; }
            public string CodicePrestazione1 { get; set; }
            public decimal? ImportoPrestazione1 { get; set; }
            public string CodicePrestazione2 { get; set; }
            public decimal? ImportoPrestazione2 { get; set; }
            public string CodicePrestazione3 { get; set; }
            public decimal? ImportoPrestazione3 { get; set; }
            public string CartaOriginaleAnnullato { get; set; }
            public string CausaleAnnullamento { get; set; }

            public string PartecipanteNome { get; set; }
            public string PartecipanteCognome { get; set; }
            public string AcqregAutenticazione { get; set; }
            public string AcqregCodiceunivocoAcq { get; set; }
            public string AcqregIndirizzoipReg { get; set; }
            public DateTime? AcqregDataoraReg { get; set; }
            public string AcqtranCodiceunivocoNumTran { get; set; }
            public string AcqtranCellulareAcq { get; set; }
            public string AcqtranEmailAcq { get; set; }
            public string AcqtranIndirizzoIpTran { get; set; }
            public DateTime? AcqtranDataorainiziocheckout { get; set; }
            public DateTime? AcqtranDataoraesecuzionePag { get; set; }
            public string AcqtranCro { get; set; }
            public string AcqtranMetodoSpedTitolo { get; set; }
            public string AcqtranIndirizzoSpedTitolo { get; set; }
            public string DigitaleTradizionale { get; set; }

            public clsResult_LtaParziale Lta { get; set; }

            public clsResultTransazione()
            {
            }

            public clsResultTransazione(IRecordSet oRS, bool loadLta)
            {
                if (oRS != null && !oRS.EOF)
                {
                    this.CodiceFiscaleOrganizzatore = (oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").Value.ToString());
                    this.CodiceFiscaleTitolare = (oRS.Fields("CODICE_FISCALE_TITOLARE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_TITOLARE").Value.ToString());
                    this.TitoloAbbonamento = (oRS.Fields("TITOLO_ABBONAMENTO").IsNull ? "" : oRS.Fields("TITOLO_ABBONAMENTO").Value.ToString());
                    this.TitoloIvaPreassolta = (oRS.Fields("TITOLO_IVA_PREASSOLTA").IsNull ? "" : oRS.Fields("TITOLO_IVA_PREASSOLTA").Value.ToString());
                    this.SpettacoloIntrattenimento = (oRS.Fields("SPETTACOLO_INTRATTENIMENTO").IsNull ? "" : oRS.Fields("SPETTACOLO_INTRATTENIMENTO").Value.ToString());
                    this.Valuta = (oRS.Fields("VALUTA").IsNull ? "" : oRS.Fields("VALUTA").Value.ToString());

                    if (!oRS.Fields("IMPONIBILE_INTRATTENIMENTI").IsNull) this.ImponibileIntrattenimenti = decimal.Parse(oRS.Fields("IMPONIBILE_INTRATTENIMENTI").Value.ToString());

                    this.OrdineDiPosto = (oRS.Fields("ORDINE_DI_POSTO").IsNull ? "" : oRS.Fields("ORDINE_DI_POSTO").Value.ToString());
                    this.Posto = (oRS.Fields("POSTO").IsNull ? "" : oRS.Fields("POSTO").Value.ToString());
                    this.TipoTitolo = (oRS.Fields("TIPO_TITOLO").IsNull ? "" : oRS.Fields("TIPO_TITOLO").Value.ToString());
                    this.Annullato = (oRS.Fields("ANNULLATO").IsNull ? "" : oRS.Fields("ANNULLATO").Value.ToString());
                    if (this.Annullato == "A")
                        this.Annullato = "S";
                    else
                        this.Annullato = "N";
                    if (!oRS.Fields("PROGRESSIVO_ANNULLATI").IsNull) this.ProgressivoAnnullati = Int64.Parse(oRS.Fields("PROGRESSIVO_ANNULLATI").Value.ToString());
                    this.DataEmissioneAnnullamento = (oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("DATA_EMISSIONE_ANNULLAMENTO").Value.ToString());
                    this.OraEmissioneAnnullamento = (oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("ORA_EMISSIONE_ANNULLAMENTO").Value.ToString());
                    
                    if (!oRS.Fields("PROGRESSIVO_TITOLO").IsNull) this.ProgressivoTitolo = Int64.Parse(oRS.Fields("PROGRESSIVO_TITOLO").Value.ToString());
                    this.CodiceRichiedenteEmissioneSigillo = (oRS.Fields("CODICE_PUNTO_VENDITA").IsNull ? "" : oRS.Fields("CODICE_PUNTO_VENDITA").Value.ToString());
                    this.Sigillo = (oRS.Fields("SIGILLO").IsNull ? "" : oRS.Fields("SIGILLO").Value.ToString());
                    this.CodiceSistema = (oRS.Fields("CODICE_SISTEMA").IsNull ? "" : oRS.Fields("CODICE_SISTEMA").Value.ToString());
                    this.CodiceCarta = (oRS.Fields("CODICE_CARTA").IsNull ? "" : oRS.Fields("CODICE_CARTA").Value.ToString());
                    this.Prestampa = (oRS.Fields("PRESTAMPA").IsNull ? "" : oRS.Fields("PRESTAMPA").Value.ToString());
                    this.CodiceLocale = (oRS.Fields("CODICE_LOCALE").IsNull ? "" : oRS.Fields("CODICE_LOCALE").Value.ToString());
                    this.DataEvento = (oRS.Fields("DATA_EVENTO").IsNull ? "" : oRS.Fields("DATA_EVENTO").Value.ToString());
                    this.TipoEvento = (oRS.Fields("TIPO_EVENTO").IsNull ? "" : oRS.Fields("TIPO_EVENTO").Value.ToString());
                    this.TitoloEvento = (oRS.Fields("TITOLO_EVENTO").IsNull ? "" : oRS.Fields("TITOLO_EVENTO").Value.ToString());
                    this.OraEvento = (oRS.Fields("ORA_EVENTO").IsNull ? "" : oRS.Fields("ORA_EVENTO").Value.ToString());
                    this.CausaleOmaggioRiduzione = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").Value.ToString());
                    this.TipoTurno = (oRS.Fields("TIPO_TURNO").IsNull ? "" : oRS.Fields("TIPO_TURNO").Value.ToString());
                    if (!oRS.Fields("NUMERO_EVENTI_ABILITATI").IsNull) this.NumeroEventiAbilitati = Int64.Parse(oRS.Fields("NUMERO_EVENTI_ABILITATI").Value.ToString());
                    this.DataLimiteValidita = (oRS.Fields("DATA_LIMITE_VALIDITA").IsNull ? "" : oRS.Fields("DATA_LIMITE_VALIDITA").Value.ToString());
                    
                    this.CodiceAbbonamento = (oRS.Fields("CODICE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("NUM_PROG_ABBONAMENTO").IsNull) this.NumProgAbbonamento = Int64.Parse(oRS.Fields("NUM_PROG_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("RATEO_EVENTO").IsNull) this.RateoEvento = decimal.Parse(oRS.Fields("RATEO_EVENTO").Value.ToString());
                    if (!oRS.Fields("IVA_RATEO").IsNull) this.IvaRateo = decimal.Parse(oRS.Fields("IVA_RATEO").Value.ToString());
                    if (!oRS.Fields("RATEO_IMPONIBILE_INTRA").IsNull) this.RateoImponibileIntra = decimal.Parse(oRS.Fields("RATEO_IMPONIBILE_INTRA").Value.ToString());
                    this.CausaleOmaggioRiduzioneOpen = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_TITOLO").IsNull) this.CorrispettivoTitolo = decimal.Parse(oRS.Fields("CORRISPETTIVO_TITOLO").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_PREVENDITA").IsNull) this.CorrispettivoPrevendita = decimal.Parse(oRS.Fields("CORRISPETTIVO_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("IVA_TITOLO").IsNull) this.IvaTitolo = decimal.Parse(oRS.Fields("IVA_TITOLO").Value.ToString());
                    if (!oRS.Fields("IVA_PREVENDITA").IsNull) this.IvaPrevendita = decimal.Parse(oRS.Fields("IVA_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_FIGURATIVO").IsNull) this.CorrispettivoFigurativo = decimal.Parse(oRS.Fields("CORRISPETTIVO_FIGURATIVO").Value.ToString());
                    if (!oRS.Fields("IVA_FIGURATIVA").IsNull) this.IvaFigurativa = decimal.Parse(oRS.Fields("IVA_FIGURATIVA").Value.ToString());
                    this.CodiceFiscaleAbbonamento = (oRS.Fields("CODICE_FISCALE_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_FISCALE_ABBONAMENTO").Value.ToString());
                    this.CodiceBigliettoAbbonamento = (oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").IsNull ? "" : oRS.Fields("CODICE_BIGLIETTO_ABBONAMENTO").Value.ToString());
                    if (!oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").IsNull) this.NumProgBigliettoAbbonamento = Int64.Parse(oRS.Fields("NUM_PROG_BIGLIETTO_ABBONAMENTO").Value.ToString());
                    this.CodicePrestazione1 = (oRS.Fields("CODICE_PRESTAZIONE1").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE1").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE1").IsNull) this.ImportoPrestazione1 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE1").Value.ToString());
                    this.CodicePrestazione2 = (oRS.Fields("CODICE_PRESTAZIONE2").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE2").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE2").IsNull) this.ImportoPrestazione2 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE2").Value.ToString());
                    this.CodicePrestazione3 = (oRS.Fields("CODICE_PRESTAZIONE3").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE3").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE3").IsNull) this.ImportoPrestazione3 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE3").Value.ToString());
                    this.CartaOriginaleAnnullato = (oRS.Fields("CARTA_ORIGINALE_ANNULLATO").IsNull ? "" : oRS.Fields("CARTA_ORIGINALE_ANNULLATO").Value.ToString());
                    this.CausaleAnnullamento = (oRS.Fields("CAUSALE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("CAUSALE_ANNULLAMENTO").Value.ToString());

                    this.PartecipanteNome = (oRS.Fields("PARTECIPANTE_NOME").IsNull ? "" : oRS.Fields("PARTECIPANTE_NOME").Value.ToString());
                    this.PartecipanteCognome = (oRS.Fields("PARTECIPANTE_COGNOME").IsNull ? "" : oRS.Fields("PARTECIPANTE_COGNOME").Value.ToString());
                    this.AcqregAutenticazione = (oRS.Fields("ACQREG_AUTENTICAZIONE").IsNull ? "" : oRS.Fields("ACQREG_AUTENTICAZIONE").Value.ToString());
                    this.AcqregCodiceunivocoAcq = (oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").IsNull ? "" : oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").Value.ToString());
                    this.AcqregIndirizzoipReg = (oRS.Fields("ACQREG_INDIRIZZOIP_REG").IsNull ? "" : oRS.Fields("ACQREG_INDIRIZZOIP_REG").Value.ToString());
                    if (!oRS.Fields("ACQREG_DATAORA_REG").IsNull) this.AcqregDataoraReg = (DateTime)oRS.Fields("ACQREG_DATAORA_REG").Value;
                    this.AcqtranCodiceunivocoNumTran = (oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").Value.ToString());
                    this.AcqtranCellulareAcq = (oRS.Fields("ACQTRAN_CELLULARE_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_CELLULARE_ACQ").Value.ToString());
                    this.AcqtranEmailAcq = (oRS.Fields("ACQTRAN_EMAIL_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_EMAIL_ACQ").Value.ToString());
                    this.AcqtranIndirizzoIpTran = (oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").Value.ToString());
                    if (!oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").IsNull) this.AcqtranDataorainiziocheckout = (DateTime)oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").Value;
                    if (!oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").IsNull) this.AcqtranDataoraesecuzionePag = (DateTime)oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").Value;
                    this.AcqtranCro = (oRS.Fields("ACQTRAN_CRO").IsNull ? "" : oRS.Fields("ACQTRAN_CRO").Value.ToString());
                    this.AcqtranMetodoSpedTitolo = (oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").Value.ToString());
                    this.AcqtranIndirizzoSpedTitolo = (oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").Value.ToString());
                    this.DigitaleTradizionale = (oRS.Fields("DIGITALE_TRADIZIONALE").IsNull ? "" : oRS.Fields("DIGITALE_TRADIZIONALE").Value.ToString());

                    if (((System.Data.DataTable)oRS).Columns.Contains("DATAORA_EMISSIONE_ANNULLAMENTO"))
                        if (!oRS.Fields("DATAORA_EMISSIONE_ANNULLAMENTO").IsNull) this.DataOraEmissioneAnnullamento = (DateTime)oRS.Fields("DATAORA_EMISSIONE_ANNULLAMENTO").Value;
                    if (((System.Data.DataTable)oRS).Columns.Contains("DATA_ORA_EVENTO"))
                        if (!oRS.Fields("DATA_ORA_EVENTO").IsNull) this.DataOraEvento = (DateTime)oRS.Fields("DATA_ORA_EVENTO").Value;
                    if (((System.Data.DataTable)oRS).Columns.Contains("DATA_SCADENZA_ABBONAMENTO"))
                        if (!oRS.Fields("DATA_SCADENZA_ABBONAMENTO").IsNull) this.DataScadenzaAbbonamento = (DateTime)oRS.Fields("DATA_SCADENZA_ABBONAMENTO").Value;

                    if (loadLta)
                    {
                        this.Lta = new clsResult_LtaParziale();
                        this.Lta.CfTitolareCa = (!oRS.Fields("CF_TITOLARE_CA").IsNull ? oRS.Fields("CF_TITOLARE_CA").Value.ToString() : "");
                        this.Lta.CodiceSistemaCa = (!oRS.Fields("CODICE_SISTEMA_CA").IsNull ? oRS.Fields("CODICE_SISTEMA_CA").Value.ToString() : "");
                        this.Lta.DataAperturaAccessi = (!oRS.Fields("DATA_APERTURA_ACCESSI").IsNull ? oRS.Fields("DATA_APERTURA_ACCESSI").Value.ToString() : "");
                        this.Lta.OraAperturaAccessi = (!oRS.Fields("ORA_APERTURA_ACCESSI").IsNull ? oRS.Fields("ORA_APERTURA_ACCESSI").Value.ToString() : "");
                        if (!oRS.Fields("DATA_ORA_APERTURA_ACCESSI").IsNull)
                            this.Lta.DataOraAperturaAccessi = (DateTime)oRS.Fields("DATA_ORA_APERTURA_ACCESSI").Value;
                        if (!oRS.Fields("CORRISPETTIVO_LORDO").IsNull)
                            this.Lta.CorrispettivoLordo = decimal.Parse(oRS.Fields("CORRISPETTIVO_LORDO").Value.ToString());
                        if (!oRS.Fields("NUMERO_EVENTI_ABILITATI").IsNull)
                            this.Lta.NumeroEventiAbilitati = long.Parse(oRS.Fields("NUMERO_EVENTI_ABILITATI").Value.ToString());
                        this.Lta.DataAnnullamento = (!oRS.Fields("DATA_ANNULLAMENTO").IsNull ? oRS.Fields("DATA_ANNULLAMENTO").Value.ToString() : "");
                        this.Lta.OraAnnullamento = (!oRS.Fields("ORA_ANNULLAMENTO").IsNull ? oRS.Fields("ORA_ANNULLAMENTO").Value.ToString() : "");
                        this.Lta.CodiceCartaAnnullamento = (!oRS.Fields("CODICE_CARTA_ANNULLAMENTO").IsNull ? oRS.Fields("CODICE_CARTA_ANNULLAMENTO").Value.ToString() : "");
                        if (!oRS.Fields("PROGRESSIVO_TITOLO_ANN").IsNull)
                            this.Lta.ProgressivoTitoloAnn = long.Parse(oRS.Fields("PROGRESSIVO_TITOLO_ANN").Value.ToString());
                        this.Lta.SigilloAnnullamento = (!oRS.Fields("SIGILLO_ANNULLAMENTO").IsNull ? oRS.Fields("SIGILLO_ANNULLAMENTO").Value.ToString() : "");
                        this.Lta.CodiceSuppIdentificativo = (!oRS.Fields("CODICE_SUPP_IDENTIFICATIVO").IsNull ? oRS.Fields("CODICE_SUPP_IDENTIFICATIVO").Value.ToString() : "");
                        this.Lta.DescrSuppIdentificativo = (!oRS.Fields("DESCR_SUPP_IDENTIFICATIVO").IsNull ? oRS.Fields("DESCR_SUPP_IDENTIFICATIVO").Value.ToString() : "");
                        this.Lta.IdentificativoSupp = (!oRS.Fields("IDENTIFICATIVO_SUPP").IsNull ? oRS.Fields("IDENTIFICATIVO_SUPP").Value.ToString() : "");
                        this.Lta.IdentificativoSuppAlt = (!oRS.Fields("IDENTIFICATIVO_SUPP_ALT").IsNull ? oRS.Fields("IDENTIFICATIVO_SUPP_ALT").Value.ToString() : "");
                        this.Lta.DataNascitaPartecipante = (!oRS.Fields("DATA_NASCITA_PARTECIPANTE").IsNull ? oRS.Fields("DATA_NASCITA_PARTECIPANTE").Value.ToString() : "");
                        this.Lta.LuogoNascitaPartecipante = (!oRS.Fields("LUOGO_NASCITA_PARTECIPANTE").IsNull ? oRS.Fields("LUOGO_NASCITA_PARTECIPANTE").Value.ToString() : "");
                        this.Lta.DataInserimentoLta = (!oRS.Fields("DATA_INSERIMENTO_LTA").IsNull ? oRS.Fields("DATA_INSERIMENTO_LTA").Value.ToString() : "");
                        this.Lta.OraInserimentoLta = (!oRS.Fields("ORA_INSERIMENTO_LTA").IsNull ? oRS.Fields("ORA_INSERIMENTO_LTA").Value.ToString() : "");
                        if (!oRS.Fields("DATA_ORA_INSERIMENTO_LTA").IsNull)
                            this.Lta.DataOraInserimentoLta = (DateTime)oRS.Fields("DATA_ORA_INSERIMENTO_LTA").Value;
                        this.Lta.StatoTitolo = (!oRS.Fields("STATO_TITOLO").IsNull ? oRS.Fields("STATO_TITOLO").Value.ToString() : "");
                        this.Lta.DataIngressoCa = (!oRS.Fields("DATA_INGRESSO_CA").IsNull ? oRS.Fields("DATA_INGRESSO_CA").Value.ToString() : "");
                        this.Lta.OraIngressoCa = (!oRS.Fields("ORA_INGRESSO_CA").IsNull ? oRS.Fields("ORA_INGRESSO_CA").Value.ToString() : "");
                        if (!oRS.Fields("DATA_ORA_INGRESSO_CA").IsNull)
                            this.Lta.DataOraIngressoCa = (DateTime)oRS.Fields("DATA_ORA_INGRESSO_CA").Value;
                    }
                }
            }



            public clsResultTransazione(System.Data.DataRow oRow, bool loadLta)
            {
                if (oRow != null)
                {
                    this.CodiceFiscaleOrganizzatore = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? "" : oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString());
                    this.CodiceFiscaleTitolare = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_TITOLARE"].ToString()) ? "" : oRow["CODICE_FISCALE_TITOLARE"].ToString());
                    this.TitoloAbbonamento = (string.IsNullOrEmpty(oRow["TITOLO_ABBONAMENTO"].ToString()) ? "" : oRow["TITOLO_ABBONAMENTO"].ToString());
                    this.TitoloIvaPreassolta = (string.IsNullOrEmpty(oRow["TITOLO_IVA_PREASSOLTA"].ToString()) ? "" : oRow["TITOLO_IVA_PREASSOLTA"].ToString());
                    this.SpettacoloIntrattenimento = (string.IsNullOrEmpty(oRow["SPETTACOLO_INTRATTENIMENTO"].ToString()) ? "" : oRow["SPETTACOLO_INTRATTENIMENTO"].ToString());
                    this.Valuta = (string.IsNullOrEmpty(oRow["VALUTA"].ToString()) ? "" : oRow["VALUTA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString())) this.ImponibileIntrattenimenti = decimal.Parse(oRow["IMPONIBILE_INTRATTENIMENTI"].ToString());
                    this.OrdineDiPosto = (string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? "" : oRow["ORDINE_DI_POSTO"].ToString());
                    this.Posto = (string.IsNullOrEmpty(oRow["POSTO"].ToString()) ? "" : oRow["POSTO"].ToString());
                    this.TipoTitolo = (string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? "" : oRow["TIPO_TITOLO"].ToString());
                    this.Annullato = (string.IsNullOrEmpty(oRow["ANNULLATO"].ToString()) ? "" : oRow["ANNULLATO"].ToString());
                    if (this.Annullato == "A")
                        this.Annullato = "S";
                    else
                        this.Annullato = "N";
                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_ANNULLATI"].ToString())) this.ProgressivoAnnullati = Int64.Parse(oRow["PROGRESSIVO_ANNULLATI"].ToString());
                    this.DataEmissioneAnnullamento = (string.IsNullOrEmpty(oRow["DATA_EMISSIONE_ANNULLAMENTO"].ToString()) ? "" : oRow["DATA_EMISSIONE_ANNULLAMENTO"].ToString());
                    this.OraEmissioneAnnullamento = (string.IsNullOrEmpty(oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString()) ? "" : oRow["ORA_EMISSIONE_ANNULLAMENTO"].ToString());

                    

                    if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO"].ToString())) this.ProgressivoTitolo = Int64.Parse(oRow["PROGRESSIVO_TITOLO"].ToString());
                    this.CodiceRichiedenteEmissioneSigillo = (string.IsNullOrEmpty(oRow["CODICE_PUNTO_VENDITA"].ToString()) ? "" : oRow["CODICE_PUNTO_VENDITA"].ToString());
                    this.Sigillo = (string.IsNullOrEmpty(oRow["SIGILLO"].ToString()) ? "" : oRow["SIGILLO"].ToString());
                    this.CodiceSistema = (string.IsNullOrEmpty(oRow["CODICE_SISTEMA"].ToString()) ? "" : oRow["CODICE_SISTEMA"].ToString());
                    this.CodiceCarta = (string.IsNullOrEmpty(oRow["CODICE_CARTA"].ToString()) ? "" : oRow["CODICE_CARTA"].ToString());
                    this.Prestampa = (string.IsNullOrEmpty(oRow["PRESTAMPA"].ToString()) ? "" : oRow["PRESTAMPA"].ToString());
                    this.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? "" : oRow["CODICE_LOCALE"].ToString());
                    this.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? "" : oRow["DATA_EVENTO"].ToString());
                    this.TipoEvento = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? "" : oRow["TIPO_EVENTO"].ToString());
                    this.TitoloEvento = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? "" : oRow["TITOLO_EVENTO"].ToString());
                    this.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? "" : oRow["ORA_EVENTO"].ToString());
                    this.CausaleOmaggioRiduzione = (string.IsNullOrEmpty(oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString()) ? "" : oRow["CAUSALE_OMAGGIO_RIDUZIONE"].ToString());
                    this.TipoTurno = (string.IsNullOrEmpty(oRow["TIPO_TURNO"].ToString()) ? "" : oRow["TIPO_TURNO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString())) this.NumeroEventiAbilitati = Int64.Parse(oRow["NUMERO_EVENTI_ABILITATI"].ToString());
                    this.DataLimiteValidita = (string.IsNullOrEmpty(oRow["DATA_LIMITE_VALIDITA"].ToString()) ? "" : oRow["DATA_LIMITE_VALIDITA"].ToString());

                    this.CodiceAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUM_PROG_ABBONAMENTO"].ToString())) this.NumProgAbbonamento = Int64.Parse(oRow["NUM_PROG_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["RATEO_EVENTO"].ToString())) this.RateoEvento = decimal.Parse(oRow["RATEO_EVENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_RATEO"].ToString())) this.IvaRateo = decimal.Parse(oRow["IVA_RATEO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["RATEO_IMPONIBILE_INTRA"].ToString())) this.RateoImponibileIntra = decimal.Parse(oRow["RATEO_IMPONIBILE_INTRA"].ToString());
                    this.CausaleOmaggioRiduzioneOpen = (string.IsNullOrEmpty(oRow["CAUSALE_OMAGGIO_RIDUZIONE_OPEN"].ToString()) ? "" : oRow["CAUSALE_OMAGGIO_RIDUZIONE_OPEN"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_TITOLO"].ToString())) this.CorrispettivoTitolo = decimal.Parse(oRow["CORRISPETTIVO_TITOLO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_PREVENDITA"].ToString())) this.CorrispettivoPrevendita = decimal.Parse(oRow["CORRISPETTIVO_PREVENDITA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_TITOLO"].ToString())) this.IvaTitolo = decimal.Parse(oRow["IVA_TITOLO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_PREVENDITA"].ToString())) this.IvaPrevendita = decimal.Parse(oRow["IVA_PREVENDITA"].ToString());
                    if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_FIGURATIVO"].ToString())) this.CorrispettivoFigurativo = decimal.Parse(oRow["CORRISPETTIVO_FIGURATIVO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IVA_FIGURATIVA"].ToString())) this.IvaFigurativa = decimal.Parse(oRow["IVA_FIGURATIVA"].ToString());
                    this.CodiceFiscaleAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_FISCALE_ABBONAMENTO"].ToString());
                    this.CodiceBigliettoAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString()) ? "" : oRow["CODICE_BIGLIETTO_ABBONAMENTO"].ToString());
                    if (!string.IsNullOrEmpty(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString())) this.NumProgBigliettoAbbonamento = Int64.Parse(oRow["NUM_PROG_BIGLIETTO_ABBONAMENTO"].ToString());
                    this.CodicePrestazione1 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE1"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE1"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE1"].ToString())) this.ImportoPrestazione1 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE1"].ToString());
                    this.CodicePrestazione2 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE2"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE2"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) this.ImportoPrestazione2 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE2"].ToString());
                    this.CodicePrestazione3 = (string.IsNullOrEmpty(oRow["CODICE_PRESTAZIONE3"].ToString()) ? "" : oRow["CODICE_PRESTAZIONE3"].ToString());
                    if (!string.IsNullOrEmpty(oRow["IMPORTO_PRESTAZIONE3"].ToString())) this.ImportoPrestazione3 = decimal.Parse(oRow["IMPORTO_PRESTAZIONE3"].ToString());
                    this.CartaOriginaleAnnullato = (string.IsNullOrEmpty(oRow["CARTA_ORIGINALE_ANNULLATO"].ToString()) ? "" : oRow["CARTA_ORIGINALE_ANNULLATO"].ToString());
                    this.CausaleAnnullamento = (string.IsNullOrEmpty(oRow["CAUSALE_ANNULLAMENTO"].ToString()) ? "" : oRow["CAUSALE_ANNULLAMENTO"].ToString());

                    this.PartecipanteNome = (string.IsNullOrEmpty(oRow["PARTECIPANTE_NOME"].ToString()) ? "" : oRow["PARTECIPANTE_NOME"].ToString());
                    this.PartecipanteCognome = (string.IsNullOrEmpty(oRow["PARTECIPANTE_COGNOME"].ToString()) ? "" : oRow["PARTECIPANTE_COGNOME"].ToString());
                    this.AcqregAutenticazione = (string.IsNullOrEmpty(oRow["ACQREG_AUTENTICAZIONE"].ToString()) ? "" : oRow["ACQREG_AUTENTICAZIONE"].ToString());
                    this.AcqregCodiceunivocoAcq = (string.IsNullOrEmpty(oRow["ACQREG_CODICEUNIVOCO_ACQ"].ToString()) ? "" : oRow["ACQREG_CODICEUNIVOCO_ACQ"].ToString());
                    this.AcqregIndirizzoipReg = (string.IsNullOrEmpty(oRow["ACQREG_INDIRIZZOIP_REG"].ToString()) ? "" : oRow["ACQREG_INDIRIZZOIP_REG"].ToString());
                    if (!string.IsNullOrEmpty(oRow["ACQREG_DATAORA_REG"].ToString())) this.AcqregDataoraReg = (DateTime)oRow["ACQREG_DATAORA_REG"];
                    this.AcqtranCodiceunivocoNumTran = (string.IsNullOrEmpty(oRow["ACQTRAN_CODICEUNIVOCO_NUM_TRAN"].ToString()) ? "" : oRow["ACQTRAN_CODICEUNIVOCO_NUM_TRAN"].ToString());
                    this.AcqtranCellulareAcq = (string.IsNullOrEmpty(oRow["ACQTRAN_CELLULARE_ACQ"].ToString()) ? "" : oRow["ACQTRAN_CELLULARE_ACQ"].ToString());
                    this.AcqtranEmailAcq = (string.IsNullOrEmpty(oRow["ACQTRAN_EMAIL_ACQ"].ToString()) ? "" : oRow["ACQTRAN_EMAIL_ACQ"].ToString());
                    this.AcqtranIndirizzoIpTran = (string.IsNullOrEmpty(oRow["ACQTRAN_INDIRIZZO_IP_TRAN"].ToString()) ? "" : oRow["ACQTRAN_INDIRIZZO_IP_TRAN"].ToString());
                    if (!string.IsNullOrEmpty(oRow["ACQTRAN_DATAORAINIZIOCHECKOUT"].ToString())) this.AcqtranDataorainiziocheckout = (DateTime)oRow["ACQTRAN_DATAORAINIZIOCHECKOUT"];
                    if (!string.IsNullOrEmpty(oRow["ACQTRAN_DATAORAESECUZIONE_PAG"].ToString())) this.AcqtranDataoraesecuzionePag = (DateTime)oRow["ACQTRAN_DATAORAESECUZIONE_PAG"];
                    this.AcqtranCro = (string.IsNullOrEmpty(oRow["ACQTRAN_CRO"].ToString()) ? "" : oRow["ACQTRAN_CRO"].ToString());
                    this.AcqtranMetodoSpedTitolo = (string.IsNullOrEmpty(oRow["ACQTRAN_METODO_SPED_TITOLO"].ToString()) ? "" : oRow["ACQTRAN_METODO_SPED_TITOLO"].ToString());
                    this.AcqtranIndirizzoSpedTitolo = (string.IsNullOrEmpty(oRow["ACQTRAN_INDIRIZZO_SPED_TITOLO"].ToString()) ? "" : oRow["ACQTRAN_INDIRIZZO_SPED_TITOLO"].ToString());
                    this.DigitaleTradizionale = (string.IsNullOrEmpty(oRow["DIGITALE_TRADIZIONALE"].ToString()) ? "" : oRow["DIGITALE_TRADIZIONALE"].ToString());



                    if (oRow.Table.Columns.Contains("DATAORA_EMISSIONE_ANNULLAMENTO"))
                        if (!string.IsNullOrEmpty(oRow["DATAORA_EMISSIONE_ANNULLAMENTO"].ToString()))
                            this.DataOraEmissioneAnnullamento = (DateTime)oRow["DATAORA_EMISSIONE_ANNULLAMENTO"];
                    if (oRow.Table.Columns.Contains("DATA_ORA_EVENTO"))
                        if (!string.IsNullOrEmpty(oRow["DATA_ORA_EVENTO"].ToString()))
                            this.DataOraEvento = (DateTime)oRow["DATA_ORA_EVENTO"];
                    if (oRow.Table.Columns.Contains("DATA_SCADENZA_ABBONAMENTO"))
                        if (!string.IsNullOrEmpty(oRow["DATA_SCADENZA_ABBONAMENTO"].ToString()))
                            this.DataScadenzaAbbonamento = (DateTime)oRow["DATA_SCADENZA_ABBONAMENTO"];

                    if (loadLta)
                    {
                        this.Lta = new clsResult_LtaParziale();

                        this.Lta.CfTitolareCa = (!string.IsNullOrEmpty(oRow["CF_TITOLARE_CA"].ToString()) ? oRow["CF_TITOLARE_CA"].ToString() : "");
                        this.Lta.CodiceSistemaCa = (!string.IsNullOrEmpty(oRow["CODICE_SISTEMA_CA"].ToString()) ? oRow["CODICE_SISTEMA_CA"].ToString() : "");
                        this.Lta.DataAperturaAccessi = (!string.IsNullOrEmpty(oRow["DATA_APERTURA_ACCESSI"].ToString()) ? oRow["DATA_APERTURA_ACCESSI"].ToString() : "");
                        this.Lta.OraAperturaAccessi = (!string.IsNullOrEmpty(oRow["ORA_APERTURA_ACCESSI"].ToString()) ? oRow["ORA_APERTURA_ACCESSI"].ToString() : "");
                        if (!string.IsNullOrEmpty(oRow["DATA_ORA_APERTURA_ACCESSI"].ToString()))
                            this.Lta.DataOraAperturaAccessi = (DateTime)oRow["DATA_ORA_APERTURA_ACCESSI"];
                        if (!string.IsNullOrEmpty(oRow["CORRISPETTIVO_LORDO"].ToString()))
                            this.Lta.CorrispettivoLordo = decimal.Parse(oRow["CORRISPETTIVO_LORDO"].ToString());
                        if (!string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString()))
                            this.Lta.NumeroEventiAbilitati = long.Parse(oRow["NUMERO_EVENTI_ABILITATI"].ToString());
                        this.Lta.DataAnnullamento = (!string.IsNullOrEmpty(oRow["DATA_ANNULLAMENTO"].ToString()) ? oRow["DATA_ANNULLAMENTO"].ToString() : "");
                        this.Lta.OraAnnullamento = (!string.IsNullOrEmpty(oRow["ORA_ANNULLAMENTO"].ToString()) ? oRow["ORA_ANNULLAMENTO"].ToString() : "");
                        this.Lta.CodiceCartaAnnullamento = (!string.IsNullOrEmpty(oRow["CODICE_CARTA_ANNULLAMENTO"].ToString()) ? oRow["CODICE_CARTA_ANNULLAMENTO"].ToString() : "");
                        if (!string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO_ANN"].ToString()))
                            this.Lta.ProgressivoTitoloAnn = long.Parse(oRow["PROGRESSIVO_TITOLO_ANN"].ToString());
                        this.Lta.SigilloAnnullamento = (!string.IsNullOrEmpty(oRow["SIGILLO_ANNULLAMENTO"].ToString()) ? oRow["SIGILLO_ANNULLAMENTO"].ToString() : "");
                        this.Lta.CodiceSuppIdentificativo = (!string.IsNullOrEmpty(oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString() : "");
                        this.Lta.DescrSuppIdentificativo = (!string.IsNullOrEmpty(oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString() : "");
                        this.Lta.IdentificativoSupp = (!string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP"].ToString()) ? oRow["IDENTIFICATIVO_SUPP"].ToString() : "");
                        this.Lta.IdentificativoSuppAlt = (!string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP_ALT"].ToString()) ? oRow["IDENTIFICATIVO_SUPP_ALT"].ToString() : "");
                        this.Lta.DataNascitaPartecipante = (!string.IsNullOrEmpty(oRow["DATA_NASCITA_PARTECIPANTE"].ToString()) ? oRow["DATA_NASCITA_PARTECIPANTE"].ToString() : "");
                        this.Lta.LuogoNascitaPartecipante = (!string.IsNullOrEmpty(oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString()) ? oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString() : "");
                        this.Lta.DataInserimentoLta = (!string.IsNullOrEmpty(oRow["DATA_INSERIMENTO_LTA"].ToString()) ? oRow["DATA_INSERIMENTO_LTA"].ToString() : "");
                        this.Lta.OraInserimentoLta = (!string.IsNullOrEmpty(oRow["ORA_INSERIMENTO_LTA"].ToString()) ? oRow["ORA_INSERIMENTO_LTA"].ToString() : "");
                        if (!string.IsNullOrEmpty(oRow["DATA_ORA_INSERIMENTO_LTA"].ToString()))
                            this.Lta.DataOraInserimentoLta = (DateTime)oRow["DATA_ORA_INSERIMENTO_LTA"];
                        this.Lta.StatoTitolo = (!string.IsNullOrEmpty(oRow["STATO_TITOLO"].ToString()) ? oRow["STATO_TITOLO"].ToString() : "");
                        this.Lta.DataIngressoCa = (!string.IsNullOrEmpty(oRow["DATA_INGRESSO_CA"].ToString()) ? oRow["DATA_INGRESSO_CA"].ToString() : "");
                        this.Lta.OraIngressoCa = (!string.IsNullOrEmpty(oRow["ORA_INGRESSO_CA"].ToString()) ? oRow["ORA_INGRESSO_CA"].ToString() : "");
                        if (!string.IsNullOrEmpty(oRow["DATA_ORA_INGRESSO_CA"].ToString()))
                            this.Lta.DataOraIngressoCa = (DateTime)oRow["DATA_ORA_INGRESSO_CA"];
                    }
                }
            }
        }

        public class clsChangeItemStatoLta
        {
            public string Stato { get; set; }
            public string Descrizione { get; set; }
        }

        public class clsResultLta
        {
            public string CfTitolareCa { get; set; }
            public string CodiceSistemaCa { get; set; }
            public string CodiceFiscaleOrganizzatore { get; set; }
            public string CodiceLocale { get; set; }
            public string DataEvento { get; set; }
            public string OraEvento { get; set; }
            public DateTime? DataOraEvento { get; set; }
            public string TitoloEvento { get; set; }
            public string TipoEvento { get; set; }
            public string DataAperturaAccessi { get; set; }
            public string OraAperturaAccessi { get; set; }
            public DateTime? DataOraAperturaAccessi { get; set; }
            public string CodiceSistemaEmissione { get; set; }
            public string CodiceCartaEmissione { get; set; }
            public long? ProgressivoTitolo { get; set; }
            public string Sigillo { get; set; }
            public string DataEmissione { get; set; }
            public string OraEmissione { get; set; }
            public string TipoTitolo { get; set; }
            public decimal? CorrispettivoLordo { get; set; }
            public string OrdineDiPosto { get; set; }
            public string Posto { get; set; }
            public string CfOrganizzatoreAbbonamento { get; set; }
            public string CodiceAbbonamento { get; set; }
            public string NumProgAbbonamento { get; set; }
            public long? NumeroEventiAbilitati { get; set; }
            public string DataAnnullamento { get; set; }
            public string OraAnnullamento { get; set; }
            public string CodiceCartaAnnullamento { get; set; }
            public long? ProgressivoTitoloAnn { get; set; }
            public string SigilloAnnullamento { get; set; }
            public string CodiceSuppIdentificativo { get; set; }
            public string DescrSuppIdentificativo { get; set; }
            public string IdentificativoSupp { get; set; }
            public string IdentificativoSuppAlt { get; set; }
            public string CognomePartecipante { get; set; }
            public string NomePartecipante { get; set; }
            public string DataNascitaPartecipante { get; set; }
            public string LuogoNascitaPartecipante { get; set; }
            public string DataInserimentoLta { get; set; }
            public string OraInserimentoLta { get; set; }
            public DateTime? DataOraInserimentoLta { get; set; }
            public string StatoTitolo { get; set; }
            public string DataIngressoCa { get; set; }
            public string OraIngressoCa { get; set; }
            public DateTime? DataOraIngressoCa { get; set; }

            public clsResult_LogParziale Log { get; set; }

            public List<clsChangeItemStatoLta> CambioStatoTitoloLta { get; set; }

            public clsResultLta()
            {

            }

            public clsResultLta(IRecordSet oRS, bool loadLog, System.Data.DataTable tableStatiLta)
            {
                this.CfTitolareCa = (!oRS.Fields("CF_TITOLARE_CA").IsNull ? oRS.Fields("CF_TITOLARE_CA").Value.ToString() : "");
                this.CodiceSistemaCa = (!oRS.Fields("CODICE_SISTEMA_CA").IsNull ? oRS.Fields("CODICE_SISTEMA_CA").Value.ToString() : "");
                this.CodiceFiscaleOrganizzatore = (!oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").IsNull ? oRS.Fields("CODICE_FISCALE_ORGANIZZATORE").Value.ToString() : "");
                this.CodiceLocale = (!oRS.Fields("CODICE_LOCALE").IsNull ? oRS.Fields("CODICE_LOCALE").Value.ToString() : "");
                this.DataEvento = (!oRS.Fields("DATA_EVENTO").IsNull ? oRS.Fields("DATA_EVENTO").Value.ToString() : "");
                this.OraEvento = (!oRS.Fields("ORA_EVENTO").IsNull ? oRS.Fields("ORA_EVENTO").Value.ToString() : "");
                if (!oRS.Fields("DATA_ORA_EVENTO").IsNull)
                    this.DataOraEvento = (DateTime)oRS.Fields("DATA_ORA_EVENTO").Value;
                this.TitoloEvento = (!oRS.Fields("TITOLO_EVENTO").IsNull ? oRS.Fields("TITOLO_EVENTO").Value.ToString() : "");
                this.TipoEvento = (!oRS.Fields("TIPO_EVENTO").IsNull ? oRS.Fields("TIPO_EVENTO").Value.ToString() : "");
                this.DataAperturaAccessi = (!oRS.Fields("DATA_APERTURA_ACCESSI").IsNull ? oRS.Fields("DATA_APERTURA_ACCESSI").Value.ToString() : "");
                this.OraAperturaAccessi = (!oRS.Fields("ORA_APERTURA_ACCESSI").IsNull ? oRS.Fields("ORA_APERTURA_ACCESSI").Value.ToString() : "");
                if (!oRS.Fields("DATA_ORA_APERTURA_ACCESSI").IsNull)
                    this.DataOraAperturaAccessi = (DateTime)oRS.Fields("DATA_ORA_APERTURA_ACCESSI").Value;
                this.CodiceSistemaEmissione = (!oRS.Fields("CODICE_SISTEMA_EMISSIONE").IsNull ? oRS.Fields("CODICE_SISTEMA_EMISSIONE").Value.ToString() : "");
                this.CodiceCartaEmissione = (!oRS.Fields("CODICE_CARTA_EMISSIONE").IsNull ? oRS.Fields("CODICE_CARTA_EMISSIONE").Value.ToString() : "");
                if (!oRS.Fields("PROGRESSIVO_TITOLO").IsNull)
                    this.ProgressivoTitolo = long.Parse(oRS.Fields("PROGRESSIVO_TITOLO").Value.ToString());
                this.Sigillo = (!oRS.Fields("SIGILLO").IsNull ? oRS.Fields("SIGILLO").Value.ToString() : "");
                this.DataEmissione = (!oRS.Fields("DATA_EMISSIONE").IsNull ? oRS.Fields("DATA_EMISSIONE").Value.ToString() : "");
                this.OraEmissione = (!oRS.Fields("ORA_EMISSIONE").IsNull ? oRS.Fields("ORA_EMISSIONE").Value.ToString() : "");
                this.TipoTitolo = (!oRS.Fields("TIPO_TITOLO").IsNull ? oRS.Fields("TIPO_TITOLO").Value.ToString() : "");
                if (!oRS.Fields("CORRISPETTIVO_LORDO").IsNull)
                    this.CorrispettivoLordo = decimal.Parse(oRS.Fields("CORRISPETTIVO_LORDO").Value.ToString());
                this.OrdineDiPosto = (!oRS.Fields("ORDINE_DI_POSTO").IsNull ? oRS.Fields("ORDINE_DI_POSTO").Value.ToString() : "");
                this.Posto = (!oRS.Fields("POSTO").IsNull ? oRS.Fields("POSTO").Value.ToString() : "");
                this.CfOrganizzatoreAbbonamento = (!oRS.Fields("CF_ORGANIZZATORE_ABBONAMENTO").IsNull ? oRS.Fields("CF_ORGANIZZATORE_ABBONAMENTO").Value.ToString() : "");
                this.CodiceAbbonamento = (!oRS.Fields("CODICE_ABBONAMENTO").IsNull ? oRS.Fields("CODICE_ABBONAMENTO").Value.ToString() : "");
                this.NumProgAbbonamento = (!oRS.Fields("NUM_PROG_ABBONAMENTO").IsNull ? oRS.Fields("NUM_PROG_ABBONAMENTO").Value.ToString() : "");
                if (!oRS.Fields("NUMERO_EVENTI_ABILITATI").IsNull)
                    this.NumeroEventiAbilitati = long.Parse(oRS.Fields("NUMERO_EVENTI_ABILITATI").Value.ToString());
                this.DataAnnullamento = (!oRS.Fields("DATA_ANNULLAMENTO").IsNull ? oRS.Fields("DATA_ANNULLAMENTO").Value.ToString() : "");
                this.OraAnnullamento = (!oRS.Fields("ORA_ANNULLAMENTO").IsNull ? oRS.Fields("ORA_ANNULLAMENTO").Value.ToString() : "");
                this.CodiceCartaAnnullamento = (!oRS.Fields("CODICE_CARTA_ANNULLAMENTO").IsNull ? oRS.Fields("CODICE_CARTA_ANNULLAMENTO").Value.ToString() : "");
                if (!oRS.Fields("PROGRESSIVO_TITOLO_ANN").IsNull)
                    this.ProgressivoTitoloAnn = long.Parse(oRS.Fields("PROGRESSIVO_TITOLO_ANN").Value.ToString());
                this.SigilloAnnullamento = (!oRS.Fields("SIGILLO_ANNULLAMENTO").IsNull ? oRS.Fields("SIGILLO_ANNULLAMENTO").Value.ToString() : "");
                this.CodiceSuppIdentificativo = (!oRS.Fields("CODICE_SUPP_IDENTIFICATIVO").IsNull ? oRS.Fields("CODICE_SUPP_IDENTIFICATIVO").Value.ToString() : "");
                this.DescrSuppIdentificativo = (!oRS.Fields("DESCR_SUPP_IDENTIFICATIVO").IsNull ? oRS.Fields("DESCR_SUPP_IDENTIFICATIVO").Value.ToString() : "");
                this.IdentificativoSupp = (!oRS.Fields("IDENTIFICATIVO_SUPP").IsNull ? oRS.Fields("IDENTIFICATIVO_SUPP").Value.ToString() : "");
                this.IdentificativoSuppAlt = (!oRS.Fields("IDENTIFICATIVO_SUPP_ALT").IsNull ? oRS.Fields("IDENTIFICATIVO_SUPP_ALT").Value.ToString() : "");
                this.CognomePartecipante = (!oRS.Fields("COGNOME_PARTECIPANTE").IsNull ? oRS.Fields("COGNOME_PARTECIPANTE").Value.ToString() : "");
                this.NomePartecipante = (!oRS.Fields("NOME_PARTECIPANTE").IsNull ? oRS.Fields("NOME_PARTECIPANTE").Value.ToString() : "");
                this.DataNascitaPartecipante = (!oRS.Fields("DATA_NASCITA_PARTECIPANTE").IsNull ? oRS.Fields("DATA_NASCITA_PARTECIPANTE").Value.ToString() : "");
                this.LuogoNascitaPartecipante = (!oRS.Fields("LUOGO_NASCITA_PARTECIPANTE").IsNull ? oRS.Fields("LUOGO_NASCITA_PARTECIPANTE").Value.ToString() : "");
                this.DataInserimentoLta = (!oRS.Fields("DATA_INSERIMENTO_LTA").IsNull ? oRS.Fields("DATA_INSERIMENTO_LTA").Value.ToString() : "");
                this.OraInserimentoLta = (!oRS.Fields("ORA_INSERIMENTO_LTA").IsNull ? oRS.Fields("ORA_INSERIMENTO_LTA").Value.ToString() : "");
                if (!oRS.Fields("DATA_ORA_INSERIMENTO_LTA").IsNull)
                    this.DataOraInserimentoLta = (DateTime)oRS.Fields("DATA_ORA_INSERIMENTO_LTA").Value;
                this.StatoTitolo = (!oRS.Fields("STATO_TITOLO").IsNull ? oRS.Fields("STATO_TITOLO").Value.ToString() : "");
                this.DataIngressoCa = (!oRS.Fields("DATA_INGRESSO_CA").IsNull ? oRS.Fields("DATA_INGRESSO_CA").Value.ToString() : "");
                this.OraIngressoCa = (!oRS.Fields("ORA_INGRESSO_CA").IsNull ? oRS.Fields("ORA_INGRESSO_CA").Value.ToString() : "");
                if (!oRS.Fields("DATA_ORA_INGRESSO_CA").IsNull)
                    this.DataOraIngressoCa = (DateTime)oRS.Fields("DATA_ORA_INGRESSO_CA").Value;

                if (tableStatiLta != null &&
                    (this.StatoTitolo.StartsWith("V") || this.StatoTitolo.StartsWith("D") || this.StatoTitolo.StartsWith("F") || this.StatoTitolo.StartsWith("B")))
                {
                    this.CambioStatoTitoloLta = new List<clsChangeItemStatoLta>();
                    foreach (System.Data.DataRow rowItemStatoLta in tableStatiLta.Rows)
                    {
                        string codice = rowItemStatoLta["STATO"].ToString();
                        if (codice != this.StatoTitolo && // diverso dall'attuale stato del titolo
                            codice.EndsWith(this.StatoTitolo.Substring(1,1)) && // che finisce con la stessa lettera per prendere TRADIZIONALE / DIGITALE e non confonderli
                            (codice.StartsWith("V") || codice.StartsWith("D") || codice.StartsWith("F") || codice.StartsWith("B")) // stati cambiabili: V, D, F, B
                           )
                        {
                            clsChangeItemStatoLta item = new clsChangeItemStatoLta();
                            item.Stato = rowItemStatoLta["STATO"].ToString();
                            item.Descrizione = rowItemStatoLta["DESCRIZIONE"].ToString();
                            this.CambioStatoTitoloLta.Add(item);
                        }
                    }
                }

                if (loadLog)
                {
                    this.Log = new clsResult_LogParziale();
                    this.Log.CodiceFiscaleTitolare = (oRS.Fields("CODICE_FISCALE_TITOLARE").IsNull ? "" : oRS.Fields("CODICE_FISCALE_TITOLARE").Value.ToString());
                    this.Log.TitoloAbbonamento = (oRS.Fields("TITOLO_ABBONAMENTO").IsNull ? "" : oRS.Fields("TITOLO_ABBONAMENTO").Value.ToString());
                    this.Log.TitoloIvaPreassolta = (oRS.Fields("TITOLO_IVA_PREASSOLTA").IsNull ? "" : oRS.Fields("TITOLO_IVA_PREASSOLTA").Value.ToString());
                    this.Log.SpettacoloIntrattenimento = (oRS.Fields("SPETTACOLO_INTRATTENIMENTO").IsNull ? "" : oRS.Fields("SPETTACOLO_INTRATTENIMENTO").Value.ToString());
                    this.Log.Valuta = (oRS.Fields("VALUTA").IsNull ? "" : oRS.Fields("VALUTA").Value.ToString());
                    if (!oRS.Fields("IMPONIBILE_INTRATTENIMENTI").IsNull) this.Log.ImponibileIntrattenimenti = decimal.Parse(oRS.Fields("IMPONIBILE_INTRATTENIMENTI").Value.ToString());
                    this.Log.CodiceRichiedenteEmissioneSigillo = (oRS.Fields("CODICE_PUNTO_VENDITA").IsNull ? "" : oRS.Fields("CODICE_PUNTO_VENDITA").Value.ToString());
                    this.Log.Prestampa = (oRS.Fields("PRESTAMPA").IsNull ? "" : oRS.Fields("PRESTAMPA").Value.ToString());
                    this.Log.CausaleOmaggioRiduzione = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE").Value.ToString());
                    if (!oRS.Fields("RATEO_EVENTO").IsNull) this.Log.RateoEvento = decimal.Parse(oRS.Fields("RATEO_EVENTO").Value.ToString());
                    if (!oRS.Fields("IVA_RATEO").IsNull) this.Log.IvaRateo = decimal.Parse(oRS.Fields("IVA_RATEO").Value.ToString());
                    if (!oRS.Fields("RATEO_IMPONIBILE_INTRA").IsNull) this.Log.RateoImponibileIntra = decimal.Parse(oRS.Fields("RATEO_IMPONIBILE_INTRA").Value.ToString());
                    this.Log.CausaleOmaggioRiduzioneOpen = (oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").IsNull ? "" : oRS.Fields("CAUSALE_OMAGGIO_RIDUZIONE_OPEN").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_TITOLO").IsNull) this.Log.CorrispettivoTitolo = decimal.Parse(oRS.Fields("CORRISPETTIVO_TITOLO").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_PREVENDITA").IsNull) this.Log.CorrispettivoPrevendita = decimal.Parse(oRS.Fields("CORRISPETTIVO_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("IVA_TITOLO").IsNull) this.Log.IvaTitolo = decimal.Parse(oRS.Fields("IVA_TITOLO").Value.ToString());
                    if (!oRS.Fields("IVA_PREVENDITA").IsNull) this.Log.IvaPrevendita = decimal.Parse(oRS.Fields("IVA_PREVENDITA").Value.ToString());
                    if (!oRS.Fields("CORRISPETTIVO_FIGURATIVO").IsNull) this.Log.CorrispettivoFigurativo = decimal.Parse(oRS.Fields("CORRISPETTIVO_FIGURATIVO").Value.ToString());
                    if (!oRS.Fields("IVA_FIGURATIVA").IsNull) this.Log.IvaFigurativa = decimal.Parse(oRS.Fields("IVA_FIGURATIVA").Value.ToString());
                    this.Log.CodicePrestazione1 = (oRS.Fields("CODICE_PRESTAZIONE1").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE1").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE1").IsNull) this.Log.ImportoPrestazione1 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE1").Value.ToString());
                    this.Log.CodicePrestazione2 = (oRS.Fields("CODICE_PRESTAZIONE2").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE2").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE2").IsNull) this.Log.ImportoPrestazione2 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE2").Value.ToString());
                    this.Log.CodicePrestazione3 = (oRS.Fields("CODICE_PRESTAZIONE3").IsNull ? "" : oRS.Fields("CODICE_PRESTAZIONE3").Value.ToString());
                    if (!oRS.Fields("IMPORTO_PRESTAZIONE3").IsNull) this.Log.ImportoPrestazione3 = decimal.Parse(oRS.Fields("IMPORTO_PRESTAZIONE3").Value.ToString());
                    this.Log.CausaleAnnullamento = (oRS.Fields("CAUSALE_ANNULLAMENTO").IsNull ? "" : oRS.Fields("CAUSALE_ANNULLAMENTO").Value.ToString());
                    this.Log.AcqregAutenticazione = (oRS.Fields("ACQREG_AUTENTICAZIONE").IsNull ? "" : oRS.Fields("ACQREG_AUTENTICAZIONE").Value.ToString());
                    this.Log.AcqregCodiceunivocoAcq = (oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").IsNull ? "" : oRS.Fields("ACQREG_CODICEUNIVOCO_ACQ").Value.ToString());
                    this.Log.AcqregIndirizzoipReg = (oRS.Fields("ACQREG_INDIRIZZOIP_REG").IsNull ? "" : oRS.Fields("ACQREG_INDIRIZZOIP_REG").Value.ToString());
                    if (!oRS.Fields("ACQREG_DATAORA_REG").IsNull) this.Log.AcqregDataoraReg = (DateTime)oRS.Fields("ACQREG_DATAORA_REG").Value;
                    this.Log.AcqtranCodiceunivocoNumTran = (oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_CODICEUNIVOCO_NUM_TRAN").Value.ToString());
                    this.Log.AcqtranCellulareAcq = (oRS.Fields("ACQTRAN_CELLULARE_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_CELLULARE_ACQ").Value.ToString());
                    this.Log.AcqtranEmailAcq = (oRS.Fields("ACQTRAN_EMAIL_ACQ").IsNull ? "" : oRS.Fields("ACQTRAN_EMAIL_ACQ").Value.ToString());
                    this.Log.AcqtranIndirizzoIpTran = (oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_IP_TRAN").Value.ToString());
                    if (!oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").IsNull) this.Log.AcqtranDataorainiziocheckout = (DateTime)oRS.Fields("ACQTRAN_DATAORAINIZIOCHECKOUT").Value;
                    if (!oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").IsNull) this.Log.AcqtranDataoraesecuzionePag = (DateTime)oRS.Fields("ACQTRAN_DATAORAESECUZIONE_PAG").Value;
                    this.Log.AcqtranCro = (oRS.Fields("ACQTRAN_CRO").IsNull ? "" : oRS.Fields("ACQTRAN_CRO").Value.ToString());
                    this.Log.AcqtranMetodoSpedTitolo = (oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_METODO_SPED_TITOLO").Value.ToString());
                    this.Log.AcqtranIndirizzoSpedTitolo = (oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").IsNull ? "" : oRS.Fields("ACQTRAN_INDIRIZZO_SPED_TITOLO").Value.ToString());
                    this.Log.DigitaleTradizionale = (oRS.Fields("DIGITALE_TRADIZIONALE").IsNull ? "" : oRS.Fields("DIGITALE_TRADIZIONALE").Value.ToString());
                }
            }

            public clsResultLta(System.Data.DataRow oRow, bool loadLog)
            {
                this.CfTitolareCa = (!string.IsNullOrEmpty(oRow["CF_TITOLARE_CA"].ToString()) ? oRow["CF_TITOLARE_CA"].ToString() : "");
                this.CodiceSistemaCa = (string.IsNullOrEmpty(oRow["CODICE_SISTEMA_CA"].ToString()) ? oRow["CODICE_SISTEMA_CA"].ToString() : "");
                this.CodiceFiscaleOrganizzatore = (string.IsNullOrEmpty(oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString()) ? oRow["CODICE_FISCALE_ORGANIZZATORE"].ToString() : "");
                this.CodiceLocale = (string.IsNullOrEmpty(oRow["CODICE_LOCALE"].ToString()) ? oRow["CODICE_LOCALE"].ToString() : "");
                this.DataEvento = (string.IsNullOrEmpty(oRow["DATA_EVENTO"].ToString()) ? oRow["DATA_EVENTO"].ToString() : "");
                this.OraEvento = (string.IsNullOrEmpty(oRow["ORA_EVENTO"].ToString()) ? oRow["ORA_EVENTO"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["DATA_ORA_EVENTO"].ToString()))
                    this.DataOraEvento = (DateTime)oRow["DATA_ORA_EVENTO"];
                this.TitoloEvento = (string.IsNullOrEmpty(oRow["TITOLO_EVENTO"].ToString()) ? oRow["TITOLO_EVENTO"].ToString() : "");
                this.TipoEvento = (string.IsNullOrEmpty(oRow["TIPO_EVENTO"].ToString()) ? oRow["TIPO_EVENTO"].ToString() : "");
                this.DataAperturaAccessi = (string.IsNullOrEmpty(oRow["DATA_APERTURA_ACCESSI"].ToString()) ? oRow["DATA_APERTURA_ACCESSI"].ToString() : "");
                this.OraAperturaAccessi = (string.IsNullOrEmpty(oRow["ORA_APERTURA_ACCESSI"].ToString()) ? oRow["ORA_APERTURA_ACCESSI"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["DATA_ORA_APERTURA_ACCESSI"].ToString()))
                    this.DataOraAperturaAccessi = (DateTime)oRow["DATA_ORA_APERTURA_ACCESSI"];
                this.CodiceSistemaEmissione = (string.IsNullOrEmpty(oRow["CODICE_SISTEMA_EMISSIONE"].ToString()) ? oRow["CODICE_SISTEMA_EMISSIONE"].ToString() : "");
                this.CodiceCartaEmissione = (string.IsNullOrEmpty(oRow["CODICE_CARTA_EMISSIONE"].ToString()) ? oRow["CODICE_CARTA_EMISSIONE"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO"].ToString()))
                    this.ProgressivoTitolo = long.Parse(oRow["PROGRESSIVO_TITOLO"].ToString());
                this.Sigillo = (string.IsNullOrEmpty(oRow["SIGILLO"].ToString()) ? oRow["SIGILLO"].ToString() : "");
                this.DataEmissione = (string.IsNullOrEmpty(oRow["DATA_EMISSIONE"].ToString()) ? oRow["DATA_EMISSIONE"].ToString() : "");
                this.OraEmissione = (string.IsNullOrEmpty(oRow["ORA_EMISSIONE"].ToString()) ? oRow["ORA_EMISSIONE"].ToString() : "");
                this.TipoTitolo = (string.IsNullOrEmpty(oRow["TIPO_TITOLO"].ToString()) ? oRow["TIPO_TITOLO"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["CORRISPETTIVO_LORDO"].ToString()))
                    this.CorrispettivoLordo = decimal.Parse(oRow["CORRISPETTIVO_LORDO"].ToString());
                this.OrdineDiPosto = (string.IsNullOrEmpty(oRow["ORDINE_DI_POSTO"].ToString()) ? oRow["ORDINE_DI_POSTO"].ToString() : "");
                this.Posto = (string.IsNullOrEmpty(oRow["POSTO"].ToString()) ? oRow["POSTO"].ToString() : "");
                this.CfOrganizzatoreAbbonamento = (string.IsNullOrEmpty(oRow["CF_ORGANIZZATORE_ABBONAMENTO"].ToString()) ? oRow["CF_ORGANIZZATORE_ABBONAMENTO"].ToString() : "");
                this.CodiceAbbonamento = (string.IsNullOrEmpty(oRow["CODICE_ABBONAMENTO"].ToString()) ? oRow["CODICE_ABBONAMENTO"].ToString() : "");
                this.NumProgAbbonamento = (string.IsNullOrEmpty(oRow["NUM_PROG_ABBONAMENTO"].ToString()) ? oRow["NUM_PROG_ABBONAMENTO"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["NUMERO_EVENTI_ABILITATI"].ToString()))
                    this.NumeroEventiAbilitati = long.Parse(oRow["NUMERO_EVENTI_ABILITATI"].ToString());
                this.DataAnnullamento = (string.IsNullOrEmpty(oRow["DATA_ANNULLAMENTO"].ToString()) ? oRow["DATA_ANNULLAMENTO"].ToString() : "");
                this.OraAnnullamento = (string.IsNullOrEmpty(oRow["ORA_ANNULLAMENTO"].ToString()) ? oRow["ORA_ANNULLAMENTO"].ToString() : "");
                this.CodiceCartaAnnullamento = (string.IsNullOrEmpty(oRow["CODICE_CARTA_ANNULLAMENTO"].ToString()) ? oRow["CODICE_CARTA_ANNULLAMENTO"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["PROGRESSIVO_TITOLO_ANN"].ToString()))
                    this.ProgressivoTitoloAnn = long.Parse(oRow["PROGRESSIVO_TITOLO_ANN"].ToString());
                this.SigilloAnnullamento = (string.IsNullOrEmpty(oRow["SIGILLO_ANNULLAMENTO"].ToString()) ? oRow["SIGILLO_ANNULLAMENTO"].ToString() : "");
                this.CodiceSuppIdentificativo = (string.IsNullOrEmpty(oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["CODICE_SUPP_IDENTIFICATIVO"].ToString() : "");
                this.DescrSuppIdentificativo = (string.IsNullOrEmpty(oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString()) ? oRow["DESCR_SUPP_IDENTIFICATIVO"].ToString() : "");
                this.IdentificativoSupp = (string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP"].ToString()) ? oRow["IDENTIFICATIVO_SUPP"].ToString() : "");
                this.IdentificativoSuppAlt = (string.IsNullOrEmpty(oRow["IDENTIFICATIVO_SUPP_ALT"].ToString()) ? oRow["IDENTIFICATIVO_SUPP_ALT"].ToString() : "");
                this.CognomePartecipante = (string.IsNullOrEmpty(oRow["COGNOME_PARTECIPANTE"].ToString()) ? oRow["COGNOME_PARTECIPANTE"].ToString() : "");
                this.NomePartecipante = (string.IsNullOrEmpty(oRow["NOME_PARTECIPANTE"].ToString()) ? oRow["NOME_PARTECIPANTE"].ToString() : "");
                this.DataNascitaPartecipante = (string.IsNullOrEmpty(oRow["DATA_NASCITA_PARTECIPANTE"].ToString()) ? oRow["DATA_NASCITA_PARTECIPANTE"].ToString() : "");
                this.LuogoNascitaPartecipante = (string.IsNullOrEmpty(oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString()) ? oRow["LUOGO_NASCITA_PARTECIPANTE"].ToString() : "");
                this.DataInserimentoLta = (string.IsNullOrEmpty(oRow["DATA_INSERIMENTO_LTA"].ToString()) ? oRow["DATA_INSERIMENTO_LTA"].ToString() : "");
                this.OraInserimentoLta = (string.IsNullOrEmpty(oRow["ORA_INSERIMENTO_LTA"].ToString()) ? oRow["ORA_INSERIMENTO_LTA"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["DATA_ORA_INSERIMENTO_LTA"].ToString()))
                    this.DataOraInserimentoLta = (DateTime)oRow["DATA_ORA_INSERIMENTO_LTA"];
                this.StatoTitolo = (string.IsNullOrEmpty(oRow["STATO_TITOLO"].ToString()) ? oRow["STATO_TITOLO"].ToString() : "");
                this.DataIngressoCa = (string.IsNullOrEmpty(oRow["DATA_INGRESSO_CA"].ToString()) ? oRow["DATA_INGRESSO_CA"].ToString() : "");
                this.OraIngressoCa = (string.IsNullOrEmpty(oRow["ORA_INGRESSO_CA"].ToString()) ? oRow["ORA_INGRESSO_CA"].ToString() : "");
                if (string.IsNullOrEmpty(oRow["DATA_ORA_INGRESSO_CA"].ToString()))
                    this.DataOraIngressoCa = (DateTime)oRow["DATA_ORA_INGRESSO_CA"];
            }

            public clsResultLta(clsTornelloLta ltaTornello)
            {
                this.CfTitolareCa = ltaTornello.CFTitolareCA;
                this.CodiceSistemaCa = ltaTornello.CodiceSistemaCA;
                this.CodiceFiscaleOrganizzatore = ltaTornello.CFOrganizzatore;
                this.CodiceLocale = ltaTornello.CodiceLocale;
                this.DataEvento = ltaTornello.DataEvento;
                this.OraEvento = ltaTornello.OraEvento;
                long nAnno = 0;
                long nMese = 0;
                long nGiorno = 0;
                long nOre = 0;
                long nMinuti = 0;

                if (!string.IsNullOrEmpty(ltaTornello.DataEvento) && !string.IsNullOrWhiteSpace(ltaTornello.DataEvento) && ltaTornello.DataEvento.Length == 8 &&
                    !string.IsNullOrEmpty(ltaTornello.OraEvento) && !string.IsNullOrWhiteSpace(ltaTornello.OraEvento) && ltaTornello.OraEvento.Length == 4 &&
                    long.TryParse(ltaTornello.DataEvento.Substring(0, 4), out nAnno) && long.TryParse(ltaTornello.DataEvento.Substring(4, 2), out nMese) && long.TryParse(ltaTornello.DataEvento.Substring(6, 2), out nGiorno) &&
                    long.TryParse(ltaTornello.OraEvento.Substring(0, 2), out nOre) && long.TryParse(ltaTornello.OraEvento.Substring(0, 2), out nMinuti))
                {
                    try
                    {
                        this.DataOraEvento =  new DateTime((int)nAnno, (int)nMese, (int)nGiorno, (int)nOre, (int)nMinuti, 0);
                    }
                    catch (Exception)
                    {
                    }
                }
                this.TitoloEvento = ltaTornello.TitoloEvento;
                this.TipoEvento = ltaTornello.TipoEvento;
                this.DataAperturaAccessi = ltaTornello.DataAperturaAccessi;
                this.OraAperturaAccessi = ltaTornello.OraAperturaAccessi;
                this.DataOraAperturaAccessi = ltaTornello.DataOraAperturaAccessi;
                this.CodiceSistemaEmissione = ltaTornello.CodiceSistema;
                this.CodiceCartaEmissione = ltaTornello.CodiceCarta;
                this.ProgressivoTitolo = ltaTornello.NumProgTitolo;
                this.Sigillo = ltaTornello.Sigillo;
                this.DataEmissione = ltaTornello.DataEmissione;
                this.OraEmissione = ltaTornello.OraEmissione;
                this.TipoTitolo = ltaTornello.TipoTitolo;
                this.CorrispettivoLordo = ltaTornello.CorrispettivoLordo;
                this.OrdineDiPosto = ltaTornello.OrdineDiPosto;
                this.Posto = ltaTornello.IdPosto;
                this.CfOrganizzatoreAbbonamento = ltaTornello.CodiceFiscaleOrganizzatore;
                this.CodiceAbbonamento = ltaTornello.CodiceAbbonamento;
                this.NumProgAbbonamento = ltaTornello.NumeroProgressivoAbbonamento.ToString();
                this.NumeroEventiAbilitati = ltaTornello.NumeroEventiAbilitati;
                this.DataAnnullamento = ltaTornello.DataAnnullamento;
                this.OraAnnullamento = ltaTornello.OraAnnullamento;
                //ltaTornello.DATA_ORA_ANNULLAMENTO,;
                this.CodiceCartaAnnullamento = ltaTornello.CodiceCartaAnnullamento;
                this.ProgressivoTitoloAnn = ltaTornello.ProgressivoTitoloAnnullamento;
                this.SigilloAnnullamento = ltaTornello.SigilloAnnullamento;
                if (ltaTornello.datiSupportoIdentificazione != null)
                {
                    this.CodiceSuppIdentificativo = ltaTornello.datiSupportoIdentificazione.CodiceSupportoIdentificativo;
                    this.DescrSuppIdentificativo = ltaTornello.datiSupportoIdentificazione.DescrizioneCodiceSupportoIdentificativo;
                    this.IdentificativoSupp = ltaTornello.datiSupportoIdentificazione.IdentificativoSupporto;
                    this.IdentificativoSuppAlt = ltaTornello.datiSupportoIdentificazione.IdentificativoSupportoAlternativo;
                    //ltaTornello.DescrizioneSupportoIdentificativo;
                }
                if (ltaTornello.datiIdentificativiPartecipante != null)
                {
                    this.CognomePartecipante = ltaTornello.datiIdentificativiPartecipante.Cognome;
                    this.NomePartecipante = ltaTornello.datiIdentificativiPartecipante.Nome;
                    this.DataNascitaPartecipante = ltaTornello.datiIdentificativiPartecipante.DataNascita;
                    this.LuogoNascitaPartecipante = ltaTornello.datiIdentificativiPartecipante.LuogoNascita;
                }
                this.DataInserimentoLta = ltaTornello.DataInserimentoLTA;
                this.OraInserimentoLta = ltaTornello.OraInserimentoLTA;


                nAnno = 0;
                nMese = 0;
                nGiorno = 0;
                nOre = 0;
                nMinuti = 0;


                if (!string.IsNullOrEmpty(ltaTornello.DataInserimentoLTA) && !string.IsNullOrWhiteSpace(ltaTornello.DataInserimentoLTA) && ltaTornello.DataInserimentoLTA.Length == 8 &&
                    !string.IsNullOrEmpty(ltaTornello.OraInserimentoLTA) && !string.IsNullOrWhiteSpace(ltaTornello.OraInserimentoLTA) && ltaTornello.OraInserimentoLTA.Length == 6 &&
                    long.TryParse(ltaTornello.DataInserimentoLTA.Substring(0, 4), out nAnno) && long.TryParse(ltaTornello.DataInserimentoLTA.Substring(4, 2), out nMese) && long.TryParse(ltaTornello.DataInserimentoLTA.Substring(6, 2), out nGiorno) &&
                    long.TryParse(ltaTornello.OraInserimentoLTA.Substring(0, 2), out nOre) && long.TryParse(ltaTornello.OraInserimentoLTA.Substring(0, 2), out nMinuti))
                {
                    try
                    {
                        this.DataOraInserimentoLta = new DateTime((int)nAnno, (int)nMese, (int)nGiorno, (int)nOre, (int)nMinuti, 0);
                    }
                    catch (Exception)
                    {
                    }
                }

                this.StatoTitolo = ltaTornello.StatoDelTitolo;
                this.DataIngressoCa = ltaTornello.DataIngressoControlloAccessi;
                this.OraIngressoCa = ltaTornello.OraIngressoControlloAccessi;

                if (!string.IsNullOrEmpty(ltaTornello.DataIngressoControlloAccessi) && !string.IsNullOrWhiteSpace(ltaTornello.DataIngressoControlloAccessi) && ltaTornello.DataIngressoControlloAccessi.Length == 8 &&
                    !string.IsNullOrEmpty(ltaTornello.OraIngressoControlloAccessi) && !string.IsNullOrWhiteSpace(ltaTornello.OraIngressoControlloAccessi) && ltaTornello.OraIngressoControlloAccessi.Length == 4 &&
                    long.TryParse(ltaTornello.DataIngressoControlloAccessi.Substring(0, 4), out nAnno) && long.TryParse(ltaTornello.DataIngressoControlloAccessi.Substring(4, 2), out nMese) && long.TryParse(ltaTornello.DataIngressoControlloAccessi.Substring(6, 2), out nGiorno) &&
                    long.TryParse(ltaTornello.OraIngressoControlloAccessi.Substring(0, 2), out nOre) && long.TryParse(ltaTornello.OraIngressoControlloAccessi.Substring(0, 2), out nMinuti))
                {
                    try
                    {
                        this.DataOraIngressoCa = new DateTime((int)nAnno, (int)nMese, (int)nGiorno, (int)nOre, (int)nMinuti, 0);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        // Classe singolo campo log transazioni
        public class clsResultBaseCampoLogTransazioni
        {
            public string Campo = "";
            public string Tipo = "";
            public int Inizio = 0;
            public int Dimensione = 0;
            public string DescCampo = "";
            public string Descrizione = "";
            public string NomeFiscale = "";

            public clsResultBaseCampoLogTransazioni()
            {
            }

            public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
            {
                this.Campo = cCampo;
                this.Tipo = cTipo;
                this.Inizio = nInizio;
                this.Dimensione = nDimensione;
                this.DescCampo = cDescCampo;
                this.Descrizione = cDescrizione;
                this.NomeFiscale = cNomeFiscale;
            }
        }

        public class clsResultBaseCampoLta
        {
            public string Campo = "";
            public string Tipo = "";
            public int Inizio = 0;
            public int Dimensione = 0;
            public string DescCampo = "";
            public string Descrizione = "";
            public string NomeFiscale = "";

            public clsResultBaseCampoLta()
            {
            }

            public clsResultBaseCampoLta(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
            {
                this.Campo = cCampo;
                this.Tipo = cTipo;
                this.Inizio = nInizio;
                this.Dimensione = nDimensione;
                this.DescCampo = cDescCampo;
                this.Descrizione = cDescrizione;
                this.NomeFiscale = cNomeFiscale;
            }
        }

        // Risultato base che contiene un array di descrizione dei campi del log delle transazioni 
        public class ResultBaseCampiLogTransazioni : ResultBase
        {
            public clsResultBaseCampoLogTransazioni[] Campi = null;
            public Riepiloghi.clsFiltroCampoTransazione[] Filtri = null;

            public ResultBaseCampiLogTransazioni()
            {
            }

            public static string GetNomePropertyObect(string value)
            {
                string result = value;
                switch (value)
                {
                    case "CODICE_FISCALE_ORGANIZZATORE": { result = "CodiceFiscaleOrganizzatore"; break; }
                    case "CODICE_FISCALE_TITOLARE": { result = "CodiceFiscaleTitolare"; break; }
                    case "TITOLO_ABBONAMENTO": { result = "TitoloAbbonamento"; break; }
                    case "TITOLO_IVA_PREASSOLTA": { result = "TitoloIvaPreassolta"; break; }
                    case "SPETTACOLO_INTRATTENIMENTO": { result = "SpettacoloIntrattenimento"; break; }
                    case "VALUTA": { result = "Valuta"; break; }
                    case "IMPONIBILE_INTRATTENIMENTI": { result = "ImponibileIntrattenimenti"; break; }
                    case "ORDINE_DI_POSTO": { result = "OrdineDiPosto"; break; }
                    case "POSTO": { result = "Posto"; break; }
                    case "TIPO_TITOLO": { result = "TipoTitolo"; break; }
                    case "ANNULLATO": { result = "Annullato"; break; }
                    case "DATA_EMISSIONE_ANNULLAMENTO": { result = "DataEmissioneAnnullamento"; break; }
                    case "ORA_EMISSIONE_ANNULLAMENTO": { result = "OraEmissioneAnnullamento"; break; }
                    case "PROGRESSIVO_TITOLO": { result = "ProgressivoTitolo"; break; }
                    case "SIGILLO": { result = "Sigillo"; break; }
                    case "CODICE_SISTEMA": { result = "CodiceSistema"; break; }
                    case "CODICE_CARTA": { result = "CodiceCarta"; break; }
                    case "PRESTAMPA": { result = "Prestampa"; break; }
                    case "CODICE_LOCALE": { result = "CodiceLocale"; break; }
                    case "DATA_EVENTO": { result = "DataEvento"; break; }
                    case "TIPO_EVENTO": { result = "TipoEvento"; break; }
                    case "TITOLO_EVENTO": { result = "TitoloEvento"; break; }
                    case "ORA_EVENTO": { result = "OraEvento"; break; }
                    case "CAUSALE_OMAGGIO_RIDUZIONE": { result = "CausaleOmaggioRiduzione"; break; }
                    case "TIPO_TURNO": { result = "TipoTurno"; break; }
                    case "DATA_LIMITE_VALIDITA": { result = "DataLimiteValidita"; break; }
                    case "CODICE_ABBONAMENTO": { result = "CodiceAbbonamento"; break; }
                    case "RATEO_EVENTO": { result = "RateoEvento"; break; }
                    case "IVA_RATEO": { result = "IvaRateo"; break; }
                    case "RATEO_IMPONIBILE_INTRA": { result = "RateoImponibileIntra"; break; }
                    case "CAUSALE_OMAGGIO_RIDUZIONE_OPEN": { result = "CausaleOmaggioRiduzioneOpen"; break; }
                    case "CORRISPETTIVO_TITOLO": { result = "CorrispettivoTitolo"; break; }
                    case "CORRISPETTIVO_PREVENDITA": { result = "CorrispettivoPrevendita"; break; }
                    case "IVA_TITOLO": { result = "IvaTitolo"; break; }
                    case "IVA_PREVENDITA": { result = "IvaPrevendita"; break; }
                    case "CORRISPETTIVO_FIGURATIVO": { result = "CorrispettivoFigurativo"; break; }
                    case "IVA_FIGURATIVA": { result = "IvaFigurativa"; break; }
                    case "CODICE_FISCALE_ABBONAMENTO": { result = "CodiceFiscaleAbbonamento"; break; }
                    case "CODICE_BIGLIETTO_ABBONAMENTO": { result = "CodiceBigliettoAbbonamento"; break; }
                    case "NUM_PROG_BIGLIETTO_ABBONAMENTO": { result = "NumProgBigliettoAbbonamento"; break; }
                    case "CODICE_PRESTAZIONE1": { result = "CodicePrestazione1"; break; }
                    case "IMPORTO_PRESTAZIONE1": { result = "ImportoPrestazione1"; break; }
                    case "CODICE_PRESTAZIONE2": { result = "CodicePrestazione2"; break; }
                    case "IMPORTO_PRESTAZIONE2": { result = "ImportoPrestazione2"; break; }
                    case "CODICE_PRESTAZIONE3": { result = "CodicePrestazione3"; break; }
                    case "IMPORTO_PRESTAZIONE3": { result = "ImportoPrestazione3"; break; }
                    case "CARTA_ORIGINALE_ANNULLATO": { result = "CartaOriginaleAnnullato"; break; }
                    case "CAUSALE_ANNULLAMENTO": { result = "CausaleAnnullamento"; break; }
                    case "PARTECIPANTE_NOME": { result = "PartecipanteNome"; break; }
                    case "PARTECIPANTE_COGNOME": { result = "PartecipanteCognome"; break; }
                    case "ACQREG_AUTENTICAZIONE": { result = "AcqregAutenticazione"; break; }
                    case "ACQREG_CODICEUNIVOCO_ACQ": { result = "AcqregCodiceunivocoAcq"; break; }
                    case "ACQREG_INDIRIZZOIP_REG": { result = "AcqregIndirizzoipReg"; break; }
                    case "ACQTRAN_CODICEUNIVOCO_NUM_TRAN": { result = "AcqtranCodiceunivocoNumTran"; break; }
                    case "ACQTRAN_CELLULARE_ACQ": { result = "AcqtranCellulareAcq"; break; }
                    case "ACQTRAN_EMAIL_ACQ": { result = "AcqtranEmailAcq"; break; }
                    case "ACQTRAN_INDIRIZZO_IP_TRAN": { result = "AcqtranIndirizzoIpTran"; break; }
                    case "ACQTRAN_CRO": { result = "AcqtranCro"; break; }
                    case "ACQTRAN_METODO_SPED_TITOLO": { result = "AcqtranMetodoSpedTitolo"; break; }
                    case "ACQTRAN_INDIRIZZO_SPED_TITOLO": { result = "AcqtranIndirizzoSpedTitolo"; break; }
                    case "DIGITALE_TRADIZIONALE": { result = "DigitaleTradizionale"; break; }
                    case "PROGRESSIVO_ANNULLATI": { result = "ProgressivoAnnullati"; break; }
                    case "NUMERO_EVENTI_ABILITATI": { result = "NumeroEventiAbilitati"; break; }
                    case "NUM_PROG_ABBONAMENTO": { result = "NumProgAbbonamento"; break; }
                    case "ACQREG_DATAORA_REG": { result = "AcqregDataoraReg"; break; }
                    case "ACQTRAN_DATAORAINIZIOCHECKOUT": { result = "AcqtranDataorainiziocheckout"; break; }
                    case "ACQTRAN_DATAORAESECUZIONE_PAG": { result = "AcqtranDataoraesecuzionePag"; break; }
                    case "CODICE_PUNTO_VENDITA": { result = "CodiceRichiedenteEmissioneSigillo"; break; }

                }
                return result;
            }

            public ResultBaseCampiLogTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oListaCampi != null && oListaCampi.Count > 0)
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[oListaCampi.Count];
                    int Index = 0;
                    foreach (System.Collections.DictionaryEntry ItemCampoDict in oListaCampi)
                    {
                        Riepiloghi.clsCampoTransazione oCampo = (Riepiloghi.clsCampoTransazione)ItemCampoDict.Value;

                        clsResultBaseCampoLogTransazioni oResultCampo = new clsResultBaseCampoLogTransazioni(oCampo.Campo, oCampo.Tipo, oCampo.Inizio, oCampo.Dimensione, oCampo.DescCampo, oCampo.Descrizione, oCampo.NomeFiscale);
                        oResultCampo.Campo = GetNomePropertyObect(oResultCampo.Campo);
                        if (oResultCampo.Campo == "CODICE_PUNTO_VENDITA" || oResultCampo.Campo == "CodiceRichiedenteEmissioneSigillo")
                        {
                            oResultCampo.DescCampo = "Codice Richiedente Emissione Sigillo";
                        }
                        this.Campi[Index] = oResultCampo;
                        Index += 1;
                    }
                }
                else
                {
                    this.Campi = new clsResultBaseCampoLogTransazioni[0];
                }
            }

            public ResultBaseCampiLogTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi, Riepiloghi.clsFiltroCampoTransazione[] oListaFiltri)
                : this(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oListaCampi)
            {
                if (oListaFiltri != null && oListaFiltri.Length > 0)
                {
                    this.Filtri = new Riepiloghi.clsFiltroCampoTransazione[oListaFiltri.Length];
                    int Index = 0;
                    foreach (Riepiloghi.clsFiltroCampoTransazione oFiltro in oListaFiltri)
                    {
                        Riepiloghi.clsFiltroCampoTransazione oFiltroNew = new Riepiloghi.clsFiltroCampoTransazione(oFiltro.Campo, oFiltro.Tipo, oFiltro.Predefinito);
                        oFiltroNew.Campo = GetNomePropertyObect(oFiltroNew.Campo);
                        this.Filtri[Index] = oFiltroNew;
                        this.Filtri[Index].ValoriPredefiniti = oFiltro.ValoriPredefiniti;
                        Index += 1;
                    }
                }
                else
                {
                    this.Filtri = new Riepiloghi.clsFiltroCampoTransazione[0];
                }
            }
        }

        public class ResultBaseCampiLta : ResultBase
        {
            public clsResultBaseCampoLta[] CampiLta = null;

            public ResultBaseCampiLta()
            {
            }

            public static string GetNomePropertyObect(string value)
            {
                string result = value;
                switch (value)
                {
                    case "CODICE_SISTEMA_CA": { result = "CodiceSistemaCa"; break; }
                    case "CF_TITOLARE_CA": { result = "CfTitolareCa"; break; }
                    case "CODICE_FISCALE_ORGANIZZATORE": { result = "CodiceFiscaleOrganizzatore"; break; }
                    case "CODICE_LOCALE": { result = "CodiceLocale"; break; }
                    case "DATA_EVENTO": { result = "DataEvento"; break; }
                    case "ORA_EVENTO": { result = "OraEvento"; break; }
                    case "TITOLO_EVENTO": { result = "TitoloEvento"; break; }
                    case "TIPO_EVENTO": { result = "TipoEvento"; break; }
                    case "DATA_APERTURA_ACCESSI": { result = "DataAperturaAccessi"; break; }
                    case "ORA_APERTURA_ACCESSI": { result = "OraAperturaAccessi"; break; }
                    case "CODICE_SISTEMA_EMISSIONE": { result = "CodiceSistemaEmissione"; break; }
                    case "CODICE_CARTA_EMISSIONE": { result = "CodiceCartaEmissione"; break; }
                    case "PROGRESSIVO_TITOLO": { result = "ProgressivoTitolo"; break; }
                    case "SIGILLO": { result = "Sigillo"; break; }
                    case "DATA_EMISSIONE": { result = "DataEmissione"; break; }
                    case "ORA_EMISSIONE": { result = "OraEmissione"; break; }
                    case "TIPO_TITOLO": { result = "TipoTitolo"; break; }
                    case "CORRISPETTIVO_LORDO": { result = "CorrispettivoLordo"; break; }
                    case "ORDINE_DI_POSTO": { result = "OrdineDiPosto"; break; }
                    case "POSTO": { result = "Posto"; break; }
                    case "CF_ORGANIZZATORE_ABBONAMENTO": { result = "CfOrganizzatoreAbbonamento"; break; }
                    case "CODICE_ABBONAMENTO": { result = "CodiceAbbonamento"; break; }
                    case "NUM_PROG_ABBONAMENTO": { result = "NumProgAbbonamento"; break; }
                    case "NUMERO_EVENTI_ABILITATI": { result = "NumeroEventiAbilitati"; break; }
                    case "DATA_ANNULLAMENTO": { result = "DataAnnullamento"; break; }
                    case "ORA_ANNULLAMENTO": { result = "OraAnnullamento"; break; }
                    case "CODICE_CARTA_ANNULLAMENTO": { result = "CodiceCartaAnnullamento"; break; }
                    case "PROGRESSIVO_TITOLO_ANN": { result = "ProgressivoTitoloAnn"; break; }
                    case "SIGILLO_ANNULLAMENTO": { result = "SigilloAnnullamento"; break; }
                    case "CODICE_SUPP_IDENTIFICATIVO": { result = "CodiceSuppIdentificativo"; break; }
                    case "DESCR_SUPP_IDENTIFICATIVO": { result = "DescrSuppIdentificativo"; break; }
                    case "IDENTIFICATIVO_SUPP": { result = "IdentificativoSupp"; break; }
                    case "IDENTIFICATIVO_SUPP_ALT": { result = "IdentificativoSuppAlt"; break; }
                    case "COGNOME_PARTECIPANTE": { result = "CognomePartecipante"; break; }
                    case "NOME_PARTECIPANTE": { result = "NomePartecipante"; break; }
                    case "DATA_NASCITA_PARTECIPANTE": { result = "DataNascitaPartecipante"; break; }
                    case "LUOGO_NASCITA_PARTECIPANTE": { result = "LuogoNascitaPartecipante"; break; }
                    case "DATA_INSERIMENTO_LTA": { result = "DataInserimentoLta"; break; }
                    case "ORA_INSERIMENTO_LTA": { result = "OraInserimentoLta"; break; }
                    case "STATO_TITOLO": { result = "StatoTitolo"; break; }
                    case "DATA_INGRESSO_CA": { result = "DataIngressoCa"; break; }
                    case "ORA_INGRESSO_CA": { result = "OraIngressoCa"; break; }
                }
                return result;
            }

            public ResultBaseCampiLta(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, System.Collections.SortedList oListaCampi)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oListaCampi != null && oListaCampi.Count > 0)
                {
                    this.CampiLta = new clsResultBaseCampoLta[oListaCampi.Count];
                    int Index = 0;
                    foreach (System.Collections.DictionaryEntry ItemCampoDict in oListaCampi)
                    {
                        Riepiloghi.clsCampoLta oCampo = (Riepiloghi.clsCampoLta)ItemCampoDict.Value;

                        clsResultBaseCampoLta oResultCampo = new clsResultBaseCampoLta(oCampo.Campo, oCampo.Tipo, oCampo.Inizio, oCampo.Dimensione, oCampo.DescCampo, oCampo.Descrizione, oCampo.NomeFiscale);
                        oResultCampo.Campo = GetNomePropertyObect(oResultCampo.Campo);
                        this.CampiLta[Index] = oResultCampo;
                        Index += 1;
                    }
                }
                else
                {
                    this.CampiLta = new clsResultBaseCampoLta[0];
                }
            }

        }

        public class clsResultGetEventi 
        {
            public Int64 IdCalend { get; set; }
            public Int64 IdPacchetto { get; set; }
            public Int64 IdSala { get; set; }
            public DateTime DataOraEvento { get; set; }
            public string DescrizioneSala { get; set; }
            public string CodiceLocale { get; set; }
            public string TitoloEvento { get; set; }
        }

        public class ResultBaseGetEventi : ResultBase
        {
            public clsResultGetEventi[] Eventi = null;

            public ResultBaseGetEventi()
            { }

            public ResultBaseGetEventi(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
            {
                this.StatusOK = (lStatusOK && oRS != null);
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                if (oRS != null && !oRS.EOF)
                {
                    List<clsResultGetEventi> lista = new List<clsResultGetEventi>();
                    while (!oRS.EOF)
                    {
                        lista.Add(new clsResultGetEventi()
                        {
                            IdCalend = (!oRS.Fields("IDCALEND").IsNull ? long.Parse(oRS.Fields("IDCALEND").Value.ToString()) : 0),
                            IdSala = (!oRS.Fields("IDSALA").IsNull ? long.Parse(oRS.Fields("IDSALA").Value.ToString()) : 0),
                            IdPacchetto = (!oRS.Fields("IDPACCHETTO").IsNull ? long.Parse(oRS.Fields("IDPACCHETTO").Value.ToString()) : 0),
                            CodiceLocale = (!oRS.Fields("CODICE_LOCALE").IsNull ? oRS.Fields("CODICE_LOCALE").Value.ToString() : ""),
                            DescrizioneSala = (!oRS.Fields("DESCRIZIONE_SALA").IsNull ? oRS.Fields("DESCRIZIONE_SALA").Value.ToString() : ""),
                            TitoloEvento = (!oRS.Fields("TITOLO_EVENTO").IsNull ? oRS.Fields("TITOLO_EVENTO").Value.ToString() : ""),
                            DataOraEvento = (!oRS.Fields("DATA_ORA_EVENTO").IsNull ? (DateTime)oRS.Fields("DATA_ORA_EVENTO").Value : DateTime.MinValue)
                        });
                        
                        oRS.MoveNext();
                    }
                    this.Eventi = lista.ToArray();
                }
            }
        }


        // Risultato base che contiene un array di clsResulttransazioni (lista delle transazioni)
        public class ResultBaseGetTransazioni : ResultBase
        {
            public clsResultTransazione[] Transazioni = null;
            public System.Data.DataTable TableTransazioni = null;

            public ResultBaseGetTransazioni()
            {

            }

            public ResultBaseGetTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS, bool loadLta, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = (lStatusOK && oRS != null);
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                if (oRS != null && !oRS.EOF)
                {
                    if (CodiciLocaliAbilitati != null)
                    {
                        System.Data.DataTable tableResult = new System.Data.DataTable();
                        foreach (System.Data.DataColumn column in ((System.Data.DataTable)oRS).Columns)
                        {
                            System.Data.DataColumn oCol = new System.Data.DataColumn(column.ColumnName, column.DataType, column.Expression, column.ColumnMapping);
                            tableResult.Columns.Add(oCol);
                        }
                        foreach (System.Data.DataRow row in ((System.Data.DataTable)oRS).Rows)
                        {
                            if (row["TITOLO_ABBONAMENTO"].ToString() == "A" || CodiciLocaliAbilitati.Contains(row["CODICE_LOCALE"].ToString()))
                            {
                                System.Data.DataRow oRow = tableResult.NewRow();
                                oRow.ItemArray = row.ItemArray;
                                tableResult.Rows.Add(oRow);
                            }
                        }


                        this.Transazioni = new clsResultTransazione[tableResult.Rows.Count];
                        int Index = 0;
                        foreach (System.Data.DataRow oRow in tableResult.Rows)
                        {
                            this.Transazioni[Index] = new clsResultTransazione(oRow, loadLta);
                            Index += 1;
                        }

                        if (PreserveDataTable)
                        {
                            this.TableTransazioni = tableResult;
                            this.TableTransazioni.TableName = "TRANSAZIONI";
                        }
                    }
                    else
                    {
                        this.Transazioni = new clsResultTransazione[(int)oRS.RecordCount];
                        int Index = 0;
                        while (!oRS.EOF)
                        {
                            this.Transazioni[Index] = new clsResultTransazione(oRS, loadLta);
                            Index += 1;
                            oRS.MoveNext();
                        }
                        if (PreserveDataTable)
                        {
                            this.TableTransazioni = (System.Data.DataTable)oRS;
                            this.TableTransazioni.TableName = "TRANSAZIONI";
                        }
                    }
                }
                else
                {
                    this.Transazioni = new clsResultTransazione[0];
                }

            }
        }

        public class ResultBaseGetLta : ResultBase
        {
            public clsResultLta[] Lta = null;
            public System.Data.DataTable TableLta = null;

            public ResultBaseGetLta()
            {

            }

            public ResultBaseGetLta(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS, bool loadLog, System.Data.DataTable tableStatiLta, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.StatusOK = (lStatusOK && oRS != null);
                this.StatusMessage = cStatusMessage;
                this.StatusCode = nStatusCode;
                this.execution_time = cExecutionTime;
                if (oRS != null && !oRS.EOF)
                {
                    if (CodiciLocaliAbilitati != null)
                    {
                        System.Data.DataTable tableResult = new System.Data.DataTable();
                        foreach (System.Data.DataColumn column in ((System.Data.DataTable)oRS).Columns)
                        {
                            System.Data.DataColumn oCol = new System.Data.DataColumn(column.ColumnName, column.DataType, column.Expression, column.ColumnMapping);
                            tableResult.Columns.Add(oCol);
                        }
                        foreach (System.Data.DataRow row in ((System.Data.DataTable)oRS).Rows)
                        {
                            if (CodiciLocaliAbilitati.Contains(row["CODICE_LOCALE"].ToString()))
                            {
                                System.Data.DataRow oRow = tableResult.NewRow();
                                oRow.ItemArray = row.ItemArray;
                                tableResult.Rows.Add(oRow);
                            }
                        }


                        this.Lta = new clsResultLta[tableResult.Rows.Count];
                        int Index = 0;
                        foreach (System.Data.DataRow oRow in tableResult.Rows)
                        {
                            this.Lta[Index] = new clsResultLta(oRow, loadLog);
                            Index += 1;
                        }

                        if (PreserveDataTable)
                        {
                            this.TableLta = tableResult;
                            this.TableLta.TableName = "LTA";
                        }
                    }
                    else
                    {
                        this.Lta = new clsResultLta[(int)oRS.RecordCount];
                        int Index = 0;
                        while (!oRS.EOF)
                        {
                            this.Lta[Index] = new clsResultLta(oRS, loadLog, tableStatiLta);
                            Index += 1;
                            oRS.MoveNext();
                        }
                        if (PreserveDataTable)
                        {
                            this.TableLta = (System.Data.DataTable)oRS;
                            this.TableLta.TableName = "LTA";
                        }
                    }
                }
                else
                {
                    this.Lta = new clsResultLta[0];
                }

            }
        }

        // Risultato transazioni per periodo che contiene un array di clsResulttransazioni (lista delle transazioni)
        // ed il dettaglio della richiesta con data inizio, data fine e modalità storico
        public class ResultTransazionePeriodo : ResultBaseGetTransazioni
        {
            public DateTime DataEmiInizio;
            public DateTime DataEmiFine;
            public string ModalitaStorico = "";

            public ResultTransazionePeriodo()
            {
            }

            public ResultTransazionePeriodo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataEmiInizio, DateTime dDataEmiFine, string cModalitaStorico, IRecordSet oRS, bool loadLta, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLta, PreserveDataTable, CodiciLocaliAbilitati)
            {
                this.DataEmiInizio = dDataEmiInizio;
                this.DataEmiFine = dDataEmiFine;
                this.ModalitaStorico = cModalitaStorico;
            }
        }

        public class ResultLtaPeriodo : ResultBaseGetLta
        {
            public DateTime DataOraInizio;
            public DateTime DataOraFine;


            public ResultLtaPeriodo()
            {
            }

            public ResultLtaPeriodo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataOraInizio, DateTime dDataOraFine, IRecordSet oRS, bool loadLog, System.Data.DataTable tableStatiLta, bool PreserveDataTable, List<string> CodiciLocaliAbilitati) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLog, tableStatiLta, PreserveDataTable, CodiciLocaliAbilitati)
            {
                this.DataOraInizio = dDataOraInizio;
                this.DataOraFine = dDataOraFine;
            }
        }

        public static ResultBaseCampiLogTransazioni GetListaCampiLogTransazioniCS(string Token, clsEnvironment oEnvironment, bool smart)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCampiLogTransazioni oResult = null;
            Exception oError = null;
            System.Collections.SortedList oListaCampi = null;
            Riepiloghi.clsFiltroCampoTransazione[] oListaFiltri = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            string CodiceSistema = "";
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        //Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount()
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        CodiceSistema = (oToken != null && oToken.CodiceSistema != null ? oToken.CodiceSistema.CodiceSistema : "");
                        Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false, true);
                        if (oError == null)
                            if (oAccount == null)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi", "Account non valido");

                        if (oError == null)
                        {
                            bool lFindCS = false;
                            if (oAccount.CodiciSistema != null && oAccount.CodiciSistema.Count > 0)
                            {
                                foreach (Riepiloghi.clsCodiceSistema oCodiceSistemaAccount in oAccount.CodiciSistema)
                                {
                                    if (oCodiceSistemaAccount.CodiceSistema.Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema == CodiceSistema);
                                    else if (oCodiceSistemaAccount.CodiceSistema.Length > 8 && oCodiceSistemaAccount.CodiceSistema.Contains(".") && oCodiceSistemaAccount.CodiceSistema.Split('.')[0].Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema.Split('.')[0] == CodiceSistema);
                                    if (lFindCS) break;
                                }
                            }

                            if (!lFindCS)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi", "Codice Sistema non valido per questo Account.");
                        }

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, CodiceSistema, out oError, oEnvironment);
                            if (oError == null)
                            {
                                oListaCampi = Riepiloghi.clsCampoTransazione.GetListaCampi(oConnection, out oError, smart);
                                if (oListaCampi == null || oListaCampi.Count == 0)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi log transazioni", "Lista campi log transazioni non caricati.");
                                }
                                else
                                {
                                    oListaFiltri = Riepiloghi.clsRiepiloghi.GetFiltriPreimpostati(oConnection, out oError);
                                    if (oError != null)
                                    {
                                        oListaFiltri = null;
                                    }
                                }
                            }
                        }
                    }
                }
                if (oError == null)
                {
                    bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                    if (lCheckServerFiscale)
                    {
                        if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                        {
                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Richiesta campi log transazioni", "Errore imprevisto durante la richiesta della lista dei campi log transazioni.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }


            if (oError == null)
            {
                if (oListaFiltri == null)
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
                }
                else
                {
                    oResult = new ResultBaseCampiLogTransazioni(true, "Lista campi log transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi, oListaFiltri);
                }

                oResult.Sysdate = dSysdate;


            }
            else
            {
                oResult = new ResultBaseCampiLogTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            return oResult;
        }

        public static ResultBaseCampiLta GetListaCampiLtaCS(string Token, clsEnvironment oEnvironment, bool smart)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCampiLta oResult = null;
            Exception oError = null;
            System.Collections.SortedList oListaCampi = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            string CodiceSistema = "";
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        CodiceSistema = (oToken != null && oToken.CodiceSistema != null ? oToken.CodiceSistema.CodiceSistema : "");
                        Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false, true);
                        if (oError == null)
                            if (oAccount == null)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi Lta", "Account non valido");

                        if (oError == null)
                        {
                            bool lFindCS = false;
                            if (oAccount.CodiciSistema != null && oAccount.CodiciSistema.Count > 0)
                            {
                                foreach (Riepiloghi.clsCodiceSistema oCodiceSistemaAccount in oAccount.CodiciSistema)
                                {
                                    if (oCodiceSistemaAccount.CodiceSistema.Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema == CodiceSistema);
                                    else if (oCodiceSistemaAccount.CodiceSistema.Length > 8 && oCodiceSistemaAccount.CodiceSistema.Contains(".") && oCodiceSistemaAccount.CodiceSistema.Split('.')[0].Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema.Split('.')[0] == CodiceSistema);
                                    if (lFindCS) break;
                                }
                            }

                            if (!lFindCS)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Campi Lta", "Codice Sistema non valido per questo Account.");
                        }

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, CodiceSistema, out oError, oEnvironment);
                            if (oError == null)
                            {
                                oListaCampi = Riepiloghi.clsCampoLta.GetListaCampiLta(oConnection, out oError, smart);
                                if (oListaCampi == null || oListaCampi.Count == 0)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista campi Lta transazioni", "Lista campi Lta non caricati.");
                                }

                            }

                            if (oError == null)
                            {
                                bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                                if (lCheckServerFiscale)
                                {
                                    if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Richiesta campi Lta ", "Errore imprevisto durante la richiesta della lista dei campi Lta.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultBaseCampiLta(true, "Lista campi Lta transazioni", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oListaCampi);
            }
            else
            {
                oResult = new ResultBaseCampiLta(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }

            oResult.Sysdate = dSysdate;

            return oResult;
        }

        public class ResultBaseCodiciLocali : ResultBase
        {

            public Riepiloghi.clsCodiceLocale[] CodiciLocali { get; set; }
            public ResultBaseCodiciLocali()
            { }

            public ResultBaseCodiciLocali(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.clsCodiceLocale> codiciLocali) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.CodiciLocali = codiciLocali.ToArray();
            }
        }

        public static ResultBaseCodiciLocali GetListaCodiciLocale(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCodiciLocali oResult = null;
            Exception oError = null;
            System.Collections.SortedList oListaCampi = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            string CodiceSistema = "";
            List<Riepiloghi.clsCodiceLocale> lista = new List<Riepiloghi.clsCodiceLocale>();
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        CodiceSistema = (oToken != null && oToken.CodiceSistema != null ? oToken.CodiceSistema.CodiceSistema : "");
                        Riepiloghi.clsAccount oAccount = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false, true);
                        if (oError == null)
                            if (oAccount == null)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Codici Locali", "Account non valido");

                        if (oError == null)
                        {
                            bool lFindCS = false;
                            if (oAccount.CodiciSistema != null && oAccount.CodiciSistema.Count > 0)
                            {
                                foreach (Riepiloghi.clsCodiceSistema oCodiceSistemaAccount in oAccount.CodiciSistema)
                                {
                                    if (oCodiceSistemaAccount.CodiceSistema.Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema == CodiceSistema);
                                    else if (oCodiceSistemaAccount.CodiceSistema.Length > 8 && oCodiceSistemaAccount.CodiceSistema.Contains(".") && oCodiceSistemaAccount.CodiceSistema.Split('.')[0].Length == 8)
                                        lFindCS = (oCodiceSistemaAccount.CodiceSistema.Split('.')[0] == CodiceSistema);
                                    if (lFindCS) break;
                                }
                            }

                            if (!lFindCS)
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Codici Locali", "Codice Sistema non valido per questo Account.");
                        }

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, CodiceSistema, out oError, oEnvironment);
                            if (oError == null)
                            {
                                lista = Riepiloghi.clsCodiceLocale.GetListaCodiciLocaliREMOTE(CodiceSistema, oConnection, out oError);
                                if (lista != null && lista.Count > 0 && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection))
                                {
                                    if (oToken.CodiceSistema != null && oToken.CodiceSistema.MultiCinema != null && oToken.CodiceSistema.MultiCinema.IdCinema > 0)
                                    {
                                        List<string> CodiciLocaliAbilitati = null;
                                        List<string> OrganizzatoriAbilitati = null;
                                        Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                                        if (CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.Count > 0)
                                            lista = lista.Where(o => CodiciLocaliAbilitati.Contains(o.CodiceLocale)).ToList();
                                        else
                                            lista = new List<Riepiloghi.clsCodiceLocale>() { new Riepiloghi.clsCodiceLocale() { CodiceLocale = "X".PadRight(13, 'X'), CodiceSistema = oToken.CodiceSistema.CodiceSistema, Descrizione = "Nessun codice locale", Enabled = false } };
                                    }
                                }
                            }

                            if (oError == null)
                            {
                                bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                                if (lCheckServerFiscale)
                                {
                                    if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Codici Locali ", "Errore imprevisto durante la richiesta della Lista Codici Locali.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultBaseCodiciLocali(true, "Lista Codici Locali", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), lista);
            }
            else
            {
                oResult = new ResultBaseCodiciLocali(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            return oResult;

        }


        public static bool CheckMultiCinema(IConnection oConnection)
        {
            return Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection);
        }

        // Lista delle transazioni per periodo
        public static ResultTransazionePeriodo GetTransazioni(string Token, DateTime dDataInizio, DateTime dDataFine, bool lDataEvento, string ModalitaStorico, bool loadLta, string PreserveDataTable, Int64 IdFilterExtIdVend, string Mode, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            List<string> OrganizzatoriAbilitati = null;

            string cCodiceSistemaUnico = "";
            bool lCodiceSistemaUnico = false;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetTransazioni", Token, new Dictionary<string, object>() { { "dDataEmiInizio", dDataInizio }, { "dDataEmiFine", dDataFine }, { "ModalitaStorico", ModalitaStorico }, { "PreserveDataTable", PreserveDataTable }, { "IdFilterExtIdVend", IdFilterExtIdVend } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (!lCodiceSistemaUnico)
                                CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                            else if (!lCodiceSistemaUnico)
                            {
                                OrganizzatoriAbilitati = null;
                                CodiciLocaliAbilitati = null;
                            }

                            if (oError == null)
                            {
                                oRS = Riepiloghi.clsRiepiloghi.GetTransazioni(dDataInizio, dDataFine, lDataEvento, loadLta, null, oConnection, GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati, OrganizzatoriAbilitati);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oError == null)
                {
                    try
                    {
                        oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataInizio, dDataFine, ModalitaStorico, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                        oResult.Sysdate = dSysdate;
                    }
                    catch (Exception ex)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
                    }
                }

                if (oError != null)
                {
                    oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataInizio, dDataFine, ModalitaStorico, null, false, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                    oResult.Sysdate = dSysdate;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultTransazionePeriodo", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }


        public static ResultLtaPeriodo GetLta(string Token, DateTime dDataInizio, DateTime dDataFine, bool lDataEmissione, string cCodiceLocale, string cTitoloEvento, bool loadLog, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            ResultLtaPeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            List<string> OrganizzatoriAbilitati = null;
            System.Data.DataTable tableStatiLta = null;

            string cCodiceSistemaUnico = "";
            bool lCodiceSistemaUnico = false;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);
                    //oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "GetLta", Token, new Dictionary<string, object>() { { "dDataInizio", dDataInizio }, { "dDataInizio", dDataFine }, { "cCodiceLocale", cCodiceLocale }, { "cTitoloEvento", cTitoloEvento }, { "PreserveDataTable", PreserveDataTable }  });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_LTA", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (!lCodiceSistemaUnico)
                                CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                oRS = Riepiloghi.clsRiepiloghi.GetLTA(dDataInizio, dDataFine, lDataEmissione, cCodiceLocale, cTitoloEvento, loadLog, oConnection, out oError, CodiciLocaliAbilitati, OrganizzatoriAbilitati, null, null, null, out tableStatiLta);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni", "Errore imprevisto durante lettura delle transazioni.", ex);
            }
            finally
            {
                if (oError == null)
                {
                    try
                    {
                        oResult = new ResultLtaPeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataInizio, dDataFine, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                        oResult.Sysdate = dSysdate;
                    }
                    catch (Exception ex)
                    {
                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura Lta", "Errore imprevisto durante lettura Lta.", ex);
                    }
                }

                if (oError != null)
                {
                    oResult = new ResultLtaPeriodo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataInizio, dDataFine, null, false, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
                    oResult.Sysdate = dSysdate;
                }

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultLtaPeriodo", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Lista delle transazioni annullate per perido (annullanti ed annullati)
        public static ResultTransazionePeriodo GetTransazioniAnnullati(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string ModalitaStorico, bool loadLta, string PreserveDataTable, Int64 IdFilterExtIdVend, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazionePeriodo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            List<string> OrganizzatoriAbilitati = null;

            string cCodiceSistemaUnico = "";
            bool lCodiceSistemaUnico = false;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI_ANNULLATI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            CodiciLocaliAbilitati = null;
                            OrganizzatoriAbilitati = null;
                            if (!lCodiceSistemaUnico)
                                CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                oRS = Riepiloghi.clsRiepiloghi.GetTransazioniAnnullati(dDataEmiInizio, dDataEmiFine, loadLta, oConnection, GetModalitaStoricoFromString(ModalitaStorico), out oError, IdFilterExtIdVend, CodiciLocaliAbilitati, OrganizzatoriAbilitati);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni annullate", "Errore imprevisto durante lettura delle transazioni annullate.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazionePeriodo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE", CodiciLocaliAbilitati);
            }
            else
            {
                oResult = new ResultTransazionePeriodo(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataEmiInizio, dDataEmiFine, ModalitaStorico, null, false, PreserveDataTable.Trim().ToUpper() == "TRUE", null);
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Risultato transazione per carta e sigillo o progressivo che contiene un array di clsResulttransazioni (lista delle transazioni, una sola)
        // ed il dettaglio della richiesta con carta e sigillo o progressivo
        public class ResultTransazioneCartaSigilloProgressivo : ResultBaseGetTransazioni
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;

            public Int64 ProgressivoInizio = 0;
            public Int64 ProgressivoFine = 0;

            public DateTime DataEvento { get; set; }
            public string CodiceLocale { get; set; }
            public string OraEvento { get; set; }
            public string TitoloEvento { get; set; }

            public ResultTransazioneCartaSigilloProgressivo()
            {
            }

            public ResultTransazioneCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Carta, string Sigillo, Int64 Progressivo, IRecordSet oRS, bool loadLta, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLta, PreserveDataTable, null)
            {
                this.Carta = Carta;
                this.Sigillo = Sigillo;
                this.Progressivo = Progressivo;
            }

            public ResultTransazioneCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Carta, Int64 ProgressivoInizio, Int64 ProgressivoFine, IRecordSet oRS, bool loadLta, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLta, PreserveDataTable, null)
            {
                this.Carta = Carta;
                this.ProgressivoInizio = ProgressivoInizio;
                this.ProgressivoFine = ProgressivoFine;
            }

            public ResultTransazioneCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime DataEvento, string CodiceLocale, string OraEvento, string TitoloEvento, IRecordSet oRS, bool loadLta, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLta, PreserveDataTable, null)
            {
                this.DataEvento = DataEvento;
                this.CodiceLocale = CodiceLocale;
                this.OraEvento = OraEvento;
                this.TitoloEvento = TitoloEvento;
            }
        }

        public class ResultLtaCartaSigilloProgressivo : ResultBaseGetLta
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;

            public ResultLtaCartaSigilloProgressivo()
            {
            }

            public ResultLtaCartaSigilloProgressivo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string Carta, string Sigillo, Int64 Progressivo, IRecordSet oRS, bool loadLog, System.Data.DataTable tableStatiLta, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, oRS, loadLog, tableStatiLta, PreserveDataTable, null)
            {
                this.Carta = Carta;
                this.Sigillo = Sigillo;
                this.Progressivo = Progressivo;
            }
        }

        // Ricerca transazione carta e progressivo
        public static ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_PROGRESSIVO(string Token, string Carta, Int64 Progressivo, bool loadLta, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(Carta, Progressivo, loadLta, Token, oConnection, out oError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivo", "Errore imprevisto durante lettura delle transazioni per carta e progressivo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca transazione carta e progressivi
        public static ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_PROGRESSIVI(string Token, string Carta, Int64 ProgressivoInizio, Int64 ProgressivoFine, bool loadLta, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVI(Carta, ProgressivoInizio, ProgressivoFine, Token, loadLta, oConnection, out oError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivi", "Errore imprevisto durante lettura delle transazioni per carta e progressivi.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, ProgressivoInizio, ProgressivoFine, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, ProgressivoInizio, ProgressivoFine, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca transazioni evento
        public static ResultTransazioneCartaSigilloProgressivo GetTransazioniEVENTO(string Token, DateTime DataEvento, string CodiceLocale, string OraEvento, string TitoloEvento, bool loadLta, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioniEVENTO(DataEvento, CodiceLocale, OraEvento, TitoloEvento, loadLta, oConnection, out oError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per Evento", "Errore imprevisto durante lettura delle transazioni per Evento.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), DataEvento, CodiceLocale, OraEvento, TitoloEvento, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), DataEvento, CodiceLocale, OraEvento, TitoloEvento, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca eventi
        public static ResultBaseGetEventi GetEventi(string Token, DateTime DataEvento, string CodiceLocale, string OraEvento, string TitoloEvento, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseGetEventi oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        long idCinema = 0;
                        if (oError == null)
                        {
                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                idCinema = oToken.CodiceSistema.MultiCinema.IdCinema;
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetEVENTI(DataEvento, CodiceLocale, OraEvento, TitoloEvento, idCinema, oConnection, out oError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura Eventi", "Errore imprevisto durante lettura degli Eventi.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultBaseGetEventi(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseGetEventi(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }


        // Ricerca transazione carta e sigillo
        public static ResultTransazioneCartaSigilloProgressivo GetTransazioneCARTA_SIGILLO(string Token, string Carta, string Sigillo, bool loadLta, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultTransazioneCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_TRANSAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetTransazioneCARTA_SIGILLO(Carta, Sigillo, loadLta, Token, oConnection, out oError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e sigillo", "Errore imprevisto durante lettura delle transazioni per carta e sigillo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultTransazioneCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        public static ResultLtaCartaSigilloProgressivo GetLtaCARTA_PROGRESSIVO(string Token, string Carta, Int64 Progressivo, bool loadLog, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultLtaCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Data.DataTable tableStatiLta = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_LTA", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            List<string> CodiciLocaliAbilitati = null;
                            List<string> OrganizzatoriAbilitati = null;

                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                            oRS = Riepiloghi.clsRiepiloghi.GetLTA(null, null, false, null, null, loadLog, oConnection, out oError, CodiciLocaliAbilitati, OrganizzatoriAbilitati, Carta, Progressivo, null, out tableStatiLta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivo", "Errore imprevisto durante lettura delle transazioni per carta e progressivo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultLtaCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultLtaCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        // Ricerca transazione carta e sigillo
        public static ResultLtaCartaSigilloProgressivo GetLtaCARTA_SIGILLO(string Token, string Carta, string Sigillo, bool loadLog, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultLtaCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Data.DataTable tableStatiLta = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_LTA", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            List<string> CodiciLocaliAbilitati = null;
                            List<string> OrganizzatoriAbilitati = null;

                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                            oRS = Riepiloghi.clsRiepiloghi.GetLTA(null, null, false, null, null, loadLog, oConnection, out oError, CodiciLocaliAbilitati, OrganizzatoriAbilitati, Carta, null, Sigillo, out tableStatiLta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e sigillo", "Errore imprevisto durante lettura delle transazioni per carta e sigillo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultLtaCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultLtaCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        public static ResultLtaCartaSigilloProgressivo SetStatoLtaCARTA_PROGRESSIVO(string Token, String NuovoStato, string Carta, Int64 Progressivo, bool loadLog, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultLtaCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Data.DataTable tableStatiLta = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_LTA", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }
                        List<string> CodiciLocaliAbilitati = null;
                        List<string> OrganizzatoriAbilitati = null;
                        if (oError == null)
                        {
                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.SetStatoLTA(loadLog, oConnection, out oError, Carta, Progressivo, "", CodiciLocaliAbilitati, OrganizzatoriAbilitati, NuovoStato, out tableStatiLta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e progressivo", "Errore imprevisto durante lettura delle transazioni per carta e progressivo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError == null)
            {
                oResult = new ResultLtaCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultLtaCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, "", Progressivo, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }


        public static ResultLtaCartaSigilloProgressivo SetStatoLtaCARTA_SIGILLO(string Token, String NuovoStato, string Carta, string Sigillo, bool loadLog, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultLtaCartaSigilloProgressivo oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Data.DataTable tableStatiLta = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_RICERCA_LTA", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        List<string> CodiciLocaliAbilitati = null;
                        List<string> OrganizzatoriAbilitati = null;
                        if (oError == null)
                        {
                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }

                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.SetStatoLTA(loadLog, oConnection, out oError, Carta, null, Sigillo, CodiciLocaliAbilitati, OrganizzatoriAbilitati, NuovoStato, out tableStatiLta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura transazioni per carta e sigillo", "Errore imprevisto durante lettura delle transazioni per carta e sigillo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultLtaCartaSigilloProgressivo(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultLtaCartaSigilloProgressivo(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, 0, oRS, loadLog, tableStatiLta, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }

            oResult.Sysdate = dSysdate;

            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        #endregion

        #region "Ritorno delle tabelle di base"

        // Classe generica Dictionary per ritorno tabelle standard CODICE e DESCRIZIONE
        //[XmlRoot("dictionary")]
        public class clsDizionarioTabella<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
        {
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(System.Xml.XmlReader reader)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                bool wasEmpty = reader.IsEmptyElement;
                reader.Read();

                if (wasEmpty)
                    return;

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");

                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    this.Add(key, value);

                    reader.ReadEndElement();

                    reader.MoveToContent();
                }

                reader.ReadEndElement();
            }

            public void WriteXml(System.Xml.XmlWriter writer)
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (TKey key in this.Keys)
                {
                    writer.WriteStartElement("item");

                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, key);
                    writer.WriteEndElement();

                    writer.WriteStartElement("value");
                    TValue value = this[key];
                    valueSerializer.Serialize(writer, value);

                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }

        }

        public class GenericColumn
        {
            public string Field = "";
            public string Type = "";

            public GenericColumn()
            {
            }

            public GenericColumn(System.Data.DataColumn oCol)
            {
                this.Field = oCol.ColumnName;
                if (oCol.DataType == typeof(string))
                {
                    this.Type = "S";
                }
                else if (oCol.DataType == typeof(DateTime))
                {
                    this.Type = "D";
                }
                else if (oCol.DataType == typeof(decimal) || oCol.DataType == typeof(int))
                {
                    this.Type = "N";
                }
            }

            public static List<object> GetRow(System.Data.DataRow oRow)
            {
                List<object> SingleRow = new List<object>();
                foreach (System.Data.DataColumn oCol in oRow.Table.Columns)
                {
                    SingleRow.Add(oRow[oCol.ColumnName]);
                }
                return SingleRow;
            }

            public static clsDizionarioTabella<String, GenericColumn> GetCols(System.Data.DataTable oTable)
            {
                clsDizionarioTabella<String, GenericColumn> oColumns = new clsDizionarioTabella<string, GenericColumn>();
                foreach (System.Data.DataColumn oCol in oTable.Columns)
                {
                    GenericColumn oColValue = new GenericColumn(oCol);
                    oColumns.Add(oCol.ColumnName, oColValue);
                }
                return oColumns;
            }

            public static List<List<object>> GetRows(System.Data.DataTable oTable)
            {
                List<List<object>> oRowsValue = new List<List<object>>();
                foreach (System.Data.DataRow oRow in oTable.Rows)
                {
                    oRowsValue.Add(GetRow(oRow));
                }
                return oRowsValue;
            }
        }

        public class ResultBaseGenericTable : ResultBase
        {
            public System.Data.DataTable oTable;

            public ResultBaseGenericTable()
            {
            }

            public ResultBaseGenericTable(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                System.Data.DataTable oTableSource = (System.Data.DataTable)oRS;
                this.oTable = new System.Data.DataTable("TABLE");
                foreach (System.Data.DataColumn oCol in oTableSource.Columns)
                {
                    System.Data.DataColumn oNewCol = new System.Data.DataColumn(oCol.ColumnName, oCol.DataType, oCol.Expression, oCol.ColumnMapping);
                    this.oTable.Columns.Add(oNewCol);
                }

                foreach (System.Data.DataRow oRow in oTableSource.Rows)
                {
                    System.Data.DataRow oNewRow = this.oTable.NewRow();
                    foreach (System.Data.DataColumn oCol in this.oTable.Columns)
                    {
                        oNewRow[oCol.ColumnName] = oRow[oCol.ColumnName];
                    }
                    this.oTable.Rows.Add(oNewRow);
                }
            }
        }

        private static ResultBaseGenericTable GetGenericTable(string Token, string QueryTabella, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseGenericTable oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery(QueryTabella);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseGenericTable(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseGenericTable(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            //if (oRS != null) { oRS.Close(); }
            return oResult;
        }

        // Classe per risultato tabella standard
        public class ResultBaseTabella : ResultBase
        {
            public clsDizionarioTabella<string, string> Righe = new clsDizionarioTabella<string, string>();

            public ResultBaseTabella()
            {
            }

            public ResultBaseTabella(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        if (!Righe.ContainsKey(oRS.Fields("codice").Value.ToString()))
                        {
                            Righe.Add(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                        }
                        oRS.MoveNext();
                    }
                }
            }
        }

        private static ResultBaseTabella GetTabellaStandard(string Token, string QueryTabella, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseTabella oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery(QueryTabella);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseTabella(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseTabella(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;

        }

        // VR_TIPOEVENTO
        public static ResultBaseTabella GetTabellaTipoEvento(string Token, clsEnvironment oEnvironment)
        {
            return GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOEVENTO ORDER BY CODICE",  oEnvironment);
        }

        // VR_ORDINIDIPOSTO
        public static ResultBaseTabella GetTabellaOrdiniDiPosto(string Token, clsEnvironment oEnvironment)
        {
            return GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_ORDINIDIPOSTO ORDER BY CODICE",  oEnvironment);
        }

        // VR_TIPOTITOLO
        public static ResultBaseTabella GetTabellaTipoTitolo(string Token, clsEnvironment oEnvironment)
        {
            return GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOTITOLO ORDER BY CODICE", oEnvironment);
        }

        // VR_TIPO_TITOLO_OMAGGIO
        public static ResultBaseTabella GetTabellaTipoTitoloOmaggio(string Token, clsEnvironment oEnvironment)
        {
            return GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPO_TITOLO_OMAGGIO ORDER BY CODICE",  oEnvironment);
        }

        // VR_TIPOPROVENTO
        public static ResultBaseTabella GetTabellaTipoProvento(string Token, clsEnvironment oEnvironment)
        {
            return GetTabellaStandard(Token, "SELECT CODICE, DESCRIZIONE FROM VR_TIPOPROVENTO ORDER BY CODICE",  oEnvironment);
        }

        // classe per VR_CRITERIO_OMAGGI_CONFIG
        public class ResultBaseCriteriOmaggioConfig : ResultBase
        {
            public clsDizionarioTabella<Int64, Riepiloghi.clsCriterioOmaggiConfig> Righe = new clsDizionarioTabella<long, Riepiloghi.clsCriterioOmaggiConfig>();

            public ResultBaseCriteriOmaggioConfig()
            {
            }

            public ResultBaseCriteriOmaggioConfig(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                if (oRS != null && oRS.RecordCount > 0)
                {
                    oRS.MoveFirst();
                    while (!oRS.EOF)
                    {
                        Int64 nOrdine = 0;
                        Int64 nEnabled = 0;
                        if (!oRS.Fields("ordine").IsNull &&
                            Int64.TryParse(oRS.Fields("ordine").Value.ToString(), out nOrdine) &&
                            Int64.TryParse(oRS.Fields("enabled").Value.ToString(), out nEnabled))
                        {
                            Riepiloghi.clsCriterioOmaggiConfig oConfig = new Riepiloghi.clsCriterioOmaggiConfig(nOrdine, nEnabled, oRS.Fields("origine").Value.ToString(), oRS.Fields("ordine_di_posto").Value.ToString(), oRS.Fields("tipo_titolo").Value.ToString(), oRS.Fields("descrizione").Value.ToString());
                            if (!Righe.ContainsKey(oConfig.Ordine))
                            {
                                Righe.Add(oConfig.Ordine, oConfig);
                            }
                        }

                        oRS.MoveNext();
                    }
                }
            }
        }

        // VR_CRITERIO_OMAGGI_CONFIG
        public static ResultBaseCriteriOmaggioConfig GetTabellaCriteriOmaggiConfig(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseCriteriOmaggioConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT * FROM VR_CRITERIO_OMAGGI_CONFIG ORDER BY ORDINE");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura tabella", "Errore imprevisto durante lettura della tabella.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
            }
            else
            {
                oResult = new ResultBaseCriteriOmaggioConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            if (oRS != null) { oRS.Close(); }
            return oResult;
        }

        // VR_FILM
        public static ResultBaseGenericTable GetTabellaVR_FILM(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_FILM ORDER BY TITOLO",  oEnvironment);
        }

        // VR_PACCHETTO
        public static ResultBaseGenericTable GetTabellaVR_PACCHETTO(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_PACCHETTO ORDER BY DESCRIZIONE",  oEnvironment);
        }

        // VR_ORGANIZZATORI
        public static ResultBaseGenericTable GetTabellaVR_ORGANIZZATORI(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_ORGANIZZATORI ORDER BY RAG_SOC",  oEnvironment);
        }

        // VR_NOLEGGIO
        public static ResultBaseGenericTable GetTabellaVR_NOLEGGIO(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_NOLEGGIO ORDER BY RAG_SOC",  oEnvironment);
        }

        // VR_PRODUTTORI
        public static ResultBaseGenericTable GetTabellaVR_PRODUTTORI(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_PRODUTTORI ORDER BY RAG_SOC",  oEnvironment);
        }

        // VR_ODPBIGLIETTO
        public static ResultBaseGenericTable GetTabellaVR_ODPBIGLIETTO_DESCR(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_ODPBIGLIETTO_DESCR ORDER BY IDSALA, ODP, IDTIPOBIGL", oEnvironment);
        }

        // VR_TIPO_BIGLIETTO
        public static ResultBaseGenericTable GetTabellaVR_TIPO_BIGLIETTO(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_TIPO_BIGLIETTO ORDER BY IDTIPOBIGL",  oEnvironment);
        }

        // VR_SALE
        public static ResultBaseGenericTable GetTabellaVR_SALE(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_SALE ORDER BY IDSALA",  oEnvironment);
        }

        // VR_SALE_LUOGO
        public static ResultBaseGenericTable GetTabellaVR_SALE_LUOGO(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_SALE_LUOGO ORDER BY IDSALA",  oEnvironment);
        }

        // VR_POSTI
        public static ResultBaseGenericTable GetTabellaVR_POSTI(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_POSTI ORDER BY IDSALA, FILA, COLONNA",  oEnvironment);
        }

        // VR_TE_IMPOSTE
        public static ResultBaseGenericTable GetTabellaVR_TE_IMPOSTE(string Token, clsEnvironment oEnvironment)
        {
            return GetGenericTable(Token, "SELECT * FROM VR_TE_IMPOSTE",  oEnvironment);
        }

        #endregion

        #region "Incidenza Intrattenimento"

        public class ResultBaseIncidenzaIntrattenimento : ResultBase
        {
            public Riepiloghi.clsIncidenzaIntrattenimentoPerEvento[] Eventi = new Riepiloghi.clsIncidenzaIntrattenimentoPerEvento[0];
            public DateTime InizioPeriodo = DateTime.MinValue;
            public DateTime FinePeriodo = DateTime.MaxValue;
            public bool ExistsEventoSenzaPercentuale = false;

            public ResultBaseIncidenzaIntrattenimento()
                : base()
            {

            }

            public ResultBaseIncidenzaIntrattenimento(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {

            }
        }

        public static ResultBaseIncidenzaIntrattenimento CheckIncidenzaIntrattenimento(string Token, DateTime InizioPeriodo, DateTime FinePeriodo, clsEnvironment oEnvironment)
        {
            ResultBaseIncidenzaIntrattenimento oRet = new ResultBaseIncidenzaIntrattenimento();
            oRet.InizioPeriodo = InizioPeriodo;
            oRet.FinePeriodo = FinePeriodo;

            DateTime dStart = DateTime.Now;
            Exception oError = null;
            bool lExistsEventoSenzaPercentuale = false;
            List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oLista = new List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento>();

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            if (InizioPeriodo > FinePeriodo)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Periodo non valido.");
                            }
                            else
                            {
                                if (InizioPeriodo.Date == FinePeriodo.Date || InizioPeriodo.Date == FinePeriodo.Date.AddDays(-1))
                                {
                                    List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oListaCompleta = Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.GetIncidenzaIntrattenimentoPerEvento(oConnection, oRet.InizioPeriodo, oRet.FinePeriodo, out lExistsEventoSenzaPercentuale, out oError);

                                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                                    Riepiloghi.clsAccount Account = Riepiloghi.clsAccount.GetAccount(oToken.AccountId, oConnectionInternal, out oError, true, false, true);

                                    foreach (Riepiloghi.clsIncidenzaIntrattenimentoPerEvento oItem in oListaCompleta)
                                    {
                                        bool lAppend = false;
                                        foreach (Riepiloghi.clsCodiceSistema oCodiceSistema in Account.CodiciSistema)
                                        {
                                            if (oCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oCodiceSistema.CodiciLocale)
                                                {
                                                    lAppend = (oCodiceLocale.CodiceLocale == oItem.CodiceLocale);
                                                    if (lAppend) { break; }
                                                }
                                            }
                                            if (lAppend) { break; }
                                        }
                                        if (lAppend)
                                            oLista.Add(oItem);
                                    }
                                }
                                else
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Periodo troppo esteso.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("CheckIncidenzaIntrattenimento", "Errore imprevisto nella lettura degli eventi per verifica incidenza intrattenimento.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(true, "Incidenza Intrattenimento:" + (oLista.Count == 0 ? "Nessun evento di tipo Intrattenimento nel periodo." : (oLista.Count == 1 ? "un evento" : oLista.Count.ToString() + " eventi") + " di tipo Intrattenimento nel periodo."), 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
                oRet.ExistsEventoSenzaPercentuale = lExistsEventoSenzaPercentuale;
                oRet.Eventi = oLista.ToArray();
            }

            oRet.InizioPeriodo = InizioPeriodo;
            oRet.FinePeriodo = FinePeriodo;

            return oRet;
        }

        public static ResultBaseIncidenzaIntrattenimento SetIncidenzaIntrattenimento(string Token, ResultBaseIncidenzaIntrattenimento IncidenzaIntrattenimentoEventi, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseIncidenzaIntrattenimento oRet = new ResultBaseIncidenzaIntrattenimento();
            bool lExistsEventoSenzaPercentuale = false;
            List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento> oLista = new List<Riepiloghi.clsIncidenzaIntrattenimentoPerEvento>();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            if (Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.SetIncidenzaIntrattenimentoPerEvento(oConnection, IncidenzaIntrattenimentoEventi.Eventi, out oError))
                            {
                                oLista = Riepiloghi.clsIncidenzaIntrattenimentoPerEvento.GetIncidenzaIntrattenimentoPerEvento(oConnection, oRet.InizioPeriodo, oRet.FinePeriodo, out lExistsEventoSenzaPercentuale, out oError);
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("SetIncidenzaIntrattenimento", "Errore imprevisto nella lettura degli eventi per verifica incidenza intrattenimento.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                oRet.ExistsEventoSenzaPercentuale = lExistsEventoSenzaPercentuale;
                oRet.Eventi = oLista.ToArray();
            }
            else
            {
                oRet = new ResultBaseIncidenzaIntrattenimento(true, "SetIncidenzaIntrattenimento", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart)); ;
            }
            oRet.InizioPeriodo = IncidenzaIntrattenimentoEventi.InizioPeriodo;
            oRet.FinePeriodo = IncidenzaIntrattenimentoEventi.FinePeriodo;

            return oRet;
        }

        #endregion

        #endregion

        #region "Operazioni"

        #region "Annulli"

        public class clsWSCausaleAnnullo
        {
            public string Codice = "";
            public string Descrizione = "";

            public clsWSCausaleAnnullo()
            { }

            public clsWSCausaleAnnullo(string codice, string descrizione)
            {
                this.Codice = codice;
                this.Descrizione = descrizione;
            }
        }

        public class clsWSCheckAnnullo
        {
            public string Carta = "";
            public string Sigillo = "";
            public long? Progressivo = 0;
            public long Error = 0;
            public string Descrizione = "";
            public List<string> Warnings = new List<string>();
            public List<clsWSCausaleAnnullo> Causali = new List<clsWSCausaleAnnullo>();
            public clsResultTransazione Transazione = null;
            public clsResultTransazione TransazioneAnnullante = null;

            public clsWSCheckAnnullo()
            {

            }
        }

        // classe di Controllo per annullo transazione
        public class ResultBaseCheckAnnulloTransazioni : ResultBase
        {
            public List<clsWSCheckAnnullo> CheckTransazioni = new List<clsWSCheckAnnullo>();

            public ResultBaseCheckAnnulloTransazioni()
            {
            }

            public ResultBaseCheckAnnulloTransazioni(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
            }
        }

        // classe per annullo transazione
        public class ResultBaseAnnulloTransazione : ResultBase
        {
            public string Carta = "";
            public string Sigillo = "";
            public Int64 Progressivo = 0;
            public clsResultTransazione Transazione = null;
            public string CausaleAnnullamento = "";

            public string CartaAnnullante = "";
            public string SigilloAnnullante = "";
            public Int64 ProgressivoAnnullante = 0;
            public clsResultTransazione TransazioneAnnullante = null;
            public List<string> WarningsPostAnnullo = new List<string>();

            public ResultBaseAnnulloTransazione()
            {
                this.WarningsPostAnnullo = new List<string>();
            }

            public ResultBaseAnnulloTransazione(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceCarta, string cSigillo, Int64 nProgressivo, string cCausaleAnnullamento, clsResultTransazione oTransazione, string cCodiceCartaAnnullante, string cSigilloAnnullante, Int64 nProgressivoAnnullante, clsResultTransazione oTransazioneAnnullante)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Carta = cCodiceCarta;
                this.Sigillo = cSigillo;
                this.Progressivo = nProgressivo;
                this.CausaleAnnullamento = cCausaleAnnullamento;
                this.Transazione = oTransazione;
                this.CartaAnnullante = cCodiceCartaAnnullante;
                this.SigilloAnnullante = cSigilloAnnullante;
                this.ProgressivoAnnullante = nProgressivoAnnullante;
                this.TransazioneAnnullante = oTransazioneAnnullante;
                this.WarningsPostAnnullo = new List<string>();
            }

            public ResultBaseAnnulloTransazione(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string cCodiceCarta, string cSigillo, Int64 nProgressivo, string cCausaleAnnullamento, clsResultTransazione oTransazione, string cCodiceCartaAnnullante, string cSigilloAnnullante, Int64 nProgressivoAnnullante, clsResultTransazione oTransazioneAnnullante, List<Exception> ExceptionPostAnnullo)
                : this(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime, cCodiceCarta, cSigillo, nProgressivo, cCausaleAnnullamento, oTransazione, cCodiceCartaAnnullante, cSigilloAnnullante, nProgressivoAnnullante, oTransazioneAnnullante)
            {
                this.WarningsPostAnnullo = new List<string>();
                if (ExceptionPostAnnullo != null)
                {
                    foreach (Exception ex in ExceptionPostAnnullo)
                    {
                        this.WarningsPostAnnullo.Add(ex.Message);
                    }
                }
            }
        }

        //, Int64 idCalend
        //public static ResultBaseCheckAnnulloTransazioni CheckAnnulloTransazioni(string Token, string Carta, string Sigillo, Int64 primoProgressivo, Int64 ultimoProgressivo, Int64 idCalend, bool loadLta, clsEnvironment oEnvironment)
        public static ResultBaseCheckAnnulloTransazioni CheckAnnulloTransazioni(string Token, string Carta, string Sigillo, Int64 primoProgressivo, Int64 ultimoProgressivo, Int64 idCalend, 
            string parCfOrganizzatore, string parTitoloAbbonamento, DateTime? parDataEmissione, DateTime? parDataEvento, bool loadLta, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            string cDescrizione = "";
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<Riepiloghi.clsCheckAnnulloTransazione> oListaCheckAnnulli = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ANNULLI", Token, out oError) || true)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            oListaCheckAnnulli = Riepiloghi.clsRiepiloghi.CheckAnnulloTransazione(Token, Carta, Sigillo, primoProgressivo, ultimoProgressivo, idCalend, parCfOrganizzatore, parTitoloAbbonamento, parDataEmissione, parDataEvento, loadLta, oConnection, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            if (oError != null)
            {
                return new ResultBaseCheckAnnulloTransazioni(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            else
            {
                ResultBaseCheckAnnulloTransazioni oResult = new ResultBaseCheckAnnulloTransazioni(true, cDescrizione, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                if (oListaCheckAnnulli != null)
                {
                    foreach (Riepiloghi.clsCheckAnnulloTransazione item in oListaCheckAnnulli)
                    {
                        clsWSCheckAnnullo wsItem = new clsWSCheckAnnullo();
                        wsItem.Carta = item.Carta;
                        wsItem.Error = item.Error;
                        wsItem.Descrizione = item.Descrizione;
                        wsItem.Progressivo = item.Progressivo;
                        wsItem.Sigillo = item.Sigillo;
                        if (item.Transazione != null && item.Transazione.Rows != null && item.Transazione.Rows.Count > 0)
                        {
                            wsItem.Transazione = new clsResultTransazione(item.Transazione.Rows[0], loadLta);
                        }

                        if (item.Annullamento != null && item.Annullamento.Rows != null && item.Annullamento.Rows.Count > 0)
                        {
                            wsItem.TransazioneAnnullante = new clsResultTransazione(item.Annullamento.Rows[0], loadLta);
                        }

                        if (item.Warnings != null && item.Warnings.Count > 0)
                        {
                            wsItem.Warnings = new List<string>();
                            foreach (KeyValuePair<long, string> itemWarning in item.Warnings)
                            {
                                wsItem.Warnings.Add(itemWarning.Value);
                            }
                        }

                        if (wsItem.Error == 0)
                        {
                            if (item.Causali != null && item.Causali.Count > 0)
                            {
                                wsItem.Causali = new List<clsWSCausaleAnnullo>();
                                foreach (KeyValuePair<string, string> itemCausale in item.Causali)
                                {
                                    wsItem.Causali.Add(new clsWSCausaleAnnullo(itemCausale.Key, itemCausale.Value));
                                }
                            }
                        }

                        oResult.CheckTransazioni.Add(wsItem);
                    }
                }

                oResult.Sysdate = dSysdate;

                return oResult;
            }
        }


        public static ResultBaseAnnulloTransazione AnnulloTransazione(string Token, string Carta, string Sigillo, Int64? Progressivo, string CausaleAnnullamento, bool loadLta, clsEnvironment oEnvironment)
        {
            ResultBaseAnnulloTransazione oResult = null;
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            Int64 nRet = 0;
            string CartaAnnullante = "";
            string SigilloAnnullante = "";
            Int64 ProgressivoAnnullante = 0;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            IRecordSet oRSTransazione = null;
            IRecordSet oRSTransazioneAnnullo = null;
            List<Exception> ExceptionsPostAnnullo = null;

            long nProgressivoDaAnnullare = 0;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "AnnulloTransazione", Token, new Dictionary<string, object>() { { "Carta", Carta }, { "Sigillo", (Sigillo == null ? "" : Sigillo) }, { "Progressivo", (Progressivo == null ? "" : Progressivo.ToString()) } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_ANNULLI", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {

                            ResultBaseCheckAnnulloTransazioni oCheckAnnulli = null;
                            if (CausaleAnnullamento == null || string.IsNullOrEmpty(CausaleAnnullamento) || CausaleAnnullamento.Trim() == "")
                            {
                                oError = new Exception("Impossibile annullare la transazione senza indicare la causale di annullamento.");
                            }


                            if (oError == null)
                            {
                                if (Progressivo != null && Progressivo > 0)
                                {
                                    nProgressivoDaAnnullare = (long)Progressivo;
                                    oCheckAnnulli = CheckAnnulloTransazioni(Token, Carta, Sigillo, nProgressivoDaAnnullare, nProgressivoDaAnnullare, 0, "", "", null, null, loadLta, oEnvironment);
                                }
                                else if (Sigillo != null && !string.IsNullOrEmpty(Sigillo) && Sigillo.Trim() != "")
                                {
                                    oCheckAnnulli = CheckAnnulloTransazioni(Token, Carta, Sigillo, nProgressivoDaAnnullare, nProgressivoDaAnnullare, 0, "", "", null, null, loadLta, oEnvironment);
                                }
                                else
                                    oError = new Exception("Impossibile annullare la transazione senza Carta/Progressivo o Sigillo.");
                            }

                            if (oError == null && oCheckAnnulli != null)
                            {
                                if (oCheckAnnulli.StatusOK)
                                {
                                    if (oCheckAnnulli.CheckTransazioni != null && oCheckAnnulli.CheckTransazioni.Count == 1)
                                    {
                                        clsWSCheckAnnullo oItemDaAnnullare = oCheckAnnulli.CheckTransazioni[0];

                                        if (oItemDaAnnullare.Error != 0)
                                        {
                                            nRet = oItemDaAnnullare.Error;
                                            oError = new Exception(oItemDaAnnullare.Descrizione);
                                        }
                                        else
                                        {
                                            if (oItemDaAnnullare.Transazione != null)
                                            {
                                                if ((Carta == null || oItemDaAnnullare.Transazione.CodiceCarta == Carta) &&
                                                    (nProgressivoDaAnnullare == 0 || nProgressivoDaAnnullare == oItemDaAnnullare.Transazione.ProgressivoTitolo) &&
                                                    (Sigillo == null || oItemDaAnnullare.Transazione.Sigillo == Sigillo))
                                                {
                                                    if (oItemDaAnnullare.Causali != null && oItemDaAnnullare.Causali.Count > 0)
                                                    {
                                                        bool lFind = false;
                                                        foreach (clsWSCausaleAnnullo oItemCausale in oItemDaAnnullare.Causali)
                                                        {
                                                            if (oItemCausale.Codice == CausaleAnnullamento)
                                                            {
                                                                lFind = true;
                                                                break;
                                                            }
                                                        }
                                                        if (!lFind)
                                                        {
                                                            oError = new Exception("La causale indicata non e' applicabile per l'annullo della transazione.");
                                                        }
                                                        else
                                                        {
                                                            // per ora tutto ok
                                                        }
                                                    }
                                                    else
                                                        oError = new Exception("Nessuna causale applicabile per l'annullo della transazione.");
                                                }
                                                else
                                                    oError = new Exception("Transazione da annullare non valida");
                                            }
                                            else
                                                oError = new Exception("Transazione da annullare non trovata");
                                        }
                                    }
                                    else
                                        oError = new Exception("Transazione da annullare non trovata");
                                }
                                else
                                    oError = new Exception(oCheckAnnulli.StatusMessage);
                            }
                            else
                                oError = new Exception("Errore nel controllo della transazione da annullare." + (oError != null ? oError.Message : ""));


                            if (oError == null)
                            {
                                bool lRetAnnullo = false;

                                if (nProgressivoDaAnnullare > 0)
                                    lRetAnnullo = Riepiloghi.clsRiepiloghi.AnnulloTransazione(Token, Carta, Sigillo, nProgressivoDaAnnullare, CausaleAnnullamento, loadLta, oConnection, out oError, out CartaAnnullante, out SigilloAnnullante, out ProgressivoAnnullante, out oRSTransazione, out oRSTransazioneAnnullo, out ExceptionsPostAnnullo);
                                else if (Sigillo != null && !string.IsNullOrEmpty(Sigillo) && Sigillo.Trim() != "")
                                    lRetAnnullo = Riepiloghi.clsRiepiloghi.AnnulloTransazione(Token, Carta, Sigillo, 0, CausaleAnnullamento, loadLta, oConnection, out oError, out CartaAnnullante, out SigilloAnnullante, out ProgressivoAnnullante, out oRSTransazione, out oRSTransazioneAnnullo, out ExceptionsPostAnnullo);
                                else
                                    oError = new Exception("Impossibile annullare la transazione senza Carta/Progressivo o Sigillo.");

                                if (!lRetAnnullo)
                                    if (oError == null)
                                        oError = new Exception("Errore imprevisto durante l'annullo della transazione.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Controlli annulli transazione", "Errore imprevisto nel controllo annullo transazione.", ex);
            }
            finally
            {
                if (oError != null)
                {
                    oResult = new ResultBaseAnnulloTransazione(false, oError.Message, nRet, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, nProgressivoDaAnnullare, CausaleAnnullamento, (oRSTransazione != null ? new clsResultTransazione(oRSTransazione, loadLta) : null), "", "", 0, null);
                }
                else
                {
                    oResult = new ResultBaseAnnulloTransazione(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Carta, Sigillo, nProgressivoDaAnnullare, CausaleAnnullamento, new clsResultTransazione(oRSTransazione, loadLta), CartaAnnullante, SigilloAnnullante, ProgressivoAnnullante, new clsResultTransazione(oRSTransazioneAnnullo, loadLta), ExceptionsPostAnnullo);
                }

                oResult.Sysdate = dSysdate;

                if (oLogRequest != null)
                {
                    Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ResultAnnulloTransazione", oResult);
                }

                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oResult;
        }

        #endregion

        #region "Generazione Riepilogo"

        // Classe per singola riga del riepilogo da generare
        public class clsResultBaseRiepilogoDaGenerare
        {
            public string GiornalieroMensile = "";
            public DateTime GiornoMese = DateTime.MinValue;
            public DateTime UltimaEmissione = DateTime.MinValue;
            public DateTime DataOraGenerazione = DateTime.MinValue;
            public string DescGenerazione = "";

            public clsResultBaseRiepilogoDaGenerare()
            {
            }

            public clsResultBaseRiepilogoDaGenerare(string cGiornalieroMensile, DateTime dGiornoMese, DateTime dUltimaEmissione, DateTime dDataOraGenerazione, string cDescGenerazione)
            {
                this.GiornalieroMensile = cGiornalieroMensile;
                this.GiornoMese = dGiornoMese;
                this.UltimaEmissione = dUltimaEmissione;
                this.DataOraGenerazione = dDataOraGenerazione;
                this.DescGenerazione = cDescGenerazione;
            }
        }

        // Risposta del servizio
        public class ResultBaseRiepiloghiDaGenerare : ResultBase
        {
            public clsResultBaseRiepilogoDaGenerare[] RiepiloghiDaGenerare = null;
            public System.Data.DataTable TableRiepiloghiDaGenerare = null;
            public string GiornalieroMensile = "";

            public ResultBaseRiepiloghiDaGenerare()
            {
            }

            public ResultBaseRiepiloghiDaGenerare(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, string GiornalieroMensile, IRecordSet oRS, bool PreserveDataTable)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.GiornalieroMensile = GiornalieroMensile;
                if (oRS != null && !oRS.EOF)
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[(int)oRS.RecordCount];
                    int Index = 0;
                    while (!oRS.EOF)
                    {
                        this.RiepiloghiDaGenerare[Index] = new clsResultBaseRiepilogoDaGenerare(oRS.Fields("giornaliero_mensile").Value.ToString(),
                                                                                                (oRS.Fields("giorno_mese").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("giorno_mese").Value),
                                                                                                (oRS.Fields("ultima_emissione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("ultima_emissione").Value),
                                                                                                (oRS.Fields("dataora_generazione").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("dataora_generazione").Value),
                                                                                                oRS.Fields("desc_generazione").Value.ToString());
                        Index += 1;
                        oRS.MoveNext();
                    }
                    if (PreserveDataTable)
                    {
                        this.TableRiepiloghiDaGenerare = (System.Data.DataTable)oRS;
                        this.TableRiepiloghiDaGenerare.TableName = "RIEPILOGHI_DA_GENERARE";
                    }
                }
                else
                {
                    this.RiepiloghiDaGenerare = new clsResultBaseRiepilogoDaGenerare[0];
                }
            }
        }

        public static ResultBaseRiepiloghiDaGenerare GetRiepiloghiDaGenerare(string Token, string GiornalieroMensile, string PreserveDataTable, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseRiepiloghiDaGenerare oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_CREAZIONE", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRS = Riepiloghi.clsRiepiloghi.GetRiepiloghiDaGenerare(oConnection, GiornalieroMensile, out oError);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Lettura riepiloghi da generare", "Errore imprevisto durante la lettura dei riepiloghi da generare.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, oRS, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            else
            {
                oResult = new ResultBaseRiepiloghiDaGenerare(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), GiornalieroMensile, null, PreserveDataTable.Trim().ToUpper() == "TRUE");
            }
            if (oRS != null && PreserveDataTable.Trim().ToUpper() != "TRUE") { oRS.Close(); }
            return oResult;
        }

        public class ResultBaseGenerazioneRiepilogo : ResultBase
        {
            public List<Riepiloghi.clsRiepilogoGeneratoWebApi> ListaRiepiloghi { get; set; }

            public Riepiloghi.clsRiepilogoGeneratoWebApi RichiestaRiepilogo { get; set; }

            public bool EmailToSendOrReceive { get; set; }

            public ResultBaseGenerazioneRiepilogo()
            {
            }

            public ResultBaseGenerazioneRiepilogo(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.clsRiepilogoGeneratoWebApi dettaglioRiepilogo) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.RichiestaRiepilogo = dettaglioRiepilogo;
            }
        }

        //public class clsRiepilogoGenerato
        //{
        //    public string GiornalieroMensile = "";
        //    public DateTime GiornoMese = DateTime.MinValue;
        //    public DateTime DataOraGenerazione = DateTime.MinValue;
        //    public DateTime DataOraSpedizione = DateTime.MinValue;
        //    public int StatoSpedizione = 0;
        //    public string DescrizioneStatoSpedizione = "Non spedito";
        //    public DateTime DataOraMasterizzazione = DateTime.MinValue;
        //    public int StatoMasterizzazione = 0;

        //    public string FileLogFirmato = "";
        //    public string FileRiepilogo = "";
        //    public string FileEmail = "";
        //    public Int64 Progressivo = 0;

        //    public string FileLogNonFirmato = "";
        //    public string FileRiepilogoNonFirmato = "";
        //    public byte[] BytesLogNonFirmato = new byte[] { };
        //    public byte[] BytesRiepilogoNonFirmato = new byte[] { };

        //    public clsRiepilogoGenerato()
        //    {

        //    }

        //    public clsRiepilogoGenerato(string GiornalieroMensile, DateTime GiornoMese, string cFileLogFirmato, string cFileRiepilogo, string cFileEmail, Int64 Progressivo, DateTime DataOraGenerazione, DateTime DataOraSpedizione, DateTime DataOraMasterizzazione)
        //    {
        //        this.GiornalieroMensile = GiornalieroMensile;
        //        this.GiornoMese = GiornoMese;
        //        this.Progressivo = Progressivo;
        //        this.FileLogFirmato = cFileLogFirmato;
        //        this.FileRiepilogo = cFileRiepilogo;
        //        this.FileEmail = cFileEmail;
        //        this.DataOraGenerazione = DataOraGenerazione;
        //        this.DataOraSpedizione = DataOraSpedizione;
        //        this.DataOraMasterizzazione = DataOraMasterizzazione;
        //    }

        //}

        //public class ResultBaseRiepiloghiGenerati : ResultBase
        //{
        //    public clsRiepilogoGenerato[] RiepiloghiGenerati = null;

        //    public ResultBaseRiepiloghiGenerati()
        //    {
        //    }

        //    public ResultBaseRiepiloghiGenerati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
        //        : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
        //    {
        //        if (oRS != null && !oRS.EOF)
        //        {
        //            this.RiepiloghiGenerati = new clsRiepilogoGenerato[(int)oRS.RecordCount];
        //            int Index = 0;
        //            while (!oRS.EOF)
        //            {
        //                this.RiepiloghiGenerati[Index] = new clsRiepilogoGenerato(oRS.Fields("TIPO").Value.ToString(),
        //                                                                          (oRS.Fields("GIORNO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("GIORNO").Value),
        //                                                                          "",
        //                                                                          oRS.Fields("NOME").Value.ToString(),
        //                                                                          "",
        //                                                                          Int64.Parse(oRS.Fields("PROG").Value.ToString()),
        //                                                                          (oRS.Fields("DATA_GENERAZIONE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_GENERAZIONE").Value),
        //                                                                          (oRS.Fields("MAIL_DATE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("MAIL_DATE").Value),
        //                                                                          DateTime.MinValue);
        //                Index += 1;
        //                oRS.MoveNext();
        //            }
        //        }
        //        else
        //        {
        //            this.RiepiloghiGenerati = new clsRiepilogoGenerato[0];
        //        }
        //    }

        //}






        // Generazione del riepilogo con passaggio data in formato data


        public static ResultBaseGenerazioneRiepilogo CreaRiepilogo(string Token, DateTime Giorno, string GiornalieroMensile, clsEnvironment oEnvironment)
        {

            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            string cFileLogFirmato = "";
            string cFileRiepilogoFirmato = "";
            string cFileEmailFirmato = "";
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            //string PathXslt = HttpContext.Current.Server.MapPath("RisorseWeb");
            string pathRisorse = oEnvironment.PathRisorseWeb;
            Int64 Progressivo = 0;

            Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
            percorsi.Risorse = oEnvironment.PathRisorseWeb;
            percorsi.Temp = oEnvironment.PathTemp;
            percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.clsRiepilogoGeneratoWebApi dettaglioRiepilogo = null;
            DateTime dSysdate = DateTime.MinValue;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);

                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CreaRiepilogo", Token, new Dictionary<string, object>() { { "Giorno", Giorno }, { "GiornalieroMensile", GiornalieroMensile } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_CREAZIONE", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_CREAZIONE", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);



                        if (oError == null && GiornalieroMensile == "G")
                        {
                            bool lGeneraRPGGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RPG_FUTURO", "DISABILITATO") == "ABILITATO";
                            if (!lGeneraRPGGiorniFuturi)
                            {
                                try
                                {
                                    DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
                                    if (Giorno.Date > dSysdateCodiceSistema.Date)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo giornaliero", string.Format("Impossibile Generare il Riepilogo Giornaliero di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }

                        if (oError == null && GiornalieroMensile == "R")
                        {
                            bool lGeneraRCAGiorniFuturi = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnectionInternal, "RCA_FUTURO", "DISABILITATO") == "ABILITATO";

                            if (!lGeneraRCAGiorniFuturi)
                            {
                                try
                                {
                                    DateTime dSysdateCodiceSistema = Riepiloghi.clsRiepiloghi.GetSysdate(oConnection);
                                    if (Giorno.Date > dSysdateCodiceSistema.Date)
                                    {
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Generazione riepilogo controllo accessi", string.Format("Impossibile Generare il Riepilogo Controllo Accessi di {0} in data {1}", Giorno.ToLongDateString(), dSysdateCodiceSistema.ToLongDateString()));
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oError = new Exception("Operazione disponibile solo per il titolare del sistema");
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            Riepiloghi.clsWinServiceClientOperation oClientOperation = new Riepiloghi.clsWinServiceClientOperation(Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);
                            long IdOperazione = 0;

                            Riepiloghi.clsWinServiceConfig config = Riepiloghi.clsWinServiceConfig.GetWinServiceConfig(oConnection, out oError, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);

                            lRet = oError == null;

                            if (lRet)
                            {
                                Riepiloghi.clsRiepilogoGenerato riepilogoGenerato = null;
                                lRet = oClientOperation.PushOperazione(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.AccountId, percorsi, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, GiornalieroMensile, Giorno, 0, out IdOperazione, out riepilogoGenerato, out oError);

                                if (lRet && oError == null)
                                {
                                    dettaglioRiepilogo = new Riepiloghi.clsRiepilogoGeneratoWebApi(riepilogoGenerato);

                                    // TOLGO IL PERCORSO DAL NOME DEL FILE DI RIEPILOGO
                                    try
                                    {
                                        if (dettaglioRiepilogo.Nome != null && !string.IsNullOrEmpty(dettaglioRiepilogo.Nome) && !string.IsNullOrWhiteSpace(dettaglioRiepilogo.Nome))
                                        {
                                            System.IO.FileInfo oNomeInfo = new System.IO.FileInfo(dettaglioRiepilogo.Nome);
                                            dettaglioRiepilogo.Nome = oNomeInfo.Name;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    string cDesc = string.Format("Generazione Riepilogo {0} {1}", (GiornalieroMensile == "G" ? "Giornaliero" : (GiornalieroMensile == "M" ? "Mensile" : "Controllo accessi")), (config.ComunicationMode == Riepiloghi.clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_DIRECT) ? "terminata" : "avviata");
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, cDesc, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);

                                    Exception exLista = null;
                                    List<Riepiloghi.clsRiepilogoGenerato> lista = Riepiloghi.clsRiepilogoGenerato.GetRigaRiepiloghiProgs(oConnection, out exLista, "COMPETENZA", GiornalieroMensile, Giorno, Giorno, 0);
                                    if (exLista == null && lista != null)
                                    {
                                        oResult.ListaRiepiloghi = Riepiloghi.clsRiepilogoGeneratoWebApi.GetList(lista);
                                    }

                                    exLista = null;
                                    oResult.EmailToSendOrReceive = Riepiloghi.clsRiepiloghi.CheckRiepiloghiEmailPending(oConnection, true, true, out exLista);
                                    oResult.Sysdate = dSysdate;
                                }
                                else
                                {
                                    lRet = false;
                                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                                    oResult.Sysdate = dSysdate;
                                }
                            }
                            else
                            {
                                lRet = false;
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            lRet = false;
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        lRet = false;
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    lRet = false;
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                lRet = false;
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore creazione riepilogo", "Errore imprevisto durante la creazione.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dettaglioRiepilogo);
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }


        public static ResultBaseGenerazioneRiepilogo ListaRiepiloghi(string Token, string Mode, DateTime? inizio, DateTime? fine, string tipo, long? progressivo, clsEnvironment oEnvironment)
        {

            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            string pathRisorse = oEnvironment.PathRisorseWeb;
            DateTime dSysdate = DateTime.MinValue;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "ListaRiepiloghi", Token, new Dictionary<string, object>() { { "Inizio", (inizio == null ? "" : ((DateTime)inizio).ToShortDateString()) }, { "Fine", (fine == null ? "" : ((DateTime)fine).ToShortDateString()) }, { "tipo", tipo }, { "progressivo", (progressivo == null ? 0 : progressivo) } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_CREAZIONE", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_CREAZIONE", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oError = new Exception("Operazione disponibile solo per il titolare del sistema");
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                            List<Riepiloghi.clsRiepilogoGenerato> lista = Riepiloghi.clsRiepilogoGenerato.GetRigaRiepiloghiProgs(oConnection, out oError, Mode, tipo, inizio, fine, progressivo);
                            if (oError == null)
                            {
                                lRet = true;
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                if (lista != null)
                                    oResult.ListaRiepiloghi = Riepiloghi.clsRiepilogoGeneratoWebApi.GetList(lista);
                                oResult.EmailToSendOrReceive = Riepiloghi.clsRiepiloghi.CheckRiepiloghiEmailPending(oConnection, true, true, out oError);
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore lettura lista riepiloghi", "Errore imprevisto.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        public static ResultBaseGenerazioneRiepilogo SpostaRiepilogoSpeditoInDaSpedire(string Token, clsEnvironment oEnvironment, Riepiloghi.clsRiepilogoGenerato oRiepilogo, bool invioContestuale)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            string pathRisorse = oEnvironment.PathRisorseWeb;
            DateTime dSysdate = DateTime.MinValue;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Reflection.Assembly oAssembly = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "ListaRiepiloghi", Token, new Dictionary<string, object>());

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_SPEDIZIONE_MAIL", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_SPEDIZIONE_MAIL", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oError = new Exception("Operazione disponibile solo per il titolare del sistema");
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            List<string> operations = new List<string>();
                            libRiepiloghiBase.iLib_SIAE_Provider oProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError, out operations, out oAssembly);
                            lRet = Riepiloghi.clsRiepiloghi.SpostaRiepilogoSpeditoInDaSpedire(oConnection, oRiepilogo, oProvider, out oError, invioContestuale);

                            if (lRet && oError == null)
                            {
                                lRet = true;
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Spostamento riepilogo da spedire", "Errore imprevisto.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        public static ResultBaseGenerazioneRiepilogo SendReceiveRiepiloghi(string Token, clsEnvironment oEnvironment)
        {

            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBaseGenerazioneRiepilogo oResult = new ResultBaseGenerazioneRiepilogo();
            string pathRisorse = oEnvironment.PathRisorseWeb;
            DateTime dSysdate = DateTime.MinValue;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            System.Reflection.Assembly oAssembly = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "ListaRiepiloghi", Token, new Dictionary<string, object>());

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_SPEDIZIONE_MAIL", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_SPEDIZIONE_MAIL", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oError = new Exception("Operazione disponibile solo per il titolare del sistema");
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            List<string> operations = new List<string>();
                            libRiepiloghiBase.iLib_SIAE_Provider oProvider = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oError, out operations, out oAssembly);
                            List<Riepiloghi.clsRiepilogoGenerato> lista = null;
                            if (oError == null)
                                lista = Riepiloghi.clsRiepiloghi.SendReceiveRiepiloghiEmailPending(oConnection, oProvider, true, true, out oError);

                            if (oError == null)
                            {
                                lRet = true;
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                oResult.ListaRiepiloghi = Riepiloghi.clsRiepilogoGeneratoWebApi.GetList(lista);
                                oResult.EmailToSendOrReceive = Riepiloghi.clsRiepiloghi.CheckRiepiloghiEmailPending(oConnection, true, true, out oError);
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore spedizione/ricezione lista riepiloghi", "Errore imprevisto.", ex);
                oResult = new ResultBaseGenerazioneRiepilogo(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
                if (oAssembly != null)
                {
                    // dovrei scaricarlo
                }
            }
            return oResult;
        }

        //SendReceiveRiepiloghiEmailPending


        //public class ResultBaseSingoloRiepilogoGenerato : ResultBase
        //{
        //    public clsRiepilogoGenerato RiepilogoGenerato = null;

        //    public ResultBaseSingoloRiepilogoGenerato()
        //    {
        //        this.RiepilogoGenerato = new clsRiepilogoGenerato();
        //        this.RiepilogoGenerato.BytesLogNonFirmato = new byte[] { };
        //        this.RiepilogoGenerato.BytesRiepilogoNonFirmato = new byte[] { };
        //        this.RiepilogoGenerato.DataOraGenerazione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DataOraMasterizzazione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DataOraSpedizione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DescrizioneStatoSpedizione = "";
        //        this.RiepilogoGenerato.FileEmail = "";
        //        this.RiepilogoGenerato.FileLogFirmato = "";
        //        this.RiepilogoGenerato.FileLogNonFirmato = "";
        //        this.RiepilogoGenerato.FileRiepilogo = "";
        //        this.RiepilogoGenerato.FileRiepilogoNonFirmato = "";
        //        this.RiepilogoGenerato.GiornalieroMensile = "";
        //        this.RiepilogoGenerato.GiornoMese = DateTime.MinValue;
        //        this.RiepilogoGenerato.Progressivo = 0;
        //        this.RiepilogoGenerato.StatoMasterizzazione = 0;
        //        this.RiepilogoGenerato.StatoSpedizione = 0;
        //    }

        //    public ResultBaseSingoloRiepilogoGenerato(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, IRecordSet oRS)
        //        : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
        //    {
        //        this.RiepilogoGenerato = new clsRiepilogoGenerato();
        //        this.RiepilogoGenerato.BytesLogNonFirmato = new byte[] { };
        //        this.RiepilogoGenerato.BytesRiepilogoNonFirmato = new byte[] { };
        //        this.RiepilogoGenerato.DataOraGenerazione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DataOraMasterizzazione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DataOraSpedizione = DateTime.MinValue;
        //        this.RiepilogoGenerato.DescrizioneStatoSpedizione = "";
        //        this.RiepilogoGenerato.FileEmail = "";
        //        this.RiepilogoGenerato.FileLogFirmato = "";
        //        this.RiepilogoGenerato.FileLogNonFirmato = "";
        //        this.RiepilogoGenerato.FileRiepilogo = "";
        //        this.RiepilogoGenerato.FileRiepilogoNonFirmato = "";
        //        this.RiepilogoGenerato.GiornalieroMensile = "";
        //        this.RiepilogoGenerato.GiornoMese = DateTime.MinValue;
        //        this.RiepilogoGenerato.Progressivo = 0;
        //        this.RiepilogoGenerato.StatoMasterizzazione = 0;
        //        this.RiepilogoGenerato.StatoSpedizione = 0;
        //    }
        //}

        //public static ResultBaseSingoloRiepilogoGenerato GetRiepilogoGenerato(string Token, DateTime Giorno, string GiornalieroMensile, clsEnvironment oEnvironment)
        //{
        //    DateTime dStart = DateTime.Now;
        //    Exception oError = null;
        //    ResultBaseSingoloRiepilogoGenerato oRet = new ResultBaseSingoloRiepilogoGenerato();
        //    IConnection oConnectionInternal = null;
        //    IConnection oConnection = null;
        //    oRet.RiepilogoGenerato.GiornalieroMensile = GiornalieroMensile;
        //    oRet.RiepilogoGenerato.GiornoMese = Giorno;
        //    oRet.RiepilogoGenerato.Progressivo = 0;

        //    try
        //    {
        //        oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
        //        if (oError == null)
        //        {
        //            if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "LISTA_RIEPILOGHI", Token, out oError))
        //            {
        //                oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
        //                if (oError == null)
        //                {
        //                    long nProgRiepilogo = Riepiloghi.clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, oConnection, out oError);
        //                    if (oError == null)
        //                    {
        //                        oRet.RiepilogoGenerato.Progressivo = nProgRiepilogo;
        //                        StringBuilder oSB = new StringBuilder();
        //                        oSB.Append("SELECT * FROM RIEPILOGHI WHERE GIORNO = :pGIORNO AND TIPO = :pGIORNALIERO_MENSILE AND PROG = :pPROG_RIEPILOGO");
        //                        clsParameters oPars = new clsParameters();
        //                        oPars.Add(":pGIORNO", Giorno.Date);
        //                        oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
        //                        oPars.Add(":pPROG_RIEPILOGO", nProgRiepilogo);

        //                        IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString(), oPars);
        //                        if (!oRS.EOF)
        //                        {
        //                            oRet.RiepilogoGenerato = new clsRiepilogoGenerato(oRS.Fields("TIPO").Value.ToString(),
        //                                                                             (oRS.Fields("GIORNO").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("GIORNO").Value),
        //                                                                             "",
        //                                                                             oRS.Fields("NOME").Value.ToString(),
        //                                                                             "",
        //                                                                             Int64.Parse(oRS.Fields("PROG").Value.ToString()),
        //                                                                             (oRS.Fields("DATA_GENERAZIONE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("DATA_GENERAZIONE").Value),
        //                                                                             (oRS.Fields("MAIL_DATE").IsNull ? DateTime.MinValue : (DateTime)oRS.Fields("MAIL_DATE").Value),
        //                                                                             DateTime.MinValue);
        //                        }
        //                        oRS.Close();
        //                    }
        //                    else
        //                    {
        //                        oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //                    }
        //                }
        //                else
        //                {
        //                    oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //                }
        //            }
        //            else
        //            {
        //                oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //            }
        //        }
        //        else
        //        {
        //            oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oError = Riepiloghi.clsRiepiloghi.GetNewException("lISTA rIEPILOGHI", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
        //        oRet = new ResultBaseSingoloRiepilogoGenerato(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //    }
        //    finally
        //    {
        //        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
        //        if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
        //    }

        //    return oRet;
        //}


        //public static ResultBaseRiepiloghiGenerati GetRiepiloghiGenerati(string Token, clsEnvironment oEnvironment)
        //{
        //    DateTime dStart = DateTime.Now;
        //    Exception oError = null;
        //    ResultBaseRiepiloghiGenerati oRet = new ResultBaseRiepiloghiGenerati();
        //    IConnection oConnectionInternal = null;
        //    IConnection oConnection = null;

        //    try
        //    {
        //        oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
        //        if (oError == null)
        //        {
        //            if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "LISTA_RIEPILOGHI", Token, out oError))
        //            {
        //                oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
        //                if (oError == null)
        //                {
        //                    IRecordSet oRS = (IRecordSet)oConnection.ExecuteQuery("SELECT * FROM RIEPILOGHI ORDER BY DATA_INS DESC");
        //                    oRet = new ResultBaseRiepiloghiGenerati(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), oRS);
        //                }
        //                else
        //                {
        //                    oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //                }
        //            }
        //            else
        //            {
        //                oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //            }
        //        }
        //        else
        //        {
        //            oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oError = Riepiloghi.clsRiepiloghi.GetNewException("Lista Riepiloghi", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
        //        oRet = new ResultBaseRiepiloghiGenerati(false, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
        //    }
        //    finally
        //    {
        //        if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
        //        if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
        //    }

        //    return oRet;
        //}

        public class ResultBaseOperazioneInCorso : ResultBase
        {
            public Riepiloghi.clsSequenzaCreazioneRiepilogo SequenzaOperazioneInCorso = new Riepiloghi.clsSequenzaCreazioneRiepilogo();

            public ResultBaseOperazioneInCorso()
            {
            }

            public ResultBaseOperazioneInCorso(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
            }
        }

        public static ResultBaseOperazioneInCorso GetOperazioneInCorso(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseOperazioneInCorso oRet = new ResultBaseOperazioneInCorso();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CHECK_OPERAZIONE_IN_CORSO", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oRet.SequenzaOperazioneInCorso = Riepiloghi.clsRiepiloghi.SequenzaCreazioneRiepilogo(oConnection, null, 0, 0, true, false);
                        }
                        else
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        }
                    }
                    else
                    {
                        oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    }
                }
                else
                {
                    oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Operazione In Corso", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }


        public static ResultBaseOperazioneInCorso GetIdOperazioneInCorso(string Token, Int64 IdOperazione, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            Exception oError = null;
            ResultBaseOperazioneInCorso oRet = new ResultBaseOperazioneInCorso();
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CHECK_OPERAZIONE_IN_CORSO", Token, out oError))
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        if (oError == null)
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oRet.SequenzaOperazioneInCorso = Riepiloghi.clsRiepiloghi.SequenzaCreazioneRiepilogo(oConnection, null, IdOperazione, 0, true, true);
                        }
                        else
                        {
                            oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        }
                    }
                    else
                    {
                        oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    }
                }
                else
                {
                    oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Operazione In Corso", "Errore imprevisto nella lettura della lista dei Riepiloghi.", ex);
                oRet = new ResultBaseOperazioneInCorso(true, oError.Message, 1, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }

            return oRet;
        }



        #endregion

        #region "richieste esterne per tornelli"


        public class clsTornelloLogin
        {
            public bool enabled { get; set; }
            public string token { get; set; }
            public string idterm { get; set; }
            public string terminale { get; set; }
            public string pin { get; set; }
            public int idoperatore { get; set; }
            public string Nome { get; set; }
            public string Cognome { get; set; }
            public string tipo_tornello { get; set; }
        }

        public class clsTornelloRequest
        {
            public string SERVICE_NAME { get; set; }
            public clsTornelloLogin login { get; set; }
            public string barcode { get; set; }
            public string token { get; set; }
        }

        public class clsTornelloResponse
        {
            public string SERVICE_NAME { get; set; }
            public bool esito { get; set; }
            public string errorMessage { get; set; }
            public int errorCode { get; set; }
            public string executionTime { get; set; }
            public clsTornelloLogin login { get; set; }
            public clsTornelloLta ticket { get; set; }
        }

        public class clsTornelloLta
        {
            public DateTime DataOraEmissioneAnnullamentoTitolo { get; set; }
            public String CFOrganizzatore { get; set; }
            public String OrdineDiPosto { get; set; }
            public String IdPosto { get; set; }
            public String TipoTitolo { get; set; }
            public String TipologiaBiglietto { get; set; }
            public String DataEmissione { get; set; }
            public String OraEmissione { get; set; }
            public int NumProgTitolo { get; set; }
            public String Sigillo { get; set; }
            public String CodiceSistema { get; set; }
            public String CodiceCarta { get; set; }
            public String CodiceLocale { get; set; }
            public String DataEvento { get; set; }
            public String TipoEvento { get; set; }
            public String TitoloEvento { get; set; }
            public String OraEvento { get; set; }
            public int NumeroEventiAbilitati { get; set; }
            public String CodiceAbbonamento { get; set; }
            public int NumeroProgressivoAbbonamento { get; set; }
            public String CodiceFiscaleOrganizzatore { get; set; }
            public int CorrispettivoLordo { get; set; }
            public String DigitaleTradizionale { get; set; }
            public String CFTitolareCA { get; set; }
            public String CodiceSistemaCA { get; set; }
            public DateTime DataOraAperturaAccessi { get; set; }
            public String DataAperturaAccessi { get; set; }
            public String OraAperturaAccessi { get; set; }
            public String DataAnnullamento { get; set; }
            public String OraAnnullamento { get; set; }
            public String CodiceCartaAnnullamento { get; set; }
            public int ProgressivoTitoloAnnullamento { get; set; }
            public String SigilloAnnullamento { get; set; }
            public String DataIngressoControlloAccessi { get; set; }
            public String OraIngressoControlloAccessi { get; set; }
            public String DataInserimentoLTA { get; set; }
            public String OraInserimentoLTA { get; set; }
            public String DescrizioneStatoDelTitolo { get; set; }
            public String StatoDelTitolo { get; set; }
            public Int32 TF { get; set; }
            public String Sala { get; set; }
            public clsTornelloDatiSupportoIdentificazione datiSupportoIdentificazione { get; set; }
            public clsTornelloDatiIdentificativiPartecipante datiIdentificativiPartecipante { get; set; }

            public bool esitoRichiestaIngresso { get; set; }
            public string messaggioRichiestaIngresso { get; set; }
        }

        public class clsTornelloDatiIdentificativiPartecipante 
        {
            public string Nome { get; set; }
            public string Cognome { get; set; }
            public string DataNascita { get; set; }
            public string LuogoNascita { get; set; }
        }


        public class clsTornelloDatiSupportoIdentificazione
        {
            public string DescrizioneCodiceSupportoIdentificativo { get; set; } 

            public string CodiceSupportoIdentificativo { get; set; }
            public string DescrizioneSupportoIdentificativo { get; set; }
            public string IdentificativoSupporto { get; set; }

            public string IdentificativoSupportoAlternativo { get; set; }
        }

        public class ResultBaseTornello : ResultBase
        {
            private clsTornelloResponse Response { get; set; }
            public clsResultLta Tornello_Lta { get; set; }

            public clsResultLta[] Lta { get; set; }

            public clsTornelloLogin Tornello_Login { get; set; }
            public string Tornello_Barcode { get; set; }


            public ResultBaseTornello()
            {
            }

            public ResultBaseTornello(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, clsTornelloResponse response)
                : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Response = response;
                if (this.Response != null)
                {
                    this.StatusOK = this.Response.esito;
                    this.StatusCode = this.Response.errorCode;
                    this.StatusMessage = this.Response.errorMessage;
                    this.execution_time = this.Response.executionTime;
                    if (this.Response.login != null)
                    {
                        this.Tornello_Login = new clsTornelloLogin();
                        this.Tornello_Login.token = this.Response.login.token;
                        this.Tornello_Login.pin = this.Response.login.pin;
                        this.Tornello_Login.idoperatore = this.Response.login.idoperatore;
                        this.Tornello_Login.Cognome = this.Response.login.Cognome;
                        this.Tornello_Login.Nome = this.Response.login.Nome;
                        this.Tornello_Login.idterm = this.Response.login.idterm;
                        this.Tornello_Login.terminale = this.Response.login.terminale;
                        this.Tornello_Login.tipo_tornello = this.Response.login.tipo_tornello;

                    }
                    if (this.Response.ticket != null)
                    {
                        try
                        {
                            if (!response.ticket.esitoRichiestaIngresso)
                            {
                                this.StatusOK = false;
                                this.StatusMessage = response.ticket.messaggioRichiestaIngresso;
                            }
                        }
                        catch (Exception)
                        {
                        }
                        this.Tornello_Lta = new clsResultLta(this.Response.ticket);
                        this.Tornello_Barcode = this.Response.ticket.Sigillo;
                    }
                }
            }

            
        }

        public static ResultBaseTornello LoginTornelloLta(string Token, string urlTornello, string tornello_pcName_debug, clsTornelloLogin tornelloLogin, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBaseTornello oResult = new ResultBaseTornello();
            string pathRisorse = oEnvironment.PathRisorseWeb;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "LoginTornelloLta", Token, new Dictionary<string, object>() { { "tornelloLogin", (tornelloLogin == null ? "" : tornelloLogin.ToString()) } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TORNELLO", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_TORNELLO", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);

                            clsTornelloResponse response = null;
                            string token = "";
                            string value = "";

                            if (tornelloLogin != null)
                            {
                                clsTornelloRequest TornelloRequest = new clsTornelloRequest();
                                TornelloRequest.SERVICE_NAME = "CHECK_LOGIN";
                                TornelloRequest.login = new clsTornelloLogin();
                                TornelloRequest.login.pin = tornelloLogin.pin;
                                string pcName = System.Environment.MachineName.Trim().ToUpper();
                                if (System.Diagnostics.Debugger.IsAttached && !string.IsNullOrEmpty(tornello_pcName_debug) && !string.IsNullOrWhiteSpace(tornello_pcName_debug))
                                {
                                    pcName = tornello_pcName_debug;
                                }
                                TornelloRequest.login.terminale = String.Format("{0}.TORNELLO_RIEPILOGO", pcName);
                                value = GetWebRequestTornello(urlTornello, TornelloRequest, out oError);
                            }
                            else
                            {
                                oError = new Exception("Login tornello non valorizzato");
                            }

                            // richiesta di accesso

                            if (oError == null)
                            {
                                try
                                {
                                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<clsTornelloResponse>(value);
                                    token = response.login.token;
                                    if (!response.esito)
                                    {
                                        oError = new Exception(response.errorMessage);
                                        oResult = new ResultBaseTornello(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), response);
                                    }
                                    else
                                    {
                                        oResult = new ResultBaseTornello(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), response);
                                    }
                                }
                                catch (Exception exLogin)
                                {
                                    oError = new Exception("Errore durante il login del tornello " + exLogin.Message);
                                    oResult = new ResultBaseTornello(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                }
                            }
                            else
                            {
                                oResult = new ResultBaseTornello(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            }


                        }
                        else
                        {
                            oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    }
                }
                else
                {
                    oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore Login tornello", "Errore imprevisto.", ex);
                oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        public static ResultBaseTornello CheckVoidTornelloLta(string Token, string urlTornello, bool lCheckVoidTornelloLta, clsTornelloLogin tornelloLogin, string barCode, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBaseTornello oResult = new ResultBaseTornello();
            string pathRisorse = oEnvironment.PathRisorseWeb;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "CheckVoidTornelloLta", Token, new Dictionary<string, object>() { { "lCheckVoidTornelloLta", lCheckVoidTornelloLta }, { "tornelloLogin", (tornelloLogin == null ? "" : tornelloLogin.ToString()) }, { "barCode", barCode } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TORNELLO", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_TORNELLO", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            string value = "";
                            clsTornelloResponse response = null;

                            if (tornelloLogin != null && !string.IsNullOrEmpty(barCode) && !string.IsNullOrWhiteSpace(barCode))
                            {
                                clsTornelloRequest TornelloRequest = new clsTornelloRequest();
                                TornelloRequest.SERVICE_NAME = (lCheckVoidTornelloLta ? "VOID_BARCODE" : "CHECK_BARCODE");
                                TornelloRequest.token = tornelloLogin.token;
                                TornelloRequest.login = new clsTornelloLogin();
                                TornelloRequest.login.idoperatore = tornelloLogin.idoperatore;
                                TornelloRequest.login.Cognome = tornelloLogin.Cognome;
                                TornelloRequest.login.Nome = tornelloLogin.Nome;
                                TornelloRequest.login.idterm = tornelloLogin.idterm;
                                TornelloRequest.login.terminale = tornelloLogin.terminale;
                                TornelloRequest.login.tipo_tornello = tornelloLogin.tipo_tornello;
                                TornelloRequest.barcode = barCode;
                                value = GetWebRequestTornello(urlTornello, TornelloRequest, out oError);
                            }
                            else
                            {
                                if (tornelloLogin == null)
                                    oError = new Exception("Login tornello non valorizzato");
                                else
                                    oError = new Exception("Codice biglietto non valorizzato");
                            }


                            if (oError == null)
                            {
                                try
                                {
                                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<clsTornelloResponse>(value);
                                    oResult = new ResultBaseTornello(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), response);
                                }
                                catch (Exception exCheck)
                                {
                                    oError = new Exception("Errore durante il check sul tornello " + exCheck.Message);
                                    oResult = new ResultBaseTornello(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                                }
                            }
                            else
                            {
                                oResult = new ResultBaseTornello(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                            }

                        }
                        else
                        {
                            oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                        }
                    }
                    else
                    {
                        oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                    }
                }
                else
                {
                    oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore Check tornello", "Errore imprevisto.", ex);
                oResult = new ResultBaseTornello(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), null);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        private static string GetWebRequestTornello(string parUrl, object parRequest, out Exception Error)
        {
            Error = null;
            string result = string.Empty;

            try
            {
                HttpWebResponse resp = null;

                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // comparable to modern browsers


                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(parUrl);
                req.CookieContainer = new CookieContainer();
                req.Credentials = CredentialCache.DefaultNetworkCredentials;
                req.UserAgent = ": Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
                req.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Anonymous;
                req.Method = "POST";
                req.Accept = "application/json";
                req.ContentType = "application/json";
                req.Timeout = 20000;

                string json = JsonConvert.SerializeObject(parRequest);

                using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    resp = (HttpWebResponse)req.GetResponse();
                    Encoding enc = Encoding.ASCII;
                    using (System.IO.StreamReader responseStream = new System.IO.StreamReader(resp.GetResponseStream(), enc))
                    {
                        result = responseStream.ReadToEnd();
                        responseStream.Close();
                    }
                    resp.Close();
                }

            }
            catch (Exception ex)
            {
                Error = ex;
            }

            return result;
        }

        #endregion

        #endregion

        #region "Bordero"

        public class ResultItemInfoBordero
        {
            public DateTime GiornoCompetenza { get; set; }
            public List<string> Descrizioni { get; set; }
            public string StringId { get; set; }
            public ResultItemInfoBordero()
            {
                this.GiornoCompetenza = DateTime.MinValue;
                this.Descrizioni = new List<string>();
                this.StringId = "";
            }

            public ResultItemInfoBordero(DateTime giornoCompetenza, Dictionary<string, string> infoFiltri, string stringId)
            {
                this.GiornoCompetenza = giornoCompetenza;
                this.Descrizioni = new List<string>();
                foreach (KeyValuePair<string, string> item in infoFiltri)
                {
                    this.Descrizioni.Add(item.Value);
                }
                this.StringId = stringId;
            }
        }



        public class ResultBaseBorderoDati : ResultBase
        {
            public DateTime DataOraInizio;
            public DateTime DataOraFine;
            public List<ResultItemInfoBordero> ListaBordero { get; set; }
            public DataSet DataSetBordero { get; set; }
            public ResultJsonDataSet DataSetBorderoJson { get; set; }
            public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }

            public long IdRichiestaBordero { get; set; }

            public ResultBaseBorderoDati() : base()
            {
            }

            public ResultBaseBorderoDati(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, DateTime dDataOraInizio, DateTime dDataOraFine) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.DataOraInizio = dDataOraInizio;
                this.DataOraFine = dDataOraFine;
                this.ListaBordero = new List<ResultItemInfoBordero>();
            }
        }

        public static ResultBaseBorderoDati CalcBordero(string Token, DateTime dDataOraInizio, DateTime dDataOraFine, List<long> QuoteManualiEscluse, List<long> QuoteManualiTe, List<long> QuoteManualiSi, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoDati oResult = null;
            List<ResultItemInfoBordero> lista = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DataSet dataSetBordero = null;
            clsBordQuoteConfig config = null;
            Int64 IdRichiestaBordero = 0;

            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                //string fontPath = HttpContext.Current.Server.MapPath("//RisorseWeb//Font");
                string fontPath = oEnvironment.PathFont;
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CALC_BORDERO", Token, out oError))
                    //{
                    //}
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                        {
                            oTokenCheck = new Riepiloghi.clsToken(Token);
                            idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                            if (idCinema > 0)
                            {
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }
                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            // prendo nIdUxc
                            if (idCinema > 0)
                                nIdUxc = idCinema;
                            else
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                            if (oError == null && nIdUxc > 0)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {
                                    Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                    List<string> codiciLocaliCorrenti = new List<string>();
                                    List<string> cfOrganizzatoriCorrenti = new List<string>();
                                    foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                    {
                                        if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                        {
                                            oCodiceSistemaCorrente = oItemCodiceSistema;
                                            if (idCinema > 0)
                                            {
                                                oCodiceSistemaCorrente.AllCodiciLocali = false;
                                                oCodiceSistemaCorrente.AllCFOrganizzatori = false;
                                            }
                                            foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oCodiceLocale.Enabled && CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == oCodiceLocale.CodiceLocale) != null && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                {
                                                    codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                }
                                            }

                                            foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oOrganizzatore.Enabled && OrganizzatoriAbilitati != null && OrganizzatoriAbilitati.FirstOrDefault(x => x.CFOrganizzatore == oOrganizzatore.CFOrganizzatore) != null && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                {
                                                    cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                    {

                                    }
                                }
                            }
                        }

                        if (oError == null)
                        {
                            libBordero.Bordero.DeleteDatiRichiestaBorderoFull(oConnection, false, nIdUxc);
                            oConnection.BeginTransaction();
                            try
                            {
                                List<string> tabelleDaCancellareTemporaneamente = new List<string>() { "BORD_QUOTE_TE", "BORD_QUOTE_SPETINTRA", "BORD_QUOTE_DEM", "BORD_QUOTE_ALTRO", "BORD_QUOTE" };
                                if (QuoteManualiEscluse != null)
                                {
                                    QuoteManualiEscluse.ForEach(idQuota =>
                                    {
                                        // (esiste sempre RollBackFinale)
                                        // ELIMINO LA QUOTA IN MODO CHE NON POSSA ESSERE CALCOLATA
                                        foreach (string tabellaQuote in tabelleDaCancellareTemporaneamente)
                                        {
                                            oConnection.ExecuteNonQuery(string.Format("DELETE FROM {0} WHERE IDQUOTA = :pIDQUOTA", tabellaQuote), new clsParameters(":pIDQUOTA", idQuota));
                                        }
                                    });
                                }

                                // (esiste sempre RollBackFinale)
                                // IMPOSTO LA QUOTA PER QUALSIASI TIPO EVENTO IN MODO CHE SIA IL PIU' APPLICABILE POSSIBILE
                                // VERRA' CALCOLATA SE LE REGOLE DELLE RIGHE SONO COMPATIBILI
                                if (QuoteManualiTe != null)
                                {
                                    QuoteManualiTe.ForEach(idQuota =>
                                    {
                                        oConnection.ExecuteNonQuery("DELETE FROM BORD_QUOTE_TE WHERE IDQUOTA = :pIDQUOTA", new clsParameters(":pIDQUOTA", idQuota));
                                    });
                                }

                                if (QuoteManualiSi != null)
                                {
                                    QuoteManualiSi.ForEach(idQuota =>
                                    {
                                        oConnection.ExecuteNonQuery("UPDATE BORD_QUOTE_SPETINTRA SET SPETTACOLO_INTRATTENIMENTO = 'X' WHERE IDQUOTA = :pIDQUOTA", new clsParameters(":pIDQUOTA", idQuota));
                                    });
                                }

                                dataSetBordero = libBordero.Bordero.GetBordero(oConnection, nIdUxc, dDataOraInizio, dDataOraFine, ref IdRichiestaBordero, out oError);
                                config = libBordero.Bordero.GetConfigurazione(oConnection, nIdUxc);
                            }
                            catch (Exception)
                            {
                            }
                            oConnection.RollBack();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoDati(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
                oResult.ListaBordero = lista;
                oResult.ConfigurazioneBordero = config;
                oResult.DataSetBordero = dataSetBordero;
                oResult.DataSetBorderoJson = new ResultJsonDataSet(dataSetBordero);
                oResult.IdRichiestaBordero = IdRichiestaBordero;
            }
            else
            {
                oResult = new ResultBaseBorderoDati(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
            }

            return oResult;

        }

        public static ResultBaseBorderoDati CalcBordero(string Token, DateTime dDataOraInizio, DateTime dDataOraFine, bool lFlagGrpDataEve, bool lFlagGrpOrg, bool lFlagGrpNol, bool lFlagGrpPro, bool lFlagGrpTev, bool lFlagGrpSal, bool lFlagGrpTit, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoDati oResult = null;
            List<ResultItemInfoBordero> lista = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            try
            {
                //string fontPath = HttpContext.Current.Server.MapPath("//RisorseWeb//Font");
                string fontPath = oEnvironment.PathFont;
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CALC_BORDERO", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                            Int64 nIdUxc = 0;
                            if (oError == null)
                            {
                                // prendo nIdUxc
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                                if (oError == null && nIdUxc > 0)
                                {
                                    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                    if (oError == null)
                                    {
                                        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                        List<string> codiciLocaliCorrenti = new List<string>();
                                        List<string> cfOrganizzatoriCorrenti = new List<string>();
                                        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                        {
                                            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                            {
                                                oCodiceSistemaCorrente = oItemCodiceSistema;
                                                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }

                                                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                                {
                                                    if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                        {

                                        }
                                    }
                                }
                            }

                            Int64 IdRichiestaBordero = 0;
                            List<libBordero.Bordero.clsInfoDataSet> listaDati = null;
                            if (oError == null)
                            {
                                libBordero.Bordero.DeleteDatiRichiestaBorderoFull(oConnection, false, nIdUxc);
                                oConnection.BeginTransaction();
                                try
                                {
                                    libBordero.Bordero.clsBordQuoteConfig config = libBordero.Bordero.GetConfigurazione(oConnection, nIdUxc);
                                    System.Data.DataSet oDataSetBordero = libBordero.Bordero.GetBordero(oConnection, nIdUxc, dDataOraInizio, dDataOraFine, ref IdRichiestaBordero, out oError);
                                    if (oError == null)
                                    {
                                        List<libBordero.Bordero.clsFilterBordero> oFiltri = new List<libBordero.Bordero.clsFilterBordero>();
                                        libBordero.Bordero.FilterDatiBordero(ref oDataSetBordero, oFiltri);

                                        string cTipoBorderoMultiplo = "";  // suddivisione minima per data evento

                                        cTipoBorderoMultiplo += (lFlagGrpOrg ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_ORGANIZZATORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpNol ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_NOLEGGIATORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpPro ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_PRODUTTORI" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpTev ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_TIPI_EVENTO" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpSal ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_SALE" : "");
                                        cTipoBorderoMultiplo += (lFlagGrpTit ? (cTipoBorderoMultiplo == "" ? "" : ",") + "BORDERO_FILT_TITOLI" : "");

                                        if (cTipoBorderoMultiplo.Trim() == "" && lFlagGrpDataEve)
                                        {
                                            cTipoBorderoMultiplo = "BORDERO_FILT_GIORNI"; // minima suddivisione per data evento
                                        }
                                        else if (cTipoBorderoMultiplo.Trim() != "" && lFlagGrpDataEve)
                                        {
                                            cTipoBorderoMultiplo += ",BORDERO_FILT_GIORNI";
                                        }

                                        listaDati = libBordero.Bordero.MultiBorderoStream(cTipoBorderoMultiplo, dDataOraInizio, dDataOraFine, oDataSetBordero, fontPath, config);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
                                }

                                oConnection.RollBack();
                                if (oError == null && IdRichiestaBordero > 0 && listaDati != null && listaDati.Count > 0)
                                {
                                    List<libBordero.Bordero.clsInfoDataSet> oListaFinale = libBordero.Bordero.SaveTokenPdfGenerated(oConnection, nIdUxc, dDataOraInizio, dDataOraFine, IdRichiestaBordero, listaDati, out oError);
                                    if (oListaFinale != null && oListaFinale.Count > 0)
                                    {
                                        lista = new List<ResultItemInfoBordero>();
                                        foreach (libBordero.Bordero.clsInfoDataSet item in oListaFinale)
                                        {
                                            lista.Add(new ResultItemInfoBordero(item.GiornoCompetenza, item.InfoFiltri, item.StringId));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoDati(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
                oResult.ListaBordero = lista;
            }
            else
            {
                oResult = new ResultBaseBorderoDati(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), dDataOraInizio, dDataOraFine);
            }

            return oResult;
        }


        public class ResultBaseBorderoConfig : ResultBase
        {
            public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }

            public ResultBaseBorderoConfig() : base()
            { }
            public ResultBaseBorderoConfig(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libBordero.Bordero.clsBordQuoteConfig configurazione) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.ConfigurazioneBordero = configurazione;
            }
        }

        public class ResultBaseBorderoSearch : ResultBase
        {
            public libBordero.Bordero.clsSearchEvento RichiestaEventi { get; set; }
            public ResultBaseBorderoSearch() : base()
            { }

            public ResultBaseBorderoSearch(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, libBordero.Bordero.clsSearchEvento searchEventi)
            {
                this.RichiestaEventi = searchEventi;
            }
        }

        public static ResultBaseBorderoConfig GetConfigBordero(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsBordQuoteConfig Configurazione = null;
            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                        {
                            oTokenCheck = new Riepiloghi.clsToken(Token);
                            idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                            if (idCinema > 0)
                            {
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }

                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            if (idCinema > 0)
                                nIdUxc = idCinema;
                            else
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                            if (oError == null && nIdUxc > 0)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                if (oError == null)
                                {
                                    Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                    List<string> codiciLocaliCorrenti = new List<string>();
                                    List<string> cfOrganizzatoriCorrenti = new List<string>();
                                    foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                    {
                                        if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                        {
                                            oCodiceSistemaCorrente = oItemCodiceSistema;
                                            if (idCinema > 0)
                                            {
                                                oCodiceSistemaCorrente.AllCodiciLocali = false;
                                                oCodiceSistemaCorrente.AllCFOrganizzatori = false;
                                            }
                                            foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oCodiceLocale.Enabled && CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == oCodiceLocale.CodiceLocale) != null && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                {
                                                    codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                }
                                            }

                                            foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oOrganizzatore.Enabled && OrganizzatoriAbilitati != null && OrganizzatoriAbilitati.FirstOrDefault(x => x.CFOrganizzatore == oOrganizzatore.CFOrganizzatore) != null && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                {
                                                    cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                }
                                            }
                                            break;
                                        }
                                    }

                                    if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                    {

                                    }
                                    else
                                    {
                                        Configurazione = libBordero.Bordero.GetConfigurazione(oConnection, nIdUxc);
                                        if (idCinema > 0)
                                        {
                                            List<clsDescriptorField> temp = new List<clsDescriptorField>();
                                            if (Configurazione.DescrCodiciLocali != null && Configurazione.DescrCodiciLocali.Count > 0)
                                            {
                                                
                                                foreach (clsDescriptorField item in Configurazione.DescrCodiciLocali)
                                                {
                                                    if (CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == item.Key ) != null)
                                                    {
                                                        temp.Add(new clsDescriptorField() { Key = item.Key, Value = item.Value });
                                                    }
                                                }
                                                Configurazione.DescrCodiciLocali = temp;
                                            }
                                            else if (CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.Count > 0)
                                            {
                                                CodiciLocaliAbilitati.ForEach(x =>
                                                {
                                                    temp.Add(new clsDescriptorField() { Key = x.CodiceLocale, Value = x.Descrizione });
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }
            else
            {
                oResult = new ResultBaseBorderoConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }

            return oResult;
        }

        public static ResultBaseBorderoConfig SetConfigBordero(string Token, libBordero.Bordero.clsBordQuoteConfig configurazione, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoConfig oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsBordQuoteConfig Configurazione = null;

            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONF_BORDERO", Token, out oError))
                    //{

                    //}
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                        {
                            oTokenCheck = new Riepiloghi.clsToken(Token);
                            idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                            if (idCinema > 0)
                            {
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }

                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            if (idCinema > 0)
                                nIdUxc = idCinema;
                            else
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                            if (oError == null && nIdUxc > 0)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                if (oError == null)
                                {
                                    Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                    List<string> codiciLocaliCorrenti = new List<string>();
                                    List<string> cfOrganizzatoriCorrenti = new List<string>();
                                    foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                    {
                                        if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                        {
                                            oCodiceSistemaCorrente = oItemCodiceSistema;
                                            if (idCinema > 0)
                                            {
                                                oCodiceSistemaCorrente.AllCodiciLocali = false;
                                                oCodiceSistemaCorrente.AllCFOrganizzatori = false;
                                            }
                                            foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oCodiceLocale.Enabled && CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == oCodiceLocale.CodiceLocale) != null && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                {
                                                    codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                }
                                            }

                                            foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oOrganizzatore.Enabled && OrganizzatoriAbilitati != null && OrganizzatoriAbilitati.FirstOrDefault(x => x.CFOrganizzatore == oOrganizzatore.CFOrganizzatore) != null && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                {
                                                    cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                    {

                                    }
                                    else
                                    {
                                        Configurazione = libBordero.Bordero.SetConfigurazione(oConnection, nIdUxc, configurazione, out oError);
                                        if (idCinema > 0)
                                        {
                                            List<clsDescriptorField> temp = new List<clsDescriptorField>();
                                            if (Configurazione.DescrCodiciLocali != null && Configurazione.DescrCodiciLocali.Count > 0)
                                            {

                                                foreach (clsDescriptorField item in Configurazione.DescrCodiciLocali)
                                                {
                                                    if (CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == item.Key) != null)
                                                    {
                                                        temp.Add(new clsDescriptorField() { Key = item.Key, Value = item.Value });
                                                    }
                                                }
                                                Configurazione.DescrCodiciLocali = temp;
                                            }
                                            else if (CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.Count > 0)
                                            {
                                                CodiciLocaliAbilitati.ForEach(x =>
                                                {
                                                    temp.Add(new clsDescriptorField() { Key = x.CodiceLocale, Value = x.Descrizione });
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero", "Errore imprevisto durante il calcolo del boprdero.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoConfig(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }
            else
            {
                oResult = new ResultBaseBorderoConfig(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Configurazione);
            }

            return oResult;
        }

        public static ResultBaseBorderoSearch BorderoSearchEvento(string Token, clsSearchEvento RichiestaEventi, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoSearch oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            libBordero.Bordero.clsSearchEvento Search = null;
            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CONF_BORDERO", Token, out oError))
                    //{
                        
                    //}
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                        {
                            oTokenCheck = new Riepiloghi.clsToken(Token);
                            idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                            if (idCinema > 0)
                            {
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                        }


                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            if (idCinema > 0)
                                nIdUxc = idCinema;
                            else
                                nIdUxc = libBordero.Bordero.GetIdUxcFromProfileId(oConnection, ProfileId, out oError);
                            if (oError == null && nIdUxc > 0)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                                if (oError == null)
                                {
                                    Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                                    List<string> codiciLocaliCorrenti = new List<string>();
                                    List<string> cfOrganizzatoriCorrenti = new List<string>();
                                    foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                                    {
                                        if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                                        {
                                            oCodiceSistemaCorrente = oItemCodiceSistema;
                                            if (idCinema > 0)
                                            {
                                                oCodiceSistemaCorrente.AllCodiciLocali = false;
                                                oCodiceSistemaCorrente.AllCFOrganizzatori = false;
                                            }
                                            foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oCodiceLocale.Enabled && CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == oCodiceLocale.CodiceLocale) != null && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                    {
                                                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                                                {
                                                    codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                                                }
                                            }

                                            foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                                            {
                                                if (idCinema > 0)
                                                {
                                                    if (oOrganizzatore.Enabled && OrganizzatoriAbilitati != null && OrganizzatoriAbilitati.FirstOrDefault(x => x.CFOrganizzatore == oOrganizzatore.CFOrganizzatore) != null && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                    {
                                                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                    }
                                                }
                                                else if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                                                {
                                                    cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    if (!libBordero.Bordero.CheckIdUxcFromProfileId(oConnection, nIdUxc, oCodiceSistemaCorrente.AllCodiciLocali, codiciLocaliCorrenti, oCodiceSistemaCorrente.AllCFOrganizzatori, cfOrganizzatoriCorrenti, out oError))
                                    {

                                    }
                                    else
                                    {
                                        Search = new libBordero.Bordero.clsSearchEvento();
                                        Search.TipoRicerca = RichiestaEventi.TipoRicerca;
                                        Search.CodiceLocale = RichiestaEventi.CodiceLocale;
                                        Search.InizioDataEvento = RichiestaEventi.InizioDataEvento;
                                        Search.FineDataEvento = RichiestaEventi.FineDataEvento;
                                        Search.FlagCodiceLocale = RichiestaEventi.FlagCodiceLocale;
                                        Search.FlagDataEvento = RichiestaEventi.FlagDataEvento;
                                        Search.FlagTitolo = RichiestaEventi.FlagTitolo;
                                        Search.Titolo = RichiestaEventi.Titolo;
                                        Search.Eventi = libBordero.Bordero.RicercaEventi(oConnection, nIdUxc, Search, idCinema);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Ricerca evento per configurazione Bordero", "Errore imprevisto durante la ricerca.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoSearch(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Search);
            }
            else
            {
                oResult = new ResultBaseBorderoSearch(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), Search);
            }

            return oResult;
        }

        public class ResultBaseBorderoCedas : ResultBase
        {

            public clsBorderoCedas BorderoCedas { get; set; }
            

            public ResultBaseBorderoCedas() : base()
            {
            }

            public ResultBaseBorderoCedas(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, clsBorderoCedas borderoCedas) : base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.BorderoCedas = new libBordero.Bordero.clsBorderoCedas();
                this.BorderoCedas.CEDAS_ID = borderoCedas.CEDAS_ID;
                this.BorderoCedas.CODICE_LOCALE = borderoCedas.CODICE_LOCALE;
                this.BorderoCedas.DATA = borderoCedas.DATA;
                this.BorderoCedas.PROGRESSIVO = borderoCedas.PROGRESSIVO;
                this.BorderoCedas.ORA_INIZIO = borderoCedas.ORA_INIZIO;
                this.BorderoCedas.TITOLO = borderoCedas.TITOLO;
                this.BorderoCedas.XML = borderoCedas.XML;
            }
        }

        public static ResultBaseBorderoCedas BorderoInsertCedas(string Token, clsBorderoCedas borderoCedas, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoCedas oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            long cedasId = 0;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CALC_BORDERO", Token, out oError))
                    //{
                        
                    //}
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            StringBuilder sb = null;
                            clsParameters parameters = null;
                            try
                            {
                                parameters = new clsParameters();
                                parameters.Add(":pCODICE_LOCALE", borderoCedas.CODICE_LOCALE);
                                parameters.Add(":pDATA", borderoCedas.DATA);
                                parameters.Add(":pPROGRESSIVO", borderoCedas.PROGRESSIVO);
                                parameters.Add(":pORA_INIZIO", borderoCedas.ORA_INIZIO);
                                parameters.Add(":pTITOLO", borderoCedas.TITOLO);
                                parameters.Add(":pXML", borderoCedas.XML);

                                sb = new StringBuilder();
                                sb.Append("INSERT INTO CINEMA.CEDAS_BORDERO");
                                sb.Append(" (");
                                sb.Append("    CEDAS_ID");
                                sb.Append("  , CODICE_LOCALE");
                                sb.Append("  , DATA");
                                sb.Append("  , PROGRESSIVO");
                                sb.Append("  , ORA_INIZIO");
                                sb.Append("  , TITOLO");
                                sb.Append("  , XML");
                                sb.Append(" )");
                                sb.Append(" VALUES");
                                sb.Append(" (");
                                sb.Append("    CINEMA.SEQ_CEDAS_ID.NEXTVAL");
                                sb.Append("  , :pCODICE_LOCALE");
                                sb.Append("  , :pDATA");
                                sb.Append("  , :pPROGRESSIVO");
                                sb.Append("  , :pORA_INIZIO");
                                sb.Append("  , :pTITOLO");
                                sb.Append("  , :pXML");
                                sb.Append(" )");
                                cedasId = long.Parse(oConnection.ExecuteNonQuery(sb.ToString(), "CEDAS_ID", parameters).ToString());
                                borderoCedas.CEDAS_ID = cedasId;
                            }
                            catch (Exception ex)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero Inserimento Cedas", "Errore imprevisto durante inserimento Cedas.", ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero Inserimento Cedas Generico", "Errore imprevisto durante inserimento Cedas Generico.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoCedas(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), borderoCedas);
            }
            else
            {
                oResult = new ResultBaseBorderoCedas(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), borderoCedas);
            }

            return oResult;
        }

        public static ResultBaseBorderoCedas BorderoUpdateCedas(string Token, clsBorderoCedas borderoCedas, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseBorderoCedas oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "CALC_BORDERO", Token, out oError))
                    //{

                    //}
                    Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                    long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                    if (oError == null)
                    {
                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);
                        Int64 nIdUxc = 0;
                        if (oError == null)
                        {
                            StringBuilder sb = null;
                            clsParameters parameters = null;
                            try
                            {
                                parameters = new clsParameters();
                                parameters.Add(":pCEDAS_ID", borderoCedas.CEDAS_ID);
                                string errorMessage = borderoCedas.TIPO == "xml" ? borderoCedas.SENT_XML_ERROR : borderoCedas.SENT_PDF_ERROR;
                                if (!string.IsNullOrEmpty(errorMessage))
                                    parameters.Add(":pVALUE", errorMessage);

                                //SENT_XML_ERROR
                                sb = new StringBuilder();
                                sb.Append("UPDATE CINEMA.CEDAS_BORDERO ");
                                if (borderoCedas.TIPO == "xml")
                                {
                                    if (!string.IsNullOrEmpty(errorMessage))
                                        sb.Append("SET SENT_XML_ERROR = :pVALUE ");
                                    else
                                        sb.Append("SET SENT_XML_ERROR = NULL ");
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(errorMessage))
                                        sb.Append("SET SENT_PDF_ERROR = :pVALUE ");
                                    else
                                        sb.Append("SET SENT_PDF_ERROR = NULL ");
                                }
                                sb.Append(" WHERE CEDAS_ID = :pCEDAS_ID");
                                oConnection.ExecuteNonQuery(sb.ToString(), parameters);
                            }
                            catch (Exception ex)
                            {
                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero aggiornamento Cedas", "Errore imprevisto durante aggiornamento Cedas.", ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Calcolo Bordero aggiornamento Cedas Generico", "Errore imprevisto durante aggiornamento Cedas Generico.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseBorderoCedas(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), borderoCedas);
            }
            else
            {
                oResult = new ResultBaseBorderoCedas(true, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), borderoCedas);
            }

            return oResult;
        }

        #endregion

        #region SCS

        public class ResultBaseScs : ResultBase
        {
            public Riepiloghi.SCS_SALE Scs { get; set; }

            public ResultBaseScs() : base()
            { }

            public ResultBaseScs(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, List<Riepiloghi.SCS_SALA> listaSale)
                :base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Scs = new Riepiloghi.SCS_SALE();
                this.Scs.ListaSale = listaSale;
            }

            public ResultBaseScs(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime, Riepiloghi.SCS_SALE scs)
                :base(lStatusOK, cStatusMessage, nStatusCode, cExecutionTime)
            {
                this.Scs = scs;
            }

           
        }

        

        public static ResultBaseScs ScsGetListaSale(string Token, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<Riepiloghi.SCS_SALA> listaSale = null;
            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                {
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                                }
                            }


                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null && Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                {
                                    listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Ricerca evento per configurazione Bordero", "Errore imprevisto durante la ricerca.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), listaSale);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), listaSale);
            }

            return oResult;
        }

        public static ResultBaseScs ScsGetNewScs(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE() { IDSALA1 = parScs.IDSALA1, IDSALA2 = parScs.IDSALA2, PERIODO_INIZIO = parScs.PERIODO_INIZIO, PERIODO_FINE = parScs.PERIODO_FINE };
            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {

                                    parScs.AccountId = oToken.AccountId;
                                    if (oProfile.CodiciSistema.Count == 1)
                                        parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                                    if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                    {
                                        List<Riepiloghi.SCS_SALA> listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);
                                        if (listaSale.Find(x => x.IDSALA == parScs.IDSALA1) != null && listaSale.Find(x => x.IDSALA == parScs.IDSALA2) != null)
                                        {
                                            scs = Riepiloghi.clsRiepiloghi.ScsGetNewScs(oConnection, parScs, out oError, CodiciLocaliAbilitati, OrganizzatoriAbilitati);
                                        }
                                        else
                                            oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Sale non abilitate per questa utenza.");
                                    }
                                    else
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Errore imprevisto durante la creazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }

        public static ResultBaseScs ScsConfirmScs(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE();

            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);


                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {

                                    parScs.AccountId = oToken.AccountId;
                                    if (oProfile.CodiciSistema.Count == 1)
                                        parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                                    if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                    {
                                        List<Riepiloghi.SCS_SALA> listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);

                                        Riepiloghi.SCS_SALE checkScs = Riepiloghi.clsRiepiloghi.ScsGetScs(oConnection, new Riepiloghi.SCS_SALE() { IDSCSO = parScs.IDSCSO }, out oError);

                                        if (oError == null)
                                        {
                                            if (listaSale.Find(x => x.IDSALA == checkScs.IDSALA1) == null || listaSale.Find(x => x.IDSALA == checkScs.IDSALA2) == null)
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Sale non abilitate per questa utenza.");
                                        }


                                        if (oError == null && checkScs.Eventi != null)
                                        {
                                            checkScs.Eventi.ForEach(evento =>
                                            {
                                                if (oError == null && OrganizzatoriAbilitati.Find(x => x.CFOrganizzatore == evento.ORGANIZZATORE) == null)
                                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Organizzatore non abilitato per questa utenza.");
                                            });
                                        }

                                        if (oError == null)
                                            scs = Riepiloghi.clsRiepiloghi.ScsConfirmScs(oConnection, parScs, out oError);
                                    }
                                    else
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                                }
                            }


                            //if (oError == null)
                            //{
                            //    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                            //    if (oError == null)
                            //    {
                            //        parScs.AccountId = oToken.AccountId;
                            //        if (oProfile.CodiciSistema.Count == 1)
                            //            parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                            //        if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                            //        {
                            //            List<Riepiloghi.SCS_SALA> listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);
                            //            if (listaSale.Find(x => x.IDSALA == parScs.IDSALA1) != null && listaSale.Find(x => x.IDSALA == parScs.IDSALA2) != null)
                            //            {
                            //                scs = Riepiloghi.clsRiepiloghi.ScsConfirmScs(oConnection, parScs, out oError);
                            //            }
                            //            else
                            //                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Sale non abilitate per questa utenza.");
                            //        }
                            //        else
                            //            oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Errore imprevisto durante la conferma.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }


        public static ResultBaseScs ScsAbortScs(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE();

            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {

                                    parScs.AccountId = oToken.AccountId;
                                    if (oProfile.CodiciSistema.Count == 1)
                                        parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                                    if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                    {
                                        List<Riepiloghi.SCS_SALA> listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);

                                        Riepiloghi.SCS_SALE checkScs = Riepiloghi.clsRiepiloghi.ScsGetScs(oConnection, new Riepiloghi.SCS_SALE() { IDSCSO = parScs.IDSCSO }, out oError);

                                        if (oError == null)
                                        {
                                            if (listaSale.Find(x => x.IDSALA == checkScs.IDSALA1) == null || listaSale.Find(x => x.IDSALA == checkScs.IDSALA2) == null)
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Sale non abilitate per questa utenza.");
                                        }

                                        if (oError == null && checkScs.Eventi != null)
                                        {
                                            checkScs.Eventi.ForEach(evento =>
                                            {
                                                if (oError == null && OrganizzatoriAbilitati.Find(x => x.CFOrganizzatore == evento.ORGANIZZATORE) == null)
                                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Organizzatore non abilitato per questa utenza.");
                                            });
                                        }

                                        if (oError == null)
                                            scs = Riepiloghi.clsRiepiloghi.ScsAbortScs(oConnection, parScs, out oError);
                                    }
                                    else
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Annullo Scambio", "Errore imprevisto durante l'annullo.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }

        public static ResultBaseScs ScsDefinitivoScs(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE();

            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;


            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);


                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {

                                    parScs.AccountId = oToken.AccountId;
                                    if (oProfile.CodiciSistema.Count == 1)
                                        parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                                    if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                    {
                                        List<Riepiloghi.SCS_SALA> listaSale = Riepiloghi.clsRiepiloghi.ScsGetListaSale(oConnection, out oError, CodiciLocaliAbilitati);

                                        Riepiloghi.SCS_SALE checkScs = Riepiloghi.clsRiepiloghi.ScsGetScs(oConnection, new Riepiloghi.SCS_SALE() { IDSCSO = parScs.IDSCSO }, out oError);

                                        if (oError == null)
                                        {
                                            if (listaSale.Find(x => x.IDSALA == checkScs.IDSALA1) == null || listaSale.Find(x => x.IDSALA == checkScs.IDSALA2) == null)
                                                oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma DEFINITIVA Scambio", "Sale non abilitate per questa utenza.");
                                        }

                                        if (oError == null && checkScs.Eventi != null)
                                        {
                                            checkScs.Eventi.ForEach(evento =>
                                            {
                                                if (oError == null && OrganizzatoriAbilitati.Find(x => x.CFOrganizzatore == evento.ORGANIZZATORE) == null)
                                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma DEFINITIVA Scambio", "Organizzatore non abilitato per questa utenza.");
                                            });
                                        }

                                        if (oError == null)
                                            scs = Riepiloghi.clsRiepiloghi.ScsDefinitivoScs(oConnection, parScs, out oError);
                                    }
                                    else
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma DEFINITIVA Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                                }
                            }
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma DEFINITIVA Scambio", "Errore imprevisto durante la conferma.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }

        //ScsDeleteScsProvvisorio
        public static ResultBaseScs ScsArchiviaScsProvvisorio(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE();

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {
                                    scs = Riepiloghi.clsRiepiloghi.ScsArchiviaScsProvvisorio(oConnection, parScs, out oError);
                                    scs = parScs;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Elimina Scambio provvisorio", "Errore imprevisto durante l'eliminazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }

        public static ResultBaseScs ScsGetScsStorici(string Token, Riepiloghi.SCS_SALE parScs, clsEnvironment oEnvironment)
        {
            DateTime dStart = DateTime.Now;
            ResultBaseScs oResult = null;
            Exception oError = null;
            IRecordSet oRS = null;
            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            Riepiloghi.SCS_SALE scs = new Riepiloghi.SCS_SALE();
            long idCinema = 0;
            Riepiloghi.clsToken oTokenCheck = null;
            List<Riepiloghi.clsCodiceLocale> CodiciLocaliAbilitati = null;
            List<Riepiloghi.clsCFOrganizzatore> OrganizzatoriAbilitati = null;

            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    //if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "SCAMBIO_SALE", Token, out oError))
                    //{

                    //}
                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI", Token, out oError))
                    {
                        Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                        long ProfileId = Riepiloghi.clsAccount.GetAccountProfileId(oToken.AccountId, oConnectionInternal, out oError);

                        if (oError == null)
                        {
                            oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);


                            if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                oTokenCheck = new Riepiloghi.clsToken(Token);
                                idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                                if (idCinema > 0)
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }

                            if (oError == null)
                            {
                                Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError, true);
                                if (oError == null)
                                {

                                    parScs.AccountId = oToken.AccountId;
                                    if (oProfile.CodiciSistema.Count == 1)
                                        parScs.IdOperatore = Riepiloghi.clsRiepiloghi.GetIdOperatoreCodiceSistemaUnicoFromAccountId(oConnectionInternal, oProfile.CodiciSistema[0].CodiceSistema, oToken.AccountId, out oError);

                                    if (Riepiloghi.clsRiepiloghi.GetCodiciLocaliOrganizzatoriScs(Token, oConnectionInternal, oConnection, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati))
                                    {
                                        if (oError == null)
                                            scs.ListaScs = Riepiloghi.clsRiepiloghi.ScsGetScsStorici(oConnection, parScs, out oError, CodiciLocaliAbilitati, OrganizzatoriAbilitati);
                                    }
                                    else
                                        oError = Riepiloghi.clsRiepiloghi.GetNewException("Conferma DEFINITIVA Scambio", "Nessuna Sala/Organizzatore abilitati per questa utenza.");
                                }
                            }



                            //if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            //{
                            //    oTokenCheck = new Riepiloghi.clsToken(Token);
                            //    idCinema = oTokenCheck.CodiceSistema.MultiCinema.IdCinema;
                            //    if (idCinema > 0)
                            //    {
                            //        Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            //    }
                            //}


                            //if (oError == null)
                            //{
                            //    Riepiloghi.clsProfile oProfile = Riepiloghi.clsProfile.GetProfile(ProfileId, oConnectionInternal, out oError);
                            //    if (oError == null)
                            //    {
                            //        Riepiloghi.clsCodiceSistema oCodiceSistemaCorrente = null;
                            //        List<string> codiciLocaliCorrenti = new List<string>();
                            //        List<string> cfOrganizzatoriCorrenti = new List<string>();

                            //        foreach (Riepiloghi.clsCodiceSistema oItemCodiceSistema in oProfile.CodiciSistema)
                            //        {
                            //            if (oItemCodiceSistema.CodiceSistema == oToken.CodiceSistema.CodiceSistema)
                            //            {
                            //                oCodiceSistemaCorrente = oItemCodiceSistema;
                            //                if (idCinema > 0)
                            //                {
                            //                    oCodiceSistemaCorrente.AllCodiciLocali = false;
                            //                    oCodiceSistemaCorrente.AllCFOrganizzatori = false;
                            //                }
                            //                foreach (Riepiloghi.clsCodiceLocale oCodiceLocale in oItemCodiceSistema.CodiciLocale)
                            //                {
                            //                    if (idCinema > 0)
                            //                    {
                            //                        if (oCodiceLocale.Enabled && CodiciLocaliAbilitati != null && CodiciLocaliAbilitati.FirstOrDefault(x => x.CodiceLocale == oCodiceLocale.CodiceLocale) != null && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                            //                        {
                            //                            codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                            //                        }
                            //                    }
                            //                    else if ((oCodiceSistemaCorrente.AllCodiciLocali || oCodiceLocale.Enabled) && !codiciLocaliCorrenti.Contains(oCodiceLocale.CodiceLocale))
                            //                    {
                            //                        codiciLocaliCorrenti.Add(oCodiceLocale.CodiceLocale);
                            //                    }
                            //                }

                            //                foreach (Riepiloghi.clsCFOrganizzatore oOrganizzatore in oItemCodiceSistema.CFOrganizzatori)
                            //                {
                            //                    if (idCinema > 0)
                            //                    {
                            //                        if (oOrganizzatore.Enabled && OrganizzatoriAbilitati != null && OrganizzatoriAbilitati.FirstOrDefault(x => x.CFOrganizzatore == oOrganizzatore.CFOrganizzatore) != null && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                            //                        {
                            //                            cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                            //                        }
                            //                    }
                            //                    else if ((oCodiceSistemaCorrente.AllCFOrganizzatori || oOrganizzatore.Enabled) && !cfOrganizzatoriCorrenti.Contains(oOrganizzatore.CFOrganizzatore))
                            //                    {
                            //                        cfOrganizzatoriCorrenti.Add(oOrganizzatore.CFOrganizzatore);
                            //                    }
                            //                }
                            //                break;
                            //            }
                            //        }

                            //        scs.ListaScs = Riepiloghi.clsRiepiloghi.ScsGetScsStorici(oConnection, parScs, out oError, codiciLocaliCorrenti, cfOrganizzatoriCorrenti);
                            //    }
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Creazione Scambio", "Errore imprevisto durante la creazione.", ex);
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            if (oError == null)
            {
                oResult = new ResultBaseScs(true, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }
            else
            {
                oResult = new ResultBaseScs(false, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart), scs);
            }

            return oResult;
        }

        #endregion

        #region "Stampa"


        public static ResultBase PrintRiepilogoToPDF(string Token, DateTime giorno, string tipo, long? progressivo, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBase oResult = new ResultBase();
            string pathRisorse = oEnvironment.PathRisorseWeb;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            DateTime dSysdate = DateTime.MinValue;

            Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
            percorsi.Risorse = oEnvironment.PathRisorseWeb;
            percorsi.Temp = oEnvironment.PathTemp;
            percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;

            try
            {

                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "PrintRiepilogoToPDF", Token, new Dictionary<string, object>() { { "giorno", (giorno == null ? "" : ((DateTime)giorno).ToShortDateString()) },  { "tipo", tipo }, { "progressivo", (progressivo == null ? 0 : progressivo) } });

                    bool lCheckUserOperazione = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_STAMPA", Token, out oError);

                    if (lCheckUserOperazione && oError == null)
                    {
                        try
                        {
                            string codOperazioneStampaSpecifica = "RIEPILOGHI_STAMPA_" + tipo;
                            Riepiloghi.clsOperazioneRiepiloghi operazioneRiepiloghi = Riepiloghi.clsOperazioneRiepiloghi.GetOperazioneRiepiloghi(codOperazioneStampaSpecifica, oConnectionInternal, out oError);
                            oError = null;
                            if (operazioneRiepiloghi != null && operazioneRiepiloghi.IdCategoria > 0)
                            {
                                if (!operazioneRiepiloghi.Enabled)
                                    oError = oError = Riepiloghi.clsRiepiloghi.GetNewException("Operazione", "Operazione non abilitata.");
                                else
                                    lCheckUserOperazione = ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, codOperazioneStampaSpecifica, Token, out oError);
                            }
                        }
                        catch (Exception)
                        {
                        }
                        
                    }
                    

                    if (lCheckUserOperazione)
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_STAMPA", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            long nProgressivo = 0;
                            if (progressivo != null)
                                nProgressivo = (long)progressivo.Value;
                            string BufferPdfBase64 = "";

                            List<string> CodiciLocaliAbilitati = null;
                            List<string> OrganizzatoriAbilitati = null;
                            if (Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                            {
                                Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oToken.CodiceSistema.CodiceSistema, oToken.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                            }
                            

                            if (tipo == "R")
                                lRet = Riepiloghi.clsRiepiloghi.PrintRCAToPDF(giorno, tipo, nProgressivo, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, true, out oError, "", out BufferPdfBase64, (CodiciLocaliAbilitati == null ? null : CodiciLocaliAbilitati.ToArray()), (OrganizzatoriAbilitati == null ? null : OrganizzatoriAbilitati.ToArray()), 0, percorsi);
                            else
                                lRet = Riepiloghi.clsRiepiloghi.PrintRiepilogoToPDF(Token, giorno, tipo, nProgressivo, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, true, out oError, "", out BufferPdfBase64, (CodiciLocaliAbilitati == null ? null : CodiciLocaliAbilitati.ToArray()), (OrganizzatoriAbilitati == null ? null : OrganizzatoriAbilitati.ToArray()), 0, percorsi);

                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ret PrintRiepilogoToPDF", lRet);
                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "BufferPdfBase64 PrintRiepilogoToPDF", (BufferPdfBase64 == null ? "null" : BufferPdfBase64.Length.ToString()));

                            if (lRet)
                            {
                                lRet = !string.IsNullOrEmpty(BufferPdfBase64);
                                if (!lRet) oError = new Exception("Nessun file pdf generato");
                            }


                            if (lRet)
                            {
                                oResult = new ResultBase(lRet, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.contentFile = new ResultContentFile();
                                oResult.contentFile.ContentType = "Application/Pdf";
                                oResult.contentFile.FileName = String.Format("{0}_{1}_{2}_{3}_{4}.pdf",
                                                                             (tipo == "G" ? "RPG" : (tipo == "R" ? "RCA" : "RPM")),
                                                                             giorno.Year.ToString("0000").Trim(),
                                                                             giorno.Month.ToString("00").Trim(),
                                                                             (tipo == "M" ? "00" : giorno.Day.ToString("00").Trim()),
                                                                             (tipo == "D" ? "Simulazione" : nProgressivo.ToString("000").Trim()));
                                oResult.contentFile.Buffer = BufferPdfBase64;
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore generazione pdf", "Errore imprevisto.", ex);
                oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }


        public static ResultBase StampaTransazioniAnnullati(string Token, DateTime dDataEmiInizio, DateTime dDataEmiFine, string cDataEmissioneAnnullamento, string cDataEvento, string cCodiceLocale, string cOraEvento, string cTitoloEvento, string ModalitaStorico, Int64 IdFilterExtIdVend, clsEnvironment oEnvironment)
        {
            Riepiloghi.clsLogRequest oLogRequest = null;
            DateTime dStart = DateTime.Now;
            bool lRet = false;
            Exception oError = null;
            ResultBase oResult = new ResultBase();
            string pathRisorse = oEnvironment.PathRisorseWeb;

            IConnection oConnectionInternal = null;
            IConnection oConnection = null;
            List<string> CodiciLocaliAbilitati = null;
            List<string> OrganizzatoriAbilitati = null;
            DateTime dSysdate = DateTime.MinValue;

            string cCodiceSistemaUnico = "";
            bool lCodiceSistemaUnico = false;
            Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
            percorsi.Risorse = oEnvironment.PathRisorseWeb;
            percorsi.Temp = oEnvironment.PathTemp;
            percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;
            try
            {
                oConnectionInternal = ServiceRiepiloghi.GetConnectionInternal(out oError, oEnvironment);
                if (oError == null)
                {
                    dSysdate = Riepiloghi.clsRiepiloghi.GetSysdate(oConnectionInternal);

                    lCodiceSistemaUnico = Riepiloghi.clsRiepiloghi.IsModalitaUtentiLocali(oConnectionInternal, out oError, out cCodiceSistemaUnico);

                    oLogRequest = Riepiloghi.clsLogRequest.Log(oConnectionInternal, "StampaTransazioniAnnullati", Token, new Dictionary<string, object>() { { "dDataEmiInizio", (dDataEmiInizio == null ? "" : ((DateTime)dDataEmiInizio).ToShortDateString()) },
                                                                                                                                                            { "dDataEmiFine", (dDataEmiFine == null ? "" : ((DateTime)dDataEmiFine).ToShortDateString()) },
                                                                                                                                                            { "cDataEmissioneAnnullamento", cDataEmissioneAnnullamento },
                                                                                                                                                            { "cDataEvento", cDataEvento },
                                                                                                                                                            { "cCodiceLocale", cCodiceLocale },
                                                                                                                                                            { "cOraEvento", cOraEvento },
                                                                                                                                                            { "cTitoloEvento", cTitoloEvento } });

                    if (ServiceRiepiloghi.CheckUserOperazione(oConnectionInternal, "RIEPILOGHI_TRANSAZIONI_ANNULLATI", Token, out oError))
                    {
                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "RIEPILOGHI_TRANSAZIONI_ANNULLATI", "ok");

                        oConnection = ServiceRiepiloghi.GetConnection(oConnectionInternal, Token, out oError, oEnvironment);

                        Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "errore connessione ? ", oError);

                        if (oError == null)
                        {
                            bool lCheckServerFiscale = (Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(oConnection, "CHECK_SERVER_FISCALE", "DISABILITATO") == "ABILITATO");
                            if (lCheckServerFiscale)
                            {
                                if (!Riepiloghi.clsRiepiloghi.CheckServerFiscale(oConnection, out oError))
                                {
                                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Verifica Codice Sistema", "Server Fiscale non attivo.");
                                }
                            }
                        }

                        if (oError == null)
                        {
                            Riepiloghi.clsToken oToken = new Riepiloghi.clsToken(Token);
                            string BufferPdfBase64 = "";
                            

                            if (oError == null)
                            {
                                if (!lCodiceSistemaUnico)
                                    CodiciLocaliAbilitati = Riepiloghi.clsToken.GetCodiciLocaliAccount(oConnectionInternal, out oError, Token);

                                if (!string.IsNullOrEmpty(Token) && Riepiloghi.clsMultiCinema.CheckMultiCinema(oConnection) && Riepiloghi.clsMultiCinema.CheckModalitaMultiCinema(Token))
                                {
                                    Riepiloghi.clsToken oTokenCheck = new Riepiloghi.clsToken(Token);
                                    Riepiloghi.clsMultiCinema.InitCodiciLocaliOrganizzatori(oConnection, oTokenCheck.CodiceSistema.CodiceSistema, oTokenCheck.CodiceSistema.MultiCinema.IdCinema, out CodiciLocaliAbilitati, out OrganizzatoriAbilitati, out oError);
                                }

                                if (oError == null)
                                {
                                    lRet = Riepiloghi.clsRiepiloghi.PrintTransazioniAnnullati(dDataEmiInizio, dDataEmiFine, cDataEmissioneAnnullamento, cDataEvento, cCodiceLocale, cOraEvento, cTitoloEvento, oConnection, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, out oError, "", out BufferPdfBase64, CodiciLocaliAbilitati, OrganizzatoriAbilitati, 0, percorsi);
                                }
                            }

                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "ret StampaTransazioniAnnullati", lRet);
                            Riepiloghi.clsLogRequest.LogResult(oConnectionInternal, oLogRequest, "BufferPdfBase64 StampaTransazioniAnnullati", (BufferPdfBase64 == null ? "null" : BufferPdfBase64.Length.ToString()));

                            if (lRet)
                            {
                                lRet = !string.IsNullOrEmpty(BufferPdfBase64);
                                if (!lRet) oError = new Exception("Nessun file pdf generato");
                            }


                            if (lRet)
                            {
                                oResult = new ResultBase(lRet, "", 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.contentFile = new ResultContentFile();
                                oResult.contentFile.ContentType = "Application/Pdf";
                                oResult.contentFile.FileName = String.Format("{0}_{1}_{2}.pdf",
                                                                             "RiepilogoAnnullatiPerPeriodo",
                                                                             dDataEmiInizio.Year.ToString("0000").Trim() + dDataEmiInizio.Month.ToString("00") + dDataEmiInizio.Day.ToString("00"),
                                                                             dDataEmiFine.Year.ToString("0000").Trim() + dDataEmiFine.Month.ToString("00") + dDataEmiFine.Day.ToString("00"));
                                oResult.contentFile.Buffer = BufferPdfBase64;
                                oResult.Sysdate = dSysdate;
                            }
                            else
                            {
                                oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                                oResult.Sysdate = dSysdate;
                            }
                        }
                        else
                        {
                            oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                            oResult.Sysdate = dSysdate;
                        }
                    }
                    else
                    {
                        oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        oResult.Sysdate = dSysdate;
                    }
                }
                else
                {
                    oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    oResult.Sysdate = dSysdate;
                }
            }
            catch (Exception ex)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Errore generazione pdf", "Errore imprevisto.", ex);
                oResult = new ResultBase(lRet, oError.Message, 0, Riepiloghi.clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                oResult.Sysdate = dSysdate;
            }
            finally
            {
                if (oConnection != null && oConnection.State == System.Data.ConnectionState.Open) { oConnection.Close(); oConnection.Dispose(); }
                if (oConnectionInternal != null && oConnectionInternal.State == System.Data.ConnectionState.Open) { oConnectionInternal.Close(); oConnectionInternal.Dispose(); }
            }
            return oResult;
        }

        #endregion  

        
    }
}
