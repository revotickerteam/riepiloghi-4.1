﻿using libBordero;
using Riepiloghi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRiepiloghi
{

    public class PayLoadRiepiloghi
    {
        public string Mode { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public ApiRiepiloghi.ServiceRiepiloghi.clsTornelloLogin Tornello_Login { get; set; }
        public string Tornello_Barcode { get; set; }
        public string AccountId { get; set; }

        public string CodiceSistema { get; set; }

        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }

        public string Carta { get; set; }
        public string Sigillo { get; set; }
        public Int64 Progressivo { get; set; }

        public Int64 PrimoProgressivo { get; set; }
        public Int64 UltimoProgressivo { get; set; }

        public string CausaleAnnullamento { get; set; }

        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }

        public DateTime DataRiepilogo { get; set; }
        public string TipoRiepilogo { get; set; }
        public long ProgressivoRiepilogo { get; set; }

        public string CambioStatoLta { get; set; }

        public string Proprieta { get; set; }
       
        public PlayLoadFiltri Filtri { get; set; }
        
        public Riepiloghi.clsAccount Account { get; set; }
        public Riepiloghi.clsProfile Profile { get; set; }

        public Int64 IdCalend { get; set; }
        public DateTime? DataEvento { get; set; }
        public string CodiceLocale { get; set; }
        public string OraEvento { get; set; }
        public string TitoloEvento { get; set; }

        public List<long> QuoteManualiEscluse { get; set; }
        public List<long> QuoteManualiTe { get; set; }
        public List<long> QuoteManualiSi { get; set; }

        public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }
        public libBordero.Bordero.clsSearchEvento BorderoSearchEvento { get; set; }

        public libBordero.Bordero.clsBorderoCedas BorderoCedas { get; set; }

        public Riepiloghi.SCS_SALE Scs { get; set; }

        public string CodiceFiscaleOrganizzatore { get; set; }
        public string TitoloAbbonamento { get; set; }

        public DateTime? DataEmissione { get; set; }
    }

    

    public class PlayLoadFiltri
    {
        public string DataEmissioneAnnullamento { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string CodiceLocale { get; set; }
        public string TitoloEvento { get; set; }
    }


    public class ResponseRiepiloghi
    {
        public ResponseDataRiepiloghi Data { get; set; }
        public ResultBase Status { get; set; }
    }
    public class ResponseDataRiepiloghi
    {
        public string Token { get; set; }

        // GetAccount
        public Riepiloghi.clsAccount Account { get; set; }
        public Riepiloghi.clsProfile Profile { get; set; }

        public List<Riepiloghi.clsProfile> Profiles { get; set; }
        public List<Riepiloghi.clsAccount> Accounts = null;

        // GetCodiceSistema
        public Riepiloghi.clsCodiceSistema CodiceSistema { get; set; }

        // GetListaCampiLogTransazioniCS
        public clsResultBaseCampoLogTransazioni[] Campi { get; set; }
        public clsResultBaseCampoLogTransazioni[] CampiSmart { get; set; }
        public Riepiloghi.clsFiltroCampoTransazione[] Filtri { get; set; }
        public clsResultBaseCampoLta[] CampiLta { get; set; }
        public clsResultBaseCampoLta[] CampiLtaSmart { get; set; }

        public Riepiloghi.clsCodiceLocale[] CodiciLocali { get; set; }

        // GetTransazioni, GetTransazioniAnnullati
        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }
        public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione[] Transazioni { get; set; }
        public ApiRiepiloghi.ServiceRiepiloghi.clsResultLta[] Lta { get; set; }


        public ApiRiepiloghi.ServiceRiepiloghi.clsResultGetEventi[] Eventi { get; set; }
        public string Carta { get; set; }
        public string Sigillo { get; set; }
        public Int64 Progressivo { get; set; }

        public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione Transazione { get; set; }

        public List<ApiRiepiloghi.ServiceRiepiloghi.clsWSCheckAnnullo> CheckTransazioni { get; set; }

        public string CartaAnnullante { get; set; }
        public string SigilloAnnullante { get; set; }
        public Int64 ProgressivoAnnullante { get; set; }
        public ApiRiepiloghi.ServiceRiepiloghi.clsResultTransazione TransazioneAnnullante { get; set; }

        public List<string> WarningsPostAnnullo { get; set; }

        public List<Riepiloghi.clsRiepilogoGeneratoWebApi> ListaRiepiloghi { get; set; }
        public Riepiloghi.clsRiepilogoGeneratoWebApi RichiestaRiepilogo { get; set; }


        // RIEPILOGHI DA GENERARE

        // RIEPILOGHI SU CUI RICEVERE RISPOSTA o DA INVIARE
        public bool EmailToSendOrReceive { get; set; }


        public ApiRiepiloghi.ServiceRiepiloghi.ResultContentFile contentFile { get; set; }

        public string Tornello_Barcode { get; set; }
        public ApiRiepiloghi.ServiceRiepiloghi.clsTornelloLogin Tornello_Login { get; set; }


        public ApiRiepiloghi.ServiceRiepiloghi.clsResultLta Tornello_Lta { get; set; }

        public string Proprieta { get; set; }
        public string Valore { get; set; }


        public Int64 IdCalend { get; set; }
        public DateTime DataOraEvento { get; set; }
        public string DescrizioneSala { get; set; }
        public string CodiceLocale { get; set; }
        public string OraEvento { get; set; }
        public string TitoloEvento { get; set; }

        public libBordero.Bordero.clsBordQuoteConfig ConfigurazioneBordero { get; set; }
        public DataSet DataSetBordero { get; set; }
        public ResultJsonDataSet DataSetBorderoJson { get; set; }
        public long IdRichiestaBordero { get; set; }

        public libBordero.Bordero.clsSearchEvento RichiestaEventi { get; set; }
        public libBordero.Bordero.clsBorderoCedas BorderoCedas { get; set; }

        public Riepiloghi.SCS_SALE Scs { get; set; }
    }

    public class ResultBase
    {
        public bool StatusOK = true;

        public string StatusMessage = "";

        public Int64 StatusCode = 0;

        public string execution_time = "";

        public DateTime Sysdate { get; set; }

        public ResultBase()
        {
        }

        public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
        {
            this.StatusOK = lStatusOK;
            this.StatusMessage = cStatusMessage;
            this.StatusCode = nStatusCode;
            this.execution_time = cExecutionTime;
        }
    }


    public class clsResultBaseCampoLogTransazioni
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLogTransazioni()
        {
        }

        public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

    public class clsResultBaseCampoLta
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLta()
        {
        }

        public clsResultBaseCampoLta(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

}
