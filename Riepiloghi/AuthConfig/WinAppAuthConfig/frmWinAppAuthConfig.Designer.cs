﻿namespace WinAppAuthConfig
{
    partial class frmWinAppAuthConfig
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCategorieOperazioni = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelData = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlCategorieOperazioni
            // 
            this.pnlCategorieOperazioni.AutoScroll = true;
            this.pnlCategorieOperazioni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCategorieOperazioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCategorieOperazioni.Location = new System.Drawing.Point(0, 0);
            this.pnlCategorieOperazioni.Name = "pnlCategorieOperazioni";
            this.pnlCategorieOperazioni.Size = new System.Drawing.Size(800, 132);
            this.pnlCategorieOperazioni.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 132);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(800, 11);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panelData
            // 
            this.panelData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelData.Location = new System.Drawing.Point(0, 143);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(800, 307);
            this.panelData.TabIndex = 2;
            // 
            // frmWinAppAuthConfig
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panelData);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlCategorieOperazioni);
            this.Name = "frmWinAppAuthConfig";
            this.Text = "Configurazione Riepiloghi Remoti";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCategorieOperazioni;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelData;
    }
}

