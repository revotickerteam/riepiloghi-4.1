﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ApiRiepiloghi;
using static ApiRiepiloghi.ServiceRiepiloghi;

namespace WinAppAuthConfig
{
    public partial class frmWinAppAuthConfig : frmBaseAuth
    {

        private ResultBaseToken TokenLogin = null;
        private ResultBaseAccount _account = null;


        #region Controlli personalizzati


        private class clsPanelGroup : Panel
        {

            private Size base_sizeOnTop { get; set; }
            private Size sizeOnTop { get; set; }
            public enum defPanelState : int
            {
                None = 0,
                Collapsed = 1,
                Expanded = 2
            }

            private defPanelState state { get; set; }

            private List<clsButton> buttonsOnTop { get; set; }
            private List<Rectangle> listRectButtons = null;


            private string text { get; set; }
            
            public clsPanelGroup()
                :base()
            {
                this.state = defPanelState.None;
                this.Padding = new Padding(3, 15, 3, 3);
                this.BorderStyle = BorderStyle.FixedSingle;
                this.Dock = DockStyle.Top;
                this.AutoSize = true;
                this.sizeOnTop = new Size(0, 0);
                this.MouseClick += ClsPanelGroup_MouseClick;
                this.buttonsOnTop = new List<clsButton>();
            }

            public List<clsButton> ButtonsOnTop
            {
                get { return this.buttonsOnTop; }
                set { this.buttonsOnTop = value; this.Invalidate(); }
            }

            private void ClsPanelGroup_MouseClick(object sender, MouseEventArgs e)
            {
                
                if (this.Enabled && this.State != defPanelState.None && e.Button == MouseButtons.Left)
                {
                    Rectangle rectM = new Rectangle(e.Location, new Size(1, 1));
                    if (rectM.IntersectsWith(this.GetRectIcon()))
                    {
                        if (this.State == defPanelState.Expanded)
                            this.State = defPanelState.Collapsed;
                        else
                            this.State = defPanelState.Expanded;
                    }
                }
                else if (this.Enabled && this.buttonsOnTop != null && this.buttonsOnTop.Count > 0 && this.listRectButtons != null && this.listRectButtons.Count == this.buttonsOnTop.Count)
                {
                    Rectangle rectM = new Rectangle(e.Location, new Size(1, 1));
                    int index = 0;
                    foreach (Rectangle rect in this.listRectButtons)
                    {
                        if (rect.IntersectsWith(rectM))
                        {
                            this.buttonsOnTop[index].PerformClick();
                            break;
                        }
                        index += 1;
                    }
                }
            }

            public clsPanelGroup(string descrizione)
                :this()
            {
                this.text = descrizione;
                System.Drawing.Graphics g = this.CreateGraphics();
                Size oSize = g.MeasureString(this.text, this.Font).ToSize();
                this.base_sizeOnTop = oSize;
                this.sizeOnTop = oSize;
                this.Padding = new Padding(this.Padding.Left, oSize.Height + 10, this.Padding.Right, this.Padding.Bottom);
                oSize.Width += this.Padding.Horizontal + SystemInformation.BorderSize.Width;
                oSize.Height += this.Padding.Vertical + SystemInformation.BorderSize.Height;
                this.MinimumSize = oSize;
                g.Dispose();
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                Rectangle rect = this.ClientRectangle;
                Color foreColor = this.ForeColor;
                if (!this.Enabled)
                    foreColor = Color.DarkGray;
                SolidBrush brush = new SolidBrush(foreColor);
                Pen pen = new Pen(brush);
                StringFormat oST;

                if (this.state != defPanelState.None)
                {
                    Rectangle rectIcon = this.GetRectIcon();
                    //e.Graphics.DrawRectangle(Pens.Black, rectIcon);
                    rect = new Rectangle(rectIcon.Right + 1, rect.Y, rect.Width - rectIcon.Right, rect.Height);
                    oST = new StringFormat();
                    oST.Alignment = StringAlignment.Center;
                    oST.LineAlignment = StringAlignment.Center;
                    CheckBoxRenderer.DrawCheckBox(e.Graphics, rectIcon.Location, System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
                    PointF pointText = new PointF(rectIcon.X + .2f, rectIcon.Y - .2f);
                    RectangleF rectText = new RectangleF(pointText, new SizeF(this.sizeOnTop.Height, this.sizeOnTop.Height));
                    e.Graphics.DrawString((this.state == defPanelState.Collapsed ? "+" : "-"), this.Font, brush, rectText, oST);
                }

                oST = new StringFormat();
                oST.Alignment = StringAlignment.Near;
                oST.LineAlignment = StringAlignment.Near;
                e.Graphics.DrawString(this.text, this.Font, brush, rect, oST);

                if (this.buttonsOnTop != null && buttonsOnTop.Count > 0)
                {
                    Font fontButton = new Font(this.Font.FontFamily, this.Font.Size - 2, FontStyle.Regular);
                    Queue<Rectangle> rectsButtons = this.GetRectsButtons(e.Graphics);
                    foreach (clsButton btn in this.buttonsOnTop)
                    {
                        Rectangle rectBtn = rectsButtons.Dequeue();
                        ButtonRenderer.DrawButton(e.Graphics, rectBtn, btn.Text, fontButton, false, System.Windows.Forms.VisualStyles.PushButtonState.Normal);
                    }
                }
            }

            private Rectangle GetRectIcon()
            {
                Rectangle rectIcon = new Rectangle(2, 2, this.sizeOnTop.Height, this.sizeOnTop.Height);
                return rectIcon;
            }

            private Queue<Rectangle> GetRectsButtons(Graphics g)
            {
                Queue<Rectangle> result = new Queue<Rectangle>();
                int margin = 10;
                Point pointButton = new Point(this.ClientSize.Width - this.Padding.Right - margin, 1);
                Font fontButton = new Font(this.Font.FontFamily, this.Font.Size - 3, FontStyle.Regular);
                this.listRectButtons = new List<Rectangle>();
                foreach (clsButton btn in this.buttonsOnTop)
                {
                    Size sizeText = g.MeasureString(btn.Text, fontButton).ToSize();
                    Rectangle rectBtn = new Rectangle(pointButton.X - sizeText.Width, pointButton.Y, sizeText.Width + margin, sizeText.Height + margin - 3);
                    result.Enqueue(rectBtn);
                    this.listRectButtons.Add(rectBtn);
                    pointButton.X = rectBtn.X - margin - 1;
                }
                return result;
            }

            public defPanelState State
            {
                get { return this.state; }
                set
                {
                    this.state = value;
                    this.Invalidate();
                    if (this.state == defPanelState.None)
                    {

                    }
                    else if (this.state == defPanelState.Collapsed)
                    {
                        this.AutoSize = false;
                        this.MinimumSize = new Size(this.MinimumSize.Width, this.base_sizeOnTop.Height + 10);
                        this.Height = this.base_sizeOnTop.Height;
                    }
                    else if (this.state == defPanelState.Expanded)
                    {
                        this.AutoSize = true;
                    }
                }
            }

            public override bool AutoScroll
            {
                get { return base.AutoScroll; }
                set { base.AutoScroll = value; base.AutoSize = !value; }
            }
        }

        private class clsButton : Button
        {
            public object Id { get; set; }

            private Size glyphSize = new Size(0, 0);

            private CheckState state = CheckState.Indeterminate;

            public delegate void delegateSelected(object sender, EventArgs e);
            public event delegateSelected Selected;
            public event delegateSelected Checked;

            private clsButton()
                :base()
            {
                this.Dock = DockStyle.Top;
            }

            public clsButton(object id, string text)
                :this()
            {
                this.Id = id;
                this.Text = text;
                System.Drawing.Graphics g = this.CreateGraphics();
                Size oSize = g.MeasureString(this.Text, this.Font).ToSize();
                int textMargin = 10;
                oSize.Width += (textMargin * 2) + SystemInformation.BorderSize.Width;
                oSize.Height += (textMargin * 2) + SystemInformation.BorderSize.Height;
                this.MinimumSize = oSize;
                this.glyphSize = CheckBoxRenderer.GetGlyphSize(g, System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
                g.Dispose();
            }

            public CheckState State
            {
                get { return this.state; }
                set
                {
                    this.state = value;
                    this.Invalidate();
                }
            }

            private Rectangle GetRectIcon()
            {
                Rectangle rectIcon = new Rectangle(5, (this.ClientSize.Height - this.glyphSize.Height ) / 2, this.glyphSize.Width, this.glyphSize.Height);
                return rectIcon;
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                if (this.state != CheckState.Indeterminate)
                {
                    System.Windows.Forms.VisualStyles.CheckBoxState checkState = (this.Enabled ? (this.state == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal) :
                                                                                                 (this.state == CheckState.Checked ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedDisabled : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedDisabled));
                    CheckBoxRenderer.DrawCheckBox(e.Graphics, GetRectIcon().Location, checkState);
                }
            }

            protected override void OnMouseClick(MouseEventArgs e)
            {
                Rectangle rectMouse = new Rectangle(e.X, e.Y, 1, 1);
                if (rectMouse.IntersectsWith(this.GetRectIcon()))
                {
                    if (this.Checked != null)
                        this.Checked(this, new EventArgs());
                    else
                        base.OnMouseClick(e);
                }
                else
                {
                    if (this.Selected != null)
                        this.Selected(this, new EventArgs());
                    else
                        base.OnMouseClick(e);
                }
                
            }
        }

        private class clsItem
        {
            public object Id { get; set; }
            public string Text { get; set; }

            private bool ViewId { get; set; }

            public clsItem(object id, string text)
            {
                this.Id = id;
                this.Text = text;
                this.ViewId = false;
            }

            public clsItem(string id, string text, bool viewId)
            {
                this.Id = id;
                this.Text = text;
                this.ViewId = viewId;
            }

            public override string ToString()
            {
                return (this.ViewId ? string.Format("{0} {1}", this.Id.ToString(), this.Text) : this.Text);
            }
        }

        private void RemoveEvents(Control T)
        {
            var propInfo = T.GetType().GetProperty("Events", BindingFlags.NonPublic | BindingFlags.Instance);
            var list = (EventHandlerList)propInfo.GetValue(T, null);
            list.Dispose();
        }

        private void RemoveControlsInside(Control T)
        {
            RemoveEvents(T);
            if (T.HasChildren)
            {
                foreach (Control item in T.Controls)
                {
                    RemoveControlsInside(item);
                }
                T.Controls.Clear();
            }
        }

        #endregion

        public frmWinAppAuthConfig()
        {
            InitializeComponent();
            this.Shown += FrmWinAppAuthConfig_Shown;
        }

        private void FrmWinAppAuthConfig_Shown(object sender, EventArgs e)
        {
            this.CheckTokenLogin();
        }

        #region Inizializzazione interfaccia Account

        private bool CheckTokenLogin()
        {
            bool result = false;
            this.TokenLogin = WinAppUtility.CheckTokenLogin(this.TokenLogin, ref this._account);
            result = this.TokenLogin != null && this.Account != null;
            if (!result)
            {
                this.Close();
                this.Dispose();
            }
            else
            {
                this.InitAccountLogged();
            }
            return result;
        }

        private Riepiloghi.clsAccount Account
        {
            get { return (this._account != null ? this._account.Account : null); }
        }

        #endregion

        #region Menu Operazioni Account


        private void InitAccountLogged()
        {
            this.RemoveControlsInside(this.pnlCategorieOperazioni); ;

            foreach (Riepiloghi.clsCategoriaOperazioniRiepiloghi categoriaOperazioni in this.Account.Operazioni)
            {
                clsPanelGroup panelCategoria = null;
                foreach (Riepiloghi.clsOperazioneRiepiloghi operazione in categoriaOperazioni.ListaOperazioni)
                {
                    if (operazione.Enabled && WinAppUtility.GetListaOperazioniConfig().Contains(operazione.IdOperazione))
                    {
                        if (panelCategoria == null)
                        {
                            panelCategoria = new clsPanelGroup(operazione.DescrizioneCategoria);
                            panelCategoria.Text = categoriaOperazioni.Descrizione;
                            this.pnlCategorieOperazioni.Controls.Add(panelCategoria);
                            panelCategoria.BringToFront();
                        }
                        clsButton btnOperazione = new clsButton(operazione.IdOperazione, operazione.Descrizione);
                        panelCategoria.Controls.Add(btnOperazione);
                        btnOperazione.BringToFront();
                        btnOperazione.Click += BtnOperazione_Click;
                        panelCategoria.PerformLayout();
                    }
                }
            }
        }

        private void BtnOperazione_Click(object sender, EventArgs e)
        {
            this.ResetPanelData();
            try
            {
                clsButton button = (clsButton)sender;
                switch (button.Id.ToString())
                {
                    case "CONFIG_SISTEMI":
                        {
                            this.InitSistemi();
                            break;
                        }
                    case "RIEPILOGHI_PROFILES":
                        {
                            this.InitProfili();
                            break;
                        }
                    case "RIEPILOGHI_ACCOUNTS":
                        {
                            this.InitAccounts();
                            break;
                        }
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Panel Data funzioni di base

        private void ResetPanelData()
        {
            RemoveControlsInside(this.panelData);
        }

        #endregion

        #region Sistemi

        private void InitSistemi()
        {
            clsPanelGroup panelGroup = new clsPanelGroup("Sistemi");
            this.panelData.Controls.Add(panelGroup);
            panelGroup.Dock = DockStyle.Fill;
            panelGroup.BringToFront();

            foreach (Riepiloghi.clsCodiceSistema codiceSistema in this.Account.CodiciSistema)
            {
                clsButton buttonSistema = new clsButton(codiceSistema.CodiceSistema, string.Format("{0} {1}", codiceSistema.CodiceSistema, codiceSistema.Descrizione));
                panelGroup.Controls.Add(buttonSistema);
                buttonSistema.Click += ButtonSistema_Click;
            }

            clsButton btnAdd = new clsButton("ADD", "Nuovo");
            panelGroup.ButtonsOnTop.Add(btnAdd);
            btnAdd.Click += (o, e) => {
                System.Windows.Forms.MessageBox.Show("nuovo sistema");
            };
        }

        private void ButtonSistema_Click(object sender, EventArgs e)
        {
            this.ResetPanelData();
            this.InitSistema(((clsButton)sender).Id.ToString());
        }

        #region singolo sistema

        private void InitSistema(string codiceSistemaSelected)
        {
            Riepiloghi.clsCodiceSistema CurrentCodiceSistema = null;
            foreach (Riepiloghi.clsCodiceSistema item in this.Account.CodiciSistema)
            {
                if (item.CodiceSistema == codiceSistemaSelected)
                {
                    CurrentCodiceSistema = item;
                    break;
                }
            }

            if (CurrentCodiceSistema != null)
            {
                clsPanelGroup panelSistema = new clsPanelGroup("Sistema");
                panelSistema.Dock = DockStyle.Fill;
                panelSistema.AutoScroll = true;
                this.panelData.Controls.Add(panelSistema);
                panelSistema.BringToFront();

                CheckBox checkCodiceSistema = new CheckBox();
                checkCodiceSistema.Dock = DockStyle.Top;
                checkCodiceSistema.Text = string.Format("{0} {1}", CurrentCodiceSistema.CodiceSistema, CurrentCodiceSistema.Descrizione);
                checkCodiceSistema.AutoCheck = true;
                checkCodiceSistema.Checked = CurrentCodiceSistema.Enabled;
                panelSistema.Controls.Add(checkCodiceSistema);
                checkCodiceSistema.BringToFront();

                checkCodiceSistema.CheckedChanged += (sender, eventArgs) =>
                {
                    CurrentCodiceSistema.Enabled = checkCodiceSistema.Checked;
                };

                clsPanelGroup panelDescrizione = new clsPanelGroup("Descrizione");
                panelSistema.Controls.Add(panelDescrizione);
                panelDescrizione.BringToFront();

                TextBox txtDescrizione = new TextBox();
                txtDescrizione.Dock = DockStyle.Top;
                txtDescrizione.Text = CurrentCodiceSistema.Descrizione;
                panelDescrizione.Controls.Add(txtDescrizione);
                txtDescrizione.BringToFront();

                txtDescrizione.TextChanged += (sender, eventArgs) =>
                {
                    CurrentCodiceSistema.Descrizione = txtDescrizione.Text;
                    checkCodiceSistema.Text = string.Format("{0} {1}", CurrentCodiceSistema.CodiceSistema, CurrentCodiceSistema.Descrizione);
                };


                clsPanelGroup panelStringConn = new clsPanelGroup("Connessione");
                panelSistema.Controls.Add(panelStringConn);
                panelStringConn.BringToFront();

                TextBox txtStringConn = new TextBox();
                txtStringConn.Dock = DockStyle.Top;
                txtStringConn.Text = CurrentCodiceSistema.ConnectionString;
                panelStringConn.Controls.Add(txtStringConn);
                txtStringConn.BringToFront();

                txtStringConn.TextChanged += (sender, eventArgs) =>
                {
                    CurrentCodiceSistema.ConnectionString = txtStringConn.Text;
                };


                clsPanelGroup panelOrganizzatori = new clsPanelGroup("Organizzatori");
                panelSistema.Controls.Add(panelOrganizzatori);
                panelOrganizzatori.BringToFront();

                CheckedListBox listOrganizzatori = new CheckedListBox();
                listOrganizzatori.Dock = DockStyle.Top;
                listOrganizzatori.Text = "Organizzatori";
                listOrganizzatori.CheckOnClick = true;
                panelOrganizzatori.Controls.Add(listOrganizzatori);
                listOrganizzatori.BringToFront();
                
                foreach (Riepiloghi.clsCFOrganizzatore organizzatore in CurrentCodiceSistema.CFOrganizzatori)
                {
                    listOrganizzatori.Items.Add(new clsItem(organizzatore.CFOrganizzatore, organizzatore.Descrizione, true), organizzatore.Enabled);
                }

                listOrganizzatori.SelectedValueChanged += (sender, eventArgs) =>
                {
                    foreach (Riepiloghi.clsCFOrganizzatore organizzatore in CurrentCodiceSistema.CFOrganizzatori)
                    {
                        bool lChecked = false;
                        foreach (clsItem item in listOrganizzatori.CheckedItems)
                        {
                            if (item.Id.ToString() == organizzatore.CFOrganizzatore)
                            {
                                lChecked = true;
                                break;
                            }
                        }
                        organizzatore.Enabled = lChecked;
                    }
                };

                clsPanelGroup panelCodiciLocali = new clsPanelGroup("Codici Locali");
                panelSistema.Controls.Add(panelCodiciLocali);
                panelCodiciLocali.BringToFront();

                CheckedListBox listCodiciLocali = new CheckedListBox();
                listCodiciLocali.Dock = DockStyle.Top;
                listCodiciLocali.Text = "Codici Locali";
                listCodiciLocali.CheckOnClick = true;
                panelCodiciLocali.Controls.Add(listCodiciLocali);
                listCodiciLocali.BringToFront();

                foreach (Riepiloghi.clsCodiceLocale codiceLocale in CurrentCodiceSistema.CodiciLocale)
                {
                    listCodiciLocali.Items.Add(new clsItem(codiceLocale.CodiceLocale, codiceLocale.Descrizione, true), codiceLocale.Enabled);
                }

                listCodiciLocali.SelectedValueChanged += (sender, eventArgs) =>
                {
                    foreach (Riepiloghi.clsCodiceLocale codiceLocale in CurrentCodiceSistema.CodiciLocale)
                    {
                        bool lChecked = false;
                        foreach (clsItem item in listCodiciLocali.CheckedItems)
                        {
                            if (item.Id.ToString() == codiceLocale.CodiceLocale)
                            {
                                lChecked = true;
                                break;
                            }
                        }
                        codiceLocale.Enabled = lChecked;
                    }
                };

                clsButton buttonSave = new clsButton("", "Applica Modifiche a Codice Sistema");
                buttonSave.Dock = DockStyle.Top;
                buttonSave.Click += (sender, eventArgs) =>
                {
                    ResultBaseCodiceSistema responseSave = null;
                    if (responseSave == null || responseSave.StatusOK)
                    {
                        MessageBox.Show((responseSave == null ? "Nessuna risposta." : responseSave.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Salvataggio terminato.", "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.InitAccountLogged();
                    }

                };
                panelSistema.Controls.Add(buttonSave);
                buttonSave.BringToFront();
            }
        }

  
        #endregion

        #endregion

        #region Profili

        private void InitProfili()
        {
            clsPanelGroup panelGroup = new clsPanelGroup("Profili");
            this.panelData.Controls.Add(panelGroup);
            panelGroup.Dock = DockStyle.Fill;
            panelGroup.BringToFront();

            ResultBaseProfileList response = WinAppUtility.GetProfiles(this.TokenLogin.Token);

            foreach (Riepiloghi.clsProfile profilo in response.ProfileList)
            {
                clsButton buttonProfilo = new clsButton(profilo.ProfileId, profilo.Descrizione);
                panelGroup.Controls.Add(buttonProfilo);
                buttonProfilo.Click += ButtonProfilo_Click;
            }

            clsButton btnAdd = new clsButton("ADD", "Nuovo");
            panelGroup.ButtonsOnTop.Add(btnAdd);
            btnAdd.Click += (o, e) =>
            {
                System.Windows.Forms.MessageBox.Show("nuovo profilo");
            };
        }

        private void ButtonProfilo_Click(object sender, EventArgs e)
        {
            this.ResetPanelData();
            this.InitProfilo((long)((clsButton)sender).Id);
        }

        #region Singolo profilo

        private void InitProfilo(long profileId)
        {
            ResultBaseProfile response = WinAppUtility.GetProfile(this.TokenLogin.Token, profileId);
            Riepiloghi.clsProfile profilo = null;

            if (response != null && response.StatusOK)
            {
                profilo = response.Profile;
            }
            else
            {
                MessageBox.Show((response == null ? "Nessuna risposta." : response.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            if (profilo != null)
            {
                Riepiloghi.clsCodiceSistema sistemaProfilo = null;

                clsPanelGroup panelProfilo = new clsPanelGroup("Profilo");
                panelProfilo.Dock = DockStyle.Fill;
                panelProfilo.AutoScroll = true;
                this.panelData.Controls.Add(panelProfilo);
                panelProfilo.BringToFront();

                CheckBox checkProfilo = new CheckBox();
                checkProfilo.Dock = DockStyle.Top;
                checkProfilo.Text = string.Format("{0}", profilo.Descrizione);
                checkProfilo.AutoCheck = true;
                checkProfilo.Checked = profilo.Enabled;
                panelProfilo.Controls.Add(checkProfilo);
                checkProfilo.BringToFront();

                checkProfilo.CheckedChanged += (sender, eventArgs) =>
                {
                    profilo.Enabled = checkProfilo.Checked;
                };

                clsPanelGroup panelDescrizione = new clsPanelGroup("Descrizione");
                panelProfilo.Controls.Add(panelDescrizione);
                panelDescrizione.BringToFront();

                TextBox txtDescrizione = new TextBox();
                txtDescrizione.Dock = DockStyle.Top;
                txtDescrizione.Text = profilo.Descrizione;
                panelDescrizione.Controls.Add(txtDescrizione);
                txtDescrizione.BringToFront();

                txtDescrizione.TextChanged += (sender, eventArgs) =>
                {
                    profilo.Descrizione = txtDescrizione.Text;
                    checkProfilo.Text = string.Format("{0}", profilo.Descrizione);
                };

                CheckBox checkAllSistemi = new CheckBox();
                checkAllSistemi.Dock = DockStyle.Top;
                checkAllSistemi.Text = "Tutti i sistemi";
                checkAllSistemi.AutoCheck = true;
                checkAllSistemi.Checked = profilo.AllCodiciSistema;
                panelProfilo.Controls.Add(checkAllSistemi);
                checkAllSistemi.BringToFront();


                clsPanelGroup panelOperazioni = new clsPanelGroup("Operazioni del profilo");
                panelOperazioni.State = clsPanelGroup.defPanelState.Collapsed;
                panelProfilo.Controls.Add(panelOperazioni);
                panelOperazioni.BringToFront();

                foreach (Riepiloghi.clsCategoriaOperazioniRiepiloghi categoria in profilo.OperazioniProfile)
                {
                    clsPanelGroup panelCategoria = new clsPanelGroup(categoria.Descrizione);
                    panelOperazioni.Controls.Add(panelCategoria);
                    panelCategoria.BringToFront();
                    panelCategoria.State = clsPanelGroup.defPanelState.Expanded;

                    CheckedListBox listaOperazioni = new CheckedListBox();
                    listaOperazioni.Dock = DockStyle.Top;
                    panelCategoria.Controls.Add(listaOperazioni);
                    listaOperazioni.BringToFront();

                    foreach (Riepiloghi.clsOperazioneRiepiloghi operazione in categoria.ListaOperazioni)
                    {
                        listaOperazioni.Items.Add(new clsItem(operazione.IdOperazione, operazione.Descrizione), operazione.Enabled);
                    }
                }

                clsPanelGroup panelMainSistemi = new clsPanelGroup("Sistemi abilitati del profilo");
                panelProfilo.Controls.Add(panelMainSistemi);
                panelMainSistemi.BringToFront();
                panelMainSistemi.State = clsPanelGroup.defPanelState.Collapsed;


                clsPanelGroup panelSistemi = new clsPanelGroup("");
                panelSistemi.Dock = DockStyle.Left;
                panelSistemi.AutoSize = false;
                panelMainSistemi.Controls.Add(panelSistemi);
                panelSistemi.BringToFront();


                Splitter splitSistemiSistema = new Splitter();
                splitSistemiSistema.Dock = DockStyle.Left;
                splitSistemiSistema.BorderStyle = BorderStyle.FixedSingle;
                splitSistemiSistema.BackColor = Color.Red;
                panelMainSistemi.Controls.Add(splitSistemiSistema);
                splitSistemiSistema.BringToFront();

                clsPanelGroup panelSistemaProfilo = new clsPanelGroup("Sistema selezionato");
                panelSistemaProfilo.Dock = DockStyle.Fill;
                panelMainSistemi.Controls.Add(panelSistemaProfilo);
                panelSistemaProfilo.BringToFront();

                panelSistemi.Width = (panelMainSistemi.ClientSize.Width - splitSistemiSistema.Width - panelMainSistemi.Padding.Horizontal) / 2;

                panelMainSistemi.ClientSizeChanged += (sender, eventargs) => 
                {
                    panelSistemi.Width = (panelMainSistemi.ClientSize.Width - splitSistemiSistema.Width - panelMainSistemi.Padding.Horizontal) / 2;
                };

                foreach (Riepiloghi.clsCodiceSistema codiceSistema in profilo.CodiciSistema)
                {
                    clsButton buttonSistema = new clsButton(codiceSistema.CodiceSistema, string.Format("{0} {1}", codiceSistema.CodiceSistema, codiceSistema.Descrizione));
                    buttonSistema.Dock = DockStyle.Top;
                    buttonSistema.State = (codiceSistema.Enabled ? CheckState.Checked : CheckState.Unchecked);
                    panelSistemi.Controls.Add(buttonSistema);
                    buttonSistema.BringToFront();

                    buttonSistema.Selected += (sender, eventArgs) =>
                    {
                        sistemaProfilo = null;
                        foreach (Riepiloghi.clsCodiceSistema itemSistema in profilo.CodiciSistema)
                        {
                            if (itemSistema.CodiceSistema == ((clsButton)sender).Id.ToString())
                            {
                                sistemaProfilo = itemSistema;
                                RemoveControlsInside(panelSistemaProfilo);

                                CheckBox checkAllOrganizzatori = new CheckBox();
                                checkAllOrganizzatori.Dock = DockStyle.Top;
                                checkAllOrganizzatori.Text = "Tutti gli organizzatori";
                                checkAllOrganizzatori.AutoCheck = true;
                                checkAllOrganizzatori.Checked = sistemaProfilo.AllCFOrganizzatori;
                                panelSistemaProfilo.Controls.Add(checkAllOrganizzatori);
                                checkAllOrganizzatori.BringToFront();

                                clsPanelGroup pnlLstOrganizzatori = new clsPanelGroup("Organizzatori abilitati");
                                pnlLstOrganizzatori.Dock = DockStyle.Top;
                                panelSistemaProfilo.Controls.Add(pnlLstOrganizzatori);
                                pnlLstOrganizzatori.BringToFront();
                                pnlLstOrganizzatori.State = clsPanelGroup.defPanelState.Collapsed;

                                CheckedListBox lstOrganizzatoriSistema = new CheckedListBox();
                                lstOrganizzatoriSistema.Dock = DockStyle.Top;
                                pnlLstOrganizzatori.Controls.Add(lstOrganizzatoriSistema);
                                lstOrganizzatoriSistema.BringToFront();

                                foreach (Riepiloghi.clsCFOrganizzatore organizzatore in sistemaProfilo.CFOrganizzatori)
                                {
                                    lstOrganizzatoriSistema.Items.Add(new clsItem(organizzatore.CFOrganizzatore, organizzatore.Descrizione));
                                }

                                lstOrganizzatoriSistema.SelectedValueChanged += (o, e) =>
                                {
                                    foreach (Riepiloghi.clsCFOrganizzatore organizzatore in sistemaProfilo.CFOrganizzatori)
                                    {
                                        bool lChecked = false;
                                        foreach (clsItem item in lstOrganizzatoriSistema.CheckedItems)
                                        {
                                            if (item.Id.ToString() == organizzatore.CFOrganizzatore)
                                            {
                                                lChecked = true;
                                                break;
                                            }
                                        }
                                        organizzatore.Enabled = lChecked;
                                    }
                                };

                                CheckBox checkAllCodiciLocali = new CheckBox();
                                checkAllCodiciLocali.Dock = DockStyle.Top;
                                checkAllCodiciLocali.Text = "Tutti i codici locali";
                                checkAllCodiciLocali.AutoCheck = true;
                                checkAllCodiciLocali.Checked = sistemaProfilo.AllCFOrganizzatori;
                                panelSistemaProfilo.Controls.Add(checkAllCodiciLocali);
                                checkAllCodiciLocali.BringToFront();

                                clsPanelGroup pnlLstCodiciLocali = new clsPanelGroup("Codici Locali abilitati");
                                pnlLstCodiciLocali.Dock = DockStyle.Top;
                                panelSistemaProfilo.Controls.Add(pnlLstCodiciLocali);
                                pnlLstCodiciLocali.BringToFront();
                                pnlLstCodiciLocali.State = clsPanelGroup.defPanelState.Collapsed;

                                CheckedListBox lstCodiciLocali = new CheckedListBox();
                                lstCodiciLocali.Dock = DockStyle.Top;
                                pnlLstCodiciLocali.Controls.Add(lstCodiciLocali);
                                lstCodiciLocali.BringToFront();

                                foreach (Riepiloghi.clsCodiceLocale codiceLocale in sistemaProfilo.CodiciLocale)
                                {
                                    lstCodiciLocali.Items.Add(new clsItem(codiceLocale.CodiceLocale, codiceLocale.Descrizione));
                                }

                                lstCodiciLocali.SelectedValueChanged += (o, e) =>
                                {
                                    foreach (Riepiloghi.clsCodiceLocale codiceLocale in sistemaProfilo.CodiciLocale)
                                    {
                                        bool lChecked = false;
                                        foreach (clsItem item in lstCodiciLocali.CheckedItems)
                                        {
                                            if (item.Id.ToString() == codiceLocale.CodiceLocale)
                                            {
                                                lChecked = true;
                                                break;
                                            }
                                        }
                                        codiceLocale.Enabled = lChecked;
                                    }
                                };

                                pnlLstOrganizzatori.Enabled = !sistemaProfilo.AllCFOrganizzatori;
                                checkAllOrganizzatori.CheckedChanged += (o, e) =>
                                {
                                    sistemaProfilo.AllCFOrganizzatori = checkAllOrganizzatori.Checked;
                                    pnlLstOrganizzatori.Enabled = !sistemaProfilo.AllCFOrganizzatori;
                                };

                                pnlLstCodiciLocali.Enabled = !sistemaProfilo.AllCodiciLocali;
                                checkAllCodiciLocali.CheckedChanged += (o, e) =>
                                {
                                    sistemaProfilo.AllCodiciLocali = checkAllCodiciLocali.Checked;
                                    pnlLstCodiciLocali.Enabled = !sistemaProfilo.AllCodiciLocali;
                                };



                                break;
                            }
                        }
                    };

                    if (sistemaProfilo != null)
                        panelSistemaProfilo.Enabled = sistemaProfilo.Enabled;

                    buttonSistema.Checked += (sender, eventArgs) =>
                    {
                        sistemaProfilo.Enabled = !sistemaProfilo.Enabled;
                        CheckState state = (sistemaProfilo.Enabled ? CheckState.Checked : CheckState.Unchecked);
                        ((clsButton)sender).State = state;
                        panelSistemaProfilo.Enabled = sistemaProfilo.Enabled;
                    };

                    clsButton btnSaveProfile = new clsButton("", "Applica Modifiche Profilo");
                    btnSaveProfile.Dock = DockStyle.Top;
                    panelProfilo.Controls.Add(btnSaveProfile);
                    btnSaveProfile.BringToFront();

                    btnSaveProfile.Click += (o, e) =>
                    {
                        ResultBaseProfile responseSave = null;
                        if (responseSave == null || responseSave.StatusOK)
                        {
                            MessageBox.Show((responseSave == null ? "Nessuna risposta." : responseSave.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show("Salvataggio terminato.", "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.InitAccountLogged();
                        }
                    };


                }
            }

        }

        #endregion

        #endregion

        #region Accounts

        private void InitAccounts()
        {
            clsPanelGroup panelGroup = new clsPanelGroup("Accounts");
            this.panelData.Controls.Add(panelGroup);
            panelGroup.Dock = DockStyle.Fill;
            panelGroup.BringToFront();

            ResultBaseAccountList response = WinAppUtility.GetListaAccounts(this.TokenLogin.Token);

            foreach (Riepiloghi.clsAccount account in response.AccountList)
            {
                clsButton buttonAccount = new clsButton(account.AccountId, string.Format("{0} {1} {2}", account.Cognome, account.Nome, account.Email));
                panelGroup.Controls.Add(buttonAccount);
                buttonAccount.Click += ButtonAccount_Click;
            }

            clsButton btnAdd = new clsButton("ADD", "Nuovo");
            panelGroup.ButtonsOnTop.Add(btnAdd);
            btnAdd.Click += (o, e) =>
            {
                System.Windows.Forms.MessageBox.Show("nuovo account");
            };
        }

        private void ButtonAccount_Click(object sender, EventArgs e)
        {
            this.ResetPanelData();
            this.InitSingoloAcount(((clsButton)sender).Id.ToString());
        }

        #region singolo Account

        private void InitSingoloAcount(string accountId)
        {
            Riepiloghi.clsAccount account = null;
            Riepiloghi.clsProfile accountProfile = null;

            ResultBaseAccount response = WinAppUtility.GetAccount(this.TokenLogin.Token, accountId);

            if (response != null && response.StatusOK)
            {
                account = response.Account;
            }
            else
            {
                MessageBox.Show((response == null ? "Nessuna risposta" : response.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (account != null)
            {
                ResultBaseProfile responseProfileAccount = WinAppUtility.GetProfile(this.TokenLogin.Token, account.ProfileId);
                if (responseProfileAccount != null && responseProfileAccount.Profile != null)
                    accountProfile = responseProfileAccount.Profile;
                else
                    MessageBox.Show((responseProfileAccount == null ? "Nessuna risposta" : responseProfileAccount.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (account != null && accountProfile != null)
            {

                clsPanelGroup panelAccount = new clsPanelGroup("Account");
                panelAccount.Dock = DockStyle.Fill;
                panelAccount.AutoScroll = true;
                this.panelData.Controls.Add(panelAccount);
                panelAccount.BringToFront();

                Label lblAccountProfile = new Label();
                lblAccountProfile.Text = string.Format("Profilo di appartenenza: {0}", accountProfile.Descrizione);
                lblAccountProfile.Dock = DockStyle.Top;
                panelAccount.Controls.Add(lblAccountProfile);
                lblAccountProfile.BringToFront();

                CheckBox checkAccount = new CheckBox();
                checkAccount.Dock = DockStyle.Top;
                checkAccount.Text = string.Format("{0} {1} {2}", account.Cognome, account.Nome, account.Email);
                checkAccount.AutoCheck = true;
                checkAccount.Checked = account.Enabled;
                panelAccount.Controls.Add(checkAccount);
                checkAccount.BringToFront();

                checkAccount.CheckedChanged += (sender, eventArgs) =>
                {
                    account.Enabled = checkAccount.Checked;
                };

                clsPanelGroup panelDatiAccount = new clsPanelGroup("");
                panelAccount.Controls.Add(panelDatiAccount);
                panelDatiAccount.BringToFront();

                Label lblCognome = new Label();
                lblCognome.Dock = DockStyle.Top;
                lblCognome.Text = "Cognome:";
                panelDatiAccount.Controls.Add(lblCognome);
                lblCognome.BringToFront();

                TextBox txtCognome = new TextBox();
                txtCognome.Dock = DockStyle.Top;
                txtCognome.Text = account.Cognome;
                panelDatiAccount.Controls.Add(txtCognome);
                txtCognome.BringToFront();

                txtCognome.TextChanged += (sender, eventArgs) =>
                {
                    account.Cognome = txtCognome.Text;
                    checkAccount.Text = string.Format("{0} {1} {2}", account.Cognome, account.Nome, account.Email);
                };

                Label lblNome = new Label();
                lblNome.Dock = DockStyle.Top;
                lblNome.Text = "Nome:";
                panelDatiAccount.Controls.Add(lblNome);
                lblNome.BringToFront();

                TextBox txtNome = new TextBox();
                txtNome.Dock = DockStyle.Top;
                txtNome.Text = account.Nome;
                panelDatiAccount.Controls.Add(txtNome);
                txtNome.BringToFront();

                txtNome.TextChanged += (sender, eventArgs) =>
                {
                    account.Nome = txtNome.Text;
                    checkAccount.Text = string.Format("{0} {1} {2}", account.Cognome, account.Nome, account.Email);
                };

                Label lblEmail = new Label();
                lblEmail.Dock = DockStyle.Top;
                lblEmail.Text = "email:";
                panelDatiAccount.Controls.Add(lblEmail);
                lblEmail.BringToFront();

                TextBox txtEmail = new TextBox();
                txtEmail.Dock = DockStyle.Top;
                txtEmail.Text = account.Email;
                panelDatiAccount.Controls.Add(txtEmail);
                txtEmail.BringToFront();

                txtEmail.TextChanged += (sender, eventArgs) =>
                {
                    account.Email = txtEmail.Text;
                    checkAccount.Text = string.Format("{0} {1} {2}", account.Cognome, account.Nome, account.Email);
                };

                clsButton btnRenewPassword = new clsButton("", "Recupera Password");
                btnRenewPassword.Dock = DockStyle.Top;
                panelAccount.Controls.Add(btnRenewPassword);
                btnRenewPassword.BringToFront();

                btnRenewPassword.Click += (o, e) =>
                {
                    ResultBaseAccount responseRenew = WinAppUtility.RenewAccountPassword(txtEmail.Text);
                    if (responseRenew != null)
                    {
                        if (!responseRenew.StatusOK)
                        {
                            MessageBox.Show(responseRenew.StatusMessage, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                };

                clsButton btnSaveAccount = new clsButton("", "Applica Modifiche Account");
                btnSaveAccount.Dock = DockStyle.Top;
                panelAccount.Controls.Add(btnSaveAccount);
                btnSaveAccount.BringToFront();

                btnSaveAccount.Click += (o, e) =>
                {
                    ResultBaseAccount responseSave = null;
                    if (responseSave == null || responseSave.StatusOK)
                    {
                        MessageBox.Show((responseSave == null ? "Nessuna risposta." : responseSave.StatusMessage), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Salvataggio terminato.", "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.InitAccountLogged();
                    }
                };
            }
        }

        #endregion

        #endregion
    }
}
