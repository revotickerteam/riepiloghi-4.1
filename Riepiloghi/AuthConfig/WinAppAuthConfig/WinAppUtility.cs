﻿using ApiRiepiloghi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ApiRiepiloghi.ServiceRiepiloghi;

namespace WinAppAuthConfig
{
    public static class WinAppUtility
    {


        public static clsEnvironment GetEnvironment()
        {
            clsEnvironment result = new clsEnvironment();
            result.InternalConnectionString = WinAppAuthConfig.Properties.Settings.Default.internalConnectionString;
            result.ExternalConnectionString = WinAppAuthConfig.Properties.Settings.Default.externalConnectionString;
            return result;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnectionInternal(out Exception oError, clsEnvironment oEnvironment)
        {
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                oCon.ConnectionString = oEnvironment.InternalConnectionString;
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
            }
            return oCon;
        }

        public static Wintic.Data.oracle.CConnectionOracle GetConnectionInternal(out Exception oError)
        {
            clsEnvironment oEnvironment = GetEnvironment();
            Wintic.Data.oracle.CConnectionOracle oCon = null;
            oError = null;
            try
            {
                oCon = new Wintic.Data.oracle.CConnectionOracle();
                oCon.ConnectionString = oEnvironment.InternalConnectionString;
                oCon.Open();
                if (oCon.State != System.Data.ConnectionState.Open)
                {
                    oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore apertura della connessione.");
                }
            }
            catch (Exception exGetConnection)
            {
                oError = Riepiloghi.clsRiepiloghi.GetNewException("Connessione", "Errore generico durante la connessione." + exGetConnection.ToString(), exGetConnection);
            }
            return oCon;
        }

        public static ResultBaseToken CheckTokenLogin(ResultBaseToken token, ref ResultBaseAccount account)
        {
            ResultBaseAccount backupAccount = account;
            account = null;
            ResultBaseToken result = null;
            if (token != null)
            {
                ResponseRiepiloghi response = ApiRiepiloghi.ServiceRiepiloghi.CheckToken(token.Token, GetEnvironment());
                if (response.Status.StatusOK)
                {
                    result = token;
                    if (backupAccount != null)
                        account = backupAccount;
                }
            }

            if (result == null)
            {
                frmLogin oFrmLogin = new frmLogin();
                bool loginDialog = oFrmLogin.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                if (loginDialog)
                    result = oFrmLogin.TokenLogin;
            }
            if (result != null && backupAccount == null)
            {
                account = ApiRiepiloghi.ServiceRiepiloghi.GetAccount(result.Token, "", GetEnvironment());
            }
            return result;
        }

        public static List<string> GetListaOperazioniConfig()
        {
            return new List<string>() { "RIEPILOGHI_PROFILES", "RIEPILOGHI_ACCOUNTS", "CONFIG_SISTEMI", "CONF_BORDERO" };
        }

        public static ResultBaseProfileList GetProfiles(string tokenString)
        {
            return ApiRiepiloghi.ServiceRiepiloghi.GetProfileList(tokenString, GetEnvironment());
        }

        public static ResultBaseProfile GetProfile(string tokenString, long profileId)
        {
            return ApiRiepiloghi.ServiceRiepiloghi.GetProfile(tokenString, profileId, GetEnvironment());
        }

        public static ResultBaseAccountList GetListaAccounts(string tokenString)
        {
            return ApiRiepiloghi.ServiceRiepiloghi.GetAccountList(tokenString, GetEnvironment());
        }

        public static ResultBaseAccount GetAccount(string tokenString, string accountId)
        {
            return ApiRiepiloghi.ServiceRiepiloghi.GetAccount(tokenString, accountId, GetEnvironment());
        }

        public static ResultBaseAccount RenewAccountPassword(string Email)
        {
            string url = WinAppAuthConfig.Properties.Settings.Default.urlRenewPassword;
            return ApiRiepiloghi.ServiceRiepiloghi.RenewAccountPassword(Email, url, GetEnvironment());
        }
    }
}
