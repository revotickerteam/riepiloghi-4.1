﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAppAuthConfig
{
    public partial class frmLogin : frmBaseAuth
    {
        public ApiRiepiloghi.ServiceRiepiloghi.ResultBaseToken TokenLogin { get; set; }
        public frmLogin()
        {
            InitializeComponent();
            this.btnOK.Click += BtnOK_Click;
            this.btnAbort.Click += BtnAbort_Click;
            this.txtLogin.TextChanged += TxtLoginPassword_TextChanged;
            this.txtPassword.TextChanged += TxtLoginPassword_TextChanged;
        }

        private void TxtLoginPassword_TextChanged(object sender, EventArgs e)
        {
            this.CheckOkEnabled();
        }

        private void BtnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.TokenLogin = ApiRiepiloghi.ServiceRiepiloghi.GetToken(this.txtLogin.Text, this.txtPassword.Text, WinAppUtility.GetEnvironment());
            if (this.TokenLogin != null && this.TokenLogin.StatusOK && !string.IsNullOrEmpty(this.TokenLogin.Token))
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.lblMessage.Text = (this.TokenLogin == null ? "Nessun token" : this.TokenLogin.StatusMessage);
            }
        }

        private void CheckOkEnabled()
        {
            this.btnOK.Enabled = !string.IsNullOrEmpty(this.txtLogin.Text) && !string.IsNullOrEmpty(this.txtPassword.Text);
        }
    }
}
