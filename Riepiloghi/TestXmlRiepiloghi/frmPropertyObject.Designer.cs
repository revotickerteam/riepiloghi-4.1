﻿namespace TestXmlRiepiloghi
{
    partial class frmPropertyObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.panelObject = new System.Windows.Forms.Panel();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.btnAbort = new System.Windows.Forms.Button();
            this.propertyGridObject = new System.Windows.Forms.PropertyGrid();
            this.panelTitle.SuspendLayout();
            this.panelObject.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(513, 41);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "...";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTitle
            // 
            this.panelTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTitle.Controls.Add(this.labelTitle);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(515, 43);
            this.panelTitle.TabIndex = 1;
            // 
            // panelObject
            // 
            this.panelObject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelObject.Controls.Add(this.propertyGridObject);
            this.panelObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelObject.Location = new System.Drawing.Point(0, 43);
            this.panelObject.Name = "panelObject";
            this.panelObject.Size = new System.Drawing.Size(515, 371);
            this.panelObject.TabIndex = 2;
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBottom.Controls.Add(this.btnAbort);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 414);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Padding = new System.Windows.Forms.Padding(3);
            this.panelBottom.Size = new System.Drawing.Size(515, 59);
            this.panelBottom.TabIndex = 3;
            // 
            // btnAbort
            // 
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(3, 3);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(75, 51);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Chiudi";
            this.btnAbort.UseVisualStyleBackColor = true;
            // 
            // propertyGridObject
            // 
            this.propertyGridObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridObject.Location = new System.Drawing.Point(0, 0);
            this.propertyGridObject.Name = "propertyGridObject";
            this.propertyGridObject.Size = new System.Drawing.Size(513, 369);
            this.propertyGridObject.TabIndex = 0;
            // 
            // frmPropertyObject
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(515, 473);
            this.Controls.Add(this.panelObject);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelTitle);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmPropertyObject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPropertyObject";
            this.panelTitle.ResumeLayout(false);
            this.panelObject.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Panel panelObject;
        private System.Windows.Forms.PropertyGrid propertyGridObject;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button btnAbort;
    }
}