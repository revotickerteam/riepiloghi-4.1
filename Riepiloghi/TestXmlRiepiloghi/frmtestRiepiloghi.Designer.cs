﻿namespace TestXmlRiepiloghi
{
    partial class frmtestRiepiloghi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlDati = new System.Windows.Forms.Panel();
            this.dgvDati = new System.Windows.Forms.DataGridView();
            this.pnlServizio = new System.Windows.Forms.Panel();
            this.grpFunzioni = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtRegexForm = new System.Windows.Forms.TextBox();
            this.lblRegexRES = new System.Windows.Forms.Label();
            this.lblRegexForm = new System.Windows.Forms.Label();
            this.txtRegexExpr = new System.Windows.Forms.TextBox();
            this.lblRegexExpr = new System.Windows.Forms.Label();
            this.btnFunzione = new System.Windows.Forms.Button();
            this.grpFnzEvento = new System.Windows.Forms.GroupBox();
            this.cmbVrfSale = new System.Windows.Forms.ComboBox();
            this.lblVrfCodiceLocale = new System.Windows.Forms.Label();
            this.chkPerCalcoloOmaggio = new System.Windows.Forms.CheckBox();
            this.chkRicercaOmaggitransazione = new System.Windows.Forms.CheckBox();
            this.txtIdCalend = new System.Windows.Forms.TextBox();
            this.lblIdcalend = new System.Windows.Forms.Label();
            this.dtEvento = new System.Windows.Forms.DateTimePicker();
            this.lblDtEvento = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTitolo = new System.Windows.Forms.ComboBox();
            this.dtDataEmi = new System.Windows.Forms.DateTimePicker();
            this.lblDataEmi = new System.Windows.Forms.Label();
            this.numSuppl = new System.Windows.Forms.NumericUpDown();
            this.lblSuppl = new System.Windows.Forms.Label();
            this.numPrezzo = new System.Windows.Forms.NumericUpDown();
            this.lblPrezzo = new System.Windows.Forms.Label();
            this.lblTipoTitolo = new System.Windows.Forms.Label();
            this.cmbTipoTitolo = new System.Windows.Forms.ComboBox();
            this.cmbOrdineDiPosto = new System.Windows.Forms.ComboBox();
            this.lblOrdineDiPosto = new System.Windows.Forms.Label();
            this.lblTipoEvento = new System.Windows.Forms.Label();
            this.cmbTipoEvento = new System.Windows.Forms.ComboBox();
            this.numIncidenza = new System.Windows.Forms.NumericUpDown();
            this.lblIncidenza = new System.Windows.Forms.Label();
            this.radioIntrattenimento = new System.Windows.Forms.RadioButton();
            this.radioSpettacolo = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkBiglietti = new System.Windows.Forms.CheckBox();
            this.chkAbbonamenti = new System.Windows.Forms.CheckBox();
            this.grpPeriodo = new System.Windows.Forms.GroupBox();
            this.dtFnzInizio = new System.Windows.Forms.DateTimePicker();
            this.lblFnzInizio = new System.Windows.Forms.Label();
            this.dtFnzFine = new System.Windows.Forms.DateTimePicker();
            this.lblFnzFine = new System.Windows.Forms.Label();
            this.grpTipoRiepilogo = new System.Windows.Forms.GroupBox();
            this.numProgressivo = new System.Windows.Forms.NumericUpDown();
            this.lblNumProg = new System.Windows.Forms.Label();
            this.radioChkRPM = new System.Windows.Forms.RadioButton();
            this.radioChkRPG = new System.Windows.Forms.RadioButton();
            this.radioChkSIM = new System.Windows.Forms.RadioButton();
            this.lblHelpFunzione = new System.Windows.Forms.Label();
            this.lblFunzione = new System.Windows.Forms.Label();
            this.cmbFunzione = new System.Windows.Forms.ComboBox();
            this.grpRicerca = new System.Windows.Forms.GroupBox();
            this.btnAnnullo = new System.Windows.Forms.Button();
            this.lblOperazione = new System.Windows.Forms.Label();
            this.btnCerca = new System.Windows.Forms.Button();
            this.txtSigillo = new System.Windows.Forms.TextBox();
            this.lblCarta = new System.Windows.Forms.Label();
            this.lblSigillo = new System.Windows.Forms.Label();
            this.txtCarta = new System.Windows.Forms.TextBox();
            this.lblProgressivo = new System.Windows.Forms.Label();
            this.txtProggressivo = new System.Windows.Forms.TextBox();
            this.grpTransazioni = new System.Windows.Forms.GroupBox();
            this.grpFnzModStorico = new System.Windows.Forms.GroupBox();
            this.radioStoricoNO = new System.Windows.Forms.RadioButton();
            this.radioStoricoSI = new System.Windows.Forms.RadioButton();
            this.radioStoricoCHECK = new System.Windows.Forms.RadioButton();
            this.btnFiltri = new System.Windows.Forms.Button();
            this.radioAnnulli = new System.Windows.Forms.RadioButton();
            this.radioTransazioni = new System.Windows.Forms.RadioButton();
            this.lblFine = new System.Windows.Forms.Label();
            this.dtFine = new System.Windows.Forms.DateTimePicker();
            this.lblInizio = new System.Windows.Forms.Label();
            this.dtInizio = new System.Windows.Forms.DateTimePicker();
            this.btnLoad = new System.Windows.Forms.Button();
            this.grpRiepilogo = new System.Windows.Forms.GroupBox();
            this.radioRCA = new System.Windows.Forms.RadioButton();
            this.btnCheckServerFiscale = new System.Windows.Forms.Button();
            this.btnMasterizza = new System.Windows.Forms.Button();
            this.chkListSale = new System.Windows.Forms.CheckedListBox();
            this.lblSalaSimulazione = new System.Windows.Forms.Label();
            this.chkDettagli = new System.Windows.Forms.CheckBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.lblMsgRP = new System.Windows.Forms.Label();
            this.btnGenera = new System.Windows.Forms.Button();
            this.radioSIM = new System.Windows.Forms.RadioButton();
            this.radioRPG = new System.Windows.Forms.RadioButton();
            this.lblGiorno = new System.Windows.Forms.Label();
            this.dtGiorno = new System.Windows.Forms.DateTimePicker();
            this.radioRPM = new System.Windows.Forms.RadioButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlDati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDati)).BeginInit();
            this.pnlServizio.SuspendLayout();
            this.grpFunzioni.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpFnzEvento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSuppl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrezzo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncidenza)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.grpPeriodo.SuspendLayout();
            this.grpTipoRiepilogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProgressivo)).BeginInit();
            this.grpRicerca.SuspendLayout();
            this.grpTransazioni.SuspendLayout();
            this.grpFnzModStorico.SuspendLayout();
            this.grpRiepilogo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDati
            // 
            this.pnlDati.Controls.Add(this.dgvDati);
            this.pnlDati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDati.Location = new System.Drawing.Point(0, 690);
            this.pnlDati.Name = "pnlDati";
            this.pnlDati.Size = new System.Drawing.Size(939, 54);
            this.pnlDati.TabIndex = 6;
            // 
            // dgvDati
            // 
            this.dgvDati.AllowUserToAddRows = false;
            this.dgvDati.AllowUserToDeleteRows = false;
            this.dgvDati.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvDati.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDati.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvDati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDati.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDati.Location = new System.Drawing.Point(0, 0);
            this.dgvDati.MultiSelect = false;
            this.dgvDati.Name = "dgvDati";
            this.dgvDati.ReadOnly = true;
            this.dgvDati.RowHeadersVisible = false;
            this.dgvDati.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvDati.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvDati.Size = new System.Drawing.Size(939, 54);
            this.dgvDati.TabIndex = 0;
            // 
            // pnlServizio
            // 
            this.pnlServizio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlServizio.Controls.Add(this.grpFunzioni);
            this.pnlServizio.Controls.Add(this.grpRicerca);
            this.pnlServizio.Controls.Add(this.grpTransazioni);
            this.pnlServizio.Controls.Add(this.grpRiepilogo);
            this.pnlServizio.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServizio.Location = new System.Drawing.Point(0, 0);
            this.pnlServizio.Name = "pnlServizio";
            this.pnlServizio.Padding = new System.Windows.Forms.Padding(4);
            this.pnlServizio.Size = new System.Drawing.Size(939, 680);
            this.pnlServizio.TabIndex = 0;
            // 
            // grpFunzioni
            // 
            this.grpFunzioni.BackColor = System.Drawing.Color.Gainsboro;
            this.grpFunzioni.Controls.Add(this.panel1);
            this.grpFunzioni.Controls.Add(this.btnFunzione);
            this.grpFunzioni.Controls.Add(this.grpFnzEvento);
            this.grpFunzioni.Controls.Add(this.groupBox2);
            this.grpFunzioni.Controls.Add(this.grpPeriodo);
            this.grpFunzioni.Controls.Add(this.grpTipoRiepilogo);
            this.grpFunzioni.Controls.Add(this.lblHelpFunzione);
            this.grpFunzioni.Controls.Add(this.lblFunzione);
            this.grpFunzioni.Controls.Add(this.cmbFunzione);
            this.grpFunzioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpFunzioni.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFunzioni.ForeColor = System.Drawing.Color.Black;
            this.grpFunzioni.Location = new System.Drawing.Point(4, 315);
            this.grpFunzioni.Name = "grpFunzioni";
            this.grpFunzioni.Size = new System.Drawing.Size(929, 345);
            this.grpFunzioni.TabIndex = 3;
            this.grpFunzioni.TabStop = false;
            this.grpFunzioni.Text = "Funzioni di calcolo";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtRegexForm);
            this.panel1.Controls.Add(this.lblRegexRES);
            this.panel1.Controls.Add(this.lblRegexForm);
            this.panel1.Controls.Add(this.txtRegexExpr);
            this.panel1.Controls.Add(this.lblRegexExpr);
            this.panel1.Location = new System.Drawing.Point(7, 308);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(909, 33);
            this.panel1.TabIndex = 8;
            // 
            // txtRegexForm
            // 
            this.txtRegexForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRegexForm.Location = new System.Drawing.Point(452, 4);
            this.txtRegexForm.Name = "txtRegexForm";
            this.txtRegexForm.Size = new System.Drawing.Size(390, 21);
            this.txtRegexForm.TabIndex = 3;
            this.txtRegexForm.TextChanged += new System.EventHandler(this.txtRegexForm_TextChanged);
            // 
            // lblRegexRES
            // 
            this.lblRegexRES.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblRegexRES.Location = new System.Drawing.Point(842, 4);
            this.lblRegexRES.Name = "lblRegexRES";
            this.lblRegexRES.Size = new System.Drawing.Size(61, 23);
            this.lblRegexRES.TabIndex = 4;
            this.lblRegexRES.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRegexForm
            // 
            this.lblRegexForm.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRegexForm.Location = new System.Drawing.Point(392, 4);
            this.lblRegexForm.Name = "lblRegexForm";
            this.lblRegexForm.Size = new System.Drawing.Size(60, 23);
            this.lblRegexForm.TabIndex = 2;
            this.lblRegexForm.Text = "Formula:";
            this.lblRegexForm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRegexExpr
            // 
            this.txtRegexExpr.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtRegexExpr.Location = new System.Drawing.Point(202, 4);
            this.txtRegexExpr.Name = "txtRegexExpr";
            this.txtRegexExpr.Size = new System.Drawing.Size(190, 21);
            this.txtRegexExpr.TabIndex = 1;
            this.txtRegexExpr.TextChanged += new System.EventHandler(this.txtRegexExpr_TextChanged);
            // 
            // lblRegexExpr
            // 
            this.lblRegexExpr.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRegexExpr.Location = new System.Drawing.Point(4, 4);
            this.lblRegexExpr.Name = "lblRegexExpr";
            this.lblRegexExpr.Size = new System.Drawing.Size(198, 23);
            this.lblRegexExpr.TabIndex = 0;
            this.lblRegexExpr.Text = "Test Espressioni Regolari. Espressione:";
            this.lblRegexExpr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnFunzione
            // 
            this.btnFunzione.Location = new System.Drawing.Point(831, 17);
            this.btnFunzione.Name = "btnFunzione";
            this.btnFunzione.Size = new System.Drawing.Size(85, 25);
            this.btnFunzione.TabIndex = 7;
            this.btnFunzione.Text = "esegui";
            this.btnFunzione.UseVisualStyleBackColor = true;
            // 
            // grpFnzEvento
            // 
            this.grpFnzEvento.Controls.Add(this.cmbVrfSale);
            this.grpFnzEvento.Controls.Add(this.lblVrfCodiceLocale);
            this.grpFnzEvento.Controls.Add(this.chkPerCalcoloOmaggio);
            this.grpFnzEvento.Controls.Add(this.chkRicercaOmaggitransazione);
            this.grpFnzEvento.Controls.Add(this.txtIdCalend);
            this.grpFnzEvento.Controls.Add(this.lblIdcalend);
            this.grpFnzEvento.Controls.Add(this.dtEvento);
            this.grpFnzEvento.Controls.Add(this.lblDtEvento);
            this.grpFnzEvento.Controls.Add(this.label8);
            this.grpFnzEvento.Controls.Add(this.cmbTitolo);
            this.grpFnzEvento.Controls.Add(this.dtDataEmi);
            this.grpFnzEvento.Controls.Add(this.lblDataEmi);
            this.grpFnzEvento.Controls.Add(this.numSuppl);
            this.grpFnzEvento.Controls.Add(this.lblSuppl);
            this.grpFnzEvento.Controls.Add(this.numPrezzo);
            this.grpFnzEvento.Controls.Add(this.lblPrezzo);
            this.grpFnzEvento.Controls.Add(this.lblTipoTitolo);
            this.grpFnzEvento.Controls.Add(this.cmbTipoTitolo);
            this.grpFnzEvento.Controls.Add(this.cmbOrdineDiPosto);
            this.grpFnzEvento.Controls.Add(this.lblOrdineDiPosto);
            this.grpFnzEvento.Controls.Add(this.lblTipoEvento);
            this.grpFnzEvento.Controls.Add(this.cmbTipoEvento);
            this.grpFnzEvento.Controls.Add(this.numIncidenza);
            this.grpFnzEvento.Controls.Add(this.lblIncidenza);
            this.grpFnzEvento.Controls.Add(this.radioIntrattenimento);
            this.grpFnzEvento.Controls.Add(this.radioSpettacolo);
            this.grpFnzEvento.Location = new System.Drawing.Point(245, 101);
            this.grpFnzEvento.Name = "grpFnzEvento";
            this.grpFnzEvento.Size = new System.Drawing.Size(671, 209);
            this.grpFnzEvento.TabIndex = 6;
            this.grpFnzEvento.TabStop = false;
            this.grpFnzEvento.Text = "Dati Evento";
            // 
            // cmbVrfSale
            // 
            this.cmbVrfSale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVrfSale.FormattingEnabled = true;
            this.cmbVrfSale.Location = new System.Drawing.Point(388, 42);
            this.cmbVrfSale.Name = "cmbVrfSale";
            this.cmbVrfSale.Size = new System.Drawing.Size(250, 21);
            this.cmbVrfSale.TabIndex = 29;
            // 
            // lblVrfCodiceLocale
            // 
            this.lblVrfCodiceLocale.AutoSize = true;
            this.lblVrfCodiceLocale.Location = new System.Drawing.Point(386, 26);
            this.lblVrfCodiceLocale.Name = "lblVrfCodiceLocale";
            this.lblVrfCodiceLocale.Size = new System.Drawing.Size(93, 13);
            this.lblVrfCodiceLocale.TabIndex = 28;
            this.lblVrfCodiceLocale.Text = "Sala/Codice locale";
            // 
            // chkPerCalcoloOmaggio
            // 
            this.chkPerCalcoloOmaggio.AutoSize = true;
            this.chkPerCalcoloOmaggio.Location = new System.Drawing.Point(324, 156);
            this.chkPerCalcoloOmaggio.Name = "chkPerCalcoloOmaggio";
            this.chkPerCalcoloOmaggio.Size = new System.Drawing.Size(187, 17);
            this.chkPerCalcoloOmaggio.TabIndex = 26;
            this.chkPerCalcoloOmaggio.Text = "valori indicati per calcolo Omaggio";
            this.chkPerCalcoloOmaggio.UseVisualStyleBackColor = true;
            // 
            // chkRicercaOmaggitransazione
            // 
            this.chkRicercaOmaggitransazione.AutoSize = true;
            this.chkRicercaOmaggitransazione.Location = new System.Drawing.Point(324, 180);
            this.chkRicercaOmaggitransazione.Name = "chkRicercaOmaggitransazione";
            this.chkRicercaOmaggitransazione.Size = new System.Drawing.Size(157, 17);
            this.chkRicercaOmaggitransazione.TabIndex = 27;
            this.chkRicercaOmaggitransazione.Text = "Ricerca per omaggio nei log";
            this.chkRicercaOmaggitransazione.UseVisualStyleBackColor = true;
            // 
            // txtIdCalend
            // 
            this.txtIdCalend.Location = new System.Drawing.Point(296, 43);
            this.txtIdCalend.Name = "txtIdCalend";
            this.txtIdCalend.Size = new System.Drawing.Size(84, 21);
            this.txtIdCalend.TabIndex = 7;
            // 
            // lblIdcalend
            // 
            this.lblIdcalend.AutoSize = true;
            this.lblIdcalend.Location = new System.Drawing.Point(240, 47);
            this.lblIdcalend.Name = "lblIdcalend";
            this.lblIdcalend.Size = new System.Drawing.Size(50, 13);
            this.lblIdcalend.TabIndex = 6;
            this.lblIdcalend.Text = "IdCalend";
            // 
            // dtEvento
            // 
            this.dtEvento.CustomFormat = "dddd dd MMMM yyyy HH.mm";
            this.dtEvento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEvento.Location = new System.Drawing.Point(10, 43);
            this.dtEvento.Name = "dtEvento";
            this.dtEvento.Size = new System.Drawing.Size(228, 21);
            this.dtEvento.TabIndex = 5;
            this.dtEvento.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // lblDtEvento
            // 
            this.lblDtEvento.AutoSize = true;
            this.lblDtEvento.Location = new System.Drawing.Point(7, 19);
            this.lblDtEvento.Name = "lblDtEvento";
            this.lblDtEvento.Size = new System.Drawing.Size(88, 13);
            this.lblDtEvento.TabIndex = 0;
            this.lblDtEvento.Text = "Data Ora Evento";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Titolo";
            // 
            // cmbTitolo
            // 
            this.cmbTitolo.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTitolo.FormattingEnabled = true;
            this.cmbTitolo.Location = new System.Drawing.Point(10, 123);
            this.cmbTitolo.Name = "cmbTitolo";
            this.cmbTitolo.Size = new System.Drawing.Size(370, 22);
            this.cmbTitolo.TabIndex = 15;
            // 
            // dtDataEmi
            // 
            this.dtDataEmi.Location = new System.Drawing.Point(68, 154);
            this.dtDataEmi.Name = "dtDataEmi";
            this.dtDataEmi.Size = new System.Drawing.Size(250, 21);
            this.dtDataEmi.TabIndex = 17;
            this.dtDataEmi.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // lblDataEmi
            // 
            this.lblDataEmi.AutoSize = true;
            this.lblDataEmi.Location = new System.Drawing.Point(11, 160);
            this.lblDataEmi.Name = "lblDataEmi";
            this.lblDataEmi.Size = new System.Drawing.Size(53, 13);
            this.lblDataEmi.TabIndex = 16;
            this.lblDataEmi.Text = "Emissione";
            // 
            // numSuppl
            // 
            this.numSuppl.DecimalPlaces = 2;
            this.numSuppl.Location = new System.Drawing.Point(264, 180);
            this.numSuppl.Name = "numSuppl";
            this.numSuppl.Size = new System.Drawing.Size(54, 21);
            this.numSuppl.TabIndex = 21;
            this.numSuppl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSuppl
            // 
            this.lblSuppl.AutoSize = true;
            this.lblSuppl.Location = new System.Drawing.Point(199, 184);
            this.lblSuppl.Name = "lblSuppl";
            this.lblSuppl.Size = new System.Drawing.Size(59, 13);
            this.lblSuppl.TabIndex = 20;
            this.lblSuppl.Text = "Prevendita";
            // 
            // numPrezzo
            // 
            this.numPrezzo.DecimalPlaces = 2;
            this.numPrezzo.Location = new System.Drawing.Point(68, 180);
            this.numPrezzo.Name = "numPrezzo";
            this.numPrezzo.Size = new System.Drawing.Size(92, 21);
            this.numPrezzo.TabIndex = 19;
            this.numPrezzo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPrezzo
            // 
            this.lblPrezzo.AutoSize = true;
            this.lblPrezzo.Location = new System.Drawing.Point(25, 185);
            this.lblPrezzo.Name = "lblPrezzo";
            this.lblPrezzo.Size = new System.Drawing.Size(39, 13);
            this.lblPrezzo.TabIndex = 18;
            this.lblPrezzo.Text = "Prezzo";
            // 
            // lblTipoTitolo
            // 
            this.lblTipoTitolo.AutoSize = true;
            this.lblTipoTitolo.Location = new System.Drawing.Point(385, 108);
            this.lblTipoTitolo.Name = "lblTipoTitolo";
            this.lblTipoTitolo.Size = new System.Drawing.Size(56, 13);
            this.lblTipoTitolo.TabIndex = 12;
            this.lblTipoTitolo.Text = "Tipo Titolo";
            // 
            // cmbTipoTitolo
            // 
            this.cmbTipoTitolo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoTitolo.FormattingEnabled = true;
            this.cmbTipoTitolo.Location = new System.Drawing.Point(388, 124);
            this.cmbTipoTitolo.Name = "cmbTipoTitolo";
            this.cmbTipoTitolo.Size = new System.Drawing.Size(250, 21);
            this.cmbTipoTitolo.TabIndex = 13;
            // 
            // cmbOrdineDiPosto
            // 
            this.cmbOrdineDiPosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrdineDiPosto.FormattingEnabled = true;
            this.cmbOrdineDiPosto.Location = new System.Drawing.Point(388, 83);
            this.cmbOrdineDiPosto.Name = "cmbOrdineDiPosto";
            this.cmbOrdineDiPosto.Size = new System.Drawing.Size(250, 21);
            this.cmbOrdineDiPosto.TabIndex = 11;
            // 
            // lblOrdineDiPosto
            // 
            this.lblOrdineDiPosto.AutoSize = true;
            this.lblOrdineDiPosto.Location = new System.Drawing.Point(385, 67);
            this.lblOrdineDiPosto.Name = "lblOrdineDiPosto";
            this.lblOrdineDiPosto.Size = new System.Drawing.Size(80, 13);
            this.lblOrdineDiPosto.TabIndex = 10;
            this.lblOrdineDiPosto.Text = "Ordine di posto";
            // 
            // lblTipoEvento
            // 
            this.lblTipoEvento.AutoSize = true;
            this.lblTipoEvento.Location = new System.Drawing.Point(7, 67);
            this.lblTipoEvento.Name = "lblTipoEvento";
            this.lblTipoEvento.Size = new System.Drawing.Size(64, 13);
            this.lblTipoEvento.TabIndex = 8;
            this.lblTipoEvento.Text = "Tipo Evento";
            // 
            // cmbTipoEvento
            // 
            this.cmbTipoEvento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoEvento.FormattingEnabled = true;
            this.cmbTipoEvento.Location = new System.Drawing.Point(10, 84);
            this.cmbTipoEvento.Name = "cmbTipoEvento";
            this.cmbTipoEvento.Size = new System.Drawing.Size(370, 21);
            this.cmbTipoEvento.TabIndex = 9;
            // 
            // numIncidenza
            // 
            this.numIncidenza.Location = new System.Drawing.Point(323, 15);
            this.numIncidenza.Name = "numIncidenza";
            this.numIncidenza.Size = new System.Drawing.Size(57, 21);
            this.numIncidenza.TabIndex = 4;
            this.numIncidenza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIncidenza
            // 
            this.lblIncidenza.AutoSize = true;
            this.lblIncidenza.Location = new System.Drawing.Point(269, 19);
            this.lblIncidenza.Name = "lblIncidenza";
            this.lblIncidenza.Size = new System.Drawing.Size(53, 13);
            this.lblIncidenza.TabIndex = 3;
            this.lblIncidenza.Text = "Incidenza";
            // 
            // radioIntrattenimento
            // 
            this.radioIntrattenimento.AutoSize = true;
            this.radioIntrattenimento.Location = new System.Drawing.Point(171, 17);
            this.radioIntrattenimento.Name = "radioIntrattenimento";
            this.radioIntrattenimento.Size = new System.Drawing.Size(101, 17);
            this.radioIntrattenimento.TabIndex = 2;
            this.radioIntrattenimento.Text = "Intrattenimento";
            this.radioIntrattenimento.UseVisualStyleBackColor = true;
            // 
            // radioSpettacolo
            // 
            this.radioSpettacolo.AutoSize = true;
            this.radioSpettacolo.Checked = true;
            this.radioSpettacolo.Location = new System.Drawing.Point(97, 17);
            this.radioSpettacolo.Name = "radioSpettacolo";
            this.radioSpettacolo.Size = new System.Drawing.Size(76, 17);
            this.radioSpettacolo.TabIndex = 1;
            this.radioSpettacolo.TabStop = true;
            this.radioSpettacolo.Text = "Spettacolo";
            this.radioSpettacolo.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkBiglietti);
            this.groupBox2.Controls.Add(this.chkAbbonamenti);
            this.groupBox2.Location = new System.Drawing.Point(9, 262);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(229, 48);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtro tipo transazioni";
            // 
            // chkBiglietti
            // 
            this.chkBiglietti.AutoSize = true;
            this.chkBiglietti.Location = new System.Drawing.Point(37, 20);
            this.chkBiglietti.Name = "chkBiglietti";
            this.chkBiglietti.Size = new System.Drawing.Size(60, 17);
            this.chkBiglietti.TabIndex = 0;
            this.chkBiglietti.Text = "Biglietti";
            this.chkBiglietti.UseVisualStyleBackColor = true;
            // 
            // chkAbbonamenti
            // 
            this.chkAbbonamenti.AutoSize = true;
            this.chkAbbonamenti.Location = new System.Drawing.Point(103, 20);
            this.chkAbbonamenti.Name = "chkAbbonamenti";
            this.chkAbbonamenti.Size = new System.Drawing.Size(89, 17);
            this.chkAbbonamenti.TabIndex = 1;
            this.chkAbbonamenti.Text = "Abbonamenti";
            this.chkAbbonamenti.UseVisualStyleBackColor = true;
            // 
            // grpPeriodo
            // 
            this.grpPeriodo.Controls.Add(this.dtFnzInizio);
            this.grpPeriodo.Controls.Add(this.lblFnzInizio);
            this.grpPeriodo.Controls.Add(this.dtFnzFine);
            this.grpPeriodo.Controls.Add(this.lblFnzFine);
            this.grpPeriodo.Location = new System.Drawing.Point(9, 101);
            this.grpPeriodo.Name = "grpPeriodo";
            this.grpPeriodo.Size = new System.Drawing.Size(229, 70);
            this.grpPeriodo.TabIndex = 3;
            this.grpPeriodo.TabStop = false;
            this.grpPeriodo.Text = "Periodo";
            // 
            // dtFnzInizio
            // 
            this.dtFnzInizio.Location = new System.Drawing.Point(34, 16);
            this.dtFnzInizio.Name = "dtFnzInizio";
            this.dtFnzInizio.Size = new System.Drawing.Size(186, 21);
            this.dtFnzInizio.TabIndex = 1;
            this.dtFnzInizio.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // lblFnzInizio
            // 
            this.lblFnzInizio.AutoSize = true;
            this.lblFnzInizio.Location = new System.Drawing.Point(6, 21);
            this.lblFnzInizio.Name = "lblFnzInizio";
            this.lblFnzInizio.Size = new System.Drawing.Size(22, 13);
            this.lblFnzInizio.TabIndex = 0;
            this.lblFnzInizio.Text = "Dal";
            // 
            // dtFnzFine
            // 
            this.dtFnzFine.Location = new System.Drawing.Point(34, 42);
            this.dtFnzFine.Name = "dtFnzFine";
            this.dtFnzFine.Size = new System.Drawing.Size(186, 21);
            this.dtFnzFine.TabIndex = 3;
            this.dtFnzFine.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // lblFnzFine
            // 
            this.lblFnzFine.AutoSize = true;
            this.lblFnzFine.Location = new System.Drawing.Point(6, 48);
            this.lblFnzFine.Name = "lblFnzFine";
            this.lblFnzFine.Size = new System.Drawing.Size(16, 13);
            this.lblFnzFine.TabIndex = 2;
            this.lblFnzFine.Text = "Al";
            // 
            // grpTipoRiepilogo
            // 
            this.grpTipoRiepilogo.Controls.Add(this.numProgressivo);
            this.grpTipoRiepilogo.Controls.Add(this.lblNumProg);
            this.grpTipoRiepilogo.Controls.Add(this.radioChkRPM);
            this.grpTipoRiepilogo.Controls.Add(this.radioChkRPG);
            this.grpTipoRiepilogo.Controls.Add(this.radioChkSIM);
            this.grpTipoRiepilogo.Location = new System.Drawing.Point(9, 177);
            this.grpTipoRiepilogo.Name = "grpTipoRiepilogo";
            this.grpTipoRiepilogo.Size = new System.Drawing.Size(229, 79);
            this.grpTipoRiepilogo.TabIndex = 4;
            this.grpTipoRiepilogo.TabStop = false;
            this.grpTipoRiepilogo.Text = "Tipo Riepilogo";
            // 
            // numProgressivo
            // 
            this.numProgressivo.Location = new System.Drawing.Point(160, 43);
            this.numProgressivo.Name = "numProgressivo";
            this.numProgressivo.Size = new System.Drawing.Size(55, 21);
            this.numProgressivo.TabIndex = 4;
            this.numProgressivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblNumProg
            // 
            this.lblNumProg.AutoSize = true;
            this.lblNumProg.Location = new System.Drawing.Point(100, 49);
            this.lblNumProg.Name = "lblNumProg";
            this.lblNumProg.Size = new System.Drawing.Size(63, 13);
            this.lblNumProg.TabIndex = 3;
            this.lblNumProg.Text = "Progressivo";
            // 
            // radioChkRPM
            // 
            this.radioChkRPM.AutoSize = true;
            this.radioChkRPM.Location = new System.Drawing.Point(99, 20);
            this.radioChkRPM.Name = "radioChkRPM";
            this.radioChkRPM.Size = new System.Drawing.Size(60, 17);
            this.radioChkRPM.TabIndex = 2;
            this.radioChkRPM.Text = "Mensile";
            this.radioChkRPM.UseVisualStyleBackColor = true;
            // 
            // radioChkRPG
            // 
            this.radioChkRPG.AutoSize = true;
            this.radioChkRPG.Checked = true;
            this.radioChkRPG.Location = new System.Drawing.Point(7, 19);
            this.radioChkRPG.Name = "radioChkRPG";
            this.radioChkRPG.Size = new System.Drawing.Size(76, 17);
            this.radioChkRPG.TabIndex = 0;
            this.radioChkRPG.TabStop = true;
            this.radioChkRPG.Text = "Giornaliero";
            this.radioChkRPG.UseVisualStyleBackColor = true;
            // 
            // radioChkSIM
            // 
            this.radioChkSIM.AutoSize = true;
            this.radioChkSIM.Location = new System.Drawing.Point(7, 47);
            this.radioChkSIM.Name = "radioChkSIM";
            this.radioChkSIM.Size = new System.Drawing.Size(80, 17);
            this.radioChkSIM.TabIndex = 1;
            this.radioChkSIM.Text = "Simulazione";
            this.radioChkSIM.UseVisualStyleBackColor = true;
            // 
            // lblHelpFunzione
            // 
            this.lblHelpFunzione.AutoEllipsis = true;
            this.lblHelpFunzione.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHelpFunzione.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelpFunzione.ForeColor = System.Drawing.Color.Green;
            this.lblHelpFunzione.Location = new System.Drawing.Point(7, 46);
            this.lblHelpFunzione.Name = "lblHelpFunzione";
            this.lblHelpFunzione.Size = new System.Drawing.Size(909, 52);
            this.lblHelpFunzione.TabIndex = 2;
            this.lblHelpFunzione.Text = "...";
            // 
            // lblFunzione
            // 
            this.lblFunzione.AutoSize = true;
            this.lblFunzione.Location = new System.Drawing.Point(9, 22);
            this.lblFunzione.Name = "lblFunzione";
            this.lblFunzione.Size = new System.Drawing.Size(78, 13);
            this.lblFunzione.TabIndex = 0;
            this.lblFunzione.Text = "Nome funzione";
            // 
            // cmbFunzione
            // 
            this.cmbFunzione.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFunzione.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFunzione.FormattingEnabled = true;
            this.cmbFunzione.Location = new System.Drawing.Point(108, 18);
            this.cmbFunzione.Name = "cmbFunzione";
            this.cmbFunzione.Size = new System.Drawing.Size(720, 24);
            this.cmbFunzione.TabIndex = 1;
            // 
            // grpRicerca
            // 
            this.grpRicerca.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpRicerca.Controls.Add(this.btnAnnullo);
            this.grpRicerca.Controls.Add(this.lblOperazione);
            this.grpRicerca.Controls.Add(this.btnCerca);
            this.grpRicerca.Controls.Add(this.txtSigillo);
            this.grpRicerca.Controls.Add(this.lblCarta);
            this.grpRicerca.Controls.Add(this.lblSigillo);
            this.grpRicerca.Controls.Add(this.txtCarta);
            this.grpRicerca.Controls.Add(this.lblProgressivo);
            this.grpRicerca.Controls.Add(this.txtProggressivo);
            this.grpRicerca.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpRicerca.ForeColor = System.Drawing.Color.Black;
            this.grpRicerca.Location = new System.Drawing.Point(4, 242);
            this.grpRicerca.Name = "grpRicerca";
            this.grpRicerca.Size = new System.Drawing.Size(929, 73);
            this.grpRicerca.TabIndex = 2;
            this.grpRicerca.TabStop = false;
            this.grpRicerca.Text = "Ricerca transazione";
            // 
            // btnAnnullo
            // 
            this.btnAnnullo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnullo.Location = new System.Drawing.Point(827, 13);
            this.btnAnnullo.Name = "btnAnnullo";
            this.btnAnnullo.Size = new System.Drawing.Size(91, 49);
            this.btnAnnullo.TabIndex = 9;
            this.btnAnnullo.Text = "Annullo con richiesta";
            this.btnAnnullo.UseVisualStyleBackColor = true;
            // 
            // lblOperazione
            // 
            this.lblOperazione.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOperazione.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperazione.ForeColor = System.Drawing.Color.Green;
            this.lblOperazione.Location = new System.Drawing.Point(334, 13);
            this.lblOperazione.Name = "lblOperazione";
            this.lblOperazione.Size = new System.Drawing.Size(392, 49);
            this.lblOperazione.TabIndex = 6;
            this.lblOperazione.Text = "...";
            this.lblOperazione.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCerca
            // 
            this.btnCerca.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerca.Location = new System.Drawing.Point(730, 13);
            this.btnCerca.Name = "btnCerca";
            this.btnCerca.Size = new System.Drawing.Size(91, 49);
            this.btnCerca.TabIndex = 7;
            this.btnCerca.Text = "Cerca";
            this.btnCerca.UseVisualStyleBackColor = true;
            // 
            // txtSigillo
            // 
            this.txtSigillo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSigillo.Location = new System.Drawing.Point(188, 40);
            this.txtSigillo.MaxLength = 16;
            this.txtSigillo.Name = "txtSigillo";
            this.txtSigillo.Size = new System.Drawing.Size(140, 22);
            this.txtSigillo.TabIndex = 5;
            // 
            // lblCarta
            // 
            this.lblCarta.AutoSize = true;
            this.lblCarta.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarta.Location = new System.Drawing.Point(6, 21);
            this.lblCarta.Name = "lblCarta";
            this.lblCarta.Size = new System.Drawing.Size(44, 16);
            this.lblCarta.TabIndex = 0;
            this.lblCarta.Text = "Carta";
            // 
            // lblSigillo
            // 
            this.lblSigillo.AutoSize = true;
            this.lblSigillo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSigillo.Location = new System.Drawing.Point(185, 21);
            this.lblSigillo.Name = "lblSigillo";
            this.lblSigillo.Size = new System.Drawing.Size(44, 16);
            this.lblSigillo.TabIndex = 4;
            this.lblSigillo.Text = "Sigillo";
            // 
            // txtCarta
            // 
            this.txtCarta.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCarta.Location = new System.Drawing.Point(9, 40);
            this.txtCarta.MaxLength = 8;
            this.txtCarta.Name = "txtCarta";
            this.txtCarta.Size = new System.Drawing.Size(78, 22);
            this.txtCarta.TabIndex = 1;
            // 
            // lblProgressivo
            // 
            this.lblProgressivo.AutoSize = true;
            this.lblProgressivo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressivo.Location = new System.Drawing.Point(97, 21);
            this.lblProgressivo.Name = "lblProgressivo";
            this.lblProgressivo.Size = new System.Drawing.Size(85, 16);
            this.lblProgressivo.TabIndex = 2;
            this.lblProgressivo.Text = "Progressivo";
            // 
            // txtProggressivo
            // 
            this.txtProggressivo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProggressivo.Location = new System.Drawing.Point(100, 40);
            this.txtProggressivo.MaxLength = 8;
            this.txtProggressivo.Name = "txtProggressivo";
            this.txtProggressivo.Size = new System.Drawing.Size(78, 22);
            this.txtProggressivo.TabIndex = 3;
            // 
            // grpTransazioni
            // 
            this.grpTransazioni.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpTransazioni.Controls.Add(this.grpFnzModStorico);
            this.grpTransazioni.Controls.Add(this.btnFiltri);
            this.grpTransazioni.Controls.Add(this.radioAnnulli);
            this.grpTransazioni.Controls.Add(this.radioTransazioni);
            this.grpTransazioni.Controls.Add(this.lblFine);
            this.grpTransazioni.Controls.Add(this.dtFine);
            this.grpTransazioni.Controls.Add(this.lblInizio);
            this.grpTransazioni.Controls.Add(this.dtInizio);
            this.grpTransazioni.Controls.Add(this.btnLoad);
            this.grpTransazioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTransazioni.ForeColor = System.Drawing.Color.Black;
            this.grpTransazioni.Location = new System.Drawing.Point(4, 116);
            this.grpTransazioni.Name = "grpTransazioni";
            this.grpTransazioni.Size = new System.Drawing.Size(929, 126);
            this.grpTransazioni.TabIndex = 1;
            this.grpTransazioni.TabStop = false;
            this.grpTransazioni.Text = "Carica transazioni o annulli";
            // 
            // grpFnzModStorico
            // 
            this.grpFnzModStorico.Controls.Add(this.radioStoricoNO);
            this.grpFnzModStorico.Controls.Add(this.radioStoricoSI);
            this.grpFnzModStorico.Controls.Add(this.radioStoricoCHECK);
            this.grpFnzModStorico.Location = new System.Drawing.Point(656, 43);
            this.grpFnzModStorico.Name = "grpFnzModStorico";
            this.grpFnzModStorico.Size = new System.Drawing.Size(260, 72);
            this.grpFnzModStorico.TabIndex = 10;
            this.grpFnzModStorico.TabStop = false;
            this.grpFnzModStorico.Text = "Modalità storico";
            // 
            // radioStoricoNO
            // 
            this.radioStoricoNO.Location = new System.Drawing.Point(94, 31);
            this.radioStoricoNO.Name = "radioStoricoNO";
            this.radioStoricoNO.Size = new System.Drawing.Size(51, 17);
            this.radioStoricoNO.TabIndex = 1;
            this.radioStoricoNO.Text = "No";
            this.radioStoricoNO.UseVisualStyleBackColor = true;
            // 
            // radioStoricoSI
            // 
            this.radioStoricoSI.Checked = true;
            this.radioStoricoSI.Location = new System.Drawing.Point(17, 31);
            this.radioStoricoSI.Name = "radioStoricoSI";
            this.radioStoricoSI.Size = new System.Drawing.Size(47, 20);
            this.radioStoricoSI.TabIndex = 0;
            this.radioStoricoSI.TabStop = true;
            this.radioStoricoSI.Text = "Si";
            this.radioStoricoSI.UseVisualStyleBackColor = true;
            // 
            // radioStoricoCHECK
            // 
            this.radioStoricoCHECK.Location = new System.Drawing.Point(175, 31);
            this.radioStoricoCHECK.Name = "radioStoricoCHECK";
            this.radioStoricoCHECK.Size = new System.Drawing.Size(79, 17);
            this.radioStoricoCHECK.TabIndex = 2;
            this.radioStoricoCHECK.Text = "Verifica";
            this.radioStoricoCHECK.UseVisualStyleBackColor = true;
            // 
            // btnFiltri
            // 
            this.btnFiltri.AutoSize = true;
            this.btnFiltri.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltri.Location = new System.Drawing.Point(315, 47);
            this.btnFiltri.Name = "btnFiltri";
            this.btnFiltri.Size = new System.Drawing.Size(335, 68);
            this.btnFiltri.TabIndex = 7;
            this.btnFiltri.Text = "Carica Transazioni per periodo con ulteriori filtri";
            this.btnFiltri.UseVisualStyleBackColor = true;
            // 
            // radioAnnulli
            // 
            this.radioAnnulli.AutoSize = true;
            this.radioAnnulli.Location = new System.Drawing.Point(831, 17);
            this.radioAnnulli.Name = "radioAnnulli";
            this.radioAnnulli.Size = new System.Drawing.Size(64, 20);
            this.radioAnnulli.TabIndex = 5;
            this.radioAnnulli.Text = "Annulli";
            this.radioAnnulli.UseVisualStyleBackColor = true;
            // 
            // radioTransazioni
            // 
            this.radioTransazioni.Checked = true;
            this.radioTransazioni.Location = new System.Drawing.Point(673, 17);
            this.radioTransazioni.Name = "radioTransazioni";
            this.radioTransazioni.Size = new System.Drawing.Size(92, 20);
            this.radioTransazioni.TabIndex = 4;
            this.radioTransazioni.TabStop = true;
            this.radioTransazioni.Text = "Transazioni";
            this.radioTransazioni.UseVisualStyleBackColor = true;
            // 
            // lblFine
            // 
            this.lblFine.AutoSize = true;
            this.lblFine.Location = new System.Drawing.Point(350, 20);
            this.lblFine.Name = "lblFine";
            this.lblFine.Size = new System.Drawing.Size(19, 16);
            this.lblFine.TabIndex = 2;
            this.lblFine.Text = "Al";
            // 
            // dtFine
            // 
            this.dtFine.Location = new System.Drawing.Point(375, 17);
            this.dtFine.Name = "dtFine";
            this.dtFine.Size = new System.Drawing.Size(225, 23);
            this.dtFine.TabIndex = 3;
            this.dtFine.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // lblInizio
            // 
            this.lblInizio.AutoSize = true;
            this.lblInizio.Location = new System.Drawing.Point(35, 20);
            this.lblInizio.Name = "lblInizio";
            this.lblInizio.Size = new System.Drawing.Size(26, 16);
            this.lblInizio.TabIndex = 0;
            this.lblInizio.Text = "Dal";
            // 
            // dtInizio
            // 
            this.dtInizio.Location = new System.Drawing.Point(67, 17);
            this.dtInizio.Name = "dtInizio";
            this.dtInizio.Size = new System.Drawing.Size(225, 23);
            this.dtInizio.TabIndex = 1;
            this.dtInizio.Value = new System.DateTime(2014, 11, 14, 17, 45, 0, 0);
            // 
            // btnLoad
            // 
            this.btnLoad.AutoSize = true;
            this.btnLoad.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.Location = new System.Drawing.Point(9, 47);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(300, 68);
            this.btnLoad.TabIndex = 6;
            this.btnLoad.Text = "Carica Transazioni/Annulli per periodo";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // grpRiepilogo
            // 
            this.grpRiepilogo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpRiepilogo.Controls.Add(this.radioRCA);
            this.grpRiepilogo.Controls.Add(this.btnCheckServerFiscale);
            this.grpRiepilogo.Controls.Add(this.btnMasterizza);
            this.grpRiepilogo.Controls.Add(this.chkListSale);
            this.grpRiepilogo.Controls.Add(this.lblSalaSimulazione);
            this.grpRiepilogo.Controls.Add(this.chkDettagli);
            this.grpRiepilogo.Controls.Add(this.btnPrint);
            this.grpRiepilogo.Controls.Add(this.lblMsgRP);
            this.grpRiepilogo.Controls.Add(this.btnGenera);
            this.grpRiepilogo.Controls.Add(this.radioSIM);
            this.grpRiepilogo.Controls.Add(this.radioRPG);
            this.grpRiepilogo.Controls.Add(this.lblGiorno);
            this.grpRiepilogo.Controls.Add(this.dtGiorno);
            this.grpRiepilogo.Controls.Add(this.radioRPM);
            this.grpRiepilogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpRiepilogo.ForeColor = System.Drawing.Color.Black;
            this.grpRiepilogo.Location = new System.Drawing.Point(4, 4);
            this.grpRiepilogo.Name = "grpRiepilogo";
            this.grpRiepilogo.Size = new System.Drawing.Size(929, 112);
            this.grpRiepilogo.TabIndex = 0;
            this.grpRiepilogo.TabStop = false;
            this.grpRiepilogo.Text = "Riepiloghi";
            // 
            // radioRCA
            // 
            this.radioRCA.AutoSize = true;
            this.radioRCA.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioRCA.Location = new System.Drawing.Point(272, 51);
            this.radioRCA.Name = "radioRCA";
            this.radioRCA.Size = new System.Drawing.Size(53, 20);
            this.radioRCA.TabIndex = 13;
            this.radioRCA.Text = "RCA";
            this.radioRCA.UseVisualStyleBackColor = true;
            // 
            // btnCheckServerFiscale
            // 
            this.btnCheckServerFiscale.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckServerFiscale.Location = new System.Drawing.Point(807, 41);
            this.btnCheckServerFiscale.Name = "btnCheckServerFiscale";
            this.btnCheckServerFiscale.Size = new System.Drawing.Size(111, 40);
            this.btnCheckServerFiscale.TabIndex = 12;
            this.btnCheckServerFiscale.Text = "Check Server Fiscale";
            this.btnCheckServerFiscale.UseVisualStyleBackColor = true;
            this.btnCheckServerFiscale.Click += new System.EventHandler(this.btnCheckServerFiscale_Click);
            // 
            // btnMasterizza
            // 
            this.btnMasterizza.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMasterizza.Location = new System.Drawing.Point(807, 13);
            this.btnMasterizza.Name = "btnMasterizza";
            this.btnMasterizza.Size = new System.Drawing.Size(111, 25);
            this.btnMasterizza.TabIndex = 11;
            this.btnMasterizza.Text = "Masterizza";
            this.btnMasterizza.UseVisualStyleBackColor = true;
            // 
            // chkListSale
            // 
            this.chkListSale.CheckOnClick = true;
            this.chkListSale.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkListSale.FormattingEnabled = true;
            this.chkListSale.Location = new System.Drawing.Point(609, 29);
            this.chkListSale.Name = "chkListSale";
            this.chkListSale.ScrollAlwaysVisible = true;
            this.chkListSale.Size = new System.Drawing.Size(192, 52);
            this.chkListSale.TabIndex = 10;
            // 
            // lblSalaSimulazione
            // 
            this.lblSalaSimulazione.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSalaSimulazione.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalaSimulazione.Location = new System.Drawing.Point(609, 13);
            this.lblSalaSimulazione.Name = "lblSalaSimulazione";
            this.lblSalaSimulazione.Size = new System.Drawing.Size(192, 18);
            this.lblSalaSimulazione.TabIndex = 8;
            this.lblSalaSimulazione.Text = "Filtro Cod. locale";
            // 
            // chkDettagli
            // 
            this.chkDettagli.Checked = true;
            this.chkDettagli.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDettagli.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDettagli.Location = new System.Drawing.Point(541, 22);
            this.chkDettagli.Name = "chkDettagli";
            this.chkDettagli.Size = new System.Drawing.Size(62, 49);
            this.chkDettagli.TabIndex = 7;
            this.chkDettagli.Text = "dettagli omaggi";
            this.chkDettagli.UseVisualStyleBackColor = true;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(447, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(88, 68);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "Stampa Riepilogo";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // lblMsgRP
            // 
            this.lblMsgRP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMsgRP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsgRP.ForeColor = System.Drawing.Color.Green;
            this.lblMsgRP.Location = new System.Drawing.Point(6, 84);
            this.lblMsgRP.Name = "lblMsgRP";
            this.lblMsgRP.Size = new System.Drawing.Size(912, 21);
            this.lblMsgRP.TabIndex = 7;
            this.lblMsgRP.Text = "...";
            this.lblMsgRP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGenera
            // 
            this.btnGenera.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenera.Location = new System.Drawing.Point(354, 13);
            this.btnGenera.Name = "btnGenera";
            this.btnGenera.Size = new System.Drawing.Size(87, 68);
            this.btnGenera.TabIndex = 5;
            this.btnGenera.Text = "Genera Riepilogo";
            this.btnGenera.UseVisualStyleBackColor = true;
            // 
            // radioSIM
            // 
            this.radioSIM.AutoSize = true;
            this.radioSIM.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioSIM.Location = new System.Drawing.Point(188, 51);
            this.radioSIM.Name = "radioSIM";
            this.radioSIM.Size = new System.Drawing.Size(78, 20);
            this.radioSIM.TabIndex = 4;
            this.radioSIM.Text = "Simulaz.";
            this.radioSIM.UseVisualStyleBackColor = true;
            // 
            // radioRPG
            // 
            this.radioRPG.AutoSize = true;
            this.radioRPG.Checked = true;
            this.radioRPG.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioRPG.Location = new System.Drawing.Point(7, 51);
            this.radioRPG.Name = "radioRPG";
            this.radioRPG.Size = new System.Drawing.Size(95, 20);
            this.radioRPG.TabIndex = 2;
            this.radioRPG.TabStop = true;
            this.radioRPG.Text = "Giornaliero";
            this.radioRPG.UseVisualStyleBackColor = true;
            // 
            // lblGiorno
            // 
            this.lblGiorno.AutoSize = true;
            this.lblGiorno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiorno.Location = new System.Drawing.Point(4, 27);
            this.lblGiorno.Name = "lblGiorno";
            this.lblGiorno.Size = new System.Drawing.Size(39, 16);
            this.lblGiorno.TabIndex = 0;
            this.lblGiorno.Text = "Data";
            // 
            // dtGiorno
            // 
            this.dtGiorno.Location = new System.Drawing.Point(43, 22);
            this.dtGiorno.Name = "dtGiorno";
            this.dtGiorno.Size = new System.Drawing.Size(225, 23);
            this.dtGiorno.TabIndex = 1;
            this.dtGiorno.Value = new System.DateTime(2020, 3, 30, 0, 0, 0, 0);
            // 
            // radioRPM
            // 
            this.radioRPM.AutoSize = true;
            this.radioRPM.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioRPM.Location = new System.Drawing.Point(108, 51);
            this.radioRPM.Name = "radioRPM";
            this.radioRPM.Size = new System.Drawing.Size(74, 20);
            this.radioRPM.TabIndex = 3;
            this.radioRPM.Text = "Mensile";
            this.radioRPM.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 680);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(939, 10);
            this.splitter1.TabIndex = 7;
            this.splitter1.TabStop = false;
            // 
            // frmtestRiepiloghi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(939, 744);
            this.Controls.Add(this.pnlDati);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlServizio);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmtestRiepiloghi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Riepiloghi";
            this.pnlDati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDati)).EndInit();
            this.pnlServizio.ResumeLayout(false);
            this.grpFunzioni.ResumeLayout(false);
            this.grpFunzioni.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpFnzEvento.ResumeLayout(false);
            this.grpFnzEvento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSuppl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrezzo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncidenza)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpPeriodo.ResumeLayout(false);
            this.grpPeriodo.PerformLayout();
            this.grpTipoRiepilogo.ResumeLayout(false);
            this.grpTipoRiepilogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProgressivo)).EndInit();
            this.grpRicerca.ResumeLayout(false);
            this.grpRicerca.PerformLayout();
            this.grpTransazioni.ResumeLayout(false);
            this.grpTransazioni.PerformLayout();
            this.grpFnzModStorico.ResumeLayout(false);
            this.grpRiepilogo.ResumeLayout(false);
            this.grpRiepilogo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDati;
        private System.Windows.Forms.DataGridView dgvDati;
        private System.Windows.Forms.Panel pnlServizio;
        private System.Windows.Forms.GroupBox grpRicerca;
        private System.Windows.Forms.Button btnCerca;
        private System.Windows.Forms.TextBox txtSigillo;
        private System.Windows.Forms.Label lblCarta;
        private System.Windows.Forms.Label lblSigillo;
        private System.Windows.Forms.TextBox txtCarta;
        private System.Windows.Forms.Label lblProgressivo;
        private System.Windows.Forms.TextBox txtProggressivo;
        private System.Windows.Forms.GroupBox grpTransazioni;
        private System.Windows.Forms.RadioButton radioAnnulli;
        private System.Windows.Forms.RadioButton radioTransazioni;
        private System.Windows.Forms.Label lblFine;
        private System.Windows.Forms.DateTimePicker dtFine;
        private System.Windows.Forms.Label lblInizio;
        private System.Windows.Forms.DateTimePicker dtInizio;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.GroupBox grpRiepilogo;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label lblMsgRP;
        private System.Windows.Forms.Button btnGenera;
        private System.Windows.Forms.RadioButton radioSIM;
        private System.Windows.Forms.RadioButton radioRPG;
        private System.Windows.Forms.Label lblGiorno;
        private System.Windows.Forms.DateTimePicker dtGiorno;
        private System.Windows.Forms.RadioButton radioRPM;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label lblOperazione;
        private System.Windows.Forms.Button btnFiltri;
        private System.Windows.Forms.CheckBox chkDettagli;
        private System.Windows.Forms.GroupBox grpFunzioni;
        private System.Windows.Forms.RadioButton radioChkSIM;
        private System.Windows.Forms.RadioButton radioChkRPG;
        private System.Windows.Forms.RadioButton radioChkRPM;
        private System.Windows.Forms.Label lblSalaSimulazione;
        private System.Windows.Forms.Label lblFunzione;
        private System.Windows.Forms.ComboBox cmbFunzione;
        private System.Windows.Forms.Label lblFnzFine;
        private System.Windows.Forms.DateTimePicker dtFnzFine;
        private System.Windows.Forms.Label lblFnzInizio;
        private System.Windows.Forms.DateTimePicker dtFnzInizio;
        private System.Windows.Forms.Label lblHelpFunzione;
        private System.Windows.Forms.CheckBox chkAbbonamenti;
        private System.Windows.Forms.CheckBox chkBiglietti;
        private System.Windows.Forms.GroupBox grpFnzEvento;
        private System.Windows.Forms.Label lblTipoTitolo;
        private System.Windows.Forms.ComboBox cmbTipoTitolo;
        private System.Windows.Forms.ComboBox cmbOrdineDiPosto;
        private System.Windows.Forms.Label lblOrdineDiPosto;
        private System.Windows.Forms.Label lblTipoEvento;
        private System.Windows.Forms.ComboBox cmbTipoEvento;
        private System.Windows.Forms.NumericUpDown numIncidenza;
        private System.Windows.Forms.Label lblIncidenza;
        private System.Windows.Forms.RadioButton radioIntrattenimento;
        private System.Windows.Forms.RadioButton radioSpettacolo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox grpPeriodo;
        private System.Windows.Forms.GroupBox grpTipoRiepilogo;
        private System.Windows.Forms.GroupBox grpFnzModStorico;
        private System.Windows.Forms.RadioButton radioStoricoNO;
        private System.Windows.Forms.RadioButton radioStoricoSI;
        private System.Windows.Forms.RadioButton radioStoricoCHECK;
        private System.Windows.Forms.NumericUpDown numSuppl;
        private System.Windows.Forms.Label lblSuppl;
        private System.Windows.Forms.NumericUpDown numPrezzo;
        private System.Windows.Forms.Label lblPrezzo;
        private System.Windows.Forms.DateTimePicker dtDataEmi;
        private System.Windows.Forms.Label lblDataEmi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTitolo;
        private System.Windows.Forms.NumericUpDown numProgressivo;
        private System.Windows.Forms.Label lblNumProg;
        private System.Windows.Forms.DateTimePicker dtEvento;
        private System.Windows.Forms.Label lblDtEvento;
        private System.Windows.Forms.TextBox txtIdCalend;
        private System.Windows.Forms.Label lblIdcalend;
        private System.Windows.Forms.CheckBox chkPerCalcoloOmaggio;
        private System.Windows.Forms.CheckBox chkRicercaOmaggitransazione;
        private System.Windows.Forms.Button btnFunzione;
        private System.Windows.Forms.Button btnAnnullo;
        private System.Windows.Forms.ComboBox cmbVrfSale;
        private System.Windows.Forms.Label lblVrfCodiceLocale;
        private System.Windows.Forms.CheckedListBox chkListSale;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtRegexForm;
        private System.Windows.Forms.Label lblRegexRES;
        private System.Windows.Forms.Label lblRegexForm;
        private System.Windows.Forms.TextBox txtRegexExpr;
        private System.Windows.Forms.Label lblRegexExpr;
        private System.Windows.Forms.Button btnMasterizza;
        private System.Windows.Forms.Button btnCheckServerFiscale;
        private System.Windows.Forms.RadioButton radioRCA;
    }
}