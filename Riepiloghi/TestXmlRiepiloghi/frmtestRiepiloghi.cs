﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;
using Wintic.Data.oracle;
using Riepiloghi;

namespace TestXmlRiepiloghi
{
    public partial class frmtestRiepiloghi : Form
    {
        private class clsItemFunzione
        {
            public string Nome = "";
            public string Descrizione = "";
            public clsItemFunzione(string cNome, string cDescrizione)
            {
                this.Nome = cNome;
                this.Descrizione = cDescrizione;
            }
            public override string ToString()
            {
                return this.Nome + " " + this.Descrizione;
            }
        }

        private class clsItemCombo
        {
            public object Value = null;
            public string Descrizione = "";
            public bool ValueVisible = true;

            public clsItemCombo(object oValue, string cDescrizione)
            {
                this.Value = oValue;
                this.Descrizione = cDescrizione;
            }

            public override string ToString()
            {
                if (this.Value != null && this.ValueVisible)
                {
                    return this.Value.ToString() + " " + this.Descrizione;
                }
                else
                {
                    return this.Descrizione;
                }
            }
        }

        private IConnection oCon;
        public frmtestRiepiloghi()
        {
            InitializeComponent();
        }

        public class singoloItem
        {
            [System.Xml.Serialization.XmlAttribute(AttributeName = "AttributoCiccio")]
            public string Ciccio { get; set; }

            public int Totale { get; set; }
        }

        [System.Xml.Serialization.XmlRoot(ElementName = "ClasseMasterRoot", Namespace = "")]
        public class masterClass
        {
            [System.Xml.Serialization.XmlAttribute(AttributeName = "AttributoPluto")]
            public string Pluto { get; set; }
            public string Minny { get; set; }

            [System.Xml.Serialization.XmlElement]
            public List<singoloItem> singoloItem { get; set; }
        }

        
        private void Test()
        {
            DataTable lta = null;
            Exception error = null;
            //bool result = clsRiepiloghi.LeggiTabellaLTA(oCon, new DateTime(2020, 2, 13), out lta, out error);
            //string ltsSerialize = Riepiloghi.clsXml_SerializeLTA.SerialiazeLta(lta, out error, true);

            //string g = "";

            //if (error != null) g = error.ToString();
            //string value = "";

            //masterClass master = new masterClass();
            //master.Pluto = "Sono Pluto";
            //master.Minny = "Sono Minny";
            //master.singoloItem = new List<singoloItem>();

            //singoloItem item;

            //item = new singoloItem();
            //item.Ciccio = "Io sono ciccio1";
            //item.Totale = 1;
            //master.singoloItem.Add(item);

            //item = new singoloItem();
            //item.Ciccio = "Io sono ciccio2";
            //item.Totale = 2;
            //master.singoloItem.Add(item);

            //try
            //{
            //    using (System.IO.MemoryStream xmlStream = new System.IO.MemoryStream())
            //    {
            //        System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            //        xmlWriterSettings.Encoding = System.Text.Encoding.UTF8;
            //        xmlWriterSettings.OmitXmlDeclaration = true;
            //        xmlWriterSettings.Indent = true;
            //        xmlWriterSettings.CloseOutput = false;
            //        System.Xml.Serialization.XmlSerializerNamespaces xmlNameSpaces = new System.Xml.Serialization.XmlSerializerNamespaces();
            //        xmlNameSpaces.Add("", "");

            //        using (System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(xmlStream, xmlWriterSettings))
            //        {
            //            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(masterClass));

            //            xmlSerializer.Serialize(xmlWriter, master, xmlNameSpaces);

            //            xmlStream.Write(System.Text.Encoding.UTF8.GetBytes("\r\n"), 0, 2);
            //            xmlWriter.Dispose();
            //        }
            //        value = xmlWriterSettings.Encoding.GetString(xmlStream.ToArray());
            //        xmlStream.Dispose();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}
        }


        protected override void OnShown(EventArgs e)
        {
            oCon = new CConnectionOracle();
            oCon.ConnectionString = "user=wtic;password=obelix;data source=SERVER-2019;";
            oCon.Open();
            this.InitForm();

            //DateTime Giorno = new DateTime(2019, 10, 18);
            //Exception oError = null;

            //List<string> oCodiciLocali = new List<string>() { "0131450216677" };
            ////foreach (DataRow r in oCon.ExecuteQuery("SELECT DISTINCT CODICE_LOCALE FROM CINEMA.SALE").Rows)
            ////{
            ////    oCodiciLocali.Add(r["CODICE_LOCALE"].ToString());
            ////}
            //string cFilePdf = @"c:\temp\prova.pdf";
            //Riepiloghi.clsRiepiloghi.PrintToPDF_SimulazioneMensile(Giorno, oCon, Riepiloghi.clsRiepiloghi.DefModalitaStorico.Check, true, out oError, "", out cFilePdf, oCodiciLocali.ToArray(), 0);
        }

        private void InitForm()
        {
            this.InitComboBoxes();
            //this.dtGiorno.Value = DateTime.Now.Date;
            this.dtInizio.Value = DateTime.Now.Date;
            this.dtFine.Value = DateTime.Now.Date;
            this.dtFnzInizio.Value = DateTime.Now.Date;
            this.dtFnzFine.Value = DateTime.Now.Date;
            this.dtEvento.Value = DateTime.Now.Date;
            // Assegno Eventi
            this.dtGiorno.ValueChanged += new EventHandler(dtGiorno_ValueChanged);
            this.radioRPG.CheckedChanged += new EventHandler(radioRPG_RPM_SIM_CheckedChanged);
            this.radioRPM.CheckedChanged += new EventHandler(radioRPG_RPM_SIM_CheckedChanged);
            this.radioSIM.CheckedChanged += new EventHandler(radioRPG_RPM_SIM_CheckedChanged);
            this.btnGenera.Click += new EventHandler(btnGenera_Click);
            this.btnPrint.Click += new EventHandler(btnPrint_Click);
            this.btnLoad.Click += new EventHandler(btnLoad_Click);
            this.btnCerca.Click += new EventHandler(btnCerca_Click);
            this.btnFiltri.Click += new EventHandler(btnFiltri_Click);
            this.btnAnnullo.Click += new EventHandler(btnAnnullo_Click);
            this.btnMasterizza.Click += BtnMasterizza_Click;

            // Controlli
            this.radioChkSIM.CheckedChanged += new EventHandler(radioChkSIM_CheckedChanged);

            this.cmbFunzione.Items.Clear();

            this.cmbFunzione.Items.Add(new clsItemFunzione("NESSUNA", "Selezionare una funzione base"));
            this.cmbFunzione.SelectedIndex = 0;
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_GET_CODICE_SISTEMA", "Lettura codice sistema"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_CHECK_STORICO", "Controllo se utilizzare solo anche dati storici"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_INIZIO_MESE", "Inizio del mese"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_FINE_MESE", "Fine del mese"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_TABLE_LOG_BASE_DATA", "Log delle transazioni per periodo"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_TRANSAZIONI_RIEPILOGHI", "Calcolo transazioni di base per generazione e stampa RPG/RPG/Simulazione"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_CALC_RIEPILOGHI_DETTAGLIO", "Calcolo dati riepiloghi dettaglio per generazione e stampa RPG/RPM/Simulazione"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_CALC_RIEPILOGHI","Calcolo dati riepiloghi per generazione RPG/RPM"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_CALC_RIEPILOGHI_PRINT", "Calcolo dati riepiloghi per stampa RPG/RPM/Simulazione"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_CALC_PROG_RIEPILOGO", "Calcolo del prossimo progressivo del riepilogo"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_RIEPILOGHI_CALC_NAME", "Calcono nome del file di riepilogo"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_RIEPILOGHI_CALC_TITOLI", "Calcolo lista dei titoli del ripeilogo"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_READ_RIEPILOGHI", "Lettura dati riepiloghi salvati per generazione RPG/RPM"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_READ_RIEPILOGHI_PRINT", "Lettura dati riepiloghi generati per stampa RPG/RPM"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_GETTABLE_IMPOSTE", "Lettura tabella imposte"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_RIEPILOGHI_GETRIF_OMAGGIO", "Calcolo riferimenti per omaggio"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_TABLE_CALC_IVAISI", "Calcolo IVA e Imposte intrattenimento"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_INIT_ORGANIZZATORI", "Lettura tabella organizzatori"));
            this.cmbFunzione.Items.Add(new clsItemFunzione("F_TABLE_RIEPILOGHI_DA_GENERARE", "Lista dei riepiloghi giornaliero/Mensili da generare"));
            this.cmbFunzione.SelectedValueChanged += new EventHandler(cmbFunzione_SelectedValueChanged);
            
            this.SetControlFunzione();
            this.SetControlRiepiloghi();

            this.btnFunzione.Click += new EventHandler(btnFunzione_Click);

            this.Test();
        }

        private void BtnMasterizza_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Exception oError = null;
            this.UseWaitCursorForm = true;
            try
            {
                //lRet = clsRiepiloghi.EseguiMasterizzazione_Pending(oCon, @"c:\masterizza_test", out oError);
            }
            catch (Exception ex)
            {
                oError = ex;
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }
            if (oError != null)
            {
                MessageBox.Show(oError.ToString());
            }
        }

        private void btnAnnullo_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Int64 nErrorCode = 0;
            string cErrorDesc = "";
            bool lWarning = false;
            Exception oErr = null;
            this.dgvDati.DataSource = null;
            this.lblOperazione.Text = "Ricerca...";
            this.UseWaitCursorForm = true;
            try
            {
                IRecordSet oRS;
                DateTime dStart = DateTime.Now;
                Int64 nProg = 0;
                string cCarta = this.txtCarta.Text;
                string cSigillo = this.txtSigillo.Text;

                if (cCarta.Length == 8 &&
                    (cSigillo.Length == 16 || Int64.TryParse(this.txtProggressivo.Text, out nProg)))
                {
                    //if (cSigillo.Length == 16)
                    //{
                    //    oRS = clsRiepiloghi.GetTransazioneCARTA_SIGILLO(cCarta, cSigillo, oCon, out oErr);
                    //}
                    //else
                    //{
                    //    oRS = clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(cCarta, nProg, oCon, out oErr);
                    //}

                    //this.UseWaitCursorForm = false;

                    if (oErr == null)
                    {
                        
                        Wintic.Data.IRecordSet oRSTransazione = null;
                        //nErrorCode = clsRiepiloghi.CheckAnnulloTransazione(cCarta, cSigillo, nProg, nProg, this.oCon, out oErr, out cErrorDesc, out lWarning, out oRSTransazione);
                        List<clsCheckAnnulloTransazione> ListaTransazioniDaAnnullare = clsRiepiloghi.CheckAnnulloTransazione(cCarta, cSigillo, nProg, nProg, true, this.oCon, out oErr);

                        if (oErr == null)
                        {
                            if (ListaTransazioniDaAnnullare != null && ListaTransazioniDaAnnullare.Count > 0)
                            {
                                int index = 0;
                                index += 1;
                                foreach (clsCheckAnnulloTransazione oCheckAnnulloTransazione in ListaTransazioniDaAnnullare)
                                {
                                    if (oCheckAnnulloTransazione.Causali != null && oCheckAnnulloTransazione.Causali.Count > 0 &&
                                        oCheckAnnulloTransazione.Transazione != null && oCheckAnnulloTransazione.Transazione.Rows.Count > 0)
                                    {
                                        frmPropertyObject oFrmTransazione = new frmPropertyObject(oCheckAnnulloTransazione.Descrizione, oCheckAnnulloTransazione.Transazione.Rows[0]);
                                        oFrmTransazione.ShowDialog();
                                        oFrmTransazione.Close();
                                        oFrmTransazione.Dispose();

                                        List<frmMenuOptions.clsItemMenu> listaCausali = new List<frmMenuOptions.clsItemMenu>();
                                        foreach (KeyValuePair<string, string> itemCausale in oCheckAnnulloTransazione.Causali)
                                        {
                                            listaCausali.Add(new frmMenuOptions.clsItemMenu(itemCausale.Key, itemCausale.Value.ToString()));
                                        }

                                        string CausaleDiAnnullo = "";
                                        bool lContinue = false;
                                        frmMenuOptions oFrmSelectCausale = new frmMenuOptions("Selezionare la causale di annullo", listaCausali);
                                        if (oFrmSelectCausale.ShowDialog() == DialogResult.OK && oFrmSelectCausale.SelectedValue != null)
                                        {
                                            CausaleDiAnnullo = oFrmSelectCausale.SelectedValue.ToString();
                                            lContinue = CausaleDiAnnullo.Trim() != "";
                                        }
                                        oFrmSelectCausale.Close();
                                        oFrmSelectCausale.Dispose();

                                        if (lContinue)
                                        {
                                            string cCartaDaAnnullare = oCheckAnnulloTransazione.Carta;
                                            string cSigilloDaAnnullare = oCheckAnnulloTransazione.Sigillo;
                                            long nProgressivoDaAnnullare = (long)oCheckAnnulloTransazione.Progressivo;

                                            string CartaAnnullante = "";
                                            string SigilloAnnullante = "";
                                            long ProgressivoAnnullante = 0;
                                            IRecordSet oRSTrensazione = null;
                                            IRecordSet oRSTransazioneAnnullante = null;
                                            List<Exception> ExceptionsPostAnnullo = new List<Exception>();
                                            bool lAnnullo = Riepiloghi.clsRiepiloghi.AnnulloTransazione(cCartaDaAnnullare, cSigilloDaAnnullare, nProgressivoDaAnnullare, CausaleDiAnnullo, true, this.oCon, out oErr, out CartaAnnullante, out SigilloAnnullante, out ProgressivoAnnullante, out oRSTrensazione, out oRSTransazioneAnnullante, out ExceptionsPostAnnullo);
                                            if (lAnnullo)
                                            {
                                                MessageBox.Show("Annullo avvenuto correttamente", "Annullo OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            }
                                            else
                                            {
                                                if (oErr != null)
                                                    MessageBox.Show(oErr.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                else
                                                    MessageBox.Show("Annullo non avvenuto", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cErrorDesc = oCheckAnnulloTransazione.Descrizione;
                                        nErrorCode = oCheckAnnulloTransazione.Error;
                                        object objectTrans = (oCheckAnnulloTransazione.Transazione != null && oCheckAnnulloTransazione.Transazione.Rows.Count > 0 ? oCheckAnnulloTransazione.Transazione.Rows[0] : null);
                                        frmPropertyObject frmShowError = new frmPropertyObject("Transazione non annullabile" + "\r\n" + cErrorDesc, objectTrans);
                                        frmShowError.ShowDialog();
                                        frmShowError.Close();
                                        frmShowError.Dispose();
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Nessuna transazione trovata", "Ricerca transazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show(oErr.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    this.UseWaitCursorForm = false;
                    MessageBox.Show("dati non corretti");
                }
            }
            catch (Exception ex)
            {
                oErr = ex;
                lRet = false;
                this.UseWaitCursorForm = false;
                MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }

        }

        private void cmbFunzione_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmbFunzione.Enabled)
            {
                this.SetControlFunzione();
            }
        }

        private void SetControlFunzione()
        {
            this.lblHelpFunzione.Text = "";
            this.dtFnzInizio.Enabled = false;
            this.dtFnzFine.Enabled = false;
            this.radioChkRPG.Enabled = false;
            this.radioChkRPM.Enabled = false;
            this.radioChkSIM.Enabled = false;
            this.chkBiglietti.Enabled = false;
            this.chkAbbonamenti.Enabled = false;
            this.radioSpettacolo.Enabled = false;
            this.radioIntrattenimento.Enabled = false;
            this.numIncidenza.Enabled = false;
            this.dtEvento.Enabled = false;
            this.txtIdCalend.Enabled = false;
            this.cmbOrdineDiPosto.Enabled = false;
            this.cmbTipoTitolo.Enabled = false;
            this.cmbTipoEvento.Enabled = false;
            this.cmbOrdineDiPosto.Enabled = false;
            this.cmbTitolo.Enabled = false;
            this.dtDataEmi.Enabled = false;
            this.numPrezzo.Enabled = false;
            this.numSuppl.Enabled = false;
            this.cmbVrfSale.Enabled = false;
            this.chkPerCalcoloOmaggio.Enabled = false;
            this.chkRicercaOmaggitransazione.Enabled = false;
            this.btnFunzione.Enabled = false;
            this.dgvDati.DataSource = null;
            this.cmbTipoTitolo.SelectedItem = null;
            this.cmbTipoEvento.SelectedItem = null;
            this.cmbOrdineDiPosto.SelectedItem = null;
            this.cmbVrfSale.SelectedItem = null;

            if (this.cmbFunzione.SelectedItem != null)
            {
                clsItemFunzione oFunzione = (clsItemFunzione)this.cmbFunzione.SelectedItem;
                switch (oFunzione.Nome)
                {
                    case "F_GET_CODICE_SISTEMA":
                        {
                            this.btnFunzione.Enabled = true;
                            break;
                        }
                    case "F_CHECK_STORICO":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data Inizio e selezionare il tipo di riepilogo (Giornaliero, Mensile, Simulazione)";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_INIZIO_MESE":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data Inizio";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_FINE_MESE":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data Inizio";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_TABLE_LOG_BASE_DATA":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.chkBiglietti.Enabled = true;
                            this.chkAbbonamenti.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire il periodo, il tipo di riepilogo (Giornaliero, Mensile, Simulazione) e se visualizzare sia biglietti che abbonamenti. Vengono estratti i log delle transazioni.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_TRANSAZIONI_RIEPILOGHI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.chkBiglietti.Enabled = true;
                            this.chkAbbonamenti.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire il periodo, il tipo di riepilogo (Giornaliero, Mensile, Simulazione) e se visualizzare sia biglietti che abbonamenti. Vengono estratte le righe calcolate che poi vengono ulteriormente elaborate da altre funzione finalizzate al savataggio e stampa dei riepiloghi.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_CALC_RIEPILOGHI_DETTAGLIO":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire il periodo, il tipo di riepilogo (Giornaliero, Mensile, Simulazione) e se visualizzare sia biglietti che abbonamenti. Vengono estratte le righe calcolate per essere salvate in RIEPILOGHI_BASE come base dati dei riepiloghi.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_CALC_RIEPILOGHI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire il periodo, il tipo di riepilogo (Giornaliero, Mensile, Simulazione). Vengono estratte le righe calcolate per essere utilizzate come base dati dei riepiloghi RPG/RPM e Simulazione. Le emissioni NON SONO al netto degli annullati.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_CALC_RIEPILOGHI_PRINT":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.radioChkSIM.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire il periodo, il tipo di riepilogo (Giornaliero, Mensile, Simulazione). Vengono estratte le righe calcolate per essere utilizzate come base dati per la stampa dei riepiloghi (moduli C1,C2). Le emissioni SONO al netto degli annullati.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_CALC_PROG_RIEPILOGO":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data di inizio ed il tipo di riepilogo (Giornaliero, Mensile, Simulazione). viene estratto il progressivo che verrebbe utilizzato per generare il prossimo riepilogo.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_RIEPILOGHI_CALC_NAME":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            //this.numProgressivo.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data di inizio ed il tipo di riepilogo (Giornaliero, Mensile, Simulazione). viene estratto il nome del file del riepilogo che verrebbe utilizzato per generare il prossimo riepilogo.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_RIEPILOGHI_CALC_TITOLI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.numProgressivo.Enabled = true;
                            this.lblHelpFunzione.Text = "Inserire la data di inizio, il tipo di riepilogo (Giornaliero, Mensile, Simulazione) ed il progressivo del riepilogo. Vengono estratti i titoli per evento legati al riepilogo che saranno poi salvati in RIEPILOGHI_TITOLI_EVENTI.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_READ_RIEPILOGHI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.lblHelpFunzione.Text = "Solo per riepiloghi generati. Inserire il periodo ed il tipo di riepilogo (Giornaliero, Mensile). Vengono estratte le righe di RIEPILOGHI_BASE accorpate per la generazione di RPG/RPM relative all'ultimo riepilogo generato per ogni giorno se giornaliero o mensile per i mesi del periodo inserito.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_READ_RIEPILOGHI_PRINT":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtFnzInizio.Enabled = true;
                            this.dtFnzFine.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.lblHelpFunzione.Text = "Solo per riepiloghi generati. Inserire il periodo ed il tipo di riepilogo (Giornaliero, Mensile). Vengono estratte le righe di RIEPILOGHI_BASE accorpate per la stampa (c1, c2) relative all'ultimo riepilogo generato per ogni giorno se giornaliero o mensile per i mesi del periodo inserito.";
                            this.dtFnzInizio.Focus();
                            break;
                        }
                    case "F_GETTABLE_IMPOSTE":
                        {
                            this.btnFunzione.Enabled = true;
                            this.dtDataEmi.Enabled = true;
                            this.cmbTipoEvento.Enabled = true;
                            this.radioSpettacolo.Enabled = true;
                            this.radioIntrattenimento.Enabled = true;
                            this.chkPerCalcoloOmaggio.Enabled = true;

                            this.lblHelpFunzione.Text = "Inserire il tipo evento, se spettacolo o intrattenimento, se il calcolo è riferito all'omaggio e la data di emissione. Ritorna la tabella delle imposte per data indicata.";
                            this.dtDataEmi.Focus();
                            break;
                        }
                    case "F_RIEPILOGHI_GETRIF_OMAGGIO":
                        {
                            this.btnFunzione.Enabled = true;
                            this.chkRicercaOmaggitransazione.Enabled = true;
                            this.dtEvento.Enabled = true;
                            this.cmbTitolo.Enabled = true;
                            this.cmbOrdineDiPosto.Enabled = true;
                            this.cmbTipoTitolo.Enabled = true;
                            this.txtIdCalend.Enabled = true;
                            this.cmbVrfSale.Enabled = true;
                            this.lblHelpFunzione.Text = "Calcola il biglietto di riferimento per l'omaggio. Inserire la data/ora dell'evento, il titolo, l'ordine di posto, il tipo titolo omaggio, l'eventuale IDCALEND. Indicare anche se devono essere considerate le transazioni fiscali o se il calcolo deve avvenire solo sul cinema.";
                            this.chkRicercaOmaggitransazione.Focus();
                            break;
                        }
                    case "F_TABLE_CALC_IVAISI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.cmbTipoEvento.Enabled = true;
                            this.radioSpettacolo.Enabled = true;
                            this.radioIntrattenimento.Enabled = true;
                            this.dtDataEmi.Enabled = true;
                            this.numIncidenza.Enabled = true;
                            this.numPrezzo.Enabled = true;
                            this.numSuppl.Enabled = true;
                            this.chkPerCalcoloOmaggio.Enabled = true;
                            this.lblHelpFunzione.Text = "Scegliere il tipo evento, se spettacolo o intrattenimento, l'incidenza dell'Intrattenimento, la data di emissione, Inserire prezzo e supplemento. Viene estratto il calcolo di IVA e IMPOSTA INTRATTENIMENTI. Indicare anche se il calcolo è per ottenere i valori di riferimento dell'omaggio.";
                            this.radioSpettacolo.Focus();
                            break;
                        }
                    case "F_INIT_ORGANIZZATORI":
                        {
                            this.btnFunzione.Enabled = true;
                            this.lblHelpFunzione.Text = "Ritorna la lista degli organizzatori.";
                            this.btnFunzione.Focus();
                            break;
                        }
                    case "F_TABLE_RIEPILOGHI_DA_GENERARE":
                        {
                            this.btnFunzione.Enabled = true;
                            this.radioChkRPG.Enabled = true;
                            this.radioChkRPM.Enabled = true;
                            this.lblHelpFunzione.Text = "Ritorna la lista dei riepiloghi da generare. Selezionare se Giornaliero o Mensile.";
                            this.btnFunzione.Focus();
                            break;
                        }
                }
            }
        }

        private void btnFunzione_Click(object sender, EventArgs e)
        {
            StringBuilder oSB = new StringBuilder();
            clsParameters oPars = new clsParameters();
            IRecordSet oRS = null;
            Exception oError = null;
            this.dgvDati.DataSource = null;
            this.lblOperazione.Text = (this.cmbFunzione.SelectedItem != null ? ((clsItemFunzione)this.cmbFunzione.SelectedItem).Nome + " " + ((clsItemFunzione)this.cmbFunzione.SelectedItem).Descrizione : "");
            this.UseWaitCursorForm = true;

            try
            {
                if (this.cmbFunzione.SelectedItem != null)
                {
                    clsItemFunzione oFunzione = (clsItemFunzione)this.cmbFunzione.SelectedItem;
                    switch (oFunzione.Nome)
                    {
                        case "F_GET_CODICE_SISTEMA":
                            {
                                string cCodiceSistema = clsRiepiloghi.GetCodiceSistema(this.oCon, out oError);
                                MessageBox.Show((oError == null ? cCodiceSistema : oError.ToString()), "Codice sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                        case "F_CHECK_STORICO":
                            {
                                oSB.Append("SELECT F_CHECK_STORICO(:pGIORNO, :pGIORNALIERO_MENSILE, 'CHECK') AS FLAG_STORICO FROM DUAL");
                                oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_INIZIO_MESE":
                            {
                                oSB.Append("SELECT F_INIZIO_MESE(:pGIORNO) AS DATA FROM DUAL");
                                oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_FINE_MESE":
                            {
                                oSB.Append("SELECT F_FINE_MESE(:pGIORNO) AS DATA FROM DUAL");
                                oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_TABLE_LOG_BASE_DATA":
                            {
                                if ((this.chkBiglietti.Checked && this.chkAbbonamenti.Checked) || (!this.chkBiglietti.Checked && !this.chkAbbonamenti.Checked))
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_TABLE_LOG_BASE_DATA(:pGIORNO_INIZIO, :pGIORNO_FINE, NULL, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                }
                                else
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_TABLE_LOG_BASE_DATA(:pGIORNO_INIZIO, :pGIORNO_FINE, :pTITOLO_ABBONAMENTO, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                }
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));

                                if (this.chkBiglietti.Checked && this.chkAbbonamenti.Checked)
                                {
                                }
                                else if (this.chkBiglietti.Checked)
                                {
                                    oPars.Add(":pTITOLO_ABBONAMENTO", "T");
                                }
                                else if (this.chkAbbonamenti.Checked)
                                {
                                    oPars.Add(":pTITOLO_ABBONAMENTO", "A");
                                }
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_TRANSAZIONI_RIEPILOGHI":
                            {
                                if ((this.chkBiglietti.Checked && this.chkAbbonamenti.Checked) || (!this.chkBiglietti.Checked && !this.chkAbbonamenti.Checked))
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_TRANSAZIONI_RIEPILOGHI(:pGIORNO_INIZIO, :pGIORNO_FINE, NULL, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                }
                                else
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_TRANSAZIONI_RIEPILOGHI(:pGIORNO_INIZIO, :pGIORNO_FINE, :pTITOLO_ABBONAMENTO, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                }
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));

                                if ((this.chkBiglietti.Checked && this.chkAbbonamenti.Checked) || (!this.chkBiglietti.Checked && !this.chkAbbonamenti.Checked))
                                {
                                }
                                else if (this.chkBiglietti.Checked)
                                {
                                    oPars.Add(":pTITOLO_ABBONAMENTO", "T");
                                }
                                else if (this.chkAbbonamenti.Checked)
                                {
                                    oPars.Add(":pTITOLO_ABBONAMENTO", "A");
                                }
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_CALC_RIEPILOGHI_DETTAGLIO":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_CALC_RIEPILOGHI_DETTAGLIO(:pGIORNO_INIZIO, :pGIORNO_FINE, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_CALC_RIEPILOGHI":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_CALC_RIEPILOGHI(:pGIORNO_INIZIO, :pGIORNO_FINE, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_CALC_RIEPILOGHI_PRINT":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_CALC_RIEPILOGHI_PRINT(:pGIORNO_INIZIO, :pGIORNO_FINE, :pGIORNALIERO_MENSILE, 'CHECK', 0))");
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_CALC_PROG_RIEPILOGO":
                            {
                                oSB.Append("SELECT F_CALC_PROG_RIEPILOGO(:pGIORNO, :pGIORNALIERO_MENSILE) AS PROGRESSIVO FROM DUAL");
                                oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_RIEPILOGHI_CALC_NAME":
                            {
                                if (this.numProgressivo.Value > 0)
                                {
                                    oSB.Append("SELECT F_RIEPILOGHI_CALC_NAME(:pGIORNO, :pGIORNALIERO_MENSILE, :pPROGRESSIVO) AS NOME FROM DUAL");
                                    oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                    oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                    oPars.Add(":pPROGRESSIVO", this.numProgressivo.Value);
                                    try
                                    {
                                        oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                    }
                                    catch (Exception exFunzione)
                                    {
                                        MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        oRS = null;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Indicare un progressivo riepilogo.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                        case "F_RIEPILOGHI_CALC_TITOLI":
                            {
                                if (this.numProgressivo.Value > 0)
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_RIEPILOGHI_CALC_TITOLI(:pGIORNO, :pGIORNALIERO_MENSILE, :pPROGRESSIVO))");
                                    oPars.Add(":pGIORNO", this.dtFnzInizio.Value.Date);
                                    oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                    oPars.Add(":pPROGRESSIVO", this.numProgressivo.Value);
                                    try
                                    {
                                        oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                    }
                                    catch (Exception exFunzione)
                                    {
                                        MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        oRS = null;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Indicare un progressivo riepilogo.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                break;
                            }
                        case "F_READ_RIEPILOGHI":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_READ_RIEPILOGHI(:pGIORNO_INIZIO, :pGIORNO_FINE, :pGIORNALIERO_MENSILE))");
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_READ_RIEPILOGHI_PRINT":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_READ_RIEPILOGHI_PRINT(:pGIORNO_INIZIO, :pGIORNO_FINE, :pGIORNALIERO_MENSILE))");
                                oPars.Add(":pGIORNO_INIZIO", this.dtFnzInizio.Value.Date);
                                oPars.Add(":pGIORNO_FINE", this.dtFnzFine.Value.Date);
                                oPars.Add(":pGIORNALIERO_MENSILE", (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D")));
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_GETTABLE_IMPOSTE":
                            {
                                if (this.cmbTipoEvento.SelectedItem != null)
                                {
                                    oSB.Append("SELECT");
                                    oSB.Append(" TAB_IMPOSTE.TIPO_EVENTO, ");
                                    oSB.Append(" TAB_IMPOSTE.SPETTACOLO_INTRATTENIMENTO,");
                                    oSB.Append(" TAB_IMPOSTE.INIZIO_VALIDITA, ");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_OMAGGIO, ");
                                    oSB.Append(" TAB_IMPOSTE.IMPORTO_LORDO, ");
                                    oSB.Append(" TAB_IMPOSTE.PERC_IVA,");
                                    oSB.Append(" TAB_IMPOSTE.PERC_ISI,");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_IRRIPARTIBILE, ");
                                    oSB.Append(" TAB_IMPOSTE.VAL_IVA_FISSA, ");
                                    oSB.Append(" TAB_IMPOSTE.VAL_ISI_FISSA, ");
                                    oSB.Append(" TAB_IMPOSTE.NETTO_FISSO");
                                    oSB.Append(" FROM");
                                    oSB.Append(" TABLE(F_GETTABLE_IMPOSTE()) TAB_IMPOSTE, ");
                                    oSB.Append(" (");
                                    oSB.Append(" SELECT ");
                                    oSB.Append(" TIPO_EVENTO, SPETTACOLO_INTRATTENIMENTO, FLAG_OMAGGIO, IMPORTO_LORDO, FLAG_IRRIPARTIBILE, VAL_IVA_FISSA, VAL_ISI_FISSA, NETTO_FISSO, ");
                                    oSB.Append(" MAX(INIZIO_VALIDITA) AS INIZIO_VALIDITA ");
                                    oSB.Append(" FROM TABLE(F_GETTABLE_IMPOSTE())");
                                    oSB.Append(" WHERE");
                                    oSB.Append(" INIZIO_VALIDITA <= :pDATA_EMI ");
                                    oSB.Append(" AND TIPO_EVENTO = :pTIPO_EVENTO");
                                    oSB.Append(" AND SPETTACOLO_INTRATTENIMENTO = :pSPETTACOLO_INTRATTENIMENTO");
                                    oSB.Append(" AND FLAG_OMAGGIO = :pFLAG_OMAGGIO");
                                    oSB.Append(" GROUP BY");
                                    oSB.Append(" TIPO_EVENTO, SPETTACOLO_INTRATTENIMENTO, FLAG_OMAGGIO, IMPORTO_LORDO, FLAG_IRRIPARTIBILE, VAL_IVA_FISSA, VAL_ISI_FISSA, NETTO_FISSO");
                                    oSB.Append(" ) MAX_VALIDITA");
                                    oSB.Append(" WHERE");
                                    oSB.Append(" TAB_IMPOSTE.TIPO_EVENTO = MAX_VALIDITA.TIPO_EVENTO");
                                    oSB.Append(" AND ");
                                    oSB.Append(" TAB_IMPOSTE.SPETTACOLO_INTRATTENIMENTO = MAX_VALIDITA.SPETTACOLO_INTRATTENIMENTO");
                                    oSB.Append(" AND ");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_OMAGGIO = MAX_VALIDITA.FLAG_OMAGGIO ");
                                    oSB.Append(" AND");
                                    oSB.Append(" TAB_IMPOSTE.IMPORTO_LORDO = MAX_VALIDITA.IMPORTO_LORDO");
                                    oSB.Append(" AND ");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_IRRIPARTIBILE = MAX_VALIDITA.FLAG_IRRIPARTIBILE");
                                    oSB.Append(" AND");
                                    oSB.Append(" TAB_IMPOSTE.VAL_IVA_FISSA = MAX_VALIDITA.VAL_IVA_FISSA");
                                    oSB.Append(" AND ");
                                    oSB.Append(" TAB_IMPOSTE.VAL_ISI_FISSA = MAX_VALIDITA.VAL_ISI_FISSA ");
                                    oSB.Append(" AND");
                                    oSB.Append(" TAB_IMPOSTE.NETTO_FISSO = MAX_VALIDITA.NETTO_FISSO");
                                    oSB.Append(" AND");
                                    oSB.Append(" TAB_IMPOSTE.INIZIO_VALIDITA = MAX_VALIDITA.INIZIO_VALIDITA");
                                    oSB.Append(" ORDER BY");
                                    oSB.Append(" TAB_IMPOSTE.TIPO_EVENTO, ");
                                    oSB.Append(" TAB_IMPOSTE.SPETTACOLO_INTRATTENIMENTO, ");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_OMAGGIO, ");
                                    oSB.Append(" TAB_IMPOSTE.IMPORTO_LORDO, ");
                                    oSB.Append(" TAB_IMPOSTE.FLAG_IRRIPARTIBILE, ");
                                    oSB.Append(" TAB_IMPOSTE.VAL_IVA_FISSA, ");
                                    oSB.Append(" TAB_IMPOSTE.VAL_ISI_FISSA, ");
                                    oSB.Append(" TAB_IMPOSTE.NETTO_FISSO");

                                    oPars.Add(":pTIPO_EVENTO", ((clsItemCombo)this.cmbTipoEvento.SelectedItem).Value.ToString());
                                    oPars.Add(":pSPETTACOLO_INTRATTENIMENTO", (this.radioSpettacolo.Checked ? "S" : "I"));
                                    oPars.Add(":pDATA_EMI", this.dtDataEmi.Value.Date);
                                    oPars.Add(":pFLAG_OMAGGIO", (this.chkPerCalcoloOmaggio.Checked ? "T" : "F"));

                                    try
                                    {
                                        oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                    }
                                    catch (Exception exFunzione)
                                    {
                                        MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        oRS = null;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Indicare un tipo evento.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                        case "F_RIEPILOGHI_GETRIF_OMAGGIO":
                            {
                                if (true)
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_RIEPILOGHI_GETRIF_OMAGGIO(:pFLAG_RICERCA_NEI_LOG, :pCODICE_LOCALE, :pDATA_ORA_EVENTO, :pTITOLO_EVENTO, :pORDINE_DI_POSTO, :pTIPO_TITOLO, :pIDCALEND))");

                                    oPars.Add(":pFLAG_RICERCA_NEI_LOG", (this.chkRicercaOmaggitransazione.Checked ? "TRUE" : "FALSE"));
                                    oPars.Add(":pCODICE_LOCALE", ((clsItemCombo)this.cmbVrfSale.SelectedItem).Value.ToString());
                                    oPars.Add(":pDATA_ORA_EVENTO", this.dtEvento.Value);
                                    oPars.Add(":pTITOLO_EVENTO", ((clsItemCombo)this.cmbTitolo.SelectedItem).Value.ToString().PadRight(40));
                                    oPars.Add(":pORDINE_DI_POSTO", ((clsItemCombo)this.cmbOrdineDiPosto.SelectedItem).Value.ToString());
                                    oPars.Add(":pTIPO_TITOLO", ((clsItemCombo)this.cmbTipoTitolo.SelectedItem).Value.ToString());
                                    Int64 nIdCalend = 0;
                                    if (!Int64.TryParse(this.txtIdCalend.Text, out nIdCalend))
                                    {
                                        nIdCalend = 0;
                                    }
                                    oPars.Add(":pIDCALEND", nIdCalend);
                                    try
                                    {
                                        oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                    }
                                    catch (Exception exFunzione)
                                    {
                                        MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        oRS = null;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Indicare un titolo, una sala, un ordine di posto ed un tipo titolo.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                        case "F_TABLE_CALC_IVAISI":
                            {
                                if (this.cmbTipoEvento.SelectedItem != null)
                                {
                                    oSB.Append("SELECT * FROM TABLE(F_TABLE_CALC_IVAISI(:pTIPO_EVENTO, :pSPETTACOLO_INTRATTENIMENTO, :pDATA_EMI, :pINCIDENZA_INTRATTENIMENTO, :pCORRISPETTIVO_TITOLO, :pCORRISPETTIVO_PREVENDITA, :pPERC_IVA, :pPERC_INTRATTENIMENTO, :pFLAG_OMAGGIO))");

                                    oPars.Add(":pTIPO_EVENTO", ((clsItemCombo)this.cmbTipoEvento.SelectedItem).Value.ToString());
                                    oPars.Add(":pSPETTACOLO_INTRATTENIMENTO", (this.radioSpettacolo.Checked ? "S" : "I"));
                                    oPars.Add(":pDATA_EMI", this.dtDataEmi.Value.Date);
                                    oPars.Add(":pINCIDENZA_INTRATTENIMENTO", this.numIncidenza.Value);
                                    oPars.Add(":pCORRISPETTIVO_TITOLO", this.numPrezzo.Value);
                                    oPars.Add(":pCORRISPETTIVO_PREVENDITA", this.numSuppl.Value);
                                    oPars.Add(":pFLAG_OMAGGIO", (this.chkPerCalcoloOmaggio.Checked ? "T" : "F"));
                                    oPars.Add(":pPERC_IVA", 0);
                                    oPars.Add(":pPERC_INTRATTENIMENTO", 0);
                                    try
                                    {
                                        oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);
                                    }
                                    catch (Exception exFunzione)
                                    {
                                        MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        oRS = null;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Indicare un tipo evento.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                        case "F_INIT_ORGANIZZATORI":
                            {
                                oSB.Append("SELECT * FROM TABLE(F_INIT_ORGANIZZATORI())");
                                try
                                {
                                    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                        case "F_TABLE_RIEPILOGHI_DA_GENERARE":
                            {
                                Exception oErrorRiepiloghiDagenerare = null;
                                try
                                {
                                    oRS = clsRiepiloghi.GetRiepiloghiDaGenerare(this.oCon, (this.radioChkRPG.Checked ? "G" : "M"), out oErrorRiepiloghiDagenerare);
                                    if (oErrorRiepiloghiDagenerare != null)
                                    {
                                        oRS = null;
                                        MessageBox.Show(oErrorRiepiloghiDagenerare.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                catch (Exception exFunzione)
                                {
                                    MessageBox.Show(exFunzione.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    oRS = null;
                                }
                                break;
                            }
                    }
                    if (oRS != null)
                    {
                        this.dgvDati.DataSource = (System.Data.DataTable)oRS;
                        this.dgvDati.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    }
                }
            }
            catch(Exception ex)
            {
                this.UseWaitCursorForm = false;
                MessageBox.Show("Errore " + ex.ToString());
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }

        }

        private void radioChkSIM_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioChkSIM.Checked)
            {
                //this.btnChkREAD.Enabled = false;
            }
            else
            {
                //this.btnChkREAD.Enabled = true;
            }
        }

        private void btnChkCALC_Click(object sender, EventArgs e)
        {
            //bool lRet = false;
            //Exception oErr = null;
            //this.dgvDati.DataSource = null;
            //this.lblOperazione.Text = "Caricamento...";
            //this.UseWaitCursorForm = true;
            //try
            //{

            //    string GiornalieroMensile = (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D"));
            //    DateTime Giorno = (GiornalieroMensile == "M" ? new DateTime(this.dtChkGiorno.Value.Date.Year, this.dtChkGiorno.Value.Date.Month, 1) : this.dtChkGiorno.Value.Date);
            //    Int64 Progressivo = clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, this.oCon, out oErr);
            //    DateTime dStart = DateTime.Now;
            //    IRecordSet oRS = null;
            //    StringBuilder oSB = new StringBuilder();
            //    clsParameters oPars = new clsParameters();
                
            //    if (this.chkChkComeStampa.Checked)
            //    {
            //        oSB.Append("SELECT * FROM TABLE(F_CALC_RIEPILOGHI_PRINT(:pGIORNO, :pGIORNO, :pGIORNALIERO_MENSILE, 'TRUE')) WHERE GIORNO_MESE = :pGIORNO AND TIPO_RIEPILOGO = :pGIORNALIERO_MENSILE");
            //    }
            //    else
            //    {
            //        oSB.Append("SELECT * FROM TABLE(F_CALC_RIEPILOGHI(:pGIORNO, :pGIORNO, :pGIORNALIERO_MENSILE, 'TRUE')) WHERE GIORNO_MESE = :pGIORNO AND TIPO_RIEPILOGO = :pGIORNALIERO_MENSILE");
            //    }

            //    oPars.Add(":pGIORNO", Giorno);
            //    oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
            //    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);         

            //    if (oErr == null)
            //    {
            //        lRet = true;
            //        this.UseWaitCursorForm = false;
            //        MessageBox.Show("caricamento terminato " + oRS.RecordCount.ToString() + " righe" + "\r\n" +
            //                       " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            //        this.dgvDati.DataSource = (System.Data.DataTable)oRS;
            //        this.dgvDati.Columns[0].Visible = false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    oErr = ex;
            //    lRet = false;
            //}
            //finally
            //{
            //    this.UseWaitCursorForm = false;
            //}

            //if (!lRet)
            //{
            //    MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            //}
        }

        private void btnChkREAD_Click(object sender, EventArgs e)
        {
            //bool lRet = false;
            //Exception oErr = null;
            //this.dgvDati.DataSource = null;
            //this.lblOperazione.Text = "Caricamento...";
            //this.UseWaitCursorForm = true;
            //try
            //{

            //    string GiornalieroMensile = (this.radioChkRPG.Checked ? "G" : (this.radioChkRPM.Checked ? "M" : "D"));
            //    DateTime Giorno = (GiornalieroMensile == "M" ? new DateTime(this.dtChkGiorno.Value.Date.Year, this.dtChkGiorno.Value.Date.Month, 1) : this.dtChkGiorno.Value.Date);
            //    Int64 Progressivo = clsRiepiloghi.GetLastProgRiepilogo(Giorno, GiornalieroMensile, this.oCon, out oErr);
            //    DateTime dStart = DateTime.Now;
            //    IRecordSet oRS = null;
            //    StringBuilder oSB = new StringBuilder();
            //    clsParameters oPars = new clsParameters();

            //    oPars.Add(":pGIORNO", Giorno);
            //    oPars.Add(":pGIORNALIERO_MENSILE", GiornalieroMensile);
            //    if (this.chkChkComeStampa.Checked)
            //    {
            //        oPars.Add(":pPROG_RIEPILOGO", Progressivo);
            //        oSB.Append("SELECT " + clsRiepiloghi.GetCampiDettaglioRiepiloghi() + " FROM TABLE(F_READ_RIEPILOGHI_PRINT(:pGIORNO, :pGIORNO, :pGIORNALIERO_MENSILE)) WHERE GIORNO_MESE = :pGIORNO AND TIPO_RIEPILOGO = :pGIORNALIERO_MENSILE AND PROG_RIEPILOGO = :pPROG_RIEPILOGO");
            //    }
            //    else
            //    {
            //        oSB.Append("SELECT " + clsRiepiloghi.GetCampiDettaglioRiepiloghi() + " FROM TABLE(F_READ_RIEPILOGHI(:pGIORNO, :pGIORNO, :pGIORNALIERO_MENSILE)) WHERE GIORNO_MESE = :pGIORNO AND TIPO_RIEPILOGO = :pGIORNALIERO_MENSILE");
            //    }

            //    oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString(), oPars);

            //    if (oErr == null)
            //    {
            //        lRet = true;
            //        this.UseWaitCursorForm = false;
            //        MessageBox.Show("caricamento terminato " + oRS.RecordCount.ToString() + " righe" + "\r\n" +
            //                       " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
            //        this.dgvDati.DataSource = (System.Data.DataTable)oRS;
            //        this.dgvDati.Columns[0].Visible = false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    oErr = ex;
            //    lRet = false;
            //}
            //finally
            //{
            //    this.UseWaitCursorForm = false;
            //}

            //if (!lRet)
            //{
            //    MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            //}
        }

        private bool UseWaitCursorForm
        {
            set
            {
                this.pnlServizio.Enabled = !value;
                this.pnlDati.Enabled = !value;
                if (!value)
                {
                    this.lblOperazione.Text = "...";
                }
                this.UseWaitCursor = value;
                this.Refresh();
                Application.DoEvents();
            }
        }

        private void radioRPG_RPM_SIM_CheckedChanged(object sender, EventArgs e)
        {
            this.SetControlRiepiloghi();
        }

        private void dtGiorno_ValueChanged(object sender, EventArgs e)
        {
            this.SetControlRiepiloghi();
        }

        private void SetControlRiepiloghi()
        {
            if (this.radioSIM.Checked)
            {
                this.btnPrint.Enabled = true;
                this.btnGenera.Enabled = false;
                this.lblMsgRP.Text = "Simulazione per data evento";
                this.chkListSale.Enabled = true;
            }
            else
            {
                this.btnGenera.Enabled = true;
                this.chkListSale.Enabled = false;
                this.btnPrint.Enabled = (this.GetProgNextRiepilogo() > 1);
                this.btnPrint.Enabled = true;
                if (this.GetProgNextRiepilogo() > 1)
                {
                    this.lblMsgRP.Text = "Riepilogo esistente";
                }
                else
                {
                    this.lblMsgRP.Text = "Riepilogo da creare";
                }
            }
        }

        private void InitComboBoxes()
        {
            try
            {
                this.chkListSale.Items.Clear();
                this.cmbVrfSale.Items.Clear();
                StringBuilder oSB = new StringBuilder("SELECT DESCR, ALIAS, CODICE_LOCALE FROM VR_SALE ORDER BY IDSALA");
                IRecordSet oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    clsItemCombo oItemCheck = new clsItemCombo(oRS.Fields("codice_locale").Value.ToString(), oRS.Fields("descr").Value.ToString() + " " + oRS.Fields("alias").Value.ToString());
                    oItemCheck.ValueVisible = false;
                    this.chkListSale.Items.Add(oItemCheck);
                    this.cmbVrfSale.Items.Add(new clsItemCombo(oRS.Fields("codice_locale").Value.ToString(), oRS.Fields("descr").Value.ToString() + " " + oRS.Fields("alias").Value.ToString()));
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch
            {
            }

            try
            {
                this.cmbTipoEvento.Items.Clear();
                StringBuilder oSB = new StringBuilder("SELECT CODICE, DESCRIZIONE FROM VR_TIPOEVENTO ORDER BY CODICE");
                IRecordSet oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    this.cmbTipoEvento.Items.Add(new clsItemCombo(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString()));
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch
            {
            }

            try
            {
                this.cmbOrdineDiPosto.Items.Clear();
                StringBuilder oSB = new StringBuilder("SELECT CODICE, DESCRIZIONE FROM VR_ORDINIDIPOSTO ORDER BY CODICE");
                IRecordSet oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    this.cmbOrdineDiPosto.Items.Add(new clsItemCombo(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString()));
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch
            {
            }

            try
            {
                this.cmbTipoTitolo.Items.Clear();
                StringBuilder oSB = new StringBuilder("SELECT CODICE, DESCRIZIONE FROM VR_TIPOTITOLO ORDER BY CODICE");
                IRecordSet oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    this.cmbTipoTitolo.Items.Add(new clsItemCombo(oRS.Fields("codice").Value.ToString(), oRS.Fields("descrizione").Value.ToString()));
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch
            {
            }

            try
            {
                this.cmbTitolo.Items.Clear();
                StringBuilder oSB = new StringBuilder("SELECT DESCRIZIONE FROM VR_PACCHETTO GROUP BY DESCRIZIONE ORDER BY DESCRIZIONE");
                IRecordSet oRS = (IRecordSet)this.oCon.ExecuteQuery(oSB.ToString());
                while (!oRS.EOF)
                {
                    this.cmbTitolo.Items.Add(new clsItemCombo(oRS.Fields("descrizione").Value.ToString(), oRS.Fields("descrizione").Value.ToString()));
                    oRS.MoveNext();
                }
                oRS.Close();
            }
            catch
            {
            }
        
        }

        

        private Int64 GetProgNextRiepilogo()
        {
            Int64 nRet = 0;
            try
            {
                StringBuilder oSB;
                clsParameters oPars;
                IRecordSet oRS;

                oSB = new StringBuilder();
                oPars = new clsParameters();
                oSB.Append("SELECT F_CALC_PROG_RIEPILOGO(:pGIORNO, :pGIORNALIERO_MENSILE) AS PROG_RIEPILOGO FROM DUAL");
                oPars.Add(":pGIORNO", this.dtGiorno.Value.Date);
                oPars.Add(":pGIORNALIERO_MENSILE", this.radioRPG.Checked ? "G" : "M");

                oRS = (IRecordSet)oCon.ExecuteQuery(oSB.ToString(), oPars);

                if (!oRS.EOF && !oRS.Fields("prog_riepilogo").IsNull)
                {
                    nRet = Int64.Parse(oRS.Fields("prog_riepilogo").Value.ToString());
                }
                oRS.Close();
            }
            catch
            {
            }
            return nRet;
        }

        private void btnGenera_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Exception oErr = null;
            DateTime Giorno = this.dtGiorno.Value.Date;
            string GiornalieroMensile = (this.radioRPG.Checked ? "G" : (this.radioRPM.Checked ? "M" : (this.radioSIM.Checked ? "D" : "R")));
            this.lblOperazione.Text = "Generazione...";
            this.UseWaitCursorForm = true;


            //Riepiloghi.clsRiepiloghi.RicezioneMail(oCon, out oErr);

            try
            {
                Riepiloghi.clsWinServiceClientOperation oClientOperation = new Riepiloghi.clsWinServiceClientOperation(Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);
                long IdOperazione = 0;

                clsWinServiceConfig config = clsWinServiceConfig.GetWinServiceConfig(oCon, out oErr, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE);
                lRet = oErr == null;
                //if (lRet)
                //{
                //    if (config.MachineName.Trim().ToUpper() != System.Environment.MachineName.Trim().ToUpper() || config.ApplicationType == clsWinServiceConfig.WinServiceAPPLICATION_TYPE_CONSOLEMODE)
                //    {
                //        oErr = new Exception(string.Format("Configurazione PC: {0} TIPO APPLICAZIONE: {1} COMUNICAZIONE: {2}" + "\r\n" + "in Esecuzione PC: {3} TIPO APPLICAZIONE: WINDESK/WINSRV COMUNICAZIONE: {4}",
                //                             config.MachineName, config.ApplicationType, config.ComunicationMode, Environment.MachineName, "SOCKET"));
                //    }
                //}

                lRet = oErr == null;

                if (lRet)
                {
                    Riepiloghi.clsRiepilogoGenerato dettaglioRiepilogo = null;
                    System.IO.FileInfo oFileInfoApplication = new System.IO.FileInfo(Application.ExecutablePath);
                    string pathRisorse = oFileInfoApplication.Directory.FullName + @"\Risorse";
                    lRet = oClientOperation.PushOperazione(oCon, "P0000399", "C3E82E0CC8BC4E64AEE656C5B7E62A5C", pathRisorse, Riepiloghi.clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO, GiornalieroMensile, Giorno, 0, out IdOperazione, out dettaglioRiepilogo, out oErr);
                }

                if (lRet)
                {

                    clsWinServiceModel_request_Operation request = null;
                    clsWinServiceModel_Token token = null;


                    if (clsWinServiceServerOperation.GetOperationToExecute(oCon, out request, out token, out oErr, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WEB_APIMODE))
                    {
                        clsWinServiceServerOperation oServer = new clsWinServiceServerOperation("RIEPILOGHI-CLIENT");
                        Riepiloghi.clsRiepilogoGenerato dettaglioRiepilogo = null;
                        oServer.ElaboraRichiestaRiepilogo(request, token, "", out oErr, out dettaglioRiepilogo);


                        try
                        {
                            StringBuilder oSB = new StringBuilder();
                            Wintic.Data.clsParameters oPars = new Wintic.Data.clsParameters();

                            oPars.Add(":pIDOPERAZIONE", token.IdOperazione);
                            oPars.Add(":pSTATO_OPERAZIONE", 2);

                            oSB.Append("UPDATE RIEPILOGHI_OPERAZIONI_REQ SET ");
                            oSB.Append(" STATO_OPERAZIONE = :pSTATO_OPERAZIONE ");

                            if (oErr != null)
                            {
                                oSB.Append(" , ERRORE = :pERRORE");
                                oPars.Add(":pERRORE", (oErr.Message.Length > 4000 ? oErr.Message.Substring(0, 3999) : oErr.Message));
                            }
                            oSB.Append(" WHERE IDOPERAZIONE = :pIDOPERAZIONE");
                            oCon.ExecuteNonQuery(oSB.ToString(), oPars, true);
                        }
                        catch (Exception ex)
                        {
                        }
                        System.Reflection.Assembly oAssembly = null;
                        libRiepiloghiBase.iLib_SIAE_Provider providerSIAE = libRiepiloghiBase.clsLibSigillo.GetInstanceLibSIAE(out oErr, out oAssembly);
                        if (oErr == null)
                        {
                            bool lSentEmail = clsRiepiloghi.SpedizioneEmail(request.RequestRiepilogo.Giorno, request.RequestRiepilogo.GiornalieroMensile, request.RequestRiepilogo.Progressivo, providerSIAE, oCon, out oErr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oErr = ex;
                lRet = false;
                MessageBox.Show("Errore generazione riepilogo" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            }
            finally
            {
                this.UseWaitCursorForm = false;
                this.SetControlRiepiloghi();
            }
            if (oErr != null)
                MessageBox.Show("Errore generazione riepilogo" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Exception oErr = null;
            DateTime Giorno = this.dtGiorno.Value.Date;
            string GiornalieroMensile = this.radioRPG.Checked ? "G" : (this.radioRPM.Checked ? "M" : (this.radioRCA.Checked ? "R" : "D"));
            string cFilePdf = "";
            DateTime dStart = DateTime.Now;
            this.lblOperazione.Text = "Stampa...";
            this.UseWaitCursorForm = true;
            
            try
            {
                string FilePdfToSave = @"c:\riepiloghi\text.pdf";
                System.IO.MemoryStream PdfStream = null;

                if (!string.IsNullOrEmpty(FilePdfToSave) && !string.IsNullOrWhiteSpace(FilePdfToSave) && System.IO.File.Exists(FilePdfToSave))
                {
                    System.IO.File.Delete(FilePdfToSave);
                }

                byte[] buffer;

                if (GiornalieroMensile == "D")
                {
                    string[] CodiciLocali = null;
                    if (this.chkListSale.CheckedItems != null && this.chkListSale.CheckedItems.Count > 0)
                    {
                        CodiciLocali = new string[this.chkListSale.CheckedItems.Count];
                        int IndexCodiceLocale = 0;
                        foreach (clsItemCombo ItemChkSala in this.chkListSale.CheckedItems)
                        {
                            CodiciLocali[IndexCodiceLocale] = ItemChkSala.Value.ToString();
                            IndexCodiceLocale += 1;
                        }
                    }
                    lRet = clsRiepiloghi.PrintRiepilogoToPDF(Giorno, "D", 0, oCon, clsRiepiloghi.DefModalitaStorico.Check, this.chkDettagli.Checked, out oErr, FilePdfToSave, out buffer, null, 0);
                }
                else if (GiornalieroMensile == "R")
                {
                    long nProg = 0;
                    lRet = clsRiepiloghi.PrintRCAToPDF(Giorno, GiornalieroMensile, nProg, oCon, clsRiepiloghi.DefModalitaStorico.Check, this.chkDettagli.Checked, out oErr, FilePdfToSave, out buffer, null, 0);
                }
                else
                {
                    Int64 nProg = this.GetProgNextRiepilogo();
                    lRet = clsRiepiloghi.PrintRiepilogoToPDF(Giorno, GiornalieroMensile, nProg, oCon, clsRiepiloghi.DefModalitaStorico.Check, this.chkDettagli.Checked, out oErr, FilePdfToSave, out buffer, null, 0);
                }
                this.UseWaitCursorForm = false;
                if (lRet)
                {
                    //MessageBox.Show("Stampa riepilogo terminata" + "\r\n" +
                    //                " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    ////stringPdf

                    System.IO.File.WriteAllBytes(@"c:\wintic\test.pdf", buffer);
                    Application.Exit();

                    //System.IO.File.WriteAllBytes

                    //if (!string.IsNullOrEmpty(FilePdfToSave) && !string.IsNullOrWhiteSpace(FilePdfToSave) && System.IO.File.Exists(FilePdfToSave))
                    //{
                    //    System.Threading.Thread.Sleep(5000);
                    //    System.Diagnostics.Process oRun = new System.Diagnostics.Process();
                    //    oRun.StartInfo.FileName = FilePdfToSave;
                    //    oRun.Start();
                    //}
                }
                else
                {
                    if (oErr != null)
                    {
                        MessageBox.Show("Errore stampa" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
                    }
                    else
                    {
                        //MessageBox.Show("Nessun dato da stampare.");
                    }
                }
            }
            catch
            {
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }
           
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Exception oErr = null;
            this.dgvDati.DataSource = null;
            this.lblOperazione.Text = "Caricamento...";
            this.UseWaitCursorForm = true;
            try
            {
                IRecordSet oRS;
                DateTime dStart = DateTime.Now;
                if (this.radioTransazioni.Checked)
                {
                    oRS = clsRiepiloghi.GetTransazioni(this.dtInizio.Value.Date, this.dtFine.Value.Date, true, true, null, oCon, clsRiepiloghi.DefModalitaStorico.True, out oErr, 0, null);
                }
                else
                {
                    oRS = clsRiepiloghi.GetTransazioniAnnullati(this.dtInizio.Value.Date, this.dtFine.Value.Date, true, oCon, clsRiepiloghi.DefModalitaStorico.True, out oErr, 0, null);
                }

                if (oErr == null)
                {
                    lRet = true;
                    this.UseWaitCursorForm = false;
                    MessageBox.Show("caricamento terminato " + oRS.RecordCount.ToString() + " righe" + "\r\n" +
                                    " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                    this.dgvDati.DataSource = (System.Data.DataTable)oRS;
                    this.dgvDati.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                oErr = ex;
                lRet = false;
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }

            if (!lRet)
            {
                MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            }
        }


        private void btnFiltri_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            frmCampiTransazioni oFrmCampi;
            this.radioAnnulli.Checked = false;
            this.radioTransazioni.Checked = true;
            System.Collections.SortedList oFilters = new System.Collections.SortedList();

            oFrmCampi = new frmCampiTransazioni(oCon);
            oFrmCampi.Help = "Selezionare i campi ed i rispettivi valori che devono assumere";
            lRet = (oFrmCampi.ShowDialog() == System.Windows.Forms.DialogResult.OK);

            if (lRet)
            {
                oFilters = oFrmCampi.GetListValues(false);
            }

            oFrmCampi.Close();
            oFrmCampi.Dispose();

            if (lRet)
            {
                Exception oErr = null;
                this.dgvDati.DataSource = null;
                this.lblOperazione.Text = "Caricamento...";
                this.UseWaitCursorForm = true;
                try
                {
                    IRecordSet oRS;
                    DateTime dStart = DateTime.Now;
                    oRS = clsRiepiloghi.GetTransazioni(this.dtInizio.Value.Date, this.dtFine.Value.Date, true, true, oFilters, oCon, clsRiepiloghi.DefModalitaStorico.True, out oErr, 0, null);

                    if (oErr == null)
                    {
                        lRet = true;
                        this.UseWaitCursorForm = false;
                        MessageBox.Show("caricamento terminato " + oRS.RecordCount.ToString() + " righe" + "\r\n" +
                                       " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        this.dgvDati.DataSource = (System.Data.DataTable)oRS;
                        this.dgvDati.Columns[0].Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    oErr = ex;
                    lRet = false;
                }
                finally
                {
                    this.UseWaitCursorForm = false;
                }

                if (!lRet)
                {
                    MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
                }
            }
        }


        private void btnCerca_Click(object sender, EventArgs e)
        {
            bool lRet = false;
            Exception oErr = null;
            this.dgvDati.DataSource = null;
            this.lblOperazione.Text = "Ricerca...";
            this.UseWaitCursorForm = true;
            try
            {
                IRecordSet oRS;
                DateTime dStart = DateTime.Now;
                Int64 nProg = 0;
                string cCarta = this.txtCarta.Text;
                string cSigillo = this.txtSigillo.Text;

                if (cCarta.Length == 8 &&
                    (cSigillo.Length == 16 || Int64.TryParse(this.txtProggressivo.Text, out nProg)))
                {
                    if (cSigillo.Length == 16)
                    {
                        oRS = clsRiepiloghi.GetTransazioneCARTA_SIGILLO(cCarta, cSigillo, true, oCon, out oErr);
                    }
                    else
                    {
                        oRS = clsRiepiloghi.GetTransazioneCARTA_PROGRESSIVO(cCarta, nProg, true, oCon, out oErr);
                    }
                    this.UseWaitCursorForm = false;
                    if (oErr == null && oRS != null && !oRS.EOF)
                    {
                        lRet = true;
                        MessageBox.Show("caricamento terminato" + "\r\n" +
                                       " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart));
                        this.dgvDati.DataSource = (System.Data.DataTable)oRS;
                        this.dgvDati.Columns[0].Visible = false;
                    }
                    else if (oErr == null && oRS != null && oRS.EOF)
                    {
                        MessageBox.Show("caricamento terminato" + "\r\n" +
                                       " in " + clsRiepiloghi.TimeSpanToLongTimeFormat(DateTime.Now - dStart) +
                                       "\r\n" +
                                       "Nessuna transazione trovata.");
                    }
                }
                else
                {
                    this.UseWaitCursorForm = false;
                    MessageBox.Show("dati non corretti");
                }
            }
            catch (Exception ex)
            {
                oErr = ex;
                lRet = false;
            }
            finally
            {
                this.UseWaitCursorForm = false;
            }

            if (!lRet)
            {
                MessageBox.Show("Errore" + (oErr != null ? "\r\n" + oErr.ToString() : ""));
            }
        }



        private void txtRegexExpr_TextChanged(object sender, EventArgs e)
        {
            this.TestRegex();
        }

        private void txtRegexForm_TextChanged(object sender, EventArgs e)
        {
            this.TestRegex();
        }

        private void TestRegex()
        {
            try
            {
                string cValue = this.txtRegexExpr.Text;
                System.Text.RegularExpressions.Regex oReg = new System.Text.RegularExpressions.Regex(this.txtRegexForm.Text);
                this.lblRegexRES.Text = (oReg.IsMatch(cValue) ? "OK" : "NO");
            }
            catch (Exception)
            {
            }
        }

        private void btnCheckServerFiscale_Click(object sender, EventArgs e)
        {
            Exception Error = null;
            clsRiepiloghi.CheckServerFiscale(oCon, out Error);
            //Riepiloghi.clsRiepiloghi.RicezioneMail(oCon, out Error);
            //clsRiepiloghi.CheckAnnulloTransazione(this.txtCarta.Text, "", long.Parse(this.txtProggressivo.Text), long.Parse(this.txtProggressivo.Text), true, oCon, out Error);

            //////Riepiloghi.clsP_REQ_ANNULLO_TRANSAZIONE oP_REQ_ANNULLO_TRANSAZIONE = new clsP_REQ_ANNULLO_TRANSAZIONE(Riepiloghi.myDbManager.Current());

            //////oP_REQ_ANNULLO_TRANSAZIONE.cCodiceCarta.Value = this.txtCarta.Text;
            //////oP_REQ_ANNULLO_TRANSAZIONE.nProgressivo.Value = long.Parse(this.txtProggressivo.Text);

            //////string errorMessage = "";
            //////oP_REQ_ANNULLO_TRANSAZIONE.ExecuteProcedureCommand((System.Data.Common.DbConnection)this.oCon.Conn(), null, ref errorMessage);

            //////this.dgvDati.DataSource = (System.Data.DataTable)oP_REQ_ANNULLO_TRANSAZIONE.logCUR.Value;
            //////this.dgvDati.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            //////if (oP_REQ_ANNULLO_TRANSAZIONE.errCUR.Value != null)
            //////{
            //////    DataTable tabErrCOR = (DataTable)oP_REQ_ANNULLO_TRANSAZIONE.errCUR.Value;
            //////    if (tabErrCOR.Rows.Count > 0)
            //////    {
            //////        foreach (DataRow oRow in tabErrCOR.Rows)
            //////        {
            //////            bool lError = (oRow["LIVELLO"].ToString() == "E");
            //////            MessageBox.Show(oRow["DESCRIZIONE"].ToString(), (lError ? "Errore" : "Attenzione"), MessageBoxButtons.OK, (lError ? MessageBoxIcon.Error : MessageBoxIcon.Warning));
            //////        }
            //////    }
            //////}


            //////if (oP_REQ_ANNULLO_TRANSAZIONE.cauCUR.Value != null)
            //////{
            //////    DataTable tabCauCUR = (DataTable)oP_REQ_ANNULLO_TRANSAZIONE.cauCUR.Value;
            //////    foreach (DataRow oRow in tabCauCUR.Rows)
            //////    {
            //////        MessageBox.Show(String.Format("Possibile causale di annullo {0} {1}", oRow["CODICE"].ToString(), oRow["DESCRIZIONE"].ToString()));
            //////    }
            //////}

            //Riepiloghi.P_TEST_CURSORI testCursori = new Riepiloghi.P_TEST_CURSORI(Riepiloghi.myDbManager.Current());
            //testCursori.CCODICECARTA.Value = this.txtCarta.Text;
            //testCursori.NPROGRESSIVO.Value = long.Parse(this.txtProggressivo.Text);

            //testCursori.ExecuteProcedureCommand((System.Data.Common.DbConnection)this.oCon.Conn(), null);

            //this.dgvDati.DataSource = (System.Data.DataTable)testCursori.LOGCUR.Value;

            //var result = testCursori.CastToObject<Object>();
            string f = "";



        }
    }
}
