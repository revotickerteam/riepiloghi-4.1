﻿namespace TestXmlRiepiloghi
{
    partial class frmCampiTransazioni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCampi = new System.Windows.Forms.Panel();
            this.chkCampi = new System.Windows.Forms.CheckedListBox();
            this.pnlValori = new System.Windows.Forms.Panel();
            this.lstValue = new System.Windows.Forms.CheckedListBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.dtValue = new System.Windows.Forms.DateTimePicker();
            this.numValue = new System.Windows.Forms.NumericUpDown();
            this.pnlCampi.SuspendLayout();
            this.pnlValori.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCampi
            // 
            this.pnlCampi.Controls.Add(this.chkCampi);
            this.pnlCampi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCampi.Location = new System.Drawing.Point(0, 0);
            this.pnlCampi.Name = "pnlCampi";
            this.pnlCampi.Size = new System.Drawing.Size(269, 369);
            this.pnlCampi.TabIndex = 0;
            // 
            // chkCampi
            // 
            this.chkCampi.CheckOnClick = true;
            this.chkCampi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCampi.FormattingEnabled = true;
            this.chkCampi.Location = new System.Drawing.Point(0, 0);
            this.chkCampi.Name = "chkCampi";
            this.chkCampi.ScrollAlwaysVisible = true;
            this.chkCampi.Size = new System.Drawing.Size(269, 369);
            this.chkCampi.TabIndex = 0;
            // 
            // pnlValori
            // 
            this.pnlValori.Controls.Add(this.lstValue);
            this.pnlValori.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlValori.Location = new System.Drawing.Point(280, 0);
            this.pnlValori.Name = "pnlValori";
            this.pnlValori.Size = new System.Drawing.Size(234, 369);
            this.pnlValori.TabIndex = 2;
            // 
            // lstValue
            // 
            this.lstValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstValue.FormattingEnabled = true;
            this.lstValue.Location = new System.Drawing.Point(0, 0);
            this.lstValue.Name = "lstValue";
            this.lstValue.ScrollAlwaysVisible = true;
            this.lstValue.Size = new System.Drawing.Size(234, 369);
            this.lstValue.TabIndex = 1;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.label1);
            this.pnlButtons.Controls.Add(this.btnOK);
            this.pnlButtons.Controls.Add(this.btnAbort);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 369);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(4);
            this.pnlButtons.Size = new System.Drawing.Size(514, 58);
            this.pnlButtons.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(95, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 50);
            this.label1.TabIndex = 2;
            this.label1.Text = "...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOK
            // 
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOK.Location = new System.Drawing.Point(419, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(91, 50);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Conferma";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnAbort
            // 
            this.btnAbort.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAbort.Location = new System.Drawing.Point(4, 4);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(91, 50);
            this.btnAbort.TabIndex = 0;
            this.btnAbort.Text = "Abbandona";
            this.btnAbort.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(269, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(11, 369);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // txtValue
            // 
            this.txtValue.BackColor = System.Drawing.Color.Gainsboro;
            this.txtValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtValue.Enabled = false;
            this.txtValue.Location = new System.Drawing.Point(288, 50);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(100, 23);
            this.txtValue.TabIndex = 5;
            this.txtValue.Visible = false;
            this.txtValue.WordWrap = false;
            // 
            // dtValue
            // 
            this.dtValue.Enabled = false;
            this.dtValue.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtValue.Location = new System.Drawing.Point(288, 90);
            this.dtValue.Name = "dtValue";
            this.dtValue.Size = new System.Drawing.Size(110, 23);
            this.dtValue.TabIndex = 6;
            this.dtValue.Visible = false;
            // 
            // numValue
            // 
            this.numValue.BackColor = System.Drawing.Color.Gainsboro;
            this.numValue.Enabled = false;
            this.numValue.Location = new System.Drawing.Point(290, 130);
            this.numValue.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.numValue.Name = "numValue";
            this.numValue.Size = new System.Drawing.Size(120, 23);
            this.numValue.TabIndex = 7;
            this.numValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numValue.Visible = false;
            // 
            // frmCampiTransazioni
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(514, 427);
            this.ControlBox = false;
            this.Controls.Add(this.numValue);
            this.Controls.Add(this.dtValue);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.pnlCampi);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlValori);
            this.Controls.Add(this.pnlButtons);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmCampiTransazioni";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Campi log transazioni";
            this.pnlCampi.ResumeLayout(false);
            this.pnlValori.ResumeLayout(false);
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlCampi;
        private System.Windows.Forms.CheckedListBox chkCampi;
        private System.Windows.Forms.Panel pnlValori;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.CheckedListBox lstValue;
        private System.Windows.Forms.DateTimePicker dtValue;
        private System.Windows.Forms.NumericUpDown numValue;
    }
}