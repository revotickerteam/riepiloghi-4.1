﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestXmlRiepiloghi
{
    public partial class frmMenuOptions : Form
    {
        public List<clsItemMenu> Items { get; set; }
        public class clsItemMenu
        {
            public object Value { get; set; }
            public string Descr { get; set; }

            public clsItemMenu()
            {
                this.Value = null;
                this.Descr = "";
            }

            public clsItemMenu(object value, string descr)
                :this()
            {
                this.Value = value;
                this.Descr = descr;
            }
        }

        public object SelectedValue { get; set; }

        public frmMenuOptions()
        {
            InitializeComponent();
            this.Items = new List<clsItemMenu>();
        }

        public frmMenuOptions(List<clsItemMenu> items)
            :this()
        {
            this.Items = items;
            this.InitItemsMenu();
        }

        public frmMenuOptions(string title)
            : this()
        {
            this.Title = title;
        }

        public frmMenuOptions(string title, List<clsItemMenu> items)
            : this()
        {
            this.Title = title;
            this.Items = items;
            this.InitItemsMenu();
        }

        private void InitItemsMenu()
        {
            if (this.pnlItems.Controls.Count > 0)
            {
                foreach (Control control in this.pnlItems.Controls)
                {
                    Button btn = (Button)control;
                    btn.Click -= Btn_Click;
                }
            }
            this.pnlItems.Controls.Clear();

            if (this.Items != null && this.Items.Count > 0)
            {
                Button btn = null;

                foreach (clsItemMenu item in this.Items)
                {
                    btn = GetButtonMenu(item);
                    btn.Click += Btn_Click;
                    this.pnlItems.Controls.Add(btn);
                    btn.BringToFront();
                }

                btn = GetButtonMenu(new clsItemMenu(null, "Abbandona"));
                btn.Click += Btn_Click;
                this.pnlItems.Controls.Add(btn);
                btn.BringToFront();
            }
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            this.SelectedValue = ((Button)sender).Tag;
            if (this.SelectedValue != null)
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.Abort;
        }

        public static Button GetButtonMenu(clsItemMenu item)
        {
            Button btn = new Button();
            btn.Text = item.Descr;
            btn.Tag = item.Value;
            btn.Dock = DockStyle.Top;
            btn.MinimumSize = new Size(100, 50);
            return btn;
        }

        public string Title
        {
            get { return this.labelTitle.Text; }
            set { this.labelTitle.Text = value; }
        }
    }
}
