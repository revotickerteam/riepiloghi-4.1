﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestXmlRiepiloghi
{
    public partial class frmPropertyObject : Form
    {
        public frmPropertyObject()
        {
            InitializeComponent();
            this.btnAbort.Click += BtnAbort_Click;
        }

        private void BtnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        public frmPropertyObject(string title)
            :this()
        {
            this.Title = title;
        }

        public frmPropertyObject(object value)
            : this()
        {
            this.Value = value;
        }

        public frmPropertyObject(string title, object value)
            : this()
        {
            this.Title = title;
            this.Value = value;
        }

        public object Value
        {
            get { return this.propertyGridObject.SelectedObject; }
            set { this.propertyGridObject.SelectedObject = value; }
        }

        public string Title
        {
            get { return this.labelTitle.Text; }
            set { this.labelTitle.Text = value; }
        }
    }
}
