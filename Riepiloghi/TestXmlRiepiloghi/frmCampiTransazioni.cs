﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Wintic.Data;
using Riepiloghi;

namespace TestXmlRiepiloghi
{
    public partial class frmCampiTransazioni : Form
    {
        private IConnection oCon;
        private int ItemValueSelected = -1;

        public frmCampiTransazioni(IConnection oConnection)
        {
            InitializeComponent();
            this.oCon = oConnection;
            this.Shown += new EventHandler(frmCampiTransazioni_Shown);
        }

        public System.Collections.SortedList GetListCampi
        {
            get
            {
                System.Collections.SortedList oRet = new System.Collections.SortedList();
                foreach (int Index in this.chkCampi.CheckedIndices)
                {
                    clsCampoTransazione oCampo = (clsCampoTransazione)this.chkCampi.Items[Index];
                    oRet.Add(oCampo.Campo, oCampo);
                }
                return oRet;
            }
        }

        public System.Collections.SortedList GetListValues(bool Convert)
        {
            System.Collections.SortedList oRet = new System.Collections.SortedList();
            foreach (int Index in this.chkCampi.CheckedIndices)
            {
                clsCampoTransazione oCampo = (clsCampoTransazione)this.chkCampi.Items[Index];

                if (oCampo.Tipo == "S")
                {
                    oRet.Add(oCampo.Campo, this.lstValue.Items[Index].ToString().PadRight(oCampo.Dimensione));
                }
                else if (oCampo.Tipo == "N")
                {
                    Int64 nValue = 0;
                    if (Int64.TryParse(this.lstValue.Items[Index].ToString(), out nValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, nValue.ToString("0".PadRight(oCampo.Dimensione, '0')));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, nValue);
                        }
                    }
                }
                else if (oCampo.Tipo == "C")
                {
                    decimal nValue = 0;
                    if (decimal.TryParse(this.lstValue.Items[Index].ToString(), out nValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, (System.Math.Round(nValue, 2) * 100).ToString("0".PadRight(oCampo.Dimensione, '0')));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, System.Math.Round(nValue, 2));
                        }
                    }
                }
                else if (oCampo.Tipo == "D")
                {
                    DateTime dValue;
                    if (DateTime.TryParse(this.lstValue.Items[Index].ToString(), out dValue))
                    {
                        if (Convert)
                        {
                            oRet.Add(oCampo.Campo, dValue.Year.ToString("0000") + dValue.Month.ToString("00") + dValue.Day.ToString("00"));
                        }
                        else
                        {
                            oRet.Add(oCampo.Campo, dValue.Date);
                        }
                    }
                }
            }
            return oRet;
        }

        private void frmCampiTransazioni_Shown(object sender, EventArgs e)
        {
            this.InitForm();
        }

        private void InitForm()
        {
            try
            {
                Exception oError;
                System.Collections.SortedList oListaCampi = clsCampoTransazione.GetListaCampi(oCon, out oError, false);
                foreach (System.Collections.DictionaryEntry ItemCampoDict in oListaCampi)
                {
                    clsCampoTransazione oCampo = (clsCampoTransazione)ItemCampoDict.Value;
                    this.chkCampi.Items.Add(oCampo);
                    this.lstValue.Items.Add(" ".PadRight(oCampo.Dimensione));
                }
            }
            catch
            {
            }
            this.SizeChanged += new EventHandler(frmCampiTransazioni_SizeChanged);
            this.splitter1.LocationChanged += new EventHandler(splitter1_LocationChanged);
            this.btnOK.Click += new EventHandler(btnOK_Click);
            this.btnAbort.Click += new EventHandler(btnAbort_Click);
            this.chkCampi.ItemCheck += new ItemCheckEventHandler(chkCampi_ItemCheck);
            this.chkCampi.SelectedIndexChanged += new EventHandler(chkCampi_SelectedIndexChanged);
            this.txtValue.TextChanged += new EventHandler(txtValue_TextChanged);
            this.dtValue.ValueChanged += new EventHandler(dtValue_ValueChanged);
            this.numValue.ValueChanged += new EventHandler(numValue_ValueChanged);
        }

        private void splitter1_LocationChanged(object sender, EventArgs e)
        {
            this.CloseEdit();
        }

        private void frmCampiTransazioni_SizeChanged(object sender, EventArgs e)
        {
            this.CloseEdit();
            this.pnlValori.Width = this.ClientRectangle.Width / 2;
        }

        private void numValue_ValueChanged(object sender, EventArgs e)
        {
            if (this.numValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValue.Items[ItemValueSelected] = this.numValue.Value.ToString();
            }
        }

        private void dtValue_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValue.Items[ItemValueSelected] = this.dtValue.Value.ToShortDateString();
            }
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            if (this.txtValue.Enabled && this.ValuesVisible && ItemValueSelected >= 0)
            {
                this.lstValue.Items[ItemValueSelected] = this.txtValue.Text;
            }
        }

        private void chkCampi_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (this.ValuesVisible)
            //{
            //    this.CloseEdit();

            //    if (this.chkCampi.SelectedIndex >= 0)
            //    {
            //        ItemValueSelected = this.chkCampi.SelectedIndex;
            //        this.OpenEdit(this.chkCampi.SelectedIndex);
            //    }
            //}
        }

        private void chkCampi_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (this.ValuesVisible)
            {
                this.CloseEdit();
                if (e.NewValue == CheckState.Checked)
                {
                    ItemValueSelected = e.Index;
                    this.OpenEdit(e.Index);
                }
                else
                {
                    this.lstValue.Items[e.Index] = "";
                }
            }
        }

        private void CloseEdit()
        {
            this.txtValue.Enabled = false;
            this.dtValue.Enabled = false;
            this.numValue.Enabled = false;
            this.txtValue.Visible = false;
            this.dtValue.Visible = false;
            this.numValue.Visible = false;
        }

        private void OpenEdit(int Index)
        {
            Rectangle oRect = this.chkCampi.GetItemRectangle(Index);
            clsCampoTransazione oCampo = (clsCampoTransazione)this.chkCampi.Items[Index];

            if (oCampo.Tipo == "S")
            {
                this.txtValue.Enabled = false;
                this.txtValue.Location = new Point(this.pnlValori.Location.X, oRect.Y);
                this.txtValue.Width = this.lstValue.Width - SystemInformation.VerticalScrollBarWidth - 1;
                this.txtValue.MaxLength = oCampo.Dimensione;
                this.txtValue.Text = this.lstValue.Items[Index].ToString();
                this.txtValue.Visible = true;
                this.txtValue.BringToFront();
                this.txtValue.Enabled = true;
                this.txtValue.SelectAll();
                this.txtValue.Focus();
            }
            else if (oCampo.Tipo == "N")
            {
                this.numValue.Enabled = false;
                this.numValue.Location = new Point(this.pnlValori.Location.X, oRect.Y);
                this.numValue.Width = this.lstValue.Width - SystemInformation.VerticalScrollBarWidth - 1;
                this.numValue.DecimalPlaces = 0;
                Int64 nValue = 0;
                if (Int64.TryParse(this.lstValue.Items[Index].ToString(), out nValue))
                {
                    this.numValue.Value = nValue;
                }
                else
                {
                    this.numValue.Value = 0;
                }
                this.numValue.Visible = true;
                this.numValue.BringToFront();
                this.numValue.Enabled = true;
                this.numValue.Focus();
            }
            else if (oCampo.Tipo == "C")
            {
                this.numValue.Enabled = false;
                this.numValue.Location = new Point(this.pnlValori.Location.X, oRect.Y);
                this.numValue.Width = this.lstValue.Width - SystemInformation.VerticalScrollBarWidth - 1;
                this.numValue.DecimalPlaces = 2;
                decimal nValue = 0;
                if (decimal.TryParse(this.lstValue.Items[Index].ToString(), out nValue))
                {
                    this.numValue.Value = nValue;
                }
                else
                {
                    this.numValue.Value = 0;
                }
                this.numValue.Visible = true;
                this.numValue.BringToFront();
                this.numValue.Enabled = true;
                this.numValue.Focus();
            }
            else if (oCampo.Tipo == "D")
            {
                this.dtValue.Enabled = false;
                this.dtValue.Location = new Point(this.pnlValori.Location.X, oRect.Y);
                this.dtValue.Width = this.lstValue.Width - SystemInformation.VerticalScrollBarWidth - 1;
                DateTime dValue;
                if (DateTime.TryParse(this.lstValue.Items[Index].ToString(), out dValue))
                {
                    this.dtValue.Value = dValue;
                }
                else
                {
                    this.dtValue.Value = DateTime.Now;
                }
                this.dtValue.Visible = true;
                this.dtValue.BringToFront();
                this.dtValue.Enabled = true;
                this.dtValue.Focus();
            }

        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public string Help
        {
            set
            {
                this.label1.Text = value;
            }
        }

        public bool ValuesVisible
        {
            get
            {
                return this.pnlValori.Visible;
            }
            set
            {
                this.splitter1.Visible = value;
                this.pnlValori.Visible = value;
            }
        }

    }
}
