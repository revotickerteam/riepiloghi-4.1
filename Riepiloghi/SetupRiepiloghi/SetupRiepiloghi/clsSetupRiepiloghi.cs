﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Wintic.Data;
using Wintic.Data.oracle;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace OptimizeSystem
{
    public class clsOptimizeSystem
    {

        public Exception Error { get; set; }
        public string Operation { get; set; }

        public int counterSetup { get; set; }
        public int maximumSetup { get; set; }

        public int counterOperation { get; set; }
        public int maximumOperation { get; set; }
        public bool remoteConnection { get; set; }
        public string remoteConnectionString { get; set; }

        private IConnection connection { get; set; }
        private string currentKey { get; set; }
        
        private string tableVersion { get; set; }
        private string fieldVersion { get; set; }
        private List<string> scriptTableVersion { get; set; }
        private string tempTableVersion { get; set; }
        private string connectionString { get; set; }

        private List<string> objects_executed = new List<string>();

        private Dictionary<string, string> dataXmlFiles = null;

        #region eventi

        public delegate void delegateCounterSetup(string message);
        public delegate void delegateStartOperation();
        public delegate void delegateExceptionOperation();
        public delegate void delegateDetailOperation(string message);
        public delegate void delegateCounterOperation();
        public delegate void delegateEndOperation();


        public event delegateCounterSetup CounterSetup;

        public event delegateStartOperation StartOperation;
        public event delegateExceptionOperation ExceptionOperation;
        public event delegateDetailOperation DetailOperation;
        public event delegateCounterOperation CounterOperation;
        public event delegateEndOperation EndOperation;



        #endregion

        #region primitive della classe

        public bool ErrorExist
        {
            get { return Error != null; }
        }

        private void InvokeCounterSetup(string message)
        {
            counterSetup += 1;
            if (CounterSetup != null) CounterSetup(message);
        }

        private void InvokeStartOperation(string value)
        {
            InvokeStartOperation(value, 0);
        }

        private void InvokeStartOperation(string value, int maximum)
        {
            Operation = value;
            counterOperation = 0;
            maximumOperation = maximum;
            if (StartOperation != null) StartOperation();
        }

        private void InvokeExceptionOperation()
        {
            if (ExceptionOperation != null) ExceptionOperation();
        }

        private void InvokeCounterOperation()
        {
            counterOperation += 1;
            if (CounterOperation != null) CounterOperation();
        }

        private void InvokeDetailOperation(string message)
        {
            if (DetailOperation != null) DetailOperation(message);
        }

        private void InvokeEndOperation()
        {
            if (EndOperation != null) EndOperation();
        }

        #endregion

        #region connessione


        public bool GetConnection()
        {
            InvokeStartOperation("Connessione");
            connection = new CConnectionOracle();
            try
            {
                string remoteString = "";
                if (remoteConnection)
                {
                    foreach (string item in connectionString.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (item.Split('=')[0] == "data source")
                                remoteString += string.Format("data source={0}", remoteConnectionString) + ";";
                            else
                                remoteString += item + ";";
                        }
                    }
                    connection.ConnectionString = remoteString;
                }
                else
                    connection.ConnectionString = connectionString;
                connection.Open();
                InvokeEndOperation();
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        #endregion

        #region Verifica oggetto database

        private bool CheckExistOracleObject(string objectName, string objectType)
        {
            bool result = false;

            // SEQUENCE, TYPE, TABLE, TRIGGER, INDEX, PROCEDURE, FUNCTION, VIEW
            try
            {
                clsParameters oPars = new clsParameters();
                oPars.Add(":pOBJECT_NAME", objectName);
                oPars.Add(":pOBJECT_TYPE", objectType);
                IRecordSet oRS = (IRecordSet)connection.ExecuteQuery("SELECT * FROM USER_OBJECTS where OBJECT_NAME = :pOBJECT_NAME AND OBJECT_TYPE = :pOBJECT_TYPE", oPars);
                result = (!oRS.EOF);
                oRS.Close();
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return result;
        }


        private bool CheckInvalidObjects()
        {
            bool result = false;
            InvokeStartOperation("Verifica validità oggetti");
            try
            {
                string query = "select OBJECT_TYPE, OBJECT_NAME from USER_OBJECTS where STATUS = 'INVALID' and OBJECT_TYPE in ('PACKAGE','FUNCTION','PROCEDURE','TYPE','TRIGGER','VIEW')";
                IRecordSet oRS = null;
                bool lMore = false;
                decimal count = 0;

                oRS = (IRecordSet)connection.ExecuteQuery(query);
                lMore = (!oRS.EOF);
                

                while (lMore)
                {

                    lMore = false;
                    count = oRS.RecordCount;
                    InvokeStartOperation("Verifica validità oggetti", (int)count);

                    while (!oRS.EOF)
                    {
                        InvokeCounterOperation();
                        string objectName = oRS.Fields("OBJECT_NAME").Value.ToString();
                        string objectType = oRS.Fields("OBJECT_TYPE").Value.ToString();
                        string compile = string.Format("ALTER {0} {1} COMPILE", objectType, objectName);
                        oRS.MoveNext();
                    }
                    oRS.Close();

                    oRS = (IRecordSet)connection.ExecuteQuery(query);
                    if (!oRS.EOF)
                    {
                        lMore = count != oRS.RecordCount;
                        count = oRS.RecordCount;
                    }
                    else
                        oRS.Close();
                }

                InvokeEndOperation();
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return result;
        }


        #endregion

        #region gestione versione

        private bool SetupTablesVersion()
        {
            try
            {
                if (!CheckExistOracleObject(tableVersion, "TABLE"))
                {
                    //connection.ExecuteNonQuery(string.Format("CREATE TABLE {0} (VR_NUMBER NUMBER, LAST_UPDATE DATE DEFAULT SYSDATE, LAST_FILE VARCHAR2(500))", tableVersion));
                    foreach (string command in scriptTableVersion)
                    {
                        connection.ExecuteNonQuery(command);
                    }
                }
                if (!CheckExistOracleObject(tempTableVersion, "TABLE"))
                {
                    connection.ExecuteNonQuery(string.Format("CREATE TABLE {0} ({1} NUMBER, UPDATE DATE DEFAULT SYSDATE, FILE VARCHAR2(500), OPERATION VARCHAR2(30), TYPE VARCHAR2(30), NAME VARCHAR2(30), INDEX NUMBER DEFAULT 0)", tempTableVersion, fieldVersion));
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        private long GetVersion()
        {
            long result = 0;
            try
            {
                if (CheckExistOracleObject(tableVersion, "TABLE"))
                {
                    IRecordSet oRS = (IRecordSet)connection.ExecuteQuery(string.Format("SELECT {0} FROM {1}", fieldVersion, tableVersion));
                    result = (!oRS.EOF && !oRS.Fields(fieldVersion).IsNull ? long.Parse(oRS.Fields(fieldVersion).Value.ToString()) : 0);
                    oRS.Close();
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        private bool UpdateTempVersion(long version, string file)
        {
            try
            {
                clsParameters oPars = new clsParameters(":pVERSION", version);
                connection.ExecuteNonQuery(string.Format("DELETE FROM {0} WHERE {1} < :pVERSION", tempTableVersion, fieldVersion), oPars);

                oPars = new clsParameters();
                oPars.Add(":pVERSION", version);
                oPars.Add(":pFILE", file);

                if (connection.ExecuteNonQuery(string.Format("UPDATE {0} SET UPDATE = SYSDATE, FILE = :pFILE WHERE {1} = :pVERSION", tempTableVersion, fieldVersion), oPars) == 0)
                {
                    connection.ExecuteNonQuery(string.Format("INSERT INTO {0} ({1}, UPDATE, FILE, INDEX) VALUE (:pVERSION, SYSDATE, :pFILE, 0)", tempTableVersion, fieldVersion), oPars);
                }
                else
                {
                    objects_executed = new List<string>();

                    oPars = new clsParameters();
                    oPars.Add(":pVERSION", version);
                    IRecordSet oRS = (IRecordSet)connection.ExecuteQuery(string.Format("SELECT OPERATION, TYPE, NAME, INDEX FROM {0} WHERE {1} = :pVERSION AND INDEX > 0 ORDER BY INDEX", tempTableVersion, fieldVersion), oPars);

                    while (!oRS.EOF)
                    {
                        long index = long.Parse(oRS.Fields("INDEX").Value.ToString());
                        string row = string.Format("{0}_{1}_{2}_{3}",
                                                   oRS.Fields("OPERATION").Value.ToString(),
                                                   oRS.Fields("TYPE").Value.ToString(),
                                                   oRS.Fields("NAME").Value.ToString(),
                                                   oRS.Fields("INDEX").Value.ToString());

                        objects_executed.Add(row);

                        oRS.MoveNext();
                    }
                    oRS.Close();
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        private bool UpdateTempStepVersion(long version, string operation, string type, string name, long index)
        {
            try
            {

                clsParameters oPars = new clsParameters();
                oPars.Add(":pVERSION", version);
                oPars.Add(":pOPERATION", operation);
                oPars.Add(":pTYPE", type);
                oPars.Add(":pNAME", name);
                oPars.Add(":pINDEX", index);

                if (connection.ExecuteNonQuery(string.Format("UPDATE {0} SET UPDATE = SYSDATE WHERE {1} = :pVERSION AND OPERATION = :pOPERATION AND TYPE = :pTYPE AND NAME :pNAME AND INDEX = :pINDEX", tempTableVersion, fieldVersion), oPars) == 0)
                    connection.ExecuteNonQuery(string.Format("INSERT INTO {0} ({1}, UPDATE, OPERATION, TYPE, NAME, INDEX) VALUE (:pVERSION, SYSDATE, :pOPERATION, :pTYPE, :pNAME, :pINDEX)", tempTableVersion, fieldVersion), oPars);
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        private bool UpdateVersion(long version, string file)
        {
            InvokeStartOperation("conferma operazioni");
            try
            {
                clsParameters oPars = new clsParameters(":pVR_NUMBER", version);
                connection.ExecuteNonQuery("DELETE FROM RP_TMP_NUMBER WHERE VR_NUMBER < :pVR_NUMBER", oPars);

                oPars = new clsParameters();
                oPars.Add(":pVR_NUMBER", version);
                oPars.Add(":pLAST_FILE", file);

                if (connection.ExecuteNonQuery("UPDATE RP_VR_NUMBER SET VR_NUMBER = :pVR_NUMBER, LAST_UPDATE = SYSDATE, LAST_FILE = :pLAST_FILE", oPars) == 0)
                {
                    connection.ExecuteNonQuery("INSERT INTO RP_VR_NUMBER (VR_NUMBER, LAST_UPDATE, LAST_FILE) VALUE (:pVR_NUMBER, SYSDATE, :pLAST_FILE)", oPars);
                }
                InvokeEndOperation();
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        #endregion

        #region Elaborazione

        // <object type="object_type" name="object_name">...sql...</object_type>
        // <object type="object_type" name="object_name" file="file_name"></object_type>
        // 
        // <drop type="object_type" name="object_name">...sql...</object_type>
        // <drop type="object_type" name="object_name" file="file_name"></object_type>
        // 
        // <alter type="object_type" name="object_name">...sql...</object_type>
        // <alter type="object_type" name="object_name" file="file_name"></object_type>
        // 
        // <table name="table_name">...sql...</table>
        // <table name="table_name" file="file_name"></table>
        // 
        // <index name="index_name">...sql...</index>
        // <index name="index_name" file="file_name"></index>
        // 
        // <trigger name="trigger_name">...sql...</trigger>
        // <trigger name="trigger_name" file="file_name"></trigger>
        // 
        // <view name="view_name">...sql...</view>
        // <view name="view_name" file="file_name"></view>
        // 
        // <sequence name="sequence_name">...sql...</sequence>
        // <sequence name="sequence_name" file="file_name"></sequence>
        // 
        // <type name="type_name">...sql...</type>
        // <type name="type_name" file="file_name"></type>
        // 
        // <procedure name="procedure_name">...sql...</procedure>
        // <procedure name="procedure_name" file="file_name"></procedure>
        // 
        // <function name="function_name">...sql...</function>
        // <function name="function_name" file="file_name"></function>


        // esegue la creazione/modifica dell'oggetto, l'errore viene portato  fuori perchè potrebbe non essere imporatnte per il tipo di oggetto
        private bool ExecuteScriptObject(string script, out Exception error)
        {
            bool result = false;
            error = null;
            try
            {
                connection.ExecuteNonQuery(script);
                result = true;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            return result;
        }

        // lettura del documento xml che contiene gli oggetti da creare
        private XmlDocument GetXmlDocumentFromFileContent(string file, string xmlContent)
        {
            XmlDocument result = null;
            try
            {
                result = new XmlDocument();
                result.LoadXml(xmlContent);
            }
            catch (Exception ex)
            {
                result = null;
                Error = ex;
                InvokeExceptionOperation();
            }
            return result;
        }

        public bool GetXmlDocAndVersionFile(string file, string xmlContent, out string key, out long version, out XmlDocument document)
        {
            key = "";
            version = 0;
            document = null;
            try
            {

                document = GetXmlDocumentFromFileContent(file, xmlContent);
                XmlNode node = document.GetElementsByTagName("script_list")[0];
                if (node != null)
                {
                    if (string.IsNullOrEmpty(node.Attributes["key"].Value) || string.IsNullOrWhiteSpace(node.Attributes["key"].Value))
                    {
                        Error = new Exception(string.Format("chiave file non valida {0}", file));
                        InvokeExceptionOperation();
                    }
                    else
                    {
                        key = node.Attributes["key"].Value;

                        if (!long.TryParse(node.Attributes["version"].Value, out version))
                        {
                            Error = new Exception(string.Format("Numero versione file non valido {0}", file));
                            InvokeExceptionOperation();
                        }
                    }
                }
                else
                {
                    Error = new Exception(string.Format("File non valido {0}", file));
                    InvokeExceptionOperation();
                }

            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        public bool GetVersionXmlFile(string file, string xmlContent, out string key, out long version)
        {
            key = "";
            version = 0;
            try
            {

                XmlDocument document = GetXmlDocumentFromFileContent(file, xmlContent);
                XmlNode node = document.GetElementsByTagName("script_list")[0];
                if (node != null)
                {
                    if (string.IsNullOrEmpty(node.Attributes["key"].Value) || string.IsNullOrWhiteSpace(node.Attributes["key"].Value))
                    {
                        Error = new Exception(string.Format("chiave file non valida {0}", file));
                        InvokeExceptionOperation();
                    }
                    else
                    {
                        key = node.Attributes["key"].Value;

                        if (!long.TryParse(node.Attributes["version"].Value, out version))
                        {
                            Error = new Exception(string.Format("Numero versione file non valido {0}", file));
                            InvokeExceptionOperation();
                        }
                    }
                }
                else
                {
                    Error = new Exception(string.Format("File non valido {0}", file));
                    InvokeExceptionOperation();
                }

            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            return !ErrorExist;
        }

        public Dictionary<string, string> GetTypesUtility()
        {
            Dictionary<string, string> descTypes = new Dictionary<string, string>()
                        {
                            {"generic", "script generico" },
                            {"table", "tabella" },
                            {"view", "vista" },
                            {"index", "indice" },
                            {"trigger", "trigger" },
                            {"type", "tipo" },
                            {"function", "funzione" },
                            {"procedure", "procedura" },
                            {"sequence", "sequenza" },
                            {"alter", "alter table" }
                        };
            return descTypes;
        }

        // Gestione doucmento xml script
        private bool ExecuteXmlScriptDocument(string file, string xmlContent)
        {
            InvokeStartOperation(string.Format("Elaborazione {0}", file), 0);
            XmlDocument document = null;
            string xml_key_file = "";
            long xml_version_number = 0;

            try
            {
                document = GetXmlDocumentFromFileContent(file, xmlContent);
                InvokeStartOperation("Elaborazione", document.ChildNodes.Count);
                GetVersionXmlFile(file, xmlContent, out xml_key_file, out xml_version_number);
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
            }
            
            if (!ErrorExist)
            {
                try
                {
                    if (xml_version_number == 0)
                    {
                        Error = new Exception("Numero versione file non specificato");
                        InvokeExceptionOperation();
                    }
                    else if (!ErrorExist)
                        UpdateTempVersion(xml_version_number, file);

                    // aggiornamento della versione temporanea
                    long object_index = 0;

                    // elaborazione delle modifiche contenute nei nodi xml
                    foreach (XmlNode node in document.ChildNodes)
                    {
                        InvokeCounterOperation();

                        object_index += 1;

                        string node_name = node.Name;
                        string object_type = "";
                        string object_name = "";
                        string file_name = "";
                        string inner_script = "";


                        //if (node_name != "generic" && node_name != "drop" && node_name != "alter")
                        //{
                        //    object_type = node_name;
                        //}
                        object_type = node_name;

                        foreach (XmlAttribute attribute in node.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "type":
                                    {
                                        object_type = attribute.Value;
                                        break;
                                    }
                                case "name":
                                    {
                                        object_name = attribute.Value;
                                        break;
                                    }
                                case "file":
                                    {
                                        file_name = attribute.Value;
                                        break;
                                    }
                            }
                        }

                        Dictionary<string, string> descTypes = GetTypesUtility();


                        if (!descTypes.ContainsKey(object_type))
                        {
                            Error = new Exception(string.Format("tipo sconosciuto per l'oggetto {0}: {1}", object_name, object_type));
                            InvokeExceptionOperation();
                            break;
                        }
                        else
                        {
                            string descTypeOperation = "verifica " + (node_name == "drop" ? "eliminazione" : (CheckExistOracleObject(object_name, object_type) ? "modifica" : "creazione"));
                            string descTypeObject = descTypes[object_type];
                            InvokeDetailOperation(string.Format("{0} {1} {2}", descTypeOperation, descTypeObject, object_name));

                            if (string.IsNullOrEmpty(file_name))
                                inner_script = node.InnerText;
                            else
                                inner_script = System.IO.File.ReadAllText(file_name);

                            string row = string.Format("{0}_{1}_{2}_{3}", node.Name, object_type, object_name, object_index.ToString());

                            if (!objects_executed.Contains(row) || object_type == "trigger" || object_type == "view" || object_type == "procedure" || object_type == "function" || object_type == "generic")
                            {

                                Exception internalError = null;
                                if (!ExecuteScriptObject(inner_script, out internalError))
                                {
                                    if ((object_type != "index" && object_type != "sequence") || (node_name == "drop" || node_name == "alter"))
                                    {
                                        Error = internalError;
                                        InvokeExceptionOperation();
                                        break;
                                    }
                                }

                                if (!ErrorExist)
                                {
                                    // aggiornamento step versione temporanea per non rifare l'oggetto in caso di esecuzione multipla del medesimo file xml
                                    UpdateTempStepVersion(xml_version_number, node.Name, object_type, object_name, object_index);
                                    InvokeDetailOperation(string.Format("{0} {1} {2} eseguito con successo", descTypeOperation, descTypeObject, object_name));
                                }
                            }
                            else
                                InvokeDetailOperation(string.Format("{0} {1} {2} eseguito precedentemente", descTypeOperation, descTypeObject, object_name));
                        }
                    }

                    if (!ErrorExist)
                    {
                        InvokeEndOperation();

                        // compilazione oggetti non validi
                        CheckInvalidObjects();
                    }

                    if (!ErrorExist)
                    {
                        // aggiornamento di versione
                        UpdateVersion(xml_version_number, file);
                    }

                }
                catch (Exception ex)
                {
                    Error = ex;
                    InvokeExceptionOperation();
                }
            }
            return !ErrorExist;
        }

        #region files di script aggiornamento

        #region zip

        #endregion

        public string GetDataFileName()
        {
            System.IO.FileInfo oFileInfoApplication = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dataFileName = oFileInfoApplication.FullName;
            dataFileName = dataFileName.Replace(".exe", ".dat");
            dataFileName = dataFileName.Replace(".EXE", ".dat");
            return dataFileName;
        }
        public Dictionary<string, string> GetXmlFiles()
        {
            return GetXmlFiles(false);
        }
        public Dictionary<string, string> GetXmlFiles(bool reset)
        {
            try
            {
                if (dataXmlFiles == null || reset)
                {
                    dataXmlFiles = new Dictionary<string, string>();
                    string dataFileName = GetDataFileName();
                    if (System.IO.File.Exists(dataFileName))
                    {
                        using (ZipInputStream oStreamInput = new ZipInputStream(System.IO.File.OpenRead(dataFileName)))
                        {
                            ZipEntry oEntry = oStreamInput.GetNextEntry();
                            while (oEntry != null)
                            {
                                string cPath = System.IO.Path.GetDirectoryName(oEntry.Name);
                                string cFile = System.IO.Path.GetFileName(oEntry.Name);
                                if (!string.IsNullOrEmpty(cFile) && !string.IsNullOrWhiteSpace(cFile) && cFile != string.Empty)
                                {
                                    string InternalFileXml = "";
                                    int nSize = 2048;
                                    byte[] aData = new byte[nSize];
                                    while (nSize > 0)
                                    {
                                        nSize = oStreamInput.Read(aData, 0, aData.Length);
                                        if (nSize > 0)
                                        {
                                            InternalFileXml += System.Text.Encoding.ASCII.GetString(aData);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(InternalFileXml) && !string.IsNullOrWhiteSpace(InternalFileXml))
                                            {
                                                dataXmlFiles.Add(cFile, InternalFileXml);
                                            }
                                        }
                                    }
                                }
                                oEntry = oStreamInput.GetNextEntry();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                InvokeExceptionOperation();
                dataXmlFiles = new Dictionary<string, string>();
            }
            return dataXmlFiles;
        }

        private KeyValuePair<string, string> GetNextFile(out int counterFilesToExecute)
        {
            counterFilesToExecute = 0;
            long version = GetVersion();

            long nextVersionFile = -1;
            KeyValuePair<string, string> result = new KeyValuePair<string, string>("", "");

            // ciclo individuazione del primo file da eseguire con versione superiore a quella del sistema
            foreach (KeyValuePair<string, string> itemXmlFile in GetXmlFiles())
            {
                //currentKey
                string xmlFile = itemXmlFile.Key;
                string xmlContent = itemXmlFile.Value;
                string xml_key_file = "";
                long xml_versione_file = 0;

                if (GetVersionXmlFile(xmlFile, xmlContent, out xml_key_file, out xml_versione_file))
                {
                    if (xml_key_file == currentKey && xml_versione_file > version)
                    {
                        counterFilesToExecute += 1;
                        if (nextVersionFile == -1 || xml_versione_file < nextVersionFile)
                        {
                            xml_versione_file = nextVersionFile;
                            result = itemXmlFile;
                        }
                    }
                }
            }
            return result;
        }

        private KeyValuePair<string, string> GetNextFile()
        {
            int counter = 0;
            return GetNextFile(out counter);
        }

        #endregion

        public bool Aggiornamento()
        {
            bool result = false;
            long version = 0;

            counterSetup = 0;
            maximumSetup = 5;

            InvokeCounterSetup("Analisi");

            // 1 connessione
            if (GetConnection())
            {
                // 2 get versione del sistema
                InvokeCounterSetup("Analisi avviata");
                version = GetVersion();

                InvokeCounterSetup("");
                int counterFilesToExecute = 0;
                KeyValuePair<string, string> nextFile = GetNextFile(out counterFilesToExecute);

                if (!string.IsNullOrEmpty(nextFile.Key))
                {
                    InvokeCounterSetup("");
                    maximumSetup += counterFilesToExecute;
                    long indexFileToExecute = 0;
                    while (!ErrorExist && !string.IsNullOrEmpty(nextFile.Key))
                    {
                        indexFileToExecute += 1;
                        InvokeCounterSetup(string.Format("Analisi {0} di {1}", indexFileToExecute.ToString(), counterFilesToExecute.ToString()));
                        result = ExecuteXmlScriptDocument(nextFile.Key, nextFile.Value);
                        if (result)
                            nextFile = GetNextFile();
                    }
                }
                else
                {
                    InvokeCounterSetup("Analisi terminata");
                    InvokeStartOperation("", 0);
                }
            }

            // 3 disconnessione
            
            if (connection != null)
            {
                InvokeCounterSetup("disconnessione");
                connection.Close();
                connection.Dispose();
            }

            return result;
        }

        public static class InputBox
        {
            public static DialogResult ShowMultiLine(string prompt, string caption, System.Windows.Forms.MessageBoxButtons buttons, ref string value)
            {
                string proposeValue = value;
                value = "";
                return ShowInput(prompt, caption, buttons, true, proposeValue, false, out value);
            }
            public static DialogResult Show(string prompt, string caption, System.Windows.Forms.MessageBoxButtons buttons, ref string value)
            {
                string proposeValue = value;
                value = "";
                return ShowInput(prompt, caption, buttons, false, proposeValue, false, out value);
            }

            public static DialogResult ShowMultiLine(string prompt, string caption, System.Windows.Forms.MessageBoxButtons buttons, bool readOnly, ref string value)
            {
                string proposeValue = value;
                value = "";
                return ShowInput(prompt, caption, buttons, true, proposeValue, readOnly, out value);
            }
            public static DialogResult Show(string prompt, string caption, System.Windows.Forms.MessageBoxButtons buttons, bool readOnly, ref string value)
            {
                string proposeValue = value;
                value = "";
                return ShowInput(prompt, caption, buttons, false, proposeValue, readOnly, out value);
            }

            private static DialogResult ShowInput(string prompt, string caption, System.Windows.Forms.MessageBoxButtons buttons, bool multiLine, string proposeValue, bool readOnly, out string value)
            {
                value = proposeValue;
                DialogResult acceptButton = DialogResult.None;
                DialogResult cancelButton = DialogResult.None;
                DialogResult result = DialogResult.None;
                List<DialogResult> buttonsList = new List<DialogResult>();
                
                switch (buttons)
                {
                    case MessageBoxButtons.AbortRetryIgnore:
                        {
                            acceptButton = DialogResult.Retry;
                            cancelButton = DialogResult.Abort;
                            buttonsList.Add(DialogResult.Abort);
                            buttonsList.Add(DialogResult.Retry);
                            buttonsList.Add(DialogResult.Ignore);
                            break;
                        }
                    case MessageBoxButtons.OK:
                        {
                            acceptButton = DialogResult.OK;
                            cancelButton = DialogResult.OK;
                            buttonsList.Add(DialogResult.OK);
                            break;
                        }
                    case MessageBoxButtons.OKCancel:
                        {
                            acceptButton = DialogResult.OK;
                            cancelButton = DialogResult.Cancel;
                            buttonsList.Add(DialogResult.OK);
                            buttonsList.Add(DialogResult.Cancel);
                            break;
                        }
                    case MessageBoxButtons.RetryCancel:
                        {
                            acceptButton = DialogResult.Retry;
                            cancelButton = DialogResult.Cancel;
                            buttonsList.Add(DialogResult.Retry);
                            buttonsList.Add(DialogResult.Cancel);
                            break;
                        }
                    case MessageBoxButtons.YesNo:
                        {
                            acceptButton = DialogResult.Yes;
                            cancelButton = DialogResult.No;
                            buttonsList.Add(DialogResult.Yes);
                            buttonsList.Add(DialogResult.No);
                            break;
                        }
                    case MessageBoxButtons.YesNoCancel:
                        {
                            acceptButton = DialogResult.Yes;
                            cancelButton = DialogResult.Cancel;
                            buttonsList.Add(DialogResult.Yes);
                            buttonsList.Add(DialogResult.No);
                            buttonsList.Add(DialogResult.Cancel);
                            break;
                        }
                }

                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("prompt", prompt);
                parameters.Add("caption", caption);
                parameters.Add("buttons", buttonsList);

                Form formDialog = new Form();



                formDialog.Text = caption;
                formDialog.Tag = "";
                formDialog.AutoScaleMode = AutoScaleMode.None;
                formDialog.ControlBox = false;
                formDialog.FormBorderStyle = FormBorderStyle.FixedDialog;
                formDialog.Font = new System.Drawing.Font("Calibri", 12, System.Drawing.FontStyle.Regular);
                formDialog.StartPosition = FormStartPosition.CenterScreen;
                formDialog.TopMost = true;

                formDialog.Shown += delegate(object sender, EventArgs e){

                    Panel panelPrompt = new Panel();

                    formDialog.Controls.Add(panelPrompt);
                    panelPrompt.Padding = new Padding(3);
                    panelPrompt.BorderStyle = BorderStyle.FixedSingle;
                    panelPrompt.AutoSize = true;
                    panelPrompt.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    panelPrompt.Margin = new Padding(0, 3, 0, 3);
                    panelPrompt.BringToFront();

                    Label labelPrompt = new Label();
                    panelPrompt.Controls.Add(labelPrompt);
                    labelPrompt.AutoSize = true;
                    labelPrompt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    labelPrompt.Text = prompt;
                    labelPrompt.BringToFront();
                    labelPrompt.Top = panelPrompt.Padding.Top;
                    labelPrompt.Left = panelPrompt.Padding.Left;
                    labelPrompt.Refresh();

                    TextBox textBox = new TextBox();
                    panelPrompt.Controls.Add(textBox);
                    textBox.BringToFront();
                    if (multiLine)
                    {
                        textBox.Multiline = true;
                        textBox.ScrollBars = ScrollBars.Both;
                        textBox.Dock = DockStyle.Fill;
                    }
                    else
                    {
                        textBox.Top = labelPrompt.Bottom + panelPrompt.Margin.Top;
                        textBox.Left = panelPrompt.Padding.Left;
                        textBox.Refresh();
                    }

                    panelPrompt.ClientSizeChanged += delegate (object senderPanel, EventArgs ePanel)
                    {
                        textBox.Width = panelPrompt.ClientSize.Width - panelPrompt.Padding.Horizontal;
                    };

                    formDialog.ClientSizeChanged += delegate (object form, EventArgs eForm)
                    {
                        panelPrompt.Width = formDialog.ClientSize.Width - formDialog.Padding.Horizontal;
                    };

                    panelPrompt.MinimumSize = new System.Drawing.Size(panelPrompt.Width, labelPrompt.Height + panelPrompt.Margin.Top + textBox.Height + panelPrompt.Padding.Vertical);
                    panelPrompt.AutoSize = false;
                    panelPrompt.Size = panelPrompt.MinimumSize;
                    panelPrompt.Refresh();
                    Application.DoEvents();

                    Panel panelButtons = new Panel();

                    textBox.TextChanged += delegate (object senderTextBox, EventArgs eArg)
                    {
                        formDialog.Tag = textBox.Text;
                        bool acceptEnabled = !string.IsNullOrEmpty(textBox.Text) && !string.IsNullOrWhiteSpace(textBox.Text);
                        foreach (Button btn in panelButtons.Controls)
                        {
                            if (acceptButton == btn.DialogResult)
                            {
                                btn.Enabled = acceptEnabled;
                                break;
                            }
                        }
                    };

                    panelButtons.Top = panelPrompt.Bottom + formDialog.Padding.Top;
                    formDialog.Controls.Add(panelButtons);
                    panelButtons.Padding = new Padding(3);
                    panelButtons.Margin = new Padding(3, 0, 3, 0);
                    panelButtons.BringToFront();
                    panelButtons.Top = panelPrompt.Bottom + formDialog.Padding.Top;
                    panelButtons.Refresh();

                    Button lastButton = null;
                    foreach (DialogResult dResult in (List<DialogResult>)parameters["buttons"])
                    {
                        Button btn = new Button();
                        btn.DialogResult = dResult;
                        btn.AutoSize = true;
                        btn.MinimumSize = new System.Drawing.Size(80, 40);
                        btn.Size = btn.MinimumSize;
                        if (dResult == acceptButton)
                        {
                            if (!multiLine)
                                formDialog.AcceptButton = btn;
                            btn.Enabled = false;
                        }
                        if (dResult == cancelButton) formDialog.CancelButton = btn;
                        switch (dResult)
                        {
                            case DialogResult.Abort: { btn.Text = "&Abbandona"; break; }
                            case DialogResult.Cancel: { btn.Text = "&Cancella"; break; }
                            case DialogResult.Ignore: { btn.Text = "&Ignora"; break; }
                            case DialogResult.No: { btn.Text = "&No"; break; }
                            case DialogResult.OK: { btn.Text = "&OK"; break; }
                            case DialogResult.Retry: { btn.Text = "&Riprova"; break; }
                            case DialogResult.Yes: { btn.Text = "&Si"; break; }
                        }

                        if (panelButtons.Controls.Count == 0)
                        {
                            panelButtons.MinimumSize = new System.Drawing.Size(btn.Width + panelButtons.Padding.Horizontal, btn.Height + panelButtons.Padding.Vertical);
                        }

                        panelButtons.Controls.Add(btn);
                        btn.BringToFront();
                        Application.DoEvents();
                        btn.Refresh();
                        btn.Top = panelButtons.Padding.Top;
                        if (panelButtons.Controls.Count == 1)
                        {
                            btn.Left = panelButtons.Padding.Left;
                        }
                        else
                        {
                            btn.Left = lastButton.Right + panelButtons.Margin.Left;
                        }
                        btn.Refresh();
                        lastButton = btn;
                        panelButtons.Width = btn.Right + panelButtons.Padding.Right;
                        panelButtons.Height = btn.Bottom + panelButtons.Padding.Bottom;
                        panelButtons.Refresh();
                        formDialog.Refresh();
                        Application.DoEvents();
                    }

                    panelButtons.SizeChanged += delegate (object panel, EventArgs ePanel)
                    {
                        int left = panelButtons.Padding.Left;
                        int right = panelButtons.Padding.Right;
                        int totWidth = 0;
                        foreach (Button btn in panelButtons.Controls)
                        {
                            totWidth += btn.Width;
                        }
                        int width = panelButtons.ClientSize.Width - panelButtons.Padding.Horizontal - totWidth;
                        int margin = (panelButtons.Controls.Count > 1 ? width / (panelButtons.Controls.Count - 1) : 0);
                        foreach (Button btn in panelButtons.Controls)
                        {
                            btn.Left = left;
                            btn.Refresh();
                            left = btn.Right + margin;
                        }
                    };


                    if (!string.IsNullOrEmpty(proposeValue) && !string.IsNullOrWhiteSpace(proposeValue))
                    {
                        textBox.Text = proposeValue;
                    }
                    textBox.Enabled = (!readOnly);

                    if (multiLine)
                    {
                        panelPrompt.Height = panelPrompt.Height * 4;
                    }

                    panelPrompt.Location = new System.Drawing.Point(formDialog.Padding.Left, formDialog.Padding.Top);
                    panelButtons.Location = new System.Drawing.Point(formDialog.Padding.Left, panelPrompt.Bottom + formDialog.Padding.Top);
                    formDialog.MinimumSize = new System.Drawing.Size(300, 140);
                    formDialog.ClientSize = new System.Drawing.Size((int)System.Math.Max(panelPrompt.Width, panelButtons.Width) + formDialog.Padding.Horizontal, 
                                                                     panelPrompt.Height + panelButtons.Height + formDialog.Padding.Vertical * 3);
                    if (formDialog.ClientSize.Width < formDialog.Width - SystemInformation.BorderSize.Width * 2 - formDialog.Padding.Horizontal)
                    {
                        formDialog.ClientSize = new System.Drawing.Size(formDialog.Width = formDialog.Width - SystemInformation.BorderSize.Width * 2 - formDialog.Padding.Horizontal, formDialog.ClientSize.Height); 
                    }
                    panelPrompt.Width = formDialog.ClientSize.Width - formDialog.Padding.Horizontal;
                    panelButtons.Width = formDialog.ClientSize.Width - formDialog.Padding.Horizontal;
                    formDialog.Refresh();
                    Application.DoEvents();
                    formDialog.Refresh();
                    textBox.Focus();
                };

                result = formDialog.ShowDialog();
                value = (string)formDialog.Tag;
                formDialog.Close();
                formDialog.Dispose();

                return result;
            }
        }

        
        public bool Aggiornamenti()
        {
            bool result = true;

            List<string> lista_connessioni = new List<string>();

            try
            {
                remoteConnection = false;
                remoteConnectionString = "";
                System.Windows.Forms.DialogResult dialogOption = System.Windows.Forms.MessageBox.Show("Connessione locale ?", "tipo di connessione", System.Windows.Forms.MessageBoxButtons.YesNoCancel);
                result = (dialogOption != System.Windows.Forms.DialogResult.Cancel);
                if (result)
                {
                    remoteConnection = dialogOption == System.Windows.Forms.DialogResult.No;
                    if (remoteConnection)
                    {
                        string valueDataSource = "";
                        result = (InputBox.Show("Nome connessione", "Input", MessageBoxButtons.OKCancel, ref valueDataSource) == DialogResult.OK);
                        if (result)
                            remoteConnectionString = valueDataSource;
                    }
                }

                if (result)
                {
                    XmlDocument xDocument = new XmlDocument();
                    xDocument.LoadXml(OptimizeSystem.Properties.Resources.connessioni);

                    foreach(XmlNode node_connessione in xDocument.GetElementsByTagName("lista_connessioni")[0].ChildNodes)
                    {
                        dataXmlFiles = null;
                        currentKey = "";
                        connectionString = "";
                        tableVersion = "";
                        fieldVersion = "";
                        scriptTableVersion = new List<string>();
                        tempTableVersion = "";

                        currentKey = node_connessione.Attributes["key"].Value;
                        foreach (XmlNode node in node_connessione.ChildNodes)
                        {
                            switch (node.Name)
                            {
                                case "connection_string":
                                    {
                                        connectionString = node.InnerText;
                                        break;
                                    }
                                case "table_version":
                                    {
                                        tableVersion = node.Attributes["table_name"].Value;
                                        fieldVersion = node.Attributes["version_field"].Value;
                                        foreach (XmlNode node_scripts in node.ChildNodes)
                                        {
                                            if (node_scripts.Name == "scripts")
                                            {
                                                foreach (XmlNode node_table_version_script in node_scripts.ChildNodes)
                                                {
                                                    if (node_table_version_script.Name == "script")
                                                    {
                                                        string scriptValue = node_table_version_script.InnerText;
                                                        scriptValue = scriptValue.Replace("\r\n", " ");
                                                        scriptValue = scriptValue.Replace("\r", " ");
                                                        scriptValue = scriptValue.Replace("\n", " ");
                                                        scriptTableVersion.Add(scriptValue);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                case "temp_table_version":
                                    {
                                        tempTableVersion = node.Attributes["table_name"].Value;
                                        break;
                                    }
                            }
                        }

                        if (!string.IsNullOrEmpty(currentKey)
                            && !string.IsNullOrEmpty(connectionString)
                            && !string.IsNullOrEmpty(tableVersion)
                            && !string.IsNullOrEmpty(fieldVersion)
                            && !string.IsNullOrEmpty(tempTableVersion))
                        {
                            result = Aggiornamento();
                            if (!result) break;
                        }
                        else
                        {
                            result = false;
                            Error = new Exception("connessioni.xml non corretto");
                            InvokeExceptionOperation();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                result = false;
                InvokeExceptionOperation();
            }
            return result;
        }

        #endregion







        #region editor

        public static string Beautify(XmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            return sb.ToString();
        }

        public string GetNewFileNameXml(bool fullPath)
        {
            System.IO.FileInfo oFileInfoApplication = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string path = oFileInfoApplication.Directory.FullName + @"\data";
            string lastFileName = "";
            foreach (string itemFile in System.IO.Directory.GetFiles(path, "script_*.xml"))
            {
                if (string.IsNullOrEmpty(lastFileName) || string.Compare(itemFile, lastFileName) == -1)
                {
                    System.IO.FileInfo oFInfo = new FileInfo(itemFile);
                    lastFileName = oFInfo.Name ;
                }
            }
            string fileName = "";
            DateTime dNow = DateTime.Now;
            string cNow = string.Format("{0}_{1}_{2}_{3}_{4}", dNow.Year.ToString("0000"), dNow.Month.ToString("00"), dNow.Day.ToString("00"), dNow.Hour.ToString("00"), dNow.Minute.ToString("00"));
            int counter = 1;
            if (!string.IsNullOrEmpty(lastFileName))
            {
                lastFileName = lastFileName.Replace("script_", "").Replace(".xml", "");
                counter = int.Parse(lastFileName.Substring(cNow.Length + 1)) + 1;
            }
            fileName = string.Format("script_{0}_{1}.xml", cNow, counter.ToString("00000000"));
            return (fullPath ? path + fileName : fileName);
        }

        public string SelectConnectionKey()
        {
            string result = "";
            using (Form frmMenu = new Form())
            {
                frmMenu.StartPosition = FormStartPosition.CenterScreen;
                frmMenu.WindowState = FormWindowState.Normal;
                frmMenu.FormBorderStyle = FormBorderStyle.FixedDialog;

                Button btn = null;

                try
                {
                    XmlDocument xDocument = new XmlDocument();
                    xDocument.LoadXml(OptimizeSystem.Properties.Resources.connessioni);

                    foreach (XmlNode node_connessione in xDocument.GetElementsByTagName("lista_connessioni")[0].ChildNodes)
                    {
                        currentKey = node_connessione.Attributes["key"].Value;
                        if (!string.IsNullOrEmpty(node_connessione.Attributes["key"].Value))
                        {
                            btn = new Button();
                            btn.Text = node_connessione.Attributes["key"].Value;
                            btn.MinimumSize = new System.Drawing.Size(80, 40);
                            btn.Dock = DockStyle.Top;
                            frmMenu.Controls.Add(btn);
                            btn.BringToFront();
                            btn.DialogResult = DialogResult.OK;
                            btn.Click += delegate (object sender, EventArgs e)
                            {
                                frmMenu.Tag = btn.Text;
                            };
                        }
                        else
                        {
                            Error = new Exception("connessioni.xml non corretto");
                            InvokeExceptionOperation();
                            break;
                        }
                    }

                    if (frmMenu.Controls.Count > 0)
                    {
                        if (frmMenu.ShowDialog() == DialogResult.OK)
                        {
                            result = frmMenu.Tag.ToString();
                        }
                    }
                }
                catch (Exception)
                {
                }
                frmMenu.Close();
            }
            return result;
        }

        public bool CheckExistsDataFile(out Exception Error)
        {
            Error = null;
            if (!System.IO.File.Exists(GetDataFileName()))
                ZipFiles(out Error);
            return Error == null;
        }

        public bool ZipFiles(out Exception Error )
        {
            Error = null;
            System.IO.FileInfo oFileInfoApplication = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dataFileName = GetDataFileName();
            DirectoryInfo dirFilesToZip = new DirectoryInfo(oFileInfoApplication.Directory.FullName + @"\data\");
            List<string> filesToZip = new List<string>();
            foreach (FileInfo oFInfoToZip in dirFilesToZip.GetFiles("script_*.xml"))
            {
                filesToZip.Add(oFInfoToZip.FullName);
            }
            if (filesToZip.Count > 0)
                Zip(dataFileName, filesToZip, out Error);
            return Error == null;
        }

        private bool Zip(string zipFile, List<String> filesToZip, out Exception Error)
        {
            Error = null;
            bool result = true;
            string zipFileBck = zipFile + ".bck";
            if (System.IO.File.Exists(zipFileBck))
            {
                System.IO.File.Delete(zipFileBck);
            }
            if (System.IO.File.Exists(zipFile))
            {
                System.IO.File.Copy(zipFile, zipFileBck);
                System.IO.File.Delete(zipFile);
            }
            ZipOutputStream oST;
            oST = new ZipOutputStream(System.IO.File.Create(zipFile));
            try
            {
                oST.SetLevel(9);
                byte[] buffer = new byte[4096];
                int count = 0;
                foreach (string fileName in filesToZip)
                {
                    FileStream oSF = System.IO.File.OpenRead(fileName);
                    try
                    {
                        ZipEntry oEntry = new ZipEntry(fileName);
                        oEntry.Size = oSF.Length;
                        oST.PutNextEntry(oEntry);
                        StreamUtils.Copy(oSF, oST, buffer);
                    }
                    catch (Exception exAddFile)
                    {
                        result = false;
                        Error = exAddFile;
                        break;
                    }
                    finally
                    {
                        oSF.Close();
                    }
                }
                oST.Finish();

            }
            catch (Exception exCreateZip)
            {
                result = false;
                Error = exCreateZip;
            }
            finally
            {
                oST.Close();
            }

            if (!result)
            {
                if (System.IO.File.Exists(zipFile))
                {
                    System.IO.File.Delete(zipFile);
                }
                if (System.IO.File.Exists(zipFileBck))
                {
                    System.IO.File.Copy(zipFileBck, zipFile);
                }
            }
            if (System.IO.File.Exists(zipFileBck))
            {
                System.IO.File.Delete(zipFileBck);
            }

            return result;
        }

        public bool AddScript(XmlDocument document, string fullScript)
        {
            List<string> scriptTags = new List<string>()
            {
                "create type",
                "create table", "alter table",
                "create view", "create or replace view",
                "create trigger", "create or replace trigger",
                "create sequence",
                "create index", "create unique index",
                "create procedure", "create or replace procedure",
                "create function", "create or replace function",
                "drop table", "drop type", "drop view", "drop trigger", "drop sequence", "drop index", "drop procedure", "drop function"
            };

            return true;
        }

        public class scriptItem
        {
            public string operation { get; set; }
            public string type { get; set; }
            public string name { get; set; }

            public string script { get; set; }

            public static List<string> ScriptTags()
            {
                List<string> scriptTags = new List<string>()
                    {
                        "create type",
                        "create table", "alter table",
                        "create view", "create or replace view",
                        "create trigger", "create or replace trigger",
                        "create sequence",
                        "create index", "create unique index",
                        "create procedure", "create or replace procedure",
                        "create function", "create or replace function",
                        "drop table", "drop type", "drop view", "drop trigger", "drop sequence", "drop index", "drop procedure", "drop function"
                    };
                return scriptTags;
            }

            public static List<scriptItem> Parse(string script)
            {
                List<scriptItem> result = new List<scriptItem>();
                List<string> scriptTags = ScriptTags();
                List<string> items = new List<string>();

                string tagName = "";
                string operation = "";
                string type = "";

                int indexTag = 0;
                int startindex = 0;

                while (indexTag >= 0)
                {
                    tagName = "";
                    operation = "";
                    type = "";
                    indexTag = -1;

                    int searchIndexTag = -1;
                    foreach (string tag in scriptTags)
                    {
                        tagName = tag;
                        operation = tag.Split(' ')[0];
                        type = tag.Split(' ')[tag.Split(' ').Length - 1];
                        searchIndexTag = script.IndexOf(tagName, startindex);
                        if (searchIndexTag < 0) { tagName = tagName.ToUpper(); searchIndexTag = script.IndexOf(tagName, startindex); }

                        if (searchIndexTag >= 0)
                        {
                            if (indexTag == -1 || searchIndexTag < indexTag)
                                indexTag = searchIndexTag;
                        }
                    }

                    if (indexTag >= 0)
                    {
                        if (indexTag == 0)
                        {
                            startindex = 1;
                        }
                        else
                        {
                            items.Add(script.Substring(0, indexTag - 1));
                            script = script.Substring(indexTag);
                            startindex = 0;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(script))
                    items.Add(script);

                foreach (string item in items)
                {
                    tagName = "";
                    foreach (string tag in scriptTags)
                    {
                        operation = tag.Split(' ')[0];
                        type = tag.Split(' ')[tag.Split(' ').Length - 1];
                        if (item.Contains(tag) || item.Contains(tag.ToUpper()))
                        {
                            tagName = tag;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(tagName))
                    {
                        scriptItem newItem = new scriptItem();
                        newItem.operation = operation;
                        newItem.type = type;
                        newItem.name = item.Substring(tagName.Length).TrimStart().Split(' ')[0];
                        newItem.script = item;
                        result.Add(newItem);
                    }
                }

                return result;
            }
        }



        #endregion

    }
}
