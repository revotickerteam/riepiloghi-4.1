﻿namespace OptimizeSystem
{
    partial class frmSetupRiepiloghi
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSetup = new System.Windows.Forms.Panel();
            this.txtOperation = new System.Windows.Forms.TextBox();
            this.progressBarSetup = new System.Windows.Forms.ProgressBar();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlLog = new System.Windows.Forms.Panel();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.splitter = new System.Windows.Forms.Splitter();
            this.progressBarOperation = new System.Windows.Forms.ProgressBar();
            this.pnlSetup.SuspendLayout();
            this.pnlLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSetup
            // 
            this.pnlSetup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSetup.Controls.Add(this.txtOperation);
            this.pnlSetup.Controls.Add(this.progressBarOperation);
            this.pnlSetup.Controls.Add(this.progressBarSetup);
            this.pnlSetup.Controls.Add(this.lblTitle);
            this.pnlSetup.Controls.Add(this.btnClose);
            this.pnlSetup.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSetup.Location = new System.Drawing.Point(0, 0);
            this.pnlSetup.Name = "pnlSetup";
            this.pnlSetup.Padding = new System.Windows.Forms.Padding(3);
            this.pnlSetup.Size = new System.Drawing.Size(800, 151);
            this.pnlSetup.TabIndex = 0;
            // 
            // txtOperation
            // 
            this.txtOperation.BackColor = System.Drawing.SystemColors.Control;
            this.txtOperation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOperation.Location = new System.Drawing.Point(3, 68);
            this.txtOperation.Multiline = true;
            this.txtOperation.Name = "txtOperation";
            this.txtOperation.ReadOnly = true;
            this.txtOperation.Size = new System.Drawing.Size(720, 52);
            this.txtOperation.TabIndex = 3;
            // 
            // progressBarSetup
            // 
            this.progressBarSetup.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBarSetup.Location = new System.Drawing.Point(3, 38);
            this.progressBarSetup.Name = "progressBarSetup";
            this.progressBarSetup.Size = new System.Drawing.Size(720, 30);
            this.progressBarSetup.TabIndex = 2;
            // 
            // lblTitle
            // 
            this.lblTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(720, 35);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Verifiche in corso...";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.Enabled = false;
            this.btnClose.Location = new System.Drawing.Point(723, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 141);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Chiudi";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlLog
            // 
            this.pnlLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlLog.Controls.Add(this.txtLog);
            this.pnlLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLog.Location = new System.Drawing.Point(0, 165);
            this.pnlLog.Name = "pnlLog";
            this.pnlLog.Padding = new System.Windows.Forms.Padding(3);
            this.pnlLog.Size = new System.Drawing.Size(800, 285);
            this.pnlLog.TabIndex = 1;
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(3, 3);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(790, 275);
            this.txtLog.TabIndex = 3;
            // 
            // splitter
            // 
            this.splitter.BackColor = System.Drawing.Color.Gainsboro;
            this.splitter.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter.Location = new System.Drawing.Point(0, 151);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(800, 14);
            this.splitter.TabIndex = 2;
            this.splitter.TabStop = false;
            // 
            // progressBarOperation
            // 
            this.progressBarOperation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarOperation.Location = new System.Drawing.Point(3, 120);
            this.progressBarOperation.Name = "progressBarOperation";
            this.progressBarOperation.Size = new System.Drawing.Size(720, 24);
            this.progressBarOperation.TabIndex = 5;
            // 
            // frmSetupRiepiloghi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.pnlLog);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.pnlSetup);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmSetupRiepiloghi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analisi e Ottimizzazione DB";
            this.pnlSetup.ResumeLayout(false);
            this.pnlSetup.PerformLayout();
            this.pnlLog.ResumeLayout(false);
            this.pnlLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSetup;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ProgressBar progressBarSetup;
        private System.Windows.Forms.TextBox txtOperation;
        private System.Windows.Forms.Panel pnlLog;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Splitter splitter;
        private System.Windows.Forms.ProgressBar progressBarOperation;
    }
}

