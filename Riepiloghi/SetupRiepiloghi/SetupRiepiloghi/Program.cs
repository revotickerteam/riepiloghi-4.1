﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OptimizeSystem
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            bool editMode = false;
            List<string> args = new List<string>();
            if (Environment.GetCommandLineArgs() != null)
            {
                args = new List<string>(Environment.GetCommandLineArgs());
            }
            editMode = args.Contains("EDIT");
            if (editMode)
                Application.Run(new frmEditMode());
            else
                Application.Run(new frmSetupRiepiloghi());
        }
    }
}
