﻿using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace OptimizeSystem
{
    public partial class frmEditMode : Form
    {
        private clsOptimizeSystem oOptimize = new clsOptimizeSystem();
        private Dictionary<string, string> files { get; set; }

        private long lastVersion { get; set; }
        private XmlDocument currentDocument;

        public frmEditMode()
        {
            InitializeComponent();
            lastVersion = 0;
            this.Shown += FrmEditMode_Shown;
            this.lstFiles.SelectedValueChanged += LstFiles_SelectedValueChanged;
        }

        private void LstFiles_SelectedValueChanged(object sender, EventArgs e)
        {
            foreach (Control c in this.pnlButtons.Controls)
            {
                if (c.Tag != null && c.Tag.ToString() != "delete_file")
                {
                    c.Enabled = this.lstFiles.SelectedItem != null;
                }
            }

            if (this.lstFiles.SelectedItem != null)
            {
                this.txtXml.Enabled = true;
                this.txtXml.Text = "";
                foreach (KeyValuePair<string, string> itemFile in files)
                {
                    string key = "";
                    long versionFile = 0;
                    if (itemFile.Key == this.lstFiles.SelectedItem.ToString())
                    {
                        XmlDocument document;
                        if (oOptimize.GetXmlDocAndVersionFile(itemFile.Key, itemFile.Value, out key, out versionFile, out document))
                        {
                            currentDocument = document;
                            this.lblXml.Text = string.Format("contenuto xml versione {0}", versionFile.ToString());
                            this.txtXml.Text = itemFile.Value;
                        }
                        break;
                    }
                }
            }
            else
            {
                this.txtXml.Enabled = false;
                this.txtXml.Text = "";
            }
        }

        private void FrmEditMode_Shown(object sender, EventArgs e)
        {
            this.InitEditor();
            Exception Error = null;
            oOptimize.CheckExistsDataFile(out Error);
            this.LoadFiles();
        }


        private void InitEditor()
        {
            Button btn = null;

            btn = new Button();
            btn.AutoSize = false;
            btn.Text = "aggiungi file";
            btn.Tag = "add_file";
            btn.Click += Btn_Click;
            btn.Dock = DockStyle.Top;
            btn.MinimumSize = new Size(80, 40);
            btn.Size = btn.MinimumSize;
            this.pnlButtons.Controls.Add(btn);
            btn.BringToFront();

            btn = new Button();
            btn.AutoSize = false;
            btn.Text = "elimina file";
            btn.Tag = "delete_file";
            btn.Click += Btn_Click;
            btn.Dock = DockStyle.Top;
            btn.MinimumSize = new Size(80, 40);
            btn.Size = btn.MinimumSize;
            this.pnlButtons.Controls.Add(btn);
            btn.BringToFront();
            btn.Enabled = false;

            Dictionary<string, string> descTypes = oOptimize.GetTypesUtility();
            foreach (KeyValuePair<string, string> item in descTypes)
            {
                btn = new Button();
                btn.AutoSize = false;
                btn.Text = item.Value;
                btn.Tag = item.Key;
                btn.Click += Btn_Click;
                btn.Dock = DockStyle.Top;
                btn.MinimumSize = new Size(80, 40);
                btn.Size = btn.MinimumSize;
                this.pnlButtons.Controls.Add(btn);
                btn.BringToFront();
                btn.Enabled = false;
            }

            

        }

        private void Btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string value = (string)btn.Tag;
            string fileName = oOptimize.GetNewFileNameXml(false);
            string scriptCode = "";


            if (value == "add_file")
            {
                if (clsOptimizeSystem.InputBox.Show("Nome", "Nome del file xml che conterrà gli scripts", MessageBoxButtons.OKCancel, true, ref fileName) == DialogResult.OK)
                {
                    scriptCode = oOptimize.SelectConnectionKey();
                    if (!string.IsNullOrEmpty(scriptCode))
                    {
                        lastVersion += 1;
                        string baseString = string.Format(OptimizeSystem.Properties.Resources._base, scriptCode, lastVersion.ToString());

                        System.IO.FileInfo oFileInfoApplication = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        string path = oFileInfoApplication.Directory.FullName + @"\data\" + fileName;

                        System.IO.File.AppendAllText(path, baseString);

                        Exception Error = null;
                        oOptimize.ZipFiles(out Error);
                        if (Error != null)
                        {
                            MessageBox.Show(Error.Message, "Errore");
                        }
                        this.LoadFiles();
                    }
                        
                }
            }
            else if(value == "delete_file")
            {

            }
            else
            {
                this.AddType(value);
            }
        }

        private void LoadFiles()
        {
            lastVersion = 0;
            this.lstFiles.Items.Clear();
            files = oOptimize.GetXmlFiles(true);
            foreach (KeyValuePair<string, string> itemFile in files)
            {
                string key = "";
                long versionFile = 0;
                if (oOptimize.GetVersionXmlFile(itemFile.Key, itemFile.Value, out key, out versionFile))
                {
                    if (lastVersion < versionFile) lastVersion = versionFile;
                    this.lstFiles.Items.Add(itemFile.Key);
                }
            }
        }

        private bool AddType(string TypeAdd)
        {
            bool result = false;
            string name = "";

            if (TypeAdd == "generic")
            {
                string script = "";
                if (clsOptimizeSystem.InputBox.ShowMultiLine("Script", "Script", MessageBoxButtons.OKCancel, ref script) == DialogResult.OK)
                {
                    List<clsOptimizeSystem.scriptItem> items = clsOptimizeSystem.scriptItem.Parse(script);
                    foreach (clsOptimizeSystem.scriptItem item in items)
                    {
                        XmlNode oNode = currentDocument.CreateNode(XmlNodeType.Element, item.type, "");
                        XmlAttribute oAttribute = currentDocument.CreateAttribute("name");
                        oAttribute.Value = item.name;
                        oNode.Attributes.Append(oAttribute);

                        oAttribute = currentDocument.CreateAttribute("file");
                        oAttribute.Value = "";
                        oNode.Attributes.Append(oAttribute);

                        oNode.InnerText = "\r\n" + item.script + "\r\n";
                        currentDocument.GetElementsByTagName("script_list")[0].AppendChild(oNode);
                    }
                    string fullInnerXml = clsOptimizeSystem.Beautify(currentDocument);
                    this.txtXml.Text = fullInnerXml;
                    this.LoadFiles();
                }
            }
            else
            {

                if (clsOptimizeSystem.InputBox.Show("Nome", string.Format("Nome singolo scrip/oggetto {0}", TypeAdd), MessageBoxButtons.OKCancel, ref name) == DialogResult.OK)
                {
                    XmlNode oNode = currentDocument.CreateNode(XmlNodeType.Element, TypeAdd, "");
                    XmlAttribute oAttribute = currentDocument.CreateAttribute("name");
                    oAttribute.Value = name;
                    oNode.Attributes.Append(oAttribute);

                    oAttribute = currentDocument.CreateAttribute("file");
                    oAttribute.Value = "";
                    oNode.Attributes.Append(oAttribute);

                    string innerScript = "";
                    if (clsOptimizeSystem.InputBox.ShowMultiLine("Script", string.Format("Script", TypeAdd), MessageBoxButtons.OKCancel, ref innerScript) == DialogResult.OK)
                    {
                        oNode.InnerText = "\r\n" + innerScript + "\r\n";
                        currentDocument.GetElementsByTagName("script_list")[0].AppendChild(oNode);
                        string fullInnerXml = clsOptimizeSystem.Beautify(currentDocument);
                        this.txtXml.Text = fullInnerXml;
                        this.LoadFiles();
                    }
                }
            }
            return result;
        }

        


    }
}
