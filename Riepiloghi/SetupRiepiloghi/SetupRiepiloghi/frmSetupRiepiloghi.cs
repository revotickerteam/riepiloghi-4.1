﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OptimizeSystem
{
    public partial class frmSetupRiepiloghi : Form
    {
        clsOptimizeSystem oSetup;

        public frmSetupRiepiloghi()
        {
            InitializeComponent();
            this.Shown += FrmSetupRiepiloghi_Shown;
            this.btnClose.Click += BtnClose_Click;
        }

        private void FrmSetupRiepiloghi_Shown(object sender, EventArgs e)
        {
            this.TopMost = true;
            Esecuzione();
        }
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
            this.Dispose();
        }


        private void Esecuzione()
        {
            oSetup = new clsOptimizeSystem();
            oSetup.CounterSetup += OSetup_CounterSetup;
            oSetup.StartOperation += OSetup_StartOperation;
            oSetup.EndOperation += OSetup_EndOperation;
            oSetup.DetailOperation += OSetup_DetailOperation;
            oSetup.CounterOperation += OSetup_CounterOperation;
            oSetup.ExceptionOperation += OSetup_ExceptionOperation;

            try
            {
                oSetup.Aggiornamenti();
            }
            catch (Exception ex)
            {
                UpdateFormControlTextBox(this.txtLog, ex.ToString());
            }

            this.btnClose.Enabled = true;
        }

        private void OSetup_ExceptionOperation()
        {
            if (oSetup.ErrorExist)
            {
                UpdateFormControlTextBox(this.txtLog, oSetup.Error.ToString());
            }
        }

        private void OSetup_CounterSetup(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                UpdateFormControlText(this.lblTitle, message);
                UpdateFormControlTextBox(this.txtLog, message);
            }
            UpdateProgressBar(this.progressBarSetup, oSetup.counterSetup, oSetup.maximumSetup);
        }

        private void OSetup_StartOperation()
        {
            this.UpdateFormControlText(this.txtOperation, oSetup.Operation);
            UpdateFormControlTextBox(this.txtLog, oSetup.Operation);
        }

        private void OSetup_DetailOperation(string message)
        {
            UpdateFormControlTextBox(this.txtLog, message);
        }
        private void OSetup_CounterOperation()
        {
            UpdateProgressBar(this.progressBarOperation, oSetup.counterOperation, oSetup.maximumOperation);
        }

        private void OSetup_EndOperation()
        {
            this.UpdateFormControlText(this.txtOperation, oSetup.Operation + " FINE");
            UpdateFormControlTextBox(this.txtLog, oSetup.Operation + " FINE");
        }

        private delegate void delegateUpdateProgressBar(ProgressBar p, int n, int m);

        private void UpdateProgressBar(ProgressBar p, int n, int m)
        {
            if (this.InvokeRequired)
            {
                delegateUpdateProgressBar d = new delegateUpdateProgressBar(UpdateProgressBar);
                this.Invoke(d, p, n, m);
            }
            else
            {
                if (p.Maximum != m)
                {
                    p.Value = 0;
                    p.Maximum = m;
                    Application.DoEvents();
                }

                if (p.Value != n)
                {
                    if (n <= p.Maximum) { p.Value = n; Application.DoEvents(); }
                }
            }
        }

        private delegate void delegateInvokeUpdateFormControlText(Control c, string text);

        private void UpdateFormControlText(Control c, string text)
        {
            if (this.InvokeRequired)
            {
                delegateInvokeUpdateFormControlText d = new delegateInvokeUpdateFormControlText(UpdateFormControlText);
                this.Invoke(d, c, text);
            }
            {
                c.Text = text;
                Application.DoEvents();
            }
            
        }

        private delegate void delegateInvokeUpdateFormControlTextBox(TextBox c, string text);

        private void UpdateFormControlTextBox(TextBox c, string text)
        {
            if (this.InvokeRequired)
            {
                delegateInvokeUpdateFormControlTextBox d = new delegateInvokeUpdateFormControlTextBox(UpdateFormControlTextBox);
                this.Invoke(d, c, text);
            }
            else
            {
                c.Text += (c.Text.Trim() == "" ? "" : "\r\n") + text;
                c.SelectionLength = 0;
                c.SelectionStart = c.Text.Length;
                c.ScrollToCaret();
                Application.DoEvents();
            }
        }
    }
}
