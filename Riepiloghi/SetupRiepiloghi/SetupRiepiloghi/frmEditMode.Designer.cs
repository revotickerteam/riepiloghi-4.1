﻿namespace OptimizeSystem
{
    partial class frmEditMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlData = new System.Windows.Forms.Panel();
            this.pnlXml = new System.Windows.Forms.Panel();
            this.txtXml = new System.Windows.Forms.TextBox();
            this.pnlXmlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblXml = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pnlFiles = new System.Windows.Forms.Panel();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.lblFiles = new System.Windows.Forms.Label();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.lblOperazioni = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlData.SuspendLayout();
            this.pnlXml.SuspendLayout();
            this.pnlXmlBottom.SuspendLayout();
            this.pnlFiles.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlData
            // 
            this.pnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlData.Controls.Add(this.pnlXml);
            this.pnlData.Controls.Add(this.splitter2);
            this.pnlData.Controls.Add(this.pnlFiles);
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(150, 0);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(650, 450);
            this.pnlData.TabIndex = 0;
            // 
            // pnlXml
            // 
            this.pnlXml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlXml.Controls.Add(this.txtXml);
            this.pnlXml.Controls.Add(this.pnlXmlBottom);
            this.pnlXml.Controls.Add(this.lblXml);
            this.pnlXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlXml.Location = new System.Drawing.Point(215, 0);
            this.pnlXml.Name = "pnlXml";
            this.pnlXml.Padding = new System.Windows.Forms.Padding(3);
            this.pnlXml.Size = new System.Drawing.Size(433, 448);
            this.pnlXml.TabIndex = 3;
            // 
            // txtXml
            // 
            this.txtXml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtXml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtXml.Location = new System.Drawing.Point(3, 30);
            this.txtXml.Multiline = true;
            this.txtXml.Name = "txtXml";
            this.txtXml.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtXml.Size = new System.Drawing.Size(425, 375);
            this.txtXml.TabIndex = 2;
            // 
            // pnlXmlBottom
            // 
            this.pnlXmlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlXmlBottom.Controls.Add(this.btnSave);
            this.pnlXmlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlXmlBottom.Location = new System.Drawing.Point(3, 405);
            this.pnlXmlBottom.Name = "pnlXmlBottom";
            this.pnlXmlBottom.Size = new System.Drawing.Size(425, 38);
            this.pnlXmlBottom.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 36);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Salva";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // lblXml
            // 
            this.lblXml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblXml.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblXml.Location = new System.Drawing.Point(3, 3);
            this.lblXml.Name = "lblXml";
            this.lblXml.Size = new System.Drawing.Size(425, 27);
            this.lblXml.TabIndex = 1;
            this.lblXml.Text = "contenuto xml";
            this.lblXml.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(207, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(8, 448);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // pnlFiles
            // 
            this.pnlFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFiles.Controls.Add(this.lstFiles);
            this.pnlFiles.Controls.Add(this.lblFiles);
            this.pnlFiles.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlFiles.Location = new System.Drawing.Point(0, 0);
            this.pnlFiles.Name = "pnlFiles";
            this.pnlFiles.Padding = new System.Windows.Forms.Padding(3);
            this.pnlFiles.Size = new System.Drawing.Size(207, 448);
            this.pnlFiles.TabIndex = 2;
            // 
            // lstFiles
            // 
            this.lstFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFiles.FormattingEnabled = true;
            this.lstFiles.ItemHeight = 19;
            this.lstFiles.Location = new System.Drawing.Point(3, 30);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(199, 413);
            this.lstFiles.TabIndex = 2;
            // 
            // lblFiles
            // 
            this.lblFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFiles.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFiles.Location = new System.Drawing.Point(3, 3);
            this.lblFiles.Name = "lblFiles";
            this.lblFiles.Size = new System.Drawing.Size(199, 27);
            this.lblFiles.TabIndex = 1;
            this.lblFiles.Text = "files";
            this.lblFiles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlButtons
            // 
            this.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlButtons.Controls.Add(this.lblOperazioni);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlButtons.Location = new System.Drawing.Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Padding = new System.Windows.Forms.Padding(3);
            this.pnlButtons.Size = new System.Drawing.Size(142, 450);
            this.pnlButtons.TabIndex = 1;
            // 
            // lblOperazioni
            // 
            this.lblOperazioni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOperazioni.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOperazioni.Location = new System.Drawing.Point(3, 3);
            this.lblOperazioni.Name = "lblOperazioni";
            this.lblOperazioni.Size = new System.Drawing.Size(134, 27);
            this.lblOperazioni.TabIndex = 1;
            this.lblOperazioni.Text = "operazioni";
            this.lblOperazioni.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(142, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(8, 450);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // frmEditMode
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlData);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlButtons);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmEditMode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmEditMode";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlData.ResumeLayout(false);
            this.pnlXml.ResumeLayout(false);
            this.pnlXml.PerformLayout();
            this.pnlXmlBottom.ResumeLayout(false);
            this.pnlFiles.ResumeLayout(false);
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlXml;
        private System.Windows.Forms.Label lblXml;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel pnlFiles;
        private System.Windows.Forms.Label lblFiles;
        private System.Windows.Forms.Label lblOperazioni;
        private System.Windows.Forms.ListBox lstFiles;
        private System.Windows.Forms.TextBox txtXml;
        private System.Windows.Forms.Panel pnlXmlBottom;
        private System.Windows.Forms.Button btnSave;
    }
}