<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes" method="xml" encoding="UTF-8" />
	<xsl:decimal-format name="it"  grouping-separator="." decimal-separator="," />
	<xsl:variable name="titolare" select="//titolare" />
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  main                   *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="/">
    <RiepilogoGiornaliero>
      <xsl:attribute name="Sostituzione">
        <xsl:value-of select="'S'"/>
      </xsl:attribute>
      <Titolare>
        <Denominazione>
          <xsl:value-of select="$titolare/denominazione_titolare"/>
        </Denominazione>
        <CodiceFiscale>
          <xsl:value-of select="$titolare/codice_fiscale_titolare"/>
        </CodiceFiscale>
        <SistemaEmissione>
          <xsl:value-of select="$titolare/codice_sistema"/>
        </SistemaEmissione>
      </Titolare>      
			<xsl:apply-templates select="//organizzatori" />
		</RiepilogoGiornaliero>
	</xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  organizzatori          *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="//organizzatori">
    <Organizzatore>
      <Denominazione>
        <xsl:value-of select="denominazione_organizzatore"/>
      </Denominazione>
      <CodiceFiscale>
        <xsl:value-of select="codice_fiscale_organizzatore"/>
      </CodiceFiscale>
      <TipoOrganizzatore>
        <xsl:attribute name="valore">
          <xsl:value-of select="'G'"/>
        </xsl:attribute>
      </TipoOrganizzatore>
      <xsl:apply-templates select="//eventi" />
      <xsl:apply-templates select="//abbonamenti" />
    </Organizzatore>
	</xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  eventi                 *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="//eventi">
    <Evento>
      <Intrattenimento>
        <TipoTassazione>
          <xsl:attribute name="valore">
            <xsl:value-of select="spettacolo_intrattenimento"/>
          </xsl:attribute>
        </TipoTassazione>
        <Incidenza>
          <xsl:value-of select="incidenza_isi"/>
        </Incidenza>
      </Intrattenimento>
    </Evento>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  abbonamenti            *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="//abbonamenti">
    <Abbonamento>
      ciao sono l'abbonamnento
    </Abbonamento>
  </xsl:template>
</xsl:stylesheet>