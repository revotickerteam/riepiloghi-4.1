<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="no" method="xml" encoding="US-ASCII" />
  <xsl:decimal-format name="it"  grouping-separator="." decimal-separator="," />
  <xsl:variable name="riepilogo_mensile" select="//riepilogo_mensile" />
  <xsl:variable name="titolare" select="//titolare" />
  <xsl:variable name="titoli_opere" select="//titoli_opere" />
  <xsl:variable name="ordini_di_posto" select="//ordini_di_posto" />
  <xsl:variable name="titoli_accesso" select="//titoli_accesso" />
  <xsl:variable name="titoli_annullati" select="//titoli_annullati" />
  <xsl:variable name="titoli_iva_preassolta" select="//titoli_iva_preassolta" />
  <xsl:variable name="titoli_iva_preassolta_annullati" select="//titoli_iva_preassolta_annullati" />
  <xsl:variable name="biglietti_abbonamento" select="//biglietti_abbonamento" />
  <xsl:variable name="biglietti_abbonamento_annullati" select="//biglietti_abbonamento_annullati" />
  <xsl:variable name="altri_proventi_evento" select="//altri_proventi_evento" />
  <xsl:variable name="abbonamenti" select="//abbonamenti" />
  <xsl:variable name="abbonamenti_emessi" select="//abbonamenti_emessi" />
  <xsl:variable name="abbonamenti_annullati" select="//abbonamenti_annullati" />
  <xsl:variable name="abbonamenti_iva_preassolta_emessi" select="//abbonamenti_iva_preassolta_emessi" />
  <xsl:variable name="abbonamenti_iva_preassolta_annullati" select="//abbonamenti_iva_preassolta_annullati" />
  <xsl:variable name="proventi_generici" select="//proventi_generici" />
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  main                   *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="/">
    <RiepilogoMensile>
      <xsl:attribute name="Sostituzione">
        <xsl:value-of select="'S'"/>
      </xsl:attribute>
      <xsl:attribute name="Mese">
        <xsl:value-of select="$riepilogo_mensile/data"/>
      </xsl:attribute>
      <xsl:attribute name="DataGenerazione">
        <xsl:value-of select="$riepilogo_mensile/data_generazione"/>
      </xsl:attribute>
      <xsl:attribute name="OraGenerazione">
        <xsl:value-of select="$riepilogo_mensile/ora_generazione"/>
      </xsl:attribute>
      <xsl:attribute name="ProgressivoGenerazione">
        <xsl:value-of select="$riepilogo_mensile/progressivo_generazione"/>
      </xsl:attribute>
      <Titolare>
        <Denominazione>
          <xsl:value-of select="$titolare/denominazione_titolare"/>
        </Denominazione>
        <CodiceFiscale>
          <xsl:value-of select="$titolare/codice_fiscale_titolare"/>
        </CodiceFiscale>
        <SistemaEmissione>
          <xsl:value-of select="$titolare/codice_sistema"/>
        </SistemaEmissione>
      </Titolare>
      <xsl:apply-templates select="//organizzatori" />
    </RiepilogoMensile>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  organizzatori          *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="//organizzatori">
    <Organizzatore>
      <Denominazione>
        <xsl:value-of select="denominazione_organizzatore"/>
      </Denominazione>
      <CodiceFiscale>
        <xsl:value-of select="codice_fiscale_organizzatore"/>
      </CodiceFiscale>
      <TipoOrganizzatore>
        <xsl:attribute name="valore">
          <xsl:value-of select="'G'"/>
        </xsl:attribute>
      </TipoOrganizzatore>
      <xsl:apply-templates select="eventi" />
      <xsl:apply-templates select="abbonamenti" />
      <xsl:apply-templates select="proventigenerici" />
    </Organizzatore>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  eventi                 *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="eventi">
    <Evento>
      <Intrattenimento>
        <TipoTassazione>
          <xsl:attribute name="valore">
            <xsl:value-of select="spettacolo_intrattenimento"/>
          </xsl:attribute>
        </TipoTassazione>
        <Incidenza>
          <xsl:value-of select="incidenza_isi"/>
        </Incidenza>
      </Intrattenimento>
      <Locale>
        <Denominazione>
          <xsl:value-of select="denominazione_locale"/>
        </Denominazione>
        <CodiceLocale>
          <xsl:value-of select="codice_locale"/>
        </CodiceLocale>
      </Locale>
      <DataEvento>
        <xsl:value-of select="data_evento"/>
      </DataEvento>
      <OraEvento>
        <xsl:value-of select="ora_evento"/>
      </OraEvento>
      <MultiGenere>
        <TipoGenere>
          <xsl:value-of select="tipo_evento"/>
        </TipoGenere>
        <IncidenzaGenere>
          <xsl:value-of select="100"/>
        </IncidenzaGenere>
        <xsl:apply-templates select="titoli_opere" />
      </MultiGenere>
      <xsl:apply-templates select="ordini_di_posto" />
    </Evento>
  </xsl:template>
  <xsl:template match="titoli_opere">
    <TitoliOpere>
      <Titolo>
        <xsl:value-of select="titolo"/>
      </Titolo>
      <ProduttoreCinema>
        <xsl:value-of select="produttore_cinema"/>
      </ProduttoreCinema>
      <Autore>
        <xsl:value-of select="autore"/>
      </Autore>
      <Esecutore>
        <xsl:value-of select="esecutore"/>
      </Esecutore>
      <Nazionalita>
        <xsl:value-of select="nazionalita"/>
      </Nazionalita>
      <Distributore>
        <xsl:value-of select="distributore"/>
      </Distributore>
    </TitoliOpere>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  ordini_di_posto        *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="ordini_di_posto">
    <OrdineDiPosto>
      <CodiceOrdine>
        <xsl:value-of select="ordine_di_posto"/>
      </CodiceOrdine>
      <Capienza>
        <xsl:value-of select="capienza"/>
      </Capienza>
      <IVAEccedenteOmaggi>
        <xsl:value-of select="iva_eccedente_omaggi"/>
      </IVAEccedenteOmaggi>
      <xsl:apply-templates select="titoli_accesso" />
      <xsl:apply-templates select="titoli_annullati" />
      <xsl:apply-templates select="titoli_iva_preassolta" />
      <xsl:apply-templates select="titoli_iva_preassolta_annullati" />
      <xsl:apply-templates select="biglietti_abbonamento" />
      <xsl:apply-templates select="biglietti_abbonamento_annullati" />
      <xsl:apply-templates select="altri_proventi_evento" />
    </OrdineDiPosto>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  titoli_accesso         *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="titoli_accesso">
    <TitoliAccesso>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_emessi"/>
      </Quantita>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_titolo"/>
      </CorrispettivoLordo>
      <Prevendita>
        <xsl:value-of select="corrispettivo_prevendita"/>
      </Prevendita>
      <IVACorrispettivo>
        <xsl:value-of select="iva_titolo"/>
      </IVACorrispettivo>
      <IVAPrevendita>
        <xsl:value-of select="iva_prevendita"/>
      </IVAPrevendita>
      <ImportoPrestazione>
        <xsl:value-of select="importo_prestazione"/>
      </ImportoPrestazione>
    </TitoliAccesso>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  titoli_annullati       *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="titoli_annullati">
    <TitoliAnnullati>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_annullati"/>
      </Quantita>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_titolo"/>
      </CorrispettivoLordo>
      <Prevendita>
        <xsl:value-of select="corrispettivo_prevendita"/>
      </Prevendita>
      <IVACorrispettivo>
        <xsl:value-of select="iva_titolo"/>
      </IVACorrispettivo>
      <IVAPrevendita>
        <xsl:value-of select="iva_prevendita"/>
      </IVAPrevendita>
      <ImportoPrestazione>
        <xsl:value-of select="importo_prestazione"/>
      </ImportoPrestazione>
    </TitoliAnnullati>
  </xsl:template>
  <!--  *                                                ***************************************   -->
  <!--  * ***********************************************  titoli_iva_preassolta               *   -->
  <!--  *                                                ***************************************   -->
  <xsl:template match="titoli_iva_preassolta">
    <TitoliIVAPreassolta>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_emessi"/>
      </Quantita>
      <ImportoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </ImportoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </TitoliIVAPreassolta>
  </xsl:template>
  <!--  *                                                ***************************************   -->
  <!--  * ***********************************************  titoli_iva_preassolta_annullati     *   -->
  <!--  *                                                ***************************************   -->
  <xsl:template match="titoli_iva_preassolta_annullati">
    <TitoliIVAPreassoltaAnnullati>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_annullati"/>
      </Quantita>
      <ImportoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </ImportoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </TitoliIVAPreassoltaAnnullati>
  </xsl:template>
  <!--  *                                                ***************************************   -->
  <!--  * ***********************************************  biglietti_abbonamento               *   -->
  <!--  *                                                ***************************************   -->
  <xsl:template match="biglietti_abbonamento">
    <BigliettiAbbonamento>
      <CodiceFiscale>
        <xsl:value-of select="codice_fiscale_abbonamento"/>
      </CodiceFiscale>
      <CodiceAbbonamento>
        <xsl:value-of select="codice_biglietto_abbonamento"/>
      </CodiceAbbonamento>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_emessi"/>
      </Quantita>
      <ImportoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </ImportoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </BigliettiAbbonamento>
  </xsl:template>
  <!--  *                                                ***************************************   -->
  <!--  * ***********************************************  biglietti_abbonamento_annullati     *   -->
  <!--  *                                                ***************************************   -->
  <xsl:template match="biglietti_abbonamento_annullati">
    <BigliettiAbbonamentoAnnullati>
      <CodiceFiscale>
        <xsl:value-of select="codice_fiscale_abbonamento"/>
      </CodiceFiscale>
      <CodiceAbbonamento>
        <xsl:value-of select="codice_biglietto_abbonamento"/>
      </CodiceAbbonamento>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <Quantita>
        <xsl:value-of select="qta_annullati"/>
      </Quantita>
      <ImportoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </ImportoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </BigliettiAbbonamentoAnnullati>
  </xsl:template>

  <!--  *                                                ***************************************   -->
  <!--  * ***********************************************  altri_proventi_evento               *   -->
  <!--  *                                                ***************************************   -->
  <xsl:template match="altri_proventi_evento">
    <AltriProventiEvento>
      <TipoProvento>
        <xsl:value-of select="tipo_provento"/>
      </TipoProvento>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_lordo"/>
      </CorrispettivoLordo>
      <IVACorrispettivo>
        <xsl:value-of select="iva_corrispettivo"/>
      </IVACorrispettivo>
    </AltriProventiEvento>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  abbonamenti            *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="abbonamenti">
    <Abbonamenti>
      <CodiceAbbonamento>
        <xsl:value-of select="codice_abbonamento"/>
      </CodiceAbbonamento>
      <Validita>
        <xsl:value-of select="validita"/>
      </Validita>
      <TipoTassazione>
        <xsl:attribute name="valore">
          <xsl:value-of select="spettacolo_intrattenimento"/>
        </xsl:attribute>
      </TipoTassazione>
      <Turno>
        <xsl:attribute name="valore">
          <xsl:value-of select="turno"/>
        </xsl:attribute>
      </Turno>
      <CodiceOrdine>PT</CodiceOrdine>
      <TipoTitolo>
        <xsl:value-of select="tipo_titolo"/>
      </TipoTitolo>
      <QuantitaEventiAbilitati>
        <xsl:value-of select="numero_eventi_abilitati"/>
      </QuantitaEventiAbilitati>
      <xsl:apply-templates select="abbonamenti_emessi" />
      <xsl:apply-templates select="abbonamenti_annullati" />
      <xsl:apply-templates select="abbonamenti_iva_preassolta" />
      <xsl:apply-templates select="abbonamenti_iva_preassolta_annullati" />
    </Abbonamenti>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  abbonamenti_emessi     *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="abbonamenti_emessi">
    <AbbonamentiEmessi>
      <Quantita>
        <xsl:value-of select="qta_emessi"/>
      </Quantita>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_titolo"/>
      </CorrispettivoLordo>
      <Prevendita>
        <xsl:value-of select="corrispettivo_prevendita"/>
      </Prevendita>
      <IVACorrispettivo>
        <xsl:value-of select="iva_titolo"/>
      </IVACorrispettivo>
      <IVAPrevendita>
        <xsl:value-of select="iva_prevendita"/>
      </IVAPrevendita>
    </AbbonamentiEmessi>
  </xsl:template>
  <!--  *                                                **************************   -->
  <!--  * ***********************************************  abbonamenti_annullati  *   -->
  <!--  *                                                **************************   -->
  <xsl:template match="abbonamenti_annullati">
    <AbbonamentiAnnullati>
      <Quantita>
        <xsl:value-of select="qta_annullati"/>
      </Quantita>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_titolo"/>
      </CorrispettivoLordo>
      <Prevendita>
        <xsl:value-of select="corrispettivo_prevendita"/>
      </Prevendita>
      <IVACorrispettivo>
        <xsl:value-of select="iva_titolo"/>
      </IVACorrispettivo>
      <IVAPrevendita>
        <xsl:value-of select="iva_prevendita"/>
      </IVAPrevendita>
    </AbbonamentiAnnullati>
  </xsl:template>
  <!--  *                                                **********************************   -->
  <!--  * ***********************************************  abbonamenti_iva_preassolta     *   -->
  <!--  *                                                **********************************   -->
  <xsl:template match="abbonamenti_iva_preassolta">
    <AbbonamentiIVAPreassolta>
      <Quantita>
        <xsl:value-of select="qta_emessi"/>
      </Quantita>
      <CorrispettivoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </CorrispettivoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </AbbonamentiIVAPreassolta>
  </xsl:template>
  <!--  *                                                *******************************   -->
  <!--  * ***********************************************  abbonamenti_iva_preassolta  *   -->
  <!--  *                                                *******************************   -->
  <xsl:template match="abbonamenti_iva_preassolta_annullati">
    <AbbonamentiIVAPreassoltaAnnullati>
      <Quantita>
        <xsl:value-of select="qta_annullati"/>
      </Quantita>
      <CorrispettivoFigurativo>
        <xsl:value-of select="corrispettivo_figurativo"/>
      </CorrispettivoFigurativo>
      <IVAFigurativa>
        <xsl:value-of select="iva_figurativa"/>
      </IVAFigurativa>
    </AbbonamentiIVAPreassoltaAnnullati>
  </xsl:template>
  <!--  *                                                *******************************   -->
  <!--  * ***********************************************  proventi_generici           *   -->
  <!--  *                                                *******************************   -->
  <xsl:template match="proventi_generici">
    <AltriProventiGenerici>
      <TipoProvento>
        <xsl:value-of select="tipo_provento"/>
      </TipoProvento>
      <CorrispettivoLordo>
        <xsl:value-of select="corrispettivo_lordo"/>
      </CorrispettivoLordo>
      <IVACorrispettivo>
        <xsl:value-of select="iva_corrispettivo"/>
      </IVACorrispettivo>
      <GenerePrevalente>
        <xsl:value-of select="tipo_provento"/>
      </GenerePrevalente>
    </AltriProventiGenerici>
  </xsl:template>
</xsl:stylesheet>