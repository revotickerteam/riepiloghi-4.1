﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Wintic.Data;
using Riepiloghi;

namespace WinServiceRiepiloghi
{
    public partial class WindowsServiceRiepiloghi : ServiceBase
    {

        //private string DatabaseServiceName = "SERVERX";
        private Riepiloghi.SocketServerRiepiloghi SocketServerRiepiloghi;
        private clsWinServiceServerPollingDbOperation DatabaseServerRiepiloghi;
        public clsWinServiceConfig Configuration { get; set; }

        public WindowsServiceRiepiloghi()
        {
            InitializeComponent();
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;
            SetLogEnabled(false);
        }

        protected override void OnStart(string[] args)
        {
            WriteLog("OnStart");
            base.OnStart(args);
            this.ExecuteStart();
        }

        protected override void OnStop()
        {
            WriteLog("OnStop");
            base.OnStop();
            this.ExecuteStop();
        }

        protected override void OnContinue()
        {
            WriteLog("OnContinue");
            base.OnContinue();
            this.ExecuteStart();
        }

        protected override void OnPause()
        {
            WriteLog("OnPause");
            base.OnPause();
            this.ExecuteStop();
        }

        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            WriteLog("OnPowerEvent");
            return base.OnPowerEvent(powerStatus);
        }

        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            WriteLog("OnSessionChange");
            base.OnSessionChange(changeDescription);
        }

        protected override void OnShutdown()
        {
            WriteLog("OnShutdown");
            base.OnShutdown();
            this.ExecuteStop();
        }

        protected override void OnCustomCommand(int command)
        {
            WriteLog(string.Format("OnCustomCommand {0}", command.ToString()));
            base.OnCustomCommand(command);
        }

        #region "OPERAZIONI REALI"

        private Wintic.Data.IConnection GetConnection()
        {
            Wintic.Data.IConnection oConnection = new Wintic.Data.oracle.CConnectionOracle();
            string DatabaseServiceName = Properties.Settings.Default.internalConnectionString;
            WriteLog(string.Format("GetConnection {0}", DatabaseServiceName));

            if (DatabaseServiceName.Contains("/") && DatabaseServiceName.Contains("@"))
            {
                oConnection.ConnectionString = string.Format("user={0};password={1};data source={2};", DatabaseServiceName.Split('@')[0].Split('/')[0], DatabaseServiceName.Split('@')[0].Split('/')[1], DatabaseServiceName.Split('@')[1]);
            }
            else
                oConnection.ConnectionString = string.Format("user=wtic;password=obelix;data source={0};", DatabaseServiceName);
            try
            {
                oConnection.Open();
            }
            catch (Exception ex)
            {
                WriteLog(string.Format("GetConnection {0} ERROR", DatabaseServiceName), ex);
            }
            return oConnection;
        }


        #region Gestione log

        private class clsInternalLog
        {
            public string message { get; set; }
            public Exception error { get; set; }
        }
        private System.Timers.Timer oTimerLog = null;
        private System.Collections.Generic.Queue<clsInternalLog> logs = null;
        private System.Collections.Generic.Queue<clsInternalLog> logsRefused = null;
        private bool logEnabled = false;
        private string appPathLog = "";

        private void WriteLog(string message)
        {
            WriteLog(message, null);
        }

        private void WriteLog(string message, Exception ex)
        {
            if (logEnabled)
            {
                clsInternalLog oLogItem = new clsInternalLog();
                oLogItem.message = message;
                oLogItem.error = ex;
                lock (logs)
                {
                    logs.Enqueue(oLogItem);
                }
                
            }
        }

        private void WriteLogRefused(string message)
        {
            if (logEnabled)
            {
                clsInternalLog oLogItem = new clsInternalLog();
                oLogItem.message = message;
                oLogItem.error = null;
                lock (logsRefused)
                {
                    logsRefused.Enqueue(oLogItem);
                }

            }
        }

        private void OTimerLog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Internal_WriteLog();
        }
        private void Internal_WriteLog()
        {
            Internal_WriteLog(false);
        }
        private void Internal_WriteLog(bool writeAllLog)
        {
            lock (logs)
            {
                DateTime dNow = DateTime.Now.Date;
                if (string.IsNullOrEmpty(appPathLog))
                {
                    appPathLog = (new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)).Directory.FullName + @"\logs";
                    if (!System.IO.Directory.Exists(appPathLog)) System.IO.Directory.CreateDirectory(appPathLog);
                }

                System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(appPathLog);
                if (oDirInfo.Exists)
                {
                    try
                    {
                        foreach (System.IO.FileInfo oFileInfo in oDirInfo.GetFiles("WinServiceRiepiloghiLog_*.log", System.IO.SearchOption.TopDirectoryOnly))
                        {
                            if ((dNow - oFileInfo.LastWriteTime).TotalDays > 10)
                            {
                                oFileInfo.Delete();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    int countWrite = 0;
                    string file = string.Format("WinServiceRiepiloghiLog_{0}_{1}_{2}.log", dNow.Year.ToString("0000"), dNow.Month.ToString("00"), dNow.Month.ToString("00"));
                    while (logs.Count > 0 && (writeAllLog || countWrite < 10))
                    {
                        if (DateTime.Now.Date != dNow.Date)
                        {
                            dNow = DateTime.Now.Date;
                            file = string.Format("WinServiceRiepiloghiLog_{0}_{1}_{2}.log", dNow.Year.ToString("0000"), dNow.Month.ToString("00"), dNow.Month.ToString("00"));
                        }

                        countWrite++;
                        clsInternalLog oItemLog = logs.Dequeue();
                        System.IO.File.AppendAllText(appPathLog + @"\" + file, oItemLog.message + "\r\n");
                        if (oItemLog.error != null)
                        {
                            System.IO.File.AppendAllText(appPathLog + @"\" + file, oItemLog.error.ToString() + "\r\n");
                        }
                    }
                }
                else
                {
                    while (logs.Count > 0)
                    {
                        logs.Dequeue();
                    }
                }
                
            }

            lock (logsRefused)
            {
                DateTime dNow = DateTime.Now.Date;
                if (string.IsNullOrEmpty(appPathLog))
                {
                    appPathLog = (new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)).Directory.FullName + @"\logs";
                    if (!System.IO.Directory.Exists(appPathLog)) System.IO.Directory.CreateDirectory(appPathLog);
                }

                System.IO.DirectoryInfo oDirInfo = new System.IO.DirectoryInfo(appPathLog);
                if (oDirInfo.Exists)
                {
                    try
                    {
                        foreach (System.IO.FileInfo oFileInfo in oDirInfo.GetFiles("WinServiceRiepiloghiLog_REFUSE*.log", System.IO.SearchOption.TopDirectoryOnly))
                        {
                            if ((dNow - oFileInfo.LastWriteTime).TotalDays > 10)
                            {
                                oFileInfo.Delete();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    int countWrite = 0;
                    string file = string.Format("WinServiceRiepiloghiLog_REFUSE_{0}_{1}_{2}.log", dNow.Year.ToString("0000"), dNow.Month.ToString("00"), dNow.Month.ToString("00"));
                    while (logsRefused.Count > 0 && (writeAllLog || countWrite < 10))
                    {
                        if (DateTime.Now.Date != dNow.Date)
                        {
                            dNow = DateTime.Now.Date;
                            file = string.Format("WinServiceRiepiloghiLog_REFUSE_{0}_{1}_{2}.log", dNow.Year.ToString("0000"), dNow.Month.ToString("00"), dNow.Month.ToString("00"));
                        }

                        countWrite++;
                        clsInternalLog oItemLog = logsRefused.Dequeue();
                        System.IO.File.AppendAllText(appPathLog + @"\" + file, oItemLog.message + "\r\n");
                        if (oItemLog.error != null)
                        {
                            System.IO.File.AppendAllText(appPathLog + @"\" + file, oItemLog.error.ToString() + "\r\n");
                        }
                    }
                }
                else
                {
                    while (logsRefused.Count > 0)
                    {
                        logsRefused.Dequeue();
                    }
                }

            }
        }

        private void FinalizeLogs()
        {
            SetLogEnabled(false);
            Internal_WriteLog(true);
        }

        #endregion

        public void ExecuteStart()
        {
            bool lRet = false;
            string DatabaseServiceName = Properties.Settings.Default.internalConnectionString;

            WriteLog("START...");

            string MachineName = System.Environment.MachineName;
            //clsWinServiceConfig config = null;
            string cWinServiceLog = "";

            WriteLog("START MachineName " + MachineName);

            WriteLog("START GetConnection...");
            IConnection Connection = GetConnection();
            try
            {
                lRet = Connection != null;
                if (lRet)
                {
                    lRet = Connection.State == ConnectionState.Open;
                    WriteLog(string.Format("START Connesione {0}", (lRet ? "APERTA" : "NON APERTA")));
                    if (!lRet)
                        WriteLog("START GetConnection Fallito");
                }
                else
                    WriteLog("START GetConnection Fallito");
            }
            catch (Exception exConnection)
            {
                lRet = false;
                WriteLog("START Errore connessione", exConnection);
            }

            // Lettura configurazione
            if (lRet)
            {
                Exception oError = null;
                cWinServiceLog = Riepiloghi.clsRiepiloghi.GetProprietaRiepiloghi(Connection, "WINSERVICE_LOG", "DISABILITATO");
                this.SetLogEnabled(cWinServiceLog == "ABILITATO");

                this.Configuration = clsWinServiceConfig.GetWinServiceConfig(Connection, out oError, true, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WIN_SERVICE);

                lRet = oError == null;

            }

            // Eventuale chiusura connessione se aperta
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                WriteLog("START Chiusura connessione");
                try
                {
                    Connection.Close();
                    Connection.Dispose();
                }
                catch (Exception)
                {
                }
            }


            // Avvio in modalità SOCKET
            if (lRet && this.Configuration.ComunicationMode == clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_SOCKET)
            {
                try
                {
                    WriteLog("START Ricerca IP...");
                    if (this.Configuration.ListenerIP.Trim() == "127.0.0.1")
                    {

                    }
                    else
                    {
                        lRet = false;
                        IPAddress[] aryLocalAddr = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
                        foreach (IPAddress oIP in Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork))
                        {
                            WriteLog(string.Format("START Ricerca IP {0}", oIP.ToString()));
                            if (oIP.ToString() == this.Configuration.ListenerIP)
                            {
                                lRet = true;
                                break;
                            }
                        }
                    }

                    if (lRet)
                    {
                        
                    }

                    if (lRet)
                    {
                        WriteLog("START Avvio Listener...");
                        this.SocketServerRiepiloghi = new SocketServerRiepiloghi(this.Configuration.ListenerIP, this.Configuration.ListenerPORT, DatabaseServiceName, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WIN_SERVICE);
                        this.StartCaptureEventsSockets();
                        lRet = this.SocketServerRiepiloghi.StartListen();
                        if (lRet)
                        {
                            WriteLog("START Listener avviato");
                        }
                        else
                        {
                            WriteLog("START Listener NON avviato");
                        }
                    }
                    else
                    {
                        WriteLog("START Listener NON avviato");
                    }
                }
                catch (Exception exSocket)
                {
                    lRet = false;
                    WriteLog("START ERRORE", exSocket);
                }
            }
            else if (lRet && this.Configuration.ComunicationMode == clsWinServiceConfig.WinServiceCOMUNICATION_TYPE_VIA_DB)  // Avvio in modalità DATABASE
            {
                this.DatabaseServerRiepiloghi = new clsWinServiceServerPollingDbOperation(DatabaseServiceName, this.Configuration.PollingTimeSpan, Riepiloghi.clsWinServiceConfig.WinServiceAPPLICATION_TYPE_WIN_SERVICE);
                this.StartCaptureEventDatabase();
                this.DatabaseServerRiepiloghi.StartPolling();
            }
            
            if (!lRet)
            {
                WriteLog("START richiesta di stop");
                this.OnStop();
            }
            else
            {
                WriteLog("START FINE");
            }
        }

        private void SocketServerRiepiloghi_EventServerSocketRiepiloghi_In(clsWinServiceModel_request_Operation request)
        {
            WriteLog(string.Format("IN {0} {1}", request.Code, request.Message));
        }

        private void SocketServerRiepiloghi_EventServerSocketRiepiloghi_Out(clsWinServiceModel_response_Operation response)
        {
            WriteLog(string.Format("OUT {0} {1} {2}", response.Code, response.Message, response.ErrorCode));
        }

        private void SetLogEnabled(bool value)
        {
            if (!value)
                logEnabled = value;
            try
            {
                if (oTimerLog != null)
                {
                    oTimerLog.Elapsed -= OTimerLog_Elapsed;
                    oTimerLog.Stop();
                    oTimerLog.Dispose();
                    oTimerLog = null;
                }
            }
            catch (Exception)
            {
            }
            logs = new Queue<clsInternalLog>();
            logsRefused = new Queue<clsInternalLog>();
            if (value)
            {
                oTimerLog = new System.Timers.Timer(1000);
                oTimerLog.Elapsed += OTimerLog_Elapsed;
                logEnabled = true;
                oTimerLog.Start();
            }
        }

        private void UpdateProprietaRiepiloghiLog()
        {
            IConnection Connection = GetConnection();
            try
            {
                
                if (Connection != null && Connection.State == ConnectionState.Open)
                {
                    Exception errSaveProprieta = null;
                    Riepiloghi.clsRiepiloghi.SetProprietaRiepiloghi(Connection, "WINSERVICE_LOG", (logEnabled ? "ABILITATO" : "DISABILITATO"), out errSaveProprieta);
                }
            }
            catch (Exception)
            {
            }
            try
            {
                if (Connection != null)
                {
                    Connection.Close();
                    Connection.Dispose();
                    Connection = null;
                }
            }
            catch (Exception)
            {
            }
        }

        private void Updater_OutMessage(string message)
        {
            WriteLog(string.Format("Updater_OutMessage {0}", message));
        }

        private void CheckForUpdates()
        {
            Exception error = null;
            clsWinServiceModel_response_Operation installerOperation = new clsWinServiceModel_response_Operation();
            try
            {
                IConnection Connection = GetConnection();
                installerOperation.Code = clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES;
                installerOperation.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_OK;
                installerOperation.Message = "Avvio verifica aggiornamenti";
                if (this.SocketServerRiepiloghi != null)
                    this.SocketServerRiepiloghi.SendToAllClients(installerOperation);

                installerOperation.Message = "Apertura della connessione";
                if (this.SocketServerRiepiloghi != null)
                    this.SocketServerRiepiloghi.SendToAllClients(installerOperation);
                bool lRet = false;
                try
                {
                    lRet = Connection != null;
                    if (lRet)
                    {
                        lRet = Connection.State == ConnectionState.Open;
                    }
                }
                catch (Exception exConnection)
                {
                    lRet = false;
                    installerOperation.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_ERR_GENERIC;
                    installerOperation.Message = string.Format("Errore di connessione {0}", exConnection.Message);
                    if (this.SocketServerRiepiloghi != null)
                        this.SocketServerRiepiloghi.SendToAllClients(installerOperation);
                }

                if (Connection != null && Connection.State == ConnectionState.Open)
                {
                    installerOperation.Message = "Connessione avvenuta";
                    if (this.SocketServerRiepiloghi != null)
                        this.SocketServerRiepiloghi.SendToAllClients(installerOperation);
                    bool lUpdates = false;

                    //try
                    //{
                    //    libUpdater.Updater.OutMessage += Updater_OutMessage;
                    //    lUpdates = libUpdater.Updater.CheckForUpdatesAndUpdateRunningWinService(Connection, "", "stopWinServiceRiepiloghi.bat", "removeWinServiceRiepiloghi.bat", "installWinServiceRiepiloghi.bat", "restartWinServiceRiepiloghi.bat");
                    //    libUpdater.Updater.OutMessage -= Updater_OutMessage;
                    //}
                    //catch (Exception)
                    //{
                    //}

                    installerOperation.Message = "Chiusura della connessione";
                    if (this.SocketServerRiepiloghi != null)
                        this.SocketServerRiepiloghi.SendToAllClients(installerOperation);

                    try
                    {
                        Connection.Close();
                        Connection.Dispose();
                    }
                    catch (Exception)
                    {
                    }

                    if (lUpdates)
                    {
                        installerOperation.Message = "Stop servizio per aggiornamenti";
                        if (this.SocketServerRiepiloghi != null)
                            this.SocketServerRiepiloghi.SendToAllClients(installerOperation);

                        this.OnStop();
                    }
                    else
                    {
                        installerOperation.Message = "Nessun aggiornamento disponibile";
                        if (this.SocketServerRiepiloghi != null)
                            this.SocketServerRiepiloghi.SendToAllClients(installerOperation);
                    }


                }

            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null && this.SocketServerRiepiloghi != null)
            {
                installerOperation.Code = clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES;
                installerOperation.ErrorCode = clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_ERR_GENERIC;
                installerOperation.Message = error.Message;
                this.SocketServerRiepiloghi.SendToAllClients(installerOperation);
            }
        }

        private void ExecuteStop()
        {
            WriteLog("ExecuteStop");
            try
            {
                this.StopCaptureEventDatabase();
                if (this.DatabaseServerRiepiloghi != null)
                {
                    this.DatabaseServerRiepiloghi.StopPolling();
                }
                this.StopCaptureEventsSockets();
                if (this.SocketServerRiepiloghi != null && this.SocketServerRiepiloghi.Listening)
                {
                    WriteLog("ExecuteStop StopListener");
                    this.SocketServerRiepiloghi.StopListener();
                }
                this.FinalizeLogs();
            }
            catch (Exception)
            {
            }
            WriteLog("ExecuteStop END");
        }

        #region "Gestione comunicazione SOCKET"

        private void StartCaptureEventsSockets()
        {
            if (this.SocketServerRiepiloghi != null)
            {
                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_In += SocketServerRiepiloghi_EventServerSocketRiepiloghi_In;
                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_Out += SocketServerRiepiloghi_EventServerSocketRiepiloghi_Out;
                this.SocketServerRiepiloghi.OnClientConnected += SocketServerRiepiloghi_OnClientConnected;
                this.SocketServerRiepiloghi.OnClientClosed += SocketServerRiepiloghi_OnClientClosed;
                this.SocketServerRiepiloghi.OnClientRefused += SocketServerRiepiloghi_OnClientRefused;

                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_Execute += SocketServerRiepiloghi_EventServerSocketRiepiloghi_Execute;
            }
        }

        private void SocketServerRiepiloghi_EventServerSocketRiepiloghi_Execute(clsWinServiceModel_request_Operation request, out Exception error)
        {
            error = null;
            this.ExecuteRequest(request, out error);
        }

        private string GetClientDescription(Fleck.IWebSocketConnection oClient)
        {
            string cDescClient = "";
            try
            {
                
                if (oClient != null && oClient.ConnectionInfo != null)
                {
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.ClientIpAddress))
                    {
                        cDescClient += " " + string.Format("IPAddress {0}", oClient.ConnectionInfo.ClientIpAddress);
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.ClientPort.ToString()))
                    {
                        cDescClient += " " + string.Format("Port {0}", oClient.ConnectionInfo.ClientPort.ToString());
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.Host))
                    {
                        cDescClient += " " + string.Format("Host {0}", oClient.ConnectionInfo.Host);
                    }
                    if (oClient.ConnectionInfo.Id != null && !string.IsNullOrEmpty(oClient.ConnectionInfo.Id.ToString()))
                    {
                        cDescClient += " " + string.Format("Id {0}", oClient.ConnectionInfo.Id.ToString());
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.NegotiatedSubProtocol))
                    {
                        cDescClient += " " + string.Format("NegotiatedSubProtocol {0}", oClient.ConnectionInfo.NegotiatedSubProtocol);
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.Origin))
                    {
                        cDescClient += " " + string.Format("Origin {0}", oClient.ConnectionInfo.Origin);
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.Path))
                    {
                        cDescClient += " " + string.Format("Path {0}", oClient.ConnectionInfo.Path);
                    }
                    if (!string.IsNullOrEmpty(oClient.ConnectionInfo.SubProtocol))
                    {
                        cDescClient += " " + string.Format("SubProtocol {0}", oClient.ConnectionInfo.SubProtocol);
                    }
                }
            }
            catch (Exception)
            {
            }
            return cDescClient;
        }

        private void SocketServerRiepiloghi_OnClientConnected(Fleck.IWebSocketConnection oClient)
        {
            string cDescClient = GetClientDescription(oClient);
            this.WriteLog(string.Format("CONN {0}", cDescClient));
        }

        private void SocketServerRiepiloghi_OnClientClosed(Fleck.IWebSocketConnection oClient)
        {
            string cDescClient = GetClientDescription(oClient);
            this.WriteLog(string.Format("DISC {0}", cDescClient));
        }

        private void SocketServerRiepiloghi_OnClientRefused(string message, Fleck.IWebSocketConnection oClient)
        {
            string cDescClient = GetClientDescription(oClient);
            if (!string.IsNullOrEmpty(message))
            {
                this.WriteLogRefused(string.Format("refused {0} messaggio {1}", cDescClient, message));
            }
            else
            {
                this.WriteLogRefused(string.Format("refused {0} messaggio NULLO", cDescClient));
            }
            
        }

        private void StopCaptureEventsSockets()
        {
            if (this.SocketServerRiepiloghi != null)
            {
                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_In -= SocketServerRiepiloghi_EventServerSocketRiepiloghi_In;
                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_Out -= SocketServerRiepiloghi_EventServerSocketRiepiloghi_Out;
                this.SocketServerRiepiloghi.OnClientConnected -= SocketServerRiepiloghi_OnClientConnected;
                this.SocketServerRiepiloghi.OnClientClosed -= SocketServerRiepiloghi_OnClientClosed;
                this.SocketServerRiepiloghi.OnClientRefused -= SocketServerRiepiloghi_OnClientRefused;

                this.SocketServerRiepiloghi.EventServerSocketRiepiloghi_Execute -= SocketServerRiepiloghi_EventServerSocketRiepiloghi_Execute;
            }
        }

        #endregion

        #region "Gestione comunicazione DATABASE"

        private void StartCaptureEventDatabase()
        {
            if (this.DatabaseServerRiepiloghi != null)
            {
                this.DatabaseServerRiepiloghi.EventServerDatabaseRiepiloghi_Execute += DatabaseServerRiepiloghi_EventServerDatabaseRiepiloghi_Execute;
                this.DatabaseServerRiepiloghi.Output += DatabaseServerRiepiloghi_Output;
            }
        }

        private void DatabaseServerRiepiloghi_Output(string message, Exception error)
        {
            this.WriteLog(message, error);
        }

        private void DatabaseServerRiepiloghi_EventServerDatabaseRiepiloghi_Execute(clsWinServiceModel_request_Operation request, out Exception error)
        {
            error = null;
            this.ExecuteRequest(request, out error);
        }

        private void StopCaptureEventDatabase()
        {
            if (this.DatabaseServerRiepiloghi != null)
            {
                this.DatabaseServerRiepiloghi.EventServerDatabaseRiepiloghi_Execute -= DatabaseServerRiepiloghi_EventServerDatabaseRiepiloghi_Execute;
                this.DatabaseServerRiepiloghi.Output -= DatabaseServerRiepiloghi_Output;
            }
        }

        #endregion

        #region "Elaborazione richieste indipendentemente dal metodo di comunicazione"

        public class clsEnvironment
        {
            public string InternalConnectionString { get; set; }
            public string ExternalConnectionString { get; set; }
            public string PathRisorseWeb { get; set; }
            public string PathFont { get; set; }
            public string PathTemp { get; set; }
            public string PathWriteRiepiloghi { get; set; }
        }

        public static clsEnvironment GetEnvironment()
        {
            System.IO.FileInfo fInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().FullName);
            var result = new clsEnvironment()
            {
                InternalConnectionString = Properties.Settings.Default.internalConnectionString,
                ExternalConnectionString = "",
                PathFont = Properties.Settings.Default.pathFont,
                PathRisorseWeb = Properties.Settings.Default.pathRisorseWeb,
                PathTemp = fInfo.Directory.FullName,
                PathWriteRiepiloghi = Properties.Settings.Default.pathWrite
            };
            return result;
        }

        private void ExecuteRequest(clsWinServiceModel_request_Operation request, out Exception Error)
        {
            clsWinServiceModel_Token token = null;
            Error = null;

            try
            {
                token = Newtonsoft.Json.JsonConvert.DeserializeObject<clsWinServiceModel_Token>(clsSecuritySocket.Decrypt(request.Token));
            }
            catch (Exception)
            {
            }

            if (token != null)
            {

                // EFFETTIVA ESECUZIONE DELLE RICHIESTE 
                try
                {
                    if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RIEPILOGO)
                    {
                        string DatabaseServiceName = Properties.Settings.Default.internalConnectionString;
                        clsWinServiceServerOperation oServer = new clsWinServiceServerOperation(DatabaseServiceName);

                        string pathRisorse = (new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)).Directory.FullName + @"\Risorse";

                        Riepiloghi.clsRiepilogoGenerato dettaglioRiepilogo = null;
                        clsEnvironment oEnvironment = GetEnvironment();
                        Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi percorsi = new Riepiloghi.clsRiepiloghi.clsPercorsiRiepiloghi();
                        percorsi.Risorse = oEnvironment.PathRisorseWeb;
                        percorsi.Temp = oEnvironment.PathTemp;
                        percorsi.Destinazione = oEnvironment.PathWriteRiepiloghi;
                        oServer.ElaboraRichiestaRiepilogo(request, token, percorsi, out Error, out dettaglioRiepilogo);
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_SEND_EMAIL)
                    {
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_RECEIVE_EMAIL)
                    {
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_CHECKUPDATES)
                    {
                        this.CheckForUpdates();
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_RESPONSE_STOP_SERVER)
                    {
                        this.OnStop();
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_ENABLED_LOGS)
                    {
                        this.SetLogEnabled(true);
                        UpdateProprietaRiepiloghiLog();
                    }
                    else if (request.Code == clsWinServiceModel_request_Operation.CODE_OP_REQUEST_DISABLED_LOGS)
                    {
                        this.SetLogEnabled(false);
                        UpdateProprietaRiepiloghiLog();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion  

        #endregion
    }
}
