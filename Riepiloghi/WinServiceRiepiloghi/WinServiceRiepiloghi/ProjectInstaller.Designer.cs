﻿namespace WinServiceRiepiloghi
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerWinServiceRiepiloghi = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerWinServiceRiepiloghi = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerWinServiceRiepiloghi
            // 
            this.serviceProcessInstallerWinServiceRiepiloghi.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerWinServiceRiepiloghi.Password = null;
            this.serviceProcessInstallerWinServiceRiepiloghi.Username = null;
            // 
            // serviceInstallerWinServiceRiepiloghi
            // 
            this.serviceInstallerWinServiceRiepiloghi.ServiceName = "WinServiceRiepiloghi";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerWinServiceRiepiloghi,
            this.serviceInstallerWinServiceRiepiloghi});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerWinServiceRiepiloghi;
        private System.ServiceProcess.ServiceInstaller serviceInstallerWinServiceRiepiloghi;
    }
}