﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfigRiepiloghi;

namespace ConfigRiepiloghiMulti
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        public string Login => this.txtLogin.Text;
        public string Password => this.txtPassword.Text;
    }
}
