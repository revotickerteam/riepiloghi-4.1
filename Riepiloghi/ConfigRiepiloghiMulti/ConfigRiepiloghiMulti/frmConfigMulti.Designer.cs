﻿
namespace ConfigRiepiloghiMulti
{
    partial class frmConfigMulti
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControlALL = new System.Windows.Forms.TabControl();
            this.tabACCOUNT = new System.Windows.Forms.TabPage();
            this.pnlDescProfilo = new System.Windows.Forms.Panel();
            this.txtDescProfilo = new System.Windows.Forms.TextBox();
            this.lblDescProfilo = new ConfigRiepiloghi.clsLabel();
            this.pnlNome = new System.Windows.Forms.Panel();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new ConfigRiepiloghi.clsLabel();
            this.pnlCognome = new System.Windows.Forms.Panel();
            this.txtCognome = new System.Windows.Forms.TextBox();
            this.lblCognome = new ConfigRiepiloghi.clsLabel();
            this.pnlEmail = new System.Windows.Forms.Panel();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new ConfigRiepiloghi.clsLabel();
            this.tabRIEPILOGHI_CONFIG = new System.Windows.Forms.TabPage();
            this.tabSUB_ACCOUNTS = new System.Windows.Forms.TabPage();
            this.pnlACCOUNTS = new System.Windows.Forms.Panel();
            this.pgrACCOUNTS = new System.Windows.Forms.PropertyGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlACCOUNTSbuttons = new System.Windows.Forms.Panel();
            this.btnACCOUNTSrenew = new ConfigRiepiloghi.clsButton();
            this.btnACCOUNTSadd = new ConfigRiepiloghi.clsButton();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.dgvACCOUNTS = new System.Windows.Forms.DataGridView();
            this.tabPROFILES = new System.Windows.Forms.TabPage();
            this.pnlPROFILES = new System.Windows.Forms.Panel();
            this.pgrPROFILES = new System.Windows.Forms.PropertyGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnPROFILESdel = new ConfigRiepiloghi.clsButton();
            this.btnPROFILESadd = new ConfigRiepiloghi.clsButton();
            this.pnlPROFILESbot = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dgvPROFILES = new System.Windows.Forms.DataGridView();
            this.tabCODICI_SISTEMA = new System.Windows.Forms.TabPage();
            this.pnlCODICI_SISTEMA = new System.Windows.Forms.Panel();
            this.pgrCODICI_SISTEMA = new System.Windows.Forms.PropertyGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.dgvCODICI_SISTEMA = new System.Windows.Forms.DataGridView();
            this.tabCODICI_LOCALI = new System.Windows.Forms.TabPage();
            this.pnlCODICI_LOCALI = new System.Windows.Forms.Panel();
            this.pgrCODICI_LOCALI = new System.Windows.Forms.PropertyGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.dgvCODICI_LOCALI = new System.Windows.Forms.DataGridView();
            this.tabORGANIZZATORI = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pgrCF_ORGANIZZATORI = new System.Windows.Forms.PropertyGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.dgvCF_ORGANIZZATORI = new System.Windows.Forms.DataGridView();
            this.tabCONFIG = new System.Windows.Forms.TabPage();
            this.pnlCONFIG = new System.Windows.Forms.Panel();
            this.pnlPROFILO_SELECTED = new System.Windows.Forms.Panel();
            this.lblValProfiloSelezionato = new ConfigRiepiloghi.clsLabel();
            this.lblProfiloSelected = new ConfigRiepiloghi.clsLabel();
            this.pgrPROPRIETA_RIEPILOGHI = new System.Windows.Forms.PropertyGrid();
            this.tabControlALL.SuspendLayout();
            this.tabACCOUNT.SuspendLayout();
            this.pnlDescProfilo.SuspendLayout();
            this.pnlNome.SuspendLayout();
            this.pnlCognome.SuspendLayout();
            this.pnlEmail.SuspendLayout();
            this.tabRIEPILOGHI_CONFIG.SuspendLayout();
            this.tabSUB_ACCOUNTS.SuspendLayout();
            this.pnlACCOUNTS.SuspendLayout();
            this.pnlACCOUNTSbuttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvACCOUNTS)).BeginInit();
            this.tabPROFILES.SuspendLayout();
            this.pnlPROFILES.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPROFILES)).BeginInit();
            this.tabCODICI_SISTEMA.SuspendLayout();
            this.pnlCODICI_SISTEMA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCODICI_SISTEMA)).BeginInit();
            this.tabCODICI_LOCALI.SuspendLayout();
            this.pnlCODICI_LOCALI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCODICI_LOCALI)).BeginInit();
            this.tabORGANIZZATORI.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCF_ORGANIZZATORI)).BeginInit();
            this.tabCONFIG.SuspendLayout();
            this.pnlPROFILO_SELECTED.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlALL
            // 
            this.tabControlALL.Controls.Add(this.tabACCOUNT);
            this.tabControlALL.Controls.Add(this.tabRIEPILOGHI_CONFIG);
            this.tabControlALL.Controls.Add(this.tabSUB_ACCOUNTS);
            this.tabControlALL.Controls.Add(this.tabPROFILES);
            this.tabControlALL.Controls.Add(this.tabCODICI_SISTEMA);
            this.tabControlALL.Controls.Add(this.tabCODICI_LOCALI);
            this.tabControlALL.Controls.Add(this.tabORGANIZZATORI);
            this.tabControlALL.Controls.Add(this.tabCONFIG);
            this.tabControlALL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlALL.Location = new System.Drawing.Point(0, 0);
            this.tabControlALL.Name = "tabControlALL";
            this.tabControlALL.SelectedIndex = 0;
            this.tabControlALL.Size = new System.Drawing.Size(1002, 672);
            this.tabControlALL.TabIndex = 0;
            // 
            // tabACCOUNT
            // 
            this.tabACCOUNT.AutoScroll = true;
            this.tabACCOUNT.Controls.Add(this.pnlDescProfilo);
            this.tabACCOUNT.Controls.Add(this.pnlNome);
            this.tabACCOUNT.Controls.Add(this.pnlCognome);
            this.tabACCOUNT.Controls.Add(this.pnlEmail);
            this.tabACCOUNT.Location = new System.Drawing.Point(4, 38);
            this.tabACCOUNT.Name = "tabACCOUNT";
            this.tabACCOUNT.Padding = new System.Windows.Forms.Padding(3);
            this.tabACCOUNT.Size = new System.Drawing.Size(994, 630);
            this.tabACCOUNT.TabIndex = 0;
            this.tabACCOUNT.Text = "Account";
            this.tabACCOUNT.UseVisualStyleBackColor = true;
            // 
            // pnlDescProfilo
            // 
            this.pnlDescProfilo.Controls.Add(this.txtDescProfilo);
            this.pnlDescProfilo.Controls.Add(this.lblDescProfilo);
            this.pnlDescProfilo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDescProfilo.Location = new System.Drawing.Point(3, 102);
            this.pnlDescProfilo.Name = "pnlDescProfilo";
            this.pnlDescProfilo.Padding = new System.Windows.Forms.Padding(3);
            this.pnlDescProfilo.Size = new System.Drawing.Size(988, 33);
            this.pnlDescProfilo.TabIndex = 3;
            // 
            // txtDescProfilo
            // 
            this.txtDescProfilo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescProfilo.Location = new System.Drawing.Point(168, 3);
            this.txtDescProfilo.Name = "txtDescProfilo";
            this.txtDescProfilo.ReadOnly = true;
            this.txtDescProfilo.Size = new System.Drawing.Size(817, 37);
            this.txtDescProfilo.TabIndex = 1;
            // 
            // lblDescProfilo
            // 
            this.lblDescProfilo.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDescProfilo.Location = new System.Drawing.Point(3, 3);
            this.lblDescProfilo.Name = "lblDescProfilo";
            this.lblDescProfilo.Size = new System.Drawing.Size(165, 27);
            this.lblDescProfilo.TabIndex = 0;
            this.lblDescProfilo.Text = "Profilo di appartenenza";
            this.lblDescProfilo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlNome
            // 
            this.pnlNome.Controls.Add(this.txtNome);
            this.pnlNome.Controls.Add(this.lblNome);
            this.pnlNome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNome.Location = new System.Drawing.Point(3, 69);
            this.pnlNome.Name = "pnlNome";
            this.pnlNome.Padding = new System.Windows.Forms.Padding(3);
            this.pnlNome.Size = new System.Drawing.Size(988, 33);
            this.pnlNome.TabIndex = 2;
            // 
            // txtNome
            // 
            this.txtNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNome.Location = new System.Drawing.Point(168, 3);
            this.txtNome.Name = "txtNome";
            this.txtNome.ReadOnly = true;
            this.txtNome.Size = new System.Drawing.Size(817, 37);
            this.txtNome.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblNome.Location = new System.Drawing.Point(3, 3);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(165, 27);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome";
            this.lblNome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlCognome
            // 
            this.pnlCognome.Controls.Add(this.txtCognome);
            this.pnlCognome.Controls.Add(this.lblCognome);
            this.pnlCognome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCognome.Location = new System.Drawing.Point(3, 36);
            this.pnlCognome.Name = "pnlCognome";
            this.pnlCognome.Padding = new System.Windows.Forms.Padding(3);
            this.pnlCognome.Size = new System.Drawing.Size(988, 33);
            this.pnlCognome.TabIndex = 1;
            // 
            // txtCognome
            // 
            this.txtCognome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCognome.Location = new System.Drawing.Point(168, 3);
            this.txtCognome.Name = "txtCognome";
            this.txtCognome.ReadOnly = true;
            this.txtCognome.Size = new System.Drawing.Size(817, 37);
            this.txtCognome.TabIndex = 1;
            // 
            // lblCognome
            // 
            this.lblCognome.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCognome.Location = new System.Drawing.Point(3, 3);
            this.lblCognome.Name = "lblCognome";
            this.lblCognome.Size = new System.Drawing.Size(165, 27);
            this.lblCognome.TabIndex = 0;
            this.lblCognome.Text = "Cognome:";
            this.lblCognome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlEmail
            // 
            this.pnlEmail.Controls.Add(this.txtEmail);
            this.pnlEmail.Controls.Add(this.lblEmail);
            this.pnlEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlEmail.Location = new System.Drawing.Point(3, 3);
            this.pnlEmail.Name = "pnlEmail";
            this.pnlEmail.Padding = new System.Windows.Forms.Padding(3);
            this.pnlEmail.Size = new System.Drawing.Size(988, 33);
            this.pnlEmail.TabIndex = 0;
            // 
            // txtEmail
            // 
            this.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEmail.Location = new System.Drawing.Point(168, 3);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(817, 37);
            this.txtEmail.TabIndex = 1;
            // 
            // lblEmail
            // 
            this.lblEmail.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEmail.Location = new System.Drawing.Point(3, 3);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(165, 27);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "Email:";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabRIEPILOGHI_CONFIG
            // 
            this.tabRIEPILOGHI_CONFIG.Controls.Add(this.pgrPROPRIETA_RIEPILOGHI);
            this.tabRIEPILOGHI_CONFIG.Location = new System.Drawing.Point(4, 38);
            this.tabRIEPILOGHI_CONFIG.Name = "tabRIEPILOGHI_CONFIG";
            this.tabRIEPILOGHI_CONFIG.Size = new System.Drawing.Size(994, 630);
            this.tabRIEPILOGHI_CONFIG.TabIndex = 7;
            this.tabRIEPILOGHI_CONFIG.Text = "Configurazione generale";
            this.tabRIEPILOGHI_CONFIG.UseVisualStyleBackColor = true;
            // 
            // tabSUB_ACCOUNTS
            // 
            this.tabSUB_ACCOUNTS.Controls.Add(this.pnlACCOUNTS);
            this.tabSUB_ACCOUNTS.Location = new System.Drawing.Point(4, 38);
            this.tabSUB_ACCOUNTS.Name = "tabSUB_ACCOUNTS";
            this.tabSUB_ACCOUNTS.Size = new System.Drawing.Size(994, 630);
            this.tabSUB_ACCOUNTS.TabIndex = 3;
            this.tabSUB_ACCOUNTS.Text = "Accounts modificabili";
            this.tabSUB_ACCOUNTS.UseVisualStyleBackColor = true;
            // 
            // pnlACCOUNTS
            // 
            this.pnlACCOUNTS.AutoScroll = true;
            this.pnlACCOUNTS.Controls.Add(this.pgrACCOUNTS);
            this.pnlACCOUNTS.Controls.Add(this.panel1);
            this.pnlACCOUNTS.Controls.Add(this.pnlACCOUNTSbuttons);
            this.pnlACCOUNTS.Controls.Add(this.splitter2);
            this.pnlACCOUNTS.Controls.Add(this.dgvACCOUNTS);
            this.pnlACCOUNTS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlACCOUNTS.Location = new System.Drawing.Point(0, 0);
            this.pnlACCOUNTS.Name = "pnlACCOUNTS";
            this.pnlACCOUNTS.Size = new System.Drawing.Size(994, 630);
            this.pnlACCOUNTS.TabIndex = 1;
            // 
            // pgrACCOUNTS
            // 
            this.pgrACCOUNTS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrACCOUNTS.Location = new System.Drawing.Point(341, 39);
            this.pgrACCOUNTS.Name = "pgrACCOUNTS";
            this.pgrACCOUNTS.Size = new System.Drawing.Size(653, 511);
            this.pgrACCOUNTS.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(341, 550);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(653, 80);
            this.panel1.TabIndex = 5;
            // 
            // pnlACCOUNTSbuttons
            // 
            this.pnlACCOUNTSbuttons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlACCOUNTSbuttons.Controls.Add(this.btnACCOUNTSrenew);
            this.pnlACCOUNTSbuttons.Controls.Add(this.btnACCOUNTSadd);
            this.pnlACCOUNTSbuttons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlACCOUNTSbuttons.Location = new System.Drawing.Point(341, 0);
            this.pnlACCOUNTSbuttons.Name = "pnlACCOUNTSbuttons";
            this.pnlACCOUNTSbuttons.Size = new System.Drawing.Size(653, 39);
            this.pnlACCOUNTSbuttons.TabIndex = 7;
            // 
            // btnACCOUNTSrenew
            // 
            this.btnACCOUNTSrenew.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnACCOUNTSrenew.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnACCOUNTSrenew.Location = new System.Drawing.Point(104, 0);
            this.btnACCOUNTSrenew.Name = "btnACCOUNTSrenew";
            this.btnACCOUNTSrenew.Size = new System.Drawing.Size(143, 37);
            this.btnACCOUNTSrenew.TabIndex = 2;
            this.btnACCOUNTSrenew.Text = "Rinnova Password";
            this.btnACCOUNTSrenew.UseVisualStyleBackColor = true;
            // 
            // btnACCOUNTSadd
            // 
            this.btnACCOUNTSadd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnACCOUNTSadd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnACCOUNTSadd.Location = new System.Drawing.Point(0, 0);
            this.btnACCOUNTSadd.Name = "btnACCOUNTSadd";
            this.btnACCOUNTSadd.Size = new System.Drawing.Size(104, 37);
            this.btnACCOUNTSadd.TabIndex = 1;
            this.btnACCOUNTSadd.Text = "Aggiungi";
            this.btnACCOUNTSadd.UseVisualStyleBackColor = true;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(331, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(10, 630);
            this.splitter2.TabIndex = 6;
            this.splitter2.TabStop = false;
            // 
            // dgvACCOUNTS
            // 
            this.dgvACCOUNTS.AllowUserToAddRows = false;
            this.dgvACCOUNTS.AllowUserToDeleteRows = false;
            this.dgvACCOUNTS.AllowUserToResizeColumns = false;
            this.dgvACCOUNTS.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvACCOUNTS.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvACCOUNTS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvACCOUNTS.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvACCOUNTS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvACCOUNTS.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvACCOUNTS.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvACCOUNTS.Location = new System.Drawing.Point(0, 0);
            this.dgvACCOUNTS.Name = "dgvACCOUNTS";
            this.dgvACCOUNTS.ReadOnly = true;
            this.dgvACCOUNTS.RowHeadersVisible = false;
            this.dgvACCOUNTS.RowHeadersWidth = 62;
            this.dgvACCOUNTS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvACCOUNTS.Size = new System.Drawing.Size(331, 630);
            this.dgvACCOUNTS.TabIndex = 2;
            // 
            // tabPROFILES
            // 
            this.tabPROFILES.Controls.Add(this.pnlPROFILES);
            this.tabPROFILES.Location = new System.Drawing.Point(4, 38);
            this.tabPROFILES.Name = "tabPROFILES";
            this.tabPROFILES.Padding = new System.Windows.Forms.Padding(3);
            this.tabPROFILES.Size = new System.Drawing.Size(994, 630);
            this.tabPROFILES.TabIndex = 1;
            this.tabPROFILES.Text = "Profili Modificabili";
            this.tabPROFILES.UseVisualStyleBackColor = true;
            // 
            // pnlPROFILES
            // 
            this.pnlPROFILES.AutoScroll = true;
            this.pnlPROFILES.Controls.Add(this.pgrPROFILES);
            this.pnlPROFILES.Controls.Add(this.panel3);
            this.pnlPROFILES.Controls.Add(this.pnlPROFILESbot);
            this.pnlPROFILES.Controls.Add(this.splitter1);
            this.pnlPROFILES.Controls.Add(this.dgvPROFILES);
            this.pnlPROFILES.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPROFILES.Location = new System.Drawing.Point(3, 3);
            this.pnlPROFILES.Name = "pnlPROFILES";
            this.pnlPROFILES.Padding = new System.Windows.Forms.Padding(3);
            this.pnlPROFILES.Size = new System.Drawing.Size(988, 624);
            this.pnlPROFILES.TabIndex = 1;
            // 
            // pgrPROFILES
            // 
            this.pgrPROFILES.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrPROFILES.Location = new System.Drawing.Point(344, 42);
            this.pgrPROFILES.Name = "pgrPROFILES";
            this.pgrPROFILES.Size = new System.Drawing.Size(641, 499);
            this.pgrPROFILES.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnPROFILESdel);
            this.panel3.Controls.Add(this.btnPROFILESadd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(344, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(641, 39);
            this.panel3.TabIndex = 8;
            // 
            // btnPROFILESdel
            // 
            this.btnPROFILESdel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPROFILESdel.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPROFILESdel.Location = new System.Drawing.Point(104, 0);
            this.btnPROFILESdel.Name = "btnPROFILESdel";
            this.btnPROFILESdel.Size = new System.Drawing.Size(104, 37);
            this.btnPROFILESdel.TabIndex = 2;
            this.btnPROFILESdel.Text = "Elimina";
            this.btnPROFILESdel.UseVisualStyleBackColor = true;
            // 
            // btnPROFILESadd
            // 
            this.btnPROFILESadd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPROFILESadd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPROFILESadd.Location = new System.Drawing.Point(0, 0);
            this.btnPROFILESadd.Name = "btnPROFILESadd";
            this.btnPROFILESadd.Size = new System.Drawing.Size(104, 37);
            this.btnPROFILESadd.TabIndex = 1;
            this.btnPROFILESadd.Text = "Aggiungi";
            this.btnPROFILESadd.UseVisualStyleBackColor = true;
            // 
            // pnlPROFILESbot
            // 
            this.pnlPROFILESbot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlPROFILESbot.Location = new System.Drawing.Point(344, 541);
            this.pnlPROFILESbot.Name = "pnlPROFILESbot";
            this.pnlPROFILESbot.Size = new System.Drawing.Size(641, 80);
            this.pnlPROFILESbot.TabIndex = 2;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(334, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 618);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // dgvPROFILES
            // 
            this.dgvPROFILES.AllowUserToAddRows = false;
            this.dgvPROFILES.AllowUserToDeleteRows = false;
            this.dgvPROFILES.AllowUserToResizeColumns = false;
            this.dgvPROFILES.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvPROFILES.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPROFILES.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPROFILES.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPROFILES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPROFILES.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvPROFILES.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPROFILES.Location = new System.Drawing.Point(3, 3);
            this.dgvPROFILES.Name = "dgvPROFILES";
            this.dgvPROFILES.ReadOnly = true;
            this.dgvPROFILES.RowHeadersVisible = false;
            this.dgvPROFILES.RowHeadersWidth = 62;
            this.dgvPROFILES.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPROFILES.Size = new System.Drawing.Size(331, 618);
            this.dgvPROFILES.TabIndex = 0;
            // 
            // tabCODICI_SISTEMA
            // 
            this.tabCODICI_SISTEMA.Controls.Add(this.pnlCODICI_SISTEMA);
            this.tabCODICI_SISTEMA.Location = new System.Drawing.Point(4, 29);
            this.tabCODICI_SISTEMA.Name = "tabCODICI_SISTEMA";
            this.tabCODICI_SISTEMA.Size = new System.Drawing.Size(994, 639);
            this.tabCODICI_SISTEMA.TabIndex = 2;
            this.tabCODICI_SISTEMA.Text = "Codici Sistema";
            this.tabCODICI_SISTEMA.UseVisualStyleBackColor = true;
            // 
            // pnlCODICI_SISTEMA
            // 
            this.pnlCODICI_SISTEMA.AutoScroll = true;
            this.pnlCODICI_SISTEMA.Controls.Add(this.pgrCODICI_SISTEMA);
            this.pnlCODICI_SISTEMA.Controls.Add(this.panel2);
            this.pnlCODICI_SISTEMA.Controls.Add(this.splitter3);
            this.pnlCODICI_SISTEMA.Controls.Add(this.dgvCODICI_SISTEMA);
            this.pnlCODICI_SISTEMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCODICI_SISTEMA.Location = new System.Drawing.Point(0, 0);
            this.pnlCODICI_SISTEMA.Name = "pnlCODICI_SISTEMA";
            this.pnlCODICI_SISTEMA.Size = new System.Drawing.Size(994, 639);
            this.pnlCODICI_SISTEMA.TabIndex = 1;
            // 
            // pgrCODICI_SISTEMA
            // 
            this.pgrCODICI_SISTEMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrCODICI_SISTEMA.Location = new System.Drawing.Point(341, 0);
            this.pgrCODICI_SISTEMA.Name = "pgrCODICI_SISTEMA";
            this.pgrCODICI_SISTEMA.Size = new System.Drawing.Size(653, 559);
            this.pgrCODICI_SISTEMA.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(341, 559);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(653, 80);
            this.panel2.TabIndex = 5;
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(331, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(10, 639);
            this.splitter3.TabIndex = 6;
            this.splitter3.TabStop = false;
            // 
            // dgvCODICI_SISTEMA
            // 
            this.dgvCODICI_SISTEMA.AllowUserToAddRows = false;
            this.dgvCODICI_SISTEMA.AllowUserToDeleteRows = false;
            this.dgvCODICI_SISTEMA.AllowUserToResizeColumns = false;
            this.dgvCODICI_SISTEMA.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCODICI_SISTEMA.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCODICI_SISTEMA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCODICI_SISTEMA.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCODICI_SISTEMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCODICI_SISTEMA.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvCODICI_SISTEMA.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCODICI_SISTEMA.Location = new System.Drawing.Point(0, 0);
            this.dgvCODICI_SISTEMA.Name = "dgvCODICI_SISTEMA";
            this.dgvCODICI_SISTEMA.ReadOnly = true;
            this.dgvCODICI_SISTEMA.RowHeadersVisible = false;
            this.dgvCODICI_SISTEMA.RowHeadersWidth = 62;
            this.dgvCODICI_SISTEMA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCODICI_SISTEMA.Size = new System.Drawing.Size(331, 639);
            this.dgvCODICI_SISTEMA.TabIndex = 2;
            // 
            // tabCODICI_LOCALI
            // 
            this.tabCODICI_LOCALI.Controls.Add(this.pnlCODICI_LOCALI);
            this.tabCODICI_LOCALI.Location = new System.Drawing.Point(4, 29);
            this.tabCODICI_LOCALI.Name = "tabCODICI_LOCALI";
            this.tabCODICI_LOCALI.Size = new System.Drawing.Size(994, 639);
            this.tabCODICI_LOCALI.TabIndex = 5;
            this.tabCODICI_LOCALI.Text = "Codici locali";
            this.tabCODICI_LOCALI.UseVisualStyleBackColor = true;
            // 
            // pnlCODICI_LOCALI
            // 
            this.pnlCODICI_LOCALI.AutoScroll = true;
            this.pnlCODICI_LOCALI.Controls.Add(this.pgrCODICI_LOCALI);
            this.pnlCODICI_LOCALI.Controls.Add(this.panel5);
            this.pnlCODICI_LOCALI.Controls.Add(this.splitter4);
            this.pnlCODICI_LOCALI.Controls.Add(this.dgvCODICI_LOCALI);
            this.pnlCODICI_LOCALI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCODICI_LOCALI.Location = new System.Drawing.Point(0, 0);
            this.pnlCODICI_LOCALI.Name = "pnlCODICI_LOCALI";
            this.pnlCODICI_LOCALI.Size = new System.Drawing.Size(994, 639);
            this.pnlCODICI_LOCALI.TabIndex = 2;
            // 
            // pgrCODICI_LOCALI
            // 
            this.pgrCODICI_LOCALI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrCODICI_LOCALI.Location = new System.Drawing.Point(341, 0);
            this.pgrCODICI_LOCALI.Name = "pgrCODICI_LOCALI";
            this.pgrCODICI_LOCALI.Size = new System.Drawing.Size(653, 559);
            this.pgrCODICI_LOCALI.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(341, 559);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(653, 80);
            this.panel5.TabIndex = 5;
            // 
            // splitter4
            // 
            this.splitter4.Location = new System.Drawing.Point(331, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(10, 639);
            this.splitter4.TabIndex = 6;
            this.splitter4.TabStop = false;
            // 
            // dgvCODICI_LOCALI
            // 
            this.dgvCODICI_LOCALI.AllowUserToAddRows = false;
            this.dgvCODICI_LOCALI.AllowUserToDeleteRows = false;
            this.dgvCODICI_LOCALI.AllowUserToResizeColumns = false;
            this.dgvCODICI_LOCALI.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCODICI_LOCALI.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCODICI_LOCALI.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCODICI_LOCALI.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCODICI_LOCALI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCODICI_LOCALI.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvCODICI_LOCALI.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCODICI_LOCALI.Location = new System.Drawing.Point(0, 0);
            this.dgvCODICI_LOCALI.Name = "dgvCODICI_LOCALI";
            this.dgvCODICI_LOCALI.ReadOnly = true;
            this.dgvCODICI_LOCALI.RowHeadersVisible = false;
            this.dgvCODICI_LOCALI.RowHeadersWidth = 62;
            this.dgvCODICI_LOCALI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCODICI_LOCALI.Size = new System.Drawing.Size(331, 639);
            this.dgvCODICI_LOCALI.TabIndex = 2;
            // 
            // tabORGANIZZATORI
            // 
            this.tabORGANIZZATORI.Controls.Add(this.panel4);
            this.tabORGANIZZATORI.Location = new System.Drawing.Point(4, 29);
            this.tabORGANIZZATORI.Name = "tabORGANIZZATORI";
            this.tabORGANIZZATORI.Size = new System.Drawing.Size(994, 639);
            this.tabORGANIZZATORI.TabIndex = 6;
            this.tabORGANIZZATORI.Text = "Organizzatori";
            this.tabORGANIZZATORI.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.Controls.Add(this.pgrCF_ORGANIZZATORI);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.splitter5);
            this.panel4.Controls.Add(this.dgvCF_ORGANIZZATORI);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(994, 639);
            this.panel4.TabIndex = 2;
            // 
            // pgrCF_ORGANIZZATORI
            // 
            this.pgrCF_ORGANIZZATORI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrCF_ORGANIZZATORI.Location = new System.Drawing.Point(341, 0);
            this.pgrCF_ORGANIZZATORI.Name = "pgrCF_ORGANIZZATORI";
            this.pgrCF_ORGANIZZATORI.Size = new System.Drawing.Size(653, 559);
            this.pgrCF_ORGANIZZATORI.TabIndex = 4;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(341, 559);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(653, 80);
            this.panel6.TabIndex = 5;
            // 
            // splitter5
            // 
            this.splitter5.Location = new System.Drawing.Point(331, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(10, 639);
            this.splitter5.TabIndex = 6;
            this.splitter5.TabStop = false;
            // 
            // dgvCF_ORGANIZZATORI
            // 
            this.dgvCF_ORGANIZZATORI.AllowUserToAddRows = false;
            this.dgvCF_ORGANIZZATORI.AllowUserToDeleteRows = false;
            this.dgvCF_ORGANIZZATORI.AllowUserToResizeColumns = false;
            this.dgvCF_ORGANIZZATORI.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvCF_ORGANIZZATORI.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCF_ORGANIZZATORI.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCF_ORGANIZZATORI.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCF_ORGANIZZATORI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCF_ORGANIZZATORI.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvCF_ORGANIZZATORI.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCF_ORGANIZZATORI.Location = new System.Drawing.Point(0, 0);
            this.dgvCF_ORGANIZZATORI.Name = "dgvCF_ORGANIZZATORI";
            this.dgvCF_ORGANIZZATORI.ReadOnly = true;
            this.dgvCF_ORGANIZZATORI.RowHeadersVisible = false;
            this.dgvCF_ORGANIZZATORI.RowHeadersWidth = 62;
            this.dgvCF_ORGANIZZATORI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCF_ORGANIZZATORI.Size = new System.Drawing.Size(331, 639);
            this.dgvCF_ORGANIZZATORI.TabIndex = 2;
            // 
            // tabCONFIG
            // 
            this.tabCONFIG.Controls.Add(this.pnlCONFIG);
            this.tabCONFIG.Location = new System.Drawing.Point(4, 29);
            this.tabCONFIG.Name = "tabCONFIG";
            this.tabCONFIG.Size = new System.Drawing.Size(994, 639);
            this.tabCONFIG.TabIndex = 4;
            this.tabCONFIG.Text = "Configurazione";
            this.tabCONFIG.UseVisualStyleBackColor = true;
            // 
            // pnlCONFIG
            // 
            this.pnlCONFIG.AutoScroll = true;
            this.pnlCONFIG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCONFIG.Location = new System.Drawing.Point(0, 0);
            this.pnlCONFIG.Name = "pnlCONFIG";
            this.pnlCONFIG.Size = new System.Drawing.Size(994, 639);
            this.pnlCONFIG.TabIndex = 0;
            // 
            // pnlPROFILO_SELECTED
            // 
            this.pnlPROFILO_SELECTED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPROFILO_SELECTED.Controls.Add(this.lblValProfiloSelezionato);
            this.pnlPROFILO_SELECTED.Controls.Add(this.lblProfiloSelected);
            this.pnlPROFILO_SELECTED.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlPROFILO_SELECTED.Location = new System.Drawing.Point(0, 672);
            this.pnlPROFILO_SELECTED.Name = "pnlPROFILO_SELECTED";
            this.pnlPROFILO_SELECTED.Size = new System.Drawing.Size(1002, 40);
            this.pnlPROFILO_SELECTED.TabIndex = 4;
            // 
            // lblValProfiloSelezionato
            // 
            this.lblValProfiloSelezionato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValProfiloSelezionato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblValProfiloSelezionato.Location = new System.Drawing.Point(234, 0);
            this.lblValProfiloSelezionato.Name = "lblValProfiloSelezionato";
            this.lblValProfiloSelezionato.Size = new System.Drawing.Size(766, 38);
            this.lblValProfiloSelezionato.TabIndex = 2;
            this.lblValProfiloSelezionato.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProfiloSelected
            // 
            this.lblProfiloSelected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProfiloSelected.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblProfiloSelected.Location = new System.Drawing.Point(0, 0);
            this.lblProfiloSelected.Name = "lblProfiloSelected";
            this.lblProfiloSelected.Size = new System.Drawing.Size(234, 38);
            this.lblProfiloSelected.TabIndex = 1;
            this.lblProfiloSelected.Text = "Profilo selezionato da modificare:";
            this.lblProfiloSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblProfiloSelected.Click += new System.EventHandler(this.lblProfiloSelected_Click);
            // 
            // pgrPROPRIETA_RIEPILOGHI
            // 
            this.pgrPROPRIETA_RIEPILOGHI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgrPROPRIETA_RIEPILOGHI.Location = new System.Drawing.Point(0, 0);
            this.pgrPROPRIETA_RIEPILOGHI.Name = "pgrPROPRIETA_RIEPILOGHI";
            this.pgrPROPRIETA_RIEPILOGHI.Size = new System.Drawing.Size(994, 630);
            this.pgrPROPRIETA_RIEPILOGHI.TabIndex = 5;
            // 
            // frmConfigMulti
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1002, 712);
            this.Controls.Add(this.tabControlALL);
            this.Controls.Add(this.pnlPROFILO_SELECTED);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmConfigMulti";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurazione Riepiloghi Multi";
            this.tabControlALL.ResumeLayout(false);
            this.tabACCOUNT.ResumeLayout(false);
            this.pnlDescProfilo.ResumeLayout(false);
            this.pnlDescProfilo.PerformLayout();
            this.pnlNome.ResumeLayout(false);
            this.pnlNome.PerformLayout();
            this.pnlCognome.ResumeLayout(false);
            this.pnlCognome.PerformLayout();
            this.pnlEmail.ResumeLayout(false);
            this.pnlEmail.PerformLayout();
            this.tabRIEPILOGHI_CONFIG.ResumeLayout(false);
            this.tabSUB_ACCOUNTS.ResumeLayout(false);
            this.pnlACCOUNTS.ResumeLayout(false);
            this.pnlACCOUNTSbuttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvACCOUNTS)).EndInit();
            this.tabPROFILES.ResumeLayout(false);
            this.pnlPROFILES.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPROFILES)).EndInit();
            this.tabCODICI_SISTEMA.ResumeLayout(false);
            this.pnlCODICI_SISTEMA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCODICI_SISTEMA)).EndInit();
            this.tabCODICI_LOCALI.ResumeLayout(false);
            this.pnlCODICI_LOCALI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCODICI_LOCALI)).EndInit();
            this.tabORGANIZZATORI.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCF_ORGANIZZATORI)).EndInit();
            this.tabCONFIG.ResumeLayout(false);
            this.pnlPROFILO_SELECTED.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlALL;
        private System.Windows.Forms.TabPage tabACCOUNT;
        private System.Windows.Forms.TabPage tabPROFILES;
        private System.Windows.Forms.TabPage tabSUB_ACCOUNTS;
        private System.Windows.Forms.TabPage tabCODICI_SISTEMA;
        private System.Windows.Forms.Panel pnlEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private ConfigRiepiloghi.clsLabel lblEmail;
        private System.Windows.Forms.Panel pnlNome;
        private System.Windows.Forms.TextBox txtNome;
        private ConfigRiepiloghi.clsLabel lblNome;
        private System.Windows.Forms.Panel pnlCognome;
        private System.Windows.Forms.TextBox txtCognome;
        private ConfigRiepiloghi.clsLabel lblCognome;
        private System.Windows.Forms.TabPage tabCONFIG;
        private System.Windows.Forms.Panel pnlCONFIG;
        private System.Windows.Forms.Panel pnlPROFILES;
        private System.Windows.Forms.Panel pnlACCOUNTS;
        private System.Windows.Forms.Panel pnlCODICI_SISTEMA;
        private System.Windows.Forms.DataGridView dgvPROFILES;
        private System.Windows.Forms.DataGridView dgvACCOUNTS;
        private System.Windows.Forms.DataGridView dgvCODICI_SISTEMA;
        private System.Windows.Forms.PropertyGrid pgrPROFILES;
        private System.Windows.Forms.Panel pnlPROFILESbot;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PropertyGrid pgrACCOUNTS;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.PropertyGrid pgrCODICI_SISTEMA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.TabPage tabCODICI_LOCALI;
        private System.Windows.Forms.TabPage tabORGANIZZATORI;
        private System.Windows.Forms.Panel pnlPROFILO_SELECTED;
        private ConfigRiepiloghi.clsLabel lblProfiloSelected;
        private ConfigRiepiloghi.clsLabel lblValProfiloSelezionato;
        private System.Windows.Forms.Panel pnlDescProfilo;
        private System.Windows.Forms.TextBox txtDescProfilo;
        private ConfigRiepiloghi.clsLabel lblDescProfilo;
        private System.Windows.Forms.Panel pnlACCOUNTSbuttons;
        private ConfigRiepiloghi.clsButton btnACCOUNTSrenew;
        private ConfigRiepiloghi.clsButton btnACCOUNTSadd;
        private System.Windows.Forms.Panel panel3;
        private ConfigRiepiloghi.clsButton btnPROFILESdel;
        private ConfigRiepiloghi.clsButton btnPROFILESadd;
        private System.Windows.Forms.Panel pnlCODICI_LOCALI;
        private System.Windows.Forms.PropertyGrid pgrCODICI_LOCALI;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.DataGridView dgvCODICI_LOCALI;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PropertyGrid pgrCF_ORGANIZZATORI;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.DataGridView dgvCF_ORGANIZZATORI;
        private System.Windows.Forms.TabPage tabRIEPILOGHI_CONFIG;
        private System.Windows.Forms.PropertyGrid pgrPROPRIETA_RIEPILOGHI;
    }
}

