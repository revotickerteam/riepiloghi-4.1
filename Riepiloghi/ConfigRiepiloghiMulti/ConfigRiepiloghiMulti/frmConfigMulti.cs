﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace ConfigRiepiloghiMulti
{
    public partial class frmConfigMulti : Form
    {

        #region variabili form

        public delegate void delegateInitProfile(Riepiloghi.clsProfile profile);
        public static event delegateInitProfile InitProfileEvent;

        private IConnection conn { get; set; }
        private string generalToken { get; set; }

        private Riepiloghi.clsAccount AccountLoggato { get; set; }
        private Riepiloghi.clsProfile ProfiloAccount { get; set; }
        public static List<Riepiloghi.clsProfile> ListaProfili { get; set; }
        private List<Riepiloghi.clsAccount> accountAccounts { get; set; }

        private static bool ProfilesEnabled { get; set; }
        private static bool AccountsEnabled { get; set; }
        private static bool ConfigSistemiEnabled { get; set; }
        private static bool CodiciSistemaEnabled { get; set; }

        private static Riepiloghi.clsProfile selectedProfile { get; set; }

        private clsViewProfile lastProfileSelected { get; set; }
        private clsViewCodiceSistema lastCodiceSistemaSelected { get; set; }
        private clsViewCodiceLocale lastCodiceLocaleSelected { get; set; }
        private clsViewOrganizzatore lastOrganizzatoreSelected { get; set; }

        private clsRP_PROPERTIES RP_PROPERTIES { get; set; }

        private bool loading = false;

        private static Riepiloghi.clsProfile SelectedProfile
        {
            get { return selectedProfile; }
            set
            {
                selectedProfile = value;
                InitProfileEvent(selectedProfile);
            }
        }

        //private string lastCodiceSistemaSelected = "";

        #endregion

        #region Chiamate Get/Set

        private bool Login(string login, string password, out Exception oError)
        {
            oError = null;
            if (oError == null)
                this.generalToken = Riepiloghi.clsToken.GetToken(this.conn, out oError, login, password);

            if (oError == null)
                this.AccountLoggato = Riepiloghi.clsToken.GetAccountFromToken(this.conn, out oError, this.generalToken);

            if (oError == null)
                this.ProfiloAccount = Riepiloghi.clsProfile.GetProfile(this.AccountLoggato.ProfileId, this.conn, out oError);
            return oError == null && this.generalToken != null && this.AccountLoggato != null && this.AccountLoggato.Enabled && this.ProfiloAccount != null && this.ProfiloAccount.Enabled;
        }

        private List<Riepiloghi.clsAccount> GetAccountsList(string accountId, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsAccount.GetAccountsList(accountId, this.conn, out oError);
        }

        private List<Riepiloghi.clsProfile> GetProfileList(string accountId, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsProfile.GetProfileList(accountId, this.conn, out oError);
        }

        private Riepiloghi.clsAccount GetAccount(string accountId, out Exception oError, bool lOnlyEnabled, bool CheckConfigCS, bool excludeMainRiepiloghi)
        {
            oError = null;
            return Riepiloghi.clsAccount.GetAccount(accountId, this.conn, out oError, lOnlyEnabled, CheckConfigCS, excludeMainRiepiloghi);
        }

        private Riepiloghi.clsProfile GetProfile(long profileId, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsProfile.GetProfile(profileId, this.conn, out oError);
        }


        private Riepiloghi.clsAccount SetAccount(Riepiloghi.clsAccount account, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsAccount.SetAccount(account.AccountId, this.conn, out oError, account);
        }

        private Riepiloghi.clsProfile SetProfile(Riepiloghi.clsProfile profile, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsProfile.SetProfile(profile, this.conn, out oError);
        }

        private Riepiloghi.clsAccount CreateAccount(string parentAccountId, string url, string email, string nome, string cognome, long profileId,  out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsAccount.CreateAccount(parentAccountId, url, email, nome, cognome, profileId, this.conn, out oError);
        }

       

        private bool SendMailAccountCreateRenew(string accountId, string typeLogin, string url, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsEmail.SendMailAccountCreateRenew(accountId, typeLogin, url, this.conn, out oError);
        }


        private Riepiloghi.clsProfile CreateProfile(string descrizione, long parentProfileId, out Exception oError)
        {
            oError = null;
            return Riepiloghi.clsProfile.CreateProfile(descrizione, parentProfileId, this.conn, out oError);
        }

        private clsRP_PROPERTIES InitProperties()
        {
            Exception oError = null;
            clsRP_PROPERTIES result = new clsRP_PROPERTIES();


            List<Riepiloghi.clsRP_PROPERTY> listaProprieta = Riepiloghi.clsRP_PROPERTY.GetPR_PROPERTIES(this.conn, out oError);
            if (oError == null && listaProprieta != null && listaProprieta.Count > 0)
            {
                Type ObjectType = typeof(clsRP_PROPERTIES);
                System.Reflection.PropertyInfo[] _propertyInfo = ObjectType.GetProperties();
                foreach (Riepiloghi.clsRP_PROPERTY item in listaProprieta)
                {
                    foreach (System.Reflection.PropertyInfo _property in _propertyInfo.Where(pi => pi.Name == item.Property).ToList())
                    {
                        _property.SetValue(result, item.Value);
                    }
                }
            }

            if (oError != null) MessageBox.Show(oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return result;
        }

        private clsRP_PROPERTIES SaveProperties(clsRP_PROPERTIES configuration, out Exception oError)
        {
            oError = null;
            ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
            oFrmWait.Message = "Salvataggio...";
            Application.DoEvents();
            conn.BeginTransaction();
            try
            {
                Type ObjectType = typeof(clsRP_PROPERTIES);
                System.Reflection.PropertyInfo[] _propertyInfo = ObjectType.GetProperties();

                foreach (System.Reflection.PropertyInfo _property in _propertyInfo)
                {
                    StringBuilder oSB = new StringBuilder();
                    oSB.Append("UPDATE RIEPILOGHI.RP_PROPERTIES SET VALUE = :pVALUE WHERE PROPERTY = :pPROPERTY");
                    clsParameters oPars = new clsParameters();
                    oPars.Add(":pPROPERTY", _property.Name);
                    oPars.Add(":pVALUE", _property.GetValue(configuration));
                    this.conn.ExecuteNonQuery(oSB.ToString(), oPars);
                }

            }
            catch (Exception ex)
            {
                oError = ex;
            }
            if (oError == null)
                conn.EndTransaction();
            else
                conn.RollBack();

            clsRP_PROPERTIES result = this.InitProperties();

            oFrmWait.Close();
            oFrmWait.Dispose();
            Application.DoEvents();

            if (oError != null) MessageBox.Show(oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return result;
        }

        private List<Riepiloghi.clsProprietaRiepiloghi> GetListaProprietaRiepiloghi(string codiceSistema, out Exception oError)
        {
            oError = null;
            List<Riepiloghi.clsProprietaRiepiloghi> listaProprieta = null;
            
            Wintic.Data.IConnection connCodiceSistema = Riepiloghi.clsRiepiloghi.GetConnectionByCodiceSistema(this.conn, codiceSistema, out oError, "USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0};");
            if (oError == null && connCodiceSistema != null && connCodiceSistema.State == ConnectionState.Open)
            {
                listaProprieta = Riepiloghi.clsRiepiloghi.GetListaProprietaRiepiloghi(connCodiceSistema);
            }
            else if (oError != null)
                oError = new Exception("Connessione al codice sistema selezionato" + "\r\n" + oError.Message);
            if (connCodiceSistema != null)
            {
                connCodiceSistema.Close();
                connCodiceSistema.Dispose();
            }
            return listaProprieta;
        }


        #endregion

        #region classi di visualizzazione

        #region PROPRIETA GENERALI RP_PROPERTIES

        public class clsRP_PROPERTIES
        {
            public string CODICE_SISTEMA_LOGIN_OPERATORI_LOCALI { get; set; }
            public string LOG_WEB_SERVICES { get; set; }
            public string EMAIL_SYSTEM_ACCOUNT { get; set; }
            public string EMAIL_SYSTEM_SMTP { get; set; }
            public string EMAIL_SYSTEM_PORT { get; set; }
            public string EMAIL_SYSTEM_USE_DEFAULT_CREDENTIALS { get; set; }
            public string EMAIL_SYSTEM_ENABLE_SSL { get; set; }
            public string EMAIL_SYSTEM_USER { get; set; }
            public string EMAIL_SYSTEM_PASSWORD { get; set; }
            public string EMAIL_SYSTEM_DELIVERY_METHOD { get; set; }
            public string EMAIL_SYSTEM_CREATE_ACCOUNT_SUBJECT { get; set; }
            public string EMAIL_SYSTEM_CREATE_ACCOUNT_MESSAGE { get; set; }
            public string EMAIL_SYSTEM_CREATE_ACCOUNT_CC { get; set; }
            public string EMAIL_SYSTEM_CREATE_ACCOUNT_CCN { get; set; }
            public string EMAIL_SYSTEM_CREATE_ACCOUNT_NOSEND { get; set; }
            public string TIMEOUT_TOKEN_MINUTES { get; set; }
            public string CACHE_OLD_REQUESTS_DAYS { get; set; }

            public string PUBLIC_URL { get; set; }

        }

        #endregion

        #region ACCOUNTS

        private BindingSource bindAccounts { get; set; }

        public class clsViewAccount
        {
            internal Riepiloghi.clsAccount Account { get; set; }

            public clsViewAccount(Riepiloghi.clsAccount account)
            {
                this.Account = account;
            }

            [Browsable(true), Category("01 Generale")]
            public string Cognome
            {
                get { return this.Account.Cognome; }
                set { this.Account.Cognome = value; }
            }

            [Browsable(true), Category("01 Generale")]
            public string Nome
            {
                get { return this.Account.Nome; }
                set { this.Account.Nome = value; }
            }

            [Browsable(true), Category("01 Generale")]
            public string Email
            {
                get { return this.Account.Email; }
                set { this.Account.Email = value; }
            }

            [Browsable(true), Category("01 Generale"), TypeConverter(typeof(clsFormatStringConverterProfiles))]
            public string Profilo
            {
                get
                {
                    Riepiloghi.clsProfile profile = ListaProfili.FirstOrDefault(profilo => profilo.ProfileId == this.Account.ProfileId);
                    return (profile == null ? "" : profile.Descrizione);
                }
                set
                {
                    this.Account.ProfileId = ListaProfili.FirstOrDefault(profilo => profilo.Descrizione == value).ProfileId;
                }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Abilitato")]
            public bool Enabled
            {
                get { return this.Account.Enabled; }
                set { this.Account.Enabled = value; }
            }
        }

        #endregion

        #region PROFILI
        
        private BindingSource bindProfiles { get; set; }

        private class clsViewProfile
        {
            internal Riepiloghi.clsProfile Profile { get; set; }

            public clsViewProfile(Riepiloghi.clsProfile profile)
            {
                this.Profile = profile;
            }

            [Browsable(true), Category("01 Generale")]
            public string Descrizione
            {
                get { return this.Profile.Descrizione; }
                set { this.Profile.Descrizione = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Abilitato")]
            public bool Enabled
            {
                get { return this.Profile.Enabled; }
                set { if (SelectedProfile != null) this.Profile.Enabled = value; }
            }

            [Browsable(true), Category("02 Codici di sistema"), DisplayName("Tutti i codici di sistema")]
            public bool AllCodiciSistema
            {
                get { return this.Profile.AllCodiciSistema; }
                set { if (SelectedProfile != null) this.Profile.AllCodiciSistema = value; }
            }


        }

        #region CODICI SISTEMA PROFILO SELEZIONATO

        private BindingSource bindCodiciSistema { get; set; }

        private class clsViewCodiceSistema
        {
            internal Riepiloghi.clsCodiceSistema CodiceSistema { get; set; }

            public clsViewCodiceSistema(Riepiloghi.clsCodiceSistema codiceSistema)
            {
                this.CodiceSistema = codiceSistema;
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string Codice
            {
                get { return this.CodiceSistema.CodiceSistema; }
                //set { if (SelectedProfile != null) this.CodiceSistema.CodiceSistema = value; }
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string Descrizione
            {
                get { return this.CodiceSistema.Descrizione; }
                //set { if (SelectedProfile != null) this.CodiceSistema.Descrizione = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Abilitato")]
            public bool Enabled
            {
                get { return this.CodiceSistema.Enabled; }
                set { if (SelectedProfile != null) this.CodiceSistema.Enabled = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Tutti i codici locali")]
            public bool AllCodiciLocali
            {
                get { return this.CodiceSistema.AllCodiciLocali; }
                set { if (SelectedProfile != null) this.CodiceSistema.AllCodiciLocali = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Tutti gli organizzatori")]
            public bool AllCFOrganizzatori
            {
                get { return this.CodiceSistema.AllCFOrganizzatori; }
                set { if (SelectedProfile != null) this.CodiceSistema.AllCFOrganizzatori = value; }
            }
        }

        private class clsFormatStringConverterProfiles : StringConverter
        {
            public override Boolean GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
            public override Boolean GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }
            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                List<String> list = new List<String>();
                foreach (Riepiloghi.clsProfile profile in ListaProfili)
                    list.Add(profile.Descrizione);
                return new StandardValuesCollection(list);
            }
        }

        #endregion

        #region CODICI LOCALI PROFILO SELEZIONATO

        private BindingSource bindCodiciLocale { get; set; }

        private class clsViewCodiceLocale
        {
            internal Riepiloghi.clsCodiceLocale CodiceLocale { get; set; }

            public clsViewCodiceLocale(Riepiloghi.clsCodiceLocale codiceLocale)
            {
                this.CodiceLocale = codiceLocale;
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string Codice
            {
                get { return this.CodiceLocale.CodiceLocale; }
                //set { if (SelectedProfile != null) this.CodiceLocale.CodiceLocale = value; }
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string Descrizione
            {
                get { return this.CodiceLocale.Descrizione; }
                //set { if (SelectedProfile != null) this.CodiceLocale.Descrizione = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Abilitato")]
            public bool Enabled
            {
                get { return this.CodiceLocale.Enabled; }
                set { if (SelectedProfile != null) this.CodiceLocale.Enabled = value; }
            }
        }

        #endregion

        #region ORGANIZZATORI PROFILO SELEZIONATO

        private BindingSource bindOrganizzatori { get; set; }

        private class clsViewOrganizzatore
        {
            internal Riepiloghi.clsCFOrganizzatore Organizzatore { get; set; }

            public clsViewOrganizzatore(Riepiloghi.clsCFOrganizzatore organizzatore)
            {
                this.Organizzatore = organizzatore;
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string CFOrganizzatore
            {
                get { return this.Organizzatore.CFOrganizzatore; }
                //set { if (SelectedProfile != null) this.Organizzatore.CFOrganizzatore = value; }
            }

            [Browsable(true), ReadOnly(true), Category("01 Generale")]
            public string Descrizione
            {
                get { return this.Organizzatore.Descrizione; }
                //set { if (SelectedProfile != null) this.Organizzatore.Descrizione = value; }
            }

            [Browsable(true), Category("01 Generale"), DisplayName("Abilitato")]
            public bool Enabled
            {
                get { return this.Organizzatore.Enabled; }
                set { if (SelectedProfile != null) this.Organizzatore.Enabled = value; }
            }
        }

        #endregion

        #endregion

        #endregion

        public frmConfigMulti()
        {
            InitializeComponent();
            this.Shown += FrmConfigMulti_Shown;
            this.FormClosing += FrmConfigMulti_FormClosing;
            InitProfileEvent += FrmConfigMulti_InitProfileEvent;
        }

        private void FrmConfigMulti_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.conn != null)
            {
                try
                {
                    this.conn.Close();
                    this.conn.Dispose();
                }
                catch (Exception)
                {
                }
            }
        }

        private void FrmConfigMulti_Shown(object sender, EventArgs e)
        {
            Exception oError = null;
            string tnsName = "";
            string login = "";
            string password = "";

            if (oError == null)
            {
                List<ConfigRiepiloghi.clsItemGenericData> data = new List<ConfigRiepiloghi.clsItemGenericData>() { new ConfigRiepiloghi.clsItemGenericData("TNS", "Connessione", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "SERVERX", false, true) };
                ConfigRiepiloghi.frmGetGenericData fGetConnectionName = new ConfigRiepiloghi.frmGetGenericData("Connessione", data);
                if (fGetConnectionName.ShowDialog() == DialogResult.OK)
                {
                    fGetConnectionName.Data.ForEach(i => { if (i.Code == "TNS") tnsName = i.Value.ToString(); });
                }
                if (string.IsNullOrEmpty(tnsName) || string.IsNullOrWhiteSpace(tnsName))
                    oError = new Exception("Identificativo di connessione non valido.");
            }

            if (oError == null)
            {
                try
                {
                    this.conn = new Wintic.Data.oracle.CConnectionOracle();
                    this.conn.ConnectionString = string.Format("user=riepiloghi;password=riepiloghi;data source={0};", tnsName);
                    this.conn.Open();
                }
                catch (Exception ex)
                {
                    oError = new Exception("Errore di connessione:\r\n" + ex.Message);
                }
            }

            if (oError == null)
            {
                frmLogin oFrmLogin = new frmLogin();

                if (oFrmLogin.ShowDialog() == DialogResult.OK)
                {
                    login = oFrmLogin.Login;
                    password = oFrmLogin.Password;
                    if (string.IsNullOrEmpty(login) || string.IsNullOrWhiteSpace(login) ||
                        string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password))
                        oError = new Exception("Login/password non validi");
                }
                else
                    oError = new Exception("Accesso abbandonato");

                oFrmLogin.Close();
                oFrmLogin.Dispose();
            }


            if (oError == null)
            {
                if (this.Login(login, password, out oError))
                    this.InitForm();
            }

            if (oError != null)
            {
                MessageBox.Show(oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                this.Dispose();
            }
        }

        private void InitForm()
        {
            string initOperationDescr = "";
            Exception oError = null;
            ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
            oFrmWait.Message = "Caricamento...";
            this.loading = true;

            try
            {
                this.txtCognome.Text = this.AccountLoggato.Cognome;
                this.txtNome.Text = this.AccountLoggato.Nome;
                this.txtEmail.Text = this.AccountLoggato.Email;
                this.txtDescProfilo.Text = this.ProfiloAccount.Descrizione;

                foreach (Riepiloghi.clsCategoriaOperazioniRiepiloghi categoriaOperazioni in this.AccountLoggato.Operazioni)
                {
                    foreach (Riepiloghi.clsOperazioneRiepiloghi operazione in categoriaOperazioni.ListaOperazioni)
                    {
                        switch (operazione.IdOperazione)
                        {
                            case "CONFIG_RIEPILOGHI": { this.pgrPROPRIETA_RIEPILOGHI.Enabled = operazione.Enabled || true; break; }
                            case "CONFIG_SISTEMI": { this.pnlCODICI_SISTEMA.Enabled = operazione.Enabled || true; break; }
                            case "RIEPILOGHI_PROFILES": { this.pnlPROFILES.Enabled = operazione.Enabled || true; break; }
                            case "RIEPILOGHI_ACCOUNTS": { this.pnlACCOUNTS.Enabled = operazione.Enabled || true; break; }
                        }
                    }
                }

                ConfigSistemiEnabled = this.pgrPROPRIETA_RIEPILOGHI.Enabled;
                ProfilesEnabled = this.pnlPROFILES.Enabled;
                AccountsEnabled = this.pnlACCOUNTS.Enabled;
                CodiciSistemaEnabled = this.pnlCODICI_SISTEMA.Enabled;
                selectedProfile = null;

                this.dgvACCOUNTS.SelectionChanged -= DgvACCOUNTS_SelectionChanged;
                this.btnACCOUNTSadd.Click -= BtnACCOUNTSadd_Click;
                this.btnACCOUNTSrenew.Click -= BtnACCOUNTSrenew_Click;

                this.pgrPROPRIETA_RIEPILOGHI.PropertyValueChanged -= PgrPROPRIETA_RIEPILOGHI_PropertyValueChanged;
                this.RP_PROPERTIES = this.InitProperties();
                this.pgrPROPRIETA_RIEPILOGHI.SelectedObject = this.RP_PROPERTIES;
                if (ConfigSistemiEnabled)
                {
                    this.pgrPROPRIETA_RIEPILOGHI.PropertyValueChanged += PgrPROPRIETA_RIEPILOGHI_PropertyValueChanged;
                }

                this.pgrACCOUNTS.PropertyValueChanged -= PgrACCOUNTS_PropertyValueChanged;

                if (AccountsEnabled)
                {
                    initOperationDescr = "Lettura accounts";
                    List<Riepiloghi.clsAccount> accountsData = this.GetAccountsList(this.AccountLoggato.AccountId, out oError);
                    if (oError == null)
                    {
                        this.accountAccounts = new List<Riepiloghi.clsAccount>();
                        foreach (Riepiloghi.clsAccount account in accountsData)
                        {
                            initOperationDescr = string.Format("Lettura singolo account {0}", account.AccountId);
                            //this.accountAccounts.Add(this.GetAccount(account.AccountId, out oError, true, false, false));
                            this.accountAccounts.Add(account);
                            if (oError != null)
                                break;
                        }

                        if (oError == null)
                        {
                            this.bindAccounts = new BindingSource();
                            List<clsViewAccount> lista = new List<clsViewAccount>();
                            foreach (Riepiloghi.clsAccount account in this.accountAccounts)
                                lista.Add(new clsViewAccount(account));
                            this.bindAccounts.DataSource = lista;
                            this.dgvACCOUNTS.DataSource = this.bindAccounts;
                            this.dgvACCOUNTS.SelectionChanged += DgvACCOUNTS_SelectionChanged;
                            this.btnACCOUNTSadd.Click += BtnACCOUNTSadd_Click;
                            this.btnACCOUNTSrenew.Click += BtnACCOUNTSrenew_Click;
                            this.pgrACCOUNTS.PropertyValueChanged += PgrACCOUNTS_PropertyValueChanged;
                        }
                    }
                }

                if (oError == null)
                {
                    initOperationDescr = "Lettura profili";
                    ListaProfili = this.GetProfileList(this.AccountLoggato.AccountId, out oError);
                }

                this.dgvPROFILES.SelectionChanged -= DgvProfiles_SelectionChanged;
                this.btnPROFILESadd.Click -= BtnPROFILESadd_Click;
                this.btnPROFILESdel.Click -= BtnPROFILESdel_Click;

                if (ProfilesEnabled && oError == null)
                {
                    this.bindProfiles = new BindingSource();
                    List<clsViewProfile> lista = new List<clsViewProfile>();
                    foreach (Riepiloghi.clsProfile profile in ListaProfili)
                    {
                        initOperationDescr = string.Format("Lettura singolo profilo {0}", profile.ProfileId.ToString());
                        //Riepiloghi.clsProfile profileEdit = this.GetProfile(profile.ProfileId, out oError);
                        //if (oError != null)
                        //    break;
                        //lista.Add(new clsViewProfile(profileEdit));
                        lista.Add(new clsViewProfile(profile));
                    }
                    this.bindProfiles.DataSource = lista;
                    this.dgvPROFILES.DataSource = this.bindProfiles;
                    this.dgvPROFILES.SelectionChanged += DgvProfiles_SelectionChanged;
                    this.btnPROFILESadd.Click += BtnPROFILESadd_Click;
                    this.btnPROFILESdel.Click += BtnPROFILESdel_Click;
                    this.pgrPROFILES.PropertyValueChanged += PgrPROFILES_PropertyValueChanged;
                }

                this.dgvCODICI_SISTEMA.SelectionChanged -= DgvCODICI_SISTEMA_SelectionChanged;
                this.dgvCODICI_LOCALI.SelectionChanged -= DgvCODICI_LOCALI_SelectionChanged;
                this.dgvCF_ORGANIZZATORI.SelectionChanged -= DgvCF_ORGANIZZATORI_SelectionChanged;
                this.pgrCODICI_SISTEMA.PropertyValueChanged -= PgrCODICI_SISTEMA_PropertyValueChanged;
                this.pgrCODICI_LOCALI.PropertyValueChanged -= PgrCODICI_LOCALI_PropertyValueChanged;
                this.pgrCF_ORGANIZZATORI.PropertyValueChanged -= PgrCF_ORGANIZZATORI_PropertyValueChanged;


                if (CodiciSistemaEnabled)
                {
                    this.dgvCODICI_SISTEMA.SelectionChanged += DgvCODICI_SISTEMA_SelectionChanged;
                    this.dgvCODICI_LOCALI.SelectionChanged += DgvCODICI_LOCALI_SelectionChanged;
                    this.dgvCF_ORGANIZZATORI.SelectionChanged += DgvCF_ORGANIZZATORI_SelectionChanged;
                    this.pgrCODICI_SISTEMA.PropertyValueChanged += PgrCODICI_SISTEMA_PropertyValueChanged;
                    this.pgrCODICI_LOCALI.PropertyValueChanged += PgrCODICI_LOCALI_PropertyValueChanged;
                    this.pgrCF_ORGANIZZATORI.PropertyValueChanged += PgrCF_ORGANIZZATORI_PropertyValueChanged;

                    if (this.lastProfileSelected != null)
                    {
                        foreach (DataGridViewRow row in this.dgvPROFILES.Rows)
                        {
                            if (((clsViewProfile)row.DataBoundItem).Profile.ProfileId == this.lastProfileSelected.Profile.ProfileId)
                            {
                                this.dgvPROFILES.CurrentCell = row.Cells[1];
                                break;
                            }
                        }
                        if (this.lastCodiceSistemaSelected != null)
                        {
                            foreach (DataGridViewRow row in this.dgvCODICI_SISTEMA.Rows)
                            {
                                if (((clsViewCodiceSistema)row.DataBoundItem).CodiceSistema.CodiceSistema == this.lastCodiceSistemaSelected.CodiceSistema.CodiceSistema)
                                {
                                    this.dgvCODICI_SISTEMA.CurrentCell = row.Cells[1];
                                    break;
                                }
                            }

                            if (this.lastCodiceLocaleSelected != null)
                            {
                                foreach (DataGridViewRow row in this.dgvCODICI_LOCALI.Rows)
                                {
                                    if (((clsViewCodiceLocale)row.DataBoundItem).CodiceLocale.CodiceSistema == this.lastCodiceLocaleSelected.CodiceLocale.CodiceSistema &&
                                        ((clsViewCodiceLocale)row.DataBoundItem).CodiceLocale.CodiceLocale == this.lastCodiceLocaleSelected.CodiceLocale.CodiceLocale)
                                    {
                                        this.dgvCODICI_LOCALI.CurrentCell = row.Cells[1];
                                        break;
                                    }
                                }
                            }
                            if (this.lastOrganizzatoreSelected != null)
                            {
                                foreach (DataGridViewRow row in this.dgvCF_ORGANIZZATORI.Rows)
                                {
                                    if (((clsViewOrganizzatore)row.DataBoundItem).Organizzatore.CodiceSistema == lastOrganizzatoreSelected.Organizzatore.CodiceSistema &&
                                        ((clsViewOrganizzatore)row.DataBoundItem).Organizzatore.CFOrganizzatore == lastOrganizzatoreSelected.Organizzatore.CFOrganizzatore)
                                    {
                                        this.dgvCF_ORGANIZZATORI.CurrentCell = row.Cells[1];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = ex;
            }


            //this.lastProfileSelected = null;
            //this.lastCodiceSistemaSelected = null;
            //this.lastCodiceLocaleSelected = null;
            //this.lastOrganizzatoreSelected = null;

            oFrmWait.Close();
            oFrmWait.Dispose();

            this.loading = false;

            if (oError != null) MessageBox.Show("Operazione di lettura: " + initOperationDescr + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void PgrACCOUNTS_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Exception oError = null;
            if (this.pgrACCOUNTS.SelectedObject != null)
            {
                try
                {
                    Riepiloghi.clsAccount account = ((clsViewAccount)this.pgrACCOUNTS.SelectedObject).Account;
                    if (account != null)
                    {
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        account = this.SetAccount(account, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (oError != null)
                            MessageBox.Show("Salvataggio account" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void PgrPROFILES_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Exception oError = null;
            if (this.pgrPROFILES.SelectedObject != null)
            {
                try
                {
                    Riepiloghi.clsProfile profile = null;
                    profile = ((clsViewProfile)this.pgrPROFILES.SelectedObject).Profile;

                    if (profile != null)
                    {
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        profile = this.SetProfile(profile, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (oError != null)
                            MessageBox.Show("Salvataggio profilo" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void PgrPROPRIETA_RIEPILOGHI_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Exception oError = null;
            if (this.pgrPROPRIETA_RIEPILOGHI.SelectedObject != null)
            {
                clsRP_PROPERTIES result = this.SaveProperties((clsRP_PROPERTIES)this.pgrPROPRIETA_RIEPILOGHI.SelectedObject, out oError);
                if (oError == null)
                {
                    this.pgrPROPRIETA_RIEPILOGHI.SelectedObject = result;
                }
                else
                    MessageBox.Show("Salvataggio proprietà generali" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnPROFILESdel_Click(object sender, EventArgs e)
        {
            // Elimina un profilo
        }

        private void BtnPROFILESadd_Click(object sender, EventArgs e)
        {
            // Aggiunge un profilo
            string descrizione = "";
            Exception oError = null;
            if (ListaProfili != null && ListaProfili.Count > 0)
            {
                Dictionary<object, string> profiliSelezionabili = new Dictionary<object, string>();
                ListaProfili.ForEach(p => profiliSelezionabili.Add(p.ProfileId, p.Descrizione));

                object parentProfileValue = ConfigRiepiloghi.frmGetGenericData.MenuGenerico("Selezionare il profilo padre del nuovo profilo", profiliSelezionabili);
                long ParentProfileId = 0;
                if (parentProfileValue != null && long.TryParse(parentProfileValue.ToString(), out ParentProfileId))
                {
                    ConfigRiepiloghi.frmGetGenericData frmGetDescrizione = new ConfigRiepiloghi.frmGetGenericData("Descrizione Profilo", new List<ConfigRiepiloghi.clsItemGenericData>() { new ConfigRiepiloghi.clsItemGenericData("DES","Descrizione", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "", false, true, "") });
                    if (frmGetDescrizione.ShowDialog() == DialogResult.OK)
                        descrizione = frmGetDescrizione.Data.First().Value.ToString();
                    frmGetDescrizione.Close();
                    frmGetDescrizione.Dispose();
                    if (ListaProfili.Find(p => p.Descrizione == descrizione) != null)
                    {
                        MessageBox.Show(string.Format("descrizione {0} duplicata.", descrizione));
                        descrizione = "";
                    }
                    if (!string.IsNullOrEmpty(descrizione))
                    {
                        Riepiloghi.clsProfile profile = this.CreateProfile(descrizione, ParentProfileId, out oError);
                        if (oError == null && profile != null)
                            this.InitForm();
                    }
                }
            }
        }

        private void BtnACCOUNTSrenew_Click(object sender, EventArgs e)
        {
            // Rinnovo password dell'account
            Exception oError = null;
            if (this.pgrACCOUNTS.SelectedObject != null)
            {
                try
                {
                    Riepiloghi.clsAccount account = ((clsViewAccount)this.pgrACCOUNTS.SelectedObject).Account;
                    if (account != null)
                    {
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        bool result = this.SendMailAccountCreateRenew(account.AccountId, "RIEPILOGHI_WEB", this.RP_PROPERTIES.PUBLIC_URL, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (!result && oError == null)
                            oError = new Exception("Errore di reimpostazione password e spedizione all'acount");
                        if (oError != null)
                            MessageBox.Show("Salvataggio account" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void BtnACCOUNTSadd_Click(object sender, EventArgs e)
        {
            // Aggiungi un account
            string nome = "";
            string cognome = "";
            string email = "";
            long profileId = 0;
            
            Exception oError = null;
            if (ListaProfili != null && ListaProfili.Count > 0)
            {
                List<ConfigRiepiloghi.clsItemGenericData> profiliSelezionabili = new List<ConfigRiepiloghi.clsItemGenericData>();
                ListaProfili.ForEach(p => profiliSelezionabili.Add(new ConfigRiepiloghi.clsItemGenericData(p.ProfileId.ToString(), p.Descrizione, ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeNumInt, p.ProfileId, false, true, "", null)));
                List<ConfigRiepiloghi.clsItemGenericData> data = new List<ConfigRiepiloghi.clsItemGenericData>();
                data.Add(new ConfigRiepiloghi.clsItemGenericData("NOME", "Nome", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                data.Add(new ConfigRiepiloghi.clsItemGenericData("COGNOME", "Cognome", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                data.Add(new ConfigRiepiloghi.clsItemGenericData("EMAIL", "Email", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "", false, true));
                data.Add(new ConfigRiepiloghi.clsItemGenericData("PROFILEID", "Profilo", ConfigRiepiloghi.clsItemGenericData.EnumGenericDataType.TypeString, "", false, true, "", profiliSelezionabili));

                ConfigRiepiloghi.frmGetGenericData frmGetNewAccount = new ConfigRiepiloghi.frmGetGenericData("Account", data);

                if (frmGetNewAccount.ShowDialog() == DialogResult.OK)
                {
                    foreach (ConfigRiepiloghi.clsItemGenericData item in frmGetNewAccount.Data)
                    {
                        if (item.Code == "NOME")
                            nome = item.Value.ToString();
                        else if (item.Code == "COGNOME")
                            cognome = item.Value.ToString();
                        else if (item.Code == "EMAIL")
                            email = item.Value.ToString();
                        else if (item.Code == "PROFILEID")
                        {
                            
                            foreach (ConfigRiepiloghi.clsItemGenericData dataItem in profiliSelezionabili)
                            {
                                if (item.Value.ToString() == dataItem.Description)
                                {
                                    profileId = long.Parse(dataItem.Value.ToString());
                                    break;
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(cognome) && !string.IsNullOrEmpty(email) && profileId > 0)
                    {
                        Riepiloghi.clsAccount account = this.CreateAccount(this.AccountLoggato.AccountId, this.RP_PROPERTIES.PUBLIC_URL, email, nome, cognome, profileId, out oError);
                        if (oError == null && account != null)
                        {
                            this.InitForm();
                        }
                        else if (oError != null)
                            MessageBox.Show("Creazione account" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void DgvACCOUNTS_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvACCOUNTS.SelectedRows.Count > 0)
            {
                clsViewAccount viewAccount = (clsViewAccount)this.dgvACCOUNTS.SelectedRows[0].DataBoundItem;
                this.pgrACCOUNTS.SelectedObject = viewAccount;
            }
        }

        #region Profilo selezionato

        private void DgvProfiles_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvPROFILES.SelectedRows.Count > 0)
            {
                clsViewProfile viewProfile = (clsViewProfile)this.dgvPROFILES.SelectedRows[0].DataBoundItem;
                if (!this.loading)
                {
                    this.lastProfileSelected = viewProfile;
                    this.lastCodiceSistemaSelected = null;
                    this.lastCodiceLocaleSelected = null;
                    this.lastOrganizzatoreSelected = null;
                }
                this.pgrPROFILES.SelectedObject = viewProfile;
                SelectedProfile = viewProfile.Profile;
            }
        }
        private void lblProfiloSelected_Click(object sender, EventArgs e)
        {
            SelectedProfile = null;
        }

        private void FrmConfigMulti_InitProfileEvent(Riepiloghi.clsProfile profile)
        {
            InitProfile(profile);
        }

        private void InitProfile(Riepiloghi.clsProfile profile)
        {

            if (profile != null)
            {
                this.lblValProfiloSelezionato.Text = profile.Descrizione;

                if (CodiciSistemaEnabled)
                {
                    this.bindCodiciSistema = new BindingSource();
                    List<clsViewCodiceSistema> lista = new List<clsViewCodiceSistema>();
                    foreach (Riepiloghi.clsCodiceSistema codiceSistema in profile.CodiciSistema)
                        lista.Add(new clsViewCodiceSistema(codiceSistema));
                    this.bindCodiciSistema.DataSource = lista;
                    this.dgvCODICI_SISTEMA.DataSource = this.bindCodiciSistema;
                    this.dgvCODICI_SISTEMA.ClearSelection();
                }

                if (CodiciSistemaEnabled)
                {
                    this.bindCodiciLocale = new BindingSource();
                    this.bindOrganizzatori = new BindingSource();
                    List<clsViewCodiceLocale> listaCL = new List<clsViewCodiceLocale>();
                    List<clsViewOrganizzatore> listaCF = new List<clsViewOrganizzatore>();
                    foreach (Riepiloghi.clsCodiceSistema codiceSistema in profile.CodiciSistema)
                    {
                        foreach (Riepiloghi.clsCodiceLocale codiceLocale in codiceSistema.CodiciLocale)
                        {
                            listaCL.Add(new clsViewCodiceLocale(codiceLocale));
                        }

                        foreach (Riepiloghi.clsCFOrganizzatore organizzatore in codiceSistema.CFOrganizzatori)
                        {
                            listaCF.Add(new clsViewOrganizzatore(organizzatore));
                        }
                    }
                    this.bindCodiciLocale.DataSource = listaCL;
                    this.dgvCODICI_LOCALI.DataSource = this.bindCodiciLocale;
                    this.dgvCODICI_LOCALI.ClearSelection();
                    this.bindOrganizzatori.DataSource = listaCF;
                    this.dgvCF_ORGANIZZATORI.DataSource = this.bindOrganizzatori;
                    this.dgvCF_ORGANIZZATORI.ClearSelection();
                }
            }
            else
            {
                if (this.bindCodiciSistema != null)
                    this.bindCodiciSistema.DataSource = null;
                this.bindCodiciSistema = null;
                this.dgvCODICI_SISTEMA.DataSource = null;

                if (this.bindCodiciLocale != null)
                    this.bindCodiciLocale.DataSource = null;
                this.bindCodiciLocale = null;
                this.dgvCODICI_LOCALI.DataSource = null;
            }
        }

        #region codici sistema del profilo selezionato

        private void DgvCODICI_SISTEMA_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvCODICI_SISTEMA.SelectedRows.Count > 0)
            {
                clsViewCodiceSistema viewCodiceSistema = (clsViewCodiceSistema)this.dgvCODICI_SISTEMA.SelectedRows[0].DataBoundItem;
                this.pgrCODICI_SISTEMA.SelectedObject = viewCodiceSistema;

                if (this.lastCodiceSistemaSelected == null || this.lastCodiceSistemaSelected.CodiceSistema.CodiceSistema != viewCodiceSistema.CodiceSistema.CodiceSistema)
                {
                    if (!this.loading)
                    {
                        this.lastCodiceSistemaSelected = viewCodiceSistema;
                    }
                    this.pnlCONFIG.Controls.Clear();
                    this.tabCONFIG.Text = string.Format("Configurazione {0}", viewCodiceSistema.Codice);
                    ConfigRiepiloghi.usrProprietaRiepiloghi itemProprieta = null;

                    List<Riepiloghi.clsProprietaRiepiloghi> listaProprieta = null;
                    Exception oError = null;

                    listaProprieta = this.GetListaProprietaRiepiloghi(viewCodiceSistema.CodiceSistema.CodiceSistema, out oError);
                    if (oError != null)
                        MessageBox.Show(oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    if (listaProprieta != null && listaProprieta.Count > 0)
                    {
                        Riepiloghi.clsProprietaRiepiloghi proprietaTitle = new Riepiloghi.clsProprietaRiepiloghi();
                        proprietaTitle.Descrizione = "Proprietà del Codice sistema selezionato";
                        proprietaTitle.Valore = viewCodiceSistema.Codice;
                        proprietaTitle.Proprieta = "CODICE_SISTEMA";
                        itemProprieta = new ConfigRiepiloghi.usrProprietaRiepiloghi();
                        itemProprieta.Proprieta = proprietaTitle;
                        itemProprieta.Dock = DockStyle.Top;
                        itemProprieta.ReadOnly = true;

                        this.pnlCONFIG.Controls.Add(itemProprieta);
                        itemProprieta.BringToFront();

                        foreach (Riepiloghi.clsProprietaRiepiloghi proprietaRiepiloghi in listaProprieta)
                        {
                            itemProprieta = new ConfigRiepiloghi.usrProprietaRiepiloghi();
                            itemProprieta.Proprieta = proprietaRiepiloghi;
                            itemProprieta.Dock = DockStyle.Top;
                            itemProprieta.ReadOnly = true;
                            this.pnlCONFIG.Controls.Add(itemProprieta);
                            itemProprieta.BringToFront();
                        }
                    }
                }
                if (!this.loading)
                {
                    this.lastCodiceSistemaSelected = viewCodiceSistema;
                    this.lastCodiceLocaleSelected = null;
                    this.lastOrganizzatoreSelected = null;
                }
            }
        }

        private void PgrCODICI_SISTEMA_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (this.pgrPROFILES.SelectedObject != null && this.pgrCODICI_SISTEMA.SelectedObject != null && 
                (e.ChangedItem.PropertyDescriptor.Name == "Enabled" || e.ChangedItem.PropertyDescriptor.Name == "AllCodiciLocali" || e.ChangedItem.PropertyDescriptor.Name == "AllCFOrganizzatori"))
            {
                try
                {
                    Riepiloghi.clsProfile profile = null;
                    profile = ((clsViewProfile)this.pgrPROFILES.SelectedObject).Profile;

                    if (profile != null && !profile.AllCodiciSistema)
                    {
                        Exception oError = null;
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        profile = this.SetProfile(profile, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (oError != null)
                            MessageBox.Show("Salvataggio profilo (Codice sistema)" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        #endregion

        #region codici locali del profilo selezionato

        private void DgvCODICI_LOCALI_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvCODICI_LOCALI.SelectedRows.Count > 0)
            {
                clsViewCodiceLocale viewCodiceLocale = (clsViewCodiceLocale)this.dgvCODICI_LOCALI.SelectedRows[0].DataBoundItem;
                if (!this.loading)
                    this.lastCodiceLocaleSelected = viewCodiceLocale;
                this.pgrCODICI_LOCALI.SelectedObject = viewCodiceLocale;
            }
        }

        private void PgrCODICI_LOCALI_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (this.pgrPROFILES.SelectedObject == null) MessageBox.Show("Selezionare un profile modificabile.");
            else if (this.pgrCODICI_SISTEMA.SelectedObject == null) MessageBox.Show("Selezionare un codice sistema.");
            else if (this.pgrCODICI_LOCALI.SelectedObject == null) MessageBox.Show("Selezionare un codice locale.");
            if (this.pgrPROFILES.SelectedObject != null && this.pgrCODICI_SISTEMA.SelectedObject != null && this.pgrCODICI_LOCALI.SelectedObject != null && e.ChangedItem.PropertyDescriptor.Name == "Enabled")
            {
                try
                {
                    Riepiloghi.clsProfile profile = null;
                    profile = ((clsViewProfile)this.pgrPROFILES.SelectedObject).Profile;
                    Riepiloghi.clsCodiceSistema codiceSistema = ((clsViewCodiceSistema)this.pgrCODICI_SISTEMA.SelectedObject).CodiceSistema;
                    if (profile != null && !codiceSistema.AllCodiciLocali)
                    {
                        Exception oError = null;
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        profile = this.SetProfile(profile, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (oError != null)
                            MessageBox.Show("Salvataggio profilo (Codici locali)" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region organizzatori del profilo selezionato

        private void DgvCF_ORGANIZZATORI_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvCF_ORGANIZZATORI.SelectedRows.Count > 0)
            {
                clsViewOrganizzatore viewOrganizzatore = (clsViewOrganizzatore)this.dgvCF_ORGANIZZATORI.SelectedRows[0].DataBoundItem;
                if (!this.loading)
                    this.lastOrganizzatoreSelected = viewOrganizzatore;
                this.pgrCF_ORGANIZZATORI.SelectedObject = viewOrganizzatore;
            }
        }

        private void PgrCF_ORGANIZZATORI_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (this.pgrPROFILES.SelectedObject == null) MessageBox.Show("Selezionare un profile modificabile.");
            else if (this.pgrCODICI_SISTEMA.SelectedObject == null) MessageBox.Show("Selezionare un codice sistema.");
            else if (this.pgrCF_ORGANIZZATORI.SelectedObject == null) MessageBox.Show("Selezionare un organizzatore.");
            if (this.pgrPROFILES.SelectedObject != null && this.pgrCODICI_SISTEMA.SelectedObject != null && this.pgrCF_ORGANIZZATORI.SelectedObject != null && e.ChangedItem.PropertyDescriptor.Name == "Enabled")
            {
                try
                {
                    Riepiloghi.clsProfile profile = null;
                    profile = ((clsViewProfile)this.pgrPROFILES.SelectedObject).Profile;
                    Riepiloghi.clsCodiceSistema codiceSistema = ((clsViewCodiceSistema)this.pgrCODICI_SISTEMA.SelectedObject).CodiceSistema;
                    if (profile != null && !codiceSistema.AllCFOrganizzatori)
                    {
                        Exception oError = null;
                        ConfigRiepiloghi.frmWait oFrmWait = new ConfigRiepiloghi.frmWait(false);
                        oFrmWait.Message = "Salvataggio...";
                        profile = this.SetProfile(profile, out oError);
                        oFrmWait.Close();
                        oFrmWait.Dispose();
                        this.InitForm();
                        if (oError != null)
                            MessageBox.Show("Salvataggio profilo (Organizzatori)" + "\r\n" + oError.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        #endregion

        #endregion

    }
}
