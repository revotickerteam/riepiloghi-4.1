﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Le informazioni generali relative a un assembly sono controllate dal seguente
// set di attributi. Modificare i valori di questi attributi per modificare le informazioni
// associate a un assembly.
[assembly: AssemblyTitle("WebApiRiepiloghi")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WebApiRiepiloghi")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Se si imposta il valore di ComVisible su falso, i tipi nell'assembly non sono più visibili
// dai componenti COM. Se è necessario accedere al tipo nell'assembly da
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID seguente verrà utilizzato per creare l'ID di typelib
[assembly: Guid("4d46b11d-fec0-4f63-a3ba-e725e80ca3b7")]

// Le informazioni sulla versione di un assembly sono costituite dai quattro valori seguenti:
//
//      Versione principale
//      Versione secondaria
//      Numero build
//      Revisione
//
// È possibile specificare tutti i valori o lasciare i valori predefiniti per Revisione e Numeri build
// utilizzando l'asterisco (*) come illustrato di seguito:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
