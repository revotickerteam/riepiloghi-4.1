using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

//using Db.DataAccess.Client;
//using Db.DataAccess.Types;

namespace RcLib
{
    /// <summary>
    ///     Descrizione di riepilogo per Class1.
    /// </summary>

    #region DB

    //															*--------------------------------------*
    // ------------------------------------------*	DBMap											*
    //															*--------------------------------------*
    public class DbMapTable
    {
        private Dictionary<string, string> _aAliases;
        private DataRow _aRow;
        private DbCommand _command;


        public DbMapTable(DatabaseManager parDatabaseManager)
        {
            TableName = string.Empty;
            PrefixParameter = String.Empty;
            FormatParameter = ":{2}";
            DatabaseManager = parDatabaseManager;

            Fields = new DbMapFieldDictionary();
            Relations = new List<DbMapRelation>();
            DataSet = new DataSet();
            Encoding = new UTF8Encoding();
            _aAliases = null;
        }

        public DbMapTable()
            : this(null)
        {
        }

        public string TableName { get; set; }
        public string PrefixParameter { get; set; }
        public string FormatParameter { get; set; }
        public UTF8Encoding Encoding { get; set; }
        public DatabaseManager DatabaseManager { get; private set; }
        public DataSet DataSet { get; set; }

        public List<DbMapRelation> Relations { get; set; }

        public Dictionary<String, List<String>> QueryGroups { get; set; }
        public Dictionary<String, List<String>> CommandGroups { get; set; }
        public string ExceptionError { get; set; }

        public DataRow Row
        {
            get { return _aRow; }
            set
            {
                _aRow = value;
                SetParametersFromRow();
                OnChangeRow();
            }
        }

        public DbCommand Command
        {
            get { return _command ?? (_command = DatabaseManager.CreateCommand()); }
        }

        public DbMapFieldDictionary Fields { get; private set; }

        public Dictionary<string, string> Aliases
        {
            get
            {
                if (_aAliases != null) return _aAliases;

                _aAliases = new Dictionary<string, string>();

                foreach (DbMapField dbField in Fields.Values)
                {
                    _aAliases.Add(dbField.FieldName, dbField.Alias);
                }

                return _aAliases;
            }
            set
            {
                _aAliases = value;
            }
        }

        ~DbMapTable()
        {
            Command.Dispose();
            DataSet.Dispose();
        }

        public object GetValueFromCommand(string parTableName, bool parBuildParameters)
        {
            object result = null;

            if (parBuildParameters) BuildProcedureCommand();

            result = Command.ExecuteScalar();

            return result;
        }

        public void ExecuteProcedureCommand(DbConnection parConnection, DbTransaction parTransaction)
        {
            ExecuteProcedureCommand(parConnection, parTransaction, true);
        }
        public void ExecuteProcedureCommand(DbConnection parConnection, DbTransaction parTransaction, ref string errorMessage)
        {
            ExecuteProcedureCommand(parConnection, parTransaction, true, ref errorMessage);
        }

        public void ExecuteProcedureCommand(DbConnection parConnection, DbTransaction parTransaction,bool parBuildParameters)
        {
            if (parBuildParameters) BuildProcedureCommand();

            Command.Connection = parConnection;
            Command.Transaction = parTransaction;

            var wCursors = new List<DbMapFieldCursor>();

            if (HasCursors(ref wCursors))
            {
                var da = DatabaseManager.CreateDataAdapter(Command);

                try
                {
                    da.Fill(DataSet);

                    if (DataSet.Tables.Count != wCursors.Count) return;

                    var i = 0;

                    foreach (var wCursor in wCursors)
                    {
                        DataSet.Tables[i].TableName = wCursor.CursorName;

                        wCursor.IsSingleRow = wCursor.IsSingleRow;

                        i++;
                    }

                    BuidRelations();
                }
                catch(Exception ex)
                {
                    string error = ex.Message;
                    
                }
                finally
                {
                    da.Dispose();
                }
            }
            else
            {
                Command.ExecuteNonQuery();
            }
        }
        public void ExecuteProcedureCommand(DbConnection parConnection, DbTransaction parTransaction,bool parBuildParameters, ref string errorMessage)
        {
            if (parBuildParameters) BuildProcedureCommand();

            Command.Connection = parConnection;
            Command.Transaction = parTransaction;

            var wCursors = new List<DbMapFieldCursor>();

            if (HasCursors(ref wCursors))
            {
                var da = DatabaseManager.CreateDataAdapter(Command);

                try
                {
                    da.Fill(DataSet);

                    if (DataSet.Tables.Count != wCursors.Count) return;

                    var i = 0;

                    foreach (var wCursor in wCursors)
                    {
                        DataSet.Tables[i].TableName = wCursor.CursorName;

                        wCursor.IsSingleRow = wCursor.IsSingleRow;

                        i++;
                    }

                    BuidRelations();
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
                finally
                {
                    da.Dispose();
                }
            }
            else
            {
                Command.ExecuteNonQuery();
            }
        }
        public void ExecuteCommands(DbConnection parConnection, DbTransaction parTransaction, String parCommandGroup)
        {
            var wCommands = new List<DbMapFieldCommand>();

            if (!HasCommands(ref wCommands, parCommandGroup)) return;

            foreach (var wCommand in wCommands)
            {
                using (var cmd = DatabaseManager.CreateCommand(wCommand.CommandText, parConnection, parTransaction))
                {
                    foreach (var pn in wCommand.Parameters)
                    {
                        cmd.Parameters.Add(pn.Parameter);
                    }

                    wCommand.Value = cmd.ExecuteNonQuery();

                    cmd.Parameters.Clear();
                }
            }
        }

        public void ExecuteQueries(DbConnection parConnection, DbTransaction parTransaction, String parQueryGroup)
        {
            ExecuteQueries(parConnection, parTransaction, parQueryGroup, false);
        }
        public void ExecuteQueries(DbConnection parConnection, DbTransaction parTransaction, String parQueryGroup, Boolean parKeepAdapters)
        {
            var wQueries = new List<DbMapFieldQuery>();

            if (!HasQueries(ref wQueries, parQueryGroup)) return;

            foreach (var wQuery in wQueries)
            {
                using (var cmd = DatabaseManager.CreateCommand(wQuery.CommandText, parConnection, parTransaction))
                {
                    foreach (var pf in wQuery.Parameters)
                    {
                        cmd.Parameters.Add(pf.Parameter);
                    }

                    if (parKeepAdapters)
                    {
                        wQuery.Adapter = DatabaseManager.CreateDataAdapter(cmd);
                        wQuery.Adapter.Fill(wQuery.Value);
                        DataSet.Tables.Add(wQuery.Value);
                        wQuery.Value.TableName = wQuery.CursorName;
                    }
                    else
                    {
                        using (var da = DatabaseManager.CreateDataAdapter(cmd))
                        {
                            da.Fill(wQuery.Value);
                            DataSet.Tables.Add(wQuery.Value);
                            wQuery.Value.TableName = wQuery.CursorName;
                        }
                    }


                    cmd.Parameters.Clear();
                }
            }

            BuidRelations();

        }

        public void UpdateQueries(DbConnection parConnection, DbTransaction parTransaction, String parQueryGroup, Func<DataTable, DataSet, JObject, DataTable> parUpdateMethod, JObject parJObject)
        {
            var wQueries = new List<DbMapFieldQuery>();

            if (!HasQueries(ref wQueries, parQueryGroup)) return;

            foreach (var wQuery in wQueries.Where(q => !q.ReadOnly).ToList())
            {
                using (var cmd = DatabaseManager.CreateCommand(wQuery.CommandText, parConnection, parTransaction))
                {
                    foreach (var pf in wQuery.Parameters)
                    {
                        cmd.Parameters.Add(pf.Parameter);
                    }
                    using (var da = DatabaseManager.CreateDataAdapter(cmd))
                    {
                        using (var sc = DatabaseManager.CreateCommandBuilder(da))
                        {
                            da.Fill(wQuery.Value);
                            DataSet.Tables.Add(wQuery.Value);
                            wQuery.Value.TableName = wQuery.CursorName;
                            parUpdateMethod(wQuery.Value, DataSet, parJObject);
                            da.Update(wQuery.Value);
                        }

                    }

                    cmd.Parameters.Clear();
                }
            }

            BuidRelations();

        }

        public void DisposeAdapters(String parQueryGroup)
        {
            var wQueries = new List<DbMapFieldQuery>();

            if (!HasQueries(ref wQueries, parQueryGroup)) return;

            foreach (var wQuery in wQueries)
            {
                if (wQuery.Adapter != null)
                {
                    wQuery.Adapter.Dispose();
                    wQuery.Adapter = null;
                }
            }

        }
        public void SetNullQuery(String parQueryGroup)
        {
            var wQueries = new List<DbMapFieldQuery>();

            if (!HasQueries(ref wQueries, parQueryGroup)) return;

            foreach (var wQuery in wQueries)
            {
                if (wQuery.Value != null)
                {
                    wQuery.DisposeTable();
                }
            }

        }

        public void ExecuteQueries(DbConnection parConnection, DbTransaction parTransaction)
        {
            ExecuteQueries(parConnection, parTransaction, null);
        }

        public void BuildParametesCommand(string[] parFields)
        {
            Command.Parameters.Clear();

            foreach (var whereField in parFields)
            {
                if (Fields.Contains(whereField))
                {
                    Command.Parameters.Add(Fields[whereField].Parameter);
                }
                else
                {
                    throw (new Exception(String.Format("BuildParametesCommand: Campo inesistente {0}", whereField)));
                }
            }
        }

        public void BuildProcedureCommand()
        {
            Command.Parameters.Clear();
            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.ForParameter) Command.Parameters.Add(dbField.Parameter);
            }
        }

        public void BuildInsertCommand()
        {
            var separator = string.Empty;

            var sbColumns = new StringBuilder(1024);
            var sbParameter = new StringBuilder(1024);

            Command.Parameters.Clear();

            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.ForInsert)
                {
                    Command.Parameters.Add(dbField.Parameter);
                    sbColumns.AppendFormat("{0}{1}", separator, dbField.FieldName);
                    sbParameter.AppendFormat("{0}" + FormatParameter, separator, dbField.FieldName,
                        dbField.Parameter.ParameterName);
                    separator = ",";
                }
            }

            Command.CommandText = String.Format("insert into {0} ({1}) values ({2})", TableName, sbColumns,
                sbParameter);
        }

        public void BuildUpdateCommand()
        {
            var setSeparator = string.Empty;
            var whereSeparator = string.Empty;

            var sbSet = new StringBuilder(1024);
            var sbWhere = new StringBuilder(1024);

            Command.Parameters.Clear();

            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.ForUpdate)
                {
                    Command.Parameters.Add(dbField.Parameter);
                    sbSet.AppendFormat("{0} {1} = " + FormatParameter, setSeparator, dbField.FieldName,
                        dbField.Parameter.ParameterName);
                    setSeparator = ",";
                }
            }

            foreach (var dbFieldName in Fields.Keys)
            {
                var dbField = Fields[dbFieldName];

                if (dbField.ForWhere)
                {
                    Command.Parameters.Add(dbField.Parameter);
                    sbWhere.AppendFormat("{0} {1} = " + FormatParameter + " ", whereSeparator, dbField.FieldName,
                        dbField.Parameter.ParameterName);
                    whereSeparator = "and";
                }
            }

            Command.CommandText = String.Format("update {0} set {1} where {2}", TableName, sbSet,
                sbWhere);
        }

        public void BuildSelectCommand()
        {
            var columnsSeparator = string.Empty;
            var whereSeparator = string.Empty;

            var sbColumns = new StringBuilder(1024);
            var sbWhere = new StringBuilder(1024);

            Command.Parameters.Clear();

            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.ForSelect)
                {
                    sbColumns.AppendFormat("{0}{1}", columnsSeparator, dbField.FieldName,
                        dbField.Parameter.ParameterName);
                    columnsSeparator = ",";
                }
            }

            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.ForWhere)
                {
                    Command.Parameters.Add(dbField.Parameter);
                    sbWhere.AppendFormat("{0} {1} = " + FormatParameter + " ", whereSeparator, dbField.FieldName,
                        dbField.Parameter.ParameterName);
                    whereSeparator = "and";
                }
            }

            Command.CommandText = String.Format("select {1} from {0} where {2}", TableName, sbColumns,
                sbWhere);
        }

        public void BuildDeleteCommand(string[] parWhereFields)
        {
            var whereFields = new List<DbMapField>();

            if (parWhereFields.Length == 0)
            {
                throw (new Exception("BuildDeleteCommand: Nessuna condizione di filtro per il comando"));
            }

            foreach (var whereField in parWhereFields)
            {
                if (Fields.Contains(whereField))
                {
                    whereFields.Add(Fields[whereField]);
                }
                else
                {
                    throw (new Exception(String.Format("BuildDeleteCommand: Campo inesistente {0}", whereField)));
                }
            }

            var whereSeparator = string.Empty;

            var sbWhere = new StringBuilder(1024);

            Command.Parameters.Clear();

            foreach (var dbField in whereFields)
            {
                Command.Parameters.Add(dbField.Parameter);
                sbWhere.AppendFormat("{0} {1} = " + FormatParameter + " ", whereSeparator, dbField.FieldName,
                    dbField.Parameter.ParameterName);
                whereSeparator = "and";
            }

            Command.CommandText = String.Format("delete from {0} where {1}", TableName, sbWhere);
        }

        public void SetDefaultValuesForNulls()
        {
            foreach (DbMapField dbField in Fields.Values)
            {
                if (dbField.IsNull) dbField.SetDefaultValue();
            }
        }

        public void SetDefaultValues()
        {
            foreach (DbMapField dbField in Fields.Values)
            {
                dbField.SetDefaultValue();
            }
        }
        public void SetDbNullValues()
        {
            foreach (DbMapField dbField in Fields.Values)
            {
                dbField.SetDbNullValue();
            }
        }

        public void SetParametersFromRow()
        {
            foreach (DbMapField dbField in Fields.Values)
            {
                dbField.SetParameterFromColumn();
            }
        }

        public void AddRelation(DbMapFieldCursor parParentTable, DbMapFieldCursor parChildTable,
            string parParentColumnNames, string parChildColumnNames, bool parNested)
        {
            var r = new DbMapRelation()
            {
                ParentTable = parParentTable,
                ChildTable = parChildTable,
                ParentColumns = parParentColumnNames.Split(',').ToList(),
                ChildColumns = parChildColumnNames.Split(',').ToList(),
                Nested = parNested
            };

            Relations.Add(r);

        }

        public void BuidRelations()
        {

            foreach (var relation in Relations)
            {
                var relationName = String.Format("{0}-{1}", relation.ParentTable.FieldName, relation.ChildTable.FieldName);

                if (!DataSet.Relations.Contains(relationName) &&
                    DataSet.Tables.Contains(relation.ParentTable.CursorName) &&
                    DataSet.Tables.Contains(relation.ChildTable.CursorName))
                {
                    var parentColumns = relation.ParentColumns.Select(c => relation.ParentTable.Value.Columns[c]).ToArray();
                    var childColumns = relation.ChildColumns.Select(c => relation.ChildTable.Value.Columns[c]).ToArray();

                    try
                    {
                        var r = new DataRelation(relationName, parentColumns, childColumns, relation.Nested);

                        DataSet.Relations.Add(r);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("Relation {0} - {1}", relationName, e.Message));

                    }
                }

            }


        }

        public T CastToObject<T>() where T : new()
        {
            return Utility.ToObject<T>(Utility.DataToDictionary(DataSet, true, true, Aliases));
        }
        public T CastToObject<T>(bool parRelations, bool parAllColumns) where T : new()
        {
            return Utility.ToObject<T>(Utility.DataToDictionary(DataSet, parRelations, parAllColumns, Aliases));
        }

        public virtual void OnChangeRow()
        {
        }

        public virtual string HtmlOptionValue()
        {
            var result = string.Empty;
            return result;
        }

        public virtual string HtmlOptionText()
        {
            var result = string.Empty;
            return result;
        }

        public Boolean HasCursors(ref List<DbMapFieldCursor> parCursors)
        {
            parCursors.AddRange(from DbMapField dbField in Fields.Values
                                where dbField.GetType() == typeof(DbMapFieldCursor)
                                select (dbField as DbMapFieldCursor));

            return parCursors.Count > 0;
        }


        public Boolean HasQueries(ref List<DbMapFieldQuery> parTables, String parQueryGroup)
        {

            if (QueryGroups.ContainsKey(parQueryGroup))
            {
                foreach (var queryName in QueryGroups[parQueryGroup])
                {
                    if (Fields.Contains(queryName))
                    {
                        var dbField = Fields[queryName];
                        if (dbField.GetType() == typeof(DbMapFieldQuery))
                        {
                            parTables.Add(dbField as DbMapFieldQuery);
                        }

                    }
                }
            }

            return parTables.Count > 0;
        }

        public Boolean HasCommands(ref List<DbMapFieldCommand> parCommands, String parCommandGroup)
        {

            if (CommandGroups.ContainsKey(parCommandGroup))
            {
                foreach (var commandName in CommandGroups[parCommandGroup])
                {
                    if (Fields.Contains(commandName))
                    {
                        var dbField = Fields[commandName];
                        if (dbField.GetType() == typeof(DbMapFieldCommand))
                        {
                            parCommands.Add(dbField as DbMapFieldCommand);
                        }

                    }
                }
            }

            return parCommands.Count > 0;
        }

        public string GetAlias(string parName)
        {
            return Aliases != null && Aliases.ContainsKey(parName) ? Aliases[parName] : parName;
        }

        private bool IsSingleRow(DataTable parDataTable)
        {
            var result = false;

            if (parDataTable.ExtendedProperties.ContainsKey("single"))
            {
                result = (bool)parDataTable.ExtendedProperties["single"];
            }
            return result;
        }
    }

    public class DbMapField
    {
        public DbMapField(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
        {
            DbMapTable = parDbMapTable;
            FieldName = parFieldName;
            Label = parLabel;
            Alias = parAlias;
            Parameter = parDbMapTable.DatabaseManager.CreateParameter();
            Parameter.ParameterName = string.Format("{0}{1}", DbMapTable.PrefixParameter, parFieldName);
            Parameter.Value = null;
            Type = typeof(Object);
            DbMapTable.Fields.Add(parFieldName, this);
            ForSelect = true;
            ForUpdate = true;
            ForInsert = true;
            ForWhere = false;
            ForParameter = true;
            Required = false;

            Size = -1;
            ForeignKeyFields = new DbMapFieldDictionary();
        }

        public DbMapField(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        protected DbMapTable DbMapTable { get; private set; }
        public string FieldName { get; private set; }
        public string Alias { get; set; }
        public int Size { get; set; }
        public string DisplayFormat { get; set; }
        public string Label { get; set; }
        public string ForeignKeyCommandText { get; set; }
        public DbMapFieldDictionary ForeignKeyFields { get; private set; }

        public virtual string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : ObjectValue.ToString().Trim(); }
        }

        public bool IsNull
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? Convert.IsDBNull(DbMapTable.Row[FieldName])
                    : (Parameter.Value == null || Parameter.Value == DBNull.Value);
            }
        }

        public object ObjectValue
        {
            get { return (DbMapTable.Row != null) ? DbMapTable.Row[FieldName] : Parameter.Value; }
            set
            {
                if (DbMapTable.Row != null)
                {
                    if (DbMapTable.Row.Table.Columns.Contains(FieldName))
                    {
                        DbMapTable.Row[FieldName] = value;
                    }
                }
                Parameter.Value = value;
            }
        }

        public object DefaultValue { get; set; }
        public Type Type { get; set; }
        public DbParameter Parameter { get; private set; }

        public string FormatValue
        {
            get
            {
                var result = string.Empty;

                if (!IsNull)
                {
                    result = DbMapTable.Row[FieldName].ToString();
                }

                return result;
            }
            set
            {
                object pValue = null;

                pValue = Convert.ChangeType(value, Type);

                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = pValue;

                Parameter.Value = pValue;
            }
        }

        public string FormatValueOrForceDefault
        {
            set
            {
                object pValue = null;

                try
                {
                    pValue = Convert.ChangeType(value, Type);
                }
                catch
                {
                    pValue = DefaultValue;
                }

                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = pValue;

                Parameter.Value = pValue;
            }
        }

        public bool ForParameter { get; set; }
        public bool ForSelect { get; set; }
        public bool ForUpdate { get; set; }
        public bool ForInsert { get; set; }
        public bool ForWhere { get; set; }
        public bool Required { get; set; }

        public bool CheckAndMove(string parValue, StringBuilder parMessage)
        {
            return CheckAndMove(parValue, parMessage, DisplayFormat);
        }

        public bool CheckAndMove(string parValue, StringBuilder parMessage, string parFormat)
        {
            var result = true;

            object pValue = null;

            if (parValue.Trim().Length == 0)
            {
                if (Required)
                {
                    parMessage.AppendFormat("<li><b>{0}</b>: Campo obbligatorio.</li>", Label);
                    result = false;
                }
                else
                {
                    SetDefaultValue();
                }
            }
            else
            {
                try
                {
                    if (Size > 0 && parValue.Length > Size)
                    {
                        parMessage.AppendFormat("<li><b>{0}</b>: Lunghezza contenuto superiore a {1}.</li>", Label,
                            Size);
                        result = false;
                    }
                    else
                    {
                        if (GetType() == typeof(DbMapFieldBinary))
                        {
                            pValue = parValue;
                        }
                        else if (GetType() == typeof(DbMapFieldDateTime))
                        {
                            if (String.IsNullOrEmpty(parValue))
                            {
                                pValue = null;
                            }
                            else
                            {
                                pValue = DateTime.ParseExact(parValue, parFormat, CultureInfo.InvariantCulture);
                            }
                        }
                        else
                        {
                            pValue = Convert.ChangeType(parValue, Type);
                        }

                        if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = pValue;

                        Parameter.Value = pValue;
                    }
                }
                catch
                {
                    parMessage.AppendFormat("<li><b>{0}</b>: Formato campo errato.</li>", Label);
                    result = false;
                }
                if (result)
                {
                    if (!ForeignKeyCheck())
                    {
                        parMessage.AppendFormat("<li><b>{0}</b>: Campo non codificato.</li>", Label);
                        result = false;
                    }
                }
            }

            return result;
        }

        public string SetWhereFilter()
        {
            return SetWhereFilter(string.Empty);
        }

        public string SetWhereFilter(string parPrefix)
        {
            var result = string.Empty;

            if (Parameter.Value != null)
            {
                result = String.Format(" AND {0}{1} = ? ", parPrefix, FieldName);
                DbMapTable.Command.Parameters.Add(Parameter);
            }

            return result;
        }

        public void SetDefaultValue()
        {
            ObjectValue = DefaultValue;
        }
        public void SetDbNullValue()
        {
            ObjectValue = DBNull.Value;
        }

        public void SetParameterFromColumn()
        {
            if (DbMapTable.Row != null)
            {
                if (DbMapTable.Row.Table.Columns.Contains(FieldName))
                {
                    ObjectValue = ObjectValue;
                }
            }
        }

        public bool ForeignKeyCheck()
        {
            var result = true;

            if (ForeignKeyFields.Count > 0)
            {
                DbMapTable.Command.CommandText = ForeignKeyCommandText;
                DbMapTable.Command.Parameters.Clear();
                foreach (DbMapField foreignKeyField in ForeignKeyFields.Values)
                {
                    DbMapTable.Command.Parameters.Add(foreignKeyField.Parameter);
                }
                result = Convert.ToInt32(DbMapTable.Command.ExecuteScalar()) > 0;
            }

            return result;
        }

        public void SetForeignKeyRules(string parForeignKeyCommandText, DbMapField[] parForeignKeyFields)
        {
            ForeignKeyCommandText = parForeignKeyCommandText;

            ForeignKeyFields.Clear();

            foreach (var foreignKeyField in parForeignKeyFields)
            {
                ForeignKeyFields.Add(foreignKeyField.FieldName, foreignKeyField);
            }
        }

        public void SetAsGhost()
        {
            ForSelect = false;
            ForInsert = false;
            ForUpdate = false;
            ForWhere = false;
            Required = false;
        }

        public DbMapFieldQuery AsQuery
        {
            get
            {
                return (DbMapFieldQuery)(this);
            }
        }
        public DbMapFieldDateTime AsDateTime
        {
            get
            {
                return (DbMapFieldDateTime)(this);
            }
        }

        public T ParameterAs<T>() where T : new()
        {
            return (T)(Object)Parameter;
        }

    }

    public class DbMapFieldString : DbMapField
    {
        public DbMapFieldString(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.String;
            Type = typeof(String);
            DefaultValue = string.Empty;
        }

        public DbMapFieldString(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public DbMapFieldString(DbMapTable parDbMapTable, string parFieldName, int parSize)
            : this(parDbMapTable, parFieldName)
        {
            Size = parSize;
            Parameter.Size = Size;
        }
        public void SetNullableValue(String parValue)
        {
            if (String.IsNullOrEmpty(parValue))
            {
                ObjectValue = DBNull.Value;
            }
            else
            {
                Value = parValue;
            }
        }
        public String Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (String)DbMapTable.Row[FieldName]
                    : (String)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }
    }

    public class DbMapFieldInt32 : DbMapField
    {
        public DbMapFieldInt32(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Int32;
            Type = typeof(Int32);
            DefaultValue = 0;
            DisplayFormat = "#";
        }

        public DbMapFieldInt32(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Int32 Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (Int32)DbMapTable.Row[FieldName]
                    : (Int32)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }

        public override string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : Value.ToString(DisplayFormat); }
        }
    }

    public class DbMapFieldInt16 : DbMapField
    {
        public DbMapFieldInt16(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Int16;
            Type = typeof(Int16);
            DefaultValue = 0;
            DisplayFormat = "#";
        }

        public DbMapFieldInt16(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Int16 Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (Int16)DbMapTable.Row[FieldName]
                    : (Int16)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }

        public override string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : Value.ToString(DisplayFormat); }
        }
    }

    public class DbMapFieldDecimal : DbMapField
    {
        public DbMapFieldDecimal(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Decimal;
            Type = typeof(Decimal);
            DefaultValue = 0;
            DisplayFormat = String.Empty;
        }

        public DbMapFieldDecimal(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Decimal Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? Convert.ToDecimal(DbMapTable.Row[FieldName])
                    : Convert.ToDecimal(Parameter.Value);
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }

        public override string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : Value.ToString(DisplayFormat); }
        }

        public Int32 AsInt32
        {
            get { return Convert.ToInt32(Value); }
        }

        public Boolean AsBoolean
        {
            get { return (ObjectValue != null) && Value.Equals(1); }
        }
    }

    public class DbMapFieldDouble : DbMapField
    {
        public DbMapFieldDouble(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Double;
            Type = typeof(Double);
            DefaultValue = 0;
            DisplayFormat = String.Empty;
        }

        public DbMapFieldDouble(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Double Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (Double)DbMapTable.Row[FieldName]
                    : (Double)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }

        public override string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : Value.ToString(DisplayFormat); }
        }
    }

    public class DbMapFieldDateTime : DbMapField
    {
        public DbMapFieldDateTime(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.DateTime;
            Type = typeof(DateTime);
            DefaultValue = null;
            DisplayFormat = "dd/MM/yyyy";
        }

        public DbMapFieldDateTime(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public DateTime Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (DateTime)DbMapTable.Row[FieldName]
                    : (DateTime)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }

        public void SetNullableValue(DateTime? parValue)
        {
            if (parValue.HasValue)
            {
                Value = parValue.Value;
            }
            else
            {
                ObjectValue = DBNull.Value;
            }
        }
        public override string DisplayValue
        {
            get { return (ObjectValue == null) ? string.Empty : Value.ToString(DisplayFormat); }
        }
    }

    public class DbMapFieldBoolean : DbMapField
    {
        public DbMapFieldBoolean(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Boolean;
            Type = typeof(Boolean);
            DefaultValue = false;
        }

        public DbMapFieldBoolean(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Boolean Value
        {
            get
            {
                return (DbMapTable.Row != null)
                    ? (Boolean)DbMapTable.Row[FieldName]
                    : (Boolean)Parameter.Value;
            }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }
    }

    public class DbMapFieldCursor : DbMapField
    {
        protected String _aCursorName = String.Empty;
        protected DataTable _aValue;
        protected bool _aIsSingleRow = false;

        public DbMapFieldCursor(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Object;
            Parameter.Direction = ParameterDirection.Output;
            Type = typeof(DataTable);
            DefaultValue = null;
            CursorName = parFieldName;
        }

        public DbMapFieldCursor(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public Boolean IsSingleRow
        {
            set
            {
                _aIsSingleRow = value;

                if (Value == null) return;

                if (Value.ExtendedProperties.ContainsKey("single"))
                {
                    Value.ExtendedProperties.Add("single", value);
                }
                else
                {
                    Value.ExtendedProperties["single"] = value;
                }
            }
            get
            {
                return _aIsSingleRow;
            }
        }

        public String CursorName
        {
            get { return _aCursorName; }
            set
            {
                if (Value != null)
                {
                    Value.TableName = value;
                }
                _aCursorName = value;
            }
        }

        public DataTable Value
        {
            get
            {
                if (_aValue == null)
                {
                    if (DbMapTable.DataSet.Tables.Contains(CursorName))
                    {
                        _aValue = DbMapTable.DataSet.Tables[CursorName];
                    }
                }
                return _aValue;
            }
        }
        public void DisposeTable()
        {
            if (_aValue != null)
            {
                _aValue.Dispose();
                _aValue = null;
            }
        }
    }

    public class DbMapFieldBlob : DbMapField
    {
        protected String _aBlobName = String.Empty;
        protected DataTable _aValue;
        
        public DbMapFieldBlob(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Object;
            Parameter.Direction = ParameterDirection.Output;
            Type = typeof(DataTable);
            DefaultValue = null;
            BlobName = parFieldName;
        }

        public DbMapFieldBlob(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public String BlobName
        {
            get { return _aBlobName; }
            set
            {
                if (Value != null)
                {
                    Value.TableName = value;
                }
                _aBlobName = value;
            }
        }

        public DataTable Value
        {
            get
            {
                if (_aValue == null)
                {
                    if (DbMapTable.DataSet.Tables.Contains(BlobName))
                    {
                        _aValue = DbMapTable.DataSet.Tables[BlobName];
                    }
                }
                return _aValue;
            }
        }
        public void DisposeTable()
        {
            if (_aValue != null)
            {
                _aValue.Dispose();
                _aValue = null;
            }
        }
    }

    public class DbMapFieldQuery : DbMapFieldCursor
    {
        public String CommandText = String.Empty;
        public Boolean ReadOnly = false;
        public List<DbMapField> Parameters = new List<DbMapField>();
        public DbDataAdapter Adapter = null;

        public DbMapFieldQuery(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Object;
            Parameter.Direction = ParameterDirection.Output;
            Type = typeof(DataTable);
            DefaultValue = null;
            CursorName = parFieldName;
            _aValue = new DataTable(CursorName);
        }

        public DbMapFieldQuery(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }




    }

    public class DbMapFieldCommand : DbMapField
    {
        private String _aCursorName = String.Empty;
        private int _aValue = -1;
        public String CommandText = String.Empty;
        //public List<String> Parameters = new List<String>();
        public List<DbMapField> Parameters = new List<DbMapField>();

        public DbMapFieldCommand(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Int32;
            Parameter.Direction = ParameterDirection.Output;
            Type = typeof(int);
            DefaultValue = -1;
        }

        public DbMapFieldCommand(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public int Value
        {
            get { return _aValue; }
            set { _aValue = value; }
        }
    }

    public class DbMapFieldBinary : DbMapField
    {
        public DbMapFieldBinary(DbMapTable parDbMapTable, string parFieldName, string parAlias, string parLabel)
            : base(parDbMapTable, parFieldName, parAlias, parLabel)
        {
            Parameter.DbType = DbType.Binary;
            Type = typeof(byte[]);
            DefaultValue = null;
        }

        public DbMapFieldBinary(DbMapTable parDbMapTable, string parFieldName)
            : this(parDbMapTable, parFieldName, parFieldName, parFieldName)
        {
        }

        public byte[] Value
        {
            get { return (DbMapTable.Row != null) ? (byte[])DbMapTable.Row[FieldName] : (byte[])Parameter.Value; }
            set
            {
                if (DbMapTable.Row != null) DbMapTable.Row[FieldName] = value;
                Parameter.Value = value;
            }
        }
    }

    public class DbMapFieldDictionary : ListDictionary
    {
        public new DbMapField this[object key]
        {
            get { return (DbMapField)base[key]; }
            set
            {
                if (Contains(key))
                {
                    throw new Exception(String.Format("Chiave duplicata in DBMapFieldDictionary : {0}", key));
                }
                base[key] = value;
            }
        }
    }

    public class DbMapRelation
    {
        public bool Nested = true;
        public DbMapFieldCursor ParentTable { get; set; }
        public DbMapFieldCursor ChildTable { get; set; }
        public List<string> ParentColumns { get; set; }
        public List<string> ChildColumns { get; set; }
    }

    public class DbMapSchemaField
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public object Default { get; set; }
    }

    public class DbMapSchemaQuery
    {
        public string Name { get; set; }
        public Boolean ReadOnly { get; set; }
        public string CommandText { get; set; }

        public string CommandFile { get; set; }
        public List<string> Parameters { get; set; }

        public bool IsSingleRow = false;

    }
    public class DbMapSchemaCommand
    {
        public string Name { get; set; }
        public string CommandText { get; set; }
        public string CommandFile { get; set; }
        public List<string> Parameters { get; set; }

    }
    public class DbMapSchemaColumn
    {
        public string Label { get; set; }
        public string Cell { get; set; }
        public string CssClass { get; set; }
        public string Badge { get; set; }
        public bool IsNumeric { get; set; }
        public string OrderBy { get; set; }
        public string Level { get; set; }
    }
    public class DbMapSchemaRelation
    {
        public bool Nested = true;
        public string ParentQuery { get; set; }
        public string ChildQuery { get; set; }
        public string ParentColumns { get; set; }
        public string ChildColumns { get; set; }
    }
    public class DbMapSchemaBrowse
    {
        public string Caption { get; set; }
        public List<DbMapSchemaColumn> Columns { get; set; }
    }
    public class DbMapSchema
    {
        public List<DbMapSchemaField> Fields = new List<DbMapSchemaField>();
        public List<DbMapSchemaQuery> Queries = new List<DbMapSchemaQuery>();
        public List<DbMapSchemaCommand> Commands = new List<DbMapSchemaCommand>();
        public List<DbMapSchemaRelation> Relations = new List<DbMapSchemaRelation>();
        public Dictionary<String, List<String>> QueryGroups = new Dictionary<string, List<string>>();
        public Dictionary<String, List<String>> CommandGroups = new Dictionary<string, List<string>>();
        public DbMapSchemaBrowse Browse { get; set; }
        public JObject Form { get; set; }
        public DbMapTable GetMapTable(DatabaseManager parDatabaseManager)
        {
            var result = new DbMapTable(parDatabaseManager);

            result.QueryGroups = QueryGroups ?? new Dictionary<string, List<string>>();
            result.CommandGroups = CommandGroups ?? new Dictionary<string, List<string>>();

            foreach (var field in Fields)
            {
                var mf = (DbMapField)Activator.CreateInstance(Type.GetType(String.Format("RcLib.{0}", field.Type)), result, field.Name);

                mf.ObjectValue = field.Default;
            }

            foreach (var command in Commands)
            {
                if (!String.IsNullOrEmpty(command.CommandFile))
                {
                    command.CommandText = File.ReadAllText(command.CommandFile);
                }

                new DbMapFieldCommand(result, command.Name)
                {
                    CommandText = command.CommandText,
                    Parameters = command.Parameters.Select(p => result.Fields[p]).ToList()
                };
            }

            foreach (var query in Queries)
            {
                if (!String.IsNullOrEmpty(query.CommandFile))
                {
                    query.CommandText = File.ReadAllText(query.CommandFile);
                }

                new DbMapFieldQuery(result, query.Name)
                {
                    CommandText = query.CommandText,
                    ReadOnly = query.ReadOnly,
                    Parameters = query.Parameters.Select(p => result.Fields[p]).ToList(),
                    IsSingleRow = query.IsSingleRow
                };
            }

            foreach (var relation in Relations)
            {
                result.AddRelation((DbMapFieldCursor)result.Fields[relation.ParentQuery],
                    (DbMapFieldCursor)result.Fields[relation.ChildQuery],
                    relation.ParentColumns,
                    relation.ChildColumns,
                    relation.Nested);
            }

            return result;
        }
        public static DbMapSchema Load(String parPath)
        {
            var json = File.ReadAllText(parPath);

            var result = JsonConvert.DeserializeObject<DbMapSchema>(json);

            return result;
        }

    }

    #endregion
}