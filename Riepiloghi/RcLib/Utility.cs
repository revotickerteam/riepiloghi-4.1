﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Encoder = System.Drawing.Imaging.Encoder;
using Formatting = Newtonsoft.Json.Formatting;
using Image = System.Drawing.Image;

namespace RcLib
{
    public class Utility
    {
        public static List<ActiveDirectoryAccount> GetActiveDirectoryAccounts(string parHost, string parFormatNetworkId)
        {
            var result = new List<ActiveDirectoryAccount>();

            var domainName = GetDomainFullName(parHost);

            using (var context = new PrincipalContext(ContextType.Domain, parHost))
            {

                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    result.AddRange(
                        searcher.FindAll()
                            .Select(r => r.GetUnderlyingObject() as DirectoryEntry)
                            .Select(de => new ActiveDirectoryAccount
                            {
                                SamAccountName = de.Properties.Contains("samAccountName") ? de.Properties["samAccountName"].Value.ToString() : string.Empty,
                                UserPrincipalName = de.Properties.Contains("userPrincipalName") ? de.Properties["userPrincipalName"].Value.ToString() : string.Empty,
                                Sn = de.Properties.Contains("sn") ? de.Properties["sn"].Value.ToString() : string.Empty,
                                GivenName = de.Properties.Contains("givenName") ? de.Properties["givenName"].Value.ToString() : string.Empty,
                                nETBIOSName = de.Properties.Contains("samAccountName") ? String.Format(parFormatNetworkId, de.Properties["samAccountName"].Value.ToString())  : string.Empty,
                                displayname = de.Properties.Contains("displayname") ? de.Properties["displayname"].Value.ToString() : string.Empty



                            }));
                }
            }
            return result;
        }

        private static string GetDomainFullName(string friendlyName)
        {
            DirectoryContext context = new DirectoryContext(DirectoryContextType.Domain, friendlyName);
            Domain domain = Domain.GetDomain(context);
            return domain.DomainControllers[0].Name;
        }

        public static ArrayList GetEnumerateDomains()
        {
            var alDomains = new ArrayList();
            var currentForest = Forest.GetCurrentForest();
            var myDomains = currentForest.Domains;

            foreach (Domain objDomain in myDomains)
            {
                alDomains.Add(objDomain.Name);
            }
            return alDomains;
        }

        public static List<ActionMethod> GetActionMethods()
        {
            var apiActions = new List<ActionMethod>();

            var apiDescriptions = GlobalConfiguration
                .Configuration
                .Services
                .GetApiExplorer()
                .ApiDescriptions;

            foreach (var api in apiDescriptions)
            {
                var parameters = new List<Parameter>();
                //get the parameters for this ActionMethod
                foreach (var parameterDescription in api.ParameterDescriptions)
                {
                    var parameter = new Parameter
                    {
                        Name = parameterDescription.Name,
                        Source = parameterDescription.Source.ToString(),
                        Type = parameterDescription.ParameterDescriptor.ParameterType.ToString(),
                        SubParameters = new List<Parameter>()
                    };
                    //get any Sub-Parameters (for complex types; this should probably be recursive)
                    foreach (var subProperty in parameterDescription.ParameterDescriptor.ParameterType.GetProperties())
                    {
                        parameter.SubParameters.Add(new Parameter
                        {
                            Name = subProperty.Name,
                            Type = subProperty.PropertyType.ToString()
                        });
                    }

                    parameters.Add(parameter);
                }
                //add a new action to our list
                apiActions.Add(new ActionMethod
                {
                    ID =
                        String.Format("{0}{1}", api.ActionDescriptor.ControllerDescriptor.ControllerName,
                            api.ActionDescriptor.ActionName),
                    ControllerName = api.ActionDescriptor.ControllerDescriptor.ControllerName,
                    ActionName = api.ActionDescriptor.ActionName,
                    Parameters = parameters,
                    SupportedHttpMethods = string.Join(",", api.ActionDescriptor.SupportedHttpMethods)
                });
            }

            return apiActions;
        }
        public static T DataToObject<T>(DataSet parData, bool parRelations, bool parAllColumns, Dictionary<string, string> parAliases) where T : new()
        {
            return ToObject<T>(DataToDictionary(parData, parRelations, parAllColumns, parAliases));
        }

        public static T ToObject<T>(object parObject) where T : new()
        {
            T result = default(T);

            string json = JsonConvert.SerializeObject(parObject);

            JsonSerializerSettings jss = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            jss.Converters.Add(new CreaJsonConverter());

            result = JsonConvert.DeserializeObject<T>(json, jss);

            return result;
        }

        public static Dictionary<string, object> DataToDictionary(DataSet parData)
        {
            return DataToDictionary(parData, false);
        }

        public static Dictionary<string, object> DataToDictionary(DataSet parData, bool parRelations)
        {
            return DataToDictionary(parData, false, true);
        }

        public static Dictionary<string, object> DataToDictionary(DataSet parData, bool parRelations, bool parAllColumns)
        {
            return DataToDictionary(parData, false, true, null);
        }

        public static string GetName(string parAlias, Dictionary<string, string> parAliases)
        {
            return parAliases != null && parAliases.ContainsValue(parAlias) ? parAliases.FirstOrDefault(x => x.Value.Contains(parAlias)).Key : parAlias;
        }

        public static string GetAlias(string parName, Dictionary<string, string> parAliases)
        {
            return parAliases != null && parAliases.ContainsKey(parName) ? parAliases[parName] : parName;
        }

        public static Dictionary<string, object> DataToDictionary(DataSet parData, bool parRelations, bool parAllColumns, Dictionary<string, string> parAliases)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            foreach (DataTable table in parData.Tables)
            {
                if (!parRelations || table.ParentRelations.Count == 0)
                {
                    if (IsSingleRow(table))
                    {
                        if (table.Rows.Count > 0)
                        {
                            result.Add(GetAlias(table.TableName, parAliases), RowToDictionary(table.Rows[0], null, parAllColumns, parAliases));
                        }
                        else
                        {
                            result.Add(GetAlias(table.TableName, parAliases), null);
                        }
                    }
                    else
                    {
                        result.Add(GetAlias(table.TableName, parAliases), RowsToDictionary(table, null, parAllColumns, parAliases));
                    }
                }
            }
            return result;
        }

        public static Dictionary<string, object> TableToDictionary(DataTable parTable)
        {
            return TableToDictionary(parTable, false);
        }

        public static Dictionary<string, object> TableToDictionary(DataTable parTable, bool parSigle)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            if (parSigle)
            {
                if (parTable.Rows.Count > 0)
                {
                    result.Add(parTable.TableName, RowToDictionary(parTable.Rows[0], null, true, null));
                }
                else
                {
                    result.Add(parTable.TableName, null);
                }
            }
            else
            {
                result.Add(parTable.TableName, RowsToDictionary(parTable, null));
            }
            return result;
        }

        protected static List<Dictionary<string, object>> RowsToDictionary(DataTable parTable)
        {
            return RowsToDictionary(parTable, null);
        }

        protected static List<Dictionary<string, object>> RowsToDictionary(DataRow[] parRows)
        {
            return RowsToDictionary(parRows, null);
        }

        protected static List<Dictionary<string, object>> RowsToDictionary(DataTable parTable, DataRelation parDataRelation)
        {
            return RowsToDictionary(parTable, null, true, null);
        }

        protected static List<Dictionary<string, object>> RowsToDictionary(DataRow[] parRows, DataRelation parDataRelation)
        {
            return RowsToDictionary(parRows, null, true, null);
        }

        protected static List<Dictionary<string, object>> RowsToDictionary(DataTable parTable, DataRelation parDataRelation, bool parAllColumns, Dictionary<string, string> parAliases)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            foreach (DataRow dr in parTable.Rows)
            {
                result.Add(RowToDictionary(dr, parDataRelation, parAllColumns, parAliases));
            }

            return result;
        }

        private static List<Dictionary<string, object>> RowsToDictionary(DataRow[] parRows, DataRelation parDataRelation, bool parAllColumns, Dictionary<string, string> parAliases)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            foreach (DataRow dr in parRows)
            {
                result.Add(RowToDictionary(dr, parDataRelation, parAllColumns, parAliases));
            }

            return result;
        }

        private static Dictionary<string, object> RowToDictionary(DataRow parDataRow, DataRelation parDataRelation, bool parAllColumns, Dictionary<string, string> parAliases)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            for (int i = 0; i < parDataRow.Table.Columns.Count; i++)
            {
                if (parAllColumns || parDataRelation == null || Array.IndexOf(parDataRelation.ChildColumns, parDataRow.Table.Columns[i]) == -1)
                {
                    result.Add(GetAlias(parDataRow.Table.Columns[i].ColumnName, parAliases), parDataRow.IsNull(i) ? null : parDataRow[i]);
                }
            }

            foreach (DataRelation cr in parDataRow.Table.ChildRelations)
            {
                DataRow[] childRows = parDataRow.GetChildRows(cr);

                if (IsSingleRow(cr.ChildTable))
                {
                    if (childRows.Length > 0)
                    {
                        result.Add(GetAlias(cr.ChildTable.TableName, parAliases), RowToDictionary(childRows[0], cr, parAllColumns, parAliases));
                    }
                    else
                    {
                        result.Add(GetAlias(cr.ChildTable.TableName, parAliases), null);
                    }
                }
                else
                {
                    result.Add(GetAlias(cr.ChildTable.TableName, parAliases), RowsToDictionary(childRows, cr, parAllColumns, parAliases));
                }

            }
            return result;
        }
        private static bool IsSingleRow(DataTable parDataTable)
        {
            bool result = false;

            if (parDataTable.ExtendedProperties.ContainsKey("single"))
            {
                result = (bool)parDataTable.ExtendedProperties["single"];

            }
            return result;
        }


        public static DataTable ToDataTable<T>(IList<T> data)
        {
            var props =
                TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            for (var i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            var values = new object[props.Count];
            foreach (var item in data)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static string GetUriCurrentHost(string parUrl)
        {
            Hashtable defaultPorts = new Hashtable() { { 80, true }, { 443, true } };

            UriBuilder result = new UriBuilder(parUrl);

            result.Scheme = HttpContext.Current.Request.Url.Scheme;
            result.Host = HttpContext.Current.Request.Url.Host;

            if (defaultPorts.ContainsKey(HttpContext.Current.Request.Url.Port) || HttpContext.Current.Request.Url.IsDefaultPort)
            {
                result.Port = -1;
            }
            else
            {
                result.Port = HttpContext.Current.Request.Url.Port;
            }

            return result.ToString();
        }

        public static string SerializeToXml(Object parObject)
        {
            var json = JsonConvert.SerializeObject(new { rootDocument = parObject }, new IsoDateTimeConverter());
            var doc = JsonConvert.DeserializeXmlNode(json);
            return doc.OuterXml;
        }

        public static T DeserializeFromXml<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));
            var settings = new XmlReaderSettings();
            using (var textReader = new StringReader(xml))
            {
                using (var xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        public static T DeserializeFromXml<T>(Stream xml)
        {
            xml.Position = 0;
            var serializer = new XmlSerializer(typeof(T));
            var settings = new XmlReaderSettings();
            using (var xmlReader = XmlReader.Create(xml, settings))
            {
                return (T)serializer.Deserialize(xmlReader);
            }
        }

        public static DataTable DirToDataTable(string parTableName, string parDir, string parFilter)
        {
            var extensions = parFilter.ToLower().Split(';');

            var result = new DataTable(parTableName);

            result.Columns.Add("Name", Type.GetType("System.String"));
            result.Columns.Add("IsDirectory", Type.GetType("System.Boolean"));
            result.Columns.Add("Size", Type.GetType("System.Int32"));
            result.Columns.Add("Created", Type.GetType("System.DateTime"));

            var rdi = new DirectoryInfo(parDir);

            var adi = rdi.GetDirectories();

            foreach (var di in adi)
            {
                var dr = result.NewRow();

                dr["Name"] = di.Name;
                dr["IsDirectory"] = true;
                dr["Size"] = 0;
                dr["Created"] = di.CreationTime;

                result.Rows.Add(dr);
            }

            var afi = rdi.GetFiles();

            foreach (var fi in afi)
            {
                if (Array.IndexOf(extensions, fi.Extension.ToLower()) != -1)
                {
                    var dr = result.NewRow();

                    dr["Name"] = fi.Name;
                    dr["IsDirectory"] = false;
                    dr["Size"] = fi.Length;
                    dr["Created"] = fi.CreationTime;

                    result.Rows.Add(dr);
                }
            }

            return result;
        }

        public static string[] FileToArray(string parFileName)
        {
            var al = new ArrayList();
            var sr = File.OpenText(parFileName);
            var str = sr.ReadLine();
            while (str != null)
            {
                al.Add(str);
                str = sr.ReadLine();
            }
            sr.Close();
            var astr = new string[al.Count];
            al.CopyTo(astr);

            return astr;
        }

        public static string FileToString(string parFileName)
        {
            var sr = File.OpenText(parFileName);
            var result = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();
            return result;
        }

        public static DataTable FileToDataTable(string parTableName, string parFileName)
        {
            var result = new DataTable(parTableName);

            result.Columns.Add("Row", Type.GetType("System.String"));
            result.Columns.Add("Length", Type.GetType("System.Int32"));

            var sr = File.OpenText(parFileName);

            var str = sr.ReadLine();

            while (str != null)
            {
                var dr = result.NewRow();

                dr["Row"] = str;
                dr["Length"] = str.Length;

                result.Rows.Add(dr);

                str = sr.ReadLine();
            }
            sr.Close();

            return result;
        }

        protected internal static void ArrayListToFile(string parFileName, ArrayList parMessages)
        {
            var fileName = parFileName;

            var objFileInfo = new FileInfo(fileName);
            objFileInfo.Delete();

            TextWriter fileMq = File.AppendText(fileName);

            foreach (var textMessage in parMessages)
            {
                var messageText = textMessage.ToString();

                fileMq.WriteLine(messageText);
            }

            fileMq.Close();
        }

        public static string Encrypt(string toEncrypt, string parCryptKey)
        {
            return Encrypt(toEncrypt, parCryptKey, false);
        }

        public static string Encrypt(string toEncrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);

            var tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            var cTransform = tdes.CreateEncryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt, string parCryptKey)
        {
            return Decrypt(toDecrypt, parCryptKey, false);
        }

        public static string Decrypt(string toDecrypt, string key, bool useHashing)
        {
            byte[] keyArray;
            var toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);

            var tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            var cTransform = tdes.CreateDecryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }

        public static string GetMd5Hash(string input)
        {
            var result = new StringBuilder();

            using (var md5Hasher = MD5.Create())
            {
                var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

                for (var i = 0; i < data.Length; i++)
                {
                    result.Append(data[i].ToString("x2"));
                }
            }
            return result.ToString();
        }

        public static void SendEmail(string parTo, string parFrom, string parSubject, string parBody, bool isBodyHtml,
            List<string[]> parImages)
        {
            SendEmail(parTo, parFrom, parSubject, parBody, isBodyHtml, parImages, null);
        }

        public static void SendEmail(string parTo, string parFrom, string parSubject, string parBody, bool isBodyHtml,
            List<string[]> parImages, List<Attachment> parAttachments)
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            try
            {
                message.From = new MailAddress(parFrom);
                message.To.Add(parTo);
                message.Subject = parSubject;
                message.IsBodyHtml = isBodyHtml;

                if (parAttachments != null)
                {
                    foreach (var a in parAttachments)
                    {
                        message.Attachments.Add(a);
                    }
                }

                if (message.IsBodyHtml)
                {
                    message.BodyEncoding = Encoding.GetEncoding("utf-8");

                    var htmlView = AlternateView.CreateAlternateViewFromString(parBody, null, "text/html");
                    if (parImages != null)
                    {
                        foreach (var imageParameters in parImages)
                        {
                            var url = HttpContext.Current.Server.MapPath(imageParameters[0]);
                            var id = imageParameters[1];
                            var type = imageParameters[2];

                            var imagelink = new LinkedResource(url, type);
                            imagelink.ContentId = id;
                            imagelink.TransferEncoding = TransferEncoding.Base64;
                            htmlView.LinkedResources.Add(imagelink);
                        }
                    }
                    message.AlternateViews.Add(htmlView);
                }
                else
                {
                    message.Body = parBody;
                }
                smtpClient.Send(message);
            }
            finally
            {
                message.Dispose();
            }
        }

        public static MemoryStream StringToStream(string parString)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(parString));
        }

        public static string StreamToString(Stream parStream)
        {
            parStream.Position = 0;
            return new StreamReader(parStream, Encoding.UTF8).ReadToEnd();
        }

        public static byte[] Stream_To_ByteArray(Stream input)
        {
            byte[] result = null;

            var buffer = new byte[16 * 1024];

            input.Position = 0;

            using (var ms = new MemoryStream())
            {
                int read;

                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                result = ms.ToArray();

                ms.Dispose();
            }

            return result;
        }

        public static byte[] StreamToByteArray(Stream input)
        {
            var result = new byte[input.Length];

            input.Read(result, 0, result.Length);

            return result;
        }

        public static void SaveJpeg(string parPath, Bitmap parBitmap, long parQuality)
        {
            var qualityParam = new EncoderParameter(Encoder.Quality, parQuality);

            var jpegCodec = GetEncoderInfo("image/jpeg");

            if (jpegCodec == null) return;

            var encoderParams = new EncoderParameters(1);

            encoderParams.Param[0] = qualityParam;

            parBitmap.Save(parPath, jpegCodec, encoderParams);
        }

        public static Bitmap RotateImg(Image bmp, float angle, Color bkColor)
        {
            var w = bmp.Width;
            var h = bmp.Height;
            var pf = default(PixelFormat);
            if (bkColor == Color.Transparent)
            {
                pf = PixelFormat.Format32bppArgb;
            }
            else
            {
                pf = bmp.PixelFormat;
            }

            var tempImg = new Bitmap(w, h, pf);
            var g = Graphics.FromImage(tempImg);
            g.Clear(bkColor);
            g.DrawImageUnscaled(bmp, 1, 1);
            g.Dispose();

            var path = new GraphicsPath();
            path.AddRectangle(new RectangleF(0f, 0f, w, h));
            var mtrx = new Matrix();
            var center = new Point(w / 2, h / 2);
            mtrx.RotateAt(angle, center);
            var rct = path.GetBounds(mtrx);
            var newImg = new Bitmap(Convert.ToInt32(rct.Width), Convert.ToInt32(rct.Height), pf);
            g = Graphics.FromImage(newImg);
            g.Clear(bkColor);
            g.TranslateTransform(-rct.X, -rct.Y);
            g.RotateTransform(angle);
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            g.DrawImageUnscaled(tempImg, 0, 0);
            g.Dispose();
            tempImg.Dispose();
            return newImg;
        }

        public static Bitmap RotateImage(Image bmpBu, float w, bool keepWholeImg)
        {
            Bitmap result = null;
            Graphics g = null;
            try
            {
                //Modus
                if (!keepWholeImg)
                {
                    result = new Bitmap(bmpBu.Width, bmpBu.Height);

                    g = Graphics.FromImage(result);
                    var hw = result.Width / 2f;
                    var hh = result.Height / 2f;
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    //translate center
                    g.TranslateTransform(hw, hh);
                    //rotate
                    g.RotateTransform(w);
                    //re-translate
                    g.TranslateTransform(-hw, -hh);
                    g.DrawImage(bmpBu, 0, 0);
                    g.Dispose();
                }
                else
                {
                    //get the new size and create the blank bitmap
                    var rad = (float)(w / 180.0 * Math.PI);
                    var fW = Math.Abs((Math.Cos(rad) * bmpBu.Width)) + Math.Abs((Math.Sin(rad) * bmpBu.Height));
                    var fH = Math.Abs((Math.Sin(rad) * bmpBu.Width)) + Math.Abs((Math.Cos(rad) * bmpBu.Height));

                    result = new Bitmap((int)Math.Ceiling(fW), (int)Math.Ceiling(fH));

                    g = Graphics.FromImage(result);

                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    var hw = result.Width / 2f;
                    var hh = result.Height / 2f;

                    var m = g.Transform;

                    //here we do not need to translate, we rotate at the specified point
                    m.RotateAt(w, new PointF(result.Width / 2, result.Height / 2), MatrixOrder.Append);

                    g.Transform = m;

                    //draw the rotated image
                    g.DrawImage(bmpBu, new PointF((result.Width - bmpBu.Width) / 2, (result.Height - bmpBu.Height) / 2));
                    g.Dispose();
                }
            }
            catch
            {
                if ((result != null))
                {
                    result.Dispose();
                    result = null;
                }

                if ((g != null))
                {
                    g.Dispose();
                }
            }

            return result;
        }

        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        public static Image StretchImage(Image parImage, int parWidth, int parHeight)
        {
            var result = new Bitmap(parWidth, parHeight);
            Graphics.FromImage(result).DrawImage(parImage, 0, 0, parWidth, parHeight);
            return result;
        }

        private static ImageCodecInfo GetEncoderInfo(string parMimeType)
        {
            var codecs = ImageCodecInfo.GetImageEncoders();

            for (var i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType == parMimeType) return codecs[i];
            }

            return null;
        }

        public static T StringToEnum<T>(string name)
        {
            return (T)Enum.Parse(typeof(T), name);
        }

        public static List<string> EnumToList<T>()
        {
            return Enum.GetNames(typeof(T)).ToList();
        }

        public static DateTime UnixTimestampToDateTime(long parUnixTimeStamp)
        {
            return (new DateTime(1970, 1, 1, 0, 0, 0)).AddSeconds(parUnixTimeStamp);
        }

        public static long DateTimeToUnixTimestamp(DateTime parDateTime)
        {
            var unixTimeSpan = (parDateTime - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)unixTimeSpan.TotalSeconds;
        }

        public static bool ContainsWords(string parWords, string parPhrase)
        {
            var result = false;

            var ww = parWords.Split();
            var pp = parPhrase.Split();

            foreach (var w in ww)
            {
                if (Array.IndexOf(pp, w) > -1) return true;
            }

            return result;
        }

        public static string XmlToJson(string parXml)
        {
            var doc = new XmlDocument();
            doc.LoadXml(parXml);
            return JsonConvert.SerializeXmlNode(doc);
        }

        public static string JsonToXml(string parJson)
        {
            var doc = JsonConvert.DeserializeXmlNode(parJson);
            return doc.InnerXml;
        }

        public static string ObjectToJson(object parObject)
        {
            string result = null;

            var jss = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            jss.Converters.Add(new IsoDateTimeConverter());

            result = JsonConvert.SerializeObject(parObject, Formatting.None, jss);

            return result;
        }

        public static object JsonToObject(string parJson, Type parType)
        {
            return JsonConvert.DeserializeObject(parJson, parType);
        }

        public static T JsonToObject<T>(string parJson)
        {
            return (T)JsonConvert.DeserializeObject(parJson, typeof(T));
        }

        public static T XmlToObject<T>(string parXml, string parPath)
        {
            var xd = new XmlDocument();

            xd.LoadXml(parXml);

            var n = xd.SelectSingleNode(parPath);

            var json = JsonConvert.SerializeXmlNode(n.ParentNode);

            var jo = JObject.Parse(json);

            return (T)JsonConvert.DeserializeObject(jo[n.ParentNode.Name][n.Name].ToString(), typeof(T));
        }

        public static string ObjectToXml(Object parObject)
        {
            var json = JsonConvert.SerializeObject(new { root = parObject }, new IsoDateTimeConverter());

            var doc = JsonConvert.DeserializeXmlNode(json);

            return doc.InnerXml;
        }

        public static long ZipBytesToStream(Stream parOutputStream, byte[] parBytes, string parEntryName)
        {
            long result = 0;

            using (var zos = new ZipOutputStream(parOutputStream))
            {
                var crc = new Crc32();

                zos.SetLevel(9);

                var ze = new ZipEntry(parEntryName);

                ze.DateTime = DateTime.Now;
                ze.Size = parBytes.Length;
                crc.Reset();
                crc.Update(parBytes);
                ze.Crc = crc.Value;
                zos.PutNextEntry(ze);
                zos.Write(parBytes, 0, parBytes.Length);
                zos.CloseEntry();
                zos.Finish();
                zos.Close();
            }

            return result;
        }

        public static byte[] ImageToByte(Image img)
        {
            var converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public static byte[] ImageToByte(Image img, ImageFormat frm)
        {
            var byteArray = new byte[0];
            using (var stream = new MemoryStream())
            {
                img.Save(stream, frm);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public static Image ImageFromBase64(string parBase64)
        {
            Image result;

            var bytes = Convert.FromBase64String(parBase64);


            using (var ms = new MemoryStream(bytes))
            {
                result = Image.FromStream(ms);
            }

            return result;
        }

        public static Bitmap BitmapFromBase64(string parBase64)
        {
            return new Bitmap(ImageFromBase64(parBase64));
            ;
        }

        public static string XslTransformer(string parXml, string parXsl)
        {
            return XslTransformer(parXml, parXsl, null);
        }

        public static string XslTransformer(string parXml, string parXsl, Dictionary<string, object> parXsltParameters)
        {
            var result = String.Empty;

            var xdd = new XmlDocument();

            xdd.LoadXml(parXml);

            XsltArgumentList arguments = null;

            if (parXsltParameters != null)
            {
                arguments = new XsltArgumentList();
                foreach (var k in parXsltParameters.Keys)
                {
                    arguments.AddParam(k, string.Empty, parXsltParameters[k]);
                }
            }

            using (var ms = new MemoryStream())
            using (var sr = new StringReader(parXsl))
            using (var xtr = new XmlTextReader(sr))
            {
                var xct = new XslCompiledTransform();
                xct.Load(new XmlTextReader(new StringReader(parXsl)));
                xct.Transform(xdd, arguments, ms);
                ms.Position = 0;
                using (var rsr = new StreamReader(ms))
                {
                    result = rsr.ReadToEnd();
                    rsr.Close();
                }
                xtr.Close();
                sr.Close();
                ms.Close();
            }

            return result;
        }

        public static string KeyFromRequest(string[] parOmitKeys)
        {
            var result = new StringBuilder(1024);

            foreach (string key in HttpContext.Current.Request.QueryString.Keys)
            {
                if (Array.IndexOf(parOmitKeys, key.ToLower()) == -1)
                {
                    result.AppendFormat("{0}={1}|", key.ToLower(), HttpContext.Current.Request.QueryString[key]);
                }
            }

            foreach (string key in HttpContext.Current.Request.Form.Keys)
            {
                if (Array.IndexOf(parOmitKeys, key.ToLower()) == -1)
                {
                    result.AppendFormat("{0}={1}|", key.ToLower(), HttpContext.Current.Request.Form[key]);
                }
            }

            return result.ToString();
        }

        public static byte[] SerializeData<T>(ref T data)
        {
            byte[] result = null;
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, data);
                result = stream.ToArray();
            }
            return result;
        }

        public static T DeserializeData<T>(ref byte[] data)
        {
            var result = default(T);
            using (var stream = new MemoryStream(data))
            {
                result = (T)new BinaryFormatter().Deserialize(stream);
            }
            return result;
        }

        public static T LoadDataFromCache<T>(string key) where T : class
        {
            var result = default(T);
            var cache = HttpRuntime.Cache;
            var data = (byte[])cache.Get(key);
            if (data != null)
            {
                result = DeserializeData<T>(ref data);
            }
            return result;
        }

        public static void SaveDataInCache<T>(string key, double expirationMinutes, ref T data) where T : class
        {
            if (data != null)
            {
                var result = SerializeData(ref data);
                if (result != null)
                {
                    var cache = HttpRuntime.Cache;
                    if (cache.Get(key) == null)
                    {
                        cache.Insert(
                            key,
                            result,
                            null,
                            DateTime.Now.AddMinutes(expirationMinutes),
                            TimeSpan.Zero
                            );
                    }
                }
            }
        }

        public static bool VerifyHash(string plainText,
            string hashAlgorithm,
            string hashValue)
        {
            // Convert base64-encoded hash value into a byte array.
            var hashWithSaltBytes = Convert.FromBase64String(hashValue);

            // We must know size of hash (without salt).
            int hashSizeInBits, hashSizeInBytes;

            // Make sure that hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Size of hash is based on the specified algorithm.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hashSizeInBits = 160;
                    break;

                case "SHA256":
                    hashSizeInBits = 256;
                    break;

                case "SHA384":
                    hashSizeInBits = 384;
                    break;

                case "SHA512":
                    hashSizeInBits = 512;
                    break;

                default: // Must be MD5
                    hashSizeInBits = 128;
                    break;
            }

            // Convert size of hash from bits to bytes.
            hashSizeInBytes = hashSizeInBits / 8;

            // Make sure that the specified hash value is long enough.
            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return false;

            // Allocate array to hold original salt bytes retrieved from hash.
            var saltBytes = new byte[hashWithSaltBytes.Length -
                                     hashSizeInBytes];

            // Copy salt from the end of the hash to the new array.
            for (var i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            // Compute a new hash string.
            var expectedHashString =
                ComputeHash(plainText, hashAlgorithm, saltBytes);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.
            return (hashValue == expectedHashString);
        }

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                var minSaltSize = 4;
                var maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                var random = new Random();
                var saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                var rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            var plainTextWithSaltBytes =
                new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (var i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (var i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            // Because we support multiple hashing algorithms, we must define
            // hash object as a common (abstract) base class. We will specify the
            // actual hashing algorithm class later during object creation.
            HashAlgorithm hash;

            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            var hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            var hashWithSaltBytes = new byte[hashBytes.Length +
                                             saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (var i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (var i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            var hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }

        public static List<string[]> ParseCsv(string parCsv, string parSeparator)
        {
            var result = new List<string[]>();

            string[] rowSeparator = { parSeparator };

            var rows = parCsv.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var column = string.Empty;


            foreach (var row in rows)
            {
                var columns = row.Split(rowSeparator, StringSplitOptions.None);
                result.Add(columns);
            }

            return result;
        }

        public static void DataTableToCsv(DataTable parDataTable, string parSeparator, bool parHeadNames,
            string parFilename)
        {
            var sb = new StringBuilder();

            if (parHeadNames)
            {
                var columnNames = parDataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
                sb.AppendLine(string.Join(parSeparator, columnNames));
            }

            foreach (DataRow row in parDataTable.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                sb.AppendLine(string.Join(parSeparator, fields));
            }

            File.WriteAllText(parFilename, sb.ToString());
        }

        public static void MergeObjects<T>(T parTarget, T parSource)
        {
            var t = typeof(T);

            var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);

            foreach (var prop in properties)
            {
                var value = prop.GetValue(parSource, null);
                if (value != null)
                    prop.SetValue(parTarget, value, null);
            }
        }

        public static DataRelation AddRelation(DataSet parDataSet, string parParentTable, string parChildTable,
            string parParentColumns, string parChildColumns, bool parNested)
        {
            DataRelation result = null;

            var dtParentTable = parDataSet.Tables[parParentTable];
            var dtChildTable = parDataSet.Tables[parChildTable];

            var nameParentColumns = parParentColumns.Split(',');
            var nameChildColumns = parChildColumns.Split(',');

            var parentColumns = new DataColumn[nameParentColumns.Length];
            var childColumns = new DataColumn[nameChildColumns.Length];

            for (var i = 0; i < nameParentColumns.Length; i++)
            {
                parentColumns[i] = dtParentTable.Columns[nameParentColumns[i]];
            }

            for (var i = 0; i < nameChildColumns.Length; i++)
            {
                childColumns[i] = dtChildTable.Columns[nameChildColumns[i]];
            }

            result = new DataRelation(String.Format("{0}{1}", parParentTable, parChildTable), parentColumns,
                childColumns, parNested);

            parDataSet.Relations.Add(result);

            return result;
        }

        public static bool GetWebRequest(string parUrl, string parData, Dictionary<string, string> parHeaders,
            out string parResponse)
        {
            var result = false;

            try
            {
                parResponse = string.Empty;

                HttpWebRequest webRequest = null;

                webRequest = (HttpWebRequest)WebRequest.Create(parUrl);
                webRequest.KeepAlive = true;
                webRequest.Method = "POST";
                webRequest.UserAgent =
                    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36";

                foreach (var key in parHeaders.Keys)
                {
                    switch (key)
                    {
                        case "Method":
                            webRequest.Method = parHeaders[key];
                            break;
                        case "UserAgent":
                            webRequest.UserAgent = parHeaders[key];
                            break;
                        case "Accept":
                            webRequest.Accept = parHeaders[key];
                            break;
                        case "Content-Type":
                            webRequest.ContentType = parHeaders[key];
                            break;
                        default:
                            webRequest.Headers[key] = parHeaders[key];
                            break;
                    }
                }

                if (!String.IsNullOrEmpty(parData))
                {
                    using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        streamWriter.Write(parData);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }

                var response = (HttpWebResponse)webRequest.GetResponse();

                if (response != null)
                {
                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        parResponse = sr.ReadToEnd();
                        response.Close();
                    }

                    result = true;
                }
            }
            catch (WebException e)
            {
                using (var response = e.Response)
                {
                    var httpResponse = (HttpWebResponse)response;
                    using (var data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        parResponse = reader.ReadToEnd();
                    }
                }
            }
            return result;
        }

        public static string SetUriHost(string parUrl, string parRequestUri)
        {
            if (String.IsNullOrEmpty(parRequestUri))
            {
                return parUrl;
            }

            var result = new UriBuilder(parUrl);
            var requestUri = new UriBuilder(parRequestUri);
            result.Scheme = requestUri.Scheme;
            result.Host = requestUri.Host;
            result.Port = requestUri.Port;
            return result.ToString();
        }

    }

    public class CreaJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            if (objectType == typeof(int))
            {
                return Convert.ToInt32(reader.Value);
            }

            if (objectType == typeof(int?))
            {
                return Convert.ToInt32(reader.Value);
            }

            return reader.Value;
        }

        public override bool CanConvert(Type objectType)
        {
            bool result = false;

            result = result || objectType == typeof(int);
            result = result || objectType == typeof(int?);

            return result;
        }
    }

}