﻿using System.Data.Common;

namespace RcLib
{
    /// <summary>
    ///     Design Pattern: Factory.
    /// </summary>
    public class DatabaseManager
    {
        private readonly DbProviderFactory _providerFactory;

        public DatabaseManager(string parConnectionProvider)
        {
            _providerFactory = DbProviderFactories.GetFactory(parConnectionProvider);
        }

        public DbConnection GetConnection(string parConnectionString)
        {
            var result = _providerFactory.CreateConnection();
            result.ConnectionString = parConnectionString;
            return result;
        }

        public DbCommand CreateCommand()
        {
            return _providerFactory.CreateCommand();
        }

        public DbCommand CreateCommand(string parCommandText, DbConnection parConnection, DbTransaction parTransaction)
        {
            var result = CreateCommand();
            result.CommandText = parCommandText;
            result.Connection = parConnection;
            result.Transaction = parTransaction;
            return result;
        }
        public DbParameter CreateParameter()
        {
            return _providerFactory.CreateParameter();
        }

        public DbDataAdapter CreateDataAdapter()
        {
            return _providerFactory.CreateDataAdapter();
        }

        public DbDataAdapter CreateDataAdapter(DbCommand parCommand)
        {
     
            var result = CreateDataAdapter();

            result.SelectCommand = parCommand;

            return result;
        }

        private DbCommandBuilder CreateCommandBuilder()
        {
            return _providerFactory.CreateCommandBuilder();
        }

        public DbCommandBuilder CreateCommandBuilder(DbDataAdapter parDataAdapter)
        {
            var result = CreateCommandBuilder();

            result.DataAdapter = parDataAdapter;

            return result;
        }
    }
}