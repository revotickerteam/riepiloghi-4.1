﻿using System;
using System.Web;

namespace RcLib
{
	public class DownloadData
	{
		public string Filename { get; set; }
		public string ContentType { get; set; }
		public string DataBase64 { get; set; }
		private byte[] GetBuffer()
		{
			return Convert.FromBase64String(DataBase64);
		}
		public void SetBuffer(byte[] parBuffer)
		{
			DataBase64 = Convert.ToBase64String(parBuffer);
		}
		public void Send(HttpContext context)
		{
			/*
			context.Response.Clear();
			context.Response.ContentType = ContentType;
			context.Response.AppendHeader("content-disposition", String.Format("inline; filename={0}", Filename));
			context.Response.AddHeader("content-length", GetBuffer().Length.ToString());
			context.Response.BinaryWrite(GetBuffer());
			context.Response.Flush();
			*/
			context.Response.BinaryWrite(GetBuffer());
			context.Response.ContentType = ContentType;
			context.Response.AddHeader("content-disposition", String.Format("attachment;  filename={0}", Filename));
		}
	}
	public class DownloadServices : IHttpHandler
	{
		private DownloadData _DownloadData = null;
		private DownloadData DownloadData
		{
			get
			{
				try
				{
					_DownloadData = new DownloadData()
					{
						Filename = HttpContext.Current.Request.Form["Filename"],
						ContentType = HttpContext.Current.Request.Form["ContentType"],
						DataBase64 = HttpContext.Current.Request.Form["DataBase64"]
					};
				}
				catch (Exception)
				{
					throw (new Exception("WTD0001"));
				}
				return _DownloadData;
			}
		}
		/// <summary>
		/// You will need to configure this handler in the web.config file of your 
		/// web and register it with IIS before being able to use it. For more information
		/// see the following link: http://go.microsoft.com/?linkid=8101007
		/// </summary>
		#region IHttpHandler Members
		public bool IsReusable
		{
			// Return false in case your Managed Handler cannot be reused for another request.
			// Usually this would be false in case you have some state information preserved per request.
			get { return false; }
		}
		public void ProcessRequest(HttpContext context)
		{
			DownloadData.Send(context);
		}
		#endregion
	}
}
