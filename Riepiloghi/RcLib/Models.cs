﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RcLib
{
    public class ActiveDirectoryAccount
    {
        public string SamAccountName { get; set; }
        public string UserPrincipalName { get; set; }
        public string GivenName { get; set; }
        public string Sn { get; set; }

        public string nETBIOSName { get; set; }
        public string displayname { get; set; }

        public string SearchName
        {
            get
            {
                return String.Format("{0} {1} {2}", GivenName, Sn, SamAccountName);
            }
        }

    }

    public class ActionMethod
    {
        [DataMember]
        public bool isSelected;

        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string ControllerName { get; set; }

        [DataMember]
        public string ActionName { get; set; }

        [DataMember]
        public List<Parameter> Parameters { get; set; }

        [DataMember]
        public string SupportedHttpMethods { get; set; }
    }

    [DataContract]
    public class Parameter
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Source { get; set; } //where we pass the parameter when calling the action

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public List<Parameter> SubParameters { get; set; }
    }

}
