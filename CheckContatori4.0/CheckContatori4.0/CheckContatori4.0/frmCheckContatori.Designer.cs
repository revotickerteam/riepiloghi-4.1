﻿namespace CheckContatori4._0
{
    partial class frmCheckContatori
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlFolder = new System.Windows.Forms.Panel();
            this.pnlFolderInside = new System.Windows.Forms.Panel();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.btnFolder = new System.Windows.Forms.Button();
            this.lblFolder = new System.Windows.Forms.Label();
            this.pnlDatabase = new System.Windows.Forms.Panel();
            this.pnlDatabaseInside = new System.Windows.Forms.Panel();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.chkDatabase = new System.Windows.Forms.CheckBox();
            this.pnlAnalisi = new System.Windows.Forms.Panel();
            this.txtAnalisi = new System.Windows.Forms.TextBox();
            this.progressBarAnalisi = new System.Windows.Forms.ProgressBar();
            this.lblAnalisi = new System.Windows.Forms.Label();
            this.progressBarSubCounter = new System.Windows.Forms.ProgressBar();
            this.pnlErrori = new System.Windows.Forms.Panel();
            this.txtErrori = new System.Windows.Forms.TextBox();
            this.lblErrori = new System.Windows.Forms.Label();
            this.btnExec = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlFolder.SuspendLayout();
            this.pnlFolderInside.SuspendLayout();
            this.pnlDatabase.SuspendLayout();
            this.pnlDatabaseInside.SuspendLayout();
            this.pnlAnalisi.SuspendLayout();
            this.pnlErrori.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFolder
            // 
            this.pnlFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFolder.Controls.Add(this.pnlFolderInside);
            this.pnlFolder.Controls.Add(this.lblFolder);
            this.pnlFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFolder.Location = new System.Drawing.Point(0, 0);
            this.pnlFolder.Name = "pnlFolder";
            this.pnlFolder.Size = new System.Drawing.Size(669, 58);
            this.pnlFolder.TabIndex = 2;
            // 
            // pnlFolderInside
            // 
            this.pnlFolderInside.Controls.Add(this.txtFolder);
            this.pnlFolderInside.Controls.Add(this.btnFolder);
            this.pnlFolderInside.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFolderInside.Location = new System.Drawing.Point(0, 23);
            this.pnlFolderInside.Name = "pnlFolderInside";
            this.pnlFolderInside.Padding = new System.Windows.Forms.Padding(3);
            this.pnlFolderInside.Size = new System.Drawing.Size(667, 33);
            this.pnlFolderInside.TabIndex = 2;
            // 
            // txtFolder
            // 
            this.txtFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFolder.Location = new System.Drawing.Point(3, 3);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(627, 27);
            this.txtFolder.TabIndex = 2;
            this.txtFolder.Text = "C:\\RIEPILOGHI";
            // 
            // btnFolder
            // 
            this.btnFolder.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFolder.Location = new System.Drawing.Point(630, 3);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(34, 27);
            this.btnFolder.TabIndex = 3;
            this.btnFolder.Text = "...";
            this.btnFolder.UseVisualStyleBackColor = true;
            // 
            // lblFolder
            // 
            this.lblFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFolder.Location = new System.Drawing.Point(0, 0);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(667, 23);
            this.lblFolder.TabIndex = 0;
            this.lblFolder.Text = "Cartella Riepiloghi";
            // 
            // pnlDatabase
            // 
            this.pnlDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDatabase.Controls.Add(this.pnlDatabaseInside);
            this.pnlDatabase.Controls.Add(this.chkDatabase);
            this.pnlDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDatabase.Location = new System.Drawing.Point(0, 58);
            this.pnlDatabase.Name = "pnlDatabase";
            this.pnlDatabase.Size = new System.Drawing.Size(669, 62);
            this.pnlDatabase.TabIndex = 3;
            // 
            // pnlDatabaseInside
            // 
            this.pnlDatabaseInside.Controls.Add(this.txtDatabase);
            this.pnlDatabaseInside.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDatabaseInside.Location = new System.Drawing.Point(0, 23);
            this.pnlDatabaseInside.Name = "pnlDatabaseInside";
            this.pnlDatabaseInside.Padding = new System.Windows.Forms.Padding(3);
            this.pnlDatabaseInside.Size = new System.Drawing.Size(667, 33);
            this.pnlDatabaseInside.TabIndex = 2;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDatabase.Location = new System.Drawing.Point(3, 3);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(661, 27);
            this.txtDatabase.TabIndex = 2;
            this.txtDatabase.Text = "SERVERX";
            // 
            // chkDatabase
            // 
            this.chkDatabase.Checked = true;
            this.chkDatabase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkDatabase.Location = new System.Drawing.Point(0, 0);
            this.chkDatabase.Name = "chkDatabase";
            this.chkDatabase.Size = new System.Drawing.Size(667, 23);
            this.chkDatabase.TabIndex = 0;
            this.chkDatabase.Text = "Confronta anche con database, nome connessione TNSNAMES.ORA";
            // 
            // pnlAnalisi
            // 
            this.pnlAnalisi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAnalisi.Controls.Add(this.txtAnalisi);
            this.pnlAnalisi.Controls.Add(this.progressBarAnalisi);
            this.pnlAnalisi.Controls.Add(this.lblAnalisi);
            this.pnlAnalisi.Controls.Add(this.progressBarSubCounter);
            this.pnlAnalisi.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAnalisi.Location = new System.Drawing.Point(0, 170);
            this.pnlAnalisi.Name = "pnlAnalisi";
            this.pnlAnalisi.Size = new System.Drawing.Size(669, 195);
            this.pnlAnalisi.TabIndex = 5;
            // 
            // txtAnalisi
            // 
            this.txtAnalisi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnalisi.Location = new System.Drawing.Point(0, 23);
            this.txtAnalisi.Multiline = true;
            this.txtAnalisi.Name = "txtAnalisi";
            this.txtAnalisi.ReadOnly = true;
            this.txtAnalisi.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtAnalisi.Size = new System.Drawing.Size(667, 150);
            this.txtAnalisi.TabIndex = 1;
            // 
            // progressBarAnalisi
            // 
            this.progressBarAnalisi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarAnalisi.Location = new System.Drawing.Point(0, 173);
            this.progressBarAnalisi.Name = "progressBarAnalisi";
            this.progressBarAnalisi.Size = new System.Drawing.Size(667, 10);
            this.progressBarAnalisi.TabIndex = 2;
            // 
            // lblAnalisi
            // 
            this.lblAnalisi.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAnalisi.Location = new System.Drawing.Point(0, 0);
            this.lblAnalisi.Name = "lblAnalisi";
            this.lblAnalisi.Size = new System.Drawing.Size(667, 23);
            this.lblAnalisi.TabIndex = 0;
            this.lblAnalisi.Text = "Analisi";
            // 
            // progressBarSubCounter
            // 
            this.progressBarSubCounter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarSubCounter.Location = new System.Drawing.Point(0, 183);
            this.progressBarSubCounter.Name = "progressBarSubCounter";
            this.progressBarSubCounter.Size = new System.Drawing.Size(667, 10);
            this.progressBarSubCounter.TabIndex = 3;
            // 
            // pnlErrori
            // 
            this.pnlErrori.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlErrori.Controls.Add(this.txtErrori);
            this.pnlErrori.Controls.Add(this.lblErrori);
            this.pnlErrori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlErrori.Location = new System.Drawing.Point(0, 377);
            this.pnlErrori.MinimumSize = new System.Drawing.Size(200, 200);
            this.pnlErrori.Name = "pnlErrori";
            this.pnlErrori.Size = new System.Drawing.Size(669, 200);
            this.pnlErrori.TabIndex = 7;
            // 
            // txtErrori
            // 
            this.txtErrori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtErrori.Location = new System.Drawing.Point(0, 23);
            this.txtErrori.Multiline = true;
            this.txtErrori.Name = "txtErrori";
            this.txtErrori.ReadOnly = true;
            this.txtErrori.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtErrori.Size = new System.Drawing.Size(667, 175);
            this.txtErrori.TabIndex = 1;
            // 
            // lblErrori
            // 
            this.lblErrori.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblErrori.Location = new System.Drawing.Point(0, 0);
            this.lblErrori.Name = "lblErrori";
            this.lblErrori.Size = new System.Drawing.Size(667, 23);
            this.lblErrori.TabIndex = 0;
            this.lblErrori.Text = "Errori";
            // 
            // btnExec
            // 
            this.btnExec.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExec.Location = new System.Drawing.Point(0, 120);
            this.btnExec.Name = "btnExec";
            this.btnExec.Size = new System.Drawing.Size(669, 50);
            this.btnExec.TabIndex = 8;
            this.btnExec.Text = "Avvia";
            this.btnExec.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.DarkGray;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 365);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(669, 12);
            this.splitter1.TabIndex = 9;
            this.splitter1.TabStop = false;
            // 
            // frmCheckContatori
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(669, 577);
            this.Controls.Add(this.pnlErrori);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlAnalisi);
            this.Controls.Add(this.btnExec);
            this.Controls.Add(this.pnlDatabase);
            this.Controls.Add(this.pnlFolder);
            this.Name = "frmCheckContatori";
            this.Text = "Check Contatori 4.0";
            this.pnlFolder.ResumeLayout(false);
            this.pnlFolderInside.ResumeLayout(false);
            this.pnlFolderInside.PerformLayout();
            this.pnlDatabase.ResumeLayout(false);
            this.pnlDatabaseInside.ResumeLayout(false);
            this.pnlDatabaseInside.PerformLayout();
            this.pnlAnalisi.ResumeLayout(false);
            this.pnlAnalisi.PerformLayout();
            this.pnlErrori.ResumeLayout(false);
            this.pnlErrori.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFolder;
        private System.Windows.Forms.Panel pnlFolderInside;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.Label lblFolder;
        private System.Windows.Forms.Panel pnlDatabase;
        private System.Windows.Forms.Panel pnlDatabaseInside;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.CheckBox chkDatabase;
        private System.Windows.Forms.Panel pnlAnalisi;
        private System.Windows.Forms.TextBox txtAnalisi;
        private System.Windows.Forms.Label lblAnalisi;
        private System.Windows.Forms.Panel pnlErrori;
        private System.Windows.Forms.TextBox txtErrori;
        private System.Windows.Forms.Label lblErrori;
        private System.Windows.Forms.Button btnExec;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ProgressBar progressBarAnalisi;
        private System.Windows.Forms.ProgressBar progressBarSubCounter;
    }
}

