﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace CheckContatori4._0
{
    public delegate void delegateMessage(string carta, long? minProgressivo, long? maxProgressivo, string value, Exception error);

    public static class Utils
    {
        private static Regex regLOG = new Regex(@"LOG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
        private static Regex regRPG = new Regex(@"RPG_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
        private static Regex regRPM = new Regex(@"RPM_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
        private static Regex regLTA = new Regex(@"LTA_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
        private static Regex regRCA = new Regex(@"RCA_\d{4}_\d{2}_\d{2}_\d{3}.xsi.p7m");
        private static bool lExecuting = false;
        private static bool stopProcess = false;
        public static bool Executing => lExecuting;
        public static int MaxAnalisi = 0;
        public static int CntAnalisi = 0;
        private static bool errorExists = false;
        public static string FileLogErrori = "";

        private static string RootPath { get; set; }
        private static string ApplicationExecutablePath { get; set; }
        private static string DbConnection { get; set; }

        public delegate void delegateAddAnalisi(string value);
        public delegate void delegateAddWarning(string value);
        public delegate void delegateAddErrore(string value);
        public delegate void delegateEndMode(bool errorExists);
        public delegate void delegateSubCounterChanged();

        public static event delegateAddAnalisi EventAddAnalisi;
        public static event delegateAddErrore EventAddError;
        public static event delegateEndMode EventEndMode;
        public static event delegateSubCounterChanged EventSubCounterChanged;

        private static int maxSubCounter = 0;
        private static int cntSubCounter = 0;
        private static int cntTempSubCounter = 0;

        public static System.Collections.Generic.Queue<string> queueErrors = new Queue<string>();
        private static System.Threading.Thread threadWriteErrors = null;
        private static string lastAnalisi = "";

        public static DateTime GetDataFile(string fileName, bool mensile = false)
        {
            DateTime result = DateTime.MinValue;
            if (regLOG.IsMatch(fileName))
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), mensile ? 1 : int.Parse(fileName.Substring(12, 2)));
            else if (regRPG.IsMatch(fileName))
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), mensile ? 1 : int.Parse(fileName.Substring(12, 2)));
            else if (regRPM.IsMatch(fileName))
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), mensile ? 1 : int.Parse(fileName.Substring(12, 2)));
            else if (regLTA.IsMatch(fileName))
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), mensile ? 1 : int.Parse(fileName.Substring(12, 2)));
            else if (regRCA.IsMatch(fileName))
                result = new DateTime(int.Parse(fileName.Substring(4, 4)), int.Parse(fileName.Substring(9, 2)), mensile ? 1 : int.Parse(fileName.Substring(12, 2)));
            return result;
        }

        public static bool StopProcess
        {
            set 
            { 
                stopProcess = value;
                if (stopProcess)
                    AddAnalisi("Richiesta interruzione analisi");
            }
        }

        public static int MaxSubCounter { get { return maxSubCounter; } set { maxSubCounter = value; cntSubCounter = 0 ; if (EventSubCounterChanged != null) EventSubCounterChanged(); } }
        public static int CntSubCounter 
        { 
            get { return cntSubCounter; } 
            set 
            {
                cntSubCounter = value;
                bool invokeEvent = (value == 0 || value == maxSubCounter || (maxSubCounter > 0 && System.Math.Abs(cntSubCounter - cntTempSubCounter) > (maxSubCounter / 10)));
                if (invokeEvent)
                {
                    cntTempSubCounter = value;
                    if (EventSubCounterChanged != null) EventSubCounterChanged();
                }
            } 
        }

        private static void AddAnalisi(string value)
        {
            CntAnalisi += 1;
            lastAnalisi = value;
            if (EventAddAnalisi != null) EventAddAnalisi(value);
        }

        private static void AddWarning(string value)
        {
            if (lastAnalisi != "")
            {
                AddErrore((value.StartsWith(" ") ? "".PadRight(value.Length - value.TrimStart().Length) : "") + "WARNING nella fase" + (lastAnalisi.StartsWith(" ") ? "" : " ") + lastAnalisi);
                lastAnalisi = "";
            }
            AddErrore((value.StartsWith(" ") ? "".PadRight(value.Length - value.TrimStart().Length) : "") + "WARNING" + (value.StartsWith(" ") ? "" : " ") + value);
        }

        private static void AddErrore(string value)
        {
            lock (queueErrors)
            {
                if (lastAnalisi != "")
                {
                    queueErrors.Enqueue("Errore nella fase " + lastAnalisi);
                    lastAnalisi = "";
                }
                queueErrors.Enqueue(value);
            }
            if (threadWriteErrors == null)
            {
                threadWriteErrors = new System.Threading.Thread(new System.Threading.ThreadStart(WriteErrors));
                threadWriteErrors.Start();
            }
            if (EventAddError != null) EventAddError(value);
        }

        private static void WriteErrors()
        {
            
            System.IO.FileInfo oFInfo = new System.IO.FileInfo(ApplicationExecutablePath);
            FileLogErrori = oFInfo.Directory.FullName + @"\errori.txt";
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(FileLogErrori, false, Encoding.UTF8))
            {
                string last = "";
                while (Utils.Executing || queueErrors.Count > 0)
                {
                    if (queueErrors.Count > 0)
                    {
                        errorExists = true;
                        lock (queueErrors)
                        {
                            while (queueErrors.Count > 0)
                            {
                                string value = queueErrors.Dequeue();
                                if (value != last)
                                {
                                    sw.WriteLine(value);
                                    last = value;
                                }
                            }
                        }
                    }

                }
                sw.Flush();
                sw.Close();
                sw.Dispose();
                SetEndMode(errorExists);
            }
        }

        private static void Logs_Message(string carta, long? minProgressivo, long? maxProgressivo, string value, Exception error)
        {
            string message = string.Format("CARTA {0} {1}{2}{3}{4}",
                                            carta, value,
                                            minProgressivo != null ? " " + minProgressivo.ToString() + " " : "",
                                            maxProgressivo != null ? " " + maxProgressivo.ToString() + " " : "",
                                            error != null ? " " + error.Message + " " : "");
            if (error == null)
                AddAnalisi(message);
            else
                AddErrore(message);
        }

        public static void StartExecution(string rootPath, string dbConnection, string applicationExecutablePath)
        {
            RootPath = rootPath;
            DbConnection = dbConnection;
            ApplicationExecutablePath = applicationExecutablePath;
            System.Threading.Thread oThread = new System.Threading.Thread(new System.Threading.ThreadStart(Execution));
            oThread.Start();
        }

        private static void SetEndMode(bool errorExists)
        {
            lock (queueErrors)
            {
                lExecuting = queueErrors.Count == 0;
                if (EventEndMode != null) EventEndMode(errorExists);
            }
        }

        private static void Execution()
        {
            MaxAnalisi = 31;
            CntAnalisi = 0;
            stopProcess = false;
            lExecuting = true;

            List<System.IO.DirectoryInfo> Directories = new List<System.IO.DirectoryInfo>();
            List<clsFileRiepilogo> filesLOG = new List<clsFileRiepilogo>();
            DateTime? dataMigrazione = null;
            bool lExistsOldLogs = false;
            List<clsCarta> oldLogsCarte = null;
            clsLog Logs = new clsLog();
            Logs.Message += Logs_Message;
            clsLog LogsDB = new clsLog();

            List<clsFileRiepilogo> filesLTA = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> filesRPG = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> filesRPM = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> filesRCA = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> giorniMinMaxEmissione = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> giorniMinMaxEmissione_RPM = new List<clsFileRiepilogo>();
            List<clsFileRiepilogo> giorniMinMaxEmissione_LTA = new List<clsFileRiepilogo>();


            // Caricamento cartelle
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione cartelle...");
                Directories = InitTree(RootPath);
            }

            // Caricamento LOG
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione files LOG...");
                filesLOG = GetFiles(Directories, "LOG_", "p7m");
                AddAnalisi(string.Format(" {0} files LOG trovati", filesLOG.Count.ToString()));
                MaxSubCounter = filesLOG.Count;
                foreach (clsFileRiepilogo item in filesLOG)
                {
                    CntSubCounter += 1;
                    System.IO.FileInfo fileInfo = item.FileInfo;
                    string fileName = fileInfo.FullName;
                    DateTime giorno = Utils.GetDataFile(fileInfo.Name);
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    string content = System.IO.File.ReadAllText(fileInfo.FullName);
                    content = content.Substring(content.IndexOf(@"<?xml version="));
                    int last = content.LastIndexOf("</LogTransazione>", content.Length - 1);
                    content = content.Substring(0, last + @"</LogTransazione>".Length);
                    doc.LoadXml(content);
                    long riga = 0;
                    foreach (System.Xml.XmlNode node in doc.LastChild.ChildNodes)
                    {
                        riga++;
                        DateTime giornoEmissione = new DateTime(int.Parse(node.Attributes["DataEmissione"].Value.Substring(0, 4)),
                                                                int.Parse(node.Attributes["DataEmissione"].Value.Substring(4, 2)),
                                                                int.Parse(node.Attributes["DataEmissione"].Value.Substring(6, 2)));

                        clsProgressivo progressivo = new clsProgressivo(node.Attributes["CartaAttivazione"].Value,
                                                                        long.Parse(node.Attributes["NumeroProgressivo"].Value),
                                                                        fileName, giorno, giornoEmissione, riga, true, false);
                        clsCarta carta = Logs.Carte.FirstOrDefault(c => c.Carta == progressivo.Carta);
                        if (carta == null)
                        {
                            carta = new clsCarta(progressivo.Carta);
                            carta.LastRiga = 1;
                            progressivo.Riga = carta.LastRiga;
                            Logs.AddCarta(carta);
                        }
                        else
                        {
                            carta.LastRiga = 1;
                            progressivo.Riga = carta.LastRiga;
                        }
                        carta.Progressivi.Add(progressivo);
                        if (node.FirstChild != null &&
                            (node.FirstChild.Name.StartsWith("TitoloAccesso") || node.FirstChild.Name.StartsWith("BigliettoAbbonamento")))
                        {
                            System.Xml.XmlNode nodeDataEvento = node.FirstChild.ChildNodes.Cast<System.Xml.XmlNode>().FirstOrDefault(n => n.Name == "DataEvento");
                            if (nodeDataEvento != null)
                                progressivo.GiornoEvento = new DateTime(int.Parse(nodeDataEvento.InnerText.Substring(0, 4)),
                                                                        int.Parse(nodeDataEvento.InnerText.Substring(4, 2)),
                                                                        int.Parse(nodeDataEvento.InnerText.Substring(6, 2)));
                        }
                        else if (node.FirstChild != null && node.FirstChild.Name.StartsWith("Abbonamento"))
                        {
                            progressivo.GiornoAbbonamento = progressivo.GiornoEmissione;
                        }
                    }
                }
            }

            if (Logs.Carte.Count > 0)
                Logs.Analizza();

            // Caricamento LTA
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione files LTA...");
                filesLTA = GetFiles(Directories, "LTA_", "p7m");
                AddAnalisi(string.Format(" {0} files LTA trovati", filesLTA.Count.ToString()));
            }

            // Caricamento RPG
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione files RPG...");
                filesRPG = GetFiles(Directories, "RPG_", "p7m");
                AddAnalisi(string.Format(" {0} files RPG trovati", filesRPG.Count.ToString()));
            }

            // Caricamento RCA
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione files RCA...");
                filesRCA = GetFiles(Directories, "RCA_", "p7m");
                AddAnalisi(string.Format(" {0} files RCA trovati", filesRCA.Count.ToString()));
            }

            // Caricamento RPM
            if (!stopProcess)
            {
                AddAnalisi("Inizializzazione files RPM...");
                filesRPM = GetFiles(Directories, "RPM_", "p7m");
                AddAnalisi(string.Format(" {0} files RPM trovati", filesRPM.Count.ToString()));
            }

            // Analisi con database
            if (!stopProcess && DbConnection != "")
            {
                AddAnalisi("Connessione database...");
                Wintic.Data.oracle.CConnectionOracle conn = new Wintic.Data.oracle.CConnectionOracle();
                try
                {
                    conn.ConnectionString = string.Format("USER=WTIC;PASSWORD=OBELIX;DATA SOURCE={0};", DbConnection);
                    conn.Open();
                    try
                    {
                        AddAnalisi("Lettura Codice sistema da DB...");
                        string codiceSistema = "";
                        System.Data.DataTable tableCS = conn.ExecuteQuery("SELECT CODICE_SISTEMA FROM WTIC.SMART_CS");
                        if (tableCS != null && tableCS.Rows != null && tableCS.Rows.Count > 0)
                        {
                            codiceSistema = tableCS.Rows[0]["CODICE_SISTEMA"].ToString();

                            dataMigrazione = GetDataMigrazione(conn, codiceSistema, out lExistsOldLogs, out oldLogsCarte);

                            if (Logs != null && dataMigrazione != null && oldLogsCarte != null && oldLogsCarte.Count > 0)
                            {
                                Logs.Carte.ForEach(c =>
                                {
                                    clsCarta oldCarta = oldLogsCarte.FirstOrDefault(cOld => cOld.Carta == c.Carta);
                                    if (oldCarta != null && ((c.Mancanti != null && c.Mancanti.Count > 0) || c.Minimo > 1))
                                        AddErrore(string.Format(" Considerare per la carta {0} che ci sono vecchi log progressivi {1} - {2}, emessi dal {3} - {4}", oldCarta.Carta, oldCarta.Minimo, oldCarta.Massimo, (oldCarta.MinDataEmi == null ? "" : oldCarta.MinDataEmi.Value.ToShortDateString()), (oldCarta.MaxDataEmi == null ? "" : oldCarta.MaxDataEmi.Value.ToShortDateString())));
                                });
                            }

                            AddAnalisi("Lettura Log da DB...");
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("SELECT TITOLO_ABBONAMENTO");
                            oSB.Append("       ,CODICE_CARTA");
                            oSB.Append("       ,PROGRESSIVO_TITOLO");
                            oSB.Append("       ,TRUNC(DATA_ORA_EMISSIONE) AS GIORNO");
                            oSB.Append("       ,CASE WHEN TITOLO_ABBONAMENTO = 'T' THEN TRUNC(DATA_ORA_EVENTO) ELSE NULL END AS GIORNO_EVENTO");
                            oSB.Append("       ,MAX(DATA_ORA_EMISSIONE) OVER (PARTITION BY TRUNC(DATA_ORA_EMISSIONE)) AS MAX_DATA_ORA_EMISSIONE");
                            oSB.Append(" FROM WTIC.LOG_TRANSAZIONI");
                            oSB.Append(" WHERE CODICE_SISTEMA = :pCODICE_SISTEMA");
                            oSB.Append(" ORDER BY CODICE_CARTA, PROGRESSIVO_TITOLO");
                            System.Data.DataTable tableLogs = conn.ExecuteQuery(oSB.ToString(), new Wintic.Data.clsParameters(":pCODICE_SISTEMA", codiceSistema));
                            if (tableLogs != null && tableLogs.Rows != null && tableLogs.Rows.Count > 0)
                            {
                                MaxSubCounter = tableLogs.Rows.Count;

                                AddAnalisi("Confronto logs files e DB...");
                                List<string> carteNonTrovate = new List<string>();
                                foreach (DataRow row in tableLogs.Rows)
                                {
                                    CntSubCounter += 1;
                                    clsProgressivo progressivo = new clsProgressivo(row["CODICE_CARTA"].ToString(), long.Parse(row["PROGRESSIVO_TITOLO"].ToString()), "", (DateTime)row["GIORNO"], (DateTime)row["GIORNO"], 0, false, true);

                                    clsFileRiepilogo fileMinMaxEmissione = giorniMinMaxEmissione.FirstOrDefault(g => g.Giorno.Date == progressivo.Giorno.Date);
                                    if (fileMinMaxEmissione == null)
                                    {
                                        fileMinMaxEmissione = new clsFileRiepilogo(progressivo.Giorno.Date, (DateTime)row["MAX_DATA_ORA_EMISSIONE"]);
                                        giorniMinMaxEmissione.Add(fileMinMaxEmissione);
                                    }
                                    else
                                    {
                                        if (fileMinMaxEmissione.MaxDataEmi > (DateTime)row["MAX_DATA_ORA_EMISSIONE"])
                                            fileMinMaxEmissione.MaxDataEmi = (DateTime)row["MAX_DATA_ORA_EMISSIONE"];
                                    }




                                    if (row["TITOLO_ABBONAMENTO"].ToString() == "T")
                                        progressivo.GiornoEvento = (DateTime)row["GIORNO_EVENTO"];
                                    else
                                        progressivo.GiornoAbbonamento = progressivo.GiornoEmissione;

                                    clsCarta carta = Logs.Carte.FirstOrDefault(c => c.Carta == progressivo.Carta);
                                    clsCarta cartaDB = LogsDB.Carte.FirstOrDefault(c => c.Carta == progressivo.Carta);
                                    if (cartaDB == null)
                                    {
                                        cartaDB = new clsCarta(progressivo.Carta);
                                        LogsDB.AddCarta(cartaDB);
                                    }
                                    if (carta == null)
                                    {
                                        if (!carteNonTrovate.Contains(progressivo.Carta))
                                        {
                                            carteNonTrovate.Add(progressivo.Carta);
                                            AddErrore(string.Format("carta {0} trovata nel database ma non nei file di log", progressivo.Carta));
                                        }
                                    }
                                    cartaDB.Progressivi.Add(progressivo);
                                }
                                CntSubCounter = MaxSubCounter;

                                oSB = new StringBuilder();
                                oSB.Append("SELECT TITOLO_ABBONAMENTO, DATA_GIORNO, MAX(DATA_ORA_EMISSIONE) AS MAX_DATA_ORA_EMISSIONE");
                                oSB.Append(" FROM");
                                oSB.Append(" (");

                                StringBuilder oSBTransazioni = new StringBuilder();

                                oSBTransazioni.Append("  SELECT TITOLO_ABBONAMENTO");
                                oSBTransazioni.Append("        ,TRUNC(DATA_ORA_EVENTO) AS DATA_GIORNO");
                                oSBTransazioni.Append("        ,MAX(DATA_ORA_EMISSIONE) AS DATA_ORA_EMISSIONE");
                                oSBTransazioni.Append("  FROM WTIC.LOG_TRANSAZIONI");
                                oSBTransazioni.Append("  WHERE CODICE_SISTEMA = :pCODICE_SISTEMA AND TITOLO_ABBONAMENTO = 'T'");
                                oSBTransazioni.Append("  GROUP BY TITOLO_ABBONAMENTO, TRUNC(DATA_ORA_EVENTO)");
                                oSBTransazioni.Append("  UNION ALL");
                                oSBTransazioni.Append("  SELECT TITOLO_ABBONAMENTO");
                                oSBTransazioni.Append("        ,F_INIZIO_MESE(DATA_ORA_EMISSIONE) AS DATA_GIORNO");
                                oSBTransazioni.Append("        ,MAX(DATA_ORA_EMISSIONE) AS DATA_ORA_EMISSIONE");
                                oSBTransazioni.Append("  FROM WTIC.LOG_TRANSAZIONI");
                                oSBTransazioni.Append("  WHERE CODICE_SISTEMA = :pCODICE_SISTEMA AND TITOLO_ABBONAMENTO = 'A'");
                                oSBTransazioni.Append("  GROUP BY TITOLO_ABBONAMENTO, F_INIZIO_MESE(DATA_ORA_EMISSIONE)");

                                oSB.Append(oSBTransazioni.ToString());
                                oSB.Append(" UNION ALL");
                                oSB.Append(oSBTransazioni.ToString().Replace("WTIC.LOG_TRANSAZIONI", "WTIC.LOG_TRANSAZIONI_ARCHIVE"));

                                oSB.Append(" UNION ALL");
                                oSB.Append(" SELECT TITOLO_ABBONAMENTO");
                                oSB.Append("       ,TRUNC(DATA_ORA_EVENTO) AS DATA_GIORNO");
                                oSB.Append("       ,MAX(DATAORA_EMISSIONE_ANNULLAMENTO) AS DATA_ORA_EMISSIONE");
                                oSB.Append(" FROM VR_RATEI_FISSI");
                                oSB.Append(" WHERE CODICE_SISTEMA = :pCODICE_SISTEMA");
                                oSB.Append(" GROUP BY TITOLO_ABBONAMENTO, TRUNC(DATA_ORA_EVENTO)");
                                oSB.Append(" )");
                                oSB.Append(" GROUP BY TITOLO_ABBONAMENTO, DATA_GIORNO");

                                DataTable tableLogsGiorni = conn.ExecuteQuery(oSB.ToString(), new Wintic.Data.clsParameters(":pCODICE_SISTEMA", codiceSistema));
                                if (tableLogsGiorni != null && tableLogsGiorni.Rows != null && tableLogsGiorni.Rows.Count > 0)
                                {
                                    MaxSubCounter = tableLogsGiorni.Rows.Count;
                                    foreach (DataRow row in tableLogsGiorni.Rows)
                                    {
                                        CntSubCounter += 1;
                                        clsFileRiepilogo fileMinMaxEmissione = null;
                                        DateTime dataGiorno = (DateTime)row["DATA_GIORNO"];
                                        DateTime dataInizioMese = new DateTime(dataGiorno.Year, dataGiorno.Month, 1);
                                        DateTime maxDataEmi = (DateTime)row["MAX_DATA_ORA_EMISSIONE"];
                                        if (row["TITOLO_ABBONAMENTO"].ToString() == "T")
                                        {
                                            fileMinMaxEmissione = giorniMinMaxEmissione_LTA.FirstOrDefault(g => g.Giorno.Date == dataGiorno.Date);
                                            if (fileMinMaxEmissione == null)
                                            {
                                                fileMinMaxEmissione = new clsFileRiepilogo(dataGiorno, maxDataEmi);
                                                giorniMinMaxEmissione_LTA.Add(fileMinMaxEmissione);
                                            }
                                            else if (fileMinMaxEmissione.MaxDataEmi < maxDataEmi)
                                                fileMinMaxEmissione.MaxDataEmi = maxDataEmi;
                                        }

                                        fileMinMaxEmissione = giorniMinMaxEmissione_RPM.FirstOrDefault(g => g.Giorno.Date == dataInizioMese);
                                        if (fileMinMaxEmissione == null)
                                        {
                                            fileMinMaxEmissione = new clsFileRiepilogo(dataInizioMese, maxDataEmi);
                                            giorniMinMaxEmissione_RPM.Add(fileMinMaxEmissione);
                                        }
                                        else if (fileMinMaxEmissione.MaxDataEmi < maxDataEmi)
                                            fileMinMaxEmissione.MaxDataEmi = maxDataEmi;
                                    }
                                    CntSubCounter = MaxSubCounter;
                                }

                                AddAnalisi("Ricerca progressivi nei files sul DB...");

                                Logs.Carte.ForEach(c =>
                                {
                                    clsCarta cDB = LogsDB.Carte.FirstOrDefault(carta => carta.Carta == c.Carta);
                                    if (cDB != null)
                                    {
                                        MaxSubCounter = c.Progressivi.Count;
                                        c.Progressivi.ForEach(p =>
                                        {
                                            CntSubCounter += 1;
                                            p.EsisteNelDB = cDB.Progressivi.Exists(pDB => pDB.Progressivo == p.Progressivo);
                                            if (!p.EsisteNelDB && !cDB.Mancanti.Contains(p.Progressivo))
                                                cDB.Mancanti.Add(p.Progressivo);
                                        });
                                        CntSubCounter = MaxSubCounter;
                                    }

                                });

                                AddAnalisi("Ricerca progressivi del DB nei files...");

                                LogsDB.Carte.ForEach(cDB =>
                                {
                                    clsCarta c = Logs.Carte.FirstOrDefault(carta => carta.Carta == cDB.Carta);
                                    if (c != null)
                                    {
                                        MaxSubCounter = cDB.Progressivi.Count;
                                        cDB.Progressivi.ForEach(pDB =>
                                        {
                                            CntSubCounter += 1;
                                            pDB.EsisteNelFile = c.Progressivi.Exists(p => p.Progressivo == pDB.Progressivo);
                                            if (!pDB.EsisteNelFile && !c.Mancanti.Contains(pDB.Progressivo))
                                                c.Mancanti.Add(pDB.Progressivo);
                                        });
                                        CntSubCounter = MaxSubCounter;
                                    }
                                });

                                AddAnalisi("Ricerca giorni mancanti nei file e presenti sul DB...");
                                List<clsRange> giorniInteriMancanti = new List<clsRange>();
                                List<clsRange> giorniInteriMancantiPreMigrazione = new List<clsRange>();
                                LogsDB.Carte.ForEach(cDB =>
                                {
                                    clsCarta c = Logs.Carte.FirstOrDefault(carta => carta.Carta == cDB.Carta);
                                    if (c != null)
                                    {
                                        MaxSubCounter = cDB.Progressivi.Count;
                                        cDB.Progressivi.GroupBy(pDB => pDB.GiornoEmissione).ToList().ForEach(pDB =>
                                        {
                                            CntSubCounter += 1;
                                            if (!c.Progressivi.Exists(p => p.GiornoEmissione == pDB.Key))
                                            {
                                                clsRange rangeGiornoMancante = null;
                                                if (dataMigrazione != null && pDB.Key < dataMigrazione)
                                                {
                                                    rangeGiornoMancante = giorniInteriMancantiPreMigrazione.FirstOrDefault(range => range.Fine == pDB.Key.AddDays(-1));
                                                    if (rangeGiornoMancante == null)
                                                    {
                                                        rangeGiornoMancante = new clsRange(string.Format("carta {0} interi giorni mancanti (prima della migrazione) nei file", c.Carta), pDB.Key);
                                                        giorniInteriMancantiPreMigrazione.Add(rangeGiornoMancante);
                                                    }
                                                    else
                                                        rangeGiornoMancante.Fine = pDB.Key;
                                                }
                                                else
                                                {
                                                    rangeGiornoMancante = giorniInteriMancanti.FirstOrDefault(range => range.Fine == pDB.Key.AddDays(-1));
                                                    if (rangeGiornoMancante == null)
                                                    {
                                                        rangeGiornoMancante = new clsRange(string.Format("carta {0} interi giorni mancanti nei file", c.Carta), pDB.Key);
                                                        giorniInteriMancanti.Add(rangeGiornoMancante);
                                                    }
                                                    else
                                                        rangeGiornoMancante.Fine = pDB.Key;
                                                }
                                            }
                                        });
                                        CntSubCounter = MaxSubCounter;
                                    }
                                });

                                if (giorniInteriMancanti.Count > 0)
                                {
                                    giorniInteriMancanti.ForEach(range =>
                                    {
                                        AddErrore(string.Format("{0}  {1} - {2}", range.Messaggio, range.Inizio.ToShortDateString(), range.Fine.ToShortDateString()));
                                    });
                                }
                                if (giorniInteriMancantiPreMigrazione.Count > 0)
                                {
                                    giorniInteriMancantiPreMigrazione.ForEach(range =>
                                    {
                                        AddWarning(string.Format("{0}  {1} - {2}", range.Messaggio, range.Inizio.ToShortDateString(), range.Fine.ToShortDateString()));
                                    });
                                }

                                AddAnalisi("Ricerca giorni mancanti nel DB e presenti sui files...");
                                Logs.Carte.ForEach(c =>
                                {
                                    clsCarta cDB = LogsDB.Carte.FirstOrDefault(carta => carta.Carta == c.Carta);
                                    if (cDB != null)
                                    {
                                        MaxSubCounter = c.Progressivi.Count;
                                        c.Progressivi.GroupBy(p => p.GiornoEmissione).ToList().ForEach(p =>
                                        {
                                            CntSubCounter += 1;
                                            if (!cDB.Progressivi.Exists(pDB => pDB.GiornoEmissione == p.Key))
                                                AddErrore(string.Format("carta {0} intero giorno mancante nel DB {1}", c.Carta, p.Key.ToShortDateString()));
                                        });
                                        CntSubCounter = MaxSubCounter;
                                    }
                                });


                                Logs.Carte.Where(c => c.Mancanti.Count > 0).ToList().ForEach(c =>
                                {
                                    c.Mancanti.Sort();
                                    clsCarta tempMinMax = null;
                                    c.Mancanti.ForEach(p =>
                                    {
                                        if (tempMinMax == null)
                                        {
                                            tempMinMax = new clsCarta(c.Carta);
                                            tempMinMax.Minimo = p;
                                            tempMinMax.Massimo = p;
                                            c.RangesMancanti.Add(tempMinMax);
                                        }
                                        else
                                        {
                                            if (p == tempMinMax.Massimo + 1)
                                                tempMinMax.Massimo = p;
                                            else
                                            {
                                                tempMinMax = new clsCarta(c.Carta);
                                                tempMinMax.Minimo = p;
                                                tempMinMax.Massimo = p;
                                                c.RangesMancanti.Add(tempMinMax);
                                            }
                                        }
                                    });
                                    if (c.RangesMancanti.Count > 0)
                                        c.RangesMancanti.ForEach(rangeMancante => AddErrore(string.Format("mancante nei files {0}  {1} - {2}", c.Carta, rangeMancante.Minimo, rangeMancante.Massimo)));
                                });


                                LogsDB.Carte.Where(cDB => cDB.Mancanti.Count > 0).ToList().ForEach(cDB =>
                                {
                                    cDB.Mancanti.Sort();
                                    clsCarta tempMinMax = null;
                                    cDB.Mancanti.ForEach(p =>
                                    {
                                        if (tempMinMax == null)
                                        {
                                            tempMinMax = new clsCarta(cDB.Carta);
                                            tempMinMax.Minimo = p;
                                            tempMinMax.Massimo = p;
                                            cDB.RangesMancanti.Add(tempMinMax);
                                        }
                                        else
                                        {
                                            if (p == tempMinMax.Massimo + 1)
                                                tempMinMax.Massimo = p;
                                            else
                                            {
                                                tempMinMax = new clsCarta(cDB.Carta);
                                                tempMinMax.Minimo = p;
                                                tempMinMax.Massimo = p;
                                                cDB.RangesMancanti.Add(tempMinMax);
                                            }
                                        }
                                    });
                                    if (cDB.RangesMancanti.Count > 0)
                                        cDB.RangesMancanti.ForEach(rangeMancante => AddErrore(string.Format("mancante dal database {0}  {1} - {2}", cDB.Carta, rangeMancante.Minimo, rangeMancante.Massimo)));
                                });


                            }
                            else
                                AddErrore("Nessun LOG trovato nel database");
                        }
                        else
                            AddErrore("Nessun codice sistema trovato nel database");
                    }
                    catch (Exception exElaborazione)
                    {
                        AddErrore("Errore di elaborazione " + exElaborazione.Message);
                    }
                }
                catch (Exception ex)
                {
                    AddErrore("Errore di connessione " + ex.Message);

                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                        conn = null;
                    }
                }
            }
            else
                CntAnalisi += 8;

            AddAnalisi("Raccolta giorni/mesi di emissione e giorni eventi...");

            List<DateTime> giorniLogs = new List<DateTime>();
            List<DateTime> giorniEventi = new List<DateTime>();
            List<DateTime> giorniEventiMese = new List<DateTime>();
            List<DateTime> giorniAbbonamenti = new List<DateTime>();
            Logs.Carte.ForEach(c => c.Progressivi.GroupBy(p => p.GiornoEmissione).ToList().ForEach(p => { if (!giorniLogs.Contains(p.Key)) giorniLogs.Add(p.Key); }));
            LogsDB.Carte.ForEach(c => c.Progressivi.GroupBy(p => p.GiornoEmissione).ToList().ForEach(p => { if (!giorniLogs.Contains(p.Key)) giorniLogs.Add(p.Key); }));

            Logs.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoEvento != null).GroupBy(p => p.GiornoEvento).ToList().ForEach(p => { if (!giorniEventi.Contains(p.Key.Value)) giorniEventi.Add(p.Key.Value); }));
            LogsDB.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoEvento != null).GroupBy(p => p.GiornoEvento).ToList().ForEach(p => { if (!giorniEventi.Contains(p.Key.Value)) giorniEventi.Add(p.Key.Value); }));

            Logs.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoEvento != null).GroupBy(p => new DateTime(p.GiornoEvento.Value.Year, p.GiornoEvento.Value.Month, 1)).ToList().ForEach(p => { if (!giorniEventiMese.Contains(p.Key)) giorniEventiMese.Add(p.Key); }));
            LogsDB.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoEvento != null).GroupBy(p => new DateTime(p.GiornoEvento.Value.Year, p.GiornoEvento.Value.Month, 1)).ToList().ForEach(p => { if (!giorniEventiMese.Contains(p.Key)) giorniEventiMese.Add(p.Key); }));

            Logs.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoAbbonamento != null).GroupBy(p => new DateTime(p.GiornoAbbonamento.Value.Year, p.GiornoAbbonamento.Value.Month, 1)).ToList().ForEach(p => { if (!giorniAbbonamenti.Contains(p.Key)) giorniAbbonamenti.Add(p.Key); }));
            LogsDB.Carte.ForEach(c => c.Progressivi.Where(p => p.GiornoAbbonamento != null).GroupBy(p => new DateTime(p.GiornoAbbonamento.Value.Year, p.GiornoAbbonamento.Value.Month, 1)).ToList().ForEach(p => { if (!giorniAbbonamenti.Contains(p.Key)) giorniAbbonamenti.Add(p.Key); }));

            giorniLogs.Sort();
            giorniEventi.Sort();
            giorniEventiMese.Sort();
            giorniAbbonamenti.Sort();

            DateTime dToday = DateTime.Now.Date;

            AddAnalisi("Verifica files RPG mancanti...");

            List<DateTime> listaDate = new List<DateTime>();

            listaDate = new List<DateTime>();

            giorniLogs.Where(d => !filesRPG.Exists(f => Utils.GetDataFile(f.Name) == d)).ToList().ForEach(d => listaDate.Add(d));

            if (listaDate.Count > 0)
                DescribeRangesErrors("RPG mancanti", listaDate, dataMigrazione);


            if (giorniMinMaxEmissione.Count > 0)
            {

                AddAnalisi("Verifica files RPG generati in anticipo in base all'ultima emissione per giorno...");
                listaDate = new List<DateTime>();
                MaxSubCounter = giorniMinMaxEmissione.Count;
                giorniMinMaxEmissione.ForEach(g =>
                {
                    CntSubCounter += 1;
                    List<clsFileRiepilogo> lista = filesRPG.Where(f => Utils.GetDataFile(f.Name) == g.Giorno).ToList();
                    if (lista != null && lista.Count > 0)
                    {
                        int nMaxProg = lista.ToList().Max(f => int.Parse(f.Name.Substring(15, 3)));
                        clsFileRiepilogo fileRiepilogo = lista.LastOrDefault(f => Utils.GetDataFile(f.Name) == g.Giorno && int.Parse(f.Name.Substring(15, 3)) == nMaxProg);
                        if (fileRiepilogo.DataOraGenerazione < g.MaxDataEmi && !listaDate.Contains(g.Giorno))
                            listaDate.Add(g.Giorno);
                    }
                });
                CntSubCounter = MaxSubCounter;

                if (listaDate.Count > 0)
                    DescribeRangesErrors("RPG generati in anticipo in base all'ultima emissione per giorno", listaDate, dataMigrazione, true);
            }

            AddAnalisi("Verifica files RPM mancanti...");

            listaDate = new List<DateTime>();
            giorniEventiMese.Where(d => d.Date <= dToday.Date && !filesRPM.Exists(f => Utils.GetDataFile(f.Name, true) == d)).ToList().ForEach(d => listaDate.Add(d));
            if (listaDate.Count > 0)
                DescribeRangesErrors("RPM mancanti per eventi nel mese", listaDate, dataMigrazione, true);

            listaDate = new List<DateTime>();
            giorniAbbonamenti.Where(d => d.Date <= dToday.Date && !filesRPM.Exists(f => Utils.GetDataFile(f.Name, true) == d)).ToList().ForEach(d => listaDate.Add(d));
            if (listaDate.Count > 0)
                DescribeRangesErrors("RPM mancanti per abbonamenti venduti nel mese", listaDate, dataMigrazione, true);

            if (giorniMinMaxEmissione_RPM.Count > 0)
            {
                MaxSubCounter = giorniMinMaxEmissione_RPM.Count;
                AddAnalisi("Verifica files RPM generati in anticipo in base all'ultima emissione per eventi o abbonamenti nel mese...");
                listaDate = new List<DateTime>();
                giorniMinMaxEmissione_RPM.ForEach(g =>
                {
                    CntSubCounter += 1;
                    List<clsFileRiepilogo> lista = filesRPM.Where(f => Utils.GetDataFile(f.Name, true) == g.Giorno).ToList();
                    if (lista != null && lista.Count > 0)
                    {
                        int nMaxProg = lista.ToList().Max(f => int.Parse(f.Name.Substring(15, 3)));
                        clsFileRiepilogo fileRiepilogo = lista.LastOrDefault(f => Utils.GetDataFile(f.Name, true) == g.Giorno && int.Parse(f.Name.Substring(15, 3)) == nMaxProg);
                        if (fileRiepilogo.DataOraGenerazione < g.MaxDataEmi && !listaDate.Contains(g.Giorno))
                            listaDate.Add(g.Giorno);
                    }
                });
                CntSubCounter = MaxSubCounter;

                if (listaDate.Count > 0)
                    DescribeRangesErrors("RPM generati in anticipo in base all'ultima emissione per eventi o abbonamenti nel mese", listaDate, dataMigrazione, true);
            }

            AddAnalisi("Verifica files LTA mancanti...");

            listaDate = new List<DateTime>();

            giorniEventi.Where(d => d.Date <= dToday.Date && !filesLTA.Exists(f => Utils.GetDataFile(f.Name) == d)).ToList().ForEach(d => listaDate.Add(d));

            if (listaDate.Count > 0)
                DescribeRangesErrors("LTA mancanti", listaDate, dataMigrazione);

            AddAnalisi("Verifica files RCA mancanti...");

            listaDate = new List<DateTime>();

            giorniEventi.Where(d => d.Date <= dToday.Date && !filesRCA.Exists(f => Utils.GetDataFile(f.Name) == d)).ToList().ForEach(d => listaDate.Add(d));

            if (listaDate.Count > 0)
                DescribeRangesErrors("RCA mancanti", listaDate, dataMigrazione);

            if (giorniMinMaxEmissione_LTA.Count > 0)
            {
                AddAnalisi("Verifica files RCA generati in anticipo in base all'ultima emissione per evento nel giorno...");
                listaDate = new List<DateTime>();
                giorniMinMaxEmissione_LTA.ForEach(g =>
                {
                    List<clsFileRiepilogo> lista = filesRCA.Where(f => Utils.GetDataFile(f.Name) == g.Giorno).ToList();
                    if (lista != null && lista.Count > 0)
                    {
                        int nMaxProg = lista.ToList().Max(f => int.Parse(f.Name.Substring(15, 3)));
                        clsFileRiepilogo fileRiepilogo = lista.LastOrDefault(f => Utils.GetDataFile(f.Name) == g.Giorno && int.Parse(f.Name.Substring(15, 3)) == nMaxProg);
                        if (fileRiepilogo.DataOraGenerazione < g.MaxDataEmi && !listaDate.Contains(g.Giorno))
                            listaDate.Add(g.Giorno);
                    }
                });

                if (listaDate.Count > 0)
                    DescribeRangesErrors("RCA generati in anticipo in base all'ultima emissione per evento nel giorno", listaDate, dataMigrazione, false);
            }
            MaxSubCounter = 1;
            CntSubCounter = 1;
            AddAnalisi("FINE.");
            lExecuting = false;
            SetEndMode(errorExists);
            if (threadWriteErrors != null) threadWriteErrors.Join();
        }

        private static List<System.IO.DirectoryInfo> InitTree(string path)
        {
            List<System.IO.DirectoryInfo> result = new List<System.IO.DirectoryInfo>();
            Queue<string> list = new Queue<string>();
            list.Enqueue(path);
            while (list.Count > 0 && !stopProcess)
            {
                string itemPath = list.Dequeue();
                result.Add(new System.IO.DirectoryInfo(itemPath));
                List<System.IO.DirectoryInfo> subFolders = GetDirs(itemPath);
                foreach (System.IO.DirectoryInfo folder in subFolders)
                {
                    list.Enqueue(folder.FullName);
                }
            }
            return result;
        }
        private static List<System.IO.DirectoryInfo> GetDirs(string path)
        {
            List<System.IO.DirectoryInfo> result = new List<System.IO.DirectoryInfo>();
            foreach (string subPath in System.IO.Directory.GetDirectories(path))
            {
                result.Add(new System.IO.DirectoryInfo(subPath));
            }
            return result;
        }

        private static List<clsFileRiepilogo> GetFiles(List<System.IO.DirectoryInfo> Directories, string wildCard, string ext)
        {
            List<clsFileRiepilogo> result = new List<clsFileRiepilogo>();
            foreach (System.IO.DirectoryInfo oDirInfo in Directories)
            {
                List<System.IO.FileInfo> files = new List<System.IO.FileInfo>(oDirInfo.GetFiles(wildCard + "*." + ext));
                foreach (System.IO.FileInfo fInfo in files)
                {
                    clsFileRiepilogo fileRiepilogo = new clsFileRiepilogo(fInfo);
                    if (fileRiepilogo.Valido)
                        result.Add(fileRiepilogo);
                }
            }
            return result;
        }

        private static DateTime? GetDataMigrazione(Wintic.Data.IConnection connection, string codiceSistema, out bool lExistsOldLogs, out List<clsCarta> oldLogsCarte)
        {
            AddAnalisi("Lettura data di migrazione e verifica eventuali vecchi log...");
            lExistsOldLogs = false;
            oldLogsCarte = null;
            DateTime? result = null;
            try
            {
                DataTable tableCheck = connection.ExecuteQuery("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = 'TZ_OLDS_LOGS_DATA_MIGRAZIONE'");
                if (tableCheck != null && tableCheck.Rows != null && tableCheck.Rows.Count > 0)
                {
                    DataTable tableMigrazione = connection.ExecuteQuery("SELECT DATA_MIGRAZIONE FROM TZ_OLDS_LOGS_DATA_MIGRAZIONE");
                    if (tableMigrazione != null && tableMigrazione.Rows != null && tableMigrazione.Rows.Count > 0)
                    {
                        if (tableMigrazione.Rows[0]["DATA_MIGRAZIONE"] != System.DBNull.Value)
                        {
                            result = (DateTime)tableMigrazione.Rows[0]["DATA_MIGRAZIONE"];
                        }
                    }
                }

                tableCheck = connection.ExecuteQuery("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = 'LOGTRANS_KEEP'");
                if (tableCheck != null && tableCheck.Rows != null && tableCheck.Rows.Count > 0)
                {
                    StringBuilder oSB = new StringBuilder();

                    oSB.Append("SELECT CODICE_CARTA");
                    oSB.Append("       ,MIN(MIN_DATA_EMI) AS MIN_DATA_EMI");
                    oSB.Append("       ,MAX(MAX_DATA_EMI) AS MAX_DATA_EMI");
                    oSB.Append("       ,MIN(MIN_PROGRESSIVO) AS MIN_PROGRESSIVO");
                    oSB.Append("       ,MAX(MAX_PROGRESSIVO) MAX_PROGRESSIVO");
                    oSB.Append(" FROM (SELECT SUBSTR(LOG_DATA,117,8) AS CODICE_CARTA");
                    oSB.Append("             ,MIN(TO_DATE(SUBSTR(LOG_DATA,65,12),'YYYYMMDDHH24MI')) AS MIN_DATA_EMI");
                    oSB.Append("             ,MAX(TO_DATE(SUBSTR(LOG_DATA,65,12),'YYYYMMDDHH24MI')) AS MAX_DATA_EMI");
                    oSB.Append("             ,MIN(TO_NUMBER(SUBSTR(LOG_DATA,77,8))) AS MIN_PROGRESSIVO");
                    oSB.Append("             ,MAX(TO_NUMBER(SUBSTR(LOG_DATA,77,8))) AS MAX_PROGRESSIVO");
                    oSB.Append("       FROM LOGTRANS_KEEP");
                    oSB.Append("       WHERE SUBSTR(LOG_DATA,109,8) = :pCODICE_SISTEMA");
                    oSB.Append("       GROUP BY SUBSTR(LOG_DATA,117,8)");

                    tableCheck = connection.ExecuteQuery("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = 'LOGTRANS_ARCHIVE_KEEP'");
                    if (tableCheck != null && tableCheck.Rows != null && tableCheck.Rows.Count > 0)
                    {
                        oSB.Append("       UNION ALL");
                        oSB.Append("       SELECT SUBSTR(LOG_DATA,117,8) AS CODICE_CARTA");
                        oSB.Append("             ,MIN(TO_DATE(SUBSTR(LOG_DATA,65,12),'YYYYMMDDHH24MI')) AS MIN_DATA_EMI");
                        oSB.Append("             ,MAX(TO_DATE(SUBSTR(LOG_DATA,65,12),'YYYYMMDDHH24MI')) AS MAX_DATA_EMI");
                        oSB.Append("             ,MIN(TO_NUMBER(SUBSTR(LOG_DATA,77,8))) AS MIN_PROGRESSIVO");
                        oSB.Append("             ,MAX(TO_NUMBER(SUBSTR(LOG_DATA,77,8))) AS MAX_PROGRESSIVO");
                        oSB.Append("       FROM LOGTRANS_ARCHIVE_KEEP");
                        oSB.Append("       WHERE SUBSTR(LOG_DATA,109,8) = :pCODICE_SISTEMA");
                        oSB.Append("       GROUP BY SUBSTR(LOG_DATA,117,8)");
                    }
                    oSB.Append(" )");
                    oSB.Append(" GROUP BY CODICE_CARTA");

                    DataTable tableContatoriCarteOld = connection.ExecuteQuery(oSB.ToString(), new Wintic.Data.clsParameters(":pCODICE_SISTEMA", codiceSistema));
                    if (tableContatoriCarteOld != null && tableContatoriCarteOld.Rows != null)
                    {
                        foreach (DataRow row in tableContatoriCarteOld.Rows)
                        {
                            string cCodiceCarta = row["CODICE_CARTA"].ToString();
                            clsCarta carta = (oldLogsCarte == null ? null : oldLogsCarte.FirstOrDefault(c => c.Carta == cCodiceCarta));
                            if (carta == null)
                            {
                                if (oldLogsCarte == null) oldLogsCarte = new List<clsCarta>();
                                carta = new clsCarta(cCodiceCarta);
                                carta.Minimo = long.Parse(row["MIN_PROGRESSIVO"].ToString());
                                carta.Massimo = long.Parse(row["MAX_PROGRESSIVO"].ToString());
                                carta.MinDataEmi = (DateTime)row["MIN_DATA_EMI"];
                                carta.MaxDataEmi = (DateTime)row["MAX_DATA_EMI"];
                                oldLogsCarte.Add(carta);
                            }
                        }
                    }
                }
            }
            catch (Exception EX)
            {

                throw;
            }
            return result;
        }

        private static void DescribeRangesErrors(string title, List<DateTime> listaDate, DateTime? dataMigrazione, bool mensile = false)
        {
            listaDate.Sort();
            List<clsRange> listaRanges = new List<clsRange>();
            List<clsRange> listaRangesPrimaDellaMigrazione = new List<clsRange>();
            double countMancanti = 0;

            if (!mensile)
            {
                listaDate.ForEach(d =>
                {
                    clsRange range = null;
                    if (dataMigrazione != null && d < dataMigrazione)
                    {
                        range = (listaRangesPrimaDellaMigrazione != null && listaRangesPrimaDellaMigrazione.Count > 0 ? listaRangesPrimaDellaMigrazione.FirstOrDefault(rF => rF.Fine == d.Date.AddDays(-1)) : null);
                        if (range == null)
                        {
                            range = new clsRange(string.Format("{0} (prima della migrazione {1})", title, dataMigrazione.Value.ToShortDateString()), d);
                            listaRangesPrimaDellaMigrazione.Add(range);
                        }
                        else
                            range.Fine = d;
                    }
                    else
                    {
                        range = (listaRanges.Count > 0 ? listaRanges.FirstOrDefault(rF => rF.Fine == d.Date.AddDays(-1)) : null);
                        if (range == null)
                        {
                            range = new clsRange(string.Format("{0}", title), d);
                            listaRanges.Add(range);
                        }
                        else
                            range.Fine = d;
                    }
                });
                if (listaRangesPrimaDellaMigrazione.Count > 0)
                {
                    countMancanti = 0;
                    listaRangesPrimaDellaMigrazione.ForEach(range => countMancanti += (range.Fine - range.Inizio).TotalDays + 1);
                    AddWarning(string.Format("{0} (prima della migrazione {1}) qta {2}", title, dataMigrazione.Value.ToShortDateString(), countMancanti));
                    listaRangesPrimaDellaMigrazione.ForEach(range =>
                    {
                        if (range.Inizio.Date == range.Fine.Date)
                            AddWarning(string.Format(" {0} {1}  qta 1", range.Messaggio, range.Inizio.ToShortDateString()));
                        else
                            AddWarning(string.Format(" {0} {1} - {2}  qta {3}", range.Messaggio, range.Inizio.ToShortDateString(), range.Fine.ToShortDateString(), (range.Fine - range.Inizio).TotalDays + 1));
                    });
                }
                if (listaRanges.Count > 0)
                {
                    countMancanti = 0;
                    listaRanges.ForEach(range => countMancanti += (range.Fine - range.Inizio).TotalDays + 1);
                    AddErrore(string.Format(" {0} qta {1}", title, countMancanti));
                    listaRanges.ForEach(range =>
                    {
                        if (range.Inizio.Date == range.Fine.Date)
                            AddErrore(string.Format(" {0} {1}  qta 1", range.Messaggio, range.Inizio.ToShortDateString()));
                        else
                            AddErrore(string.Format(" {0} {1} - {2}  qta {3}", range.Messaggio, range.Inizio.ToShortDateString(), range.Fine.ToShortDateString(), (range.Fine - range.Inizio).TotalDays + 1));
                    });
                }
            }
            else
            {
                Dictionary<int, string> mesi = new Dictionary<int, string>() { { 1, "Gennaio"},
                                                                               { 2, "Febbraio" },
                                                                               { 3, "Marzo" },
                                                                               { 4, "Aprile" },
                                                                               { 5, "Maggio" },
                                                                               { 6, "Giogno" },
                                                                               { 7, "Luglio" },
                                                                               { 8, "Agosto" },
                                                                               { 9, "Settembre" },
                                                                               { 10, "Ottobre" },
                                                                               { 11, "Novembre" },
                                                                               { 12, "Dicembre" } };

                Dictionary<double, string> numMesi = new Dictionary<double, string>() { { 1, "un mese"},
                                                                                  { 2, "due mesi"},
                                                                                  { 3, "tre mesi"},
                                                                                  { 4, "quattro mesi"},
                                                                                  { 5, "cinque mesi"},
                                                                                  { 6, "sei mesi"},
                                                                                  { 7, "sette mesi"},
                                                                                  { 8, "otto mesi"},
                                                                                  { 9, "nove mesi"},
                                                                                  { 10, "dieci mesi"},
                                                                                  { 11, "undici mesi"},
                                                                                  { 12, "dodici mesi"} };
            
                string message = "";

                if (dataMigrazione != null)
                {
                    listaDate.Where(d => d.Date < dataMigrazione.Value.Date).ToList().GroupBy(d => d.Year).ToList().ForEach(year =>
                    {
                        countMancanti = 0;
                        message = string.Format("{0} (prima della migrazione {1}) anno {2}", title, dataMigrazione.Value.ToShortDateString(), year.Key.ToString());
                        listaDate.Where(d => d.Date < dataMigrazione.Value.Date && d.Year == year.Key).ToList().ForEach(d =>
                        {
                            countMancanti += 1;
                            message += (message.EndsWith(year.Key.ToString()) ? " " : ", ") + mesi[d.Month];
                        });
                        AddWarning(message + (countMancanti == 1 ? "  (un mese)" : string.Format("  ({0} mesi)", countMancanti)));
                    });

                    listaDate.Where(d => d.Date >= dataMigrazione.Value.Date).ToList().GroupBy(d => d.Year).ToList().ForEach(year =>
                    {
                        countMancanti = 0;
                        message = string.Format("{0} anno {1}", title, year.Key.ToString());
                        listaDate.Where(d => d.Date < dataMigrazione.Value.Date && d.Year == year.Key).ToList().ForEach(d =>
                        {
                            countMancanti += 1;
                            message += (message.EndsWith(year.Key.ToString()) ? " " : ", ") + mesi[d.Month];
                        });
                        AddErrore(message + (numMesi.ContainsKey(countMancanti) ? numMesi[countMancanti] : (countMancanti == 1 ? "  1 mese" : string.Format("  {0} mesi", countMancanti))));
                    });
                }
                else
                {
                    listaDate.GroupBy(d => d.Year).ToList().ForEach(year =>
                    {
                        countMancanti = 0;
                        message = string.Format("{0} anno {1}", title, year.Key.ToString());
                        listaDate.Where(d => d.Year == year.Key).ToList().ForEach(d =>
                        {
                            countMancanti += 1;
                            message += (message.EndsWith(year.Key.ToString()) ? " " : ", ") + mesi[d.Month];
                        });
                        AddErrore(message + (countMancanti == 1 ? "  1 mese" : string.Format("  {0} mesi", countMancanti)));
                    });
                }

            }
        }
    }

    public class clsRange
    {
        public string Messaggio { get; set; }
        public DateTime Inizio { get; set; }
        public DateTime Fine { get; set; }

        public clsRange()
        { }

        public clsRange(string messaggio, DateTime inizio)
            :this()
        {
            Messaggio = messaggio;
            Inizio = inizio;
            Fine = inizio;
        }

        public clsRange(string messaggio, DateTime inizio, DateTime fine)
            : this(messaggio, inizio)
        {
            Fine = fine;
        }
    }

    //public class clsTipoTitolo
    //{
    //    public string TitoloIvaPreassolta { get; set; }
    //    public string TipoTitolo { get; set; }
    //    public decimal Corrispettivo { get; set; }
    //    public decimal Prevendita { get; set; }
    //    public long Emessi { get; set; }
    //    public long Annullati { get; set; }

    //    public clsTipoTitolo()
    //    { }

    //    public clsTipoTitolo(string titoloIvaPreassolta, string tipoTitolo, decimal corrispettivo, decimal prevendita)
    //    {
    //        TitoloIvaPreassolta = titoloIvaPreassolta;
    //        TipoTitolo = tipoTitolo;
    //        Corrispettivo = corrispettivo;
    //        Prevendita = prevendita;
    //        Emessi = 0;
    //        Annullati = 0;
    //    }
    //}

    //public class clsAbbonamento : clsTipoTitolo
    //{
    //    public long NumeroEventi { get; set; }
    //    public decimal Rateo { get; set; }

    //    public clsAbbonamento(string titoloIvaPreassolta, string tipoTitolo, decimal corrispettivo, decimal prevendita, long numeroEventi, decimal rateo)
    //        :base(titoloIvaPreassolta, tipoTitolo, corrispettivo, prevendita)
    //    {
    //        NumeroEventi = numeroEventi;
    //        Rateo = rateo;
    //    }
    //}

    //public class clsEvento
    //{
    //    public DateTime DataOra { get; set; }
    //    public string CodiceLocale { get; set; }
    //    public string Titolo { get; set; }
    //    public DateTime DataEmiMin { get; set; }
    //    public DateTime DataEmiMax { get; set; }
    //    public List<clsTipoTitolo> Biglietti { get; set; }

    //    public clsEvento(string codiceLocale, DateTime dataOra, string titolo)
    //    {
    //        CodiceLocale = codiceLocale;
    //        DataOra = dataOra;
    //        Titolo = titolo;
    //        Biglietti = new List<clsTipoTitolo>();
    //    }
    //}    

    //public class clsOrganizzatore
    //{
    //    public string Organizzatore { get; set; }
    //    public List<clsEvento> Eventi { get; set; }
    //    public List<clsAbbonamento> Abbonamenti { get; set; }

    //    public clsOrganizzatore(string organizzatore)
    //    {
    //        Organizzatore = organizzatore;
    //        Eventi = new List<clsEvento>();
    //        Abbonamenti = new List<clsAbbonamento>();
    //    }

    //}

    //public class clsRiepilogo
    //{
    //    public DateTime Giorno { get; set; }
    //    public DateTime DataOraGenerazione { get; set; }

    //    public string Tipo { get; set; }
        
    //    public List<clsOrganizzatore> Organizzatori { get; set; }

    //    public DateTime MinDataEmi { get; set; }
    //    public DateTime MaxDataEmi { get; set; }

    //    public clsRiepilogo(string tipo, DateTime giorno, DateTime dataOraGenerazione)
    //    {
    //        Tipo = tipo;
    //        Giorno = giorno;
    //        DataOraGenerazione = dataOraGenerazione;
    //        Organizzatori = new List<clsOrganizzatore>();
    //    }

    //    public void AddBiglietto(string organizzatore, string codiceLocale, DateTime dataOra, string titolo,  clsTipoTitolo biglietto)
    //    {
    //        clsOrganizzatore org = Organizzatori.FirstOrDefault(o => o.Organizzatore == organizzatore);
    //        if (org == null)
    //        {
    //            org = new clsOrganizzatore(organizzatore);
    //            Organizzatori.Add(org);
    //        }
    //        clsEvento evento = org.Eventi.FirstOrDefault(e => e.CodiceLocale == codiceLocale && e.DataOra == dataOra && e.Titolo == titolo);
    //        if (evento == null)
    //        {
    //            evento = new clsEvento(codiceLocale, dataOra, titolo);
    //            org.Eventi.Add(evento);
    //        }
    //        clsTipoTitolo tipoTitolo = evento.Biglietti.FirstOrDefault(t => t.TitoloIvaPreassolta == biglietto.TitoloIvaPreassolta && 
    //                                                                        t.TipoTitolo == biglietto.TipoTitolo &&
    //                                                                        t.Corrispettivo == biglietto.Corrispettivo &&
    //                                                                        t.Prevendita == biglietto.Prevendita);
    //        if (tipoTitolo == null)
    //            evento.Biglietti.Add(biglietto);
    //        else
    //        {
    //            tipoTitolo.Annullati += biglietto.Annullati;
    //            tipoTitolo.Emessi += biglietto.Emessi;
    //        }
    //    }
    //}

    public class clsFileRiepilogo
    {
        public System.IO.FileInfo FileInfo { get; set; }
        
        public DateTime Giorno { get; set; }
        public DateTime DataOraGenerazione { get; set; }

        public DateTime MaxDataEmi { get; set; }

        public string Name => (FileInfo != null ? FileInfo.Name : "");

        public bool Valido = false;

        public clsFileRiepilogo(DateTime giorno, DateTime maxDataEmi)
        {
            Giorno = giorno;
            MaxDataEmi = maxDataEmi;
        }

        public clsFileRiepilogo(System.IO.FileInfo fileInfo)
        {
            FileInfo = fileInfo;
            InitDataOraGenerazione();
        }

        private void InitDataOraGenerazione()
        {
            if (FileInfo != null && FileInfo.Exists)
            {
                XmlDocument document = null;
                string tipo = "";
                string tagRiepilogo = "";
                string Sostituzione = "";
                string Data = "";
                string Mese = "";
                string DataGenerazione = "";
                string OraGenerazione = "";
                string ProgressivoGenerazione = "";
                document = new XmlDocument();
                try
                {
                    document = null;
                    string xml = System.IO.File.ReadAllText(FileInfo.FullName);
                    if (xml.Contains(@"<?xml"))
                    {
                        xml = xml.Substring(xml.IndexOf(@"<?xml"));
                        if (xml.Contains(@"</RiepilogoGiornaliero>"))
                        {
                            xml = xml.Substring(0, xml.IndexOf(@"</RiepilogoGiornaliero>") + 23);
                            tagRiepilogo = "RiepilogoGiornaliero";
                            document = new XmlDocument();
                            document.LoadXml(xml);
                        }
                        else if (xml.Contains(@"</RiepilogoMensile>"))
                        {
                            xml = xml.Substring(0, xml.IndexOf(@"</RiepilogoMensile>") + 19);
                            tagRiepilogo = "RiepilogoMensile";
                            document = new XmlDocument();
                            document.LoadXml(xml);
                        }
                        else if (xml.Contains(@"</RiepilogoControlloAccessi>"))
                        {
                            xml = xml.Substring(0, xml.IndexOf(@"</RiepilogoControlloAccessi>") + 28);
                            tagRiepilogo = "RiepilogoControlloAccessi";
                            Valido = true;
                        }
                        else if (xml.Contains(@"</LogTransazione>"))
                        {
                            tagRiepilogo = "LogTransazione";
                            Valido = true;
                        }
                        else if (xml.Contains("</LTA_Giornaliera>"))
                        {
                            tagRiepilogo = "LTA_Giornaliera";
                            Valido = true;
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    document = null;
                }

                if (!Valido && tagRiepilogo != "")
                {
                    try
                    {
                        XmlNodeList nodeList;
                        nodeList = document.GetElementsByTagName(tagRiepilogo);
                        if (nodeList != null && nodeList.Count == 1)
                        {
                            XmlNode nodeRiepilogo = nodeList[0];
                            if (nodeRiepilogo.Attributes != null)
                            {
                                /* 
                                 * <RiepilogoGiornaliero Sostituzione="N" Data="20220227" DataGenerazione="20220227" OraGenerazione="210203" ProgressivoGenerazione="1">
                                 * <RiepilogoMensile Sostituzione="S" Mese="202111" DataGenerazione="20211113" OraGenerazione="221931" ProgressivoGenerazione="4">
                                 * */
                                foreach (XmlAttribute attr in nodeRiepilogo.Attributes)
                                {
                                    switch (attr.Name)
                                    {
                                        case "Sostituzione": { Sostituzione = attr.Value; break; }
                                        case "Data": { Data = attr.Value; break; }
                                        case "Mese": { Mese = attr.Value; break; }
                                        case "DataGenerazione": { DataGenerazione = attr.Value; break; }
                                        case "OraGenerazione": { OraGenerazione = attr.Value; break; }
                                        case "ProgressivoGenerazione": { ProgressivoGenerazione = attr.Value; break; }
                                    }
                                }

                                bool canContinue = false;
                                if (tagRiepilogo == "RiepilogoGiornaliero" && Data.Length == 8)
                                {
                                    Giorno = new DateTime(int.Parse(Data.Substring(0, 4)), int.Parse(Data.Substring(4, 2)), int.Parse(Data.Substring(6, 2)));
                                    canContinue = true;
                                }
                                else if (tagRiepilogo == "RiepilogoMensile" && Mese.Length == 6)
                                {
                                    Giorno = new DateTime(int.Parse(Mese.Substring(0, 4)), int.Parse(Mese.Substring(4, 2)), 1);
                                    canContinue = true;
                                }

                                if (canContinue && DataGenerazione.Length == 8 && OraGenerazione.Length >= 4)
                                {
                                    int anno = 0;
                                    int mese = 0;
                                    int giorno = 0;
                                    int ora = 0;
                                    int min = 0;
                                    int sec = 0;
                                    if (int.TryParse(DataGenerazione.Substring(0, 4), out anno) &&
                                        int.TryParse(DataGenerazione.Substring(4, 2), out mese) &&
                                        int.TryParse(DataGenerazione.Substring(6, 2), out giorno) &&
                                        int.TryParse(OraGenerazione.Substring(0, 2), out ora) &&
                                        int.TryParse(OraGenerazione.Substring(2, 2), out min) &&
                                        (OraGenerazione.Length < 6 || int.TryParse(OraGenerazione.Substring(4, 2), out sec)))
                                    {
                                        DataOraGenerazione = new DateTime(anno, mese, giorno, ora, min, sec);
                                        if (FileInfo.LastWriteTime > DataOraGenerazione)
                                            DataOraGenerazione = FileInfo.LastWriteTime;
                                        Valido = true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (FileInfo.LastWriteTime > DataOraGenerazione)
                            DataOraGenerazione = FileInfo.LastWriteTime;
                    }
                }
                else
                {
                    if (tagRiepilogo == "RiepilogoControlloAccessi" || tagRiepilogo == "LTA_Giornaliera")
                    {
                        DataOraGenerazione = FileInfo.LastWriteTime;
                    }
                }
            }
        }
    }

    public class clsProgressivo
    {
        public string Carta { get; set; }
        public long Progressivo { get; set; }
        public string File { get; set; }
        public DateTime Giorno { get; set; }
        public DateTime GiornoEmissione { get; set; }
        public DateTime? GiornoEvento { get; set; }
        public DateTime? GiornoAbbonamento { get; set; }
        public long Riga { get; set; }

        public bool EsisteNelFile = false;
        public bool EsisteNelDB = false;

        public clsProgressivo()
        { }

        public clsProgressivo(string carta, long progressivo, string file, DateTime giorno, DateTime giornoEmissione, long riga, bool esisteNelFile, bool esisteNelDB)
        {
            Carta = carta;
            Progressivo = progressivo;
            File = file;
            Giorno = giorno;
            GiornoEmissione = giornoEmissione;
            Riga = riga;
            EsisteNelFile = esisteNelFile;
            EsisteNelDB = esisteNelDB;
        }

        public class ProgressivoComparer : IComparer<clsProgressivo>
        {
            public int Compare(clsProgressivo x, clsProgressivo y)
            {
                 return (x.Progressivo > y.Progressivo ? 1 : (x.Progressivo < y.Progressivo ? -1 : 0));
            }
        }
    }
    public class clsCarta
    {
        public event delegateMessage Message;
        public string Carta { get; set; }
        public List<clsProgressivo> Progressivi { get; set; }
        public long Minimo { get; set; }
        public long Massimo { get; set; }

        public List<long> Mancanti { get; set; }

        public List<clsCarta> RangesMancanti = new List<clsCarta>();

        public DateTime? MinDataEmi { get; set; }
        public DateTime? MaxDataEmi { get; set; }

        public long LastRiga { get; set; }

        public clsCarta(string carta)
        {
            Carta = carta;
            Progressivi = new List<clsProgressivo>();
            Mancanti = new List<long>();
            LastRiga = 0;
        }

        

        private void RaiseMessage(string carta, long? minProgressivo, long? maxProgressivo, string value, Exception error)
        {
            if (Message != null)
                Message(carta, minProgressivo, maxProgressivo, value, error);
        }

        public void Analizza()
        {
            Utils.MaxSubCounter = Progressivi.Count;
            RaiseMessage(Carta, null, null, "Inizio analisi carta", null);
            Minimo = Progressivi.Min<clsProgressivo>(p => p.Progressivo);
            Massimo = Progressivi.Max<clsProgressivo>(p => p.Progressivo);
            RaiseMessage(Carta, Minimo, Massimo, "Minimo e Massimo", Minimo == 1 ? null : new Exception(string.Format("Progressivo iniziale {0} maggiore di 1", Minimo)));

            if (Massimo - Minimo + 1 != Progressivi.Count)
                RaiseMessage(Carta, null, null, "Contatori Mancanti", new Exception(string.Format("Trovati {0} Mancanti {1}", Progressivi.Count, (Massimo - Minimo + 1) - Progressivi.Count)));
            Progressivi.Sort(new clsProgressivo.ProgressivoComparer());

            long last = Minimo - 1;
            long lastRiga = -1;
            string lastFile = "";
            Progressivi.ForEach(item =>
            {
                Utils.CntSubCounter += 1;
                if (item.Progressivo > last + 1)
                {
                    string cMessage = "";
                    long minimo = last + 1;
                    long massimo = item.Progressivo - 1;
                    if (massimo == minimo)
                        cMessage = string.Format("{0} 1 Progressivo mancante {1} giorno {2} file {3}", item.Carta, minimo, item.Giorno.ToShortDateString(), item.File);
                    else
                        cMessage = string.Format("{0} {1} Progressivi mancanti {2}-{3} giorno {4} file {5}", item.Carta, massimo - minimo + 1, minimo, massimo, item.Giorno.ToShortDateString(), item.File);
                    RaiseMessage(item.Carta, minimo, massimo, cMessage, new Exception(cMessage));
                    for (long i = minimo; i <= massimo; i++)
                        Mancanti.Add(i);
                }
                last = item.Progressivo;

                if (lastFile != item.File)
                {
                    lastFile = item.File;
                    lastRiga = item.Riga;
                    if (lastRiga > 1)
                    {
                        string cMessage = string.Format("Prima riga maggiore di 1 ({0}) giorno {1} file {2}", item.Riga, item.Giorno.ToShortDateString(), item.File);
                        RaiseMessage(item.Carta, null, null, cMessage, new Exception(cMessage));
                    }
                }
                else
                {
                    if (item.Riga > lastRiga + 1)
                    {
                        string cMessage = string.Format("Salto riga ({0}) giorno {1} file {2}", item.Riga, item.Giorno.ToShortDateString(), item.File);
                        RaiseMessage(item.Carta, null, null, cMessage, new Exception(cMessage));
                    }
                }
                lastRiga = item.Riga;
            });
            if (Mancanti.Count > 0)
                RaiseMessage(Carta, null, null, "Fine analisi Contatori Mancanti", new Exception(string.Format("Fine analisi Contatori Mancanti {0}", Mancanti.Count.ToString())));
            RaiseMessage(Carta, null, null, "FINE analisi carta", null);
        }

        
    }

    public class clsLog
    {
        public event delegateMessage Message;
        public List<clsCarta> Carte { get; set; }
        public clsLog()
        {
            Carte = new List<clsCarta>();
        }

        public void AddCarta(clsCarta carta)
        {
            Carte.Add(carta);
            carta.Message += RaiseMessage;
        }

        private void RaiseMessage(string carta, long? minProgressivo, long? maxProgressivo, string value, Exception error)
        {
            if (Message != null)
                Message(carta, minProgressivo, maxProgressivo, value, error);
        }

        public void Analizza()
        {
            Carte.ForEach(c => c.Analizza());
        }
    }
        


}
