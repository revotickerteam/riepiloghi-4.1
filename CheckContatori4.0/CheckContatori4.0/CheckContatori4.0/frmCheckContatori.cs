﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckContatori4._0
{
    public partial class frmCheckContatori : frmBase
    {

        public frmCheckContatori()
        {
            InitializeComponent();
            this.Shown += FrmCheckContatori_Shown;
            this.btnExec.Click += BtnExec_Click;
        }

        private void BtnExec_Click(object sender, EventArgs e)
        {
            if (!Utils.Executing)
            {
                this.SetCheckMode();
                Utils.EventEndMode += SetEndMode;
                Utils.EventAddAnalisi += AddAnalisi;
                Utils.EventAddError += AddErrore;
                Utils.EventSubCounterChanged += Utils_EventSubCounterChanged;
                this.progressBarAnalisi.Maximum = Utils.MaxAnalisi;
                this.progressBarAnalisi.Value = 0;
                Utils.StartExecution(this.txtFolder.Text, (this.chkDatabase.Checked ? this.txtDatabase.Text : ""), Application.ExecutablePath);
            }
            else
            {
                Utils.StopProcess = true;
            }
        }

        private void Utils_EventSubCounterChanged()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker d = new MethodInvoker(Utils_EventSubCounterChanged);
                this.Invoke(d);
            }
            else
            {
                bool refresh = false;
                if (this.progressBarSubCounter.Maximum != Utils.MaxSubCounter)
                {
                    this.progressBarSubCounter.Maximum = Utils.MaxSubCounter;
                    refresh = true;
                }
                if (this.progressBarSubCounter.Maximum < Utils.CntSubCounter)
                {
                    this.progressBarSubCounter.Maximum = Utils.CntSubCounter;
                    refresh = true;
                }
                if (this.progressBarSubCounter.Value != Utils.CntSubCounter)
                {
                    this.progressBarSubCounter.Value = Utils.CntSubCounter;
                    refresh = true;
                }

                if (refresh)
                {
                    this.Refresh();
                    this.progressBarSubCounter.Refresh();
                    Application.DoEvents();
                }
            }
        }

        private void FrmCheckContatori_Shown(object sender, EventArgs e)
        {
            this.SetParameterMode();
        }

        private void SetParameterMode()
        {
            this.pnlFolder.Visible = true;
            this.pnlDatabase.Visible = true;
            this.btnExec.Visible = true;
            this.btnExec.Text = "Avvia";
            this.splitter1.Visible = false;
            this.pnlAnalisi.Visible = false;
            this.pnlErrori.Visible = false;
            this.txtAnalisi.Text = "";
            this.txtErrori.Text = "";
            this.btnFolder.Click += BtnFolder_Click;
            this.chkDatabase.CheckedChanged += ChkDatabase_CheckedChanged;
            this.SetClientRectangleControlInsideVisible();
        }

        private void ChkDatabase_CheckedChanged(object sender, EventArgs e)
        {
            this.txtDatabase.Enabled = this.chkDatabase.Checked;
        }

        private void BtnFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = false;
            if (dialog.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(dialog.SelectedPath))
                this.txtFolder.Text = dialog.SelectedPath;
        }

        private void SetCheckMode()
        {
            this.pnlFolder.Visible = false;
            this.pnlDatabase.Visible = false;
            this.btnExec.Visible = true;
            this.btnExec.Text = "Interrompi";
            this.splitter1.Visible = true;
            this.pnlAnalisi.Visible = true;
            this.pnlErrori.Visible = true;
            this.txtAnalisi.Text = "";
            this.txtErrori.Text = "";
            this.SetClientRectangleControlInsideVisible();
        }

        private void SetEndMode(bool errorExists)
        {
            if (this.InvokeRequired)
            {
                Utils.delegateEndMode d = new Utils.delegateEndMode(this.SetEndMode);
                this.Invoke(d, errorExists);
            }
            else
            {
                this.btnExec.Enabled = false;
                this.btnExec.Text = (Utils.queueErrors.Count > 0 ? "scrittura errori..." : "Operazione Terminata" + (errorExists ? " con errori" : ""));
                this.progressBarAnalisi.Value = this.progressBarAnalisi.Maximum;
                if (errorExists)
                {
                    this.AddAnalisi(string.Format("Verificare il log degli errori\r\n{0}", Utils.FileLogErrori));
                    this.AddErrore(string.Format("Verificare il log degli errori\r\n{0}", Utils.FileLogErrori));
                }
            }
        }

        private void SetClientRectangleControlInsideVisible()
        {
            this.Refresh();
            Application.DoEvents();
            int height = this.Padding.Vertical + this.DefaultMargin.Vertical;
            this.txtAnalisi.Text = "";
            foreach (Control oCtl in this.Controls)
            {
                if (oCtl.Visible)
                {
                    height += oCtl.Height + this.DefaultMargin.Vertical;
                }
            }
            this.Size = new Size(this.Width, SystemInformation.BorderSize.Height + SystemInformation.CaptionHeight + height);
            this.Refresh();
            Application.DoEvents();
            this.CenterToScreen();
        }

        private delegate void delegateAddText(TextBox textBox, string value);
        private void AddText(TextBox textBox, string value)
        {
            if (this.InvokeRequired)
            {
                delegateAddText d = new delegateAddText(this.AddText);
                this.Invoke(d, textBox, value);
            }
            else
            {
                textBox.Text += value + "\r\n";
                textBox.SelectionLength = 0;
                textBox.SelectionStart = textBox.Text.Length;
                textBox.ScrollToCaret();
                if (Utils.MaxAnalisi > 0 && Utils.CntAnalisi > 0)
                {
                    if (this.progressBarAnalisi.Maximum != Utils.MaxAnalisi)
                        this.progressBarAnalisi.Maximum = Utils.MaxAnalisi;
                    if (this.progressBarAnalisi.Maximum < Utils.CntAnalisi)
                        this.progressBarAnalisi.Maximum = Utils.CntAnalisi;
                    this.progressBarAnalisi.Value = Utils.CntAnalisi;
                }
                else
                    this.progressBarAnalisi.Value = 0;

                Application.DoEvents();
            }
        }
        private System.Collections.Generic.Queue<string> queueErrorsLast10 = new Queue<string>();
        

        private void AddTextLast10(TextBox textBox, string value)
        {
            
            if (this.InvokeRequired)
            {
                delegateAddText d = new delegateAddText(this.AddTextLast10);
                this.Invoke(d, textBox, value);
            }
            else
            {
                queueErrorsLast10.Enqueue(value);
                while (queueErrorsLast10.Count > 10)
                    queueErrorsLast10.Dequeue();
                string testo = "";
                queueErrorsLast10.ToList<string>().ForEach(s => testo += (testo == "" ? "" : "\r\n") + s);
                textBox.Text = testo;
                textBox.SelectionLength = 0;
                textBox.SelectionStart = textBox.Text.Length;
                textBox.ScrollToCaret();
                Application.DoEvents();
            }
        }


        private void AddAnalisi(string value)
        {
            this.AddText(this.txtAnalisi, value);
        }

        private void AddErrore(string value)
        {
            this.AddTextLast10(this.txtErrori, value);
        }



    }
}
