﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libRiepiloghiBase
{


    public interface iLibFirmaEmail
    {
        bool FirmaFile(string cCodiceSistema, string Pin, int Slot, out Exception oError, string File);
        bool Email(string cCodiceSistema, string Pin, int Slot, out Exception oError, string FileRiepilogo, string MailFrom, string MailTo);
    }

    public interface iLibGetSerialNumber
    {
        string GetSerialNumber(int slot, out Exception error);
    }

    public static class clsSmartCardReaderLib_Loader
    {

        public static void test()
        {

        }

        //GetSN (BYTE serial [8]);

        private static System.Reflection.Assembly libFirmaEmail = null;
        private static string libFirmaEmailType = "";
        public static iLibFirmaEmail GetInstance()
        {
            string implementedSearchType = "iSmartCardReaderLib";
            iLibFirmaEmail result = null;
            if (libFirmaEmail == null)
            {
                System.IO.FileInfo oFileInfoApp = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().FullName);
                string path = oFileInfoApp.Directory.FullName;
                List<string> listDll = new List<string>() { "libreria32.dll", "libreria64.dll" };

                foreach (string dllFileName in listDll)
                {
                    try
                    {
                        string dllFileNameFull = path + @"\" + dllFileName;
                        if (System.IO.File.Exists(dllFileNameFull))
                        {
                            System.Reflection.Assembly oAssembly = System.Reflection.Assembly.LoadFile(dllFileNameFull);
                            Type[] FileTypes = oAssembly.GetTypes();
                            System.Diagnostics.FileVersionInfo AssemblyVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(dllFileNameFull);
                            string AssemblyName = AssemblyVersion.OriginalFilename;
                            foreach (Type singleFileType in FileTypes)
                            {
                                Type[] interfaceTypes = singleFileType.GetInterfaces();
                                for (int indexInterface = 0; indexInterface < interfaceTypes.Length; indexInterface++)
                                {
                                    Type implementedType = interfaceTypes[indexInterface];
                                    if (implementedType.FullName == implementedSearchType)
                                    {
                                        try
                                        {

                                            result = (iLibFirmaEmail)oAssembly.CreateInstance(singleFileType.FullName);
                                            libFirmaEmail = oAssembly;
                                            libFirmaEmailType = singleFileType.FullName;
                                        }
                                        catch (Exception ex)
                                        {
                                            string errCon = "";
                                        }
                                    }
                                    if (result != null) break;
                                }
                                if (result != null) break;
                            }
                            if (result != null) break;
                        }
                    }
                    catch (Exception ex)
                    {
                        string err = "";
                    }
                }
            }
            else
            {
                result = (iLibFirmaEmail)libFirmaEmail.CreateInstance(libFirmaEmailType);
            }
            return result;
        }
    }
}

