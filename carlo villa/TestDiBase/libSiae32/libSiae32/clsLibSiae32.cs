﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace libSiae32
{
    public class clsLibSiae32 : libRiepiloghiBase.iLibGetSerialNumber
    {
        [DllImport("libSIAE32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSN(Byte[] serial);

        //int GetSNML(BYTE serial [8]);
        [DllImport("libSIAE32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static unsafe extern int GetSNML(Byte[] serial, int nSlot);

        private const Int32 C_OK = 0x0000;
        private const Int32 C_CONTEXT_ERROR = 0x0001;
        private const Int32 C_NOT_INITIALIZED = 0x0002;
        private const Int32 C_ALREADY_INITIALIZED = 0x0003;
        private const Int32 C_NO_CARD = 0x0004;
        private const Int32 C_UNKNOWN_CARD = 0x0005;
        private const Int32 C_WRONG_LENGTH = 0x6282;
        private const Int32 C_WRONG_TYPE = 0x6981;
        private const Int32 C_NOT_AUTHORIZED = 0x6982;
        private const Int32 C_PIN_BLOCKED = 0x6983;
        private const Int32 C_WRONG_DATA = 0x6A80;
        private const Int32 C_FILE_NOT_FOUND = 0x6A82;
        private const Int32 C_RECORD_NOT_FOUND = 0x6A83;
        private const Int32 C_WRONG_LEN = 0x6A85;
        private const Int32 C_UNKNOWN_OBJECT = 0x6A88;
        private const Int32 C_ALREADY_EXISTS = 0x6A89;
        private const Int32 C_GENERIC_ERROR = 0xFFFF;

        // ADDED CONSTANTS
        private const Int32 C_IS_CARD_IN = 0x0020;
        private const Int32 C_CARD_IS_NOT_IN = 0x0021;

        // 
        private const Int32 C_WRONG_PIN = 0x63C4;


        public string GetSerialNumber(int slot, out Exception error)
        {
            Byte[] sn = new Byte[8];
            error = null;
            int ret = 0;

            try
            {
                ret = GetSNML(sn, slot);

                if (ret != 0)
                {
                    error = new Exception(getErrorMessage(ret));
                    return "";
                }
                else
                {
                    System.Text.Encoding enc = System.Text.Encoding.ASCII;
                    return enc.GetString(sn).Trim('\0');
                }
            }
            catch (Exception e)
            {
                error = e;
                return "";
            }
        }

        private string getErrorMessage(Int32 cod)
        {
            string s1;

            switch (cod)
            {
                case C_OK: s1 = "OK"; break;

                case C_CONTEXT_ERROR: s1 = "Context Error"; break;

                case C_NOT_INITIALIZED: s1 = "Not Initialized"; break;

                case C_ALREADY_INITIALIZED: s1 = "Already Initialized"; break;

                case C_NO_CARD: s1 = "No Card"; break;

                case C_UNKNOWN_CARD: s1 = "Unknown Card"; break;

                case C_WRONG_LENGTH: s1 = "Wrong Length"; break;

                case C_WRONG_TYPE: s1 = "Wrong Type"; break;

                case C_NOT_AUTHORIZED: s1 = "Not Authorized"; break;

                case C_PIN_BLOCKED: s1 = "Pin Blocked"; break;

                case C_WRONG_DATA: s1 = "Wrong Data"; break;

                case C_FILE_NOT_FOUND: s1 = "File Not Found"; break;

                case C_RECORD_NOT_FOUND: s1 = "Record Not Found"; break;

                case C_WRONG_LEN: s1 = "Wrong Len"; break;

                case C_UNKNOWN_OBJECT: s1 = "Unknown Object"; break;

                case C_ALREADY_EXISTS: s1 = "Already Exist"; break;

                case C_GENERIC_ERROR: s1 = "Errore Generico"; break;


                case C_WRONG_PIN: s1 = "Pin Errato"; break;


                default: s1 = "Errore sconosciuto " + cod.ToString(); break;
            }

            return s1;
        }
    }
}
