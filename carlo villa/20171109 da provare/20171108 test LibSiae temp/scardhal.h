#include <winscard.h>

int	CALLINGCONV isCardIn(int n);
BOOL CALLINGCONV IsInitialized();
int  CALLINGCONV Initialize(int Slot);
int  CALLINGCONV Finalize();
int  CALLINGCONV FinalizeML(int nSlot);
int  CALLINGCONV Hash(int mec,BYTE *toHash, int Len, BYTE *Hashed);
int  CALLINGCONV SendAPDU(DWORD cmd, BYTE Lc, BYTE *pLe, BYTE *inBuffer, BYTE *outBuffer, WORD *pSW);
int  CALLINGCONV SendAPDUML(SCARDHANDLE hCard, DWORD cmd, BYTE Lc, BYTE *pLe, BYTE *inBuffer, BYTE *outBuffer, WORD *pSW);
