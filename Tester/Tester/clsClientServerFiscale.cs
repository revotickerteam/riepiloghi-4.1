﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Wintic.Data;

namespace Tester
{
    public class clsParametriServerFiscale
    {
        public Int64 IdSocket = 0;
        public string ServerIP = "";
        public int Port = 0;
        public string Client_ID = "";
        public string Password = "";

        public clsParametriServerFiscale()
        {
        }

        public clsParametriServerFiscale(Int64 nIdSocket, string cServerIP, int nPort, string cClient_ID, string cPassword)
        {
            this.IdSocket = nIdSocket;
            this.ServerIP = cServerIP;
            this.Port = nPort;
            this.Client_ID = cClient_ID;
            this.Password = cPassword;
        }
    }
    public class clsClientServerFiscale : IDisposable
    {

        #region "Dichiarazioni"

        private Int64 nIdSocket = 0;
        private string cServerIP = "";
        private int nPort = 0;
        private TcpClient oClient = null;
        private NetworkStream oNetworkStream = null;
        private StreamWriter oStreamWriter = null;

        private Exception oExceptionClient = null;

        private string cPassword = "";
        private string cClient_ID = "";

        private const string SEND_CHECK_SERVER_FISCALE = "666";
        private const string SEND_ANNULLO_BIGLIETTO_IVA_DA_ASSOLVERE = "002";
        private const string SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA = "004";
        private const string SEND_ANNULLO_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA = "010";
        private const string SEND_ANNULLO_ABBONAMENTO = "006";
        private const string SEND_ANNULLO_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA = "008";
        private const string SEND_UTILIZZO_SMART_CARD = "999";
        private const string SEND_RILASCIO_SMART_CARD = "998";


        private const string SEND_BIGLIETTO_IVA_DA_ASSOLVERE = "001";
        private const string SEND_BIGLIETTO_IVA_PREASSOLTA = "003";
        private const string SEND_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA = "009";
        private const string SEND_ABBONAMENTO = "005";
        private const string SEND_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA = "007";

        #endregion

        #region "Proprieta"

        // Eccezione
        public Exception ExceptionClient
        {
            get { return oExceptionClient; }
            set { oExceptionClient = value; }
        }

        // Ritorna TRUE se il CLIENT_ID e la PASSWORD sono stati inizializzati
        public bool ClientConfigValido
        {
            get
            {
                return ((cClient_ID.Trim().Length == 3) && (cPassword.Trim().Length == 8));
            }
        }

        // Ritorna true se l'IP e la PORTA di comunicazione con il server fiscale sono stati inizializzati
        public bool SocketConfigValido
        {
            get
            {
                return ((cServerIP.Trim() != "") && (nPort > 0));
            }
        }

        // Verifica se il client è valido
        public bool ClientValido
        {
            get { return ClientConfigValido && SocketConfigValido; }
        }

        #endregion

        #region "Metodi"

        // Connessione con il server fiscale
        private bool Connect()
        {
            bool lRet = false;
            try
            {
                oClient = new TcpClient();
                oClient.ReceiveTimeout = 20;

                oClient.Connect(cServerIP, nPort);
                lRet = oClient.Connected;
                oNetworkStream = oClient.GetStream();
                //oStreamReader = new StreamReader(oNetworkStream);
                oStreamWriter = new StreamWriter(oNetworkStream);
                lRet = true;
            }
            catch (Exception exConnect)
            {
                lRet = false;
                ExceptionClient = exConnect;
            }
            return lRet;
        }

        // Chiusura connessione con il server fiscale
        private void Close()
        {
            try
            {
                if (oClient != null)
                {
                    oClient.Close();
                }
            }
            catch
            {
            }
        }


        // Spedizione
        private bool Send(string cMsg)
        {
            bool lRet = false;
            try
            {
                oStreamWriter.WriteLine(cMsg);
                oStreamWriter.Flush();
                lRet = true;
            }
            catch (Exception exSend)
            {
                ExceptionClient = exSend;
                ExceptionClient = new Exception("Send error " + exSend.Message);
                lRet = false;
            }
            return lRet;
        }

        // Ricezione
        private bool GetDataReceive(ref String message)
        {
            String clientData = "";

            try
            {
                // Data buffer for incoming data.
                byte[] bytes;

                bytes = new byte[oClient.ReceiveBufferSize];

                if (oNetworkStream.DataAvailable)
                {

                    int BytesRead = oNetworkStream.Read(bytes, 0, (int)oClient.ReceiveBufferSize - 1);
                    if (BytesRead > 0)
                    {
                        clientData = Encoding.ASCII.GetString(bytes, 0, BytesRead);

                        message = clientData.Replace("\r", string.Empty).Replace("\n", string.Empty);

                    }
                }
            }
            catch (Exception e)
            {
                ExceptionClient = new Exception("GetDataReceived error " + e.Message);
                return false;
            }
            return true;
        }

        private string Receive()
        {
            string cReceive = "";
            System.Diagnostics.Stopwatch timeout = new System.Diagnostics.Stopwatch();
            timeout.Start();

            long maxTimeOut = 20000;

            while (true)
            {
                try
                {
                    if (!oClient.Connected)
                    {
                        timeout.Stop();
                        ExceptionClient = new Exception("Disconnesso dal Server Fiscale");
                        break;
                    }

                    if (timeout.ElapsedMilliseconds > maxTimeOut)
                    {
                        timeout.Stop();
                        ExceptionClient = new Exception("Timeout comunicazione Server Fiscale");
                        break;
                    }

                    if (oNetworkStream.DataAvailable)
                    {
                        timeout.Stop();
                        string buffer = "";
                        if (GetDataReceive(ref buffer))
                        {
                            cReceive = buffer;
                        }
                        else
                        {
                            ExceptionClient = new Exception("Timeout comunicazione Server Fiscale");
                        }
                        break;
                    }
                }
                catch (Exception exSendReceive)
                {
                    ExceptionClient = new Exception("Receive error " + exSendReceive.Message);
                    break;
                }
            }

            return cReceive;
        }


        // Verifica server fiscale
        public bool CheckServerFiscale(clsParametriServerFiscale oParametriServerFiscale)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);
            ExceptionClient = null;
            if (lRet)
            {
                try
                {
                    lRet = Connect();
                    if (lRet)
                    {
                        lRet = Send(cClient_ID + cPassword + clsClientServerFiscale.SEND_CHECK_SERVER_FISCALE);

                        if (lRet)
                        {
                            string cReceive = this.Receive(); ;
                            lRet = ExceptionClient == null && cReceive.EndsWith("000");
                        }
                        Close();
                    }
                }
                catch (Exception exCheckServerFiscale)
                {
                    ExceptionClient = exCheckServerFiscale;
                    lRet = false;
                }
            }
            return lRet;
        }

        #endregion

        #region "Inizializzazione da database"

        //oParametriServerFiscale
        private bool GetParametriConnessioneServerFiscale(clsParametriServerFiscale oParametriServerFiscale)
        {
            bool lRet = false;
            try
            {
                nIdSocket = oParametriServerFiscale.IdSocket;
                cServerIP = oParametriServerFiscale.ServerIP;
                nPort = oParametriServerFiscale.Port;
                cClient_ID = oParametriServerFiscale.Client_ID;
                cPassword = oParametriServerFiscale.Password;
                lRet = ClientValido;
            }
            catch
            {
            }
            return lRet;
        }

        public static clsParametriServerFiscale GetParametriServerFiscale(IConnection oConnection, out Exception oError)
        {
            clsParametriServerFiscale oRet = null;
            Int64 nIdSocket = 0;
            oError = null;
            string cServerIP = "";
            int nPort = 0;
            string cClient_ID = "";
            string cPassword = "";
            try
            {
                StringBuilder oSB = new StringBuilder();
                IRecordSet oRS = null;
                oSB.Append("select ");
                oSB.Append(" SMART_SOCKET.IDSOCKET,");
                oSB.Append(" SMART_SOCKET.IP,");
                oSB.Append(" SMART_SOCKET.PORT");
                oSB.Append(" FROM SMART_SOCKET ");

                oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString());
                if (!oRS.EOF)
                {
                    if (!oRS.Fields("idsocket").IsNull)
                    {
                        nIdSocket = Int64.Parse(oRS.Fields("idsocket").Value.ToString());
                    }
                    if (!oRS.Fields("ip").IsNull)
                    {
                        cServerIP = oRS.Fields("ip").Value.ToString();
                    }
                    if (!oRS.Fields("port").IsNull)
                    {
                        nPort = int.Parse(oRS.Fields("port").Value.ToString());
                    }
                }
                oRS.Close();


                oSB = new StringBuilder();
                oSB.Append("SELECT ");
                oSB.Append(" SMART_CLIENT.CLIENT_ID,");
                oSB.Append(" SMART_CLIENT.PWD");
                oSB.Append(" FROM SMART_CLIENT ");
                oSB.Append(" WHERE SMART_CLIENT.ENABLED = 1");

                oRS = (IRecordSet)oConnection.ExecuteQuery(oSB.ToString());
                if (!oRS.EOF && cClient_ID.Trim() == "")
                {
                    if (!oRS.Fields("client_id").IsNull && !oRS.Fields("pwd").IsNull)
                    {
                        cClient_ID = oRS.Fields("client_id").Value.ToString();
                        cPassword = oRS.Fields("pwd").Value.ToString();
                    }
                    oRS.MoveNext();
                }
                oRS.Close();
                if (nIdSocket > 0 && cServerIP.Trim() != "" && nPort > 0 && cClient_ID.Trim() != "" && cPassword.Trim() != "")
                {
                    oRet = new clsParametriServerFiscale(nIdSocket, cServerIP, nPort, cClient_ID, cPassword);
                }
            }
            catch (Exception exInitFromDatabase)
            {
                oError = exInitFromDatabase;
            }
            return oRet;
        }

        #endregion

        #region "Metodi di richiesta del sigillo"

        public bool RichiediSigilloBigliettoIvaDaAssolvere(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediSigillo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_BIGLIETTO_IVA_DA_ASSOLVERE, "", 0);
        }

        // Annullo biglietto IVA preassolta (rateo)
        public bool RichiediSigilloBigliettoIvaPreassolta(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            return RichiediSigillo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_BIGLIETTO_IVA_PREASSOLTA, CodiceBigliettoAbbonamento, NumProgBigliettoAbbonamento);
        }

        // Annullo biglietto IVA certificata tramite fattura
        public bool RichiediSigilloBigliettoIvaCertificataTramiteFattura(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediSigillo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        // Annullo abbonamento IVA da assolvere
        public bool RichiediSigilloAbbonamentoIvaDaAssolvere(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediSigillo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ABBONAMENTO, "", 0);
        }

        // Annullo abbonamento IVA certificata tramite fattura
        public bool RichiediSigilloAbbonamentoIvaCertificataTramiteFattura(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediSigillo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        private bool RichiediSigillo(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string TipoSigillo, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);

            if (lRet)
            {
                try
                {
                    if (Connect())
                    {
                        string cSend = cClient_ID + cPassword + TipoSigillo + nIdVend.ToString("00000000");

                        if (TipoSigillo == clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA)
                            cSend += NumProgBigliettoAbbonamento.ToString().Trim().PadLeft(6, '0').Trim() + CodiceBigliettoAbbonamento;

                        if (Send(cSend))
                        {
                            string cReceive = Receive();
                            if (!string.IsNullOrEmpty(cReceive))
                            {
                                lRet = (cReceive.Length >= 25) &&
                                       cReceive.Substring(0, 3) == cClient_ID &&
                                       cReceive.Substring(3, 8) == cPassword &&
                                       cReceive.Substring(11, 3) == TipoSigillo &&
                                       cReceive.Substring(14, 3) == "000" &&
                                       cReceive.Substring(17, 8) == nIdVend.ToString("00000000");
                                if (lRet)
                                {
                                    cSend = cClient_ID + cPassword + TipoSigillo + "000";
                                }
                                else
                                {
                                    cSend = cClient_ID + cPassword + TipoSigillo + "001";
                                }

                                if (oClient.Connected)
                                    Send(cSend);
                            }
                            else
                                lRet = false;
                        }
                        Close();
                    }
                }
                catch (Exception exRichiediAnnullo)
                {
                    ExceptionClient = exRichiediAnnullo;
                    lRet = false;
                }
            }
            return lRet;
        }

        #endregion

        #region "Metodi di richiesta del sigillo di annullo al server fiscale"

        // Annullo biglietto IVA da assolvere
        public bool RichiediAnnulloBigliettoIvaDaAssolvere(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_DA_ASSOLVERE, "", 0);
        }

        // Annullo biglietto IVA preassolta (rateo)
        public bool RichiediAnnulloBigliettoIvaPreassolta(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA, CodiceBigliettoAbbonamento, NumProgBigliettoAbbonamento);
        }

        // Annullo biglietto IVA certificata tramite fattura
        public bool RichiediAnnulloBigliettoIvaCertificataTramiteFattura(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        // Annullo abbonamento IVA da assolvere
        public bool RichiediAnnulloAbbonamentoIvaDaAssolvere(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_ABBONAMENTO, "", 0);
        }

        // Annullo abbonamento IVA certificata tramite fattura
        public bool RichiediAnnulloAbbonamentoIvaCertificataTramiteFattura(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend)
        {
            return RichiediAnnullo(oParametriServerFiscale, nIdVend, clsClientServerFiscale.SEND_ANNULLO_ABBONAMENTO_CERTIFICATO_TRAMITE_FATTURA, "", 0);
        }

        // Annullo sigillo
        private bool RichiediAnnullo(clsParametriServerFiscale oParametriServerFiscale, Int64 nIdVend, string TipoDiAnnullo, string CodiceBigliettoAbbonamento, long NumProgBigliettoAbbonamento)
        {
            bool lRet = GetParametriConnessioneServerFiscale(oParametriServerFiscale);

            if (lRet)
            {
                try
                {
                    if (Connect())
                    {
                        string cSend = cClient_ID + cPassword + TipoDiAnnullo + nIdVend.ToString("00000000");

                        if (TipoDiAnnullo == clsClientServerFiscale.SEND_ANNULLO_BIGLIETTO_IVA_PREASSOLTA)
                            cSend += NumProgBigliettoAbbonamento.ToString().Trim().PadLeft(6, '0').Trim() + CodiceBigliettoAbbonamento;

                        if (Send(cSend))
                        {
                            string cReceive = Receive();
                            if (!string.IsNullOrEmpty(cReceive))
                            {
                                lRet = (cReceive.Length >= 25) &&
                                       cReceive.Substring(0, 3) == cClient_ID &&
                                       cReceive.Substring(3, 8) == cPassword &&
                                       cReceive.Substring(11, 3) == TipoDiAnnullo &&
                                       cReceive.Substring(14, 3) == "000" &&
                                       cReceive.Substring(17, 8) == nIdVend.ToString("00000000");
                                if (lRet)
                                {
                                    cSend = cClient_ID + cPassword + TipoDiAnnullo + "000";
                                }
                                else
                                {
                                    cSend = cClient_ID + cPassword + TipoDiAnnullo + "001";
                                }

                                if (oClient.Connected)
                                    Send(cSend);
                            }
                            else
                                lRet = false;
                        }
                        Close();
                    }
                }
                catch (Exception exRichiediAnnullo)
                {
                    ExceptionClient = exRichiediAnnullo;
                    lRet = false;
                }
            }
            return lRet;
        }


        public void Dispose()
        {
            if (oStreamWriter != null)
            {
                oStreamWriter.Dispose();
                oStreamWriter = null;
            }

            if (oNetworkStream != null)
            {
                oNetworkStream.Dispose();
                oNetworkStream = null;
            }
            if (oClient != null)
            {
                oClient.Dispose();
                oClient = null;
            }




            oExceptionClient = null;
        }

        #endregion
    }
}
