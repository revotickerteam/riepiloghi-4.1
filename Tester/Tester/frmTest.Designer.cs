﻿
namespace Tester
{
    partial class frmTest
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlConfig = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlStringConn = new System.Windows.Forms.Panel();
            this.lblStringCon = new System.Windows.Forms.Label();
            this.txtConnessione = new System.Windows.Forms.TextBox();
            this.pnlNumeroClients = new System.Windows.Forms.Panel();
            this.lblNumClients = new System.Windows.Forms.Label();
            this.numClients = new System.Windows.Forms.NumericUpDown();
            this.pnlCheckBiglietti = new System.Windows.Forms.Panel();
            this.lblCheckBiglietti = new System.Windows.Forms.Label();
            this.chkBiglietti = new System.Windows.Forms.CheckBox();
            this.pnlCheckAbbonamenti = new System.Windows.Forms.Panel();
            this.lblCheckAbbonamenti = new System.Windows.Forms.Label();
            this.chkAbbonamenti = new System.Windows.Forms.CheckBox();
            this.pnlMaxTrans = new System.Windows.Forms.Panel();
            this.lblMaxTrans = new System.Windows.Forms.Label();
            this.numMaxTrans = new System.Windows.Forms.NumericUpDown();
            this.pnlPercBiglietti = new System.Windows.Forms.Panel();
            this.lblPercBiglietti = new System.Windows.Forms.Label();
            this.percBiglietti = new System.Windows.Forms.NumericUpDown();
            this.pnlLimiteDataOra = new System.Windows.Forms.Panel();
            this.lblLimiteDataOra = new System.Windows.Forms.Label();
            this.dtFineDataOra = new System.Windows.Forms.DateTimePicker();
            this.pnlTempoStampa = new System.Windows.Forms.Panel();
            this.lblTempoStampa = new System.Windows.Forms.Label();
            this.numStampa = new System.Windows.Forms.NumericUpDown();
            this.pnlPauseTransazioni = new System.Windows.Forms.Panel();
            this.lblPausaTransazioni = new System.Windows.Forms.Label();
            this.numPausa = new System.Windows.Forms.NumericUpDown();
            this.pnlLogger = new System.Windows.Forms.Panel();
            this.pnlExe = new System.Windows.Forms.FlowLayoutPanel();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnChiudi = new System.Windows.Forms.Button();
            this.splitterConfigExe = new System.Windows.Forms.Splitter();
            this.splitterExeLog = new System.Windows.Forms.Splitter();
            this.pnlConfig.SuspendLayout();
            this.pnlStringConn.SuspendLayout();
            this.pnlNumeroClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numClients)).BeginInit();
            this.pnlCheckBiglietti.SuspendLayout();
            this.pnlCheckAbbonamenti.SuspendLayout();
            this.pnlMaxTrans.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxTrans)).BeginInit();
            this.pnlPercBiglietti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.percBiglietti)).BeginInit();
            this.pnlLimiteDataOra.SuspendLayout();
            this.pnlTempoStampa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStampa)).BeginInit();
            this.pnlPauseTransazioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPausa)).BeginInit();
            this.pnlExe.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlConfig
            // 
            this.pnlConfig.AutoScroll = true;
            this.pnlConfig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConfig.Controls.Add(this.pnlStringConn);
            this.pnlConfig.Controls.Add(this.pnlNumeroClients);
            this.pnlConfig.Controls.Add(this.pnlCheckBiglietti);
            this.pnlConfig.Controls.Add(this.pnlCheckAbbonamenti);
            this.pnlConfig.Controls.Add(this.pnlMaxTrans);
            this.pnlConfig.Controls.Add(this.pnlPercBiglietti);
            this.pnlConfig.Controls.Add(this.pnlLimiteDataOra);
            this.pnlConfig.Controls.Add(this.pnlTempoStampa);
            this.pnlConfig.Controls.Add(this.pnlPauseTransazioni);
            this.pnlConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlConfig.Location = new System.Drawing.Point(0, 0);
            this.pnlConfig.Name = "pnlConfig";
            this.pnlConfig.Size = new System.Drawing.Size(670, 178);
            this.pnlConfig.TabIndex = 0;
            // 
            // pnlStringConn
            // 
            this.pnlStringConn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStringConn.Controls.Add(this.lblStringCon);
            this.pnlStringConn.Controls.Add(this.txtConnessione);
            this.pnlStringConn.Location = new System.Drawing.Point(3, 3);
            this.pnlStringConn.Name = "pnlStringConn";
            this.pnlStringConn.Padding = new System.Windows.Forms.Padding(2);
            this.pnlStringConn.Size = new System.Drawing.Size(362, 33);
            this.pnlStringConn.TabIndex = 5;
            // 
            // lblStringCon
            // 
            this.lblStringCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStringCon.Location = new System.Drawing.Point(2, 2);
            this.lblStringCon.Margin = new System.Windows.Forms.Padding(0);
            this.lblStringCon.Name = "lblStringCon";
            this.lblStringCon.Size = new System.Drawing.Size(79, 27);
            this.lblStringCon.TabIndex = 0;
            this.lblStringCon.Text = "Connessione:";
            this.lblStringCon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtConnessione
            // 
            this.txtConnessione.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtConnessione.Location = new System.Drawing.Point(81, 2);
            this.txtConnessione.Name = "txtConnessione";
            this.txtConnessione.Size = new System.Drawing.Size(277, 27);
            this.txtConnessione.TabIndex = 1;
            this.txtConnessione.Text = "USER=CINEMA;PASSWORD=CINEMA;DATA SOURCE=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS =" +
    " (PROTOCOL = TCP)(HOST = 172.16.0.77)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAM" +
    "E = srv2019.webtic.it)))";
            // 
            // pnlNumeroClients
            // 
            this.pnlNumeroClients.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNumeroClients.Controls.Add(this.lblNumClients);
            this.pnlNumeroClients.Controls.Add(this.numClients);
            this.pnlNumeroClients.Location = new System.Drawing.Point(371, 3);
            this.pnlNumeroClients.Name = "pnlNumeroClients";
            this.pnlNumeroClients.Padding = new System.Windows.Forms.Padding(2);
            this.pnlNumeroClients.Size = new System.Drawing.Size(125, 33);
            this.pnlNumeroClients.TabIndex = 2;
            // 
            // lblNumClients
            // 
            this.lblNumClients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumClients.Location = new System.Drawing.Point(2, 2);
            this.lblNumClients.Margin = new System.Windows.Forms.Padding(0);
            this.lblNumClients.Name = "lblNumClients";
            this.lblNumClients.Size = new System.Drawing.Size(55, 27);
            this.lblNumClients.TabIndex = 0;
            this.lblNumClients.Text = "Clients:";
            this.lblNumClients.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numClients
            // 
            this.numClients.Dock = System.Windows.Forms.DockStyle.Right;
            this.numClients.Location = new System.Drawing.Point(57, 2);
            this.numClients.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numClients.Name = "numClients";
            this.numClients.Size = new System.Drawing.Size(64, 27);
            this.numClients.TabIndex = 1;
            this.numClients.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numClients.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pnlCheckBiglietti
            // 
            this.pnlCheckBiglietti.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCheckBiglietti.Controls.Add(this.lblCheckBiglietti);
            this.pnlCheckBiglietti.Controls.Add(this.chkBiglietti);
            this.pnlCheckBiglietti.Location = new System.Drawing.Point(502, 3);
            this.pnlCheckBiglietti.Name = "pnlCheckBiglietti";
            this.pnlCheckBiglietti.Padding = new System.Windows.Forms.Padding(2);
            this.pnlCheckBiglietti.Size = new System.Drawing.Size(142, 33);
            this.pnlCheckBiglietti.TabIndex = 3;
            // 
            // lblCheckBiglietti
            // 
            this.lblCheckBiglietti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckBiglietti.Location = new System.Drawing.Point(2, 2);
            this.lblCheckBiglietti.Margin = new System.Windows.Forms.Padding(0);
            this.lblCheckBiglietti.Name = "lblCheckBiglietti";
            this.lblCheckBiglietti.Size = new System.Drawing.Size(54, 27);
            this.lblCheckBiglietti.TabIndex = 0;
            this.lblCheckBiglietti.Text = "Biglietti:";
            this.lblCheckBiglietti.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkBiglietti
            // 
            this.chkBiglietti.Checked = true;
            this.chkBiglietti.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBiglietti.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkBiglietti.Location = new System.Drawing.Point(56, 2);
            this.chkBiglietti.Margin = new System.Windows.Forms.Padding(0);
            this.chkBiglietti.Name = "chkBiglietti";
            this.chkBiglietti.Size = new System.Drawing.Size(82, 27);
            this.chkBiglietti.TabIndex = 1;
            this.chkBiglietti.Text = "Abilitato";
            this.chkBiglietti.UseVisualStyleBackColor = true;
            // 
            // pnlCheckAbbonamenti
            // 
            this.pnlCheckAbbonamenti.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCheckAbbonamenti.Controls.Add(this.lblCheckAbbonamenti);
            this.pnlCheckAbbonamenti.Controls.Add(this.chkAbbonamenti);
            this.pnlCheckAbbonamenti.Location = new System.Drawing.Point(3, 42);
            this.pnlCheckAbbonamenti.Name = "pnlCheckAbbonamenti";
            this.pnlCheckAbbonamenti.Padding = new System.Windows.Forms.Padding(2);
            this.pnlCheckAbbonamenti.Size = new System.Drawing.Size(175, 33);
            this.pnlCheckAbbonamenti.TabIndex = 4;
            // 
            // lblCheckAbbonamenti
            // 
            this.lblCheckAbbonamenti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckAbbonamenti.Location = new System.Drawing.Point(2, 2);
            this.lblCheckAbbonamenti.Margin = new System.Windows.Forms.Padding(0);
            this.lblCheckAbbonamenti.Name = "lblCheckAbbonamenti";
            this.lblCheckAbbonamenti.Size = new System.Drawing.Size(87, 27);
            this.lblCheckAbbonamenti.TabIndex = 0;
            this.lblCheckAbbonamenti.Text = "Abbonamenti:";
            this.lblCheckAbbonamenti.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkAbbonamenti
            // 
            this.chkAbbonamenti.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkAbbonamenti.Location = new System.Drawing.Point(89, 2);
            this.chkAbbonamenti.Margin = new System.Windows.Forms.Padding(0);
            this.chkAbbonamenti.Name = "chkAbbonamenti";
            this.chkAbbonamenti.Size = new System.Drawing.Size(82, 27);
            this.chkAbbonamenti.TabIndex = 1;
            this.chkAbbonamenti.Text = "Abilitato";
            this.chkAbbonamenti.UseVisualStyleBackColor = true;
            // 
            // pnlMaxTrans
            // 
            this.pnlMaxTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaxTrans.Controls.Add(this.lblMaxTrans);
            this.pnlMaxTrans.Controls.Add(this.numMaxTrans);
            this.pnlMaxTrans.Location = new System.Drawing.Point(184, 42);
            this.pnlMaxTrans.Name = "pnlMaxTrans";
            this.pnlMaxTrans.Padding = new System.Windows.Forms.Padding(2);
            this.pnlMaxTrans.Size = new System.Drawing.Size(262, 33);
            this.pnlMaxTrans.TabIndex = 9;
            // 
            // lblMaxTrans
            // 
            this.lblMaxTrans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaxTrans.Location = new System.Drawing.Point(2, 2);
            this.lblMaxTrans.Margin = new System.Windows.Forms.Padding(0);
            this.lblMaxTrans.Name = "lblMaxTrans";
            this.lblMaxTrans.Size = new System.Drawing.Size(180, 27);
            this.lblMaxTrans.TabIndex = 0;
            this.lblMaxTrans.Text = "Numero massimo di transazioni";
            this.lblMaxTrans.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numMaxTrans
            // 
            this.numMaxTrans.Dock = System.Windows.Forms.DockStyle.Right;
            this.numMaxTrans.Location = new System.Drawing.Point(182, 2);
            this.numMaxTrans.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMaxTrans.Name = "numMaxTrans";
            this.numMaxTrans.Size = new System.Drawing.Size(76, 27);
            this.numMaxTrans.TabIndex = 1;
            this.numMaxTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pnlPercBiglietti
            // 
            this.pnlPercBiglietti.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPercBiglietti.Controls.Add(this.lblPercBiglietti);
            this.pnlPercBiglietti.Controls.Add(this.percBiglietti);
            this.pnlPercBiglietti.Location = new System.Drawing.Point(452, 42);
            this.pnlPercBiglietti.Name = "pnlPercBiglietti";
            this.pnlPercBiglietti.Padding = new System.Windows.Forms.Padding(2);
            this.pnlPercBiglietti.Size = new System.Drawing.Size(192, 33);
            this.pnlPercBiglietti.TabIndex = 8;
            // 
            // lblPercBiglietti
            // 
            this.lblPercBiglietti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPercBiglietti.Location = new System.Drawing.Point(2, 2);
            this.lblPercBiglietti.Margin = new System.Windows.Forms.Padding(0);
            this.lblPercBiglietti.Name = "lblPercBiglietti";
            this.lblPercBiglietti.Size = new System.Drawing.Size(138, 27);
            this.lblPercBiglietti.TabIndex = 0;
            this.lblPercBiglietti.Text = "% Biglietti/Abbonamenti:";
            this.lblPercBiglietti.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // percBiglietti
            // 
            this.percBiglietti.Dock = System.Windows.Forms.DockStyle.Right;
            this.percBiglietti.Location = new System.Drawing.Point(140, 2);
            this.percBiglietti.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.percBiglietti.Name = "percBiglietti";
            this.percBiglietti.Size = new System.Drawing.Size(48, 27);
            this.percBiglietti.TabIndex = 4;
            this.percBiglietti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.percBiglietti.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pnlLimiteDataOra
            // 
            this.pnlLimiteDataOra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLimiteDataOra.Controls.Add(this.lblLimiteDataOra);
            this.pnlLimiteDataOra.Controls.Add(this.dtFineDataOra);
            this.pnlLimiteDataOra.Location = new System.Drawing.Point(3, 81);
            this.pnlLimiteDataOra.Name = "pnlLimiteDataOra";
            this.pnlLimiteDataOra.Padding = new System.Windows.Forms.Padding(2);
            this.pnlLimiteDataOra.Size = new System.Drawing.Size(352, 33);
            this.pnlLimiteDataOra.TabIndex = 7;
            // 
            // lblLimiteDataOra
            // 
            this.lblLimiteDataOra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLimiteDataOra.Location = new System.Drawing.Point(2, 2);
            this.lblLimiteDataOra.Margin = new System.Windows.Forms.Padding(0);
            this.lblLimiteDataOra.Name = "lblLimiteDataOra";
            this.lblLimiteDataOra.Size = new System.Drawing.Size(122, 27);
            this.lblLimiteDataOra.TabIndex = 0;
            this.lblLimiteDataOra.Text = "Eventi fino alla data:";
            this.lblLimiteDataOra.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtFineDataOra
            // 
            this.dtFineDataOra.Dock = System.Windows.Forms.DockStyle.Right;
            this.dtFineDataOra.Location = new System.Drawing.Point(124, 2);
            this.dtFineDataOra.Name = "dtFineDataOra";
            this.dtFineDataOra.Size = new System.Drawing.Size(224, 27);
            this.dtFineDataOra.TabIndex = 1;
            // 
            // pnlTempoStampa
            // 
            this.pnlTempoStampa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTempoStampa.Controls.Add(this.lblTempoStampa);
            this.pnlTempoStampa.Controls.Add(this.numStampa);
            this.pnlTempoStampa.Location = new System.Drawing.Point(361, 81);
            this.pnlTempoStampa.Name = "pnlTempoStampa";
            this.pnlTempoStampa.Padding = new System.Windows.Forms.Padding(2);
            this.pnlTempoStampa.Size = new System.Drawing.Size(221, 33);
            this.pnlTempoStampa.TabIndex = 5;
            // 
            // lblTempoStampa
            // 
            this.lblTempoStampa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTempoStampa.Location = new System.Drawing.Point(2, 2);
            this.lblTempoStampa.Margin = new System.Windows.Forms.Padding(0);
            this.lblTempoStampa.Name = "lblTempoStampa";
            this.lblTempoStampa.Size = new System.Drawing.Size(139, 27);
            this.lblTempoStampa.TabIndex = 0;
            this.lblTempoStampa.Text = "Millisecondi per stampa:";
            this.lblTempoStampa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numStampa
            // 
            this.numStampa.Dock = System.Windows.Forms.DockStyle.Right;
            this.numStampa.Location = new System.Drawing.Point(141, 2);
            this.numStampa.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numStampa.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numStampa.Name = "numStampa";
            this.numStampa.Size = new System.Drawing.Size(76, 27);
            this.numStampa.TabIndex = 1;
            this.numStampa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numStampa.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pnlPauseTransazioni
            // 
            this.pnlPauseTransazioni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPauseTransazioni.Controls.Add(this.lblPausaTransazioni);
            this.pnlPauseTransazioni.Controls.Add(this.numPausa);
            this.pnlPauseTransazioni.Location = new System.Drawing.Point(3, 120);
            this.pnlPauseTransazioni.Name = "pnlPauseTransazioni";
            this.pnlPauseTransazioni.Padding = new System.Windows.Forms.Padding(2);
            this.pnlPauseTransazioni.Size = new System.Drawing.Size(256, 33);
            this.pnlPauseTransazioni.TabIndex = 6;
            // 
            // lblPausaTransazioni
            // 
            this.lblPausaTransazioni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPausaTransazioni.Location = new System.Drawing.Point(2, 2);
            this.lblPausaTransazioni.Margin = new System.Windows.Forms.Padding(0);
            this.lblPausaTransazioni.Name = "lblPausaTransazioni";
            this.lblPausaTransazioni.Size = new System.Drawing.Size(174, 27);
            this.lblPausaTransazioni.TabIndex = 0;
            this.lblPausaTransazioni.Text = "Millisecondi pause transazioni:";
            this.lblPausaTransazioni.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numPausa
            // 
            this.numPausa.Dock = System.Windows.Forms.DockStyle.Right;
            this.numPausa.Location = new System.Drawing.Point(176, 2);
            this.numPausa.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numPausa.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPausa.Name = "numPausa";
            this.numPausa.Size = new System.Drawing.Size(76, 27);
            this.numPausa.TabIndex = 1;
            this.numPausa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPausa.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // pnlLogger
            // 
            this.pnlLogger.AutoScroll = true;
            this.pnlLogger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLogger.Location = new System.Drawing.Point(0, 244);
            this.pnlLogger.Name = "pnlLogger";
            this.pnlLogger.Size = new System.Drawing.Size(670, 206);
            this.pnlLogger.TabIndex = 1;
            // 
            // pnlExe
            // 
            this.pnlExe.AutoScroll = true;
            this.pnlExe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlExe.Controls.Add(this.btnStart);
            this.pnlExe.Controls.Add(this.btnStop);
            this.pnlExe.Controls.Add(this.btnClear);
            this.pnlExe.Controls.Add(this.btnChiudi);
            this.pnlExe.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlExe.Location = new System.Drawing.Point(0, 190);
            this.pnlExe.Name = "pnlExe";
            this.pnlExe.Size = new System.Drawing.Size(670, 42);
            this.pnlExe.TabIndex = 2;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(3, 3);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 33);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Avvia";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(109, 3);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 33);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(215, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 33);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Pulisci";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnChiudi
            // 
            this.btnChiudi.Location = new System.Drawing.Point(321, 3);
            this.btnChiudi.Name = "btnChiudi";
            this.btnChiudi.Size = new System.Drawing.Size(100, 33);
            this.btnChiudi.TabIndex = 7;
            this.btnChiudi.Text = "Chiudi";
            this.btnChiudi.UseVisualStyleBackColor = true;
            // 
            // splitterConfigExe
            // 
            this.splitterConfigExe.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitterConfigExe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitterConfigExe.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterConfigExe.Location = new System.Drawing.Point(0, 178);
            this.splitterConfigExe.Name = "splitterConfigExe";
            this.splitterConfigExe.Size = new System.Drawing.Size(670, 12);
            this.splitterConfigExe.TabIndex = 3;
            this.splitterConfigExe.TabStop = false;
            // 
            // splitterExeLog
            // 
            this.splitterExeLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitterExeLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitterExeLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterExeLog.Location = new System.Drawing.Point(0, 232);
            this.splitterExeLog.Name = "splitterExeLog";
            this.splitterExeLog.Size = new System.Drawing.Size(670, 12);
            this.splitterExeLog.TabIndex = 4;
            this.splitterExeLog.TabStop = false;
            // 
            // frmTest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(670, 450);
            this.Controls.Add(this.pnlLogger);
            this.Controls.Add(this.splitterExeLog);
            this.Controls.Add(this.pnlExe);
            this.Controls.Add(this.splitterConfigExe);
            this.Controls.Add(this.pnlConfig);
            this.Font = new System.Drawing.Font("Bahnschrift Light Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tester";
            this.pnlConfig.ResumeLayout(false);
            this.pnlStringConn.ResumeLayout(false);
            this.pnlStringConn.PerformLayout();
            this.pnlNumeroClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numClients)).EndInit();
            this.pnlCheckBiglietti.ResumeLayout(false);
            this.pnlCheckAbbonamenti.ResumeLayout(false);
            this.pnlMaxTrans.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numMaxTrans)).EndInit();
            this.pnlPercBiglietti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.percBiglietti)).EndInit();
            this.pnlLimiteDataOra.ResumeLayout(false);
            this.pnlTempoStampa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numStampa)).EndInit();
            this.pnlPauseTransazioni.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numPausa)).EndInit();
            this.pnlExe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel pnlConfig;
        private System.Windows.Forms.Panel pnlCheckAbbonamenti;
        private System.Windows.Forms.CheckBox chkAbbonamenti;
        private System.Windows.Forms.Label lblCheckAbbonamenti;
        private System.Windows.Forms.Panel pnlCheckBiglietti;
        private System.Windows.Forms.CheckBox chkBiglietti;
        private System.Windows.Forms.Label lblCheckBiglietti;
        private System.Windows.Forms.Panel pnlNumeroClients;
        private System.Windows.Forms.NumericUpDown numClients;
        private System.Windows.Forms.Label lblNumClients;
        private System.Windows.Forms.Panel pnlTempoStampa;
        private System.Windows.Forms.Label lblTempoStampa;
        private System.Windows.Forms.NumericUpDown numStampa;
        private System.Windows.Forms.Panel pnlStringConn;
        private System.Windows.Forms.Label lblStringCon;
        private System.Windows.Forms.TextBox txtConnessione;
        private System.Windows.Forms.Panel pnlLogger;
        private System.Windows.Forms.FlowLayoutPanel pnlExe;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnChiudi;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Splitter splitterConfigExe;
        private System.Windows.Forms.Splitter splitterExeLog;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel pnlPauseTransazioni;
        private System.Windows.Forms.Label lblPausaTransazioni;
        private System.Windows.Forms.NumericUpDown numPausa;
        private System.Windows.Forms.Panel pnlLimiteDataOra;
        private System.Windows.Forms.Label lblLimiteDataOra;
        private System.Windows.Forms.DateTimePicker dtFineDataOra;
        private System.Windows.Forms.Panel pnlPercBiglietti;
        private System.Windows.Forms.Label lblPercBiglietti;
        private System.Windows.Forms.NumericUpDown percBiglietti;
        private System.Windows.Forms.Panel pnlMaxTrans;
        private System.Windows.Forms.Label lblMaxTrans;
        private System.Windows.Forms.NumericUpDown numMaxTrans;
    }
}

