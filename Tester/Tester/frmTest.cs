﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wintic.Data;

namespace Tester
{
    public partial class frmTest : Form
    {
        public class TIPO_BIGLIETTO
        {
            public int IDTIPOBIGL { get; set; }
            public string DESCR { get; set; }
            public string TIPO_TITOLO { get; set; }

            public string TASTO { get; set; }
        }

        public class BIGLIETTO
        {
            public TIPO_BIGLIETTO TIPO_BIGLIETTO { get; set; }
            public int IDBIGLIETTO { get; set; }
            public decimal PREZZO_LORDO { get; set; }
            public decimal DIR_PREV { get; set; }
            public decimal DIR_PREN { get; set; }
            public decimal DIR_INTERNET { get; set; }
        }

        public class TARIFFA
        {
            public int IDTARIFFA { get; set; }
            public string DESCR { get; set; }
            public List<BIGLIETTO> BIGLIETTI { get; set; }
        }

        public class POSTO
        {
            public string IDPOSTO { get; set; }
        }

        public class SALA
        {
            public int IDSALA { get; set; }
            public string DESCR { get; set; }
            public List<POSTO> POSTI { get; set; }
        }

        public class VENDITA
        {
            public int IDVEND { get; set; }
            public string SIGILLO { get; set; }
            public int IDBIGLIETTO => BIGLIETTO != null ? BIGLIETTO.IDBIGLIETTO : 0;
            public string TIPO { get; set; }
            public int NUM_BIGL { get; set; }
            public string NOMEPRE { get; set; }
            public string VALUTA { get; set; }
            public string IDPOSTO { get; set; }
            public int IDCALEND { get; set; }
            public int PREVENDUTO { get; set; }
            public BIGLIETTO BIGLIETTO { get; set; }
            public TESSERA TESSERA { get; set; }
            public CALENDARIO CALENDARIO { get; set; }
        }

        public class TESSERA
        {
            public int IDTEX { get; set; }
            public int PIN { get; set; }
            public int TOTING { get; set; }
            public string DOTAZIONEF { get; set; }
            public DateTime SCADENZA { get; set; }
            public int RIMANENZA { get; set; }
            public int Qta { get; set; }
        }

        public class SMART_VENDITE
        {
            public int IDVEND { get; set; }
            public string SIGILLO { get; set; }
            public bool VENDITE_TRACING_INSERTED { get; set; }
        }

        public class CALENDARIO
        {
            public int IDCALEND { get; set; }
            public DateTime DATA_ORA { get; set; }
            public string TE { get; set; }
            public string TITOLO { get; set; }
            public TARIFFA TARIFFA { get; set; }
            public SALA SALA { get; set; }
            public List<POSTO> POSTIDOPPI { get; set; }
            //public IConnection Connection { get; set; }
            public object objLock { get; set; }

            public CALENDARIO(IConnection conn, int idcalend)
            {
                this.objLock = new object();
                this.IDCALEND = idcalend;
                this.POSTIDOPPI = new List<POSTO>();
                StringBuilder oSB = null;
                DataTable table = null;
                oSB = new StringBuilder();
                oSB.Append("SELECT CALENDARIO.IDCALEND");
                oSB.Append("     , CALENDARIO.IDSALA");
                oSB.Append("     , CALENDARIO.IDTARIFFA");
                oSB.Append("     , CALENDARIO.IDPACCHETTO");
                oSB.Append("     , CALENDARIO.DATA_ORA");
                oSB.Append("     , CALENDARIO.TE");
                oSB.Append("     , PACCHETTO.DESCRIZIONE AS TITOLO");
                oSB.Append("     , TARIFFE.DESCR AS DESCR_TARIFFA");
                oSB.Append("     , SALE.DESCR AS DESCR_SALA");
                oSB.Append(" FROM CINEMA.CALENDARIO ");
                oSB.Append(" INNER JOIN CINEMA.SALE ON SALE.IDSALA = CALENDARIO.IDSALA");
                oSB.Append(" INNER JOIN CINEMA.TARIFFE ON TARIFFE.IDTARIFFA = CALENDARIO.IDTARIFFA");
                oSB.Append(" INNER JOIN CINEMA.PACCHETTO ON PACCHETTO.IDPACCHETTO = CALENDARIO.IDPACCHETTO AND PACCHETTO.PROGR = 1");
                oSB.Append(" WHERE CALENDARIO.IDCALEND = :pIDCALEND");
                table = conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pIDCALEND", this.IDCALEND));
                if (table.Rows.Count > 0)
                {
                    this.DATA_ORA = (DateTime)table.Rows[0]["DATA_ORA"];
                    this.TE = table.Rows[0]["TE"].ToString();
                    this.TITOLO = table.Rows[0]["TITOLO"].ToString();
                    this.SALA = new SALA()
                    {
                        IDSALA = int.Parse(table.Rows[0]["IDSALA"].ToString()),
                        DESCR = table.Rows[0]["DESCR_SALA"].ToString(),
                        POSTI = new List<POSTO>()
                    };
                    this.TARIFFA = new TARIFFA()
                    {
                        IDTARIFFA = int.Parse(table.Rows[0]["IDTARIFFA"].ToString()),
                        DESCR = table.Rows[0]["DESCR_TARIFFA"].ToString(),
                        BIGLIETTI = new List<BIGLIETTO>()
                    };
                    table.Dispose();

                    if (!sale.ContainsKey(this.SALA.IDSALA))
                    {
                        DataTable tableSub = conn.ExecuteQuery("SELECT * FROM CINEMA.POSTI WHERE IDSALA = :pIDSALA ORDER BY FILA, COLONNA", new clsParameters(":pIDSALA", this.SALA.IDSALA));
                        if (tableSub.Rows.Count > 0)
                        {
                            foreach (DataRow row in tableSub.Rows)
                                this.SALA.POSTI.Add(new POSTO() { IDPOSTO = row["IDPOSTO"].ToString() });
                        }
                        tableSub.Dispose();
                        sale.Add(this.SALA.IDSALA, new List<POSTO>(this.SALA.POSTI.ToArray()));
                    }
                    else
                        this.SALA.POSTI = new List<POSTO>(sale[this.SALA.IDSALA].ToArray());

                    if (!tariffe.ContainsKey(this.TARIFFA.IDTARIFFA))
                    {

                        oSB = new StringBuilder();
                        oSB.Append("SELECT BIGLIETTO.IDBIGLIETTO");
                        oSB.Append("     , BIGLIETTO.PREZZO_LORDO");
                        oSB.Append("     , BIGLIETTO.DIR_PREV");
                        oSB.Append("     , BIGLIETTO.DIR_PREN");
                        oSB.Append("     , BIGLIETTO.DIR_INTERNET");
                        oSB.Append("     , BIGLIETTO.IDTIPOBIGL");
                        oSB.Append("     , TIPO_BIGLIETTO.DESCR");
                        oSB.Append("     , TIPO_BIGLIETTO.TASTO");
                        oSB.Append("     , TIPO_BIGLIETTO.TIPO_TITOLO");
                        oSB.Append(" FROM CINEMA.BIGLIETTO INNER JOIN CINEMA.TIPO_BIGLIETTO ON TIPO_BIGLIETTO.IDTIPOBIGL = BIGLIETTO.IDTIPOBIGL");
                        oSB.Append(" WHERE BIGLIETTO.IDTARIFFA = :pIDTARIFFA");
                        oSB.Append("   AND BIGLIETTO.FINE_VALIDITA IS NULL");
                        oSB.Append("   AND TIPO_BIGLIETTO.TASTO NOT LIKE 'T%'"); // per ora escludo i ratei
                        oSB.Append("   AND TIPO_BIGLIETTO.TASTO NOT LIKE 'P%'"); // per ora escludo i ratei

                        DataTable tableSub = conn.ExecuteQuery(oSB.ToString(), new clsParameters(":pIDTARIFFA", this.TARIFFA.IDTARIFFA));
                        if (tableSub.Rows.Count > 0)
                        {
                            foreach (DataRow row in tableSub.Rows)
                                this.TARIFFA.BIGLIETTI.Add(new BIGLIETTO()
                                {
                                    IDBIGLIETTO = int.Parse(row["IDBIGLIETTO"].ToString()),
                                    PREZZO_LORDO = decimal.Parse(row["PREZZO_LORDO"].ToString()),
                                    DIR_PREV = decimal.Parse(row["DIR_PREV"].ToString()),
                                    DIR_PREN = decimal.Parse(row["DIR_PREN"].ToString()),
                                    DIR_INTERNET = decimal.Parse(row["DIR_INTERNET"].ToString()),
                                    TIPO_BIGLIETTO = new TIPO_BIGLIETTO() { IDTIPOBIGL = int.Parse(row["IDTIPOBIGL"].ToString()), DESCR = row["DESCR"].ToString(), TASTO = row["TASTO"].ToString(), TIPO_TITOLO = row["TIPO_TITOLO"].ToString() }
                                });
                        }
                        tableSub.Dispose();
                        tariffe.Add(this.TARIFFA.IDTARIFFA, new List<BIGLIETTO>(this.TARIFFA.BIGLIETTI.ToArray()));
                    }
                    else
                    {
                        this.TARIFFA.BIGLIETTI = new List<BIGLIETTO>(tariffe[this.TARIFFA.IDTARIFFA].ToArray());
                    }

                    DataTable tablePostiDoppi = conn.ExecuteQuery("SELECT DISTINCT IDPOSTO FROM CINEMA.POSTIDOPPI WHERE IDCALEND = :pIDCALEND", new clsParameters(":pIDCALEND", this.IDCALEND));
                    foreach (DataRow row in tablePostiDoppi.Rows)
                    {
                        this.POSTIDOPPI.Add(new POSTO() { IDPOSTO = row["IDPOSTO"].ToString() });
                    }
                }
            }

            public List<VENDITA> GetPosti(int qta, IConnection conn, out Exception error)
            {
                error = null;
                List<VENDITA> result = new List<VENDITA>();
                Random rnd = new Random(this.TARIFFA.BIGLIETTI.Count);
                lock (objLock)
                {
                    DataTable table = conn.ExecuteQuery("SELECT IDCALEND FROM CINEMA.V_CAR_SALE WHERE IDCALEND = :pIDCALEND", new clsParameters(":pIDCALEND", this.IDCALEND));
                    string tipoVendita = (table == null || table.Rows == null || table.Rows.Count == 0 ? "K" : "V");
                    while (qta > 0 && this.PostiDisponibili > 0 && error == null)
                    {

                        POSTO posto = this.SALA.POSTI.Where(p => !this.POSTIDOPPI.Exists(pd => pd.IDPOSTO == p.IDPOSTO)).ToList().FirstOrDefault();
                        if (posto != null)
                        {
                            try
                            {
                                clsParameters pars = new clsParameters();
                                pars.Add(":pIDCALEND", this.IDCALEND);
                                pars.Add(":pIDPOSTO", posto.IDPOSTO);
                                int inserted = (int)conn.ExecuteNonQuery("INSERT INTO CINEMA.POSTIDOPPI (IDCALEND, IDPOSTO, INSERTED) VALUES (:pIDCALEND, :pIDPOSTO, SYSDATE)", pars);
                                if (inserted > 0)
                                {
                                    int numBigl = 0;
                                    int index = rnd.Next(0, this.TARIFFA.BIGLIETTI.Count - 1);
                                    BIGLIETTO biglietto = this.TARIFFA.BIGLIETTI[index];

                                    if (result.FirstOrDefault(v => v.BIGLIETTO.IDBIGLIETTO == biglietto.IDBIGLIETTO) != null)
                                        numBigl = result.Where(v => v.BIGLIETTO.IDBIGLIETTO == biglietto.IDBIGLIETTO).Max(v => v.NUM_BIGL) + 1;
                                    else
                                    {
                                        pars = new clsParameters();
                                        pars.Add(":pIDCALEND", this.IDCALEND);
                                        pars.Add(":pIDBIGLIETTO", biglietto.IDBIGLIETTO);
                                        table = conn.ExecuteQuery("SELECT NVL(MAX(NUM_BIGL), 0) + 1 AS NUM_BIGL FROM CINEMA.VENDITE WHERE IDCALEND = :pIDCALEND AND IDBIGLIETTO = :pIDBIGLIETTO", pars);
                                        numBigl = int.Parse(table.Rows[0]["NUM_BIGL"].ToString());
                                        table.Dispose();
                                    }
                                    this.POSTIDOPPI.Add(posto);
                                    result.Add(new VENDITA() { BIGLIETTO = biglietto, IDCALEND = this.IDCALEND, IDPOSTO = posto.IDPOSTO, TIPO = tipoVendita, VALUTA = "EU", PREVENDUTO = -1, NUM_BIGL = numBigl, CALENDARIO = this });
                                }
                            }
                            catch (Exception EX)
                            {
                                error = EX;
                            }
                        }

                        if (posto != null)
                        {

                        }
                        qta -= 1;
                    }
                }
                return result;
            }

            public int PostiDisponibili => this.SALA.POSTI.Count - this.POSTIDOPPI.Count;

            public static void InitCalendariInProgrammazione(IConnection conn, DateTime fineDataOra)
            {
                sale = new Dictionary<int, List<POSTO>>();
                tariffe = new Dictionary<int, List<BIGLIETTO>>();
                List<CALENDARIO> result = new List<CALENDARIO>();
                DataTable table = conn.ExecuteQuery("SELECT IDCALEND FROM CINEMA.CALENDARIO WHERE DATA_ORA BETWEEN SYSDATE AND :pFINE_DATA_ORA + 1 - (1/86400) ORDER BY TRUNC(DATA_ORA), IDSALA, DATA_ORA, IDCALEND", new clsParameters(":pFINE_DATA_ORA", fineDataOra.Date));
                foreach (DataRow row in table.Rows)
                {
                    CALENDARIO calendario = new CALENDARIO(conn, int.Parse(row["IDCALEND"].ToString()));
                    result.Add(calendario);
                }
                CalendariInProgrammazione = result;
            }

            public static List<CALENDARIO> CalendariInProgrammazione { get; set; }
            public static Dictionary<int, List<POSTO>> sale = new Dictionary<int, List<POSTO>>();
            public static Dictionary<int, List<BIGLIETTO>> tariffe = new Dictionary<int, List<BIGLIETTO>>();
            public static Queue<int> CASSE = new Queue<int>();
            public static Queue<int> OPERATORI = new Queue<int>();
            public static clsParametriServerFiscale ParametriServerFiscale { get; set; }
            public static int PercBiglietti { get; set; }
            public static int MaxTransazioni { get; set; }
            public static bool AbbonamentiAbilitati => MaxAbbonamenti > 0;
            public static int CounterBiglietti {get;set;}
            public static int CounterAbbonamenti { get; set; }
            public static int MaxBiglietti { get; set; }
            public static int MaxAbbonamenti { get; set; }

            public static int Counter => CounterBiglietti + CounterAbbonamenti;
            public static List<SMART_VENDITE> SIGILLI { get; set; }

            public static List<VENDITA> GetPostiNewTransaction(int qta, IConnection conn, out Exception error)
            {
                error = null;
                List<VENDITA> result = null;
                if (CalendariInProgrammazione.FirstOrDefault(c => c.PostiDisponibili > 0) != null)
                {
                    List<CALENDARIO> calendariDisponibili = new List<CALENDARIO>();

                    int count = CalendariInProgrammazione.Where(c => c.PostiDisponibili > 0).ToList().Count;
                    Random rnd = new Random(count);
                    int index = rnd.Next(0, count - 1);
                    CALENDARIO calendario = CalendariInProgrammazione[index];
                    result = calendario.GetPosti(qta, conn, out error);
                }    
                return result;
            }

            public static bool EliminaVendita(IConnection conn, int IDVEND)
            {
                conn.ExecuteNonQuery("DELETE FROM CINEMA.VENDITE WHERE IDCALEND = :pIDVEND", new clsParameters(":pIDVEND", IDVEND));
                return true;
            }
            public static bool LiberaPosti(IConnection conn, int IDCALEND, string IDPOSTO)
            {
                clsParameters pars = new clsParameters();
                pars.Add(":pIDCALEND", IDCALEND);
                pars.Add(":pIDPOSTO", IDPOSTO);
                conn.ExecuteNonQuery("DELETE FROM CINEMA.POSTIDOPPI WHERE IDCALEND = :pIDCALEND AND IDPOSTO = :pIDPOSTO", pars);
                return true;
            }

            public static int GetIdCassa(IConnection conn, out string descCassa)
            {
                int result = 0;
                DataTable table = null;
                if (CASSE == null || CASSE.Count == 0)
                {
                    if (CASSE == null) CASSE = new Queue<int>();
                    table = conn.ExecuteQuery("SELECT IDCASSA FROM CINEMA.CASSE WHERE CODICE_SUPPORTO_IDENTIFICATIVO = 'BT' ORDER BY IDCASSA");
                    foreach (DataRow row in table.Rows)
                    {
                        CASSE.Enqueue(int.Parse(row["IDCASSA"].ToString()));
                    }
                    table.Dispose();
                }
                result = CASSE.Dequeue();
                table = conn.ExecuteQuery("SELECT PC FROM CINEMA.CASSE WHERE IDCASSA = :pIDCASSA", new clsParameters(":pIDCASSA", result));
                descCassa = table.Rows[0]["PC"].ToString();
                table.Dispose();
                return result;
            }

            public static int GetIdOperatore(IConnection conn, out string descOperatore)
            {
                int result = 0;
                DataTable table = null;
                if (OPERATORI == null || OPERATORI.Count == 0)
                {
                    if (OPERATORI == null) OPERATORI = new Queue<int>();
                    table = conn.ExecuteQuery("SELECT IDOPERATORE FROM CINEMA.OPERATORI ORDER BY IDOPERATORE");
                    foreach (DataRow row in table.Rows)
                    {
                        OPERATORI.Enqueue(int.Parse(row["IDOPERATORE"].ToString()));
                    }
                }
                result = OPERATORI.Dequeue();
                table = conn.ExecuteQuery("SELECT LOGIN||' '||NVL(COGNOME,'')||' '||NVL(NOME,'') AS DESCR FROM CINEMA.OPERATORI ORDER BY IDOPERATORE");
                descOperatore = table.Rows[0]["DESCR"].ToString();
                table.Dispose();
                return result;
            }

            public static TESSERA GetTessera(IConnection conn, string tasto, bool nuovaTessera, out Exception error)
            {
                error = null;
                TESSERA result = null;

                StringBuilder oSB = null;
                clsParameters pars = null;

                try
                {
                    oSB = new StringBuilder();
                    oSB.Append("SELECT TESSERE.IDTEX");
                    oSB.Append("     , TESSERE.PIN");
                    oSB.Append("     , TESSERE.DOTAZIONEF");
                    oSB.Append("     , TESSERE.RIMANENZA");
                    oSB.Append("     , TESSERE.SCADENZA");
                    oSB.Append("     , TESSERE.TOTING");
                    oSB.Append(" FROM TEX.TESSERE");

                    if (!nuovaTessera)
                    {
                        oSB.Append(" WHERE TESSERE.ENABLED = 1");
                        oSB.Append("   AND TRUNC(TESSERE.SCADENZA) >= TRUNC(SYSDATE)");
                        oSB.Append("   AND TESSERE.RIMANENZA > 0");
                        oSB.Append("   AND TESSERE.DOTAZIONEF IS NOT NULL");
                        oSB.Append("   AND TESSERE.TASTO = :pTASTO");
                    }
                    else
                    {
                        oSB.Append(" WHERE TESSERE.ENABLED = 0");
                        oSB.Append("   AND TESSERE.DOTAZIONEF IS NULL");
                        oSB.Append("   AND TESSERE.TASTO = :pTASTO");
                    }
                    pars = new clsParameters(":pTASTO", tasto);
                    DataTable table = conn.ExecuteQuery(oSB.ToString(), pars);
                    if (table.Rows.Count > 0)
                    {
                        DataRow row = table.Rows[0];
                        result = new TESSERA()
                        {
                            IDTEX = int.Parse(row["IDTEX"].ToString()),
                            PIN = int.Parse(row["PIN"].ToString()),
                            DOTAZIONEF = nuovaTessera ? "" : row["DOTAZIONEF"].ToString(),
                            SCADENZA = nuovaTessera ? DateTime.MinValue : (DateTime)row["SCADENZA"],
                            RIMANENZA = nuovaTessera ? 0 : int.Parse(row["RIMANENZA"].ToString()),
                            TOTING = int.Parse(row["TOTING"].ToString())
                        };
                    }
                }
                catch (Exception ex)
                {
                    error = ex;
                }

                return result;
            }
        }

        public class pnlThread : Panel
        {
            public int IDCASSA { get; set; }
            public string DESC_CASSA { get; set; }
            public int NING_EMESSI { get; set; }

            public string DESC_OPERATORE { get; set; }
            public System.Threading.Thread Thread { get; set; }
            private TextBox txtLog { get; set; }

            public pnlThread MainPanel { get; set; }
            public bool CanStart { get; set; }
            public int ClientId { get; set; }
            
            public int TempoDiStampa { get; set; }
            public int PausaTransazioni { get; set; }

            public Queue<string> queueWrite { get; set; }
            public string fileName { get; set; }
            public bool Running { get; set; }
            
            public System.Threading.Thread threadWrite { get; set; }

            public pnlThread(System.Threading.Thread thread, pnlThread pnlMain = null)
                :base()
            {
                this.txtLog = new TextBox();
                this.txtLog.Dock = DockStyle.Fill;
                this.txtLog.Multiline = true;
                this.txtLog.ScrollBars = ScrollBars.Both;
                this.Controls.Add(this.txtLog);
                this.txtLog.BringToFront();
                this.Thread = thread;
                this.MainPanel = pnlMain;
            }

            public delegate void delagateMessage(string message, Exception error = null);
            public void Message(string message, Exception error = null)
            {
                if (this.InvokeRequired)
                {
                    delagateMessage d = new delagateMessage(this.Message);
                    this.Invoke(d, message, error);
                }
                else
                {
                    this.txtLog.Text += "\r\n" + message;
                    if (error != null)
                        this.txtLog.Text += "\r\n" + error.Message;
                    this.txtLog.SelectionLength = 0;
                    this.txtLog.SelectionStart = this.txtLog.Text.Length;
                    this.txtLog.ScrollToCaret();
                    Application.DoEvents();
                    //if (this.queueWrite == null)
                    //    this.queueWrite = new Queue<string>();
                    //lock (this.queueWrite)
                    //{
                    //    this.txtLog.Text += "\r\n" + message;
                    //    this.queueWrite.Enqueue(message);
                    //    if (error != null)
                    //    {
                    //        this.txtLog.Text += "\r\n" + error.Message;
                    //        this.queueWrite.Enqueue(error.Message);
                    //    }
                    //}
                    //if (this.threadWrite == null)
                    //{
                    //    System.IO.FileInfo fAppInfo = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    //    this.fileName = fAppInfo.Directory.FullName + string.Format(@"\logClient{0}.log", this.ClientId);
                    //    System.IO.File.WriteAllText(this.fileName, "");
                    //    this.threadWrite = new System.Threading.Thread(new System.Threading.ThreadStart(writeLog));
                    //    this.threadWrite.Start();
                    //}
                }
            }

            public static string GetErroreQuery(string sql, clsParameters pars = null)
            {
                StringBuilder oSB = new StringBuilder();
                oSB.AppendLine(sql);
                if (pars != null)
                {
                    pars.Parameters.ForEach(p =>
                    {
                        oSB.AppendLine(string.Format("{0} = {1}", p.Name, p.Value == null || p.Value == System.DBNull.Value ? "NULL" : "[" + p.Value.ToString() + "]"));
                    });
                }
                return oSB.ToString();
            }

            private void writeLog()
            {
                while (this.Running || this.queueWrite.Count > 0)
                {
                    lock (this.queueWrite)
                    {
                        while (this.queueWrite.Count > 0)
                        {
                            System.IO.File.AppendAllText(this.fileName, this.queueWrite.Dequeue() + "\r\n");
                        }
                    }
                }
                string s = "fine";
            }
        }
        public bool InEsecuzione { get; set; }
        public bool RequestStop { get; set; }

        public pnlThread pnlThreadMain { get; set; }
        public List<pnlThread> ThreadsPanels { get; set; }
        public frmTest()
        {
            InitializeComponent();
            this.Shown += FrmTest_Shown;
            
        }

        private void FrmTest_SizeChanged(object sender, EventArgs e)
        {
            this.AdjustSizePanels();
        }

        private void AdjustSizePanels()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker d = new MethodInvoker(AdjustSizePanels);
                this.Invoke(d);
            }
            else
            {
                if (this.pnlLogger.Controls.Count > 0)
                {
                    int width = 0;
                    List<pnlThread> panels = new List<pnlThread>();
                    foreach (Control c in this.pnlLogger.Controls)
                    {
                        width += c.Width;
                        try
                        {
                            pnlThread p = (pnlThread)c;
                            panels.Add(p);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    if (panels.Count > 0 && this.pnlLogger.ClientSize.Width > width)
                    {
                        width = (int)((this.pnlLogger.ClientSize.Width - width) / panels.Count);
                        foreach (Control c in panels)
                        {
                            c.Width += width;
                        }
                    }
                    Application.DoEvents();
                }
            }
        }

        private void FrmTest_Shown(object sender, EventArgs e)
        {
            this.InEsecuzione = false;
            this.RequestStop = false;
            this.SetAbilitazioni();
            this.btnStart.Click += BtnStart_Click;
            this.btnStop.Click += BtnStop_Click;
            this.btnChiudi.Click += BtnChiudi_Click;
            this.btnClear.Click += BtnClear_Click;
            this.chkAbbonamenti.CheckedChanged += (obj, eve) => { SetAbilitazioni(); };
            this.SizeChanged += FrmTest_SizeChanged;
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            if (!this.InEsecuzione)
            {
                if (this.ThreadsPanels != null && this.ThreadsPanels.Count > 0)
                {
                    this.pnlLogger.Controls.Clear();
                    this.ThreadsPanels.Clear();
                }
            }
        }

        private void BtnChiudi_Click(object sender, EventArgs e)
        {
            if (!this.InEsecuzione)
            {
                this.Close();
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (!this.InEsecuzione)
            {
                this.RequestStop = false;
                this.InEsecuzione = true;
                if (this.ThreadsPanels != null && this.ThreadsPanels.Count > 0)
                {
                    while (this.ThreadsPanels.FirstOrDefault(p => p.Running) != null)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    this.pnlLogger.Controls.Clear();
                    this.ThreadsPanels.Clear();
                }
                this.SetAbilitazioni();
                this.InitThreads();
            }
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            if (this.InEsecuzione)
            {
                this.RequestStop = true;
                this.SetAbilitazioni();
            }
        }

        private delegate void delegateSetMaxTransazioni(int value);
        private void SetMaxTransazioni(int value)
        {
            if (this.InvokeRequired)
                this.Invoke(new delegateSetMaxTransazioni(SetMaxTransazioni), value);
            else
            {
                this.numMaxTrans.Value = value;
                Application.DoEvents();
            }
        }
        public void SetAbilitazioni()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker d = new MethodInvoker(this.SetAbilitazioni);
                this.Invoke(d);
            }
            else
            {
                DateTime dNow = DateTime.Now;
                this.dtFineDataOra.Value = dNow;
                this.dtFineDataOra.MinDate = dNow;
                this.btnStart.Enabled = !this.InEsecuzione;
                this.btnStop.Enabled = this.InEsecuzione && !this.RequestStop;
                this.btnChiudi.Enabled = !this.InEsecuzione;
                this.btnClear.Enabled = !this.InEsecuzione;
                this.pnlConfig.Enabled = !this.InEsecuzione;
                if (this.chkAbbonamenti.Checked)
                {
                    this.percBiglietti.Enabled = true;
                }
                else
                {
                    this.percBiglietti.Value = 100;
                    this.percBiglietti.Enabled = false;
                }
            }
        }

        private void InitThreads()
        {
            if (this.ThreadsPanels == null)
                this.ThreadsPanels = new List<pnlThread>();
            this.pnlThreadMain = new pnlThread(new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(StartProcesses)))
            {
                TempoDiStampa = (int)this.numStampa.Value, PausaTransazioni = (int)this.numPausa.Value,
                Running = true
            };
            this.ThreadsPanels.Add(this.pnlThreadMain);

            pnlThread panel;
            for (int client = 1; client <= (int)this.numClients.Value; client++)
            {
                panel = new pnlThread(new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(StartOne)), this.pnlThreadMain) { ClientId = client, Running = true };
                this.ThreadsPanels.Add(panel);
            }

            this.ThreadsPanels.ForEach(th => this.AddPanel(th));
            this.AdjustSizePanels();
            this.pnlThreadMain.Thread.Start(this.pnlThreadMain);
        }

        private void StartProcesses(object parameter)
        {
            Exception error = null;
            IConnection conn = null;
            this.pnlThreadMain.Message("AVVIO.");
            try
            {
                conn = new Wintic.Data.oracle.CConnectionOracle();
                conn.ConnectionString = this.txtConnessione.Text;
                conn.Open();
                CALENDARIO.ParametriServerFiscale = clsClientServerFiscale.GetParametriServerFiscale(conn, out error);
                CALENDARIO.PercBiglietti = (int)this.percBiglietti.Value;
                CALENDARIO.CalendariInProgrammazione = new List<CALENDARIO>();
                CALENDARIO.sale = new Dictionary<int, List<POSTO>>();
                CALENDARIO.tariffe = new Dictionary<int, List<BIGLIETTO>>();
                CALENDARIO.CASSE = null;
                CALENDARIO.OPERATORI = null;
                CALENDARIO.CounterBiglietti = 0;
                CALENDARIO.CounterAbbonamenti = 0;
                CALENDARIO.SIGILLI = new List<SMART_VENDITE>();
                CALENDARIO.InitCalendariInProgrammazione(conn, this.dtFineDataOra.Value);

                if (this.numMaxTrans.Value == 0)
                {
                    CALENDARIO.MaxBiglietti = 0;
                    CALENDARIO.CalendariInProgrammazione.ForEach(c =>
                    {
                        CALENDARIO.MaxBiglietti += c.SALA.POSTI.Count;
                    });
                    CALENDARIO.MaxTransazioni = (!CALENDARIO.AbbonamentiAbilitati ? CALENDARIO.MaxBiglietti : (int)System.Math.Truncate((decimal)CALENDARIO.MaxBiglietti * 100 / CALENDARIO.PercBiglietti));
                }
                else
                {
                    CALENDARIO.MaxTransazioni = (int)this.numMaxTrans.Value;
                    CALENDARIO.MaxBiglietti = (CALENDARIO.PercBiglietti == 100 ? CALENDARIO.MaxTransazioni : (int)System.Math.Truncate((decimal)CALENDARIO.MaxTransazioni * CALENDARIO.PercBiglietti / 100));
                }
                CALENDARIO.MaxAbbonamenti = CALENDARIO.MaxTransazioni - CALENDARIO.MaxBiglietti;
                this.SetMaxTransazioni(CALENDARIO.MaxTransazioni);

                this.ThreadsPanels.Where(th => !this.pnlThreadMain.Equals(th)).ToList().ForEach(th =>
                {
                    string descCassa = "";
                    string descOperatore = "";
                    th.IDCASSA = CALENDARIO.GetIdCassa(conn, out descCassa);
                    th.NING_EMESSI = CALENDARIO.GetIdOperatore(conn, out descOperatore);
                    th.DESC_CASSA = descCassa;
                    th.DESC_OPERATORE = descOperatore;
                });

                this.ThreadsPanels.Where(th => !this.pnlThreadMain.Equals(th)).ToList().ForEach(th => th.Thread.Start(th));
                this.pnlThreadMain.CanStart = true;
                while (!this.RequestStop && this.ThreadsPanels != null && this.ThreadsPanels.Count > 0 && this.ThreadsPanels.Where(th => !this.pnlThreadMain.Equals(th)).ToList().FirstOrDefault(p => p.Thread != null && p.Thread.IsAlive) != null)
                {
                    System.Threading.Thread.Sleep(1000);
                }

                try
                {
                    this.pnlThreadMain.Message("OPERAZIONI TERMINATE, seguono controlli di annullo...attesa 20 secondi");
                    System.Threading.Thread.Sleep(20000);
                    while (CALENDARIO.SIGILLI != null && CALENDARIO.SIGILLI.Count > 0)
                    {
                        bool foundAnnullo = false;
                        CALENDARIO.SIGILLI.Where(s => !string.IsNullOrEmpty(s.SIGILLO)).ToList().ForEach(s =>
                        {
                            StringBuilder oSB = new StringBuilder();
                            oSB.Append("SELECT SV.DATA_EMI AS DATA_EMISSIONE, SA.SIGILLO AS SIGILLO_ANNULLO, SA.DATA_EMI AS DATA_ANNULLO");
                            oSB.Append(" FROM");
                            oSB.Append(" CINEMA.SMART_VENDITE SV");
                            oSB.Append(" INNER JOIN (SELECT IDVEND FROM CINEMA.SMART_VENDITE WHERE SIGILLO = :pSIGILLO) SG ON SG.IDVEND = SV.IDVEND");
                            oSB.Append(" INNER JOIN CINEMA.SMART_VENDITE SA ON SA.IDVEND = SV.IDVEND AND SA.TIPO = 'A'");
                            oSB.Append(" WHERE SV.TIPO = 'V'");
                            clsParameters pars = new clsParameters();
                            pars.Add(":pSIGILLO", s.SIGILLO);
                            DataTable table = conn.ExecuteQuery(oSB.ToString(), pars);
                            if (table.Rows.Count > 0)
                            {
                                foundAnnullo = true;
                                DateTime dataEmissione = (DateTime)table.Rows[0]["DATA_EMISSIONE"];
                                string sigilloAnnullo = (table.Rows[0]["SIGILLO_ANNULLO"] == null || table.Rows[0]["SIGILLO_ANNULLO"] == System.DBNull.Value ? "" : table.Rows[0]["SIGILLO_ANNULLO"].ToString());
                                DateTime dataAnnullo = (sigilloAnnullo == "" ? DateTime.MinValue : (DateTime)table.Rows[0]["DATA_ANNULLO"]);
                                
                                this.pnlThreadMain.Message(string.Format(" {0} idvend: {1} sigillo: {2} data_emi: {3}{4}{5}", 
                                                                          sigilloAnnullo == "" ? "OK" : "ANNULLATO",
                                                                          s.IDVEND,
                                                                          s.SIGILLO,
                                                                          dataEmissione.ToShortDateString() + " " + dataEmissione.ToLongTimeString(),
                                                                          sigilloAnnullo == "" ? "" : ", sigillo annullo: " + sigilloAnnullo,
                                                                          sigilloAnnullo == "" ? "" : ", data annullo: " + dataAnnullo.ToShortDateString() + " " + dataAnnullo.ToLongTimeString()));;
                            }
                            table.Dispose();
                        });

                        if (foundAnnullo)
                            this.pnlThreadMain.Message("TROVATI ANNULLATI");
                        else
                            this.pnlThreadMain.Message("nessun annulato");

                        if (CALENDARIO.SIGILLI.Where(s => !s.VENDITE_TRACING_INSERTED).ToList().Count > 0 &&
                            MessageBox.Show("Inserire in vendite tracing ?", "Ultima Operazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            conn.BeginTransaction();

                            CALENDARIO.SIGILLI.Where(s => !s.VENDITE_TRACING_INSERTED).ToList().ForEach(s =>
                            {
                                //Public Enum VenditeTracingOperation
                                //    PrenotazioneBiglietti = 1
                                //    AcquistoBiglietti = 2
                                //    PrenotazioneRatei = 3
                                //    FiscalizzazioneRateiAbb = 4
                                //    AcquistoAbbonamenti = 5
                                //    AcquistoBigliettiPrenotati = 6
                                //    FiscalizzazioneRateiPrenotati = 7
                                //End Enum
                                //
                                //Public Enum PaymentMode
                                //    PagoContanti = 0
                                //    PagoCartaCredito = 1
                                //    PagoRateo = 2
                                //    PagoBancomat = 3
                                //    PagoPayPal = 4
                                //    PagoBorsellino = 5
                                //    PagoCoupon = 6
                                //    PagoCartaCreditoPOS = 7
                                //    PagoGiftCard = 8
                                //    PagoApp18 = 10
                                //    PagoAppDocenti = 11
                                //    PagoBonifico = 12
                                //    PagoBorsellinoCertFatt = 16
                                //    
                                //    ' # GOLIS SATISPAY
                                //    PagoSatispay = 13

                                if (error == null)
                                {
                                    StringBuilder oSB = new StringBuilder();
                                    oSB.Append("INSERT INTO CINEMA.VENDITE_TRACING");
                                    oSB.Append(" (IDVEND, TRACKID, OPERATION, PAYMENT_DATE, PAYMENT_MODE, PRINT_DATE, PRINT_MODE, INSERT_DATE, PRINT_TRACKID)");
                                    oSB.Append(" SELECT :pIDVEND AS IDVEND,");
                                    oSB.Append(" :pTRACKID AS TRACKID,");
                                    oSB.Append(" CASE WHEN VENDITE.NOMEPRE LIKE 'TKH%' THEN 4 ELSE 2 END AS OPERATION,");
                                    oSB.Append(" SYSDATE AS PAYMENT_DATE, ");
                                    oSB.Append(" CASE WHEN VENDITE.NOMEPRE LIKE 'TKH%' THEN 2 ELSE 0 END AS PAYMENT_MODE,");
                                    oSB.Append(" SYSDATE AS PRINT_DATE,");
                                    oSB.Append(" :pPRINT_MODE AS PRINT_MODE,");
                                    oSB.Append(" SYSDATE AS INSERT_DATE,");
                                    oSB.Append(" :pPRINT_TRACKID AS PRINT_TRACKID");
                                    oSB.Append(" FROM CINEMA.VENDITE WHERE VENDITE.IDVEND = :pIDVEND");

                                    clsParameters pars = new clsParameters();
                                    pars.Add(":pIDVEND", s.IDVEND);
                                    pars.Add(":pTRACKID", 100);
                                    pars.Add(":pPRINT_MODE", 1);
                                    pars.Add(":pPRINT_TRACKID", 100);
                                    try
                                    {
                                        int inserted = (int)conn.ExecuteNonQuery(oSB.ToString(), pars);
                                        if (inserted == 1)
                                        {
                                            s.VENDITE_TRACING_INSERTED = true;
                                        }
                                        else
                                        {

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex;
                                    }
                                }
                            });
                            if (error == null)
                                conn.EndTransaction();
                            else
                                conn.RollBack();
                        }

                        if (MessageBox.Show("Terminare le operazioni ?", "Termine Operazioni", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            break;
                    }

                }
                catch (Exception)
                {
                }



                this.pnlThreadMain.Running = false;
            }
            catch (Exception ex)
            {
                this.pnlThreadMain.Message("Main error", ex);
            }
            finally
            {
                this.pnlThreadMain.Message("chiusura in corso...");
                while (this.ThreadsPanels != null && this.ThreadsPanels.Count > 0 && this.ThreadsPanels.Where(th => !this.pnlThreadMain.Equals(th)).ToList().FirstOrDefault(p => p.Thread != null && p.Thread.IsAlive) != null)
                {
                    if (!this.RequestStop)
                        this.RequestStop = true;
                    System.Threading.Thread.Sleep(100);
                }
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                this.InEsecuzione = false;
                this.SetAbilitazioni();
                this.pnlThreadMain.Message("FINE.");
            }
        }

        private delegate void delegateAddPanel(pnlThread panel);

        private void AddPanel(pnlThread panel)
        {
            if (this.InvokeRequired)
            {
                delegateAddPanel d = new delegateAddPanel(AddPanel);
                this.Invoke(d, panel);
            }
            else
            {
                panel.Dock = DockStyle.Left;
                this.pnlLogger.Controls.Add(panel);
                panel.BringToFront();
                Splitter splitter = new Splitter();
                splitter.Dock = DockStyle.Left;
                splitter.BorderStyle = this.splitterConfigExe.BorderStyle;
                splitter.BackColor = this.splitterConfigExe.BackColor;
                this.pnlLogger.Controls.Add(splitter);
                splitter.BringToFront();
            }
        }

        public void StartOne(object parameter)
        {
            pnlThread panel = null;
            IConnection Connection = new Wintic.Data.oracle.CConnectionOracle();
            clsClientServerFiscale clientServerFiscale = new clsClientServerFiscale();
            bool vendiUnaTessera = false;
            bool inTransaction = false;
            int countSigilliClient = 0;
            try
            {
                panel = (pnlThread)parameter;

                Connection.ConnectionString = this.txtConnessione.Text;
                Connection.Open();

                if (!panel.MainPanel.CanStart)
                {
                    panel.Message(string.Format("client {0} in attesa...", panel.ClientId));
                    while (!panel.MainPanel.CanStart)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                panel.Message(string.Format("client {0} avvio", panel.ClientId));

                panel.Message(string.Format("client {0} CASSA {1} {2}", panel.ClientId, panel.IDCASSA, panel.DESC_CASSA));
                panel.Message(string.Format("client {0} OPERATORE {1} {2}", panel.ClientId, panel.NING_EMESSI, panel.DESC_OPERATORE));

                Random rnd = new Random(6);


                // CHECK SERVER FISCALE
                Exception error = null;
                int countTentativi = 1;
                while (countTentativi <= 10)
                {
                    panel.Message(string.Format("client {0} check server fiscale", panel.ClientId));
                    if (!clientServerFiscale.CheckServerFiscale(CALENDARIO.ParametriServerFiscale))
                    {
                        error = new Exception("Server fiscale non attivo");
                        panel.Message(string.Format("client {0} server fiscale", panel.ClientId), error);
                    }
                    else
                    {
                        error = null;
                        panel.Message(string.Format("client {0} server fiscale OK", panel.ClientId));
                        break;
                    }
                    countTentativi += 1;
                }

                while (!this.RequestStop && CALENDARIO.Counter < CALENDARIO.MaxTransazioni)
                {
                    int qta = rnd.Next(1, 3);
                    if (qta > CALENDARIO.MaxTransazioni - CALENDARIO.Counter)
                        qta = CALENDARIO.MaxTransazioni - CALENDARIO.Counter;
                    List<VENDITA> vendite = null;
                    StringBuilder oSB = null;
                    clsParameters pars = null;
                    int idTransaction = 0;
                    TESSERA tessera = null;
                    List<SMART_VENDITE> sigilliTransazione = new List<SMART_VENDITE>();

                    try
                    {

                        // BIGLIETTI
                        if (!vendiUnaTessera && error == null)
                        {
                            // BLOCCO I POSTI
                            panel.Message(string.Format("client {0} Richiesta {1} POSTI", panel.ClientId, qta));

                            Connection.BeginTransaction();
                            inTransaction = true;

                            vendite = CALENDARIO.GetPostiNewTransaction(qta, Connection, out error);

                            if (error != null)
                            {
                                Connection.RollBack();
                                vendite = new List<VENDITA>();
                            }
                            else
                                Connection.EndTransaction();
                            inTransaction = false;

                            panel.Message(string.Format("client {0} trovati {1} POSTI di {2}", panel.ClientId, vendite.Count, qta));
                            if (vendite.Where(v => v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T")).ToList().Count > 0)
                                panel.Message(string.Format("client {0} di cui RATEI {1}", panel.ClientId, vendite.Where(v => v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T")).ToList().Count));

                            if (error == null)
                            {
                                if (vendite.Count > 0)
                                {
                                    CALENDARIO calendario = vendite.FirstOrDefault().CALENDARIO;
                                    panel.Message(string.Format("client {0} idcalend: {1}, sala: {2} {3}, tariffa: {4} {5}, data ora: {6}, titolo: {7}, te: {8}", panel.ClientId, calendario.IDCALEND, calendario.SALA.IDSALA, calendario.SALA.DESCR, calendario.TARIFFA.IDTARIFFA, calendario.TARIFFA.DESCR, calendario.DATA_ORA.ToShortDateString() + " " + calendario.DATA_ORA.ToShortTimeString(), calendario.TITOLO, calendario.TE));
                                }

                                tessera = null;
                                vendite.Where(v => v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T")).ToList().ForEach(v =>
                                {
                                    if (tessera == null)
                                        tessera = CALENDARIO.GetTessera(Connection, v.BIGLIETTO.TIPO_BIGLIETTO.TASTO, false, out error);

                                    while (tessera != null)
                                    {
                                        panel.Message(string.Format("client {0} BIGLIETTO TESSERA idtex: {1}, pin: {2}, dotazionef: {3}, rimanenza: {4}, ingressi presi:{5}", panel.ClientId, tessera.IDTEX, tessera.PIN, tessera.DOTAZIONEF, tessera.RIMANENZA - tessera.Qta, tessera.Qta));
                                        //if ((vendiUnaTessera && tessera.Qta < tessera.TOTING) || (!vendiUnaTessera && tessera.Qta < tessera.RIMANENZA))
                                        if (tessera.Qta < tessera.RIMANENZA)
                                        {
                                            tessera.Qta += 1;
                                            v.TESSERA = tessera;
                                            break;
                                        }
                                        else
                                            tessera = null;

                                        if (v.TESSERA == null)
                                        {
                                            tessera = CALENDARIO.GetTessera(Connection, v.BIGLIETTO.TIPO_BIGLIETTO.TASTO, vendiUnaTessera, out error);
                                        }
                                    }
                                });

                                tessera = null;
                                vendite.Where(v => v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T") && v.TESSERA != null).ToList().ForEach(v =>
                                {
                                    if (tessera == null || tessera.IDTEX != v.TESSERA.IDTEX)
                                    {
                                        tessera = v.TESSERA;
                                        panel.Message(string.Format("client {0} tessera idtex: {1}, pin: {2}, toting: {3}, dotazionef: {4}, scadenza: {5}, rimanenza: {6}, INGRESSI PRESI:{7}",
                                                                    panel.ClientId,
                                                                    tessera.IDTEX,
                                                                    tessera.PIN,
                                                                    tessera.TOTING,
                                                                    tessera.DOTAZIONEF,
                                                                    tessera.SCADENZA.ToShortDateString(),
                                                                    tessera.RIMANENZA.ToString(),
                                                                    tessera.Qta.ToString()));
                                    }
                                });
                            }

                            // EMISSIONE
                            if (error == null)
                            {
                                Connection.BeginTransaction();
                                inTransaction = true;

                                // INSERIMENTO IN VENDITE E TRANSACTION
                                vendite.Where(v => (v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T") && v.TESSERA != null && !string.IsNullOrEmpty(v.TESSERA.DOTAZIONEF))
                                               || v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("F")).ToList().ForEach(v =>
                                               {
                                                   panel.Message(string.Format("client {0} INSERIMENTO VENDITE idcalend: {1}, idposto: {2}, idbiglietto: {3}", panel.ClientId, v.IDCALEND, v.IDPOSTO, v.IDBIGLIETTO));
                                                   oSB = new StringBuilder();

                                                   if (v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T") && v.TESSERA != null && !string.IsNullOrEmpty(v.TESSERA.DOTAZIONEF))
                                                   {
                                                       v.NOMEPRE = string.Format("TKH{0}", v.TESSERA.IDTEX.ToString().Trim());
                                                   }

                                                   oSB.Append("INSERT INTO CINEMA.VENDITE");
                                                   oSB.Append(" (IDBIGLIETTO  , TIPO  , NUM_BIGL  , IDCASSA  , NOMEPRE  , VALUTA  , IDPOSTO  , IDCALEND  , ECCEDENZA  , DATA_EMI, PREVENDUTO  , DATA_AGG, IDACQUISTO  , NING_EMESSI)");
                                                   oSB.Append(" VALUES ");
                                                   oSB.Append(" (:pIDBIGLIETTO, :pTIPO, :pNUM_BIGL, :pIDCASSA, :pNOMEPRE, :pVALUTA, :pIDPOSTO, :pIDCALEND, :pECCEDENZA, SYSDATE , :pPREVENDUTO, SYSDATE , :pIDACQUISTO, :pNING_EMESSI)");
                                                   pars = new clsParameters();
                                                   pars.Add(":pIDBIGLIETTO", v.IDBIGLIETTO);
                                                   pars.Add(":pTIPO", v.TIPO);
                                                   pars.Add(":pNUM_BIGL", v.NUM_BIGL);
                                                   pars.Add(":pIDCASSA", panel.IDCASSA);
                                                   pars.Add(":pNOMEPRE", string.IsNullOrEmpty(v.NOMEPRE) ? "" : v.NOMEPRE);
                                                   pars.Add(":pVALUTA", v.VALUTA);
                                                   pars.Add(":pIDPOSTO", v.IDPOSTO);
                                                   pars.Add(":pIDCALEND", v.IDCALEND);
                                                   pars.Add(":pECCEDENZA", 0);
                                                   pars.Add(":pPREVENDUTO", (v.TIPO == "V" ? 0 : -1));
                                                   pars.Add(":pIDACQUISTO", panel.IDCASSA);
                                                   pars.Add(":pNING_EMESSI", panel.NING_EMESSI);
                                                   try
                                                   {
                                                       v.IDVEND = (int)Connection.ExecuteNonQuery(oSB.ToString(), "IDVEND", pars);
                                                   }
                                                   catch (Exception ex)
                                                   {
                                                       error = new Exception(string.Format("Errore inserimento VENDITE {0}\r\n{1}", ex.Message, pnlThread.GetErroreQuery(oSB.ToString(), pars)));
                                                       panel.Message(string.Format("client {0}", panel.ClientId), error);
                                                   }
                                                   
                                                   if (error == null && v.IDVEND > 0)
                                                   {
                                                       panel.Message(string.Format("client {0} INSERIMENTO VENDITE idvend: {1}, idcalend: {2}, idposto: {3}, idbiglietto: {4}", panel.ClientId, v.IDVEND, v.IDCALEND, v.IDPOSTO, v.IDBIGLIETTO));
                                                       if (idTransaction == 0)
                                                       {
                                                           DataTable tabTrans = Connection.ExecuteQuery("SELECT CINEMA.SEQ_TRANSACTION.NEXTVAL AS IDTRANSACTION FROM DUAL");
                                                           idTransaction = int.Parse(tabTrans.Rows[0]["IDTRANSACTION"].ToString());
                                                       }

                                                       panel.Message(string.Format("client {0} INSERIMENTO TRANSACTION idvend: {1}, idtransaction: {2}", panel.ClientId, v.IDVEND, idTransaction));
                                                       oSB = new StringBuilder();
                                                       oSB.Append("INSERT INTO CINEMA.TRANSACTION ");
                                                       oSB.Append(" (IDTRANSACTION  , IDVEND  , IDSESSIONE, DATA_TRANS, APP_CODE_CATEGORY  , APP_CODE  , CODICE_SUPPORTO_IDENTIFICATIVO, DESCR_SUPPORTO_IDENTIFICATIVO  , IDENTIFICATIVO_SUPPORTO  , IDENTIFICATIVO_SUPP_ALT , DIGITALE_TRADIZIONALE)");
                                                       oSB.Append(" VALUES");
                                                       oSB.Append(" (:pIDTRANSACTION, :pIDVEND, 0         , SYSDATE   , :pAPP_CODE_CATEGORY, :pAPP_CODE, :pCODICE_SUPPORTO, :pDESCR_SUPPORTO, :pIDENTIFICATIVO_SUPPORTO, :pIDENTIFICATIVO_SUPP_ALT, :pDIGITALE_TRADIZIONALE)");

                                                       pars = new clsParameters();
                                                       pars.Add(":pIDTRANSACTION", idTransaction);
                                                       pars.Add(":pIDVEND", v.IDVEND);
                                                       pars.Add(":pAPP_CODE_CATEGORY", "CL");
                                                       pars.Add(":pAPP_CODE", string.Format("CL{0}", panel.IDCASSA.ToString().Trim().PadLeft(6, '0')));
                                                       pars.Add(":pCODICE_SUPPORTO", "BT");
                                                       pars.Add(":pDESCR_SUPPORTO", "Biglietto Tradizionale");
                                                       pars.Add(":pIDENTIFICATIVO_SUPPORTO", v.IDVEND.ToString().Trim().PadLeft(32, '0'));
                                                       pars.Add(":pIDENTIFICATIVO_SUPP_ALT", idTransaction.ToString().Trim().PadLeft(32, '0'));
                                                       pars.Add(":pDIGITALE_TRADIZIONALE", "T");
                                                       try
                                                       {
                                                           int inserted = (int)Connection.ExecuteNonQuery(oSB.ToString(), pars);
                                                           if (inserted != 1)
                                                               throw new Exception("Nessuna riga inserita in TRANSACTION");
                                                       }
                                                       catch (Exception ex)
                                                       {
                                                           error = new Exception(string.Format("Errore inserimento TRANSACTION {0}\r\n{1}", ex.Message, pnlThread.GetErroreQuery(oSB.ToString(), pars)));
                                                           panel.Message(string.Format("client {0}", panel.ClientId), error);
                                                       }
                                                   }
                                               });

                                if (error != null)
                                    Connection.RollBack();
                                else
                                    Connection.EndTransaction();

                                inTransaction = false;

                                // SIGILLI
                                if (error == null)
                                {
                                    vendite.Where(v => v.IDVEND > 0).ToList().ForEach(v =>
                                    {
                                        if (error == null)
                                        {
                                            panel.Message(string.Format("client {0} Richiesta sigillo {1}", panel.ClientId, v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T") ? "RATEO" : "IVA DA ASSOLVERE"));
                                            if (v.BIGLIETTO.TIPO_BIGLIETTO.TASTO.StartsWith("T"))
                                            {
                                                if (clientServerFiscale.RichiediSigilloBigliettoIvaPreassolta(CALENDARIO.ParametriServerFiscale, v.IDVEND, v.TESSERA.DOTAZIONEF, v.TESSERA.IDTEX))
                                                {
                                                    DataTable tableSigillo = Connection.ExecuteQuery("SELECT SIGILLO FROM SMART_VENDITE WHERE IDVEND = :pIDVEND AND TIPO = 'V'", new clsParameters(":pIDVEND", v.IDVEND));
                                                    if (tableSigillo.Rows.Count > 0)
                                                        v.SIGILLO = tableSigillo.Rows[0]["SIGILLO"].ToString();
                                                    tableSigillo.Dispose();

                                                    if (!string.IsNullOrEmpty(v.SIGILLO))
                                                    {
                                                        SMART_VENDITE SMART_VENDITE = new SMART_VENDITE() { IDVEND = v.IDVEND, SIGILLO = v.SIGILLO };
                                                        CALENDARIO.SIGILLI.Add(SMART_VENDITE);
                                                        sigilliTransazione.Add(SMART_VENDITE);
                                                        panel.Message(string.Format("client {0} tessera idtex: {1}, pin: {2}, toting: {3}, dotazionef: {4}, scadenza: {5}, rimanenza: {6}, INGRESSI PRESI:{7}", 
                                                                                    panel.ClientId,
                                                                                    v.TESSERA.IDTEX,
                                                                                    v.TESSERA.PIN,
                                                                                    v.TESSERA.TOTING,
                                                                                    v.TESSERA.DOTAZIONEF,
                                                                                    v.TESSERA.SCADENZA.ToShortDateString(),
                                                                                    v.TESSERA.RIMANENZA.ToString(),
                                                                                    v.TESSERA.Qta.ToString()));
                                                        try
                                                        {
                                                            oSB = new StringBuilder();
                                                            oSB.Append("UPDATE TEX.TESSERE SET RIMANENZA = RIMANENZA - 1 WHERE IDTEX = :pIDTEX AND PIN = :pPIN AND DOTAZIONEF = :pDOTAZIONEF");
                                                            pars = new clsParameters();
                                                            pars.Add(":pIDTEX", v.TESSERA.IDTEX);
                                                            pars.Add(":pPIN", v.TESSERA.PIN);
                                                            pars.Add(":pDOTAZIONEF", v.TESSERA.DOTAZIONEF);
                                                            Connection.ExecuteNonQuery(oSB.ToString(), pars);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            error = new Exception(string.Format("Errore aggiornamento TESSERE {0}\r\n{1}", ex.Message, pnlThread.GetErroreQuery(oSB.ToString(), pars)));
                                                            panel.Message(string.Format("client {0}", panel.ClientId), error);
                                                        }
                                                    }
                                                    else
                                                        panel.Message(string.Format("{0} Errore sigillo non trovato  idvend: {1}", panel.ClientId, v.IDVEND));
                                                    System.Threading.Thread.Sleep((int)panel.MainPanel.TempoDiStampa);
                                                }
                                                else
                                                {
                                                    error = (clientServerFiscale.ExceptionClient != null ? clientServerFiscale.ExceptionClient : new Exception("Errore generico richiesta sigillo"));
                                                    panel.Message(string.Format("{0} Errore richiesta sigillo RATEO", panel.ClientId), error);
                                                }
                                            }
                                            else
                                            {
                                                if (clientServerFiscale.RichiediSigilloBigliettoIvaDaAssolvere(CALENDARIO.ParametriServerFiscale, v.IDVEND))
                                                {
                                                    DataTable tableSigillo = Connection.ExecuteQuery("SELECT SIGILLO FROM SMART_VENDITE WHERE IDVEND = :pIDVEND AND TIPO = 'V'", new clsParameters(":pIDVEND", v.IDVEND));
                                                    if (tableSigillo.Rows.Count > 0)
                                                    {
                                                        v.SIGILLO = tableSigillo.Rows[0]["SIGILLO"].ToString();
                                                    }
                                                    tableSigillo.Dispose();
                                                    if (!string.IsNullOrEmpty(v.SIGILLO))
                                                    {
                                                        SMART_VENDITE SMART_VENDITE = new SMART_VENDITE() { IDVEND = v.IDVEND, SIGILLO = v.SIGILLO };
                                                        CALENDARIO.SIGILLI.Add(SMART_VENDITE);
                                                        sigilliTransazione.Add(SMART_VENDITE);
                                                    }
                                                    else
                                                        panel.Message(string.Format("{0} Errore sigillo non trovato  idvend: {1}", panel.ClientId, v.IDVEND));
                                                    System.Threading.Thread.Sleep((int)panel.MainPanel.TempoDiStampa);
                                                }
                                                else
                                                {
                                                    error = (clientServerFiscale.ExceptionClient != null ? clientServerFiscale.ExceptionClient : new Exception("Errore generico richiesta sigillo"));
                                                    panel.Message(string.Format("client {0} Errore richiesta sigillo IVA DA ASSOLVERE", panel.ClientId), error);
                                                }
                                            }
                                            CALENDARIO.CounterBiglietti += 1;
                                        }
                                    });
                                }
                            }

                            if (vendite == null || vendite.Count == 0)
                                break;
                        }
                        else if (vendiUnaTessera && error == null)
                        { 
                            // ABBONAMENTO
                        }
                    }
                    catch (Exception ex)
                    {
                        if (inTransaction)
                        {
                            if (sigilliTransazione.Count > 0)
                            {
                                Connection.EndTransaction();
                            }
                            else
                                Connection.RollBack();
                            inTransaction = false;
                        }
                        panel.Message(string.Format("client {0} errore...", panel.ClientId), ex);
                    }
                    finally
                    {
                        if (vendite != null && vendite.Count > 0 && vendite.Where(v => v.IDVEND == 0 || string.IsNullOrEmpty(v.SIGILLO)).ToList().Count > 0)
                        {
                            panel.Message(string.Format("client {0} libero {1} posti invenduti/non sigillati...", panel.ClientId, vendite.Where(v => v.IDVEND == 0 || string.IsNullOrEmpty(v.SIGILLO)).ToList().Count));
                            vendite.Where(v => v.IDVEND == 0 || string.IsNullOrEmpty(v.SIGILLO)).ToList().ForEach(v =>
                            {
                                if (v.IDVEND > 0)
                                    CALENDARIO.EliminaVendita(Connection, v.IDVEND);
                                CALENDARIO.LiberaPosti(Connection, v.IDCALEND, v.IDPOSTO);
                            });
                        }
                    }
                    countSigilliClient += (sigilliTransazione != null ? sigilliTransazione.Count() : 0);

                    System.Threading.Thread.Sleep((int)panel.MainPanel.PausaTransazioni);

                    if (error != null)
                        break;
                }
                panel.Message(string.Format("client {0} FINE {1} sigilli", panel.ClientId, countSigilliClient));
            }
            catch (Exception ex)
            {
                if (panel != null) panel.Message(string.Format("client {0} ERRORE... sigilli {1}", panel.ClientId, countSigilliClient), ex);
            }
            finally
            {
                if (Connection != null)
                {
                    Connection.Close();
                    Connection.Dispose();
                }
            }
            panel.Running = false;
        }

        
    }
}
