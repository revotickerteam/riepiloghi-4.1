﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestServizi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private delegate void delegateAddText(string message);

        private void AddText(string message)
        {
            if (this.InvokeRequired)
            {
                delegateAddText d = new delegateAddText(this.AddText);
                this.Invoke(d, message);
            }
            else
            {
                this.textBox1.Text += "\r\n" + message;
                this.textBox1.SelectionLength = 0;
                this.textBox1.SelectionStart = this.textBox1.Text.Length;
                this.textBox1.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void Send(string parUrl, object parRequest)
        {
            string result = string.Empty;

            HttpWebResponse resp = null;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(parUrl);
            req.CookieContainer = new CookieContainer();
            req.Credentials = CredentialCache.DefaultNetworkCredentials;
            req.UserAgent = ": Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 4.0.20506)";
            req.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Anonymous;
            req.Method = "POST";
            req.ContentType = "text/json";
            //req.Timeout = 5000;

            string json = JsonConvert.SerializeObject(parRequest);

            using (StreamWriter streamWriter = new StreamWriter(req.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();

                resp = (HttpWebResponse)req.GetResponse();
                //Encoding enc = Encoding.GetEncoding(1252);
                Encoding enc = Encoding.UTF8;
                using (StreamReader responseStream = new StreamReader(resp.GetResponseStream(), enc))
                {
                    result = responseStream.ReadToEnd();
                    this.AddText(result);
                    responseStream.Close();
                }
                resp.Close();
            }
        }

    }
}
