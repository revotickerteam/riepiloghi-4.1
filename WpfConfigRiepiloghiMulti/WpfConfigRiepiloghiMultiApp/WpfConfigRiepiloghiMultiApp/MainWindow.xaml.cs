﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfConfigRiepiloghiMultiLib.Functions;
using WpfConfigRiepiloghiMultiLib.Models;
using WpfConfigRiepiloghiMultiLib.Views;

namespace WpfConfigRiepiloghiMultiApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string token { get; set; }
        private clsAccount account { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            this.Opacity = .01;
            this.Width = 10;
            this.Height = 10;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.None;
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.Execution();
        }

        private void Execution()
        {
            Recover.BaseUrl = System.Configuration.ConfigurationManager.AppSettings.Get("urlRiepiloghi");
            if (ExecuteLogin())
            {
                List<string> operations = new List<string>() { "RIEPILOGHI_PROFILES", "RIEPILOGHI_ACCOUNTS", "CONFIG_RIEPILOGHI", "CONFIG_SISTEMI" };
                int countOperations = 0;
                account.Operazioni.OrderBy(categoria => categoria.Ordine).ToList().ForEach(categoria =>
                {
                    if (categoria.ListaOperazioni != null)
                    {
                        categoria.ListaOperazioni.Where(operazione => operations.Contains(operazione.IdOperazione)).OrderBy(operazione => operazione.Ordine).ToList().ForEach(operazione =>
                        {
                            countOperations += 1;
                        });
                    }
                });

                if (countOperations > 0)
                {
                    ManagerWindow manager = new ManagerWindow(this.token, this.account);
                    manager.ShowDialog();
                    manager.Close();
                }
                else
                    ViewsWindows.MsgBox("Nessuna operazione di configurazione abilitata per questo account.");
            }

            this.Close();
        }
        private bool ExecuteLogin()
        {
            bool resultLogin = false;
            ResponseRiepiloghi baseResponse = null;
            WindowLogin.defActionLogin action = WindowLogin.defActionLogin.Login;
            string login = "";
            string password = "";
            string token = "";
            clsDebugLogin loginDebug = null;
            if (Debugger.IsAttached && System.IO.File.Exists(System.Reflection.Assembly.GetExecutingAssembly().Location + ".login.json"))
            {
                loginDebug = Newtonsoft.Json.JsonConvert.DeserializeObject<clsDebugLogin>(System.IO.File.ReadAllText(System.Reflection.Assembly.GetExecutingAssembly().Location + ".login.json"));
                this.token = loginDebug.token;
                this.account = loginDebug.account;
                resultLogin = true;
                return resultLogin;
            }

            bool? result = true;

            while (result != null && result.Value)
            {
                WindowLogin Wlogin = new WindowLogin(action);
                result = Wlogin.ShowDialog();
                if (result != null && result.Value)
                    action = Wlogin.Action;
                else
                    break;
                login = Wlogin.Login;
                
                Wlogin.Close();
                Wlogin = null;

                if (action == WindowLogin.defActionLogin.Login ||
                    action == WindowLogin.defActionLogin.PasswordRenew)
                {
                    if (action == WindowLogin.defActionLogin.Login)
                    {
                        password = Wlogin.Password;
                        baseResponse = Recover.Login(login, password);
                    }
                    else if (action == WindowLogin.defActionLogin.PasswordRenew)
                    {
                        token = Wlogin.TokenRenew;
                        // login, password nuova, token
                        baseResponse = Recover.Login(login, password);
                    }
                    
                    if (baseResponse.Status.StatusOK)
                    {
                        result = null;
                        token = baseResponse.Data.Token;
                        baseResponse = Recover.GetAccount(token);
                        if (baseResponse.Status.StatusOK)
                        {
                            account = baseResponse.Data.Account;
                            if (Debugger.IsAttached)
                            {
                                loginDebug = new clsDebugLogin() { token = this.token, account = this.account };
                                System.IO.File.WriteAllText(System.Reflection.Assembly.GetExecutingAssembly().Location + ".login.json", Newtonsoft.Json.JsonConvert.SerializeObject(loginDebug, Newtonsoft.Json.Formatting.Indented));
                            }
                            resultLogin = true;
                        }
                    }
                }
                else if (action == WindowLogin.defActionLogin.PasswordByTokenRenew)
                {
                    ViewsWindows.MsgBox("Verificare sulla propria email");
                    action = WindowLogin.defActionLogin.PasswordRenew;
                }
                else if (action == WindowLogin.defActionLogin.PasswordByToken)
                {
                    action = WindowLogin.defActionLogin.PasswordRenew;
                }
                else
                    result = null;
            }
            return resultLogin;
        }

        private class clsDebugLogin
        {
            public string token { get; set; }
            public clsAccount account { get; set; }
        }

    }
}
