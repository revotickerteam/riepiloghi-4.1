﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfConfigRiepiloghiMultiLib.Models
{

    public class clsCodiceLocale
    {
        public string CodiceSistema { get; set; }
        public string CodiceLocale { get; set; }
        public string Descrizione { get; set; }
        public bool Enabled { get; set; }

        public clsCodiceLocale Clone()
        {
            clsCodiceLocale result = new clsCodiceLocale() { CodiceSistema = this.CodiceSistema, CodiceLocale = this.CodiceLocale, Descrizione = this.Descrizione, Enabled = this.Enabled };
            return result;
        }
    }

    public class clsCFOrganizzatore
    {
        public string CodiceSistema { get; set; }
        public string CFOrganizzatore { get; set; }
        public string Descrizione { get; set; }
        public bool Enabled { get; set; }

        public clsCFOrganizzatore()
        {

        }


        public clsCFOrganizzatore Clone()
        {
            clsCFOrganizzatore result = new clsCFOrganizzatore() { CodiceSistema = this.CodiceSistema, CFOrganizzatore = this.CFOrganizzatore, Descrizione = this.Descrizione, Enabled = this.Enabled };
            return result;
        }
    }

    //public class clsProfileFilter
    //{
    //    public string CodiceSistema { get; set; }
    //    public string Campo { get; set; }
    //    public string Tipo { get; set; }
    //    public string DescCampo { get; set; }
    //    public string TipoRiga { get; set; }
    //    public string DescrizioneTipoRiga { get; set; }
    //    public string DescrizioneFiltro { get; set; }
    //    public List<object> Valori { get; set; }

    //    public clsProfileFilter()
    //    {
    //    }


    //    public clsProfileFilter Clone()
    //    {
    //        clsProfileFilter result = new clsProfileFilter();
    //        result.CodiceSistema = this.CodiceSistema;
    //        result.Campo = this.Campo;
    //        result.Tipo = this.Tipo;
    //        result.DescCampo = this.DescCampo;
    //        result.TipoRiga = this.TipoRiga;
    //        result.DescrizioneTipoRiga = this.DescrizioneTipoRiga;
    //        result.DescrizioneFiltro = this.DescrizioneFiltro;
    //        result.Valori = new List<object>();
    //        foreach (object item in this.Valori)
    //        {
    //            result.Valori.Add(item);
    //        }
    //        return result;
    //    }
    //}

    public delegate void delegateModificato(object o, EventArgs e);
    

    public class clsCodiceSistema
    {
        public string CodiceSistema { get; set; }
        public string Descrizione { get; set; }
        public bool Enabled { get; set; }
        public string ConnectionString { get; set; }
        public List<clsCodiceLocale> CodiciLocale { get; set; }
        public List<clsCFOrganizzatore> CFOrganizzatori { get; set; }
        public bool AllCodiciLocali { get; set; }
        public bool AllCFOrganizzatori { get; set; }

        private bool modificato { get; set; }
        public bool Modificato { get => this.modificato; set { this.modificato = value; if (value && EventModificato != null) { EventModificato(this, new EventArgs()); } } }
        public event delegateModificato EventModificato;

        public clsCodiceSistema()
        { }

        public clsCodiceSistema Clone()
        {
            clsCodiceSistema result = new clsCodiceSistema()
            {
                CodiceSistema = this.CodiceSistema,
                Descrizione = this.Descrizione,
                Enabled = this.Enabled,
                CodiciLocale = (this.CodiciLocale != null ? new List<clsCodiceLocale>(this.CodiciLocale.ToArray()) : null),
                CFOrganizzatori = (this.CFOrganizzatori != null ? new List<clsCFOrganizzatore>(this.CFOrganizzatori.ToArray()) : null),
                AllCodiciLocali = this.AllCodiciLocali,
                AllCFOrganizzatori = this.AllCFOrganizzatori
            };
            return result;
        }

        public string DescrizioneCodiciLocali => this.CodiciLocale == null ? "Nessuno" : string.Format("{0} di {1}", this.CodiciLocale.Where(x => x.Enabled).Count(), this.CodiciLocale.Count());
        public string DescrizioneOrganizzatori => this.CFOrganizzatori == null ? "Nessuno" : string.Format("{0} di {1}", this.CFOrganizzatori.Where(x => x.Enabled).Count(), this.CFOrganizzatori.Count());
    }

    public class clsOperazioneRiepiloghi
    {
        public Int64 Ordine { get; set; }
        public string IdOperazione { get; set; }
        public string Descrizione { get; set; }
        public Int64 IdCategoria { get; set; }
        public Int64 OrdineCategoria { get; set; }
        public string DescrizioneCategoria { get; set; }
        public bool Enabled { get; set; }


        private bool modificato { get; set; }
        public bool Modificato { get => this.modificato; set { this.modificato = value; if (value && EventModificato != null) { EventModificato(this, new EventArgs()); } } }
        public event delegateModificato EventModificato;


        public clsOperazioneRiepiloghi()
        {
        }
    }
    public class clsCategoriaOperazioniRiepiloghi
    {
        public Int64 IdCategoria { get; set; }
        public Int64 Ordine { get; set; }
        public string Descrizione { get; set; }
        public List<clsOperazioneRiepiloghi> ListaOperazioni { get; set; }

        public clsCategoriaOperazioniRiepiloghi()
        {
        }
    }

    public class clsProfile
    {
        public Int64 ProfileId { get; set; }
        public Int64 ParentProfileId { get; set; }
        public string Descrizione { get; set; }
        public bool Enabled { get; set; }
        public bool AllCodiciSistema { get; set; }
        public List<clsCodiceSistema> CodiciSistema { get; set; }
        public List<clsCategoriaOperazioniRiepiloghi> OperazioniProfile { get; set; }
        private bool modificato { get; set; }
        public bool Modificato { get => this.modificato; set { this.modificato = value; if (value && EventModificato != null) { EventModificato(this, new EventArgs()); } } }
        public event delegateModificato EventModificato;

        public int CountOperazioni
        {
            get
            {
                int result = 0;
                if (this.OperazioniProfile != null)
                {
                    this.OperazioniProfile.ForEach(categoria =>
                    {
                        if (categoria.ListaOperazioni != null)
                            result += categoria.ListaOperazioni.Count();
                    });
                }
                return result;
            }
        }

        public int CountOperazioniAbilitate
        {
            get
            {
                int result = 0;
                if (this.OperazioniProfile != null)
                {
                    this.OperazioniProfile.ForEach(categoria =>
                    {
                        if (categoria.ListaOperazioni != null)
                            result += categoria.ListaOperazioni.Where(operazione => operazione.Enabled).Count();
                    });
                }
                return result;
            }
        }


        public int CountSistemi => this.CodiciSistema != null ? this.CodiciSistema.Count : 0;
        public int CountSistemiAbilitati => this.CodiciSistema != null ? this.CodiciSistema.Where(cs => cs.Enabled).ToList().Count : 0;

        public string DescrizioneSistemiProfilo => this.AllCodiciSistema ? "Tutti " : 
                                                   (this.CountSistemi == 1 && this.CountSistemiAbilitati == 1 ? string.Format("Un C.S. {0} di {1}", this.CodiciSistema[0].CodiceSistema, this.CodiciSistema[0].Descrizione) :
                                                    (this.CountSistemi == 1 && this.CountSistemiAbilitati == 0 ? string.Format("Un C.S. non abil. {0} di {1}", this.CodiciSistema[0].CodiceSistema, this.CodiciSistema[0].Descrizione) : 
                                                     string.Format("{0} di {1}", this.CountSistemiAbilitati, this.CountSistemi)));

        public string DescrizioneOperazioniProfilo => string.Format("{0} di {1}", this.CountOperazioniAbilitate, this.CountOperazioni);

        
    }
    public class clsAccount
    {
        public string AccountId { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Email { get; set; }

        public string Descrizione => string.Format("{0} {1} {2}", this.Email, this.Cognome, this.Nome);

        public bool Enabled { get; set; }
        public List<clsCodiceSistema> CodiciSistema { get; set; }
        public List<clsCategoriaOperazioniRiepiloghi> Operazioni { get; set; }
        public long ProfileId { get; set; }

        private bool modificato { get; set; }
        public bool Modificato { get => this.modificato; set { this.modificato = value; if (value && EventModificato != null) { EventModificato(this, new EventArgs()); } } }
        public event delegateModificato EventModificato;
        public clsAccount()
        { }

        public List<clsProfile> VisibleProfiles { get; set; }
    }

    public class clsToken
    {
        public Int64 TokenId { get; set; }
        public clsCodiceSistema CodiceSistema { get; set; }
        public string AccountId { get; set; }

        public clsToken()
        {
        }
    }

    public class PayLoadRiepiloghi
    {
        public string Mode { get; set; }
        public string Token { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Tornello_Barcode { get; set; }
        public string AccountId { get; set; }

        public string CodiceSistema { get; set; }

        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }

        public string Carta { get; set; }
        public string Sigillo { get; set; }
        public Int64 Progressivo { get; set; }

        public Int64 PrimoProgressivo { get; set; }
        public Int64 UltimoProgressivo { get; set; }

        public string CausaleAnnullamento { get; set; }

        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }

        public DateTime DataRiepilogo { get; set; }
        public string TipoRiepilogo { get; set; }
        public long ProgressivoRiepilogo { get; set; }

        public string CambioStatoLta { get; set; }

        public string Proprieta { get; set; }

        public PlayLoadFiltri Filtri { get; set; }

        public clsAccount Account { get; set; }
        public clsProfile Profile { get; set; }
    }

    public class PlayLoadFiltri
    {
        public string DataEmissioneAnnullamento { get; set; }
        public string DataEvento { get; set; }
        public string OraEvento { get; set; }
        public string CodiceLocale { get; set; }
        public string TitoloEvento { get; set; }
    }


    public class ResponseRiepiloghi
    {
        public ResponseDataRiepiloghi Data { get; set; }
        public ResultBase Status { get; set; }
    }

    public class ResponseDataRiepiloghi
    {
        public string Token { get; set; }

        // GetAccount
        public clsAccount Account { get; set; }

        // GetProfileList
        public List<clsProfile> Profiles { get; set; }
        public List<clsAccount> Accounts { get; set; }

        // GetCodiceSistema
        public clsCodiceSistema CodiceSistema { get; set; }

        // GetListaCampiLogTransazioniCS
        public clsResultBaseCampoLogTransazioni[] Campi { get; set; }
        public clsResultBaseCampoLogTransazioni[] CampiSmart { get; set; }
        
        public clsResultBaseCampoLta[] CampiLta { get; set; }
        public clsResultBaseCampoLta[] CampiLtaSmart { get; set; }

        public clsCodiceLocale[] CodiciLocali { get; set; }

        // GetTransazioni, GetTransazioniAnnullati
        public DateTime DataEmiInizio { get; set; }
        public DateTime DataEmiFine { get; set; }
        
        public string Carta { get; set; }
        public string Sigillo { get; set; }
        public Int64 Progressivo { get; set; }


        public string CartaAnnullante { get; set; }
        public string SigilloAnnullante { get; set; }
        public Int64 ProgressivoAnnullante { get; set; }
        

        public List<string> WarningsPostAnnullo { get; set; }

        
        // RIEPILOGHI DA GENERARE

        // RIEPILOGHI SU CUI RICEVERE RISPOSTA o DA INVIARE
        public bool EmailToSendOrReceive { get; set; }


                public string Proprieta { get; set; }
        public string Valore { get; set; }

    }

    public class ResultBase
    {
        public bool StatusOK = true;

        public string StatusMessage = "";

        public Int64 StatusCode = 0;

        public string execution_time = "";

        public DateTime Sysdate { get; set; }

        public ResultBase()
        {
        }

        public ResultBase(bool lStatusOK, string cStatusMessage, Int64 nStatusCode, string cExecutionTime)
        {
            this.StatusOK = lStatusOK;
            this.StatusMessage = cStatusMessage;
            this.StatusCode = nStatusCode;
            this.execution_time = cExecutionTime;
        }
    }


    public class clsResultBaseCampoLogTransazioni
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLogTransazioni()
        {
        }

        public clsResultBaseCampoLogTransazioni(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

    public class clsResultBaseCampoLta
    {
        public string Campo = "";
        public string Tipo = "";
        public int Inizio = 0;
        public int Dimensione = 0;
        public string DescCampo = "";
        public string Descrizione = "";
        public string NomeFiscale = "";

        public clsResultBaseCampoLta()
        {
        }

        public clsResultBaseCampoLta(string cCampo, string cTipo, int nInizio, int nDimensione, string cDescCampo, string cDescrizione, string cNomeFiscale)
        {
            this.Campo = cCampo;
            this.Tipo = cTipo;
            this.Inizio = nInizio;
            this.Dimensione = nDimensione;
            this.DescCampo = cDescCampo;
            this.Descrizione = cDescrizione;
            this.NomeFiscale = cNomeFiscale;
        }
    }

    
}
