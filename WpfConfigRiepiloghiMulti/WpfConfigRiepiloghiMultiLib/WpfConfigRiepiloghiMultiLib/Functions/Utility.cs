﻿using Newtonsoft.Json;
using RestClient.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfConfigRiepiloghiMultiLib.Models;
using WpfConfigRiepiloghiMultiLib.Views;

namespace WpfConfigRiepiloghiMultiLib.Functions
{
    public class NewtonsoftSerializationAdapter : RestClient.Net.ISerializationAdapter
    {
        #region Implementation
        public TResponseBody? Deserialize<TResponseBody>(byte[] responseData, RestClient.Net.IHeadersCollection? responseHeaders)
        {
            //Note: on some services the headers should be checked for encoding 
            var markup = Encoding.UTF8.GetString(responseData);

            object markupAsObject = markup;

            return typeof(TResponseBody) == typeof(string) ? (TResponseBody)markupAsObject : JsonConvert.DeserializeObject<TResponseBody>(markup);
        }

        public byte[] Serialize<TRequestBody>(TRequestBody value, RestClient.Net.IHeadersCollection requestHeaders)
        {
            var json = JsonConvert.SerializeObject(value);

            var binary = Encoding.UTF8.GetBytes(json);

            return binary;
        }
        #endregion
    }

    public static class Recover
    {

        public static string BaseUrl { get; set; }
        private static ResponseRiepiloghi getResponse(string url, PayLoadRiepiloghi payload, string description)
        {
            WindowsWait.SetWaitMessage(description + "...");
            ResponseRiepiloghi response = new ResponseRiepiloghi();
            response.Status = new ResultBase();
            try
            {
                using (RestClient.Net.Client client = new RestClient.Net.Client(BaseUrl + url, new NewtonsoftSerializationAdapter(), HeadersExtensions.FromJsonContentType()))
                {
                    using (var postAsync = client.PostAsync<ResponseRiepiloghi, PayLoadRiepiloghi>(payload))
                    {
                        postAsync.Wait();
                        response.Status.StatusOK = postAsync.Result.IsSuccess;
                        if (postAsync.Result.IsSuccess)
                        {
                            response = postAsync.Result;
                        }
                    };
                }

            }
            catch (Exception ex)
            {
                response.Status.StatusOK = false;
                response.Status.StatusMessage = ex.Message;
            }
            WindowsWait.SetWaitMessage("");
            if (!response.Status.StatusOK)
            {
                ViewsWindows.MsgBox(response.Status.StatusMessage, "Errore", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
            return response;
        }

        public static ResponseRiepiloghi Login(string login, string password)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Login = login, Password = password };
            ResponseRiepiloghi response = getResponse("Auth/Login", payload, "Login");
            return response;
        }

        public static ResponseRiepiloghi GetAccount(string token)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Token = token };
            ResponseRiepiloghi response = getResponse("Auth/GetAccount", payload, "Recupero Account");
            return response;
        }

        // GetProfileList
        public static ResponseRiepiloghi GetProfileList(string token)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Token = token };
            ResponseRiepiloghi response = null;
            try
            {
                response = getResponse("Auth/GetProfileList", payload, "Recupero Lista Profili");
            }
            catch (Exception)
            {
            }
            return response;
        }

        //GetAccountList
        public static ResponseRiepiloghi GetAccountList(string token)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Token = token };
            ResponseRiepiloghi response = null;
            try
            {
                response = getResponse("Auth/GetAccountList", payload, "Recupero Lista Accounts");
            }
            catch (Exception)
            {
            }
            return response;
        }

        // CreateProfile
        // GetProfile
        public static ResponseRiepiloghi SetProfile(string token, clsProfile profile)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Token = token, Profile = profile };
            ResponseRiepiloghi response = null;
            try
            {
                response = getResponse("Auth/SetProfile", payload, "Salvataggio profilo");
            }
            catch (Exception)
            {
            }
            return response;
        }

        public static ResponseRiepiloghi CreateAccount(string token, clsAccount account)
        {
            PayLoadRiepiloghi payload = new PayLoadRiepiloghi() { Token = token, Account = account };
            ResponseRiepiloghi response = null;
            try
            {
                response = getResponse("Auth/CreateAccount", payload, "Salvataggio account");
            }
            catch (Exception)
            {
            }
            return response;
        }
    }
}
