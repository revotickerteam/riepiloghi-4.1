﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using static WpfConfigRiepiloghiMultiLib.Views.ManagerWindow;

namespace WpfConfigRiepiloghiMultiLib.Views
{
    public class WindowLogin : WindowBase
    {
        public enum defActionLogin : int
        {
            None = 0,
            Login = 1,
            PasswordByToken = 2,
            PasswordByTokenRenew = 3,
            PasswordRenew = 4
        }

        public defActionLogin Action = defActionLogin.None;
        public string Login { get; set; }
        public string Password { get; set; }
        public string TokenRenew { get; set; }
        public WindowLogin(defActionLogin action)
        {
            this.Action = action;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            Grid grid = new Grid();
            grid.Margin = new Thickness(3);
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[0].Width = GridLength.Auto;
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            Label label = null;
            TextBox text = null;
            PasswordBox pswBox = null;
            Button btn = null;

            Dictionary<string, string> items = new Dictionary<string, string>();

            if (action == defActionLogin.PasswordRenew)
            {
                this.Title = "Inserire nuova password e token";
                items = new Dictionary<string, string>()
                {
                    { "Login", "Login" },
                    { "Password", "Nuova password" },
                    { "Token", "Token" },
                    { "DialogResult", "OK" }
                };
            }
            else
            {
                this.Title = "Login";
                items = new Dictionary<string, string>()
                {
                    { "Login", "Login" },
                    { "Password", "Password" },
                    { "Renew", "Password dimenticata/Nuovo utente" },
                    { "DialogResult", "OK" }
                };
            }

            foreach (KeyValuePair<string, string> item in items)
            {
                grid.RowDefinitions.Add(new RowDefinition());
                if (item.Key == "Login")
                {
                    label = new Label() { Content = string.Format("{0}:", item.Value) };
                    label.Margin = new Thickness(5, 2, 10, 2);
                    grid.Children.Add(label);
                    Grid.SetColumn(label, 0);
                    Grid.SetRow(label, grid.RowDefinitions.Count - 1);
                    text = new TextBox();
                    text.Margin = new Thickness(2);
                    text.VerticalContentAlignment = VerticalAlignment.Center;
                    text.TextChanged += (o, e) => { this.Login = ((TextBox)o).Text; };
                    text.Text = "system.admin@creaweb.it";
                    grid.Children.Add(text);
                    Grid.SetColumn(text, 1);
                    Grid.SetRow(text, grid.RowDefinitions.Count - 1);
                }
                else if (item.Key == "Password")
                {
                    label = new Label() { Content = string.Format("{0}:", item.Value) };
                    label.Margin = new Thickness(5, 2, 10, 2);
                    grid.Children.Add(label);
                    Grid.SetColumn(label, 0);
                    Grid.SetRow(label, grid.RowDefinitions.Count - 1);
                    pswBox = new PasswordBox();
                    pswBox.Margin = new Thickness(2);
                    pswBox.VerticalContentAlignment = VerticalAlignment.Center;
                    pswBox.PasswordChanged += (o, e) => { this.Password = ((PasswordBox)o).Password; };
                    pswBox.Password = "sys";
                    grid.Children.Add(pswBox);
                    Grid.SetColumn(pswBox, 1);
                    Grid.SetRow(pswBox, grid.RowDefinitions.Count - 1);
                }
                else if (item.Key == "Token")
                {
                    label = new Label() { Content = string.Format("{0}:", item.Value) };
                    label.Margin = new Thickness(5, 2, 10, 2);
                    grid.Children.Add(label);
                    Grid.SetColumn(label, 0);
                    Grid.SetRow(label, grid.RowDefinitions.Count - 1);
                    text = new TextBox();
                    text.Margin = new Thickness(2);
                    text.VerticalContentAlignment = VerticalAlignment.Center;
                    text.TextChanged += (o, e) => { this.Login = ((TextBox)o).Text; };
                    grid.Children.Add(text);
                    Grid.SetColumn(text, 1);
                    Grid.SetRow(text, grid.RowDefinitions.Count - 1);
                }
                else if (item.Key == "NewUser")
                {
                    label = new Label() { Content = string.Format("{0}:", item.Value) };
                    label.Margin = new Thickness(5, 2, 10, 2);
                    grid.Children.Add(label);
                    Grid.SetColumn(label, 0);
                    Grid.SetRow(label, grid.RowDefinitions.Count - 1);
                    text = new TextBox();
                    text.Margin = new Thickness(2);
                    text.VerticalContentAlignment = VerticalAlignment.Center;
                    text.TextChanged += (o, e) => { this.Login = ((TextBox)o).Text; };
                    grid.Children.Add(text);
                    Grid.SetColumn(text, 1);
                    Grid.SetRow(text, grid.RowDefinitions.Count - 1);
                }
                else if (item.Key == "Renew")
                {
                    btn = new Button();
                    btn.Margin = new Thickness(2);
                    btn.MinHeight = 35;
                    grid.Children.Add(btn);
                    btn.Content = item.Value;
                    Grid.SetColumn(btn, 1);
                    Grid.SetRow(btn, grid.RowDefinitions.Count - 1);
                    btn.Click += (o, e) => 
                    {
                        List<clsItemGenericData> GenericData = new List<clsItemGenericData>()
                        {
                            new clsItemGenericData(){ Key = "RENEW", Description = "Password dimenticata", Value = false, Type = typeof(bool), Selection = true },
                            new clsItemGenericData(){ Key = "NEWUSER", Description = "Nuovo utente", Value = false, Type = typeof(bool), Selection = true }
                        };
                        ManagerWindow win = new ManagerWindow("", null, "Azione", GenericData);
                        bool? actionDialog = win.ShowDialog();
                        if (actionDialog != null && actionDialog.Value)
                        {
                            defActionLogin actionResult = defActionLogin.None;
                            GenericData.ForEach(a =>
                            {
                                if (((bool)a.Value))
                                {
                                    actionResult = (a.Key == "RENEW" ? defActionLogin.PasswordByTokenRenew : defActionLogin.PasswordByToken);
                                }
                            });
                            win.Close();
                            if (actionResult != defActionLogin.None)
                            {
                                this.Action = actionResult;
                                this.DialogResult = true;
                            }
                        }
                    };
                }
                else if (item.Key == "DialogResult")
                {
                    btn = new Button();
                    btn.Content = " Abbandona ";
                    btn.Margin = new Thickness(5, 2, 10, 2);
                    btn.MinHeight = 35;
                    grid.Children.Add(btn);
                    Grid.SetColumn(btn, 0);
                    Grid.SetRow(btn, grid.RowDefinitions.Count - 1);
                    btn.Click += (o, e) => { this.DialogResult = false; };
                    btn = new Button();
                    btn.Margin = new Thickness(2);
                    btn.MinHeight = 35;
                    grid.Children.Add(btn);
                    btn.Content = item.Value;
                    Grid.SetColumn(btn, 1);
                    Grid.SetRow(btn, grid.RowDefinitions.Count - 1);
                    btn.Click += (o, e) => 
                    {
                        this.DialogResult = true; 
                    };
                }
            }

            this.AddChild(grid);
            this.SizeToContent = System.Windows.SizeToContent.Height;
            this.Width = 400;

        }

        

        
    }
}
