﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfConfigRiepiloghiMultiLib.Functions;
using WpfConfigRiepiloghiMultiLib.Models;

namespace WpfConfigRiepiloghiMultiLib.Views
{
    public class ManagerWindow : WindowBase
    {
        private List<clsItemGenericData> genericData { get; set; }
        

        private string token { get; set; }
        private clsAccount account { get; set; }
        private clsProfile selectedProfile { get; set; }
        private clsCodiceSistema selectedCodiceSistema { get; set; }
        private clsAccount selectedAccount { get; set; }
        private Grid gridSelectedObject { get; set; }

        private List<clsProfile> profiles { get; set; }

        private string search { get; set; }
        private Grid gridSearchItems { get; set; }
        private List<clsButtonItemVisible> itemsToFilter { get; set; }

        public string Search
        {
            get { return this.search; }
            set
            {
                this.search = value;
                if (this.itemsToFilter != null)
                {
                    this.itemsToFilter.ForEach(b =>
                    {
                        b.Visibility = string.IsNullOrEmpty(this.Search) || b.Content.ToString().ToUpper().Contains(this.search.ToUpper()) ? Visibility.Visible : Visibility.Hidden;
                    });
                }
                Action emptyDelegateLoader = delegate { };
                this.gridSearchItems.Dispatcher.Invoke(emptyDelegateLoader, System.Windows.Threading.DispatcherPriority.DataBind);
                this.gridSearchItems.Dispatcher.Invoke(emptyDelegateLoader, System.Windows.Threading.DispatcherPriority.Render);
            }
        }



        #region Menu account

        public ManagerWindow(string Token, clsAccount account)
        {
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            this.Title = "Menu";
            this.token = Token;
            this.account = account;
            Grid grid = new Grid();
            grid.Margin = new Thickness(3);
            List<clsOperazioneRiepiloghi> menus = new List<clsOperazioneRiepiloghi>();
            List<string> operations = new List<string>() { "RIEPILOGHI_PROFILES", "RIEPILOGHI_ACCOUNTS", "CONFIG_RIEPILOGHI", "CONFIG_SISTEMI" };

            account.Operazioni.OrderBy(categoria => categoria.Ordine).ToList().ForEach(categoria =>
            {
                if (categoria.ListaOperazioni != null)
                {
                    categoria.ListaOperazioni.Where(operazione => operations.Contains(operazione.IdOperazione)).OrderBy(operazione => operazione.Ordine).ToList().ForEach(operazione =>
                    {
                        menus.Add(operazione);
                    });
                }
            });

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            Button btn = null;
            List<long> categorieAggiunte = new List<long>();
            menus.ForEach(operazione =>
            {
                if (categorieAggiunte.Count == 0 || !categorieAggiunte.Contains(operazione.IdCategoria))
                {
                    categorieAggiunte.Add(operazione.IdCategoria);
                    grid.RowDefinitions.Add(new RowDefinition());
                    Label lblCategoria = new Label() { Content = operazione.DescrizioneCategoria, HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center };
                    grid.Children.Add(lblCategoria);
                    Grid.SetColumn(lblCategoria, 0);
                    Grid.SetRow(lblCategoria, grid.RowDefinitions.Count - 1);
                }

                grid.RowDefinitions.Add(new RowDefinition());
                btn = new Button();
                btn.MinHeight = 40;
                btn.Margin = new Thickness(2);
                btn.Content = operazione.Descrizione;
                btn.Tag = operazione.IdOperazione;
                grid.Children.Add(btn);
                Grid.SetColumn(btn, 0);
                Grid.SetRow(btn, grid.RowDefinitions.Count - 1);
                btn.Click += (o, e) =>
                {
                    if (((Button)o).Tag.ToString() == "RIEPILOGHI_PROFILES")
                    {
                        this.GestioneProfili();
                    }
                    else if (((Button)o).Tag.ToString() == "RIEPILOGHI_ACCOUNTS")
                    {
                        this.GestioneAccounts();
                    }
                };
            });

            grid.RowDefinitions.Add(new RowDefinition());
            btn = new Button();
            btn.MinHeight = 40;
            btn.Margin = new Thickness(2);
            btn.Content = "Chiudi";
            btn.Click += (o, e) => { this.DialogResult = true; };
            grid.Children.Add(btn);
            Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count);
            Grid.SetColumn(btn, 0);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);

            this.AddChild(grid);
            this.SizeToContent = System.Windows.SizeToContent.Height;
            this.Width = 400;
        }

        private void GestioneProfili()
        {
            bool reload = true;
            while (reload)
            {
                reload = false;
                ResponseRiepiloghi response = Functions.Recover.GetProfileList(this.token);
                if (response.Status.StatusOK)
                {
                    this.account.VisibleProfiles = response.Data.Profiles;
                    ManagerWindow managerProfiles = new ManagerWindow(this.token, this.account, response.Data.Profiles);
                    bool? dialog = managerProfiles.ShowDialog();
                    managerProfiles.Close();
                    reload = dialog.HasValue && dialog.Value;
                }
            }

        }



        private void GestioneAccounts()
        {
            bool reload = true;
            while (reload)
            {
                reload = false;
                ResponseRiepiloghi response = Functions.Recover.GetAccountList(this.token);
                if (response.Status.StatusOK)
                {
                    List<clsAccount> accounts = new List<clsAccount>(response.Data.Accounts);

                    response = Functions.Recover.GetProfileList(this.token);

                    if (response.Status.StatusOK)
                    {
                        this.account.VisibleProfiles = response.Data.Profiles;
                    }

                    ManagerWindow managerAccounts = new ManagerWindow(this.token, this.account, accounts);
                    bool? dialog = managerAccounts.ShowDialog();
                    managerAccounts.Close();
                    reload = dialog.HasValue && dialog.Value;
                }
            }
        }

        #endregion

        #region Profili

        public ManagerWindow(string Token, clsAccount account, List<clsProfile> profilesList)
        {
            this.token = Token;
            this.account = account;
            this.profiles = profilesList;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            this.Title = "Gestione profili";
            Button btn;
            Label lbl;
            Grid grid = new Grid();
            int colPropertyGrid = 2;

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[2].Width = GridLength.Auto;

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
            DockPanel panelTop = new DockPanel();
            panelTop.HorizontalAlignment = HorizontalAlignment.Stretch;

            grid.Children.Add(panelTop);
            Grid.SetColumn(panelTop, 0);
            Grid.SetColumnSpan(panelTop, 2);
            Grid.SetRow(panelTop, grid.RowDefinitions.Count - 1);

            Grid gridTop = new Grid();
            panelTop.Children.Add(gridTop);
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions[0].Width = new GridLength(2, GridUnitType.Star);
            gridTop.ColumnDefinitions[1].Width = new GridLength(2, GridUnitType.Star);
            gridTop.ColumnDefinitions[2].Width = GridLength.Auto;

            btn = new Button();
            btn.MinHeight = 30;
            btn.MaxHeight = btn.MinHeight;
            btn.Margin = new Thickness(3);
            btn.Content = "Nuovo";
            btn.Click += (o, e) => { this.NuovoProfilo(); };
            gridTop.Children.Add(btn);
            Grid.SetColumn(btn, 0);

            TextBox txtSearch = new TextBox();
            txtSearch.VerticalContentAlignment = VerticalAlignment.Center;
            txtSearch.MinHeight = 30;
            txtSearch.Margin = new Thickness(3);
            gridTop.Children.Add(txtSearch);
            Grid.SetColumn(txtSearch, 1);
            txtSearch.TextChanged += (o, e) => { this.Search = ((TextBox)o).Text; };

            clsButton btnSearch = new clsButton(ViewsWindows.LoadImage(WpfConfigRiepiloghiMultiLib.Properties.Resources.open, 30, 30), new Size(30, 30));
            gridTop.Children.Add(btnSearch);
            Grid.SetColumn(btnSearch, 2);

            lbl = new Label();
            lbl.MinHeight = 30;
            lbl.MaxHeight = lbl.MinHeight;
            lbl.Margin = new Thickness(3);
            lbl.Content = "Profilo selezionato";
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, colPropertyGrid);
            Grid.SetColumnSpan(lbl, 2);
            Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

            grid.RowDefinitions.Add(new RowDefinition());

            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(3);
            panel.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panel);
            Grid.SetRow(panel, grid.RowDefinitions.Count - 1);
            Grid.SetColumn(panel, 0);
            Grid.SetColumnSpan(panel, 2);

            ScrollViewer scroll = new ScrollViewer();
            scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panel.Children.Add(scroll);

            StackPanel panelGrid = new StackPanel();
            panelGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelGrid.VerticalAlignment = VerticalAlignment.Top;
            scroll.Content = panelGrid;

            Grid gridItems = new Grid();
            gridItems.ColumnDefinitions.Add(new ColumnDefinition());
            panelGrid.Children.Add(gridItems);
            this.itemsToFilter = new List<clsButtonItemVisible>();
            if (profiles != null)
                profiles.ForEach(i => this.AddProfileToGrid(i, gridItems));


            DockPanel panelSelectedProfile = new DockPanel();
            panelSelectedProfile.Margin = new Thickness(3);
            panelSelectedProfile.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panelSelectedProfile);
            Grid.SetRow(panelSelectedProfile, 1);
            Grid.SetColumn(panelSelectedProfile, colPropertyGrid);
            Grid.SetColumnSpan(panelSelectedProfile, 2);

            ScrollViewer scrollSelectedProfile = new ScrollViewer();
            scrollSelectedProfile.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panelSelectedProfile.Children.Add(scrollSelectedProfile);

            StackPanel panelgridSelectedObject = new StackPanel();
            panelgridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.VerticalAlignment = VerticalAlignment.Top;
            scrollSelectedProfile.Content = panelgridSelectedObject;

            this.gridSelectedObject = new Grid();
            this.gridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.Children.Add(this.gridSelectedObject);

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;

            btn = new Button();
            btn.MinHeight = 30;
            btn.MaxHeight = btn.MinHeight;
            btn.Margin = new Thickness(3);
            btn.Content = "Chiudi";
            btn.Click += (o, e) => { this.DialogResult = false; };
            grid.Children.Add(btn);
            Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count);
            Grid.SetColumn(btn, 0);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);


            GridSplitter splitter = new GridSplitter();
            splitter.HorizontalAlignment = HorizontalAlignment.Left;
            DockPanel.SetDock(splitter, Dock.Left);
            splitter.Width = 5;
            splitter.Background = Brushes.DarkGray;
            grid.Children.Add(splitter);
            Grid.SetRow(splitter, 0);
            Grid.SetColumn(splitter, colPropertyGrid);
            Grid.SetRowSpan(splitter, grid.RowDefinitions.Count - 1);

            this.AddChild(grid);
            this.SizeToContent = System.Windows.SizeToContent.Manual;
            this.Height = 600;
            this.Width = 800;

            this.Loaded += (o, e) =>
            {
                for (int i = 0; i < 2; i++)
                {
                    grid.ColumnDefinitions[i].Width = new GridLength((grid.ActualWidth - grid.ColumnDefinitions[2].ActualWidth) / 4);
                }
            };
        }

        private void AddProfileToGrid(clsProfile profile, Grid gridItems)
        {
            if (this.account.CodiciSistema != null)
            {
                this.account.CodiciSistema.ForEach(codiceSistema =>
                {
                    clsCodiceSistema codiceSistemaToCheck = profile.CodiciSistema != null ? profile.CodiciSistema.FirstOrDefault(x => x.CodiceSistema == codiceSistema.CodiceSistema) : null;
                    if (codiceSistemaToCheck == null)
                    {
                        codiceSistemaToCheck = new clsCodiceSistema()
                        {
                            CodiceSistema = codiceSistema.CodiceSistema,
                            Descrizione = codiceSistema.Descrizione,
                            AllCFOrganizzatori = codiceSistema.AllCFOrganizzatori,
                            AllCodiciLocali = codiceSistema.AllCodiciLocali,
                            CFOrganizzatori = new List<clsCFOrganizzatore>(codiceSistema.CFOrganizzatori.ToArray()),
                            CodiciLocale = new List<clsCodiceLocale>(codiceSistema.CodiciLocale.ToArray()),
                            Enabled = false,
                            Modificato = false
                        };
                        if (profile.CodiciSistema == null)
                            profile.CodiciSistema = new List<clsCodiceSistema>();
                        profile.CodiciSistema.Add(codiceSistemaToCheck);
                    }
                });
            }

            if (this.account.Operazioni != null)
            {
                this.account.Operazioni.OrderBy(c => c.Ordine).ToList().ForEach(categoriaOperazione =>
                {
                    categoriaOperazione.ListaOperazioni.OrderBy(o => o.Ordine).ToList().ForEach(operazione =>
                    {
                        clsOperazioneRiepiloghi operazioneToCheck = profile.OperazioniProfile != null &&
                                                                    profile.OperazioniProfile.FirstOrDefault(x => x.IdCategoria == operazione.IdCategoria) != null &&
                                                                    profile.OperazioniProfile.FirstOrDefault(x => x.IdCategoria == operazione.IdCategoria).ListaOperazioni != null ?
                                                                    profile.OperazioniProfile.FirstOrDefault(x => x.IdCategoria == operazione.IdCategoria).ListaOperazioni.FirstOrDefault(x => x.IdOperazione == operazione.IdOperazione) : null;
                        if (operazioneToCheck == null)
                        {
                            if (profile.OperazioniProfile == null)
                                profile.OperazioniProfile = new List<clsCategoriaOperazioniRiepiloghi>();
                            if (profile.OperazioniProfile.FirstOrDefault(x => x.IdCategoria == operazione.IdCategoria) == null)
                            {
                                clsCategoriaOperazioniRiepiloghi categoriaToCheck = new clsCategoriaOperazioniRiepiloghi()
                                {
                                    IdCategoria = categoriaOperazione.IdCategoria,
                                    Descrizione = categoriaOperazione.Descrizione,
                                    Ordine = categoriaOperazione.Ordine,
                                    ListaOperazioni = new List<clsOperazioneRiepiloghi>()
                                };
                            }
                            operazioneToCheck = new clsOperazioneRiepiloghi()
                            {
                                Descrizione = operazione.Descrizione,
                                DescrizioneCategoria = operazione.DescrizioneCategoria,
                                IdCategoria = operazione.IdCategoria,
                                IdOperazione = operazione.IdOperazione,
                                Ordine = operazione.Ordine,
                                OrdineCategoria = operazione.OrdineCategoria,
                                Enabled = false,
                                Modificato = false
                            };
                            profile.OperazioniProfile.FirstOrDefault(x => x.IdCategoria == operazione.IdCategoria).ListaOperazioni.Add(operazioneToCheck);
                        }
                    });
                });
            }

            clsButtonItemVisible btnItem = new clsButtonItemVisible(this) { HorizontalContentAlignment = HorizontalAlignment.Left };
            btnItem.Margin = new Thickness(1);
            btnItem.Tag = profile;
            btnItem.MinHeight = 30;
            btnItem.MinWidth = 100;
            btnItem.Content = profile.Descrizione;
            btnItem.Click += (o, e) => { this.SelectedProfile = profile; };
            gridItems.RowDefinitions.Add(new RowDefinition());
            gridItems.Children.Add(btnItem);
            Grid.SetRow(btnItem, gridItems.RowDefinitions.Count - 1);
            gridItems.RowDefinitions[gridItems.RowDefinitions.Count - 1].Height = GridLength.Auto;
            if (this.gridSearchItems == null || !this.gridSearchItems.Equals(gridItems))
                this.gridSearchItems = gridItems;
            this.itemsToFilter.Add(btnItem);
        }

        public clsProfile SelectedProfile
        {
            get { return this.selectedProfile; }
            set
            {
                this.gridSelectedObject.Children.Clear();
                this.gridSelectedObject.RowDefinitions.Clear();
                this.gridSelectedObject.ColumnDefinitions.Clear();

                this.selectedProfile = value;

                if (this.selectedProfile != null)
                {
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());
                    this.gridSelectedObject.ColumnDefinitions[0].Width = GridLength.Auto;
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());

                    Label lbl;
                    TextBox text;
                    CheckBox checkBox;
                    Button btn;
                    ComboBox comboProfile;

                    lbl = new Label() { Content = "Descrizione" };
                    text = new TextBox() { Text = this.selectedProfile.Descrizione };
                    text.TextChanged += (o, e) => { this.selectedProfile.Descrizione = ((TextBox)o).Text; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, text);

                    lbl = new Label() { Content = "Abilitato" };
                    checkBox = new CheckBox() { IsChecked = this.selectedProfile.Enabled };
                    checkBox.Click += (o, e) => { this.selectedProfile.Enabled = ((CheckBox)o).IsChecked.Value; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Tutti i codici sistema" };
                    checkBox = new CheckBox() { IsChecked = this.selectedProfile.AllCodiciSistema };
                    checkBox.Click += (o, e) => { this.selectedProfile.AllCodiciSistema = ((CheckBox)o).IsChecked.Value; ; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Codici sistema" };
                    btn = new Button() { Content = "..." };
                    btn.SetBinding(Button.ContentProperty, new Binding("DescrizioneSistemiProfilo") { Source = this.selectedProfile });
                    this.AddRowProperty(gridSelectedObject, lbl, btn);
                    btn.Click += (o, e) =>
                    {
                        this.GestioneCodiciSistema();
                    };

                    lbl = new Label() { Content = "Operazioni" };
                    btn = new Button() { Content = "..." };
                    btn.SetBinding(Button.ContentProperty, new Binding("DescrizioneOperazioniProfilo") { Source = this.selectedProfile });
                    btn.Click += (o, e) =>
                    {
                        ManagerWindow windowCS = new ManagerWindow(this.token, this.account, this.selectedProfile, null, "Operazioni");
                        windowCS.ShowDialog();
                        windowCS.Close();

                    };
                    this.AddRowProperty(gridSelectedObject, lbl, btn);

                    lbl = new Label() { Content = "Profilo di appartenenza" };
                    comboProfile = new ComboBox();
                    comboProfile.IsEnabled = false;
                    if (this.account.VisibleProfiles != null && this.account.VisibleProfiles.Count > 0)
                    {
                        this.account.VisibleProfiles.ForEach(p =>
                        {
                            ComboBoxItem item = new ComboBoxItem() { Tag = p, Content = p.Descrizione };
                            comboProfile.Items.Add(item);
                            if (p.ProfileId == this.selectedProfile.ParentProfileId)
                                comboProfile.SelectedItem = item;
                        });
                        comboProfile.SelectionChanged += (o, e) =>
                        {
                            this.selectedAccount.ProfileId = ((clsProfile)((ComboBoxItem)comboProfile.SelectedItem).Tag).ProfileId;
                            this.selectedAccount.Modificato = true;
                        };
                    }
                    comboProfile.IsEnabled = comboProfile.Items.Count > 0;
                    this.AddRowProperty(gridSelectedObject, lbl, comboProfile);

                    Button btnSave = new Button() { Content = "Salva", MinHeight = 30 };
                    btnSave.IsEnabled = this.selectedProfile.Modificato;
                    btnSave.Click += (o, e) =>
                    {
                        this.SalvaProfiliModificati();
                        this.DialogResult = true;
                    };
                    this.AddRowProperty(gridSelectedObject, null, btnSave);
                    this.selectedProfile.EventModificato += (o, e) => { btnSave.IsEnabled = true; };
                }
            }
        }

        private void GestioneCodiciSistema()
        {
            if (this.selectedProfile != null)
            {
                ManagerWindow managerProfiles = new ManagerWindow(this.token, this.account, this.selectedProfile, this.selectedProfile.CodiciSistema);
                managerProfiles.ShowDialog();
                managerProfiles.Close();
            }
        }

        private void NuovoProfilo()
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            clsItemGenericData item;
            item = new clsItemGenericData() { Key = "DESCR", Description = "Nome profilo", Type = typeof(string) };
            items.Add(item);

            item = new clsItemGenericData() { Key = "PARENT", Description = "Profilo padre", Type = typeof(long) };
            item.Values = new Dictionary<object, string>();
            this.account.VisibleProfiles.ForEach(visibleProfile =>
            {
                item.Values.Add(visibleProfile.ProfileId, visibleProfile.Descrizione);
            });
            items.Add(item);

            ManagerWindow winNew = new ManagerWindow(this.token, this.account, "Nuovo profilo", items);
            bool? result = winNew.ShowDialog();
            if (result != null && result.Value)
            {
                clsProfile profiloToCreate = new clsProfile();
                items.ForEach(item =>
                {
                    if (item.Key == "DESCR")
                        profiloToCreate.Descrizione = item.Value.ToString();
                    else if (item.Key == "PARENT")
                        profiloToCreate.ParentProfileId = long.Parse(item.Value.ToString());
                });
                // chiamata per salvare e poi ricarico
                this.DialogResult = true;
            }
            winNew.Close();
        }

        private bool SalvaProfiliModificati()
        {
            WindowsWait.SetWaitMessage("Salvataggio...");
            bool result = false;
            if (this.profiles != null)
            {
                profiles.Where(profile => profile.Modificato).ToList().ForEach(profile =>
                {
                    if (profile.AllCodiciSistema)
                        profile.CodiciSistema = new List<clsCodiceSistema>();
                    if (profile.CodiciSistema != null && profile.CodiciSistema.Count > 0)
                    {
                        profile.CodiciSistema = new List<clsCodiceSistema>(profile.CodiciSistema.Where(cs => cs.Enabled).ToList());
                    }
                    if (profile.OperazioniProfile != null && profile.OperazioniProfile.Count > 0)
                    {
                        List<clsCategoriaOperazioniRiepiloghi> lista = new List<clsCategoriaOperazioniRiepiloghi>();
                        profile.OperazioniProfile.ForEach(cat =>
                        {
                            if (cat.ListaOperazioni != null && cat.ListaOperazioni.FirstOrDefault(o => o.Enabled) != null)
                            {
                                cat.ListaOperazioni = new List<clsOperazioneRiepiloghi>(cat.ListaOperazioni.Where(o => o.Enabled).ToList());
                                lista.Add(cat);
                            }
                        });
                        profile.OperazioniProfile = lista;
                    }
                    ResponseRiepiloghi baseResponse = null;
                    if (profile.ProfileId == 0)
                    {

                    }
                    else
                    {
                        baseResponse = Recover.SetProfile(this.token, profile);
                    }
                });
            }
            WindowsWait.SetWaitMessage("");
            return result;
        }

        #region Codici di sistema

        public ManagerWindow(string Token, clsAccount account, clsProfile selectedProfile, List<clsCodiceSistema> codiciSistemaSelectedProfile)
        {
            this.token = Token;
            this.account = account;
            this.selectedProfile = selectedProfile;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;

            this.Title = string.Format("Gestione Codici Sistema {0}", this.selectedProfile.Descrizione);
            Button btn;
            Label lbl;
            Grid grid = new Grid();
            int colPropertyGrid = 2;

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[2].Width = GridLength.Auto;

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
            lbl = new Label();
            lbl.MinHeight = 30;
            lbl.MaxHeight = lbl.MinHeight;
            lbl.Margin = new Thickness(3);
            lbl.Content = "Lista codici di sistema";
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, 0);
            Grid.SetColumnSpan(lbl, 2);
            Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

            lbl = new Label();
            lbl.MinHeight = 30;
            lbl.MaxHeight = lbl.MinHeight;
            lbl.Margin = new Thickness(3);
            lbl.Content = "Codice Sistema selezionato";
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, colPropertyGrid);
            Grid.SetColumnSpan(lbl, 2);
            Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

            grid.RowDefinitions.Add(new RowDefinition());

            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(3);
            panel.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panel);
            Grid.SetRow(panel, grid.RowDefinitions.Count - 1);
            Grid.SetColumn(panel, 0);
            Grid.SetColumnSpan(panel, 2);

            ScrollViewer scroll = new ScrollViewer();
            scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panel.Children.Add(scroll);

            StackPanel panelGrid = new StackPanel();
            panelGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelGrid.VerticalAlignment = VerticalAlignment.Top;
            scroll.Content = panelGrid;

            Grid gridItems = new Grid();
            gridItems.ColumnDefinitions.Add(new ColumnDefinition());
            panelGrid.Children.Add(gridItems);
            if (this.selectedProfile.CodiciSistema != null)
                this.selectedProfile.CodiciSistema.ForEach(codiceSistema => this.AddCodiceSistema(codiceSistema, gridItems));

            DockPanel panelSelectedCS = new DockPanel();
            panelSelectedCS.Margin = new Thickness(3);
            panelSelectedCS.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panelSelectedCS);
            Grid.SetRow(panelSelectedCS, 1);
            Grid.SetColumn(panelSelectedCS, colPropertyGrid);
            Grid.SetColumnSpan(panelSelectedCS, 2);

            ScrollViewer scrollSelectedCS = new ScrollViewer();
            scrollSelectedCS.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panelSelectedCS.Children.Add(scrollSelectedCS);

            StackPanel panelgridSelectedObject = new StackPanel();
            panelgridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.VerticalAlignment = VerticalAlignment.Top;
            scrollSelectedCS.Content = panelgridSelectedObject;

            this.gridSelectedObject = new Grid();
            this.gridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.Children.Add(this.gridSelectedObject);

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;

            btn = new Button();
            btn.MinHeight = 30;
            btn.MaxHeight = btn.MinHeight;
            btn.Margin = new Thickness(3);
            btn.Content = "Chiudi";
            btn.Click += (o, e) => { this.DialogResult = true; };
            grid.Children.Add(btn);
            Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count);
            Grid.SetColumn(btn, 0);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);


            GridSplitter splitter = new GridSplitter();
            splitter.HorizontalAlignment = HorizontalAlignment.Left;
            DockPanel.SetDock(splitter, Dock.Left);
            splitter.Width = 5;
            splitter.Background = Brushes.DarkGray;
            grid.Children.Add(splitter);
            Grid.SetRow(splitter, 0);
            Grid.SetColumn(splitter, colPropertyGrid);
            Grid.SetRowSpan(splitter, grid.RowDefinitions.Count - 1);

            this.AddChild(grid);
            this.SizeToContent = System.Windows.SizeToContent.Manual;
            this.Height = 600;
            this.Width = 800;

            this.Loaded += (o, e) =>
            {
                for (int i = 0; i < 2; i++)
                {
                    grid.ColumnDefinitions[i].Width = new GridLength((grid.ActualWidth - grid.ColumnDefinitions[2].ActualWidth) / 4);
                }
            };
        }

        private void AddCodiceSistema(clsCodiceSistema codiceSistema, Grid gridItems)
        {
            Button btnItem = new Button() { HorizontalContentAlignment = HorizontalAlignment.Left };
            btnItem.Margin = new Thickness(1);
            btnItem.Tag = codiceSistema;
            btnItem.MinHeight = 30;
            btnItem.MinWidth = 100;
            btnItem.Content = codiceSistema.Descrizione;
            btnItem.Click += (o, e) => { this.SelectedCodiceSistema = codiceSistema; };
            gridItems.RowDefinitions.Add(new RowDefinition());
            gridItems.Children.Add(btnItem);
            Grid.SetRow(btnItem, gridItems.RowDefinitions.Count - 1);
            gridItems.RowDefinitions[gridItems.RowDefinitions.Count - 1].Height = GridLength.Auto;
        }

        public clsCodiceSistema SelectedCodiceSistema
        {
            get { return this.selectedCodiceSistema; }
            set
            {
                this.gridSelectedObject.Children.Clear();
                this.gridSelectedObject.RowDefinitions.Clear();
                this.gridSelectedObject.ColumnDefinitions.Clear();

                this.selectedCodiceSistema = value;

                if (this.selectedCodiceSistema != null)
                {
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());
                    this.gridSelectedObject.ColumnDefinitions[0].Width = GridLength.Auto;
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());

                    Label lbl;
                    TextBox text;
                    CheckBox checkBox;
                    Button btn;
                    ComboBox comboProfile;

                    lbl = new Label() { Content = "Abilitato" };
                    checkBox = new CheckBox() { IsChecked = this.selectedCodiceSistema.Enabled };
                    checkBox.Click += (o, e) => { this.selectedCodiceSistema.Enabled = ((CheckBox)o).IsChecked.Value; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Tutti i codici Locali" };
                    checkBox = new CheckBox() { IsChecked = this.selectedCodiceSistema.AllCodiciLocali };
                    checkBox.Click += (o, e) => { this.selectedCodiceSistema.AllCodiciLocali = ((CheckBox)o).IsChecked.Value; ; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Codici locali" };
                    btn = new Button() { Content = "..." };
                    btn.SetBinding(Button.ContentProperty, new Binding("DescrizioneCodiciLocali") { Source = this.selectedCodiceSistema });
                    this.AddRowProperty(gridSelectedObject, lbl, btn);
                    btn.Click += (o, e) =>
                    {
                        ManagerWindow windowCS = new ManagerWindow(this.token, this.account, this.selectedProfile, this.selectedCodiceSistema, "CodiciLocali");
                        windowCS.ShowDialog();
                        windowCS.Close();
                    };

                    lbl = new Label() { Content = "Tutti gli Organizzatori" };
                    checkBox = new CheckBox() { IsChecked = this.selectedCodiceSistema.AllCFOrganizzatori };
                    checkBox.Click += (o, e) => { this.selectedCodiceSistema.AllCFOrganizzatori = ((CheckBox)o).IsChecked.Value; ; this.selectedProfile.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Organizzatori" };
                    btn = new Button() { Content = "..." };
                    btn.SetBinding(Button.ContentProperty, new Binding("DescrizioneOrganizzatori") { Source = this.selectedCodiceSistema });
                    btn.Click += (o, e) =>
                    {
                        ManagerWindow windowCS = new ManagerWindow(this.token, this.account, this.selectedProfile, this.selectedCodiceSistema, "CFOrganizzatori");
                        windowCS.ShowDialog();
                        windowCS.Close();
                    };
                    this.AddRowProperty(gridSelectedObject, lbl, btn);
                }
            }
        }

        #endregion

        #endregion

        #region Accounts

        public ManagerWindow(string Token, clsAccount account, List<clsAccount> accounts)
        {
            this.token = Token;
            this.account = account;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            this.Title = "Gestione accounts";
            Button btn;
            Label lbl;
            Grid grid = new Grid();
            int colPropertyGrid = 2;

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[2].Width = GridLength.Auto;

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;

            DockPanel panelTop = new DockPanel();
            panelTop.HorizontalAlignment = HorizontalAlignment.Stretch;

            grid.Children.Add(panelTop);
            Grid.SetColumn(panelTop, 0);
            Grid.SetColumnSpan(panelTop, 2);
            Grid.SetRow(panelTop, grid.RowDefinitions.Count - 1);

            Grid gridTop = new Grid();
            panelTop.Children.Add(gridTop);
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions.Add(new ColumnDefinition());
            gridTop.ColumnDefinitions[0].Width = new GridLength(2, GridUnitType.Star);
            gridTop.ColumnDefinitions[1].Width = new GridLength(2, GridUnitType.Star);
            gridTop.ColumnDefinitions[2].Width = GridLength.Auto;

            btn = new Button();
            btn.MinHeight = 30;
            btn.MaxHeight = btn.MinHeight;
            btn.Margin = new Thickness(3);
            btn.Content = "Nuovo";
            btn.Click += (o, e) => { this.NuovoAccount(); };
            gridTop.Children.Add(btn);
            Grid.SetColumn(btn, 0);

            TextBox txtSearch = new TextBox();
            txtSearch.VerticalContentAlignment = VerticalAlignment.Center;
            txtSearch.MinHeight = 30;
            txtSearch.Margin = new Thickness(3);
            gridTop.Children.Add(txtSearch);
            Grid.SetColumn(txtSearch, 1);
            txtSearch.TextChanged += (o, e) => { this.Search = ((TextBox)o).Text; };

            clsButton btnSearch = new clsButton(ViewsWindows.LoadImage(WpfConfigRiepiloghiMultiLib.Properties.Resources.open, 30, 30), new Size(30, 30));
            gridTop.Children.Add(btnSearch);
            Grid.SetColumn(btnSearch, 2);

            lbl = new Label();
            lbl.MinHeight = 30;
            lbl.MaxHeight = btn.MinHeight;
            lbl.Margin = new Thickness(3);
            lbl.Content = "Account selezionato";
            grid.Children.Add(lbl);
            Grid.SetColumn(lbl, colPropertyGrid);
            Grid.SetColumnSpan(lbl, 2);
            Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

            grid.RowDefinitions.Add(new RowDefinition());

            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(3);
            panel.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panel);
            Grid.SetRow(panel, grid.RowDefinitions.Count - 1);
            Grid.SetColumn(panel, 0);
            Grid.SetColumnSpan(panel, 2);

            ScrollViewer scroll = new ScrollViewer();
            scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panel.Children.Add(scroll);

            StackPanel panelGrid = new StackPanel();
            panelGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelGrid.VerticalAlignment = VerticalAlignment.Top;
            scroll.Content = panelGrid;

            Grid gridItems = new Grid();
            gridItems.ColumnDefinitions.Add(new ColumnDefinition());
            panelGrid.Children.Add(gridItems);
            this.itemsToFilter = new List<clsButtonItemVisible>();
            if (accounts != null)
                accounts.ForEach(i => this.AddAccountToGrid(i, gridItems));


            DockPanel panelSelectedProfile = new DockPanel();
            panelSelectedProfile.Margin = new Thickness(3);
            //panelSelectedProfile.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelSelectedProfile.VerticalAlignment = VerticalAlignment.Stretch;
            grid.Children.Add(panelSelectedProfile);
            Grid.SetRow(panelSelectedProfile, 1);
            Grid.SetColumn(panelSelectedProfile, colPropertyGrid);
            Grid.SetColumnSpan(panelSelectedProfile, 2);

            ScrollViewer scrollSelectedProfile = new ScrollViewer();
            scrollSelectedProfile.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panelSelectedProfile.Children.Add(scrollSelectedProfile);

            StackPanel panelgridSelectedObject = new StackPanel();
            panelgridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.VerticalAlignment = VerticalAlignment.Top;
            //panelgridSelectedObject.Margin = new Thickness(5, 0, 0, 0);
            scrollSelectedProfile.Content = panelgridSelectedObject;

            this.gridSelectedObject = new Grid();
            this.gridSelectedObject.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelgridSelectedObject.Children.Add(this.gridSelectedObject);

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;

            btn = new Button();
            btn.MinHeight = 30;
            btn.MaxHeight = btn.MinHeight;
            btn.Margin = new Thickness(3);
            btn.Content = "Chiudi";
            btn.Click += (o, e) => { this.DialogResult = true; };
            grid.Children.Add(btn);
            Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count);
            Grid.SetColumn(btn, 0);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);


            GridSplitter splitter = new GridSplitter();
            splitter.HorizontalAlignment = HorizontalAlignment.Left;
            DockPanel.SetDock(splitter, Dock.Left);
            splitter.Width = 5;
            splitter.Background = Brushes.DarkGray;
            grid.Children.Add(splitter);
            Grid.SetRow(splitter, 0);
            Grid.SetColumn(splitter, colPropertyGrid);
            Grid.SetRowSpan(splitter, grid.RowDefinitions.Count - 1);

            this.AddChild(grid);
            this.SizeToContent = System.Windows.SizeToContent.Manual;
            this.Height = 600;
            this.Width = 800;

            this.Loaded += (o, e) =>
            {
                for (int i = 0; i < 2; i++)
                {
                    grid.ColumnDefinitions[i].Width = new GridLength((grid.ActualWidth - grid.ColumnDefinitions[2].ActualWidth) / 4);
                }
            };
        }

        private void AddAccountToGrid(clsAccount account, Grid gridItems)
        {
            clsButtonItemVisible btnItem = new clsButtonItemVisible(this) { HorizontalContentAlignment = HorizontalAlignment.Left };
            btnItem.Margin = new Thickness(1);
            btnItem.Tag = account;
            btnItem.MinHeight = 30;
            btnItem.MinWidth = 100;
            btnItem.Content = account.Descrizione;
            btnItem.Click += (o, e) => { this.SelectedAccount = account; };
            gridItems.RowDefinitions.Add(new RowDefinition());
            gridItems.Children.Add(btnItem);
            Grid.SetRow(btnItem, gridItems.RowDefinitions.Count - 1);
            gridItems.RowDefinitions[gridItems.RowDefinitions.Count - 1].Height = GridLength.Auto;
            if (this.gridSearchItems == null || !this.gridSearchItems.Equals(gridItems))
                this.gridSearchItems = gridItems;
            this.itemsToFilter.Add(btnItem);
        }

        public clsAccount SelectedAccount
        {
            get { return this.selectedAccount; }
            set
            {
                this.gridSelectedObject.Children.Clear();
                this.gridSelectedObject.RowDefinitions.Clear();
                this.gridSelectedObject.ColumnDefinitions.Clear();

                this.selectedAccount = value;

                if (this.selectedAccount != null)
                {
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());
                    this.gridSelectedObject.ColumnDefinitions[0].Width = GridLength.Auto;
                    this.gridSelectedObject.ColumnDefinitions.Add(new ColumnDefinition());

                    Label lbl;
                    TextBox text;
                    CheckBox checkBox;
                    ComboBox comboProfile;

                    lbl = new Label() { Content = "Email" };
                    text = new TextBox() { Text = this.selectedAccount.Email };
                    text.TextChanged += (o, e) => { this.selectedAccount.Email = ((TextBox)o).Text; this.selectedAccount.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, text);

                    lbl = new Label() { Content = "Cognome" };
                    text = new TextBox() { Text = this.selectedAccount.Cognome };
                    text.TextChanged += (o, e) => { this.selectedAccount.Cognome = ((TextBox)o).Text; this.selectedAccount.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, text);

                    lbl = new Label() { Content = "Nome" };
                    text = new TextBox() { Text = this.selectedAccount.Nome };
                    text.TextChanged += (o, e) => { this.selectedAccount.Nome = ((TextBox)o).Text; this.selectedAccount.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, text);

                    lbl = new Label() { Content = "Abilitato" };
                    checkBox = new CheckBox() { IsChecked = this.selectedAccount.Enabled };
                    checkBox.Click += (o, e) => { this.selectedAccount.Enabled = ((CheckBox)o).IsChecked.Value; this.selectedAccount.Modificato = true; };
                    this.AddRowProperty(gridSelectedObject, lbl, checkBox);

                    lbl = new Label() { Content = "Profilo di appartenenza" };
                    comboProfile = new ComboBox();
                    comboProfile.IsEnabled = false;
                    if (this.account.VisibleProfiles != null && this.account.VisibleProfiles.Count > 0)
                    {
                        this.account.VisibleProfiles.ForEach(p =>
                        {
                            ComboBoxItem item = new ComboBoxItem() { Tag = p, Content = p.Descrizione };
                            comboProfile.Items.Add(item);
                            if (p.ProfileId == this.selectedAccount.ProfileId)
                                comboProfile.SelectedItem = item;
                        });
                        comboProfile.SelectionChanged += (o, e) =>
                        {
                            this.selectedAccount.ProfileId = ((clsProfile)((ComboBoxItem)comboProfile.SelectedItem).Tag).ProfileId;
                            this.selectedAccount.Modificato = true;
                        };
                    }
                    comboProfile.IsEnabled = comboProfile.Items.Count > 0;
                    this.AddRowProperty(gridSelectedObject, lbl, comboProfile);

                    Button btnSave = new Button() { Content = "Salva", MinHeight = 30 };
                    btnSave.IsEnabled = this.selectedAccount.Modificato;
                    btnSave.Click += (o, e) =>
                    {

                    };
                    this.AddRowProperty(gridSelectedObject, null, btnSave);
                    this.selectedAccount.EventModificato += (o, e) => { btnSave.IsEnabled = true; };
                }
            }
        }

        private void NuovoAccount()
        {
            List<clsItemGenericData> items = new List<clsItemGenericData>();
            clsItemGenericData item;
            item = new clsItemGenericData() { Key = "EMAIL", Description = "Email", Type = typeof(string) };
            items.Add(item);
            item = new clsItemGenericData() { Key = "COGNOME", Description = "Cognome", Type = typeof(string) };
            items.Add(item);
            item = new clsItemGenericData() { Key = "NOME", Description = "Nome", Type = typeof(string) };
            items.Add(item);
            item = new clsItemGenericData() { Key = "PROFILO", Description = "Profilo", Type = typeof(long) };
            item.Values = new Dictionary<object, string>();
            this.account.VisibleProfiles.ForEach(visibleProfile =>
            {
                item.Values.Add(visibleProfile.ProfileId, visibleProfile.Descrizione);
            });
            items.Add(item);

            ManagerWindow winNew = new ManagerWindow(this.token, this.account, "Nuovo account", items);
            bool? result = winNew.ShowDialog();
            if (result != null && result.Value)
            {
                clsAccount accountToCreate = new clsAccount();
                items.ForEach(item =>
                {
                    if (item.Key == "EMAIL")
                        accountToCreate.Email = item.Value.ToString();
                    else if (item.Key == "NOME")
                        accountToCreate.Nome = item.Value.ToString();
                    else if (item.Key == "COGNOME")
                        accountToCreate.Cognome = item.Value.ToString();
                    else if (item.Key == "PROFILO")
                        accountToCreate.ProfileId = long.Parse(item.Value.ToString());
                });
                // chiamata per salvare e poi ricarico
                Recover.CreateAccount(this.token, accountToCreate);
                this.DialogResult = true;
            }
            winNew.Close();

        }

        #endregion

        #region Altro

        public class clsItemGenericData
        {
            public string Key { get; set; }
            public object Value { get; set; }
            public string Description { get; set; }
            public Type Type { get; set; }
            public Dictionary<object, string> Values { get; set; }
            public bool Selection { get; set; }
        }

        private void AddRowProperty(Grid gridProperty, Control description, Control value)
        {
            
            gridProperty.RowDefinitions.Add(new RowDefinition());
            gridProperty.RowDefinitions[gridProperty.RowDefinitions.Count - 1].Height = GridLength.Auto;

            if (description != null)
            {
                description.Margin = new Thickness(3, 0, 0, 3);
                gridProperty.Children.Add(description);
                Grid.SetColumn(description, 0);
                Grid.SetRow(description, gridProperty.RowDefinitions.Count - 1);
            }
            DockPanel pnl = new DockPanel();
            pnl.HorizontalAlignment = HorizontalAlignment.Stretch;
            pnl.VerticalAlignment = VerticalAlignment.Center;
            pnl.MinWidth = 100;
            gridProperty.Children.Add(pnl);
            pnl.LastChildFill = true;
            pnl.Children.Add(value);

            value.Margin = new Thickness(3, 0, 0, 3);
            value.VerticalAlignment = VerticalAlignment.Center;
            value.HorizontalAlignment = HorizontalAlignment.Stretch;

            if (description != null)
            {
                Grid.SetColumn(pnl, 1);
            }
            else
            {
                Grid.SetColumn(pnl, 0);
                Grid.SetColumnSpan(pnl, 2);
            }
            Grid.SetRow(pnl, gridProperty.RowDefinitions.Count - 1);
        }

        public ManagerWindow(string Token, clsAccount account, clsProfile profile, clsCodiceSistema codiceSistema, string action)
        {
            this.account = account;
            this.selectedProfile = profile;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            


            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(3);
            panel.HorizontalAlignment = HorizontalAlignment.Left;
            panel.VerticalAlignment = VerticalAlignment.Stretch;
            this.AddChild(panel);


            ScrollViewer scroll = new ScrollViewer();
            scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            scroll.HorizontalAlignment = HorizontalAlignment.Right;
            scroll.HorizontalContentAlignment = HorizontalAlignment.Left;
            panel.Children.Add(scroll);

            Label lbl;
            CheckBox checkBox;
            StackPanel panelGrid = new StackPanel();
            panelGrid.HorizontalAlignment = HorizontalAlignment.Left;
            panelGrid.VerticalAlignment = VerticalAlignment.Top;
            panelGrid.Margin = new Thickness(0, 0, 10, 0);
            scroll.Content = panelGrid;

            Grid grid = new Grid();
            panelGrid.Children.Add(grid);
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[0].Width = GridLength.Auto;
            grid.ColumnDefinitions[1].Width = GridLength.Auto;

            if (action == "Operazioni")
            {
                this.Title = "Operazioni Abilitate";
                this.selectedProfile.OperazioniProfile.OrderBy(c => c.Ordine).ToList().ForEach(categoriaOperazione =>
                {
                    grid.RowDefinitions.Add(new RowDefinition());
                    grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
                    lbl = new Label() { Content = categoriaOperazione.Descrizione, HorizontalContentAlignment = HorizontalAlignment.Center };
                    grid.Children.Add(lbl);
                    Grid.SetColumn(lbl, 0);
                    Grid.SetColumnSpan(lbl, grid.ColumnDefinitions.Count);
                    Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

                    categoriaOperazione.ListaOperazioni.OrderBy(o => o.Ordine).ToList().ForEach(operazione => 
                    {
                        grid.RowDefinitions.Add(new RowDefinition());
                        grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
                        lbl = new Label() { Content = operazione.Descrizione, HorizontalContentAlignment = HorizontalAlignment.Left };
                        grid.Children.Add(lbl);
                        Grid.SetColumn(lbl, 0);
                        Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);
                        
                        checkBox = new CheckBox() { IsChecked = operazione.Enabled, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
                        checkBox.Click += (o, e) => { operazione.Enabled = ((CheckBox)o).IsChecked.Value; operazione.Modificato = true; };
                        operazione.EventModificato += (o, e) => profile.Modificato = true;

                        grid.Children.Add(checkBox);
                        Grid.SetColumn(checkBox, 1);
                        Grid.SetRow(checkBox, grid.RowDefinitions.Count - 1);
                    });
                });
            }
            else if (action == "CodiciLocali")
            {
                this.Title = "Codici Locali Abilitati";
                if (this.selectedProfile.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == codiceSistema.CodiceSistema) != null)
                {
                    this.selectedProfile.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == codiceSistema.CodiceSistema).CodiciLocale.ForEach(codiceLocale =>
                    {
                        grid.RowDefinitions.Add(new RowDefinition());
                        grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
                        lbl = new Label() { Content = codiceLocale.CodiceLocale + " " + codiceLocale.Descrizione, HorizontalContentAlignment = HorizontalAlignment.Left };
                        grid.Children.Add(lbl);
                        Grid.SetColumn(lbl, 0);
                        Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

                        checkBox = new CheckBox() { IsChecked = codiceLocale.Enabled, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
                        checkBox.Click += (o, e) => { codiceLocale.Enabled = ((CheckBox)o).IsChecked.Value; this.selectedProfile.Modificato = true; };

                        grid.Children.Add(checkBox);
                        Grid.SetColumn(checkBox, 1);
                        Grid.SetRow(checkBox, grid.RowDefinitions.Count - 1);
                    });
                }
            }
            else if (action == "CFOrganizzatori")
            {
                this.Title = "Organizzatori Abilitati";
                if (this.selectedProfile.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == codiceSistema.CodiceSistema) != null)
                {
                    this.selectedProfile.CodiciSistema.FirstOrDefault(cs => cs.CodiceSistema == codiceSistema.CodiceSistema).CFOrganizzatori.ForEach(organizzatore =>
                    {
                        grid.RowDefinitions.Add(new RowDefinition());
                        grid.RowDefinitions[grid.RowDefinitions.Count - 1].Height = GridLength.Auto;
                        lbl = new Label() { Content =  organizzatore.CFOrganizzatore + " " + organizzatore.Descrizione, HorizontalContentAlignment = HorizontalAlignment.Left };
                        grid.Children.Add(lbl);
                        Grid.SetColumn(lbl, 0);
                        Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);

                        checkBox = new CheckBox() { IsChecked = organizzatore.Enabled, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
                        checkBox.Click += (o, e) => { organizzatore.Enabled = ((CheckBox)o).IsChecked.Value; this.selectedProfile.Modificato = true; };

                        grid.Children.Add(checkBox);
                        Grid.SetColumn(checkBox, 1);
                        Grid.SetRow(checkBox, grid.RowDefinitions.Count - 1);
                    });
                }
            }

            this.SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
            this.Height = 400;
        }

        public ManagerWindow(string token, clsAccount account, string title, List<clsItemGenericData> GenericData)
        {
            this.token = token;
            this.account = account;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            this.Title = title;
            this.genericData = GenericData;

            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(3);
            panel.HorizontalAlignment = HorizontalAlignment.Stretch;
            panel.VerticalAlignment = VerticalAlignment.Stretch;
            this.AddChild(panel);

            ScrollViewer scroll = new ScrollViewer();
            scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            panel.Children.Add(scroll);

            Label lbl;
            TextBox textBox;
            ComboBox combo;
            Button btn;

            StackPanel panelGrid = new StackPanel();
            panelGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            panelGrid.VerticalAlignment = VerticalAlignment.Top;
            panelGrid.Margin = new Thickness(0, 0, 10, 0);
            scroll.Content = panelGrid;

            Grid grid = new Grid();
            panelGrid.Children.Add(grid);
            grid.HorizontalAlignment = HorizontalAlignment.Stretch;
            grid.VerticalAlignment = VerticalAlignment.Top;
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());

            this.genericData.ForEach(item =>
            {
                if (item.Values != null && item.Values.Count > 0)
                {
                    lbl = new Label() { Content = item.Description };
                    combo = new ComboBox() { Tag = item };
                    foreach (KeyValuePair<object, string> itemToChoice in item.Values)
                    {
                        ComboBoxItem singleItemCombo = new ComboBoxItem() { MinHeight = 30 };
                        singleItemCombo.Content = itemToChoice.Value;
                        singleItemCombo.Tag = itemToChoice.Key;
                        combo.Items.Add(singleItemCombo);
                    }
                    combo.SelectionChanged += (o, e) => 
                    {
                        ComboBox comboEvent = (ComboBox)o;
                        clsItemGenericData itemEvent = (clsItemGenericData)comboEvent.Tag;
                        if (comboEvent.SelectedItem != null)
                        {
                            ComboBoxItem comboItemEvent = (ComboBoxItem)comboEvent.SelectedItem;
                            itemEvent.Value = comboItemEvent.Tag;
                        }
                        else
                            itemEvent.Value = null;
                    };
                    AddRowProperty(grid, lbl, combo);
                }
                else if (item.Selection)
                {
                    btn = new Button() { Content = item.Description, Margin = new Thickness(3), MinHeight = 30 };
                    btn.Tag = item;
                    btn.Click += (o, e) => 
                    { 
                        Button btnSelected = (Button)o; 
                        ((clsItemGenericData)btnSelected.Tag).Value = true; 
                        this.DialogResult = true; 
                    };
                    AddRowProperty(grid, null, btn);
                }
                else
                {
                    if (item.Type == typeof(string))
                    {
                        lbl = new Label() { Content = item.Description };
                        textBox = new TextBox() { Tag = item, Text = (item.Value != null ? item.Value.ToString() : ""), MinHeight = 30, HorizontalAlignment = HorizontalAlignment.Stretch, VerticalContentAlignment = VerticalAlignment.Center };
                        textBox.TextChanged += (o, e) => 
                        {
                            TextBox textBoxEvent = (TextBox)o;
                            clsItemGenericData itemEvent = (clsItemGenericData)textBoxEvent.Tag;
                            item.Value = textBoxEvent.Text; 
                        };
                        AddRowProperty(grid, lbl, textBox);
                    }
                    
                }
            });

            grid.RowDefinitions.Add(new RowDefinition());

            btn = new Button() { Content = "Abbandona", Margin = new Thickness(3), MinHeight = 30 };
            grid.Children.Add(btn);
            Grid.SetColumn(btn, 0);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);
            btn.Click += (o, e) => { this.DialogResult = false; };

            btn = new Button() { Content = "Conferma", Margin = new Thickness(3), MinHeight = 30 };
            grid.Children.Add(btn);
            Grid.SetColumn(btn, 1);
            Grid.SetRow(btn, grid.RowDefinitions.Count - 1);

            btn.Click += (o, e) => { this.DialogResult = true; };

            this.MinWidth = 600;
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }

        #endregion
    }
}
