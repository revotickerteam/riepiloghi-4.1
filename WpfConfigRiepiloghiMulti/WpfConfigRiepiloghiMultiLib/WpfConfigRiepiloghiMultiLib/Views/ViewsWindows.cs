﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfConfigRiepiloghiMultiLib.Views
{
    public class clsButton : Button
    {
        public clsButton()
            : base()
        { }

        public clsButton(string content)
        {
            TextBlock lbl = new TextBlock();
            lbl.Margin = new Thickness(3);
            lbl.Text = content;
            lbl.VerticalAlignment = VerticalAlignment.Center;
            this.Content = lbl;
        }

        public clsButton(BitmapImage bitmap)
        {
            Image image = new Image();
            image.RenderSize = this.RenderSize;
            image.Source = bitmap;
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(3);
            this.Content = image;
        }

        public clsButton(BitmapImage bitmap, Size renderSize)
        {
            this.Width = renderSize.Width;
            this.Height = renderSize.Height;
            this.RenderSize = renderSize;
            Image image = new Image();
            image.RenderSize = this.RenderSize;
            image.Source = bitmap;
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(3);
            this.Content = image;
        }

        public clsButton(BitmapImage bitmap, string content)
        {
            StackPanel st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            Image image = new Image();
            image.RenderSize = this.RenderSize;
            image.Source = bitmap;
            image.Stretch = Stretch.Fill;
            image.Margin = new Thickness(3);
            st.Children.Add(image);
            TextBlock lbl = new TextBlock();
            lbl.Margin = new Thickness(3);
            lbl.Text = content;
            lbl.VerticalAlignment = VerticalAlignment.Center;
            st.Children.Add(lbl);
            this.Content = st;
        }
    }

    public class clsButtonItemVisible : Button
    {
        public ManagerWindow windowRef { get; set; }

        public clsButtonItemVisible()
            :base()
        {
        }

        public clsButtonItemVisible(ManagerWindow refWin)
            :this()
        {
            this.windowRef = refWin;
        }
    }
    public class ViewsWindows : WindowBase
    {

        public static BitmapImage LoadImage(byte[] imageData, int width = 0, int height = 0)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.None;
                image.CacheOption = BitmapCacheOption.OnLoad;
                if (width > 0 && height > 0)
                {
                    image.DecodePixelHeight = width;
                    image.DecodePixelWidth = height;
                }

                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        public static MessageBoxResult MsgBox(string messageBoxText)
        {
            return MsgBox(messageBoxText, "");
        }

        public static MessageBoxResult MsgBox(string messageBoxText, string caption)
        {
            return MsgBox(messageBoxText, caption, MessageBoxButton.OK);
        }

        public static MessageBoxResult MsgBox(string messageBoxText, string caption, MessageBoxButton button)
        {
            return MsgBox(messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static MessageBoxResult MsgBox(string messageBoxText, string caption, MessageBoxButton button, MessageBoxImage image)
        {
            MessageBoxResult result = MessageBoxResult.None;
            int row = 0;

            ViewsWindows msg = new ViewsWindows();
            msg.Topmost = true;
            msg.ShowInTaskbar = false;
            msg.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            msg.WindowStyle = WindowStyle.None;
            msg.WindowState = System.Windows.WindowState.Normal;
            msg.SizeToContent = SizeToContent.WidthAndHeight;

            MemoryStream stream = null;

            DockPanel dp = null;

            Grid grid = new Grid();
            grid.Margin = new Thickness(1);

            msg.AddChild(grid);

            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions[0].Height = GridLength.Auto;

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions[0].Width = GridLength.Auto;
            grid.ColumnDefinitions[1].Width = new GridLength(2, GridUnitType.Star);
            grid.ColumnDefinitions[2].Width = GridLength.Auto;

            BitmapImage bitmapImage = null;

            TextBlock text = null;

            if (!string.IsNullOrEmpty(caption))
            {
                dp = new DockPanel();
                dp.Margin = new Thickness(2, 0, 2, 1);
                dp.Background = Brushes.Black;
                dp.MinHeight = 25;
                text = new TextBlock() { Text = " " + caption };
                text.Margin = new Thickness(1);
                text.VerticalAlignment = VerticalAlignment.Stretch;
                text.Background = Brushes.LightSkyBlue;
                dp.Children.Add(text);
                grid.Children.Add(dp);
                Grid.SetRow(dp, row);
                Grid.SetColumn(dp, 0);
                Grid.SetColumnSpan(dp, grid.ColumnDefinitions.Count);
            }

            row += 1;

            Label labelTextImage = new Label();
            labelTextImage.Margin = new Thickness(2);

            Label labelImage = new Label();

            labelImage.Content = "";
            labelImage.Margin = new Thickness(2);

            int width = 32;
            switch (image)
            {
                case MessageBoxImage.Information:
                    {
                        bitmapImage = LoadImage(WpfConfigRiepiloghiMultiLib.Properties.Resources.info, width, width);
                        labelTextImage.Content = "Informazione";
                        break;
                    }
                case MessageBoxImage.Error: //MessageBoxImage.Hand: MessageBoxImage.Stop
                    {
                        bitmapImage = LoadImage(WpfConfigRiepiloghiMultiLib.Properties.Resources.Error, width, width);
                        labelTextImage.Content = "Errore";
                        break;
                    }
                case MessageBoxImage.Question:
                    {
                        labelImage = new Label();
                        labelImage.FontSize = 24;
                        labelImage.Content = "?";
                        labelImage.HorizontalContentAlignment = HorizontalAlignment.Right;
                        break;
                    }
                case MessageBoxImage.Warning: //MessageBoxImage.Exclamation
                    {
                        bitmapImage = LoadImage(WpfConfigRiepiloghiMultiLib.Properties.Resources.warn, width, width);
                        labelTextImage.Content = "Attenzione";
                        break;
                    }
            }

            if (bitmapImage != null)
            {
                clsButton btnInfo = new clsButton(bitmapImage);
                btnInfo.MaxHeight = width;
                btnInfo.MaxWidth = width;
                btnInfo.Margin = new Thickness(2);
                grid.Children.Add(btnInfo);
                Grid.SetRow(btnInfo, row);
                Grid.SetColumn(btnInfo, 2);

                grid.Children.Add(labelTextImage);
                Grid.SetRow(labelTextImage, row);
                Grid.SetColumn(labelTextImage, 0);
                Grid.SetColumnSpan(labelTextImage, grid.ColumnDefinitions.Count - 1);
            }
            else
            {
                grid.Children.Add(labelImage);
                Grid.SetRow(labelImage, row);
                Grid.SetColumn(labelImage, 2);
                //Grid.SetColumnSpan(labelTextImage, grid.ColumnDefinitions.Count);
                grid.Children.Add(labelTextImage);
                Grid.SetRow(labelTextImage, row);
                Grid.SetColumn(labelTextImage, 0);
                Grid.SetColumnSpan(labelTextImage, grid.ColumnDefinitions.Count - 1);
            }

            row += 1;

            text = new TextBlock() { Text = messageBoxText };
            text.Margin = new Thickness(10);
            grid.Children.Add(text);
            Grid.SetRow(text, row);
            Grid.SetColumn(text, 0);
            Grid.SetColumnSpan(text, grid.ColumnDefinitions.Count);

            row += 1;

            Dictionary<string, MessageBoxResult> buttons = new Dictionary<string, MessageBoxResult>();

            switch (button)
            {
                case MessageBoxButton.OK: { buttons.Add("OK", MessageBoxResult.OK); break; }
                case MessageBoxButton.OKCancel:
                    {
                        buttons.Add("Abbandona", MessageBoxResult.Cancel);
                        buttons.Add("OK", MessageBoxResult.OK);
                        break;
                    }
                case MessageBoxButton.YesNo:
                    {
                        buttons.Add("Si", MessageBoxResult.Yes);
                        buttons.Add("No", MessageBoxResult.No);
                        break;
                    }
                case MessageBoxButton.YesNoCancel:
                    {
                        buttons.Add("Si", MessageBoxResult.Yes);
                        buttons.Add("No", MessageBoxResult.No);
                        buttons.Add("Abbandona", MessageBoxResult.Cancel);
                        break;
                    }
            }

            foreach (KeyValuePair<string, MessageBoxResult> item in buttons)
            {
                clsButton btn = new clsButton(item.Key) { Tag = item.Value };
                btn.Margin = new Thickness(2);
                btn.MinWidth = 100;
                btn.MinHeight = 30;
                grid.Children.Add(btn);
                Grid.SetRow(btn, row);
                if (buttons.Count == 1)
                {
                    Grid.SetColumn(btn, 0);
                    Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count);
                }
                else if (buttons.Count == 2)
                {
                    if (item.Equals(buttons.First()))
                    {
                        Grid.SetColumn(btn, 0);
                    }
                    else
                    {
                        Grid.SetColumn(btn, 1);
                        Grid.SetColumnSpan(btn, grid.ColumnDefinitions.Count - 1);
                    }
                }
                else if (buttons.Count == 3)
                {
                    if (item.Equals(buttons.First()))
                    {
                        Grid.SetColumn(btn, 0);
                    }
                    else if (item.Equals(buttons.Last()))
                    {
                        Grid.SetColumn(btn, 2);
                    }
                    else
                    {
                        Grid.SetColumn(btn, 1);
                    }
                }
                btn.Click += (o, e) =>
                {
                    result = (MessageBoxResult)((clsButton)o).Tag;
                    ((ViewsWindows)FindWindow((clsButton)o)).DialogResult = true;
                };
            }
            msg.ShowDialog();
            msg.Close();
            return result;
        }
    }
}
