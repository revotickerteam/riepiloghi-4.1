﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfConfigRiepiloghiMultiLib
{
    /// <summary>
    /// Logica di interazione per WindowBase.xaml
    /// </summary>
    public partial class WindowBase : Window
    {
        public static string fontFamily = "Bahnschrift Light Condensed";
        public static double fontSize = 18;
        public WindowBase()
        {
            this.FontFamily = new FontFamily(fontFamily);
            this.FontSize = fontSize;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.WindowStyle = WindowStyle.None;
            MemoryStream stream = new MemoryStream(WpfConfigRiepiloghiMultiLib.Properties.Resources.CreaLogo);
            this.Icon = BitmapFrame.Create(stream);

        }

        public static Window FindWindow(UIElement element)
        {
            var parent = VisualTreeHelper.GetParent(element) as UIElement;
            while (VisualTreeHelper.GetParent(parent) != null)
            {
                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return (Window)parent;
        }
        public static Point GetPointInWindow(UIElement element)
        {
            var parent = VisualTreeHelper.GetParent(element) as UIElement;
            while (VisualTreeHelper.GetParent(parent) != null)
            {
                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            Point point = element.TransformToVisual(parent).Transform(new Point(0, 0));
            return point;
        }
        public static Point GetPointInScreen(UIElement element)
        {
            Window window = FindWindow(element);
            Point pointInWindow = GetPointInWindow(element);
            Point point = new Point(window.Left + pointInWindow.X, window.Top + pointInWindow.Y);
            return point;
        }
    }
}
