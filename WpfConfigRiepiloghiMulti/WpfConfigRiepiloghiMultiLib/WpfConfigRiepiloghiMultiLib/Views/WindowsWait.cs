﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfConfigRiepiloghiMultiLib.Views
{
    public class WindowsWait : WindowBase
    {
        private static WindowsWait wait { get; set; }
        public static void SetWaitMessage(string message)
        {
            if (wait != null)
            {
                wait.Close();
                wait = null;
            }
            if (message != "")
            {
                wait = new WindowsWait(message);
                wait.Show();
                Action emptyDelegateLoader = delegate { };
                wait.Dispatcher.Invoke(emptyDelegateLoader, System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        private Label lbl { get; set; }
        private Grid grid { get; set; }

        private DateTime dtStart { get; set; }
        public WindowsWait(string message)
            : base()
        {
            this.SizeToContent = SizeToContent.WidthAndHeight;
            this.ShowInTaskbar = false;
            dtStart = DateTime.Now;
            this.Topmost = true;
            this.AllowsTransparency = true;
            this.Background = Brushes.Black;
            this.WindowStyle = WindowStyle.None;
            DockPanel panel = new DockPanel();
            panel.Background = Brushes.LightGray;
            panel.Margin = new Thickness(1, 1, .5, .5);
            this.AddChild(panel);

            grid = new Grid();
            grid.Margin = new Thickness(5);
            grid.RowDefinitions.Add(new RowDefinition());
            panel.Children.Add(grid);

            lbl = new Label();
            grid.Children.Add(lbl);
            Grid.SetRow(lbl, grid.RowDefinitions.Count - 1);
            lbl.Content = message;
            lbl.HorizontalAlignment = HorizontalAlignment.Stretch;
            lbl.VerticalAlignment = VerticalAlignment.Bottom;
            lbl.BorderThickness = new Thickness(0);
            lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
            lbl.VerticalContentAlignment = VerticalAlignment.Center;
            lbl.Margin = new Thickness(5);
        }
    }
}
